#pragma once

// Was pretty much a straight copy of Pyar's quaternion class, since I needed one and was too lazy to write my own, has some extra features added and some bugs fixed.

#include "Vector/VectorCommon.h"
#include "Matrix/MatrixCommon.h"

namespace Math
{
	/* Copyright 2011 Pyarelal Knowles, under GNU LGPL (see LICENCE.txt) */
	// With some modifications and additions by Jess Archer (eg the toRation method, and other stuff I can't remember).
	struct Quat
	{
		float x;
		float y;
		float z;
		float w;

		Quat();
		Quat(float nx, float ny, float nz, float nw);
		Quat(float angle, vec3 vec);
		explicit Quat(mat3 m);
		explicit Quat(mat4 m);

		Quat operator - () const;
		Quat operator + (const Quat &A) const; //sum quat components
		Quat operator - (const Quat &A) const; //sub quat components
		Quat operator * (float d) const; //scale quat components
		Quat operator * (const Quat &A) const; //quaternion rotation
		vec3 operator * (const vec3 &v) const; //rotate vector (via mat33)
		Quat operator / (float d) const; //divide quat components
		Quat operator / (const Quat &A) const; //return Quat rotation from A to *this
		Quat &operator += (const Quat &A); //sum quat components
		Quat &operator -= (const Quat &A); //sub quat components
		Quat &operator *= (float d); //scale quat components
		Quat &operator *= (const Quat &A); //quaternion rotation
		Quat &operator /= (float d); //divide quat components
		float operator [] (int index);
		bool operator == (const Quat &A); //test if all elements match. FIXME: use epsilon
		bool operator != (const Quat &A);
		operator float *() { return (float*) &x; }

		void angleAxis(float &a, vec3 &v) const;
		float getAngle() const;
		vec3 getAxis() const;
		Quat inverse() const;
		float dot(const Quat &A) const;
		vec3 euler() const; //pitch, yaw, roll
		float sqsize() const; //this squared + A squared
		float size() const; //length of vector

		Quat unit() const; //returns normalized quat
		Quat &normalize(void);
		mat4 getMatrix() const; //returns the matrix representation of this quaternion. Note: normalize first!

		Quat slerp(Quat q, float d) const;
		Quat lerp(Quat q, float d) const;
		Quat nlerp(Quat q, float d) const;

		static Quat identity();
		static Quat fromEuler(float pitch, float yaw, float roll);
		static Quat fromEuler(const vec2 &angles);
		static Quat fromEuler(const vec3 &angles);
		static Quat fromTo(const vec3 &from, const vec3 &to);
		static Quat toRotation(const vec3 &zAxis = vec3(0, 0, 1), const vec3 &yAxis = vec3(0, 1, 0), const vec3 &xAxis = vec3(1, 0, 0));
		static Quat random();
	};
}

std::ostream &operator << (std::ostream &out, Math::Quat q);

