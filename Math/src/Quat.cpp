#include "Quat.h"

using namespace Math;

/* Copyright 2011 Pyarelal Knowles, under GNU LGPL (see LICENCE.txt) */
const float quatTolerance = 0.00001f;

const float pi = 3.14159265f;

Quat::Quat()
{
	x = 0.0f; y = 0.0f; z = 0.0f; w = 1.0f;
}

Quat::Quat(float nx, float ny, float nz, float nw)
{
	x = nx;
	y = ny;
	z = nz;
	w = nw;
}

Quat::Quat(float angle, vec3 vec)
{
	vec.normalize();
	angle *= 0.5f;
	float sina = sinf(angle);
	w = cosf(angle);
	x = vec.x * sina;
	y = vec.y * sina;
	z = vec.z * sina;
}

Quat::Quat(mat3 m)
{
	//http://www.flipcode.com/documents/matrfaq.html#Q55
	// 0  3  6
	// 1  4  7
	// 2  5  8
	float T = m[0] + m[4] + m[8] + 1.0f;
	float S;
	if (T > 0.0f)
	{
		S = 0.5f / sqrt(T);
		x = (m[5] - m[7]) * S;
		y = (m[6] - m[2]) * S;
		z = (m[1] - m[3]) * S;
		w = 0.25f / S;
	}
	else
	{
		//a/b/c = array indices greater than others. find maximum index
		//a = 0 > 1
		//b = 0 > 2
		//c = 1 > 2
		
		// +true/!false: order = decimal = binary
		//!a !b !c: 0 1 2 = 2 = 1 0
		//!a !b +c: 0 2 1 = 1 = 0 1
		//+a !b !c: 1 0 2 = 2 = 1 0
		//+a +b !c: 1 2 0 = 0 = 0 0
		//!a +b +c: 2 0 1 = 1 = 0 1
		//+a +b +c: 2 1 0 = 0 = 0 0
		
		//reorder for binary repr
		//!a !b !c: 0 1 2 = 2 = 1 0
		//!a !b +c: 0 2 1 = 1 = 0 1
		//!a +b !c: not possible
		//!a +b +c: 2 0 1 = 1 = 0 1
		//+a !b !c: 1 0 2 = 2 = 1 0
		//+a !b +c: not possible
		//+a +b !c: 1 2 0 = 0 = 0 0
		//+a +b +c: 2 1 0 = 0 = 0 0
		
		//bit 1 = !bc + !ab (from: http://www.ee.calpoly.edu/media/uploads/resources/KarnaughExplorer_1.html)
		//bit 2 = !a!c + a!b
		
		static int precompModulo[3] = {1, 2, 0};
		int i = 0;
		if (m[4] > m[0])
			i = 1;
		if (m[8] > m.t[i][i])
			i = 2;
		int j = precompModulo[i+1];
		int k = precompModulo[i+2];
		
		S = sqrt(m.t[i][i] - m.t[j][j] - m.t[k][k] + 1.0f);
		*this[i] = 0.5f / S;
		S = 0.5f / S;
		w = (m.t[k][j] - m.t[j][k]) * S;
		*this[j] = (m.t[j][i] + m.t[i][j]) * S;
		*this[k] = (m.t[k][i] + m.t[i][k]) * S;
		printf("FIXME: using untested code\n");
	}
}

Quat::Quat(mat4 m)
{
	//http://www.flipcode.com/documents/matrfaq.html#Q55
	// 0  4  8  12
	// 1  5  9  13
	// 2  6  10 14
	// 3  7  11 15
	float S;
	float T = m[0] + m[5] + m[10] + 1.0f;
	if (T > 0.0f)
	{
		S = 0.5f / sqrt(T);
		x = (m[6] - m[9]) * S;
		y = (m[8] - m[2]) * S;
		z = (m[1] - m[4]) * S;
		w = 0.25f / S;
	}
	else
	{
		static int precompModulo[3] = {1, 2, 0};
		int i = 0;
		if (m[5] > m[0])
			i = 1;
		if (m[10] > m.t[i][i])
			i = 2;
		int j = precompModulo[i + 1];
		int k = precompModulo[i + 2];
		
		S = sqrt(m.t[i][i] - m.t[j][j] - m.t[k][k] + 1.0f) * 2.0f;
		*this[i] = 0.5f / S;
		w = (m.t[k][j] - m.t[j][k]) / S;
		*this[j] = (m.t[j][i] + m.t[i][j]) / S;
		*this[k] = (m.t[k][i] + m.t[i][k]) / S;
		printf("FIXME: using untested code\n");
	}
}

Quat Quat::operator - () const
{
	return Quat(-x, -y, -z, -w);
}

Quat Quat::operator + (const Quat &A) const
{
	return Quat(x + A.x, y + A.y, z + A.z, w + A.w);
}

Quat Quat::operator - (const Quat &A) const
{
	return Quat(x - A.x, y - A.y, z - A.z, w - A.w);
}

Quat Quat::operator * (float d) const
{
	return Quat(x * d, y * d, z * d, w * d);
}

Quat Quat::operator * (const Quat &A) const
{
	return Quat(
		x * A.w + w * A.x + y * A.z - z * A.y,
		w * A.y - x * A.z + y * A.w + z * A.x,
		w * A.z + x * A.y - y * A.x + z * A.w,
		w * A.w - x * A.x - y * A.y - z * A.z);
#if 0
	Quat q = *this;
	Quat r = A;
	Quat t;
	t.w = r.w*q.w - r.x*q.x - r.y*q.y - r.z*q.z;
	t.x = r.w*q.x + r.x*q.w - r.y*q.z + r.z*q.y;
	t.y = r.w*q.y + r.x*q.z + r.y*q.w - r.z*q.x;
	t.z = r.w*q.z - r.x*q.y + r.y*q.x + r.z*q.w;
	return t;
#endif
}

vec3 Quat::operator * (const vec3 &v) const
{
	float m[9];
	m[0] = w * w + x * x - y * y - z * z;	m[1] = 2 * x * y - 2 * w * z;			m[2] = 2 * x * z + 2 * w * y;
	m[3] = 2 * x * y + 2 * w * z;			m[4] = w * w - x * x + y * y - z * z;	m[5] = 2 * y * z - 2 * w * x;
	m[6] = 2 * x * z - 2 * w * y;			m[7] = 2 * y * z + 2 * w * x;			m[8] = w * w - x * x - y * y + z * z;

	vec3 r;
	r.x = m[0] * v.x + m[1] * v.y + m[2] * v.z;
	r.y = m[3] * v.x + m[4] * v.y + m[5] * v.z;
	r.z = m[6] * v.x + m[7] * v.y + m[8] * v.z;
	return r;
}

Quat Quat::operator / (float d) const
{
	return Quat(x / d, y / d, z / d, w / d);
}

Quat Quat::operator / (const Quat &A) const
{
	float qq =
		A.x * A.x + 
		A.y * A.y + 
		A.z * A.z + 
		A.w * A.w;

	return *this * Quat(
		-A.x / qq,
		-A.y / qq,
		-A.z / qq,
		A.w / qq
		);
}

Quat &Quat::operator += (const Quat &A)
{
	x += A.x;
	y += A.y;
	z += A.z;
	w += A.w;
	return *this;
}

Quat &Quat::operator -= (const Quat &A)
{
	x -= A.x;
	y -= A.y;
	z -= A.z;
	w -= A.w;
	return *this;
}

Quat &Quat::operator *= (float d)
{
	x *= d;
	y *= d;
	z *= d;
	w *= d;
	return *this;
}

Quat &Quat::operator*=(const Quat &A)
{
	float tx = x * A.w + w * A.x + y * A.z - z * A.y;
	float ty = w * A.y - x * A.z + y * A.w + z * A.x;
	float tz = w * A.z + x * A.y - y * A.x + z * A.w;

	w = w * A.w - x * A.x - y * A.y - z * A.z;
	x = tx;
	y = ty;
	z = tz;
	return *this;
}

Quat &Quat::operator/=(float d)
{
	x /= d;
	y /= d;
	z /= d;
	w /= d;
	return *this;
}

float Quat::operator[](int index)
{
	if (index == 0)
	{
		return x;
	}
	else if (index == 1)
	{
		return y;
	}
	else if (index == 2)
	{
		return z;
	}
	else if (index == 3)
	{
		return w;
	}
	return 0.0f;
}

bool Quat::operator == (const Quat &A)
{
	return x == A.x &&
		y == A.y &&
		z == A.z &&
		w == A.w;
}

bool Quat::operator != (const Quat &A)
{
	return x != A.x ||
		y != A.y ||
		z != A.z ||
		w != A.w;
}

void Quat::angleAxis(float &a, vec3 &v) const
{
	float wsign = w > 0.0f ? 1.0f : -1.0f; //get shortest direction
	float ww = sqrt(1.0f - w * w) * wsign;
	if (ww == 0.0f)
		v = vec3(0, 1, 0);
	else
		v = vec3(x / ww, y / ww, z / ww);
	a = 2.0f * acos(w * wsign);
}

float Quat::getAngle() const
{
	return 2.0f * acos(fabs(w));
}

vec3 Quat::getAxis() const
{
	float ww = sqrt(1.0f - w * w);
	if (ww == 0.0f)
		return vec3(0, 1, 0);
	else
		return vec3(x / ww, y / ww, z / ww);
}

Quat Quat::inverse() const
{
	float qq =
		x * x + 
		y * y + 
		z * z + 
		w * w;
	return Quat(-x, -y, -z, w) / qq;
}

float Quat::dot(const Quat &A) const
{
	return
		x * A.x +
		y * A.y +
		z * A.z +
		w * A.w;
}

vec3 Quat::euler() const
{
	//awwwwesome: http://www.3dgametechnology.com/wp/converting-quaternion-to-euler-angle/
	float sqx = x * x;
	float sqy = y * y;
	float sqz = z * z;
	float sqw = w * w;

	float unit = sqx + sqy + sqz + sqw;
	float test = (x * w - y * z);
	
	if (test > 0.4999999f * unit)
	{
		return vec3(pi / 2.0f, 2.0f * atan2(y, w), 0.0f);
	}
	else if (test < -0.4999999f * unit)
	{
		return vec3(-pi / 2.0f, 2.0f * atan2(y, w), 0.0f);
	}
    else
    {
		return vec3(
			asin(2.0f * (x * w - y * z)),
			atan2(2.0f * (x * z + y * w), 1.0f - 2.0f * (sqx + sqy)),
			atan2(2.0f * (x * y + z * w), 1.0f - 2.0f * (sqx + sqz))
			);
	}
	
	//stupid fail coordinate systems

	//nope: http://forums.create.msdn.com/forums/t/4574.aspx
	//atan2(2.0f*(x*w-y*z), 1.0f - 2.0f*(x*x+z*z)),
	//atan2(2.0f*(y*w-x*z), 1.0f - 2.0f*(y*y+z*z)),
	//asin(2.0f*(x*y+z*w))
	
	//full of crap: http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/
	//atan2(2.0f*(y*w-x*z), 1.0f - 2.0f*(y*y+z*z)),
	//asin(2.0f*(x*y + z*w)),
	//atan2(2.0f*(x*w-y*z), 1.0f - 2.0f*(x*x+z*z))
		
	//full of crap: http://en.wikipedia.org/wiki/Euler_angles
	//atan2(2.0f*(w*x+y*z), 1.0f - 2.0f*(x*x+y*y)),
	//asin(2.0f*(w*y - z*x)),
	//atan2(2.0f*(w*z+x*y), 1.0f - 2.0f*(y*y+z*z))
}

float Quat::sqsize() const
{
	return x * x + 
		y * y + 
		z * z + 
		w * w;
}

float Quat::size() const
{
	return sqrt(x * x + 
		y * y + 
		z * z + 
		w * w);
}

Quat Quat::unit() const
{
	float size = sqrt(
		x * x + 
		y * y + 
		z * z + 
		w * w
		);
	return Quat(x / size, y / size, z / size, w / size);
}

Quat &Quat::normalize()
{
	float size = sqrt(
		x * x + 
		y * y + 
		z * z + 
		w * w
		);
	if (size == 0.0f)
	{
		// FIXME: not sure if this is necessary or not
		w = 1.0f;
		return *this;
	}
	if (w < 0)
		size = -size;
	x /= size;
	y /= size;
	z /= size;
	w /= size;
	return *this;
}

mat4 Quat::getMatrix() const
{
	//from: http://gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation
	float x2 = x * x;
	float y2 = y * y;
	float z2 = z * z;
	float xy = x * y;
	float xz = x * z;
	float yz = y * z;
	float wx = w * x;
	float wy = w * y;
	float wz = w * z;
	return mat4( 1.0f - 2.0f * (y2 + z2), 2.0f * (xy - wz), 2.0f * (xz + wy), 0.0f,
		2.0f * (xy + wz), 1.0f - 2.0f * (x2 + z2), 2.0f * (yz - wx), 0.0f,
		2.0f * (xz - wy), 2.0f * (yz + wx), 1.0f - 2.0f * (x2 + y2), 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
}

Quat Quat::slerp(Quat q, float d) const
{
	float cosHalfTheta = dot(q);
	if (fabs(cosHalfTheta) >= 1.0f)
		return *this;
	if (cosHalfTheta < 0.0)
	{
		q = -q;
		cosHalfTheta = -cosHalfTheta;
	}
	float halfTheta = acos(cosHalfTheta);
	float sinHalfTheta = sqrt(1.0f - cosHalfTheta * cosHalfTheta);
	if (fabs(sinHalfTheta) < 0.0001)
		return (*this + q) * 0.5;
	float a = sin((1.0f - d) * halfTheta) / sinHalfTheta;
	float b = sin(d * halfTheta) / sinHalfTheta;
	return (*this * a + q * b).unit(); //wtf shouldn't have to normalize
}

Quat Quat::lerp(Quat q, float d) const
{
	return *this + (q - *this) * d;
}

Quat Quat::nlerp(Quat q, float d) const
{
	(void) (q);
	(void) (d);
	return *this; //NOT IMPLEMENTED
}

Quat Quat::identity()
{
	Quat r;
	r.x = 0.0f;
	r.y = 0.0f;
	r.z = 0.0f;
	r.w = 1.0f;
	return r;
}

Quat operator - (const Quat& q) //negate
{
	Quat r;
	r.w = -q.w; //FIXME: should w be kept positive?
	r.x = -q.x;
	r.y = -q.y;
	r.z = -q.z;
	return r;
}

Quat Quat::fromEuler(float pitch, float yaw, float roll)
{
	Quat r;

	pitch *= 0.5f;
	yaw *= 0.5f;
	roll *= 0.5f;
	float c2 = cos(pitch);
	float s2 = sin(pitch);
	float c1 = cos(yaw);
	float s1 = sin(yaw);
	float c3 = cos(roll);
	float s3 = sin(roll);
	
	r.w = c1 * c2 * c3 - s1 * s2 * s3;
	r.x = s1 * s2 * c3 + c1 * c2 * s3;
	r.y = s1 * c2 * c3 + c1 * s2 * s3;
	r.z = c1 * s2 * c3 - s1 * c2 * s3;
	return r;
	/*
	    Quaternion quaternion;
    float num9 = roll * 0.5f;

    float num6 = (float) Math.Sin((double) num9);
    float num5 = (float) Math.Cos((double) num9);
    float num8 = pitch * 0.5f;
    float num4 = (float) Math.Sin((double) num8);

    float num3 = (float) Math.Cos((double) num8);
    float num7 = yaw * 0.5f;
    float num2 = (float) Math.Sin((double) num7);
    float num = (float) Math.Cos((double) num7);

    quaternion.X = ((num * num4) * num5) + ((num2 * num3) * num6);
    quaternion.Y = ((num2 * num3) * num5) - ((num * num4) * num6);
    quaternion.Z = ((num * num3) * num6) - ((num2 * num4) * num5);
    quaternion.W = ((num * num3) * num5) + ((num2 * num4) * num6);

    return quaternion;
    */
}

Quat Quat::fromEuler(const vec2 &angles)
{
	return fromEuler(angles.x, angles.y, 0.0f);
}

Quat Quat::fromEuler(const vec3 &angles)
{
	return fromEuler(angles.x, angles.y, angles.z);
}

Quat Quat::fromTo(const vec3 &from, const vec3 &to)
{
	vec3 v1 = from.unit();
    vec3 v2 = to.unit();
    float d = v1.dot(v2);
    float s = sqrtf((1 + d) * 2.0f);
    if (s == 0.0f)
    {
        // Generate an axis
        vec3 a = vec3(0, 0, -1).cross(v1);
        // pick another if colinear
        if (a.lenSq() < 0.001f)
		{
			a = vec3(-1, 0, 0).cross(v1);
			return Quat((float) pi, a).normalize();
		}
		a.normalize();
		return Quat((float) pi, a).normalize();
    }
    else
    {
		vec3 i = v1.cross(v2) / s;
		return Quat(i.x, i.y, i.z, s * 0.5f).normalize();
    }
}

Quat Quat::toRotation(const vec3 &zAxis, const vec3 &yAxis, const vec3 &xAxis)
{
	vec3 f = zAxis.unit();
	vec3 u = yAxis.unit();
	vec3 s = xAxis.unit();

	mat3 kRot;
	kRot.t[0][0] = s.x;
	kRot.t[1][0] = s.y;
	kRot.t[2][0] = s.z;
	kRot.t[0][1] = u.x;
	kRot.t[1][1] = u.y;
	kRot.t[2][1] = u.z;
	kRot.t[0][2] = f.x;
	kRot.t[1][2] = f.y;
	kRot.t[2][2] = f.z;

	// 0  1  2  3  4  5  6  7  8
	//00 01 02 10 11 12 20 21 22

	Quat ret;

	float fTrace = kRot.t[0][0] + kRot.t[1][1] + kRot.t[2][2];

	if (fTrace > 0.0) // FIXME: or -1.0, or -0.5, not sure
	{
		float fRoot = sqrtf(fTrace + 1.0f);
		ret.w = 0.5f * fRoot;
		fRoot = 2.0f * fRoot;
		ret.x = (kRot.t[2][1] - kRot.t[1][2]) / fRoot;
		ret.y = (kRot.t[0][2] - kRot.t[2][0]) / fRoot;
		ret.z = (kRot.t[1][0] - kRot.t[0][1]) / fRoot;
		/*
		// |w| > 1/2, may as well choose w > 1/2
		fRoot = sqrtf(fTrace + 1.0f);  // 2w
		ret.w = 0.5f * fRoot;
		fRoot = 0.5f / fRoot;  // 1/(4w)
		ret.x = (kRot.t[2][1] - kRot.t[1][2]) * fRoot;
		ret.y = (kRot.t[0][2] - kRot.t[2][0]) * fRoot;
		ret.z = (kRot.t[1][0] - kRot.t[0][1]) * fRoot;
		*/
	}

	else
	{
		// |w| <= 1/2
		static size_t s_iNext[3] = { 1, 2, 0 };
		size_t i = 0;
		if (kRot.t[1][1] > kRot.t[0][0])
			i = 1;
		if (kRot.t[2][2] > kRot.t[i][i])
			i = 2;
		size_t j = s_iNext[i];
		size_t k = s_iNext[j];

		float fRoot = sqrtf(kRot.t[i][i] - kRot.t[j][j] - kRot.t[k][k] + 1.0f);
		float* apkQuat[3] = { &ret.x, &ret.y, &ret.z };
		*apkQuat[i] = 0.5f * fRoot;
		fRoot = 0.5f / fRoot;
		ret.w = (kRot.t[k][j] - kRot.t[j][k]) * fRoot;
		*apkQuat[j] = (kRot.t[j][i] + kRot.t[i][j]) * fRoot;
		*apkQuat[k] = (kRot.t[k][i] + kRot.t[i][k]) * fRoot;
	}

	return ret;
#if 0
	Quat ret;
	vec3f f = dir.unit();
	vec3f u = up.unit();
	vec3f s = f.cross(u).unit();
	u = s.cross(f);
	
	mat3f m;
	m[0] = s.x();
	m[1] = s.y();
	m[2] = s.z();
	m[3] = u.x();
	m[4] = u.y();
	m[5] = u.z();
	m[6] = -f.x();
	m[7] = -f.y();
	m[8] = -f.z();
	
	float r = sqrt(1.0f + m[0] + m[4] + m[8]);
	if (r == 0.0f)
	{
		// This is wrong
		ret.w = 1.0f;
		ret.x = ret.y = ret.z = 0.0f;
	}
	else
	{
		ret.w = r * 0.5f;
		ret.x = (m[5] - m[7]) / (2.0f * r);
		ret.y = (m[6] - m[2]) / (2.0f * r);
		ret.z = (m[1] - m[3]) / (2.0f * r);
	}
	return ret;
#endif
#if 0
	Quat ret;
	vec3f f = dir.unit();
	vec3f u = up.unit();
	vec3f s = right.unit();
	//u = s.cross(f);
	
	mat3f m;
	m[0] = s.x();
	m[1] = s.y();
	m[2] = s.z();
	m[3] = u.x();
	m[4] = u.y();
	m[5] = u.z();
	m[6] = f.x();
	m[7] = f.y();
	m[8] = f.z();
	
	float sqr = 1.0f + m[0] + m[4] + m[8];
	if (sqr <= 0.0f)
	{
		// Deal with some special cases
		/*if (m[8] == -1 && m[4] == -1 && m[0] == 1)
			return Quat(1, 0, 0, 0);

		if (m[8] == 1 && m[4] == -1 && m[0] == -1)
			return Quat(0, 0, -1, 0);*/

		std::cout << "Bmdfsjipfrhnu3480hfuasdhbc803q4cghd8egwshd\n";
		ret.w = 1.0f;
		ret.x = ret.y = ret.z = 0.0f;
		return ret;
	}
	float r = sqrtf(sqr);
	ret.w = r * 0.5f;
	ret.x = ((m[5] - m[7]) / (2.0f * r));
	ret.y = ((m[6] - m[2]) / (2.0f * r));
	ret.z = ((m[1] - m[3]) / (2.0f * r));
	return ret;
#endif
	return Quat(0, 0, 0, 1);
}

Quat Quat::random()
{
	//http://jmonkeyengine.org/forum/topic/random-rotation/
	float u1 = rand() / (float) RAND_MAX;
	float u2 = rand() / (float) RAND_MAX;
	float u3 = rand() / (float) RAND_MAX;
	float u1sqrt = sqrt(u1);
	float u1m1sqrt = sqrt(1.0f - u1);
	float x = u1m1sqrt * sin(2 * pi * u2);
	float y = u1m1sqrt * cos(2 * pi * u2);
	float z = u1sqrt * sin(2 * pi * u3);
	float w = u1sqrt * cos(2 * pi * u3);
	return Quat(x, y, w, z);
}

std::ostream &operator << (std::ostream &out, Math::Quat q)
{
	out << q.x << ", " << q.y << ", " << q.z << ", " << q.w;
	return out;
}

