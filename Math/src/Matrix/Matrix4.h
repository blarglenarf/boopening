#pragma once

#include "../Vector/Vec4.h"
#include "../Vector/Vec3.h"

#include <cmath>

namespace Math
{
	template <typename T>
	class mat4_basic
	{
	public:
		vec4_basic<T> t[4];

	public:
		mat4_basic(const mat4_basic &m)
		{
			for (int i = 0; i < 4; i++)
				t[i] = m.t[i];
		}

		mat4_basic(T *t)
		{
			int k = 0;
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					this->t[i].m[j] = t[k++];
		}

		mat4_basic(T t[4][4])
		{
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					this->t[i].m[j] = t[i][j];
		}

		mat4_basic(T t1 = 0, T t2 = 0, T t3 = 0, T t4 = 0, T t5 = 0, T t6 = 0, T t7 = 0, T t8 = 0, T t9 = 0, T t10 = 0, T t11 = 0, T t12 = 0, T t13 = 0, T t14 = 0, T t15 = 0, T t16 = 0)
		{
			t[0].m[0] = t1;  t[0].m[1] = t2;  t[0].m[2] = t3;  t[0].m[3] = t4;
			t[1].m[0] = t5;  t[1].m[1] = t6;  t[1].m[2] = t7;  t[1].m[3] = t8;
			t[2].m[0] = t9;  t[2].m[1] = t10; t[2].m[2] = t11; t[2].m[3] = t12;
			t[3].m[0] = t13; t[3].m[1] = t14; t[3].m[2] = t15; t[3].m[3] = t16;
		}

		mat4_basic(const vec4_basic<T> &v1, const vec4_basic<T> &v2, const vec4_basic<T> &v3, const vec4_basic<T> &v4)
		{
			t[0] = v1;
			t[1] = v2;
			t[2] = v3;
			t[3] = v4;
		}

		mat4_basic &operator = (const mat4_basic &m)
		{
			for (int i = 0; i < 4; i++)
				t[i] = m.t[i];
			return *this;
		}

		mat4_basic operator * (const mat4_basic &m1) const
		{
			mat4_basic m;
			for (int i = 0; i < 4; i++)
			{
				auto tVec = vec4_basic<T>();
				for (int j = 0; j < 4; j++)
					tVec[j] = m1.t[j].m[i];

				for (int j = 0; j < 4; j++)
					m.t[j].m[i] = dot(t[j], tVec);
			}
			return m;
		}

		vec4_basic<T> operator * (const vec4_basic<T> &v) const
		{
			vec4_basic<T> v1, v2;
			for (int i = 0; i < 4; i++)
			{
				for (int j = 0; j < 4; j++)
					v2.m[j] = t[j].m[i];
				v1.m[i] = dot(v2, v);
			}
			return v1;
		}

		mat4_basic operator * (T u) const
		{
			mat4_basic m;
			for (int i = 0; i < 4; i++)
				m.t[i] = t[i] * u;
			return m;
		}

		mat4_basic &operator *= (const mat4_basic &m1)
		{
			mat4_basic m = *this * m1;
			*this = m;
			return *this;
		}

		mat4_basic &operator *= (const vec3_basic<T> &v)
		{
			mat4_basic m = *this * v;
			*this = m;
			return *this;
		}

		mat4_basic &operator *= (T u)
		{
			for (int i = 0; i < 4; i++)
				t[i] *= u;
			return *this;
		}

		mat4_basic operator + (const mat4_basic &m1) const
		{
			mat4_basic m;
			for (int i = 0; i < 4; i++)
				m.t[i] = t[i] + m1.t[i];
			return m;
		}

		mat4_basic operator + (T u) const
		{
			mat4_basic m;
			for (int i = 0; i < 4; i++)
				m.t[i] = t[i] + u;
			return m;
		}

		mat4_basic &operator += (const mat4_basic &m1)
		{
			for (int i = 0; i < 4; i++)
				t[i] += m1.t[i];
			return *this;
		}

		mat4_basic &operator += (T u)
		{
			for (int i = 0; i < 4; i++)
				t[i] += u;
			return *this;
		}

		mat4_basic operator - (const mat4_basic &m1) const
		{
			mat4_basic m;
			for (int i = 0; i < 4; i++)
				m.t[i] = t[i] - m1.t[i];
			return m;
		}

		mat4_basic operator - (T u) const
		{
			mat4_basic m;
			for (int i = 0; i < 4; i++)
				m.t[i] = t[i] - u;
			return m;
		}

		mat4_basic &operator -= (const mat4_basic &m1)
		{
			for (int i = 0; i < 4; i++)
				t[i] -= m1.t[i];
			return *this;
		}

		mat4_basic &operator -= (T u)
		{
			for (int i = 0; i < 4; i++)
				t[i] -= u;
			return *this;
		}

		mat4_basic &bias()
		{
			*this =  mat4_basic<T>(	0.5, 0, 0, 0,
									0, 0.5, 0, 0,
									0, 0, 0.5, 0,
									0.5, 0.5, 0.5, 1);
			return *this;
		}

		mat4_basic &identity()
		{
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					if (i == j)
						t[i].m[j] = 1;
					else
						t[i].m[j] = 0;
			return *this;
		}

		static mat4_basic getIdentity()
		{
			mat4_basic m;
			m.identity();
			return m;
		}

		mat4_basic &transpose()
		{
			mat4_basic m;
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					m.t[j].m[i] = t[i].m[j];
			*this = m;
			return *this;
		}

		mat4_basic getTranspose() const
		{
			mat4_basic m = *this;
			m.transpose();
			return m;
		}

		T det() const
		{
			return t[0].m[0]*t[1].m[1]*t[2].m[2]*t[3].m[3] + t[0].m[0]*t[1].m[2]*t[2].m[3]*t[3].m[1] + t[0].m[0]*t[1].m[3]*t[2].m[1]*t[3].m[2] +
				t[0].m[1]*t[1].m[0]*t[2].m[3]*t[3].m[2] + t[0].m[1]*t[1].m[2]*t[2].m[0]*t[3].m[3] + t[0].m[1]*t[1].m[3]*t[2].m[2]*t[3].m[0] +
				t[0].m[2]*t[1].m[0]*t[2].m[1]*t[3].m[3] + t[0].m[2]*t[1].m[1]*t[2].m[3]*t[3].m[0] + t[0].m[2]*t[1].m[3]*t[2].m[0]*t[3].m[1] +
				t[0].m[3]*t[1].m[0]*t[2].m[2]*t[3].m[1] + t[0].m[3]*t[1].m[1]*t[2].m[0]*t[3].m[2] + t[0].m[3]*t[1].m[2]*t[2].m[1]*t[3].m[0] -
				t[0].m[0]*t[1].m[1]*t[2].m[3]*t[3].m[2] - t[0].m[0]*t[1].m[2]*t[2].m[1]*t[3].m[3] - t[0].m[0]*t[1].m[3]*t[2].m[2]*t[3].m[1] -
				t[0].m[1]*t[1].m[0]*t[2].m[2]*t[3].m[3] - t[0].m[1]*t[1].m[2]*t[2].m[3]*t[3].m[0] - t[0].m[1]*t[1].m[3]*t[2].m[0]*t[3].m[2] -
				t[0].m[2]*t[1].m[0]*t[2].m[3]*t[3].m[1] - t[0].m[2]*t[1].m[1]*t[2].m[0]*t[3].m[3] - t[0].m[2]*t[1].m[3]*t[2].m[1]*t[3].m[0] -
				t[0].m[3]*t[1].m[0]*t[2].m[1]*t[3].m[2] - t[0].m[3]*t[1].m[1]*t[2].m[2]*t[3].m[0] - t[0].m[3]*t[1].m[2]*t[2].m[0]*t[3].m[1];
		}

		mat4_basic detReciprocal() const
		{
			return mat4(
				t[1].m[1]*t[2].m[2]*t[3].m[3]+t[2].m[1]*t[3].m[2]*t[1].m[3]+t[3].m[1]*t[1].m[2]*t[2].m[3]-t[1].m[1]*t[3].m[2]*t[2].m[3]-t[2].m[1]*t[1].m[2]*t[3].m[3]-t[3].m[1]*t[2].m[2]*t[1].m[3],
				t[0].m[1]*t[3].m[2]*t[2].m[3]+t[2].m[1]*t[0].m[2]*t[3].m[3]+t[3].m[1]*t[2].m[2]*t[0].m[3]-t[0].m[1]*t[2].m[2]*t[3].m[3]-t[2].m[1]*t[3].m[2]*t[0].m[3]-t[3].m[1]*t[0].m[2]*t[2].m[3],
				t[0].m[1]*t[1].m[2]*t[3].m[3]+t[1].m[1]*t[3].m[2]*t[0].m[3]+t[3].m[1]*t[0].m[2]*t[1].m[3]-t[0].m[1]*t[3].m[2]*t[1].m[3]-t[1].m[1]*t[0].m[2]*t[3].m[3]-t[3].m[1]*t[1].m[2]*t[0].m[3],
				t[0].m[1]*t[2].m[2]*t[1].m[3]+t[1].m[1]*t[0].m[2]*t[2].m[3]+t[2].m[1]*t[1].m[2]*t[0].m[3]-t[0].m[1]*t[1].m[2]*t[2].m[3]-t[1].m[1]*t[2].m[2]*t[0].m[3]-t[2].m[1]*t[0].m[2]*t[1].m[3],
				t[1].m[0]*t[3].m[2]*t[2].m[3]+t[2].m[0]*t[1].m[2]*t[3].m[3]+t[3].m[0]*t[2].m[2]*t[1].m[3]-t[1].m[0]*t[2].m[2]*t[3].m[3]-t[2].m[0]*t[3].m[2]*t[1].m[3]-t[3].m[0]*t[1].m[2]*t[2].m[3],
				t[0].m[0]*t[2].m[2]*t[3].m[3]+t[2].m[0]*t[3].m[2]*t[0].m[3]+t[3].m[0]*t[0].m[2]*t[2].m[3]-t[0].m[0]*t[3].m[2]*t[2].m[3]-t[2].m[0]*t[0].m[2]*t[3].m[3]-t[3].m[0]*t[2].m[2]*t[0].m[3],
				t[0].m[0]*t[3].m[2]*t[1].m[3]+t[1].m[0]*t[0].m[2]*t[3].m[3]+t[3].m[0]*t[1].m[2]*t[0].m[3]-t[0].m[0]*t[1].m[2]*t[3].m[3]-t[1].m[0]*t[3].m[2]*t[0].m[3]-t[3].m[0]*t[0].m[2]*t[1].m[3],
				t[0].m[0]*t[1].m[2]*t[2].m[3]+t[1].m[0]*t[2].m[2]*t[0].m[3]+t[2].m[0]*t[0].m[2]*t[1].m[3]-t[0].m[0]*t[2].m[2]*t[1].m[3]-t[1].m[0]*t[0].m[2]*t[2].m[3]-t[2].m[0]*t[1].m[2]*t[0].m[3],
				t[1].m[0]*t[2].m[1]*t[3].m[3]+t[2].m[0]*t[3].m[1]*t[1].m[3]+t[3].m[0]*t[1].m[1]*t[2].m[3]-t[1].m[0]*t[3].m[1]*t[2].m[3]-t[2].m[0]*t[1].m[1]*t[3].m[3]-t[3].m[0]*t[2].m[1]*t[1].m[3],
				t[0].m[0]*t[3].m[1]*t[2].m[3]+t[2].m[0]*t[0].m[1]*t[3].m[3]+t[3].m[0]*t[2].m[1]*t[0].m[3]-t[0].m[0]*t[2].m[1]*t[3].m[3]-t[2].m[0]*t[3].m[1]*t[0].m[3]-t[3].m[0]*t[0].m[1]*t[2].m[3],
				t[0].m[0]*t[1].m[1]*t[3].m[3]+t[1].m[0]*t[3].m[1]*t[0].m[3]+t[2].m[0]*t[0].m[1]*t[1].m[3]-t[0].m[0]*t[3].m[1]*t[1].m[3]-t[1].m[0]*t[0].m[1]*t[3].m[3]-t[3].m[0]*t[1].m[1]*t[0].m[3],
				t[0].m[0]*t[2].m[1]*t[1].m[3]+t[1].m[0]*t[0].m[1]*t[2].m[3]+t[2].m[0]*t[1].m[1]*t[0].m[3]-t[0].m[0]*t[1].m[1]*t[2].m[3]-t[1].m[0]*t[2].m[1]*t[0].m[3]-t[2].m[0]*t[0].m[1]*t[1].m[3],
				t[1].m[0]*t[3].m[1]*t[2].m[2]+t[2].m[0]*t[1].m[1]*t[3].m[2]+t[3].m[0]*t[2].m[1]*t[1].m[2]-t[1].m[0]*t[2].m[1]*t[3].m[2]-t[2].m[0]*t[3].m[1]*t[1].m[2]-t[3].m[0]*t[1].m[1]*t[2].m[2],
				t[0].m[0]*t[2].m[1]*t[3].m[2]+t[2].m[0]*t[3].m[1]*t[0].m[2]+t[3].m[0]*t[0].m[1]*t[2].m[2]-t[0].m[0]*t[3].m[1]*t[2].m[2]-t[2].m[0]*t[0].m[1]*t[3].m[2]-t[3].m[0]*t[2].m[1]*t[0].m[2],
				t[0].m[0]*t[3].m[1]*t[1].m[2]+t[1].m[0]*t[0].m[1]*t[3].m[2]+t[3].m[0]*t[1].m[1]*t[0].m[2]-t[0].m[0]*t[1].m[1]*t[3].m[2]-t[1].m[0]*t[3].m[1]*t[0].m[2]-t[3].m[0]*t[0].m[1]*t[1].m[2],
				t[0].m[0]*t[1].m[1]*t[2].m[2]+t[1].m[0]*t[2].m[1]*t[0].m[2]+t[2].m[0]*t[0].m[1]*t[1].m[2]-t[0].m[0]*t[2].m[1]*t[1].m[2]-t[1].m[0]*t[0].m[1]*t[2].m[2]-t[2].m[0]*t[1].m[1]*t[0].m[2]);
		}

		mat4_basic &invert()
		{
			*this = getInverse();
			return *this;
		}

		mat4_basic getInverse() const
		{
			// From glu, use elementary row operations, otherwise it'll be way too slow...
			mat4_basic newMat;

			#define SWAP_ROWS(a, b) { T *_tmp = a; (a)=(b); (b)=_tmp; }
			#define MAT(m,r,c) (m)[(c)*4+(r)]

			T *m = (T*) &t[0].m[0];
			T *out = &newMat.t[0].m[0];

			T wtmp[4][8];
			T m0, m1, m2, m3, s;
			T *r0, *r1, *r2, *r3;
			r0 = wtmp[0], r1 = wtmp[1], r2 = wtmp[2], r3 = wtmp[3];
			r0[0] = MAT(m, 0, 0), r0[1] = MAT(m, 0, 1),
			r0[2] = MAT(m, 0, 2), r0[3] = MAT(m, 0, 3),
			r0[4] = 1.0, r0[5] = r0[6] = r0[7] = 0.0,
			r1[0] = MAT(m, 1, 0), r1[1] = MAT(m, 1, 1),
			r1[2] = MAT(m, 1, 2), r1[3] = MAT(m, 1, 3),
			r1[5] = 1.0, r1[4] = r1[6] = r1[7] = 0.0,
			r2[0] = MAT(m, 2, 0), r2[1] = MAT(m, 2, 1),
			r2[2] = MAT(m, 2, 2), r2[3] = MAT(m, 2, 3),
			r2[6] = 1.0, r2[4] = r2[5] = r2[7] = 0.0,
			r3[0] = MAT(m, 3, 0), r3[1] = MAT(m, 3, 1),
			r3[2] = MAT(m, 3, 2), r3[3] = MAT(m, 3, 3),
			r3[7] = 1.0, r3[4] = r3[5] = r3[6] = 0.0;
			/* choose pivot - or die */
			if (fabsf(r3[0]) > fabsf(r2[0]))
				SWAP_ROWS(r3, r2);
			if (fabsf(r2[0]) > fabsf(r1[0]))
				SWAP_ROWS(r2, r1);
			if (fabsf(r1[0]) > fabsf(r0[0]))
				SWAP_ROWS(r1, r0);
			if (0.0 == r0[0])
				return *this;
			/* eliminate first variable     */
			m1 = r1[0] / r0[0];
			m2 = r2[0] / r0[0];
			m3 = r3[0] / r0[0];
			s = r0[1];
			r1[1] -= m1 * s;
			r2[1] -= m2 * s;
			r3[1] -= m3 * s;
			s = r0[2];
			r1[2] -= m1 * s;
			r2[2] -= m2 * s;
			r3[2] -= m3 * s;
			s = r0[3];
			r1[3] -= m1 * s;
			r2[3] -= m2 * s;
			r3[3] -= m3 * s;
			s = r0[4];
			if (s != 0.0) {
				r1[4] -= m1 * s;
				r2[4] -= m2 * s;
				r3[4] -= m3 * s;
			}
			s = r0[5];
			if (s != 0.0) {
				r1[5] -= m1 * s;
				r2[5] -= m2 * s;
				r3[5] -= m3 * s;
			}
			s = r0[6];
			if (s != 0.0) {
				r1[6] -= m1 * s;
				r2[6] -= m2 * s;
				r3[6] -= m3 * s;
			}
			s = r0[7];
			if (s != 0.0) {
				r1[7] -= m1 * s;
				r2[7] -= m2 * s;
				r3[7] -= m3 * s;
			}
			/* choose pivot - or die */
			if (fabsf(r3[1]) > fabsf(r2[1]))
				SWAP_ROWS(r3, r2);
			if (fabsf(r2[1]) > fabsf(r1[1]))
				SWAP_ROWS(r2, r1);
			if (0.0 == r1[1])
				return *this;
			/* eliminate second variable */
			m2 = r2[1] / r1[1];
			m3 = r3[1] / r1[1];
			r2[2] -= m2 * r1[2];
			r3[2] -= m3 * r1[2];
			r2[3] -= m2 * r1[3];
			r3[3] -= m3 * r1[3];
			s = r1[4];
			if (0.0 != s) {
				r2[4] -= m2 * s;
				r3[4] -= m3 * s;
			}
			s = r1[5];
			if (0.0 != s) {
				r2[5] -= m2 * s;
				r3[5] -= m3 * s;
			}
			s = r1[6];
			if (0.0 != s) {
				r2[6] -= m2 * s;
				r3[6] -= m3 * s;
			}
			s = r1[7];
			if (0.0 != s) {
				r2[7] -= m2 * s;
				r3[7] -= m3 * s;
			}
			/* choose pivot - or die */
			if (fabsf(r3[2]) > fabsf(r2[2]))
				SWAP_ROWS(r3, r2);
			if (0.0 == r2[2])
				return *this;
			/* eliminate third variable */
			m3 = r3[2] / r2[2];
			r3[3] -= m3 * r2[3], r3[4] -= m3 * r2[4],
			r3[5] -= m3 * r2[5], r3[6] -= m3 * r2[6], r3[7] -= m3 * r2[7];
			/* last check */
			if (0.0 == r3[3])
				return *this;
			s = (T) 1.0 / r3[3];             /* now back substitute row 3 */
			r3[4] *= s;
			r3[5] *= s;
			r3[6] *= s;
			r3[7] *= s;
			m2 = r2[3];                  /* now back substitute row 2 */
			s = (T) 1.0 / r2[2];
			r2[4] = s * (r2[4] - r3[4] * m2), r2[5] = s * (r2[5] - r3[5] * m2),
			r2[6] = s * (r2[6] - r3[6] * m2), r2[7] = s * (r2[7] - r3[7] * m2);
			m1 = r1[3];
			r1[4] -= r3[4] * m1, r1[5] -= r3[5] * m1,
			r1[6] -= r3[6] * m1, r1[7] -= r3[7] * m1;
			m0 = r0[3];
			r0[4] -= r3[4] * m0, r0[5] -= r3[5] * m0,
			r0[6] -= r3[6] * m0, r0[7] -= r3[7] * m0;
			m1 = r1[2];                  /* now back substitute row 1 */
			s = (T) 1.0 / r1[1];
			r1[4] = s * (r1[4] - r2[4] * m1), r1[5] = s * (r1[5] - r2[5] * m1),
			r1[6] = s * (r1[6] - r2[6] * m1), r1[7] = s * (r1[7] - r2[7] * m1);
			m0 = r0[2];
			r0[4] -= r2[4] * m0, r0[5] -= r2[5] * m0,
			r0[6] -= r2[6] * m0, r0[7] -= r2[7] * m0;
			m0 = r0[1];                  /* now back substitute row 0 */
			s = (T) 1.0 / r0[0];
			r0[4] = s * (r0[4] - r1[4] * m0), r0[5] = s * (r0[5] - r1[5] * m0),
			r0[6] = s * (r0[6] - r1[6] * m0), r0[7] = s * (r0[7] - r1[7] * m0);
			MAT(out, 0, 0) = r0[4];
			MAT(out, 0, 1) = r0[5], MAT(out, 0, 2) = r0[6];
			MAT(out, 0, 3) = r0[7], MAT(out, 1, 0) = r1[4];
			MAT(out, 1, 1) = r1[5], MAT(out, 1, 2) = r1[6];
			MAT(out, 1, 3) = r1[7], MAT(out, 2, 0) = r2[4];
			MAT(out, 2, 1) = r2[5], MAT(out, 2, 2) = r2[6];
			MAT(out, 2, 3) = r2[7], MAT(out, 3, 0) = r3[4];
			MAT(out, 3, 1) = r3[5], MAT(out, 3, 2) = r3[6];
			MAT(out, 3, 3) = r3[7];
			return newMat;

			#undef MAT
			#undef SWAP_ROWS
		}

		mat2_basic<T> getMat2() const
		{
			mat2_basic<T> mat;
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 2; j++)
					mat.t[i].m[j] = t[i].m[j];
			return mat;
		}

		mat3_basic<T> getMat3() const
		{
			mat3_basic<T> mat;
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					mat.t[i].m[j] = t[i].m[j];
			return mat;
		}

		bool operator == (const mat4_basic &m) const
		{
			for (int i = 0; i < 4; i++)
				if (m.t[i] != t[i])
					return false;
			return true;
		}

		bool operator != (const mat4_basic &m) const
		{
			return !(*this == m);
		}

		mat4_basic &translate(T x, T y, T z)
		{
			*this *= mat4_basic<T>(	1, 0, 0, 0,
									0, 1, 0, 0,
									0, 0, 1, 0,
									x, y, z, 1);
			return *this;
		}

		mat4_basic &translate(const vec3_basic<T> &v)
		{
			return translate(v.x, v.y, v.z);
		}

		mat4_basic &scale(T x, T y, T z)
		{
			*this *= mat4_basic<T>(	x, 0, 0, 0,
									0, y, 0, 0,
									0, 0, z, 0,
									0, 0, 0, 1);
			return *this;
		}

		mat4_basic &scale(const vec3_basic<T> &v)
		{
			return scale(v.x, v.y, v.z);
		}

		mat4_basic &rotateX(T angle)
		{
			*this *= mat4_basic<T>(	1, 0, 0, 0,
									0, cos(angle), sin(angle), 0,
									0, -sin(angle), cos(angle), 0,
									0, 0, 0, 1);
			return *this;
		}

		mat4_basic &rotateY(T angle)
		{
			*this *= mat4_basic<T>(	cos(angle), 0, -sin(angle), 0,
									0, 1, 0, 0,
									sin(angle), 0, cos(angle), 0,
									0, 0, 0, 1);
			return *this;
		}

		mat4_basic &rotateZ(T angle)
		{
			*this *= mat4_basic<T>(	cos(angle), sin(angle), 0, 0,
									-sin(angle), cos(angle), 0, 0,
									0, 0, 1, 0,
									0, 0, 0, 1);
			return *this;
		}

		operator T *() { return (T*) t; }
		operator const T *() const { return (T*) t; }
	};

	template <typename T>
	mat4_basic<T> getBias()
	{
		return mat4_basic<T>(	0.5, 0, 0, 0,
								0, 0.5, 0, 0,
								0, 0, 0.5, 0,
								0.5, 0.5, 0.5, 1);
	}

	template <typename T>
	mat4_basic<T> getIdentity()
	{
		return mat4_basic<T>(	1, 0, 0, 0,
								0, 1, 0, 0,
								0, 0, 1, 0,
								0, 0, 0, 1);
	}

	template <typename T>
	mat4_basic<T> getTranslate(T x, T y, T z)
	{
		return mat4_basic<T>(	1, 0, 0, 0,
								0, 1, 0, 0,
								0, 0, 1, 0,
								x, y, z, 1);
	}

	template <typename T>
	mat4_basic<T> getTranslate(const vec3_basic<T> &v)
	{
		return getTranslate(v.x, v.y, v.z);
	}

	template <typename T>
	mat4_basic<T> getScale(T x, T y, T z)
	{
		return mat4_basic<T>(	x, 0, 0, 0,
								0, y, 0, 0,
								0, 0, z, 0,
								0, 0, 0, 1);
	}

	template <typename T>
	mat4_basic<T> getScale(const vec3_basic<T> &v)
	{
		return getScale(v.x, v.y, v.z);
	}

	template <typename T>
	mat4_basic<T> getRotateX(T angle)
	{
		return mat4_basic<T>(	1, 0, 0, 0,
								0, cos(angle), sin(angle), 0,
								0, -sin(angle), cos(angle), 0,
								0, 0, 0, 1);
	}

	template <typename T>
	mat4_basic<T> getRotateY(T angle)
	{
		return mat4_basic<T>(	cos(angle), 0, -sin(angle), 0,
								0, 1, 0, 0,
								sin(angle), 0, cos(angle), 0,
								0, 0, 0, 1);
	}

	template <typename T>
	mat4_basic<T> getRotateZ(T angle)
	{
		return mat4_basic<T>(cos(angle), sin(angle), 0, 0,
							-sin(angle), cos(angle), 0, 0,
							0, 0, 1, 0,
							0, 0, 0, 1);
	}
	
	template <typename T, typename U, typename V, typename W, typename X, typename Y>
	mat4_basic<T> getOrtho(T left, U right, V bottom, W top, X zNear, Y zFar)
	{
		T rl = (T) right - (T) left;
		T tb = (T) top - (T) bottom;
		T fn = (T) zFar - (T) zNear;

		T tx = ((T) right + (T) left) / (T) rl;
		T ty = ((T) top + (T) bottom) / (T) tb;
		T tz = ((T) zFar + (T) zNear) / (T) fn;

		return mat4_basic<T>(	(T) 2.0 / rl, 0, 0, 0,
								0, (T) 2.0 / tb, 0, 0,
								0, 0, (T) -2.0 / fn, 0,
								-tx, -ty, -tz, 1);
	}
	
	template <typename T, typename U, typename V, typename W>
	mat4_basic<T> getPerspective(T fovy, U aspect, V zNear, W zFar)
	{
		T f = (T) 1.0 / tan((T) fovy / (T) 2.0);

		return mat4_basic<T>(	(T) f / (T) aspect, 0, 0, 0,
								0, f, 0, 0,
								0, 0, ((T) zFar + (T) zNear) / ((T) zNear - (T) zFar), -1,
								0, 0, ((T) 2.0 * (T) zFar * (T) zNear) / ((T) zNear - (T) zFar), 0);
	}
	
	template <typename T, typename U, typename V, typename W>
	mat4_basic<T> getDxPerspective(T fovy, U aspect, V zNear, W zFar) // TODO: test me!
	{
		T f = (T) 1.0 / tan((T) fovy / (T) 2.0);

		return mat4_basic<T>(	(T) f / (T) aspect, 0, 0, 0,
								0, f, 0, 0,
								0, 0, ((T) zFar + (T) zNear) / ((T) zFar - (T) zNear), -1,
								0, 0, ((T) 2.0 * (T) zFar * (T) -zNear) / ((T) zFar - (T) zNear), 0);
	}
	
	template <typename T>
	vec3_basic<T> project(const vec3_basic<T> &pos, const mat4_basic<T> &model, const mat4_basic<T> &proj, const ivec4 &view)
	{
		mat4_basic<T> m = model * proj;
		vec4_basic<T> v = m * vec4_basic<T>(pos, 1);

		v.x = view.m[0] + (view.m[2] * (v.x / v.w + 1)) / (T) 2.0;
		v.y = view.m[1] + (view.m[3] * (v.y / v.w + 1)) / (T) 2.0;
		v.z = (v.z / v.w + 1) / (T) 2.0;

		return vec3_basic<T>(v.x, v.y, v.z);
	}
	
	template <typename T>
	vec3_basic<T> unProject(const vec3_basic<T> &pos, const mat4_basic<T> &model, const mat4_basic<T> &proj, const ivec4 &view)
	{
		vec4_basic<T> v, v1;

		v.x = (((T) 2.0 * (pos.x - view.m[0])) / view.m[2]) - 1;
		v.y = (((T) 2.0 * (pos.y - view.m[1])) / view.m[3]) - 1;
		v.z = (T) 2.0 * pos.z - 1;
		v.w = 1;

		mat4_basic<T> m = (model * proj).invert();

		v = m * v;
		v.w = (T) 1.0 / v.w;

		vec3_basic<T> tmp(v.x * v.w, v.y * v.w, v.z * v.w);
		return tmp;
	}
	
	template <typename T>
	vec3_basic<T> unProjectZ(T x, T y, const vec3_basic<T> &pos, const mat4_basic<T> &model, const mat4_basic<T> &proj, const ivec4 &view)
	{
		vec3_basic<T> newPos = project(pos, model, proj, view);
		newPos = unProject(vec3_basic<T>(x, y, newPos.z), model, proj, view);
		return newPos;
	}

	typedef mat4_basic<float> mat4;
	typedef mat4_basic<double> dmat4;
	typedef mat4_basic<int> imat4;
	typedef mat4_basic<unsigned int> umat4;
}

template <typename T>
std::ostream &operator << (std::ostream &out, Math::mat4_basic<T> v)
{
	out << v.t[0] << "\n" << v.t[1] << "\n" << v.t[2] << "\n" << v.t[3];
	return out;
}

