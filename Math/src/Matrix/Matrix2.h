#pragma once

#include "../Vector/Vec2.h"

namespace Math
{
	template <typename T>
	class mat2_basic
	{
	public:
		vec2_basic<T> t[2];

	public:
		mat2_basic(const mat2_basic &m)
		{
			for (int i = 0; i < 2; i++)
				t[i] = m.t[i];
		}

		mat2_basic(T *t)
		{
			int k = 0;
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 2; j++)
					this->t[i].m[j] = t[k++];
		}

		mat2_basic(T t1 = 0, T t2 = 0, T t3 = 0, T t4 = 0)
		{
			t[0].m[0] = t1;
			t[0].m[1] = t2;
			t[1].m[0] = t3;
			t[1].m[1] = t4;
		}

		mat2_basic(const vec2_basic<T> &v1, const vec2_basic<T> &v2)
		{
			t[0] = v1;
			t[1] = v2;
		}

		mat2_basic &operator = (const mat2_basic &m)
		{
			for (int i = 0; i < 2; i++)
				t[i] = m.t[i];
			return *this;
		}

		mat2_basic operator * (const mat2_basic &m1) const
		{
			mat2_basic m;
			for (int i = 0; i < 2; i++)
			{
				auto tVec = vec2_basic<T>();
				for (int j = 0; j < 2; j++)
					tVec[j] = m1.t[j].m[i];

				for (int j = 0; j < 2; j++)
					m.t[j].m[i] = dot(t[j], tVec);
			}
			return m;
		}

		vec2_basic<T> operator * (const vec2_basic<T> &v) const
		{
			vec2_basic<T> v1, v2;
			for (int i = 0; i < 2; i++)
			{
				for (int j = 0; j < 2; j++)
					v2.m[j] = t[j].m[i];
				v1.m[i] = dot(v2, v);
			}
			return v1;
		}

		mat2_basic operator * (T u) const
		{
			mat2_basic m;
			for (int i = 0; i < 2; i++)
				m.t[i] = t[i] * u;
			return m;
		}

		mat2_basic &operator *= (const mat2_basic &m1)
		{
			mat2_basic m = *this * m1;
			*this = m;
			return *this;
		}

		mat2_basic &operator *= (const vec2_basic<T> &v)
		{
			mat2_basic m = *this * v;
			*this = m;
			return *this;
		}

		mat2_basic &operator *= (T u)
		{
			for (int i = 0; i < 2; i++)
				t[i] *= u;
			return *this;
		}

		mat2_basic operator + (const mat2_basic &m1) const
		{
			mat2_basic m;
			for (int i = 0; i < 2; i++)
				m.t[i] = t[i] + m1.t[i];
			return m;
		}

		mat2_basic operator + (T u) const
		{
			mat2_basic m;
			for (int i = 0; i < 2; i++)
				m.t[i] = t[i] + u;
			return m;
		}

		mat2_basic &operator += (const mat2_basic &m1)
		{
			for (int i = 0; i < 2; i++)
				t[i] += m1.t[i];
			return *this;
		}

		mat2_basic &operator += (T u)
		{
			for (int i = 0; i < 2; i++)
				t[i] += u;
			return *this;
		}

		mat2_basic operator - (const mat2_basic &m1) const
		{
			mat2_basic m;
			for (int i = 0; i < 2; i++)
				m.t[i] = t[i] - m1.t[i];
			return m;
		}

		mat2_basic operator - (T u) const
		{
			mat2_basic m;
			for (int i = 0; i < 2; i++)
				m.t[i] = t[i] - u;
			return m;
		}

		mat2_basic &operator -= (const mat2_basic &m1)
		{
			for (int i = 0; i < 2; i++)
				t[i] -= m1.t[i];
			return *this;
		}

		mat2_basic &operator -= (T u)
		{
			for (int i = 0; i < 2; i++)
				t[i] -= u;
			return *this;
		}

		mat2_basic &identity()
		{
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 2; j++)
					if (i == j)
						t[i].m[j] = 1;
					else
						t[i].m[j] = 0;
			return *this;
		}

		static mat2_basic getIdentity()
		{
			mat2_basic m;
			m.identity();
			return m;
		}

		mat2_basic &transpose()
		{
			mat2_basic m;
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 2; j++)
					m.t[j].m[i] = t[i].m[j];
			*this = m;
		}

		mat2_basic getTranspose() const
		{
			mat2_basic m = *this;
			m.transpose();
			return m;
		}

		T det() const
		{
			return t[0].m[0] * t[1].m[1] - t[1].m[0] * t[0].m[1];
		}

		mat2_basic detReciprocal() const
		{
			return mat2_basic(t[1].m[1], -t[0].m[1], -t[1].m[0], t[0].m[0]);
		}

		mat2_basic &invert()
		{
			*this = getInverse();
			return *this;
		}

		mat2_basic getInverse() const
		{
			T d = det();
			if (d == 0)
				return *this;
			return detReciprocal() * ((T) 1.0 / d);
		}

		bool operator == (const mat2_basic &m) const
		{
			for (int i = 0; i < 2; i++)
				if (m.t[i] != t[i])
					return false;
			return true;
		}

		bool operator != (const mat2_basic &m) const
		{
			return !(*this == m);
		}

		operator T *() { return (T*) t; }
		operator const T *() const { return (T*) t; }
	};

	typedef mat2_basic<float> mat2;
	typedef mat2_basic<double> dmat2;
	typedef mat2_basic<int> imat2;
	typedef mat2_basic<unsigned int> umat2;
}

template <typename T>
std::ostream &operator << (std::ostream &out, Math::mat2_basic<T> v)
{
	out << v.t[0] << "\n" << v.t[1];
	return out;
}

