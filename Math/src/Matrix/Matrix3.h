#pragma once

#include "../Vector/Vec3.h"
#include "Matrix2.h"

namespace Math
{
	template <typename T>
	class mat3_basic
	{
	public:
		vec3_basic<T> t[3];

	public:
		mat3_basic(const mat3_basic &m)
		{
			for (int i = 0; i < 3; i++)
				t[i] = m.t[i];
		}

		mat3_basic(T *t)
		{
			int k = 0;
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					this->t[i].m[j] = t[k++];
		}

		mat3_basic(T t1 = 0, T t2 = 0, T t3 = 0, T t4 = 0, T t5 = 0, T t6 = 0, T t7 = 0, T t8 = 0, T t9 = 0)
		{
			t[0].m[0] = t1; t[0].m[1] = t2; t[0].m[2] = t3;
			t[1].m[0] = t4; t[1].m[1] = t5; t[1].m[2] = t6;
			t[2].m[0] = t7; t[2].m[1] = t8; t[2].m[2] = t9;
		}

		mat3_basic(const vec3_basic<T> &v1, const vec3_basic<T> &v2, const vec3_basic<T> &v3)
		{
			t[0] = v1;
			t[1] = v2;
			t[2] = v3;
		}

		mat3_basic(const mat2_basic<T> &m)
		{
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 2; j++)
					t[i].m[j] = m.t[i].m[j];
		}

		mat3_basic &operator = (const mat3_basic &m)
		{
			for (int i = 0; i < 3; i++)
				t[i] = m.t[i];
			return *this;
		}

		mat3_basic operator * (const mat3_basic &m1) const
		{
			mat3_basic m;
			for (int i = 0; i < 3; i++)
			{
				auto tVec = vec3_basic<T>();
				for (int j = 0; j < 3; j++)
					tVec[j] = m1.t[j].m[i];

				for (int j = 0; j < 3; j++)
					m.t[j].m[i] = dot(t[j], tVec);
			}
			return m;
		}

		vec3_basic<T> operator * (const vec3_basic<T> &v) const
		{
			vec3_basic<T> v1, v2;
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 3; j++)
					v2.m[j] = t[j].m[i];
				v1.m[i] = dot(v2, v);
			}
			return v1;
		}

		mat3_basic operator * (T u) const
		{
			mat3_basic m;
			for (int i = 0; i < 3; i++)
				m.t[i] = t[i] * u;
			return m;
		}

		mat3_basic &operator *= (const mat3_basic &m1)
		{
			mat3_basic m = *this * m1;
			*this = m;
			return *this;
		}

		mat3_basic &operator *= (const vec3_basic<T> &v)
		{
			mat3_basic m = *this * v;
			*this = m;
			return *this;
		}

		mat3_basic &operator *= (T u)
		{
			for (int i = 0; i < 3; i++)
				t[i] *= u;
			return *this;
		}

		mat3_basic operator + (const mat3_basic &m1) const
		{
			mat3_basic m;
			for (int i = 0; i < 3; i++)
				m.t[i] = t[i] + m1.t[i];
			return m;
		}

		mat3_basic operator + (T u) const
		{
			mat3_basic m;
			for (int i = 0; i < 3; i++)
				m.t[i] = t[i] + u;
			return m;
		}

		mat3_basic &operator += (const mat3_basic &m1)
		{
			for (int i = 0; i < 3; i++)
				t[i] += m1.t[i];
			return *this;
		}

		mat3_basic &operator += (T u)
		{
			for (int i = 0; i < 3; i++)
				t[i] += u;
			return *this;
		}

		mat3_basic operator - (const mat3_basic &m1) const
		{
			mat3_basic m;
			for (int i = 0; i < 3; i++)
				m.t[i] = t[i] - m1.t[i];
			return m;
		}

		mat3_basic operator - (T u) const
		{
			mat3_basic m;
			for (int i = 0; i < 3; i++)
				m.t[i] = t[i] - u;
			return m;
		}

		mat3_basic &operator -= (const mat3_basic &m1)
		{
			for (int i = 0; i < 3; i++)
				t[i] -= m1.t[i];
			return *this;
		}

		mat3_basic &operator -= (T u)
		{
			for (int i = 0; i < 3; i++)
				t[i] -= u;
			return *this;
		}

		mat3_basic &identity()
		{
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					if (i == j)
						t[i].m[j] = 1;
					else
						t[i].m[j] = 0;
			return *this;
		}

		static mat3_basic getIdentity()
		{
			mat3_basic m;
			m.identity();
			return m;
		}

		mat3_basic &transpose()
		{
			mat3_basic m;
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 3; j++)
					m.t[j].m[i] = t[i].m[j];
			*this = m;
			return *this;
		}

		mat3_basic getTranspose() const
		{
			mat3_basic m = *this;
			m.transpose();
			return m;
		}

		T det() const
		{
			return t[0].m[0] * t[1].m[1] * t[2].m[2] + t[1].m[0] * t[2].m[1] * t[0].m[2] + t[2].m[0] * t[0].m[1] * t[1].m[2] -
				t[2].m[0] * t[1].m[1] * t[0].m[2] - t[1].m[0] * t[0].m[1] * t[2].m[2] - t[0].m[0] * t[2].m[1] * t[1].m[2];
		}

		mat3_basic detReciprocal() const
		{
			return mat3_basic(t[1].m[1] * t[2].m[2] - t[2].m[1] * t[1].m[2], t[2].m[1] * t[0].m[2] - t[0].m[1] * t[2].m[2], t[0].m[1] * t[1].m[2] - t[1].m[1] * t[0].m[2],
						t[2].m[0] * t[1].m[2] - t[1].m[0] * t[2].m[2], t[0].m[0] * t[2].m[2] - t[2].m[0] * t[0].m[2], t[0].m[2] * t[1].m[0] - t[0].m[0] * t[1].m[2],
						t[1].m[0] * t[2].m[1] - t[2].m[0] * t[1].m[1], t[2].m[0] * t[0].m[1] - t[0].m[0] * t[2].m[1], t[0].m[0] * t[1].m[1] - t[1].m[0] * t[0].m[1]);
		}

		mat3_basic &invert()
		{
			*this = getInverse();
			return *this;
		}

		mat3_basic getInverse() const
		{
			T d = det();
			if (d == 0)
				return *this;
			return detReciprocal() * ((T) 1.0 / d);
		}

		mat2_basic<T> getMat2() const
		{
			mat2_basic<T> mat;
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 2; j++)
					mat.t[i].m[j] = t[i].m[j];
			return mat;
		}

		bool operator == (const mat3_basic &m) const
		{
			for (int i = 0; i < 3; i++)
				if (m.t[i] != t[i])
					return false;
			return true;
		}

		bool operator != (const mat3_basic &m) const
		{
			return !(*this == m);
		}

		operator T *() { return (T*) t; }
		operator const T *() const { return (T*) t; }
	};

	typedef mat3_basic<float> mat3;
	typedef mat3_basic<double> dmat3;
	typedef mat3_basic<int> imat3;
	typedef mat3_basic<unsigned int> umat3;
}

template <typename T>
std::ostream &operator << (std::ostream &out, Math::mat3_basic<T> v)
{
	out << v.t[0] << "\n" << v.t[1] << "\n" << v.t[2];
	return out;
}

