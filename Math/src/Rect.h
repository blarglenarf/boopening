#pragma once

#include "Vector/VectorCommon.h"

namespace Math
{
	// TODO: use unions/swizzling, similar to how the vector classes work
	template <typename T>
	class Rect
	{
	private:
		vec2_basic<T> bL;
		vec2_basic<T> tR;

	public:
		Rect() {}

		Rect(vec2_basic<T> bottomLeft, vec2_basic<T> topRight) : bL(bottomLeft), tR(topRight) {}

		Rect(T left, T bottom, T right, T top)
		{
			bL.x = left;
			bL.y = bottom;
			tR.x = right;
			tR.y = top;
		}

		const vec2_basic<T> &bottomLeft() const { return bL; }
		vec2_basic<T> &bottomLeft() { return bL; }

		vec2_basic<T> topLeft() const { return vec2_basic<T>(left(), top()); }

		vec2_basic<T> bottomRight() const { return vec2_basic<T>(right(), bottom()); }

		const vec2_basic<T> &topRight() const { return tR; }
		vec2_basic<T> &topRight() { return tR; }

		vec2_basic<T> middle() const { return vec2_basic<T>(left() + (width() / (T) 2), bottom() + (height() / (T) 2)); }

		const T &bottom() const { return bL.y; }
		T &bottom() { return bL.y; }

		const T &left() const { return bL.x; }
		T &left() { return bL.x; }

		const T &top() const { return tR.y; }
		T &top() { return tR.y; }

		const T &right() const { return tR.x; }
		T &right() { return tR.x; }

		T width() const { return tR.x - bL.x; }
		T height() const { return tR.y - bL.y; }

		bool intersects(const vec2_basic<T> &pos) const { return intersects(pos.x, pos.y); }
		bool intersects(T x, T y) const { return x >= left() && x <= right() && y >= bottom() && y <= top(); }
		bool intersects(const Rect<T> &rect) const
		{
			return intersects(rect.bottomLeft()) || intersects(rect.topRight()) || intersects(rect.topLeft()) || intersects(rect.bottomRight()) ||
					rect.intersects(bottomLeft()) || rect.intersects(topRight()) || rect.intersects(topLeft()) || rect.intersects(bottomRight());
		}

		bool isInside(const Rect<T> &rect) const
		{
			return intersects(rect.bottomLeft()) && intersects(rect.topRight());
		}

		bool isOnBorder(const vec2_basic<T> &pos, float range) const { return isOnBorder(pos.x, pos.y, range); }
		bool isOnBorder(T x, T y, float range) const
		{
			bool xLeft = x >= left() - range && x <= left() + range && y >= bottom() - range && y <= top() + range;
			bool xRight = x >= right() - range && x <= right() + range && y >= bottom() - range && y <= top() + range;
			bool yBot = y >= bottom() - range && y <= bottom() + range && x >= left() - range && x <= right() + range;
			bool yTop = y >= top() - range && y <= top() + range && x >= left() - range && x <= right() + range;

			return xLeft || xRight || yBot || yTop;
		}
	};

	typedef Rect<float> RectF;
	typedef Rect<int> RectI;
	typedef Rect<double> RectD;
}

template <typename T>
std::ostream &operator << (std::ostream &out, Math::Rect<T> rect)
{
	out << "Left: " << rect.left() << ", Right: " << rect.right() << ", Top: " << rect.top() << ", Bottom: " << rect.bottom() << ", Width: " << rect.width() << ", Height: " << rect.height();
	return out;
}

