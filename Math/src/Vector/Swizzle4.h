#pragma once

namespace Math
{
	template <typename T, typename V, size_t a, size_t b, size_t c, size_t d>
	class swizzle4
	{
	private:
		T m[4];

	public:
		V &operator = (const V &v)
		{
			m[a] = v.m[0];
			m[b] = v.m[1];
			m[c] = v.m[2];
			m[d] = v.m[3];
			return *((V*) this);
		}

		operator V() { return V(m[a], m[b], m[c], m[d]); }
		operator T *() const { return (T*) m; }

		swizzle4 operator + (const V &v1) const
		{
			swizzle4 v;
			v.m[0] = m[a] + v1.m[0];
			v.m[1] = m[b] + v1.m[1];
			v.m[2] = m[c] + v1.m[2];
			v.m[3] = m[d] + v1.m[3];
			return v;
		}

		swizzle4 operator + (T t) const
		{
			swizzle4 v;
			v.m[0] = m[a] + t;
			v.m[1] = m[b] + t;
			v.m[2] = m[c] + t;
			v.m[3] = m[d] + t;
			return v;
		}

		swizzle4 &operator += (const V &v1)
		{
			m[a] += v1.m[0];
			m[b] += v1.m[1];
			m[c] += v1.m[2];
			m[d] += v1.m[3];
			return *this;
		}

		swizzle4 &operator += (T t)
		{
			m[a] += t;
			m[b] += t;
			m[c] += t;
			m[d] += t;
			return *this;
		}

		swizzle4 &operator - ()
		{
			m[a] = -m[0];
			m[b] = -m[1];
			m[c] = -m[2];
			m[d] = -m[3];
			return *this;
		}

		swizzle4 operator - (const V &v1) const
		{
			swizzle4 v;
			v.m[0] = m[a] - v1.m[0];
			v.m[1] = m[b] - v1.m[1];
			v.m[2] = m[c] - v1.m[2];
			v.m[3] = m[d] - v1.m[3];
			return v;
		}

		swizzle4 operator - (T t) const
		{
			swizzle4 v;
			v.m[0] = m[a] - t;
			v.m[1] = m[b] - t;
			v.m[2] = m[c] - t;
			v.m[3] = m[d] - t;
			return v;
		}

		swizzle4 &operator -= (const V &v1)
		{
			m[a] -= v1.m[0];
			m[b] -= v1.m[1];
			m[c] -= v1.m[2];
			m[d] -= v1.m[3];
			return *this;
		}

		swizzle4 &operator -= (T t)
		{
			m[a] -= t;
			m[b] -= t;
			m[c] -= t;
			m[d] -= t;
			return *this;
		}

		swizzle4 operator * (const V &v1) const
		{
			swizzle4 v;
			v.m[0] = m[a] * v1.m[0];
			v.m[1] = m[b] * v1.m[1];
			v.m[2] = m[c] * v1.m[2];
			v.m[3] = m[d] * v1.m[3];
			return v;
		}

		swizzle4 operator * (T t) const
		{
			swizzle4 v;
			v.m[0] = m[a] * t;
			v.m[1] = m[b] * t;
			v.m[2] = m[c] * t;
			v.m[3] = m[d] * t;
			return v;
		}

		swizzle4 &operator *= (const V &v1)
		{
			m[a] *= v1.m[0];
			m[b] *= v1.m[1];
			m[c] *= v1.m[2];
			m[d] *= v1.m[3];
			return *this;
		}

		swizzle4 &operator *= (T t)
		{
			m[a] *= t;
			m[b] *= t;
			m[c] *= t;
			m[d] *= t;
			return *this;
		}

		swizzle4 operator / (const V &v1) const
		{
			swizzle4 v;
			v.m[0] = m[a] / v1.m[0];
			v.m[1] = m[b] / v1.m[1];
			v.m[2] = m[c] / v1.m[2];
			v.m[3] = m[d] / v1.m[3];
			return v;
		}

		swizzle4 operator / (T t) const
		{
			swizzle4 v;
			v.m[0] = m[a] / t;
			v.m[1] = m[b] / t;
			v.m[2] = m[c] / t;
			v.m[3] = m[d] / t;
			return v;
		}

		swizzle4 &operator /= (const V &v1)
		{
			m[a] /= v1.m[0];
			m[b] /= v1.m[1];
			m[c] /= v1.m[2];
			m[d] /= v1.m[3];
			return *this;
		}

		swizzle4 &operator /= (T t)
		{
			m[a] /= t;
			m[b] /= t;
			m[c] /= t;
			m[d] /= t;
			return *this;
		}

		bool operator == (const V &v1) const
		{
			return m[a] == v1.m[0] && m[b] == v1.m[1] && m[c] == v1.m[2] && m[d] == v1.m[3];
		}

		bool operator != (const V &v1) const
		{
			return !(*this == v1);
		}

		T dot(const V &v) const
		{
			T val = 0;
			val += m[a] * v.m[0];
			val += m[b] * v.m[1];
			val += m[c] * v.m[2];
			val += m[d] * v.m[3];
			return val;
		}

		T dot(const swizzle4 &v) const
		{
			T val = 0;
			val += m[a] * v.m[0];
			val += m[b] * v.m[1];
			val += m[c] * v.m[2];
			val += m[d] * v.m[3];
			return val;
		}

		T lenSq() const
		{
			return dot(*this);
		}

		T len() const
		{
			return sqrt(lenSq());
		}

		swizzle4 &toLen(T len)
		{
			return *this *= (len / this->len());
		}

		swizzle4 toLen(T len) const
		{
			return *this * (len / this->len());
		}

		swizzle4 getLen(T len) const
		{
			return *this * (len / this->len());
		}

		swizzle4 &normalize()
		{
			if (this->lenSq() == 0)
				return *this;
			*this /= len();
			return *this;
		}

		/*V normalize() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}*/

		/*swizzle4 unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}*/

		swizzle4 unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}

		T distanceSq(const V &v) const
		{
			return (*this - v).lenSq();
		}

		T distance(const V &v) const
		{
			return (*this - v).len();
		}

		swizzle4 &reflect(const V &normal)
		{
			*this = *this - (normal * ((T) 2.0 * dot(normal, *this)));
			return *this;
		}
	};
}

template <typename T, typename V, size_t a, size_t b, size_t c, size_t d>
std::ostream &operator << (std::ostream &out, Math::swizzle4<T, V, a, b, c, d> v)
{
	auto vec = V(v);
	out << vec.x << "," << vec.y << "," << vec.z << "," << vec.w;
	return out;
}

