#pragma once

#include "../FixedPoint.h"
#include "Swizzle3.h"

#include "Vec2.h"

namespace Math
{
	template <typename T>
	class vec3_basic
	{
	public:
		union
		{
			struct
			{
				T x;
				T y;
				T z;
			};
			struct
			{
				T width;
				T height;
				T depth;
			};
			struct
			{
				T s;
				T t;
				T p;
			};
			struct
			{
				T r;
				T g;
				T b;
			};
			struct
			{
				T u;
				T v;
			};
			struct
			{
				T m[3];
			};

			swizzle2<T, vec2_basic<T>, 0, 0> xx, rr, ss, uu;
			swizzle2<T, vec2_basic<T>, 0, 1> xy, rg, st, uv;
			swizzle2<T, vec2_basic<T>, 0, 2> xz, rb, sp;
			swizzle2<T, vec2_basic<T>, 1, 0> yx, gr, ts, vu;
			swizzle2<T, vec2_basic<T>, 1, 1> yy, gg, tt, vv;
			swizzle2<T, vec2_basic<T>, 1, 2> yz, gb, tp;
			swizzle2<T, vec2_basic<T>, 2, 0> zx, br, ps;
			swizzle2<T, vec2_basic<T>, 2, 1> zy, bg, pt;
			swizzle2<T, vec2_basic<T>, 2, 2> zz, bb, pp;

			swizzle3<T, vec3_basic, 0, 0, 0> xxx, rrr, sss;
			swizzle3<T, vec3_basic, 0, 0, 1> xxy, rrg, sst;
			swizzle3<T, vec3_basic, 0, 0, 2> xxz, rrb, ssp;
			swizzle3<T, vec3_basic, 0, 1, 0> xyx, rgr, sts;
			swizzle3<T, vec3_basic, 0, 1, 1> xyy, rgg, stt;
			swizzle3<T, vec3_basic, 0, 1, 2> xyz, rgb, stp;
			swizzle3<T, vec3_basic, 0, 2, 0> xzx, rbr, sps;
			swizzle3<T, vec3_basic, 0, 2, 1> xzy, rbg, spt;
			swizzle3<T, vec3_basic, 0, 2, 2> xzz, rbb, spp;
			swizzle3<T, vec3_basic, 1, 0, 0> yxx, grr, tss;
			swizzle3<T, vec3_basic, 1, 0, 1> yxy, grg, tst;
			swizzle3<T, vec3_basic, 1, 0, 2> yxz, grb, tsp;
			swizzle3<T, vec3_basic, 1, 1, 0> yyx, ggr, tts;
			swizzle3<T, vec3_basic, 1, 1, 1> yyy, ggg, ttt;
			swizzle3<T, vec3_basic, 1, 1, 2> yyz, ggb, ttp;
			swizzle3<T, vec3_basic, 1, 2, 0> yzx, gbr, tps;
			swizzle3<T, vec3_basic, 1, 2, 1> yzy, gbg, tpt;
			swizzle3<T, vec3_basic, 1, 2, 2> yzz, gbb, tpp;
			swizzle3<T, vec3_basic, 2, 0, 0> zxx, brr, pss;
			swizzle3<T, vec3_basic, 2, 0, 1> zxy, brg, pst;
			swizzle3<T, vec3_basic, 2, 0, 2> zxz, brb, psp;
			swizzle3<T, vec3_basic, 2, 1, 0> zyx, bgr, pts;
			swizzle3<T, vec3_basic, 2, 1, 1> zyy, bgg, ptt;
			swizzle3<T, vec3_basic, 2, 1, 2> zyz, bgb, ptp;
			swizzle3<T, vec3_basic, 2, 2, 0> zzx, bbr, pps;
			swizzle3<T, vec3_basic, 2, 2, 1> zzy, bbg, ppt;
			swizzle3<T, vec3_basic, 2, 2, 2> zzz, bbb, ppp;
		};

	public:
		vec3_basic() : x(0), y(0), z(0) {}

		template <typename A, typename B, typename C>
		vec3_basic(A x, B y, C z) : x((T) x), y((T) y), z((T) z) {}

		template <typename A>
		vec3_basic(A *t) : x((T) *t), y((T) *t), z((T) *t) {}

		template <typename A, typename B, typename C>
		vec3_basic(A *x, B *y, C *z) : x((T) *x), y((T) *y), z((T) *z) {}

		vec3_basic(const vec3_basic &v) : x(v.x), y(v.y), z(v.z) {}
		vec3_basic(const vec2_basic<T> &v) : x(v.x), y(v.y), z(0) {}
		vec3_basic(const vec2_basic<T> &v, T u) : x(v.x), y(v.y), z(u) {}
		vec3_basic(T u, const vec2_basic<T> &v) : x(u), y(v.x), z(v.y) {}

		vec3_basic &operator = (const vec3_basic &v)
		{
			for (int i = 0; i < 3; i++)
				m[i] = v.m[i];
			return *this;
		}

		vec3_basic operator + (const vec3_basic &v1) const
		{
			vec3_basic v;
			for (int i = 0; i < 3; i++)
				v.m[i] = m[i] + v1.m[i];
			return v;
		}

		vec3_basic operator + (T t) const
		{
			vec3_basic v;
			for (int i = 0; i < 3; i++)
				v.m[i] = m[i] + t;
			return v;
		}

		vec3_basic &operator += (const vec3_basic &v1)
		{
			for (int i = 0; i < 3; i++)
				m[i] += v1.m[i];
			return *this;
		}

		vec3_basic &operator += (T t)
		{
			for (int i = 0; i < 3; i++)
				m[i] += t;
			return *this;
		}

		vec3_basic &operator - ()
		{
			for (int i = 0; i < 3; i++)
				m[i] = -m[i];
			return *this;
		}

		vec3_basic operator - (const vec3_basic &v1) const
		{
			vec3_basic v;
			for (int i = 0; i < 3; i++)
				v.m[i] = m[i] - v1.m[i];
			return v;
		}

		vec3_basic operator - (T t) const
		{
			vec3_basic v;
			for (int i = 0; i < 3; i++)
				v.m[i] = m[i] - t;
			return v;
		}

		vec3_basic &operator -= (const vec3_basic &v1)
		{
			for (int i = 0; i < 3; i++)
				m[i] -= v1.m[i];
			return *this;
		}

		vec3_basic &operator -= (T t)
		{
			for (int i = 0; i < 3; i++)
				m[i] -= t;
			return *this;
		}

		vec3_basic operator * (const vec3_basic &v1) const
		{
			vec3_basic v;
			for (int i = 0; i < 3; i++)
				v.m[i] = m[i] * v1.m[i];
			return v;
		}

		vec3_basic operator * (T t) const
		{
			vec3_basic v;
			for (int i = 0; i < 3; i++)
				v.m[i] = m[i] * t;
			return v;
		}

		vec3_basic &operator *= (const vec3_basic &v1)
		{
			for (int i = 0; i < 3; i++)
				m[i] *= v1.m[i];
			return *this;
		}

		vec3_basic &operator *= (T t)
		{
			for (int i = 0; i < 3; i++)
				m[i] *= t;
			return *this;
		}

		vec3_basic operator / (const vec3_basic &v1) const
		{
			vec3_basic v;
			for (int i = 0; i < 3; i++)
				v.m[i] = m[i] / v1.m[i];
			return v;
		}

		vec3_basic operator / (T t) const
		{
			vec3_basic v;
			for (int i = 0; i < 3; i++)
				v.m[i] = m[i] / t;
			return v;
		}

		vec3_basic &operator /= (const vec3_basic &v1)
		{
			for (int i = 0; i < 3; i++)
				m[i] /= v1.m[i];
			return *this;
		}

		vec3_basic &operator /= (T t)
		{
			for (int i = 0; i < 3; i++)
				m[i] /= t;
			return *this;
		}

		bool operator == (const vec3_basic &v1) const
		{
			for (int i = 0; i < 3; i++)
				if (m[i] != v1.m[i])
					return false;
			return true;
		}

		bool operator != (const vec3_basic &v1) const
		{
			return !(*this == v1);
		}

		T dot(const vec3_basic &v) const
		{
			T val = 0;
			for (int i = 0; i < 3; i++)
				val += m[i] * v.m[i];
			return val;
		}

		T lenSq() const
		{
			return dot(*this);
		}

		T len() const
		{
			return sqrt(lenSq());
		}

		vec3_basic &toLen(T len)
		{
			return *this *= (len / this->len());
		}

		vec3_basic toLen(T len) const
		{
			return *this * (len / this->len());
		}

		vec3_basic getLen(T len) const
		{
			return *this * (len / this->len());
		}

		vec3_basic &normalize()
		{
			if (this->lenSq() == 0)
				return *this;
			*this /= len();
			return *this;
		}

		/*vec3_basic normalize() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}*/

		vec3_basic unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}

		T distanceSq(const vec3_basic &v) const
		{
			return (*this - v).lenSq();
		}

		T distance(const vec3_basic &v) const
		{
			return (*this - v).len();
		}

		vec3_basic &reflect(const vec3_basic &normal)
		{
			*this = *this - (normal * ((T) 2.0 * dot(normal)));
			return *this;
		}

		vec3_basic cross(const vec3_basic &v2) const
		{
			vec3_basic v;
			v.m[0] = m[1] * v2.m[2] - m[2] * v2.m[1];
			v.m[1] = m[2] * v2.m[0] - m[0] * v2.m[2];
			v.m[2] = m[0] * v2.m[1] - m[1] * v2.m[0];
			return v;
		}

		operator T *() { return (T*) m; }
		operator const T *() const { return (T*) m; }
	};

	template <typename T>
	vec3_basic<T> operator + (T t, const vec3_basic<T> &v1)
	{
		vec3_basic<T> v;
		for (int i = 0; i < 3; i++)
			v.m[i] = t + v1.m[i];
		return v;
	}
	
	template <typename T>
	vec3_basic<T> operator - (T t, const vec3_basic<T> &v1)
	{
		vec3_basic<T> v;
		for (int i = 0; i < 3; i++)
			v.m[i] = t - v1.m[i];
		return v;
	}
	
	template <typename T>
	vec3_basic<T> operator * (T t, const vec3_basic<T> &v1)
	{
		vec3_basic<T> v;
		for (int i = 0; i < 3; i++)
			v.m[i] = t * v1.m[i];
		return v;
	}
	
	template <typename T>
	vec3_basic<T> operator / (T t, const vec3_basic<T> &v1)
	{
		vec3_basic<T> v;
		for (int i = 0; i < 3; i++)
			v.m[i] = t / v1.m[i];
		return v;
	}

	template <typename T>
	T distanceSq(const vec3_basic<T> &v1, const vec3_basic<T> &v2)
	{
		return (v1 - v2).lenSq();
	}

	template <typename T>
	T distance(const vec3_basic<T> &v1, const vec3_basic<T> &v2)
	{
		return (v1 - v2).len();
	}

	template <typename T>
	T dot(const vec3_basic<T> &v1, const vec3_basic<T> &v2)
	{
		T val = 0;
		for (int i = 0; i < 3; i++)
			val += v1.m[i] * v2.m[i];
		return val;
	}

	template <typename T>
	vec3_basic<T> &reflect(const vec3_basic<T> &v1, const vec3_basic<T> &normal)
	{
		vec3_basic<T> v = v1 - (normal * ((T) 2.0 * dot(normal, v1)));
		return v;
	}

	template <typename T>
	vec3_basic<T> cross(const vec3_basic<T> &v1, const vec3_basic<T> &v2)
	{
		vec3_basic<T> v;
		v.m[0] = v1.m[1] * v2.m[2] - v1.m[2] * v2.m[1];
		v.m[1] = v1.m[2] * v2.m[0] - v1.m[0] * v2.m[2];
		v.m[2] = v1.m[0] * v2.m[1] - v1.m[1] * v2.m[0];
		return v;
	}

	typedef vec3_basic<float> vec3;
	typedef vec3_basic<double> dvec3;
	typedef vec3_basic<int> ivec3;
	typedef vec3_basic<unsigned int> uvec3;
	//typedef vec3_basic<fixed> fivec3;
}

template <typename T>
std::ostream &operator << (std::ostream &out, Math::vec3_basic<T> v)
{
	out << v.x << "," << v.y << "," << v.z;
	return out;
}

