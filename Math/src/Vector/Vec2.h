#pragma once

#include "../FixedPoint.h"
#include "Swizzle2.h"

#include <cmath>

namespace Math
{
	template <typename T>
	class vec2_basic
	{
	public:
		union
		{
			struct
			{
				T x;
				T y;
			};
			struct
			{
				T width;
				T height;
			};
			struct
			{
				T u;
				T v;
			};
			struct
			{
				T s;
				T t;
			};
			struct
			{
				T r;
				T g;
			};
			struct
			{
				T m[2];
			};

			swizzle2<T, vec2_basic, 0, 0> xx, uu, ss, rr;
			swizzle2<T, vec2_basic, 0, 1> xy, uv, st, rg;
			swizzle2<T, vec2_basic, 1, 0> yx, vu, ts, gr;
			swizzle2<T, vec2_basic, 1, 1> yy, vv, tt, gg;
		};

	public:
		vec2_basic() : x(0), y(0) {}

		template <typename A, typename B>
		vec2_basic(A x, B y = 0) : x((T) x), y((T) y) {}

		template <typename A>
		vec2_basic(A *t) : x((T) *t), y((T) *t) {}

		template <typename A, typename B>
		vec2_basic(A *x, B *y) : x((T) *x), y((T) *y) {}

		vec2_basic(const vec2_basic &v) : x(v.x), y(v.y) {}

		vec2_basic &operator = (const vec2_basic &v)
		{
			for (int i = 0; i < 2; i++)
				m[i] = v.m[i];
			return *this;
		}

		vec2_basic operator + (const vec2_basic &v1) const
		{
			vec2_basic v;
			for (int i = 0; i < 2; i++)
				v.m[i] = m[i] + v1.m[i];
			return v;
		}

		vec2_basic operator + (T t) const
		{
			vec2_basic v;
			for (int i = 0; i < 2; i++)
				v.m[i] = m[i] + t;
			return v;
		}

		vec2_basic &operator += (const vec2_basic &v1)
		{
			for (int i = 0; i < 2; i++)
				m[i] += v1.m[i];
			return *this;
		}

		vec2_basic &operator += (T t)
		{
			for (int i = 0; i < 2; i++)
				m[i] += t;
			return *this;
		}

		vec2_basic &operator - ()
		{
			for (int i = 0; i < 2; i++)
				m[i] = -m[i];
			return *this;
		}

		vec2_basic operator - (const vec2_basic &v1) const
		{
			vec2_basic v;
			for (int i = 0; i < 2; i++)
				v.m[i] = m[i] - v1.m[i];
			return v;
		}

		vec2_basic operator - (T t) const
		{
			vec2_basic v;
			for (int i = 0; i < 2; i++)
				v.m[i] = m[i] - t;
			return v;
		}

		vec2_basic &operator -= (const vec2_basic &v1)
		{
			for (int i = 0; i < 2; i++)
				m[i] -= v1.m[i];
			return *this;
		}

		vec2_basic &operator -= (T t)
		{
			for (int i = 0; i < 2; i++)
				m[i] -= t;
			return *this;
		}

		vec2_basic operator * (const vec2_basic &v1) const
		{
			vec2_basic v;
			for (int i = 0; i < 2; i++)
				v.m[i] = m[i] * v1.m[i];
			return v;
		}

		vec2_basic operator * (T t) const
		{
			vec2_basic v;
			for (int i = 0; i < 2; i++)
				v.m[i] = m[i] * t;
			return v;
		}

		vec2_basic &operator *= (const vec2_basic &v1)
		{
			for (int i = 0; i < 2; i++)
				m[i] *= v1.m[i];
			return *this;
		}

		vec2_basic &operator *= (T t)
		{
			for (int i = 0; i < 2; i++)
				m[i] *= t;
			return *this;
		}

		vec2_basic operator / (const vec2_basic &v1) const
		{
			vec2_basic v;
			for (int i = 0; i < 2; i++)
				v.m[i] = m[i] / v1.m[i];
			return v;
		}

		vec2_basic operator / (T t) const
		{
			vec2_basic v;
			for (int i = 0; i < 2; i++)
				v.m[i] = m[i] / t;
			return v;
		}

		vec2_basic &operator /= (const vec2_basic &v1)
		{
			for (int i = 0; i < 2; i++)
				m[i] /= v1.m[i];
			return *this;
		}

		vec2_basic &operator /= (T t)
		{
			for (int i = 0; i < 2; i++)
				m[i] /= t;
			return *this;
		}

		bool operator == (const vec2_basic &v1) const
		{
			for (int i = 0; i < 2; i++)
				if (m[i] != v1.m[i])
					return false;
			return true;
		}

		bool operator != (const vec2_basic &v1) const
		{
			return !(*this == v1);
		}

		T dot(const vec2_basic &v) const
		{
			T val = 0;
			for (int i = 0; i < 2; i++)
				val += m[i] * v.m[i];
			return val;
		}

		T lenSq() const
		{
			return dot(*this);
		}

		T len() const
		{
			return sqrt(lenSq());
		}

		vec2_basic &toLen(T len)
		{
			return *this *= (len / this->len());
		}

		vec2_basic toLen(T len) const
		{
			return *this * (len / this->len());
		}

		vec2_basic getLen(T len) const
		{
			return *this * (len / this->len());
		}

		vec2_basic &normalize()
		{
			if (this->lenSq() == 0)
				return *this;
			*this /= len();
			return *this;
		}

		/*vec2_basic normalize() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}*/

		vec2_basic unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}

		T distanceSq(const vec2_basic &v) const
		{
			return (*this - v).lenSq();
		}

		T distance(const vec2_basic &v) const
		{
			return (*this - v).len();
		}

		vec2_basic &reflect(const vec2_basic &normal)
		{
			*this = *this - (normal * ((T) 2.0 * dot(normal)));
			return *this;
		}

		operator T *() { return (T*) m; }
		operator const T *() const { return (T*) m; }
	};

	template <typename T>
	vec2_basic<T> operator + (T t, const vec2_basic<T> &v1)
	{
		vec2_basic<T> v;
		for (int i = 0; i < 2; i++)
			v.m[i] = t + v1.m[i];
		return v;
	}
	
	template <typename T>
	vec2_basic<T> operator - (T t, const vec2_basic<T> &v1)
	{
		vec2_basic<T> v;
		for (int i = 0; i < 2; i++)
			v.m[i] = t - v1.m[i];
		return v;
	}
	
	template <typename T>
	vec2_basic<T> operator * (T t, const vec2_basic<T> &v1)
	{
		vec2_basic<T> v;
		for (int i = 0; i < 2; i++)
			v.m[i] = t * v1.m[i];
		return v;
	}
	
	template <typename T>
	vec2_basic<T> operator / (T t, const vec2_basic<T> &v1)
	{
		vec2_basic<T> v;
		for (int i = 0; i < 2; i++)
			v.m[i] = t / v1.m[i];
		return v;
	}

	template <typename T>
	T distanceSq(const vec2_basic<T> &v1, const vec2_basic<T> &v2)
	{
		return (v1 - v2).lenSq();
	}

	template <typename T>
	T distance(const vec2_basic<T> &v1, const vec2_basic<T> &v2)
	{
		return (v1 - v2).len();
	}

	template <typename T>
	T dot(const vec2_basic<T> &v1, const vec2_basic<T> &v2)
	{
		T val = 0;
		for (int i = 0; i < 2; i++)
			val += v1.m[i] * v2.m[i];
		return val;
	}

	template <typename T>
	vec2_basic<T> &reflect(const vec2_basic<T> &v1, const vec2_basic<T> &normal)
	{
		vec2_basic<T> v = v1 - (normal * ((T) 2.0 * dot(normal, v1)));
		return v;
	}

	typedef vec2_basic<float> vec2;
	typedef vec2_basic<double> dvec2;
	typedef vec2_basic<int> ivec2;
	typedef vec2_basic<unsigned int> uvec2;
	//typedef vec2_basic<fixed> fivec2;
}

template <typename T>
std::ostream &operator << (std::ostream &out, Math::vec2_basic<T> v)
{
	out << v.x << "," << v.y;
	return out;
}

