#pragma once

namespace Math
{
	template <typename T, typename V, size_t a, size_t b, size_t c>
	class swizzle3
	{
	private:
		T m[3];

	public:
		V &operator = (const V &v)
		{
			m[a] = v.m[0];
			m[b] = v.m[1];
			m[c] = v.m[2];
			return *((V*) this);
		}

		operator V() { return V(m[a], m[b], m[c]); }
		operator T *() const { return (T*) m; }

		swizzle3 operator + (const V &v1) const
		{
			swizzle3 v;
			v.m[0] = m[a] + v1.m[0];
			v.m[1] = m[b] + v1.m[1];
			v.m[2] = m[c] + v1.m[2];
			return v;
		}

		swizzle3 operator + (T t) const
		{
			swizzle3 v;
			v.m[0] = m[a] + t;
			v.m[1] = m[b] + t;
			v.m[2] = m[c] + t;
			return v;
		}

		swizzle3 &operator += (const V &v1)
		{
			m[a] += v1.m[0];
			m[b] += v1.m[1];
			m[c] += v1.m[2];
			return *this;
		}

		swizzle3 &operator += (T t)
		{
			m[a] += t;
			m[b] += t;
			m[c] += t;
			return *this;
		}

		swizzle3 &operator - ()
		{
			m[a] = -m[0];
			m[b] = -m[1];
			m[c] = -m[2];
			return *this;
		}

		swizzle3 operator - (const V &v1) const
		{
			swizzle3 v;
			v.m[0] = m[a] - v1.m[0];
			v.m[1] = m[b] - v1.m[1];
			v.m[2] = m[c] - v1.m[2];
			return v;
		}

		swizzle3 operator - (T t) const
		{
			swizzle3 v;
			v.m[0] = m[a] - t;
			v.m[1] = m[b] - t;
			v.m[2] = m[c] - t;
			return v;
		}

		swizzle3 &operator -= (const V &v1)
		{
			m[a] -= v1.m[0];
			m[b] -= v1.m[1];
			m[c] -= v1.m[2];
			return *this;
		}

		swizzle3 &operator -= (T t)
		{
			m[a] -= t;
			m[b] -= t;
			m[c] -= t;
			return *this;
		}

		swizzle3 operator * (const V &v1) const
		{
			swizzle3 v;
			v.m[0] = m[a] * v1.m[0];
			v.m[1] = m[b] * v1.m[1];
			v.m[2] = m[c] * v1.m[2];
			return v;
		}

		swizzle3 operator * (T t) const
		{
			swizzle3 v;
			v.m[0] = m[a] * t;
			v.m[1] = m[b] * t;
			v.m[2] = m[c] * t;
			return v;
		}

		swizzle3 &operator *= (const V &v1)
		{
			m[a] *= v1.m[0];
			m[b] *= v1.m[1];
			m[c] *= v1.m[2];
			return *this;
		}

		swizzle3 &operator *= (T t)
		{
			m[a] *= t;
			m[b] *= t;
			m[c] *= t;
			return *this;
		}

		swizzle3 operator / (const V &v1) const
		{
			swizzle3 v;
			v.m[0] = m[a] / v1.m[0];
			v.m[1] = m[b] / v1.m[1];
			v.m[2] = m[c] / v1.m[2];
			return v;
		}

		swizzle3 operator / (T t) const
		{
			swizzle3 v;
			v.m[0] = m[a] / t;
			v.m[1] = m[b] / t;
			v.m[2] = m[c] / t;
			return v;
		}

		swizzle3 &operator /= (const V &v1)
		{
			m[a] /= v1.m[0];
			m[b] /= v1.m[1];
			m[c] /= v1.m[2];
			return *this;
		}

		swizzle3 &operator /= (T t)
		{
			m[a] /= t;
			m[b] /= t;
			m[c] /= t;
			return *this;
		}

		bool operator == (const V &v1) const
		{
			return m[a] == v1.m[0] && m[b] == v1.m[1] && m[c] == v1.m[2];
		}

		bool operator != (const V &v1) const
		{
			return !(*this == v1);
		}

		T dot(const V &v) const
		{
			T val = 0;
			val += m[a] * v.m[0];
			val += m[b] * v.m[1];
			val += m[c] * v.m[2];
			return val;
		}

		T dot(const swizzle3 &v) const
		{
			T val = 0;
			val += m[a] * v.m[0];
			val += m[b] * v.m[1];
			val += m[c] * v.m[2];
			return val;
		}

		T lenSq() const
		{
			return dot(*this);
		}

		T len() const
		{
			return sqrt(lenSq());
		}

		swizzle3 &toLen(T len)
		{
			return *this *= (len / this->len());
		}

		swizzle3 toLen(T len) const
		{
			return *this * (len / this->len());
		}

		swizzle3 getLen(T len) const
		{
			return *this * (len / this->len());
		}

		swizzle3 &normalize()
		{
			if (this->lenSq() == 0)
				return *this;
			*this /= len();
			return *this;
		}

		/*V normalize() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}*/

		/*swizzle3 unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}*/

		swizzle3 unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}

		T distanceSq(const V &v) const
		{
			return (*this - v).lenSq();
		}

		T distance(const V &v) const
		{
			return (*this - v).len();
		}

		swizzle3 &reflect(const V &normal)
		{
			*this = *this - (normal * ((T) 2.0 * normal.dot(*this)));
			return *this;
		}
	};
}

template <typename T, typename V, size_t a, size_t b, size_t c>
std::ostream &operator << (std::ostream &out, Math::swizzle3<T, V, a, b, c> v)
{
	auto vec = V(v);
	out << vec.x << "," << vec.y << "," << vec.z;
	return out;
}

