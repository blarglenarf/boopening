#pragma once

namespace Math
{
	template <typename T, typename V, size_t a, size_t b>
	class swizzle2
	{
	private:
		T m[2];

	public:
		V &operator = (const V &v)
		{
			m[a] = v.m[0];
			m[b] = v.m[1];
			return *((V*) this);
		}

		operator V() { return V(m[a], m[b]); }
		operator T *() const { return (T*) m; }

		swizzle2 operator + (const V &v1) const
		{
			swizzle2 v;
			v.m[0] = m[a] + v1.m[0];
			v.m[1] = m[b] + v1.m[1];
			return v;
		}

		swizzle2 operator + (T t) const
		{
			swizzle2 v;
			v.m[0] = m[a] + t;
			v.m[1] = m[b] + t;
			return v;
		}

		swizzle2 &operator += (const V &v1)
		{
			m[a] += v1.m[0];
			m[b] += v1.m[1];
			return *this;
		}

		swizzle2 &operator += (T t)
		{
			m[a] += t;
			m[b] += t;
			return *this;
		}

		swizzle2 &operator - ()
		{
			m[a] = -m[0];
			m[b] = -m[1];
			return *this;
		}

		swizzle2 operator - (const V &v1) const
		{
			swizzle2 v;
			v.m[0] = m[a] - v1.m[0];
			v.m[1] = m[b] - v1.m[1];
			return v;
		}

		swizzle2 operator - (T t) const
		{
			swizzle2 v;
			v.m[0] = m[a] - t;
			v.m[1] = m[b] - t;
			return v;
		}

		swizzle2 &operator -= (const V &v1)
		{
			m[a] -= v1.m[0];
			m[b] -= v1.m[1];
			return *this;
		}

		swizzle2 &operator -= (T t)
		{
			m[a] -= t;
			m[b] -= t;
			return *this;
		}

		swizzle2 operator * (const V &v1) const
		{
			swizzle2 v;
			v.m[0] = m[a] * v1.m[0];
			v.m[1] = m[b] * v1.m[1];
			return v;
		}

		swizzle2 operator * (T t) const
		{
			swizzle2 v;
			v.m[0] = m[a] * t;
			v.m[1] = m[b] * t;
			return v;
		}

		swizzle2 &operator *= (const V &v1)
		{
			m[a] *= v1.m[0];
			m[b] *= v1.m[1];
			return *this;
		}

		swizzle2 &operator *= (T t)
		{
			m[a] *= t;
			m[b] *= t;
			return *this;
		}

		swizzle2 operator / (const V &v1) const
		{
			swizzle2 v;
			v.m[0] = m[a] / v1.m[0];
			v.m[1] = m[b] / v1.m[1];
			return v;
		}

		swizzle2 operator / (T t) const
		{
			swizzle2 v;
			v.m[0] = m[a] / t;
			v.m[1] = m[b] / t;
			return v;
		}

		swizzle2 &operator /= (const V &v1)
		{
			m[a] /= v1.m[0];
			m[b] /= v1.m[1];
			return *this;
		}

		swizzle2 &operator /= (T t)
		{
			m[a] /= t;
			m[b] /= t;
			return *this;
		}

		bool operator == (const V &v1) const
		{
			return m[a] == v1.m[0] && m[b] == v1.m[1];
		}

		bool operator != (const V &v1) const
		{
			return !(*this == v1);
		}

		T dot(const V &v) const
		{
			T val = 0;
			val += m[a] * v.m[0];
			val += m[b] * v.m[1];
			return val;
		}

		T dot(const swizzle2 &v) const
		{
			T val = 0;
			val += m[a] * v.m[0];
			val += m[b] * v.m[1];
			return val;
		}

		T lenSq() const
		{
			return dot(*this);
		}

		T len() const
		{
			return sqrt(lenSq());
		}

		swizzle2 &toLen(T len)
		{
			return *this *= (len / this->len());
		}

		swizzle2 toLen(T len) const
		{
			return *this * (len / this->len());
		}

		swizzle2 getLen(T len) const
		{
			return *this * (len / this->len());
		}

		swizzle2 &normalize()
		{
			if (this->lenSq() == 0)
				return *this;
			*this /= len();
			return *this;
		}

		/*V normalize()
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}*/

		/*swizzle2 unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}*/

		swizzle2 unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}

		T distanceSq(const V &v) const
		{
			return (*this - v).lenSq();
		}

		T distance(const V &v) const
		{
			return (*this - v).len();
		}

		swizzle2 &reflect(const V &normal)
		{
			*this = *this - (normal * ((T) 2.0 * dot(normal, *this)));
			return *this;
		}
	};
}

template <typename T, typename V, size_t a, size_t b>
std::ostream &operator << (std::ostream &out, Math::swizzle2<T, V, a, b> v)
{
	auto vec = V(v);
	out << vec.x << "," << vec.y;
	return out;
}

