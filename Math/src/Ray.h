#pragma once

#include "Matrix/MatrixCommon.h"
#include "Vector/VectorCommon.h"

namespace Math
{
	class Ray
	{
	public:
		vec3 origin;
		vec3 dir;

	public:
		Ray() {}
		Ray(const vec3 &origin, const vec3 &dir) : origin(origin), dir(dir) {}

		static Ray screenToRay(int x, int y, const mat4 &model, const mat4 &proj, const ivec4 &view, float near = 0, float far = 1);
	};
}

