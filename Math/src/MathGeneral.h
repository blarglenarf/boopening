#pragma once

#include <algorithm>

#undef min
#undef max

#ifndef M_PI
#define M_PI 3.14159265359
#endif

namespace Math
{
	static const double pi = M_PI;

	template <typename T>
	T toRad(T deg)
	{
		return deg * (T) (M_PI / 180.0);
	}

	template <typename T>
	T toDeg(T rad)
	{
		return rad * (T) (180.0 / M_PI);
	}

	template <typename T>
	bool isPower2(T t)
	{
		return (t != 0) && ((t & (t - 1)) == 0);
	}

	template <typename T>
	T nextPower2(T t)
	{
		t--;
		t |= t >> 1;   // Divide by 2^k for consecutive doublings of k up to 32,
		t |= t >> 2;   // and then or the results.
		t |= t >> 4;
		t |= t >> 8;
		t |= t >> 16;
		t++;
		return t;
	}

	template <typename T>
	T clamp(T val, T min, T max)
	{
		T newVal = val;
		if (val < min)
			newVal = min;
		if (val > max)
			newVal = max;
		return newVal;
	}

	template <typename T>
	T dist(T x, T y)
	{
		/*if (x == 0)
		{
			x += 1;
			y += 1;
		}
		if (y == 0)
		{
			y += 1;
			x += 1;
		}

		return abs(x - y) / std::max(abs(x), abs(y));*/
		return abs(x - y);
	}

	// Performs a linear interpolation of this vector and v1, with val as the time between 0 and 1.
	/*vec lerp(vec &v1, T val) const
	{
		return *this + val * (v1 - *this); // FIXME: should be: start + t * ((endValue - startValue) / (endTime - startTime))
	}*/

	// TODO: Not sure if this should be replaced or not. See lerp from Animation.cpp for an alternative.
	template <typename T>
	T lerp(T val, T min, T max)
	{
		return min + (max - min) * val;
	}

	template <typename T>
	T min(T x, T y)
	{
		if (x < y)
			return x;
		return y;
	}

	template <typename T>
	T max(T x, T y)
	{
		if (x > y)
			return x;
		return y;
	}
}
