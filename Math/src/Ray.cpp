#include "Ray.h"

using namespace Math;

Ray Ray::screenToRay(int x, int y, const mat4 &model, const mat4 &proj, const ivec4 &view, float near, float far)
{
	vec3 v1 = unProject(vec3((float) x, (float) y, near), model, proj, view);
	vec3 v2 = unProject(vec3((float) x, (float) y, far), model, proj, view);

	// Should this be v2 - v1 or v1 - v2?
	vec3 r = (v2 - v1).normalize();
	return Ray(v1, r);
}

