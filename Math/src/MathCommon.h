#pragma once

#include "MathGeneral.h"
#include "FixedPoint.h"
#include "Vector/VectorCommon.h"
#include "Matrix/MatrixCommon.h"
#include "Quat.h"
#include "Ray.h"
#include "Rect.h"

#ifndef M_PI
#define M_PI 3.14159265359
#endif
