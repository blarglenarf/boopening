#pragma once

#include <ostream>

namespace Math
{
	typedef long long int64;

	class fixed
	{
	public:
		int64 value;

		// The number of decimal places we'll be using (same as dividing/multiplying by 4096).
		static const int shiftAmount = 12;

		// If a fixed number is within range of another fixed number by this amount, == will return true.
		static const int errorMargin = 2;

		// Fixed value of one, stored as a 64 bit integer for double conversion.
		static const int64 one64 = 1 << shiftAmount;

		// Fixed value of one, stored as a 32 bit integer for float conversion (on 32 bit systems).
		static const int one32 = 1 << shiftAmount;

	public:
		// Default constructor creates a fixed with a value of 0.
		fixed() : value(0) {}

		// Conversion constructor to convert a 64 bit int to a fixed, works implicitly.
		fixed(int64 i) : value(i << shiftAmount) {}

		// Conversion constructor to convert an int (either 32 or 64 bits) to a fixed, works implicitly.
		fixed(int i) : value((int64) i << shiftAmount) {}

		// Conversion constructor to convert a float to a fixed, works implicitly.
		fixed(float f)
		{
			f *= (float) one32;
			value = (int) (f + 0.5f);
		}

		// Conversion constructor to convert a double to a fixed, works implicitly.
		fixed(double d)
		{
			d *= (double) one64;
			value = (int64) (d + 0.5);
		}

		// Returns the value of the fixed as an int, loses any data after the decimal point.
		int toInt() const { return (int) (value >> shiftAmount); }

		// Returns the value of the fixed as a double.
		double toDouble() const { return (double) (value) / (double) one64; }

		// Returns the value of the fixed as a float, possible loss of data with numbers using more than 32 bits.
		float toFloat() const { return (float) (value) / (float) one32; }

		// Standard addition assignment operator.
		void operator += (fixed f) { value += f.value; }

		// Standard subtraction assignment operator.
		void operator -= (fixed f) { value -= f.value; }

		// Standard multiplication assignment operator.
		void operator *= (fixed f) { value = (value * f.value) >> shiftAmount; }

		// Standard division assignment operator.
		void operator /= (fixed f) { value = (value << shiftAmount) / f.value; }

		// Standard < comparison operator.
		bool operator < (fixed f) const { return value < f.value; }

		// Standard > comparison operator.
		bool operator > (fixed f) const { return value > f.value; }

		// Standard >= comparison operator.
		bool operator >= (fixed f) const { return value >= f.value; }

		// Standard <= comparison operator.
		bool operator <= (fixed f) const { return value <= f.value; }

		// Standard addition operator, doesn't alter the value stored in this.
		fixed operator + (fixed f1) const
		{
			fixed f;
			f.value = value + f1.value;
			return f;
		}

		// Standard subtraction operator, doesn't alter the value stored in this.
		fixed operator - (fixed f1) const
		{
			fixed f;
			f.value = value - f1.value;
			return f;
		}

		// Standard multiplication operator, doesn't alter the value stored in this.
		fixed operator * (fixed f1) const
		{
			fixed f;
			f.value = (value * f1.value) >> fixed::shiftAmount;
			return f;
		}

		// Standard division operator, doesn't alter the value stored in this.
		fixed operator / (fixed f1) const
		{
			fixed f;
			f.value = (value << fixed::shiftAmount) / f1.value;
			return f;
		}

		// Standard == operator, returns true if values are within range stored in errorMargin.
		bool operator == (fixed f1) const
		{
			int64 newValue = value - f1.value;
			return newValue <= fixed::errorMargin && newValue >= -fixed::errorMargin;
		}

		// Standard != operator, returns false if values are within range stored in errorMargin.
		bool operator != (fixed f1) const
		{
			return !(*this == f1);
		}
	};
#if 0
	// Standard addition operator for fixed values.
	template <typename T>
	fixed operator + (fixed f1, T f2)
	{
		return f1 + (fixed) f2;
	}

	// Standard addition operator for fixed values.
	template <typename T>
	fixed operator + (T f1, fixed f2)
	{
		return (fixed) f1 + f2;
	}

	// Standard subtraction operator for fixed values.
	template <typename T>
	fixed operator - (fixed f1, T f2)
	{
		return f1 - (fixed) f2;
	}

	// Standard subtraction operator for fixed values.
	template <typename T>
	fixed operator - (T f1, fixed f2)
	{
		return (fixed) f1 - f2;
	}

	// Standard multiplication operator for fixed values.
	template <typename T>
	fixed operator * (fixed f1, T f2)
	{
		return f1 * (fixed) f2;
	}

	// Standard multiplication operator for fixed values.
	template <typename T>
	fixed operator * (T f1, fixed f2)
	{
		return (fixed) f1 * f2;
	}

	// Standard division operator for fixed values.
	template <typename T>
	fixed operator / (fixed f1, T f2)
	{
		return f1 / (fixed) f2;
	}

	// Standard division operator for fixed values.
	template <typename T>
	fixed operator / (T f1, fixed f2)
	{
		return (fixed) f1 / f2;
	}
#endif
}

// For converting fixed points to strings, exists outside the namespace so it will work anywhere.
std::ostream &operator << (std::ostream &out, Math::fixed f);
