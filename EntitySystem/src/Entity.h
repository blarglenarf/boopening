#pragma once

#include <string>
#include <vector>

namespace EntitySystem
{
	class Component;

	class Entity
	{
	private:
		std::string type;

		// TODO: How do we actually want the components to be stored?
		// TODO: Would it be better to store indices of the components in their respective systems?
		// TODO: Entity could just be a set of components with a common index, not sure how that's different to what I have here...
		std::vector<Component*> components;

	public:
		Entity() {}
		~Entity() {}

		void setType(const std::string &type) { this->type = type; }

		const std::string &getType() const { return type; }
	};
}

