#pragma once

#include <vector>
#include <list>
#include <map>

namespace EntitySystem
{
	class Component;
	class Entity;

	// TODO: System phases:
	// TODO: Query components (eg, query an entity to see if it has the correct component combination, could be multiple correct combinations).
	// TODO: Allocate temp result buffers.
	// TODO: Declare data used and permisions.
	// TODO: Execute.
	// TODO: Batched execution, eg 64K of components executed per-thread.
	// TODO: May also want some barriers in case some systems need to wait for others.
	// TODO: Do we want the ability to do any of this stuff on the GPU, or should that just be used for rendering?
	// TODO: Do we want each component to only be one piece of data or potentially multiple?

	// TODO: May want a system archetype for grouping systems together for processing?
	// TODO: System may want to operate on more than one type of component.
	// TODO: Each system may be better operating on component groups rather than individual components.
	class System
	{
	protected:
		// Fast iterating of components for updating.
		std::vector<Component*> components;

		// Fast removal of components.
		std::map<Component*, size_t> componentIndices;

		// Empty indices in the components vector.
		std::list<size_t> empty;

	public:
		System() {}
		virtual ~System() {}

		virtual Component *addComponent(Component *comp);
		virtual Component *removeComponent(Component *comp);

		virtual void update(float dt);
	};
}

