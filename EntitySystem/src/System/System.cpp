#include "System.h"

using namespace EntitySystem;

Component *System::addComponent(Component *comp)
{
	if (!empty.empty())
	{
		size_t index = empty.front();
		components[index] = comp;
		componentIndices[comp] = index;
		empty.pop_front();
	}
	else
	{
		components.push_back(comp);
		componentIndices[comp] = components.size() - 1;
	}
	return comp;
}

Component *System::removeComponent(Component *comp)
{
	auto it = componentIndices.find(comp);
	if (it == componentIndices.end())
		return nullptr;

	size_t index = it->second;
	components[index] = nullptr;
	empty.push_back(index);
	componentIndices.erase(it);
	return comp;
}

void System::update(float dt)
{
	(void) (dt);
	//for (auto *component : components)
	//	component->update(dt);
}

