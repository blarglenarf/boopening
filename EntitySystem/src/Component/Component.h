#pragma once

#include <string>

namespace EntitySystem
{
	class Entity;

	// TODO: May want a component archetype for grouping components together for processing.
	// TODO: Could just call this a component group.
	class Component
	{
	protected:
		Entity *entity;
		std::string type;

	public:
		Component() : entity(nullptr) {}
		virtual ~Component() {}

		const std::string &getType() const { return type; }
		Entity *getEntity() { return entity; }
	};
}

