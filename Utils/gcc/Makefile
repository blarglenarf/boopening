####################################################################
# make commands:
#	* make						- defaults to release build
#	* make release				- release build (-O3)
#	* make debug				- debug build (-O2 -g)
#	* make clean				- removes .o and .d files
#	* make cleaner				- removes .o and .d files, and bin file
#	* make verbose=true			- verbose mode (prints all compile information)
####################################################################


####################################################################
#### Modify any of these settings to suit your specific project ####
####################################################################

# Default build.
.PHONY: all
all: release

# Name of executable/library to be created.
BIN := libUtils

# Path to put the executable/lib relative to makefile.
BIN_PATH := ../../../libs/lib

# Compiler to be used for compiling object files.
CC := g++

# Linker to be used for linking object files, or creating lib.
LD := ar

# Path of source folder relative to makefile.
SRC_PATH := ../src

# Path of folder to place object files, relative to makefile.
OBJ_PATH := .build

# Additional include paths.
INCLUDES := -I../../../libs/include

# Additional lib paths.
LIBS := 

# General compile flags.
CFLAGS := -Wall -Wfatal-errors -Wextra -Werror -std=c++11 -D_LINUX

# General linker flags.
LDFLAGS := 

# 64 bit linker flags (debug versions will have dbg appended to them).
LDFLAGS_64 := 

# 32 bit linker flags (debug versions will have dbg appended to them).
LDFLAGS_32 := 

# Debug specific compile flags.
DBG_CFLAGS := -g

# Release specific compile flags.
RLS_CFLAGS := -O3

# 64 bit compile flags.
CFLAGS_64 := -m64

# 32 bit compile flags.
CFLAGS_32 := -m32

# Choose either 32 or 64 bit mode.
MODE := 64

# Extra libs that may need to be compiled.
# TODO: Come up with a better way of doing this!!!
.PHONY: dep_libs_rel
.PHONY: dep_libs_dbg
.PHONY: dep_libs_cln

dep_libs_rel: 
dep_libs_dbg: 
dep_libs_cln: 





###############################################################
#### You shouldn't need to modify anything below this line ####
###############################################################

# Setup CFLAGS, LDFLAGS and BIN.
PLATFORM_CFLAGS :=
PLATFORM_LDFLAGS_RLS :=

ifeq ($(MODE),64)
	PLATFORM_CFLAGS := $(CFLAGS_64)
	PLATFORM_LDFLAGS_RLS := $(LDFLAGS_64)
	BIN := $(BIN)64
endif

ifeq ($(MODE),32)
	PLATFORM_CFLAGS += $(CFLAGS_32)
	PLATFORM_LDFLAGS_RLS := $(LDFLAGS_32)
	BIN := $(BIN)32
endif

PLATFORM_LDFLAGS_DBG := $(PLATFORM_LDFLAGS_RLS:%=%dbg)

# Remove command for cleaning.
RM := rm -f

# Shell used in this makefile.
SHELL := /bin/bash

# Verbose option, to output compile and link commands.
verbose := false
CMD_PREFIX := @
ifeq ($(verbose),true)
	CMD_PREFIX := 
endif

# Command option for compiling, -o for exe, rcs for lib, binary extension for libs.
BIN_EXT :=
CMD_OPT := -o
ifeq ($(LD),ar)
	CMD_OPT := rcs
	BIN_EXT := .a
endif

# Source files.
CPP_SOURCES := $(shell find $(SRC_PATH)/ -name '*.cpp')
C_SOURCES := $(shell find $(SRC_PATH)/ -name '*.c')

# Object files, mimicking src sub-folder structure, without added object path.
CPP_OBJECTS := $(CPP_SOURCES:$(SRC_PATH)/%.cpp=%.o)
C_OBJECTS := $(C_SOURCES:$(SRC_PATH)/%.c=%.o)

# Temporary folders created for the object files, mimicking structure of source files.
DIRS := $(addprefix $(OBJ_PATH)/, $(filter-out ./, $(dir $(CPP_OBJECTS) $(C_OBJECTS))))

# Add object file path, now that temporary folder info has been created.
CPP_OBJECTS := $(CPP_OBJECTS:%.o=$(OBJ_PATH)/%.o)
C_OBJECTS := $(C_OBJECTS:%.o=$(OBJ_PATH)/%.o)

# Dependency files.
DEPS := $(CPP_OBJECTS:.o=.d)
DEPS += $(C_OBJECTS:.o=.d)

# Default build.
.PHONY: all
all: release

TMP_LDFLAGS := 

# Release build.
.PHONY: release
release: CFLAGS += $(RLS_CFLAGS)
release: TMP_LDFLAGS += $(PLATFORM_LDFLAGS_RLS)
release: TMP_LDFLAGS += $(LDFLAGS)
release: BIN := $(BIN)$(BIN_EXT)
release: dep_libs_rel
release: compile

# Debug build.
.PHONY: debug
debug: CFLAGS += $(DBG_CFLAGS)
debug: TMP_LDFLAGS += $(PLATFORM_LDFLAGS_DBG)
debug: TMP_LDFLAGS += $(LDFLAGS)
debug: BIN := $(BIN)dbg$(BIN_EXT)
debug: dep_libs_dbg
debug: compile

# Standard compilation command.
.PHONY: compile
compile: dirs $(BIN_PATH)/$(BIN)

# Link command for the exe, will compile object files first if necessary.
$(BIN_PATH)/$(BIN) : $(CPP_OBJECTS) $(C_OBJECTS)
	@echo linking $(BIN_PATH)/$(BIN)
	$(CMD_PREFIX)$(LD) $(CMD_OPT) $(BIN_PATH)/$(BIN) $(CPP_OBJECTS) $(C_OBJECTS) $(LIBS) $(TMP_LDFLAGS)

# Removes all build files.
.PHONY: clean
clean: dep_libs_cln
clean:
	@echo "Removing dependencies"
	$(CMD_PREFIX)$(RM) $(DEPS)
	@echo "Removing object files"
	$(CMD_PREFIX)$(RM) $(CPP_OBJECTS) $(C_OBJECTS)
ifneq ($(strip $(DIRS)),)
	@echo "Removing folders"
	$(CMD_PREFIX)rmdir $(DIRS) 2> /dev/null || true
endif

# Removes all build files and binary.
.PHONY: cleaner
cleaner: clean
	@echo "Removing executable"
	$(CMD_PREFIX)$(RM) $(BIN_PATH)/$(BIN)

# All folders that need to be created for the object files.
.PHONY: dirs
dirs:
	@echo "Creating folders"
	@mkdir -p .build
ifneq ($(strip $(DIRS)),)
	@mkdir -p $(DIRS)
endif

# Lists all source files.
.PHONY: echosrc
echosrc:
	@echo "Listing source files"
	@echo $(CPP_SOURCES) $(C_SOURCES)

# Lists all object files.
.PHONY: echoobj
echoobj:
	@echo "Listing object files"
	@echo $(CPP_OBJECTS) $(C_OBJECTS)

# Lists all dependencies, can only be done after the dependencies have been generated (ie compiled).
.PHONY: echodeps
echodeps:
	@echo "Listing dependencies"
	@cat $(DEPS)

# Add dependency files, if they exist.
-include $(DEPS)

# Source file rules.
$(CPP_OBJECTS): $(OBJ_PATH)/%.o: $(SRC_PATH)/%.cpp
	@echo "Compiling: $< -> $@"
	$(CMD_PREFIX)$(CC) $(INCLUDES) $(CFLAGS) -MP -MMD -c $< -o $@

$(C_OBJECTS): $(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@echo "Compiling: $< -> $@"
	$(CMD_PREFIX)$(CC) $(INCLUDES) $(CFLAGS) -MP -MMD -c $< -o $@
