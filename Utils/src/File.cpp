#include "File.h"

#include <sys/stat.h>

using namespace Utils;

std::map<std::string, size_t> TypeInfo::typeSizes;

bool ObjectBuffer::sysLittleEndianFlag = true;


void TypeInfo::initDefaultTypeSizes()
{
	static bool initFlag = false;
	if (initFlag)
		return;
	setTypeSize("bool", sizeof(bool));
	setTypeSize("char", sizeof(char));
	setTypeSize("uchar", sizeof(unsigned char));
	setTypeSize("int", sizeof(int));
	setTypeSize("uint", sizeof(unsigned int));
	setTypeSize("float", sizeof(float));
	setTypeSize("double", sizeof(double));
	setTypeSize("string", sizeof(std::string));

	initFlag = true;
}


TypeInfo::TypeInfo(const std::string &data, size_t &curr)
{
	initDefaultTypeSizes();

	size_t prev = curr;
	auto str = [&]() -> std::string { auto s = std::string(data.begin() + prev, data.begin() + curr); prev = curr + 1; return s; };

	for (; curr < data.size(); curr++)
	{
		if (data[curr] == ' ')
		{
			prev = curr + 1;
			continue;
		}
		else if (data[curr] == '(')
			type = str();
		else if (data[curr] == ')' || data[curr] == '{')
			break;

		if (data[curr] == ':')
		{
			auto s = str();
			curr = prev + 1;
			types.push_back({s, str() == "n" ? false : true});
			if (data[curr] == ')' || data[curr] == '{')
				curr--;
		}
	}
}



ObjectBuffer::ObjectBuffer(int blockSize) : dataPos(0), blockSize(blockSize), fileLittleEndianFlag(true), firstReadFlag(true)
{
	int n = 1;
	sysLittleEndianFlag = (*(char*) &n) == 1;

	data.resize(blockSize > 0 ? blockSize : 255);
}

void ObjectBuffer::write(std::string *s)
{
	size_t bytes = s->length();
	if (blockSize > 0 && (int) (dataPos + bytes + sizeof(bytes)) >= blockSize)
		saveToDisk(false);
	if (data.size() < dataPos + bytes + sizeof(bytes))
	{
		// TODO: split the data up into smaller chunks
		data.resize(dataPos + bytes + sizeof(bytes));
	}
	memcpy(&data[dataPos], &bytes, sizeof(bytes));
	memcpy(&data[dataPos + sizeof(bytes)], &s->data()[0], bytes);
	dataPos += bytes + sizeof(bytes);
}

void ObjectBuffer::write(char *c, size_t bytes)
{
	if (blockSize > 0 && (int) (dataPos + bytes) >= blockSize)
		saveToDisk(false);
	if (data.size() < dataPos + bytes)
		data.resize(dataPos + bytes);
	memcpy(&data[dataPos], c, bytes);
	dataPos += bytes;
}

void ObjectBuffer::read(std::string *s)
{
	size_t bytes = 0;
	if (firstReadFlag || (blockSize > 0 && (int) (dataPos + sizeof(bytes)) >= blockSize))
		loadFromDisk(false);
	memcpy((char*) &bytes, &data[dataPos], sizeof(bytes));
	dataPos += sizeof(bytes);
	if (firstReadFlag || (blockSize > 0 && (int) (dataPos + bytes) >= blockSize))
		loadFromDisk(false);
	s->resize(bytes);
	memcpy((char*) &s->data()[0], &data[dataPos], bytes);
	dataPos += bytes;
}

void ObjectBuffer::read(char *c, size_t bytes)
{
	if (firstReadFlag || (blockSize > 0 && (int) (dataPos + bytes) >= blockSize))
		loadFromDisk(false);
	memcpy(c, &data[dataPos], bytes);
	dataPos += bytes;
}

void ObjectBuffer::saveToDisk(bool closeFlag)
{
	firstReadFlag = true;

	if (ifstr.is_open())
		ifstr.close();
	if (!ofstr.is_open())
	{
		ofstr.open(filename, std::ios::binary | std::ios::out | std::ios::trunc);		
		if (!ofstr.is_open())
			throw std::runtime_error("Failed to open file " + filename + " for writing");

		fileLittleEndianFlag = sysLittleEndianFlag;
		ofstr.write((char*) &fileLittleEndianFlag, sizeof(fileLittleEndianFlag));
	}

	if (dataPos > 0)
	{
		ofstr.write((char*) &data[0], dataPos);
		dataPos = 0;
	}

	if (closeFlag)
		ofstr.close();
}

void ObjectBuffer::loadFromDisk(bool closeFlag)
{
	if (ofstr.is_open())
		ofstr.close();
	if (!ifstr.is_open())
	{
		ifstr.open(filename, std::ios::binary | std::ios::in);
		if (!ifstr.is_open())
			throw std::runtime_error("Failed to open file " + filename + " for reading");

		ifstr.read((char*) &fileLittleEndianFlag, sizeof(fileLittleEndianFlag));
	}

	if (blockSize < 0)
	{
		size_t size = File::fileSize(filename);
		data.resize(size);
		ifstr.read(&data[0], size);
		dataPos = 0;
	}
	else
	{
		if (!firstReadFlag)
		{
			for (size_t i = dataPos, j = 0; (int) i < blockSize; i++, j++)
				data[j] = data[i];
			dataPos = blockSize - dataPos;
		}
		else
			dataPos = 0;
		ifstr.read(&data[dataPos], blockSize - dataPos);
		dataPos = 0;
	}

	if (closeFlag)
	{
		firstReadFlag = true;
		ifstr.close();
	}
	else
		firstReadFlag = false;
}



void File::saveToDisk()
{
	currBlock = 0;
	buffer.saveToDisk();
	buffer.clear();
}

File::File(const std::string &filename, const std::string &infoStr) : currBlock(0)
{
	if (filename != "")
		buffer.setFilename(filename);
	if (infoStr != "")
		setFormat(infoStr);
}

void File::setFormat(const std::string &infoStr)
{
	// First build table of different types.
	size_t curr = 0;
	for (; curr < infoStr.size(); curr++)
	{
		if (infoStr[curr] == ' ')
			continue;

		TypeInfo type(infoStr, curr);
		infoTable[type.getType()] = type;

		if (infoStr[curr] == '{')
			break;
	}

	// Then create actual file format.
	size_t prev = curr;
	auto str = [&]() -> std::string { auto s = std::string(infoStr.begin() + prev, infoStr.begin() + curr); prev = curr + 1; return s; };
	for (; curr < infoStr.size(); curr++)
	{
		if (infoStr[curr] == ' ' || infoStr[curr] == '{')
		{
			prev = curr + 1;
			continue;
		}

		if (infoStr[curr] == ':')
		{
			auto s = str();
			curr = prev + 1;
			auto amt = str();
			fileFormat.push_back({s, amt[0]});
		}
		else if (infoStr[curr] == '}')
			break;
	}
}

File::TextRange File::getLines(char delim)
{
	auto &ifstr = buffer.getInputStream();
	if (!ifstr.is_open())
		ifstr.open(getFilename(), std::ios::in);
	if (!ifstr.is_open())
		throw std::runtime_error("Failed to open file " + buffer.getFilename() + " for reading");

	return TextRange(&ifstr, delim);
}

std::string File::getStr()
{
	std::string str;

	auto &ifstr = buffer.getInputStream();
	if (!ifstr.is_open())
		ifstr.open(getFilename(), std::ios::in);
	if (!ifstr.is_open())
		throw std::runtime_error("Failed to open file " + buffer.getFilename() + " for reading");

	str.resize(size());
	ifstr.read(&str[0], str.size());

	ifstr.close();

	return str;
}

void File::write(const std::vector<std::string> &strs, char delim, bool append)
{
	auto &ofstr = buffer.getOutputStream();
	if (!ofstr.is_open())
		ofstr.open(getFilename(), append ? std::ios::out | std::ios::app : std::ios::out | std::ios::trunc);
	if (!ofstr.is_open())
		throw std::runtime_error("Failed to open file " + buffer.getFilename() + " for writing");

	for (auto &str : strs)
		ofstr << str << delim;

	ofstr.close();
}

void File::write(const std::string &str, char delim, bool append)
{
	// No real advantage to doing this over just using an std::ofstream...
	auto &ofstr = buffer.getOutputStream();
	if (!ofstr.is_open())
		ofstr.open(getFilename(), append ? std::ios::out | std::ios::app : std::ios::out | std::ios::trunc);
	if (!ofstr.is_open())
		throw std::runtime_error("Failed to open file " + buffer.getFilename() + " for writing");

	ofstr << str << delim;
}

File::TextIterator File::begin()
{
	auto &ifstr = buffer.getInputStream();
	if (!ifstr.is_open())
		ifstr.open(getFilename(), std::ios::in);
	if (!ifstr.is_open())
		throw std::runtime_error("Failed to open file " + buffer.getFilename() + " for reading");

	return TextIterator(&line, &buffer.getInputStream(), '\n');
}

File::TextIterator File::end()
{
	return TextIterator();
}

size_t File::fileSize(const std::string &filename)
{
	struct stat st;
	if (stat(filename.c_str(), &st) < 0)
		throw std::runtime_error("Unable to get information about file " + filename + " when calculating file size");
	return st.st_size;
}

bool File::exists(const std::string &filename)
{
	struct stat st;
	if (stat(filename.c_str(), &st) < 0)
		return false;
	return true;
}

