#include "MazeBuilder.h"

#include <stack>
#include <algorithm>

using namespace Utils;

void MazeBuilder::generate(bool unvisitedFlag)
{
	// Set maze to appropriate size
	cells.resize(width);
	for (int x = 0; x < (int) cells.size(); x++)
	{
		cells[x].resize(height);
		for (int y = 0; y < (int) cells[x].size(); y++)
		{
			auto &cell = cells[x][y];
			cell.x = x;
			cell.y = y;

			if (unvisitedFlag)
				unvisited.insert(&cell);
		}
	}
}

void MazeBuilder::generateDFS()
{
	generate(true);
}

void MazeBuilder::generateKruskal()
{
	generate();

	// Wall arrangment:
	//
	//    ___2___
	//    |     |
	//   0| x,y |3
	//    |_____|
	//       1
	//
	// No. of walls is 2 * no. of cells + no. of rows + no. of cols
	walls.resize(2 * width * height + width + height);
	int currWall = 0;

	for (int x = 0; x < (int) cells.size(); x++)
	{
		for (int y = 0; y < (int) cells[x].size(); y++)
		{
			// First handle walls 2 and 3 for every cell
			walls[currWall].left = &cells[x][y];
			walls[currWall++].right = y < height - 1 ? &cells[x][y + 1] : 0;
			walls[currWall].left = &cells[x][y];
			walls[currWall++].right = x < width - 1 ? &cells[x + 1][y] : 0;
		}
	}

	// Technically no need for the below walls, but there for completeness

	// Handle left column walls
	for (int y = 0; y < height; y++)
	{
		walls[currWall].left = nullptr;
		walls[currWall++].right = &cells[0][y];
	}

	// Handle bottom row walls
	for (int x = 0; x < width; x++)
	{
		walls[currWall].left = nullptr;
		walls[currWall++].right = &cells[x][0];
	}
}

void MazeBuilder::generatePrim(std::mt19937 &rng)
{
	generate();

	// Wall arrangment:
	//
	//    ___2___
	//    |     |
	//   0| x,y |3
	//    |_____|
	//       1
	//
	// No. of walls is 2 * no. of cells + no. of rows + no. of cols
	walls.resize(2 * width * height + width + height);
	int currWall = 0;

	for (int x = 0; x < (int) cells.size(); x++)
	{
		for (int y = 0; y < (int) cells[x].size(); y++)
		{
			cells[x][y].top = &walls[currWall];
			cells[x][y].right = &walls[currWall + 1];

			cells[x][y].left = x > 0 ? cells[x - 1][y].right : 0;
			cells[x][y].bottom = y > 0 ? cells[x][y - 1].bottom : 0;

			// First handle walls 2 and 3 for every cell
			walls[currWall].weight = rng();
			walls[currWall].left = &cells[x][y];
			walls[currWall++].right = y < height - 1 ? &cells[x][y + 1] : 0;

			walls[currWall].weight = rng();
			walls[currWall].left = &cells[x][y];
			walls[currWall++].right = x < width - 1 ? &cells[x + 1][y] : 0;
		}
	}

	// Technically no need for the below walls, but there for completeness

	// Handle left column walls
	for (int y = 0; y < height; y++)
	{
		cells[0][y].left = &walls[currWall];
		walls[currWall].weight = rng();
		walls[currWall].left = nullptr;
		walls[currWall++].right = &cells[0][y];
	}

	// Handle bottom row walls
	for (int x = 0; x < width; x++)
	{
		cells[x][0].bottom = &walls[currWall];
		walls[currWall].weight = rng();
		walls[currWall].left = nullptr;
		walls[currWall++].right = &cells[x][0];
	}
}

MazeBuilder::Cell *MazeBuilder::getUnvisitedNeighbour(Cell *cell, std::mt19937 &rng)
{
	// Get all unvisited neighbours of the current cell
	std::vector<Cell*> neighbours;

	if (cell->x > 0 && !cells[cell->x - 1][cell->y].visitFlag)
		neighbours.push_back(&cells[cell->x - 1][cell->y]);
	if (cell->y > 0 && !cells[cell->x][cell->y - 1].visitFlag)
		neighbours.push_back(&cells[cell->x][cell->y - 1]);
	if (cell->x < width - 1 && !cells[cell->x + 1][cell->y].visitFlag)
		neighbours.push_back(&cells[cell->x + 1][cell->y]);
	if (cell->y < height - 1 && !cells[cell->x][cell->y + 1].visitFlag)
		neighbours.push_back(&cells[cell->x][cell->y + 1]);

	// If there are no unvisited neighbours, then we're done here
	if (neighbours.empty())
		return nullptr;

	// Pick a random neighbour and return it
	return neighbours[rng() % neighbours.size()];
}

MazeBuilder::Cell *MazeBuilder::getRandomUnvisited(std::mt19937 &rng)
{
	int r = rng() % unvisited.size();
	int i = 0;
	for (auto *c : unvisited)
	{
		if (i == r)
			return c;
		i++;
	}
	return nullptr; // This should never happen
}

MazeBuilder::Cell *MazeBuilder::visitDFS(Cell *cell)
{
	cell->visitFlag = true;
	unvisited.erase(cell);
	return cell;
}

MazeBuilder::Cell *MazeBuilder::visitPrim(Cell *cell)
{
	if (cell->left)
		heap.push(cell->left);
	if (cell->right)
		heap.push(cell->right);
	if (cell->top)
		heap.push(cell->top);
	if (cell->bottom)
		heap.push(cell->bottom);
	cell->visitFlag = true;
	return cell;
}

void MazeBuilder::buildDepthFirst(int seed)
{
	std::stack<Cell*> visited;

	generateDFS();

	std::mt19937 rng;
	rng.seed(seed);

	// Start at cell 0,0
	auto *curr = visitDFS(&cells[0][0]);

	while (!unvisited.empty())
	{
		auto *chosen = getUnvisitedNeighbour(curr, rng);
		if (chosen)
		{
			visited.push(curr);
			chosen->adjacents.push_back(curr);
			curr->adjacents.push_back(chosen);
			curr = visitDFS(chosen);
		}
		else if (!visited.empty())
		{
			curr = visited.top();
			visited.pop();
		}
		else
		{
			// Never seems to be called. Could ignore it.
			curr = visitDFS(getRandomUnvisited(rng));
		}
	}

	unvisited.clear();

	buildGeneral();
}

void MazeBuilder::buildKruskal(int seed)
{
	generateKruskal();

	std::mt19937 rng;
	rng.seed(seed);

	// Randomize the walls
	std::shuffle(walls.begin(), walls.end(), rng);

	for (auto &wall : walls)
	{
		if (!wall.left || !wall.right)
			continue;

		if (wall.left->set.findParent() != wall.right->set.findParent())
		{
			Set::unionSets(&wall.left->set, &wall.right->set);
			wall.left->adjacents.push_back(wall.right);
			wall.right->adjacents.push_back(wall.left);

			// Not actually necessary to remove the wall, unless you want to draw walls instead of paths
		}
	}

	walls.clear();

	buildGeneral();
}

void MazeBuilder::buildPrim(int seed)
{
	std::mt19937 rng;
	rng.seed(seed);

	generatePrim(rng);

	// Start at cell 0,0
	visitPrim(&cells[0][0]);

	while (!heap.empty())
	{
		auto *wall = heap.pop();
		if (!wall->left || !wall->right)
			continue;

		auto *curr = wall->left->visitFlag ? wall->left : wall->right;
		auto *neighbour = wall->left == curr ? wall->right : wall->left;
		if (!neighbour->visitFlag)
		{
			neighbour->adjacents.push_back(curr);
			curr->adjacents.push_back(neighbour);
			visitPrim(neighbour);
		}
	}

	walls.clear();
	heap.clear();

	buildGeneral();
}

void MazeBuilder::buildGeneral()
{
	int p = 0;
	points.resize(width * height);

	std::set<std::pair<int, int>> edgeList;

	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++)
		{
			points[p++] = Point(Math::vec2((float) cells[x][y].x / (float) width * 2.0f - 1.0f, (float) cells[x][y].y / (float) height * 2.0f - 1.0f));
			for (auto *adj : cells[x][y].adjacents)
			{
				int adjPos = adj->x * width + adj->y;
				int cellPos = p - 1;

				if (edgeList.find({adjPos, cellPos}) == edgeList.end() && edgeList.find({cellPos, adjPos}) == edgeList.end())
				{
					edgeList.insert({cellPos, adjPos});
					edges.push_back(Edge(cellPos, adjPos));
				}
			}
		}
}

#if 0
void MazeBuilder::drawPoints()
{
	glPushAttrib(GL_CURRENT_BIT | GL_POINT_BIT);
	glPointSize(4);
	glColor3f(1, 0, 0);
	glBegin(GL_POINTS);
	for (auto &p : points)
		glVertex3f(p.p.x(), p.p.y(), 0);
	glEnd();
	glPopAttrib();
}

void MazeBuilder::drawEdges()
{
	glPushAttrib(GL_CURRENT_BIT);
	glColor3f(1, 1, 1);
	glBegin(GL_LINES);
	for (auto &e : edges)
	{
		auto &a = points[e.a];
		auto &b = points[e.b];
		glVertex3f(a.p.x(), a.p.y(), 0);
		glVertex3f(b.p.x(), b.p.y(), 0);
	}
	glEnd();
	glPopAttrib();
}

void MazeBuilder::draw()
{
	glPushAttrib(GL_TRANSFORM_BIT | GL_ENABLE_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glTranslatef(2.0f / (float) width * 0.5f, 2.0f / (float) height * 0.5f, 0);

	glDisable(GL_DEPTH_TEST);

	drawEdges();
	drawPoints();

	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
}
#endif
