#pragma once

#include <algorithm>
#include <stdexcept>

namespace Utils
{
	// Expects data of type T to overload the < operator.
	template <typename T>
	class BinaryHeap
	{
	public:
		class BinaryHeapIterator
		{
		private:
			BinaryHeap *heap;
			size_t pos;

		public:
			BinaryHeapIterator(BinaryHeap *heap, size_t pos = 1) : heap(heap), pos(pos) {}

			const BinaryHeapIterator &operator ++ () { pos++; return *this; }
			bool operator != (const BinaryHeapIterator &iter) { return pos != iter.pos; }
			T operator * () { return heap->data[pos]; }
		};

	private:
		T *data;
		size_t size;
		size_t endPos;

	private:
		void heapAdd(const size_t left, const size_t right)
		{
			if (right == 0)
				return;

			bool b = ref(data[left]) < ref(data[right]);

			if (b)
			{
				std::swap(data[left], data[right]);
				heapAdd(right, right >> 1);
			}
		}

		void heapRemove(size_t pos)
		{
			size_t left = pos * 2;
			size_t right = pos * 2 + 1;

			if (left < endPos && right < endPos)
			{
				if (ref(data[left]) < ref(data[pos]) || ref(data[right]) < ref(data[pos]))
				{
					if (ref(data[left]) < ref(data[right]))
					{
						std::swap(data[pos], data[left]);
						heapRemove(left);
					}
					else
					{
						std::swap(data[pos], data[right]);
						heapRemove(right);
					}
				}
			}
			else if (left < endPos && ref(data[left]) < ref(data[pos]))
			{
				std::swap(data[pos], data[left]);
				heapRemove(left);
			}
			else if (right < endPos && ref(data[right]) < ref(data[pos]))
			{
				std::swap(data[pos], data[right]);
				heapRemove(right);
			}
		}

		template <typename U>
		const U &ref(const U &t) const { return t; }

		template <typename U>
		const U &ref(U *t) const { return *t; }

	public:
		BinaryHeap(size_t size = 0) : data(nullptr), size(size), endPos(1)
		{
			if (this->size < 2)
				this->size = 2;
			data = (T*) calloc(this->size, sizeof(T));
		}

		BinaryHeap(const BinaryHeap &heap)
		{
			size = heap.size;
			endPos = heap.endPos;
			data = (T*) calloc(size, sizeof(T));
			if (data)
			{
				for (size_t i = 0; i < heap.size; i++)
					data[i] = heap.data[i];
			}
			else
			{
				size = 0;
				endPos = 1;
			}
		}

		~BinaryHeap()
		{
			free(data);
		}

		void clear()
		{
			free(data);

			size = 2;
			endPos = 1;

			data = (T*) calloc(size, sizeof(T));
		}

		size_t getCapacity() const { return size; }
		size_t getSize() const { return endPos - 1; }
		bool empty() const { return endPos == 1; }

		BinaryHeapIterator begin() { return BinaryHeapIterator(this, 1); }
		BinaryHeapIterator end() { return BinaryHeapIterator(this, endPos); }

		T top()
		{
			return data[1];
		}

		T push(T t)
		{
			data[endPos] = t;
			heapAdd(endPos, endPos >> 1);
			endPos++;

			if (endPos >= size)
			{
				size *= 2;
				data = (T*) realloc(data, sizeof(T) * size);
			}

			return t;
		}

		T pop()
		{
			if (endPos == 1)
				throw std::runtime_error("Cannot pop from empty Binary Heap");

			T t = data[1];
			data[1] = data[--endPos];
			data[endPos + 1] = 0;

			heapRemove(1);

			return t;
		}

		T operator [] (size_t pos) { return data[pos + 1]; }

		const BinaryHeap &operator = (const BinaryHeap &heap)
		{
			if (&heap == this)
				return *this;

			free(data);
			size = heap.size;
			endPos = heap.endPos;
			data = (T*) malloc(sizeof(T) * size);

			for (size_t i = 0; i < heap.size; i++)
				data[i] = heap.data[i];

			return *this;
		}
	};
}
