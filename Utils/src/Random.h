#pragma once

#include <random>
#include <cstdlib>

// TODO: lots of stuff!!!
namespace Utils
{
	void seedRand(unsigned int seed);

	template <typename T>
	T getRand(T min, T max)
	{
		double r = rand() / (double) RAND_MAX;
		r *= (max - min);
		r += min;
		return (T) r;
	}
}

