#pragma once

#include <vector>
#include <list>
#include <set>
#include <random>

#include "Set.h"
#include "BinaryHeap.h"

#include "../../Math/src/Vector/VectorCommon.h"

namespace Utils
{
	class MazeBuilder
	{
	public:
		class Point
		{
		public:
			Math::vec2 p;

		public:
			Point() {}
			Point(const Math::vec2 &p) : p(p) {}
		};

		class Edge
		{
		public:
			int a;
			int b;

		public:
			Edge(int a = 0, int b = 0) : a(a), b(b) {}
		};

	private:
		class Wall;

		class Cell
		{
		public:
			int x;
			int y;
			bool visitFlag; // DFS/Prim
			std::list<Cell*> adjacents; // All/pathfinding

			Set set; // Kruskal

			Wall *left; // Prim
			Wall *right; // Prim
			Wall *top; // Prim
			Wall *bottom; // Prim

			Cell(int x = 0, int y = 0) : x(x), y(y), visitFlag(false), left(nullptr), right(nullptr), top(nullptr), bottom(nullptr) {}
		};

		class Wall
		{
		public:
			Cell *left; // Or down
			Cell *right; // Or up
			unsigned int weight;
			Wall(Cell *left = nullptr, Cell *right = nullptr) : left(left), right(right), weight(0) {}

			bool operator < (const Wall &wall) const { return weight < wall.weight; }
		};

		int width;
		int height;

		std::vector<std::vector<Cell>> cells; // 2D Array
		std::set<Cell*> unvisited; // DFS
		std::vector<Wall> walls; // Kruskal/Prim
		BinaryHeap<Wall*> heap; // Prim

		// General stuff
		std::vector<Point> points;
		std::vector<Edge> edges;

	private:
		void generate(bool unvisitedFlag = false);
		void generateDFS(); // DFS
		void generateKruskal(); // Kruskal
		void generatePrim(std::mt19937 &rng); // Prim

		Cell *getUnvisitedNeighbour(Cell *cell, std::mt19937 &rng); // DFS
		Cell *getRandomUnvisited(std::mt19937 &rng); // DFS
		Cell *visitDFS(Cell *cell); // DFS

		Cell *visitPrim(Cell *cell); // Prim

		void buildGeneral();

	public:
		MazeBuilder(int width = 100, int height = 100) : width(width), height(height) {}

		void buildDepthFirst(int seed = 0);
		void buildKruskal(int seed = 0);
		void buildPrim(int seed = 0);
#if 0
		void drawPoints();
		void drawEdges();
		void draw();
#endif
		std::vector<Point> &getPoints() { return points; }
		std::vector<Edge> &getEdges() { return edges; }
	};
}
