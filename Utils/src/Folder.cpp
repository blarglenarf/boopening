#include "Folder.h"
#include "StringUtils.h"

#include <stdexcept>
#include <dirent.h>

using namespace Utils;

#include <iostream>

void Folder::load(const std::string &path)
{
	DIR *dir = opendir(path.c_str());
	if (!dir)
		throw std::runtime_error("Unable to find folder " + path);

	auto tokens = Utils::split(path, "/\\");
	if (tokens.size() == 1)
	{
		this->path = "./";
		this->name = path;
	}
	else
	{
		this->name = tokens[tokens.size() - 1];
		for (size_t i = 0; i < tokens.size() - 1; i++)
			this->path += tokens[i] + "/";
	}

	struct dirent *desc = readdir(dir);

	// Ignore "." and ".."
	for (int i = 0; i < 2; i++)
		desc = readdir(dir);

	while (desc)
	{
		switch (desc->d_type)
		{
		case DT_REG:
			files.push_back(desc->d_name);
			break;

		case DT_DIR:
			folders.push_back(Folder(path + "/" + desc->d_name));
			break;

		default:
			break;
		}
		desc = readdir(dir);
	}

	closedir(dir);
}
