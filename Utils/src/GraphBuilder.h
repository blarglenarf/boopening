#pragma once

#include "../../Math/src/Vector/VectorCommon.h"

#include <vector>

namespace Utils
{
	class GraphBuilder
	{
	private:
		enum class Mode
		{
			CUSTOM,
			DELAUNAY,
			URQUHART,
			GABRIEL,
			NEIGHBOR,
			MST
		};

	public:
		class Point
		{
		public:
			Math::vec2 p;

		public:
			Point() {}
			Point(const Math::vec2 &p) : p(p) {}

			static float cross(const Point &p1, const Point &p2, const Point &p3);
		};

		class Edge
		{
		public:
			int a;
			int b;

			int left;
			int right;

			float len;

			bool visibleFlag;

		public:
			Edge(float len = 0, int a = 0, int b = 0, int left = -1, int right = -1) : a(a), b(b), left(left), right(right), len(len), visibleFlag(true) {}

			bool operator < (const Edge &e) const { return len < e.len; }
		};

	private:
		class Circle
		{
		public:
			Point center;
			float radius;

		public:
			Circle() { radius = 0; }
			Circle(const Point &p1, const Point &p2);
			Circle(const Point &p1, const Point &p2, const Point &p3, float cp);
			Circle(const Point &center, float radius) : center(center), radius(radius) {}

			bool contains(const Point &p);
		};

		class Triangle
		{
		public:
			int edges[3];
			int points[3];

		public:
			Triangle(int e1 = 0, int e2 = 0, int e3 = 0) { edges[0] = e1; edges[1] = e2; edges[2] = e3; }
		};

	private:
		std::vector<Point> points;
		std::vector<Edge> edges;
		std::vector<Circle> circles;
		std::vector<Triangle> triangles;

		Mode mode;
		bool connectFlag;

	private:
		Edge findShortestEdge();
		int findEdge(int a, int b);
		int findEdge(const Math::vec2 &p, float margin = 0.05f);
		int findPoint(const Math::vec2 &p, float margin = 0.05f);

		void removePoint(int e);
		void removeEdge(int p);

		void addCircle(int e);
		int findLeftPoint(int a, int b, float &cp);
		int findBestPoint(int curr, int a, int b, float &cp, Circle &c);

		void updateFaceInfo(int p, int a, int b, int e, Circle &c);
		void updateEdgeInfo(int e, int a, int b, int c);

		void cleanupEdges();

	public:
		GraphBuilder() : mode(Mode::CUSTOM), connectFlag(false) {}

		void addPoint(const Math::vec2 &p);
		void addRandomPoints(int nPoints, const Math::vec2 &min, const Math::vec2 &max, int seed = 0);

		void addEdge(int p1, int p2);

		void buildDelaunayGraph();
		void buildUrquhartGraph();
		void buildGabrielGraph();
		void buildNeighborhoodGraph();
		void buildMST();
#if 0
		void drawPoints();
		void drawEdges();
		void drawCircles();

		void draw(bool identityFlag = true);
		void update(float dt, bool identityFlag = true);
#endif
		std::vector<Point> &getPoints() { return points; }
		std::vector<Edge> &getEdges() { return edges; }
	};
}
