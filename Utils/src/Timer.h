#pragma once

#include <chrono>

namespace Utils
{
	class Timer
	{
	private:
		std::chrono::time_point<std::chrono::system_clock> lastTime;

	public:
		Timer() { time(); }
		float time();
	};
}
