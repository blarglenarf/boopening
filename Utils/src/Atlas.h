#pragma once

#include "../../Math/src/Rect.h"

#include <vector>

namespace Utils
{
	// Stores pixel data and textue coordinates for multiple images. Can be treated the same as a
	// regular image. Uses the algorithm described here: http://www.blackpawn.com/texts/lightmaps/default.html.
	class Atlas
	{
	public:
		class AtlasRegion
		{
		private:
			Math::RectI rect;
			Math::RectF texRect;

			AtlasRegion *left;
			AtlasRegion *right;

			bool filled;

		public:
			AtlasRegion(Math::RectI rect, int maxWidth, int maxHeight);
			~AtlasRegion() { delete left; delete right; }

			AtlasRegion *insert(int width, int height, int maxWidth, int maxHeight);

			Math::RectI getRect() { return rect; }
			Math::RectF getTexRect() { return texRect; }

			AtlasRegion *getLeft() { return left; }
			AtlasRegion *getRight() { return right; }
		};

	private:
		std::vector<AtlasRegion*> regions;
		AtlasRegion *head;

	public:
		Atlas();
		Atlas(int width, int height);
		~Atlas();

		void init(int width, int height);

		size_t add(int width, int height);

		Math::RectF getTexCoords(size_t id);
		Math::RectI getRealCoords(size_t id);

		int getWidth();
		int getHeight();

		AtlasRegion *getHead() { return head; }
		void clear();
	};
}

