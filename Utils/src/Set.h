#pragma once

namespace Utils
{
	class Set
	{
	private:
		int rank;
		Set *parent;

	private:
		static Set *findParentRecursive(Set *s);

	public:
		Set() : rank(0), parent(this) {}

		Set *findParent() { return findParentRecursive(this); }

		static void unionSets(Set *s, Set *t);
	};
}
