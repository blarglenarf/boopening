#include "GraphBuilder.h"
#include "Set.h"
#include "Unused.h"

#include <algorithm>
#include <random>

using namespace Utils;

float GraphBuilder::Point::cross(const Point &p1, const Point &p2, const Point &p3)
{
	float u1 = p2.p.x - p1.p.x;
	float v1 = p2.p.y - p1.p.y;
	float u2 = p3.p.x - p1.p.x;
	float v2 = p3.p.y - p1.p.y;

	return u1 * v2 - v1 * u2;
}

GraphBuilder::Circle::Circle(const Point &p1, const Point &p2)
{
	center = (Math::vec2) ((p1.p + p2.p) / 2.0f);
	radius = center.p.distance(p1.p);
}

GraphBuilder::Circle::Circle(const Point &p1, const Point &p2, const Point &p3, float cp)
{
	float n = 2.0f * cp;
	float p1Sq = p1.p.lenSq();
	float p2Sq = p2.p.lenSq();
	float p3Sq = p3.p.lenSq();
	float cx = (p1Sq * (p2.p.y - p3.p.y) + p2Sq * (p3.p.y - p1.p.y) + p3Sq * (p1.p.y - p2.p.y)) / n;
	float cy = (p1Sq * (p3.p.x - p2.p.x) + p2Sq * (p1.p.x - p3.p.x) + p3Sq * (p2.p.x - p1.p.x)) / n;

	center = Math::vec2(cx, cy);
	radius = center.p.distance(p1.p);
}

bool GraphBuilder::Circle::contains(const Point &p)
{
	return center.p.distance(p.p) < radius;
}

GraphBuilder::Edge GraphBuilder::findShortestEdge()
{
	float min = -1;
	int a = 0, b = 0;

	for (int i = 0; i < (int) points.size() - 1; i++)
	{
		for (int j = 0; j < (int) points.size(); j++)
		{
			if (i == j)
				continue;
			auto d = points[i].p.distanceSq(points[j].p);
			if (d < min || min < 0)
			{
				min = d;
				a = i;
				b = j;
			}
		}
	}

	return Edge(min, a, b);
}

int GraphBuilder::findEdge(int a, int b)
{
	for (int i = 0; i < (int) edges.size(); i++)
		if (edges[i].a == a && edges[i].b == b)
			return i;
	return -1;
}

int GraphBuilder::findEdge(const Math::vec2 &p, float margin)
{
	for (int i = 0; i < (int) edges.size(); i++)
	{
		auto &p1 = points[edges[i].a].p;
		auto &p2 = points[edges[i].b].p;
		float u = ((p.x - p1.x) * (p2.x - p1.x) + (p.y - p1.y) * (p2.y - p1.y)) / (p1.distanceSq(p2));
		float x = p1.x + u * (p2.x - p1.x);
		float y = p1.y + u * (p2.y - p1.y);
		if (p.distance(Math::vec2(x, y)) <= margin)
			return i;
	}
	return -1;
}

int GraphBuilder::findPoint(const Math::vec2 &p, float margin)
{
	for (int i = 0; i < (int) points.size(); i++)
		if (points[i].p.distance(p) <= margin)
			return i;
	return -1;
}

void GraphBuilder::removePoint(int p)
{
	// Remove any edges connected to this point
	for (auto it = edges.begin(); it != edges.end();)
	{
		if (it->a == p || it->b == p)
			it = edges.erase(it);
		else
		{
			if (it->a > p)
				it->a--;
			if (it->b > p)
				it->b--;
			it++;
		}
	}

	auto it = points.begin();
	it += p;
	points.erase(it);
}

void GraphBuilder::removeEdge(int p)
{
	auto it = edges.begin();
	it += p;
	edges.erase(it);
}

void GraphBuilder::addCircle(int e)
{
	int a, b;

	auto &edge = edges[e];
	if (edge.left < 0)
	{
		a = edge.a;
		b = edge.b;
	}
	else if (edge.right < 0)
	{
		a = edge.b;
		b = edge.a;
	}
	else
		// Edge complete
		return;

	// Find point to connect edge to
	float cp;
	Circle c;
	int curr = findLeftPoint(a, b, cp);
	int best = findBestPoint(curr, a, b, cp, c);

	// Add the new data to the triangulation
	updateFaceInfo(best, a, b, e, c);
}

int GraphBuilder::findLeftPoint(int a, int b, float &cp)
{
	for (int i = 0; i < (int) points.size(); i++)
	{
		if (i == a || i == b)
			continue;
		cp = Point::cross(points[a], points[b], points[i]);
		if (cp > 0)
			return i;
	}

	return (int) points.size();
}

int GraphBuilder::findBestPoint(int curr, int a, int b, float &cp, Circle &c)
{
	if (curr >= (int) points.size())
		return curr;

	// Check for any points inside the circle
	c = Circle(points[a], points[b], points[curr], cp);

	for (int i = curr + 1; i < (int) points.size(); i++)
	{
		if (i == a || i == b)
			continue;

		cp = Point::cross(points[a], points[b], points[i]);
		if (cp > 0 && c.contains(points[i]))
		{
			curr = i;
			c = Circle(points[a], points[b], points[curr], cp);
		}
	}

	return curr;
}

void GraphBuilder::updateFaceInfo(int p, int a, int b, int e, Circle &c)
{
	if (p >= (int) points.size())
	{
		updateEdgeInfo(e, a, b, 0);
		return;
	}

	// Update face info
	updateEdgeInfo(e, a, b, (int) circles.size());
	circles.push_back(c);

	// Add new edge or update info of old edge for point a
	int e1 = findEdge(p, a);
	if (e1 < 0)
	{
		e1 = (int) edges.size();
		float len = points[p].p.distance(points[a].p);
		edges.push_back(Edge(len, p, a, (int) circles.size()));
	}
	else
		updateEdgeInfo(e1, p, a, (int) circles.size());

	// Add new edge or update info of old edge for point b
	int e2 = findEdge(b, p);
	if (e2 < 0)
	{
		e2 = (int) edges.size();
		float len = points[b].p.distance(points[p].p);
		edges.push_back(Edge(len, b, p, (int) circles.size()));
	}
	else
		updateEdgeInfo(e2, b, p, (int) circles.size());

	triangles.push_back(Triangle(e, e1, e2));
}

void GraphBuilder::updateEdgeInfo(int e, int a, int b, int c)
{
	UNUSED(b);

	auto &edge = edges[e];
	if (edge.a == a && edge.left < 0)
		edge.left = c;
	else if (edge.b == b && edge.right < 0) // TODO: check this... Possibly meant to be edge.b == a, not sure
		edge.right = c;
}

void GraphBuilder::cleanupEdges()
{
	for (auto it = edges.begin(); it != edges.end();)
	{
		if (!it->visibleFlag)
			it = edges.erase(it);
		else
			it++;
	}
}

void GraphBuilder::addPoint(const Math::vec2 &p)
{
	points.push_back(Point(p));
}

void GraphBuilder::addEdge(int p1, int p2)
{
	float len = points[p1].p.distance(points[p2].p);
	edges.push_back(Edge(len, p1, p2));
}

void GraphBuilder::addRandomPoints(int nPoints, const Math::vec2 &min, const Math::vec2 &max, int seed)
{
	std::mt19937 gen(seed);
	std::uniform_real_distribution<float> xVals(min.x, max.x);
	std::uniform_real_distribution<float> yVals(min.y, max.y);

	for (int i = 0; i < nPoints; i++)
	{
		auto x = xVals(gen);
		auto y = yVals(gen);
		addPoint(Math::vec2(x, y));
	}
}

void GraphBuilder::buildDelaunayGraph()
{
	mode = Mode::DELAUNAY;

	// FIXME: current complexity is O(n2), can be reduced to O(n log n) by using divide and conquer approach
	edges.clear();
	circles.clear();
	triangles.clear();

	// Less than two points, no need
	if (points.size() < 2)
		return;

	// Start by finding the closest neighbours, make them our first edge
	edges.push_back(findShortestEdge());

	// Go through all incomplete edges, and attempt to connect them with points in the graph
	for (int currEdge = 0; currEdge < (int) edges.size(); currEdge++)
	{
		if (edges[currEdge].left == -1)
			addCircle(currEdge);
		if (edges[currEdge].right == -1)
			addCircle(currEdge);
	}
}

void GraphBuilder::buildUrquhartGraph()
{
	buildDelaunayGraph();
	mode = Mode::URQUHART;

	// Go through all triangles in the graph, remove longest edge from each
	for (auto &tri : triangles)
	{
		auto l1 = edges[tri.edges[0]].len;
		auto l2 = edges[tri.edges[1]].len;
		auto l3 = edges[tri.edges[2]].len;

		if (l1 > l2 && l1 > l3)
			edges[tri.edges[0]].visibleFlag = false;
		else if (l2 > l1 && l2 > l3)
			edges[tri.edges[1]].visibleFlag = false;
		else
			edges[tri.edges[2]].visibleFlag = false;
	}

	cleanupEdges();
}

void GraphBuilder::buildGabrielGraph()
{
	buildDelaunayGraph();
	mode = Mode::GABRIEL;

	// Go through all triangles in the graph
	for (auto &tri : triangles)
	{
		int p1, p2, p3;
		p1 = edges[tri.edges[0]].a;
		p2 = edges[tri.edges[0]].b;
		if (p1 != edges[tri.edges[1]].a && p2 != edges[tri.edges[1]].a)
			p3 = edges[tri.edges[1]].a;
		else
			p3 = edges[tri.edges[1]].b;

		// Go through all edges in the triangle, if any point in the triangle lies inside the circumcircle of the edge, the edge is removed
		for (int i = 0; i < 3; i++)
		{
			auto &e = edges[tri.edges[i]];
			Circle c(points[e.a], points[e.b]);
			int p = p3;
			if (p1 != e.a && p1 != e.b)
				p = p1;
			else if (p2 != e.a && p2 != e.b)
				p = p2;

			if (c.contains(points[p]))
				e.visibleFlag = false;
		}
	}

	cleanupEdges();
}

void GraphBuilder::buildNeighborhoodGraph()
{
	// FIXME: Confirm that this is actually the relative neighborhood graph, so far it appears the same as urquhart
	buildDelaunayGraph();
	mode = Mode::NEIGHBOR;

	// Go through all triangles in the graph
	for (auto &tri : triangles)
	{
		int p1, p2, p3;
		p1 = edges[tri.edges[0]].a;
		p2 = edges[tri.edges[0]].b;
		if (p1 != edges[tri.edges[1]].a && p2 != edges[tri.edges[1]].a)
			p3 = edges[tri.edges[1]].a;
		else
			p3 = edges[tri.edges[1]].b;

		// Go through all edges in the triangle, if any point in the triangle is closer than the length of the edge, the edge is removed
		for (int i = 0; i < 3; i++)
		{
			auto &e = edges[tri.edges[i]];
			int p = p3;
			if (p1 != e.a && p1 != e.b)
				p = p1;
			else if (p2 != e.a && p2 != e.b)
				p = p2;

			float distA = points[p].p.distance(points[e.a].p);
			float distB = points[p].p.distance(points[e.b].p);

			if (distA < e.len && distB < e.len)
				e.visibleFlag = false;
		}
	}

	cleanupEdges();
}

void GraphBuilder::buildMST()
{
	buildDelaunayGraph();
	mode = Mode::MST;

	// Using Kruskal's atm, may switch to Prim's later if I feel like it
	std::vector<Set> sets(points.size());
	std::sort(edges.begin(), edges.end());

	for (auto &e : edges)
	{
		if (sets[e.a].findParent() != sets[e.b].findParent())
			Set::unionSets(&sets[e.a], &sets[e.b]);
		else
			e.visibleFlag = false;
	}

	cleanupEdges();
}
#if 0
void GraphBuilder::drawPoints()
{
	glPushAttrib(GL_CURRENT_BIT | GL_POINT_BIT);
	glPointSize(4);
	glColor3f(1, 0, 0);
	glBegin(GL_POINTS);
	for (auto &p : points)
		glVertex3f(p.p.x(), p.p.y(), 0);
	glEnd();
	glPopAttrib();
}

void GraphBuilder::drawEdges()
{
	glPushAttrib(GL_CURRENT_BIT);
	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	for (auto &e : edges)
	{
		auto &a = points[e.a];
		auto &b = points[e.b];
		glVertex3f(a.p.x(), a.p.y(), 0);
		glVertex3f(b.p.x(), b.p.y(), 0);
	}
	glEnd();
	glPopAttrib();
}

void GraphBuilder::drawCircles()
{
	glPushAttrib(GL_CURRENT_BIT | GL_POINT_BIT);
	for (auto &c : circles)
	{
		glPointSize(4);
		glColor3f(1, 1, 0);
		glBegin(GL_POINTS);
		glVertex3f(c.center.p.x(), c.center.p.y(), 0);
		glEnd();

		glColor3f(0, 1, 0);
		glBegin(GL_LINES);
		float step = 3.14159f / 20.0f;
		for (float theta = 0; theta < 2 * 3.14159f + step; theta += step)
		{
			glVertex3f(c.center.p.x() + c.radius * cosf(theta), c.center.p.y() + c.radius * sinf(theta), 0);
			glVertex3f(c.center.p.x() + c.radius * cosf(theta + step), c.center.p.y() + c.radius * sinf(theta + step), 0);
		}
		glEnd();
	}
	glPopAttrib();
}

void GraphBuilder::draw(bool identityFlag)
{
	glPushAttrib(GL_TRANSFORM_BIT);

	if (identityFlag)
	{
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
	}

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	drawPoints();
	drawEdges();

	glPopMatrix();

	if (identityFlag)
	{
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
	}

	glPopAttrib();
}

void GraphBuilder::update(float dt, bool identityFlag)
{
	UNUSED(dt);

	static int prevIndex = -1;

	Math::vec2f cursorPos((float) Platform::Mouse::x, (float) Platform::Mouse::y);

	if (identityFlag)
	{
		cursorPos.x() = cursorPos.x() / (float) Platform::Window::getWidth() * 2.0f - 1.0f;
		cursorPos.y() = cursorPos.y() / (float) Platform::Window::getHeight() * 2.0f - 1.0f;
	}

	if (Platform::Mouse::isPressed(Platform::Mouse::LEFT))
	{
		addPoint(cursorPos);
		connectFlag = false;

		switch (mode)
		{
		case Mode::DELAUNAY:
			buildDelaunayGraph();
			break;
		case Mode::URQUHART:
			buildUrquhartGraph();
			break;
		case Mode::GABRIEL:
			buildGabrielGraph();
			break;
		case Mode::NEIGHBOR:
			buildNeighborhoodGraph();
			break;
		case Mode::MST:
			buildMST();
			break;
		default:
			break;
		}
	}
	else if (Platform::Mouse::isPressed(Platform::Mouse::RIGHT))
	{
		// If a point has been clicked, connect it to the last point that was right-clicked (unless a point was added since then)
		if (connectFlag)
		{
			int index = findPoint(cursorPos);
			if (index != -1 && prevIndex != -1)
			{
				addEdge(index, prevIndex);
				prevIndex = index;
			}
			else if (index == -1)
				connectFlag = false;
		}
		else
		{
			connectFlag = true;
			prevIndex = findPoint(cursorPos);
		}
	}
	else if (Platform::Mouse::isPressed(Platform::Mouse::MIDDLE))
	{
		// If an edge was clicked, remove it
		int e = findEdge(cursorPos);
		if (e != -1)
			removeEdge(e);

		// If a point was clicked, remove it
		int p = findPoint(cursorPos);
		if (p != -1)
			removePoint(p);
	}
}
#endif
