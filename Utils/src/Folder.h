#pragma once

#include <vector>
#include <string>

namespace Utils
{
	class Folder
	{
	private:
		std::vector<std::string> files;
		std::vector<Folder> folders;

		std::string path;
		std::string name;

	public:
		Folder() {}
		Folder(const std::string &path) { load(path); }

		void load(const std::string &path);

		std::vector<std::string> &getFiles() { return files; }
		std::vector<Folder> &getFolders() { return folders; }
	};
}

