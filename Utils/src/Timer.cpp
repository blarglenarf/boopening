#include "Timer.h"

using namespace Utils;

float Timer::time()
{
	auto thisTime = std::chrono::system_clock::now();
	std::chrono::duration<float> deltaTime = thisTime - lastTime;
	lastTime = thisTime;
	return deltaTime.count() * 1000.0f;
}
