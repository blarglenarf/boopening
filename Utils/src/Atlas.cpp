#include "Atlas.h"

#include <queue>
#include <cassert>
#include <cstring>

using namespace Utils;

Atlas::AtlasRegion::AtlasRegion(Math::RectI rect, int maxWidth, int maxHeight) : rect(rect), left(nullptr), right(nullptr), filled(false)
{
	// FIXME: Why do I need to subtract 1 here?
	texRect.left() = (float) rect.left() / (float) maxWidth;
	texRect.right() = (float) (rect.right() - 1) / (float) maxWidth;
	texRect.bottom() = (float) rect.bottom() / (float) maxHeight;
	texRect.top() = (float) (rect.top() - 1) / (float) maxHeight;
}

Atlas::AtlasRegion *Atlas::AtlasRegion::insert(int width, int height, int maxWidth, int maxHeight)
{
	if (left || right)
	{
		AtlasRegion *newRegion = left->insert(width, height, maxWidth, maxHeight);
		if (newRegion)
			return newRegion;
		return right->insert(width, height, maxWidth, maxHeight);
	}
	else
	{
		// int border = 1;

		if (filled)
			return nullptr;

		if (rect.width() < width || rect.height() < height)
			return nullptr;

		if (rect.width() == width && rect.height() == height)
		{
			filled = true;
			return this;
		}
		else
		{
			// TODO: borders
			int dw = rect.width() - width;
			int dh = rect.height() - height;

			Math::RectI leftRect;
			Math::RectI rightRect;

			if (dw > dh)
			{
				leftRect = Math::RectI(rect.left(), rect.bottom(), rect.left() + width, rect.top());
				rightRect = Math::RectI(rect.left() + width, rect.bottom(), rect.right(), rect.top());
			}
			else
			{
				leftRect = Math::RectI(rect.left(), rect.bottom(), rect.right(), rect.bottom() + height);
				rightRect = Math::RectI(rect.left(), rect.bottom() + height, rect.right(), rect.top());
			}

			left = new AtlasRegion(leftRect, maxWidth, maxHeight);
			right = new AtlasRegion(rightRect, maxWidth, maxHeight);

			return left->insert(width, height, maxWidth, maxHeight);
		}
	}
}

Atlas::Atlas() : head(nullptr)
{
}

Atlas::Atlas(int width, int height) : head(nullptr)
{
	init(width, height);
}

Atlas::~Atlas()
{
	clear();
}

void Atlas::init(int width, int height)
{
	head = new AtlasRegion(Math::RectI(0, 0, width, height), width, height);
}

size_t Atlas::add(int width, int height)
{
	AtlasRegion *region = head->insert(width, height, head->getRect().width(), head->getRect().height());
	assert(region != nullptr);

	regions.push_back(region);
	return regions.size() - 1;
}

Math::RectF Atlas::getTexCoords(size_t id)
{
	return regions[id]->getTexRect();
}

Math::RectI Atlas::getRealCoords(size_t id)
{
	return regions[id]->getRect();
}

int Atlas::getWidth()
{
	if (head)
		return head->getRect().width();
	return 0;
}

int Atlas::getHeight()
{
	if (head)
		return head->getRect().height();
	return 0;
}

void Atlas::clear()
{
	delete head;
	head = nullptr;

	regions.clear();
}


