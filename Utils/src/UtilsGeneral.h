#pragma once

#include "../../Math/src/Vector/VectorCommon.h"

// TODO: lots of stuff!!!
namespace Utils
{
	int ceilLog2(int x);

	void printBinary(unsigned int i);

	unsigned int f32ToF16(float f);
	float f16ToF32(unsigned int i);

	unsigned int floatBitsToUint(float f);
	float uintBitsToFloat(unsigned int i);

	Math::vec4 uintToRGBA8(unsigned int i);
	Math::vec4 floatToRGBA8(float f);

	unsigned int rgba8ToUint(const Math::vec4 v);
	float rgba8ToFloat(const Math::vec4 &v);

	bool fileExists(const std::string &filename);
}

