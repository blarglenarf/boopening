#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <set>
#include <iostream>

namespace Utils
{
	class StringHash
	{
	private:
		std::set<std::string> hashes;

	public:
		bool existsStr(const std::string &str);
		bool existsHash(const std::string &str);
		std::string addStr(const std::string &str);
		std::string addHash(const std::string &str);
		std::string getHash(const std::string &str);
		void removeStr(const std::string &str);
		void removeHash(const std::string &str);
		void clear() { hashes.clear(); }
	};

	// Counts the number of characters c that appear in str.
	unsigned int count(const std::string &str, char c);

	// Checks whether char token is in string s.
	bool checkTokens(const std::string &s, char token);

	// Checks wether any char from string tokens is in string s.
	bool checkTokens(const std::string &s, const std::string &tokens);

	// Removes all instances of c from s.
	std::string removeChars(const std::string &s, char c);

	// Returns the extension from the given str (ie, str.obj would return obj).
	std::string getExtension(const std::string &str);

	// Converts any argument to a string, provided the given argument overloads the << operator
	template <typename T>
	std::string toString(T t)
	{
		std::stringstream ss;
		ss << t;
		return ss.str();
	}

	template <typename T>
	std::string toString(T t, size_t digits)
	{
		std::stringstream ss;
		ss << t;
		auto s = ss.str();
		std::string digitStr;
		while (s.length() > digits)
			s.pop_back();
		for (int i = 0; i < (int) digits - (int) s.length(); i++)
			digitStr += "0";
		return digitStr + s;
	}

	// Automatically strips carriage returns '\r' characters.
	std::istream &safeGetline(std::istream &is, std::string &t, char delim = '\n');

	// Returns a vector of strings, tokenised using the characters given in tokens.
	std::vector<std::string> split(const std::string &contents, const std::string &tokens);
	std::vector<std::string> split(const char *contents, size_t size, const std::string &tokens);

	// Removes any leading or trailing whitespace from the string.
	std::string trim(const std::string &str);

	// Compares two strings, returning true if they are the same (ignoring case).
	bool compareIgnoreCase(const std::string &str1, const std::string &str2);
	size_t findIgnoreCase(std::string str1, std::string str2);

	// Self explanatory.
	std::string tolower(std::string str);
	std::string toupper(std::string str);

	bool tobool(std::string str);

	bool beginsWith(const std::string &str, const std::string &comp);
	bool endsWith(const std::string &str, const std::string &comp);

	// Removes all characters from contents after the first point any character from tokens is encountered.
	std::string removeTrailing(const std::string &contents, const std::string &tokens);

	// Removes all comments from the string conents (eg /* and //).
	void removeComments(std::string &contents);

	std::string getFilePath(const std::string &path);
	std::string getBaseFilename(const std::string &path);
}

