#include "UtilsGeneral.h"

#include "../../Math/src/MathGeneral.h"

#include <cstring>
#include <string>
#include <iostream>
#include <fstream>
#include <bitset>

using namespace Utils;

#define WORDBITS (sizeof(int) * 8)

static unsigned int ones32(register unsigned int x)
{
	// 32-bit recursive reduction using SWAR... but first step is mapping 2-bit values into sum of 2 1-bit values in sneaky way.
	x -= ((x >> 1) & 0x55555555);
	x = (((x >> 2) & 0x33333333) + (x & 0x33333333));
	x = (((x >> 4) + x) & 0x0f0f0f0f);
	x += (x >> 8);
	x += (x >> 16);
	return(x & 0x0000003f);
}

int Utils::ceilLog2(int x)
{
	register int y = (x & (x - 1));
	y |= -y;
	y >>= (WORDBITS - 1);
	x |= (x >> 1);
	x |= (x >> 2);
	x |= (x >> 4);
	x |= (x >> 8);
	x |= (x >> 16);
	return(ones32(x) - 1 - y);
}

void Utils::printBinary(unsigned int i)
{
	std::bitset<32> b(i);
	std::cout << b << "\n";
}

#if 0
    class Float16Compressor
    {
        union Bits
        {
            float f;
            int32_t si;
            uint32_t ui;
        };

        static int const shift = 13;
        static int const shiftSign = 16;

        static int32_t const infN = 0x7F800000; // flt32 infinity
        static int32_t const maxN = 0x477FE000; // max flt16 normal as a flt32
        static int32_t const minN = 0x38800000; // min flt16 normal as a flt32
        static int32_t const signN = 0x80000000; // flt32 sign bit

        static int32_t const infC = infN >> shift;
        static int32_t const nanN = (infC + 1) << shift; // minimum flt16 nan as a flt32
        static int32_t const maxC = maxN >> shift;
        static int32_t const minC = minN >> shift;
        static int32_t const signC = signN >> shiftSign; // flt16 sign bit

        static int32_t const mulN = 0x52000000; // (1 << 23) / minN
        static int32_t const mulC = 0x33800000; // minN / (1 << (23 - shift))

        static int32_t const subC = 0x003FF; // max flt32 subnormal down shifted
        static int32_t const norC = 0x00400; // min flt32 normal down shifted

        static int32_t const maxD = infC - maxC - 1;
        static int32_t const minD = minC - subC - 1;

    public:

        static uint16_t compress(float value)
        {
            Bits v, s;
            v.f = value;
            uint32_t sign = v.si & signN;
            v.si ^= sign;
            sign >>= shiftSign; // logical shift
            s.si = mulN;
            s.si = s.f * v.f; // correct subnormals
            v.si ^= (s.si ^ v.si) & -(minN > v.si);
            v.si ^= (infN ^ v.si) & -((infN > v.si) & (v.si > maxN));
            v.si ^= (nanN ^ v.si) & -((nanN > v.si) & (v.si > infN));
            v.ui >>= shift; // logical shift
            v.si ^= ((v.si - maxD) ^ v.si) & -(v.si > maxC);
            v.si ^= ((v.si - minD) ^ v.si) & -(v.si > subC);
            return v.ui | sign;
        }

        static float decompress(uint16_t value)
        {
            Bits v;
            v.ui = value;
            int32_t sign = v.si & signC;
            v.si ^= sign;
            sign <<= shiftSign;
            v.si ^= ((v.si + minD) ^ v.si) & -(v.si > subC);
            v.si ^= ((v.si + maxD) ^ v.si) & -(v.si > maxC);
            Bits s;
            s.si = mulC;
            s.f *= v.si;
            int32_t mask = -(norC > v.si);
            v.si <<= shift;
            v.si ^= (s.si ^ v.si) & mask;
            v.si |= sign;
            return v.f;
        }
    };
#endif

// Conversion code from http://stackoverflow.com/questions/1659440/32-bit-to-16-bit-floating-point-conversion
unsigned int Utils::f32ToF16(float f)
{
	int v = (int) Utils::floatBitsToUint(f);
	unsigned int sign = v & 0x80000000;
	v ^= sign;
	sign >>= 16;
	int s = 0x52000000;
	float sf = Utils::uintBitsToFloat((unsigned int) s);
	float vf = Utils::uintBitsToFloat((unsigned int) v);
	s = (int) Utils::floatBitsToUint(sf * vf);
	v ^= (s ^ v) & -int(0x38800000 > v);
	v ^= (0x7F800000 ^ v) & -(int(0x7F800000 > v) & int(v > 0x477FE000));
	v ^= ((((0x7F800000 >> 13) + 1) << 13) ^ v) & -(int((((0x7F800000 >> 13) + 1) << 13) > v) & int(v > 0x7F800000));
	unsigned int vu = ((unsigned int) v) >> 13;
	v = (int) vu;
	v ^= ((v - ((0x7F800000 >> 13) - (0x477FE000 >> 13) - 1)) ^ v) & -int(v > (0x477FE000 >> 13));
	v ^= ((v - ((0x38800000 >> 13) - 0x003FF - 1)) ^ v) & -int(v > 0x003FF);
	return ((unsigned int) v) | sign;
}

float Utils::f16ToF32(unsigned int i)
{
	int v = i;
	int sign = v & (0x80000000 >> 16);
	v ^= sign;
	sign <<= 16;
	v ^= ((v + ((0x38800000 >> 13) - 0x003FF - 1)) ^ v) & -int(v > 0x003FF);
	v ^= ((v + ((0x7F800000 >> 13) - (0x477FE000 >> 13) - 1)) ^ v) & -int(v > (0x477FE000 >> 13));
	int s = 0x33800000;
	float sf = Utils::uintBitsToFloat((unsigned int) s);
	sf *= v;
	s = (int) Utils::floatBitsToUint(sf);
	int mask = -int(0x00400 > v);
	v <<= 13;
	v ^= (s ^ v) & mask;
	v |= sign;
	return Utils::uintBitsToFloat((unsigned int) v);
}

unsigned int Utils::floatBitsToUint(float f)
{
	unsigned int i = 0;
	memcpy(&i, &f, sizeof(f));
	return i;
}

float Utils::uintBitsToFloat(unsigned int i)
{
	float f = 0;
	memcpy(&f, &i, sizeof(f));
	return f;
}

Math::vec4 Utils::uintToRGBA8(unsigned int i)
{
	return Math::vec4(
		(float(i >> 24U)) / 255.0f,
		(float((i >> 16U) & 0xFFU)) / 255.0f,
		(float((i >> 8U) & 0xFFU)) / 255.0f,
		(float(i & 0xFFU)) / 255.0f
		);
}

Math::vec4 Utils::floatToRGBA8(float f)
{
	unsigned int i = floatBitsToUint(f);
	return uintToRGBA8(i);
}

unsigned int Utils::rgba8ToUint(const Math::vec4 v)
{
	Math::uvec4 i = Math::uvec4(v.x * 255.0f, v.y * 255.0f, v.z * 255.0f, v.w * 255.0f);
	i.x = Math::clamp(i.x, 0U, 255U);
	i.y = Math::clamp(i.y, 0U, 255U);
	i.z = Math::clamp(i.z, 0U, 255U);
	i.w = Math::clamp(i.w, 0U, 255U);
	return (i.x << 24U) | (i.y << 16U) | (i.z << 8U) | i.w;
}

float Utils::rgba8ToFloat(const Math::vec4 &v)
{
	unsigned int i = rgba8ToUint(v);
	return uintBitsToFloat(i);
}

bool Utils::fileExists(const std::string &filename)
{
	std::ifstream ifstr(filename);
	if (!ifstr.is_open())
		return false;
	ifstr.close();
	return true;
}

