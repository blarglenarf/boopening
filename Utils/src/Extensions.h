#pragma once

#include <vector>

namespace Utils
{
	// deleteAll function used to delete all elements in collection t and set their pointers to 0.
	template <typename T>
	void deleteAll(T &t)
	{
		for (auto *a : t)
		{
			delete a;
			a = nullptr;
		}
	}

	// deleteAll function used to delete all elements in an array of pointers and sets them to 0.
	template <typename T>
	void deleteAll(T &t, int size)
	{
		for (int i = 0; i < size; i++)
		{
			delete t[i];
			t[i] = nullptr;
		}
	}

	// forEach function used to execute a given lambda (u) on all elements in collection (t), eg:
	// Utils::forEach(vector, [](int i) -> void { print i; });
	template <typename T, typename U>
	void forEach(T &t, U u)
	{
		for (auto &a : t)
			u(a);
	}

	// forEach function used to execute a given lambda (u) on all elements in array (t), eg:
	// Utils::forEach(array, size, [](int i) -> void { print i; });
	template <typename T, typename U>
	void forEach(T t, int size, U u)
	{
		for (int i = 0; i < size; i++)
			u(t[i]);
	}

	// contains function used to determine wether a given object (u) exists in a given collection (t).
	template <typename T, typename U>
	bool contains(T &t, U u)
	{
		for (auto &a : t)
			if (a == u)
				return true;
		return false;
	}

	// remove function used to remove a given object (u) from a given collection (t). Returns true if anything has been removed.
	template <typename T, typename U>
	bool remove(T &t, U u)
	{
		bool removed = false;

		for (typename T::iterator it = t.begin(); it != t.end(); )
		{
			if (*it == u)
			{
				it = t.erase(it);
				removed = true;
			}
			else
				it++;
		}

		return removed;
	}

	template <typename T, typename U>
	std::vector<T*> find(T &t, U &u)
	{
		std::vector<T*> res;
		for (auto &x : t)
			if (x == u)
				res.push_back(&x);
		return res;
	}

	template <typename T, typename U, typename V>
	std::vector<T*> findIf(T &t, U &u, V v)
	{
		std::vector<T*> res;
		for (auto &x : t)
			if (v(x, u))
				res.push_back(&x);
		return res;
	}
}
