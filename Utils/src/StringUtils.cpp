#include "StringUtils.h"
#include "MD5.h"

#include <cstdio>
#include <stdexcept>
#include <ctype.h>

using namespace Utils;

static unsigned int tokenize(const char *contents, size_t size, const std::string &tokens, std::vector<std::string> *v)
{
	unsigned int numTokens = 0;
	size_t start = 0;

	for (size_t i = 0; i < size; i++)
	{
		if (Utils::checkTokens(tokens, contents[i]))
		{
			if (i == start) start = i + 1;
			else if (i > start)
			{
				if (v)
					v->push_back(std::string(&contents[start], i - start));
				start = i + 1;
				numTokens++;
			}
		}
	}

	if (start < size)
	{
		numTokens++;
		if (v)
			v->push_back(std::string(&contents[start], size - start));
	}

	return numTokens;
}



bool StringHash::existsStr(const std::string &str)
{
	MD5 md5;
	std::string hash = md5.digestString(str.c_str());
	return hashes.find(hash) != hashes.end();
}

bool StringHash::existsHash(const std::string &str)
{
	return hashes.find(str) != hashes.end();
}

std::string StringHash::addStr(const std::string &str)
{
	MD5 md5;
	std::string hash = md5.digestString(str.c_str());
	hashes.insert(hash);
	return hash;
}

std::string StringHash::addHash(const std::string &str)
{
	hashes.insert(str);
	return str;
}

std::string StringHash::getHash(const std::string &str)
{
	MD5 md5;
	return md5.digestString(str.c_str());
}

void StringHash::removeStr(const std::string &str)
{
	MD5 md5;
	std::string hash = md5.digestString(str.c_str());
	hashes.erase(hash);
}

void StringHash::removeHash(const std::string &str)
{
	hashes.erase(str);
}



unsigned int Utils::count(const std::string &str, char c)
{
	unsigned int count = 0;
	for (size_t i = 0; i < str.length(); i++)
		if (str[i] == c)
			count++;
	return count;
}

bool Utils::checkTokens(const std::string &s, char token)
{
	return s.find(token) != std::string::npos;
}

bool Utils::checkTokens(const std::string &s, const std::string &tokens)
{
	for (auto &token : tokens)
		if (checkTokens(s, token))
			return true;
	return false;
}

std::string Utils::removeChars(const std::string &s, char c)
{
	std::string str;
	for (size_t i = 0; i < s.length(); i++)
	{
		if (s[i] != c)
			str += s[i];
	}
	return str;
}

std::string Utils::getExtension(const std::string &str)
{
	size_t pos = str.rfind('.');
	if (pos == std::string::npos)
		throw std::runtime_error("Unable to get extension from " + str);

	return str.substr(pos + 1);
}
#if 0
float Utils::toFloat(const std::string &s)
{
	if (s == "")
		return 0;

	float f;
	if (sscanf(s.c_str(), "%f", &f) == EOF)
		throw std::runtime_error("Unable to convert " + s + " to float");
	return f;
}

double Utils::toDouble(const std::string &s)
{
	if (s == "")
		return 0;

	double d;
	if (sscanf(s.c_str(), "%lf", &d) == EOF)
		throw std::runtime_error("Unable to convert " + s + " to double");
	return d;
}

int Utils::toInt(const std::string &s)
{
	if (s == "")
		return 0;

	int i;
	if (sscanf(s.c_str(), "%d", &i) == EOF)
		throw std::runtime_error("Unable to convert " + s + " to int");
	return i;
}
#endif

// Details from: https://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf
std::istream &Utils::safeGetline(std::istream &is, std::string &t, char delim)
{
	t.clear();

	// This is for thread synchronisation. Not sure if we actually want this to happen or not.
	std::istream::sentry se(is, true);
	std::streambuf *sb = is.rdbuf();

	for (;;)
	{
		int c = sb->sbumpc();
		if ((char) c == delim)
			return is;
		else if ((char) c == '\r')
			continue;
		else if (c == EOF)
		{
			if (t.empty())
				is.setstate(std::ios::eofbit);
			return is;
		}
		t += (char) c; // FIXME: Not sure if this results in reallocs or not... should probably check.
	}
}

std::vector<std::string> Utils::split(const std::string &contents, const std::string &tokens)
{
	return split(contents.c_str(), contents.length(), tokens);
}

std::vector<std::string> Utils::split(const char *contents, size_t size, const std::string &tokens)
{
	int numTokens = tokenize(contents, size, tokens, 0);

	std::vector<std::string> v;
	v.reserve(numTokens);

	tokenize(contents, size, tokens, &v);

	return v;
}

std::string Utils::trim(const std::string &str)
{
	if (str.length() == 0)
		return str;

	size_t startPos = 0;
	while (isspace(str[startPos]) && startPos < str.length())
		startPos++;

	size_t endPos = str.length() - 1;
	while (isspace(str[endPos]) && endPos != (size_t) -1)
		endPos--;

	return str.substr(startPos, endPos - startPos + 1);
}

bool Utils::compareIgnoreCase(const std::string &str1, const std::string &str2)
{
	if (str1.length() != str2.length())
		return false;

	for (size_t i = 0; i < str1.length(); i++)
		if (::toupper(str1[i]) != ::toupper(str2[i]))
			return false;

	return true;
}

size_t Utils::findIgnoreCase(std::string str1, std::string str2)
{
	for (size_t i = 0; i < str1.length(); i++)
		str1[i] = ::toupper(str1[i]);
	for (size_t i = 0; i < str2.length(); i++)
		str2[i] = ::toupper(str2[i]);
	return str1.find(str2);
}

std::string Utils::tolower(std::string str)
{
	for (size_t i = 0; i < str.size(); i++)
		str[i] = ::tolower(str[i]);
	return str;
}

std::string Utils::toupper(std::string str)
{
	for (size_t i = 0; i < str.size(); i++)
		str[i] = ::toupper(str[i]);
	return str;
}

bool Utils::tobool(std::string str)
{
	if (str == "0")
		return false;
	if (str == "1")
		return true;
	return compareIgnoreCase(str, "true");
}

bool Utils::beginsWith(const std::string &str, const std::string &comp)
{
	if (comp.length() > str.length())
		return false;

	for (size_t i = 0; i < comp.length(); i++)
		if (str[i] != comp[i])
			return false;

	return true;
}

bool Utils::endsWith(const std::string &str, const std::string &comp)
{
	if (comp.length() > str.length())
		return false;

	for (size_t i = comp.length() - 1, j = str.length() - 1; i != (size_t) -1; i--, j--)
		if (str[j] != comp[i])
			return false;

	return true;
}

std::string Utils::removeTrailing(const std::string &contents, const std::string &tokens)
{
	std::string s;

	for (auto c : contents)
	{
		for (auto t : tokens)
			if (c == t)
				return s;
		s += c;
	}

	return s;
}

void Utils::removeComments(std::string &contents)
{
	if (contents.length() == 0)
		return;

	// FIXME: this is probably a little inefficient.
	for (size_t i = 0; i < contents.length() - 1; i++)
	{
		// Multi-line comment.
		if (contents[i] == '/' && contents[i + 1] == '*')
		{
			// Remove everything until the end comment tag.
			for (size_t j = i + 2; j < contents.length() - 1; j++)
			{
				if (contents[j] == '*' && contents[j + 1] == '/')
				{
					contents.erase(i, j + 1 - i);
					break;
				}
			}
		}

		// Single line comment.
		else if (contents[i] == '/' && contents[i + 1] == '/')
		{
			// Remove everything until the newline.
			for (size_t j = i + 2; j < contents.length(); j++)
			{
				if (contents[j] == '\n')
				{
					contents.erase(i, j - i);
					break;
				}
			}
		}
	}
}

std::string Utils::getFilePath(const std::string &path)
{
	if (path.empty())
		return "";
	size_t loc = path.find_last_of("/");
	if (loc == std::string::npos)
		return "";
	return path.substr(0, loc + 1);
}

std::string Utils::getBaseFilename(const std::string &path)
{
	if (path.empty())
		return "";
	size_t startLoc = path.find_last_of("/");
	size_t endLoc = path.find_last_of(".");
	if (startLoc == std::string::npos)
		startLoc = 0;
	else
		startLoc++;
	if (endLoc == std::string::npos)
		return path.substr(startLoc, path.size() - 1 - startLoc);
	return path.substr(startLoc, endLoc - startLoc);
}

