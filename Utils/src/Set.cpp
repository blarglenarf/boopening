#include "Set.h"

using namespace Utils;

Set *Set::findParentRecursive(Set *s)
{
	if (s->parent == s)
		return s;
	
	s->parent = findParentRecursive(s->parent);
	return s->parent;
}

void Set::unionSets(Set *s, Set *t)
{
	Set *sRoot = s->findParent();
	Set *tRoot = t->findParent();
	if (sRoot == tRoot)
		return;

	if (sRoot->rank < tRoot->rank)
		sRoot->parent = tRoot;
	else if (sRoot->rank > tRoot->rank)
		tRoot->parent = sRoot;
	else
	{
		tRoot->parent = sRoot;
		sRoot->rank += 1;
	}
}
