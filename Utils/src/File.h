#pragma once

#include "StringUtils.h"

#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cstring>
#include <iostream>
#include <stdexcept>

namespace Utils
{
	class TypeInfo
	{
	private:
		static std::map<std::string, size_t> typeSizes;

		std::string type;
		std::vector<std::pair<std::string, bool>> types;

	private:
		static void initDefaultTypeSizes();
		static void setTypeSize(const std::string &type, size_t size) { typeSizes[type] = size; }

	public:
		TypeInfo() {}
		TypeInfo(const std::string &data, size_t &curr);

		std::vector<std::pair<std::string, bool>> &getTypes() { return types; }
		const std::string &getType() const { return type; }

		static size_t getSize(const std::string &type) { return typeSizes[type]; }
		static bool exists(const std::string &type) { return typeSizes.find(type) != typeSizes.end(); }
	};



	class ObjectBuffer
	{
	private:
		static bool sysLittleEndianFlag;

		std::vector<char> data;
		size_t dataPos;
		int blockSize;

		std::string filename;
		std::ofstream ofstr;
		std::ifstream ifstr;

		bool fileLittleEndianFlag;
		bool firstReadFlag;

	private:
		void writeBlock();

	public:
		ObjectBuffer(int blockSize = 4096);

		void write(std::string *s);
		void write(char *c, size_t bytes);

		void read(std::string *s);
		void read(char *c, size_t bytes);

		void saveToDisk(bool closeFlag = true);
		void loadFromDisk(bool closeFlag = true);
		void clear() { dataPos = 0; ofstr.close(); ifstr.close(); firstReadFlag = true; }

		void setBlockSize(int blockSize) { this->blockSize = blockSize; if (blockSize > (int) data.size()) data.resize(blockSize); }
		int getBlockSize() const { return blockSize; }

		void setFilename(const std::string &filename) { this->filename = filename; }
		const std::string &getFilename() const { return filename; }

		std::ifstream &getInputStream() { return ifstr; }
		std::ofstream &getOutputStream() { return ofstr; }
	};



	class File
	{
	public:
		// Note: this will automatically ignore any carriage returns '\r' in the text.
		class TextIterator
		{
		private:
			std::string *str;
			std::ifstream *ifstr;
			char delim;

		private:
			void reset() { ifstr->close(); str = nullptr; ifstr = nullptr; delim = '\n'; }
			//void increment() { if (!std::getline(*ifstr, *str, delim)) reset(); }
			void increment() { if (!Utils::safeGetline(*ifstr, *str, delim)) reset(); }

		public:
			TextIterator(std::string *str = nullptr, std::ifstream *ifstr = nullptr, char delim = '\n') : str(str), ifstr(ifstr), delim(delim) { if (ifstr) increment(); }

			std::string &operator * () const { return *str; }
			bool operator != (const TextIterator &iter) { return str != iter.str; }
			const TextIterator &operator ++ () { increment(); return *this; }
			const TextIterator &operator ++ (int) { increment(); return *this; }
		};

		class TextRange
		{
		private:
			std::ifstream *ifstr;
			std::string data;
			char delim;

		public:
			TextRange(std::ifstream *ifstr, char delim = '\n') : ifstr(ifstr), delim(delim) {}

			TextIterator begin() { return TextIterator(&data, ifstr, delim); }
			TextIterator end() { return TextIterator(); }
		};

	private:
		std::map<std::string, TypeInfo> infoTable;
		std::vector<std::pair<std::string, char>> fileFormat;

		ObjectBuffer buffer;
		size_t currBlock;
		std::string line;

	private:
		//template <typename T>
		//T &ref(T &t) { return t; }

		//template <typename T>
		//T &ref(T *t) { return *t; }

		void saveToDisk();
		void loadFromDisk();

		template <typename T>
		void writeToBuffer(T &t, TypeInfo &info)
		{
			size_t objPos = 0, bytes = 0;

			for (auto &type : info.getTypes())
			{
				// variable size data
				if (type.first == "string" && type.second)
				{
					buffer.write((std::string*) ((char*) (&t) + objPos));
					objPos += sizeof(std::string);
					continue;
				}

				// fixed size data
				bytes = info.getSize(type.first);
				if (type.second)
					buffer.write((char*) (&t) + objPos, bytes);
				objPos += bytes;
			}
		}

		template <typename T>
		void readFromBuffer(T &t, TypeInfo &info)
		{
			size_t objPos = 0, bytes = 0;

			for (auto &type : info.getTypes())
			{
				// variable size data
				if (type.first == "string" && type.second)
				{
					buffer.read((std::string*) ((char*) (&t) + objPos));
					objPos += sizeof(std::string);
					continue;
				}

				// fixed size data
				bytes = info.getSize(type.first);
				if (type.second)
					buffer.read((char*) (&t) + objPos, bytes);
				objPos += bytes;
			}
		}

		template <typename T>
		void saveObj(T &t)
		{
			if (currBlock >= fileFormat.size())
				throw std::runtime_error("Too many args supplied to save");

			auto &block = fileFormat[currBlock++];
			auto &type = block.first;
			bool manyFlag = block.second == 'm';

			if (manyFlag)
				throw std::runtime_error("Unable to save many objects required by format");
			else
			{
				// Simple data, single object
				if (TypeInfo::exists(type))
				{
					if (type == "string")
						buffer.write((std::string*) &t);
					else
						buffer.write((char*) &t, sizeof(t));
				}

				// Complex data, single object
				else
					writeToBuffer(t, infoTable[type]);
			}
		}

		template <typename T>
		void saveObj(std::vector<T> &t)
		{
			if (currBlock >= fileFormat.size())
				throw std::runtime_error("Too many args supplied to save");

			auto &block = fileFormat[currBlock++];
			auto &type = block.first;
			bool manyFlag = block.second == 'm';

			if (manyFlag)
			{
				// Simple data, many objects
				if (TypeInfo::exists(type))
				{
					size_t n = t.size();
					buffer.write((char*) &n, sizeof(n));
					if (type == "string")
						for (auto &str : t)
							buffer.write((std::string*) &str);
					else
						for (auto val : t)
							buffer.write((char*) &val, sizeof(val));
				}

				// Complex data, many objects
				else
				{
					size_t n = t.size();
					buffer.write((char*) &n, sizeof(n));
					for (auto &obj : t)
						writeToBuffer(obj, infoTable[type]);
				}
			}
			else
				throw std::runtime_error("Unable to save single object required by format");//writeSingle(t, block.first);
		}

		template <typename T>
		void loadObj(T &t)
		{
			if (currBlock >= fileFormat.size())
				throw std::runtime_error("Too many args supplied to load");

			auto &block = fileFormat[currBlock++];
			auto &type = block.first;
			bool manyFlag = block.second == 'm';

			if (manyFlag)
				throw std::runtime_error("Unable to load many objects required by format");
			else
			{
				// Simple data, single object
				if (TypeInfo::exists(type))
				{
					if (type == "string")
						buffer.read((std::string*) &t);
					else
						buffer.read((char*) &t, sizeof(t));
				}

				// Complex data, single object
				else
					readFromBuffer(t, infoTable[type]);
			}
		}

		template <typename T>
		void loadObj(std::vector<T> &t)
		{
			if (currBlock >= fileFormat.size())
				throw std::runtime_error("Too many args supplied to load");

			auto &block = fileFormat[currBlock++];
			auto &type = block.first;
			bool manyFlag = block.second == 'm';

			if (manyFlag)
			{
				// Simple data, many objects
				if (TypeInfo::exists(type))
				{
					size_t n = 0;
					buffer.read((char*) &n, sizeof(n));
					t.resize(n);
					if (type == "string")
						for (auto &str : t)
							buffer.read((std::string*) &str);
					else
						for (auto &val : t)
							buffer.read((char*) &val, sizeof(val));
				}

				// Complex data, many objects
				else
				{
					size_t n = 0;
					buffer.read((char*) &n, sizeof(n));
					t.resize(n);
					for (auto &obj : t)
						readFromBuffer(obj, infoTable[type]);
				}
			}
			else
				throw std::runtime_error("Unable to load single object required by format");//readSingle(t, block.first);
		}

	public:
		File(const std::string &filename = "", const std::string &infoStr = "");

		void setFormat(const std::string &infoStr);

		void setFilename(const std::string &filename) { buffer.setFilename(filename); }
		const std::string &getFilename() const { return buffer.getFilename(); }

		template <typename T>
		void save(T &t)
		{
			saveObj(t);
			saveToDisk();
		}

		template <typename First, typename ... Rest>
		void save(First &first, Rest &...rest)
		{
			saveObj(first);
			save(rest...);
		}

		template <typename T>
		void load(T &t)
		{
			loadObj(t);
			buffer.clear();
		}

		template <typename First, typename ... Rest>
		void load(First &first, Rest &...rest)
		{
			loadObj(first);
			load(rest...);
		}

		void setBlockSize(int blockSize) { buffer.setBlockSize(blockSize); }
		void close() { buffer.clear(); }

		TextRange getLines(char delim = '\n');
		std::string getStr();

		void write(const std::vector<std::string> &strs, char delim = '\n', bool append = false);
		void write(const std::string &str, char delim = '\n', bool append = false);

		// Note: using this to iterate over the file will cause it to stay open for scope duration.
		TextIterator begin();
		TextIterator end();

		static size_t fileSize(const std::string &filename);
		size_t size() { return fileSize(buffer.getFilename()); }

		static bool exists(const std::string &filename);
	};
}

