#include "../../Platform/src/PlatformCommon.h"

#if _WIN32
#include <Windows.h>
#endif
#include <GL/gl.h>
#include <cstdlib>

using namespace Platform;

#include <iostream>

static void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, Window::getWidth(), Window::getHeight());

	glColor3f(0.8f, 0, 0);
	glBegin(GL_QUADS);
	glVertex2f(-0.5f, -0.5f);
	glVertex2f(0.5f, -0.5f);
	glVertex2f(0.5f, 0.5f);
	glVertex2f(-0.5f, 0.5f);
	glEnd();
}

int main(int argc, char **argv)
{
	(void) (argc);
	(void) (argv);

	std::cerr << "Note: last time I ran this on linux it worked!\n";
	
	std::cerr << "Here!\n";

	Window::createWindow(512, 512);

	std::cerr << "And here!\n";

	bool finished = false;
	while (!finished)
	{
		float dt = Window::getDeltaTime();
		finished = Window::handleEvents();

		render();

		Window::swapBuffers();

		std::cerr << "Looping! " << dt << "\n";
	}

	Window::destroyWindow();

	return EXIT_SUCCESS;
}
