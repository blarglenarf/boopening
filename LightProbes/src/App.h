#pragma once

#define GRID_CELL_SIZE 16
#define CLUSTERS 32
#define MAX_EYE_Z 30.0
#define MASK_SIZE (CLUSTERS / 32)

#include "Lighting/Lighting.h"

#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/RenderResource/Mesh.h"
#include "../../Renderer/src/Lighting/Bloom.h"
#include "../../Renderer/src/Profiler.h"
#include "../../Renderer/src/Camera.h"

#include "../../Math/src/Vector/Vec4.h"

#include <vector>

class App
{
private:
	int currMesh;
	int currLighting;

	bool hudFlag;

	Rendering::Shader basicShader;

	Rendering::GPUMesh gpuMesh;

	Rendering::Profiler profiler;

	Lighting *lighting;

	int width;
	int height;

	int tmpWidth;
	int tmpHeight;

public:
	App() : currMesh(0), currLighting(0), hudFlag(true), lighting(nullptr), width(0), height(0), tmpWidth(0), tmpHeight(0) {}
	~App() {}

	void init();

	void setTmpViewport(int width, int height);
	void restoreViewport();

	void drawMesh();
	void drawMesh(Rendering::Camera& camera);

	void setCameraUniforms(Rendering::Shader* shader);

	void render();
	void update(float dt);

	int useMesh(int currMesh);
	void loadNextMesh();
	void loadPrevMesh();

	int useLighting(int currLighting);
	void useNextLighting();
	void usePrevLighting();

	void setLightPos();

	void reloadShaders();
	void saveData();

	void playAnim();
	void runBenchmarks();

	void saveImg();

	Rendering::GPUMesh& getGpuMesh() { return gpuMesh; }
	Rendering::Profiler& getProfiler() { return profiler; }

	void toggleHud() { hudFlag = !hudFlag; }
};

