#include <cstdlib>

#include "../App.h"

#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Platform/src/PlatformCommon.h"

#include <iostream>

#define UNUSED(x) (void)(x)

#define USE_FBO 0
#define USE_4K 0

using namespace Rendering;
using namespace Platform;

/*

Camera stuff

Atrium:

Balcony
type: PERSPECTIVE
pos: -0.5,-0.8,0
euler rot: -0.209439,0,0
quat rot: 0, 0, -0.104528, 0.994522
zoom: 6.68847
fov/near/far: 1.309, 0.01, 1000
viewport: 0,0,512,512


Outside
type: PERSPECTIVE
pos: 1.1,-1.95,0
euler rot: -0.392698,-0.366519,0
quat rot: 0.0355523, -0.178734, -0.191823, 0.964362
zoom: 23.4493
fov/near/far: 1.309, 0.01, 30
viewport: 0,0,512,512



Hairball
type: PERSPECTIVE
pos: -0.5,-0.8,0
euler rot: -0.0785394,-0.00872635,0
quat rot: 0.000171296, -0.0043598, -0.0392592, 0.99922
zoom: 16.4913
fov/near/far: 1.309, 0.01, 1000
viewport: 0,0,512,512



*/

// TODO: Resize the fbo when the window size changes perhaps?
#if USE_FBO
	#if USE_4K
		// 4k fbo.
		static int fboWidth = 4096;
		static int fboHeight = 2160;
	#else
		// 2k fbo
		static int fboWidth = 512;
		static int fboHeight = 512;
	#endif
#endif

bool wireframeFlag = false;


static void render(App &app)
{
#if USE_FBO
	Renderer::instance().getActiveCamera().setViewport(0, 0, fboWidth, fboHeight);
#else
	Renderer::instance().getActiveCamera().setViewport(0, 0, Window::getWidth(), Window::getHeight());
#endif
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glLoadIdentity();
	Renderer::instance().getActiveCamera().upload();

	app.render();
}

static void update(App &app, float dt)
{
	app.update(dt);

	auto &camera = Renderer::instance().getActiveCamera();
	camera.update(dt);
	//Renderer::instance().getActiveCamera().update(dt);

	if (Keyboard::isDown(Keyboard::w))
		camera.moveForward(dt);
	if (Keyboard::isDown(Keyboard::s))
		camera.moveBackward(dt);
	if (Keyboard::isDown(Keyboard::a))
		camera.moveLeft(dt);
	if (Keyboard::isDown(Keyboard::d))
		camera.moveRight(dt);
}

static void toggleWireframe()
{
	wireframeFlag = !wireframeFlag;
	if (wireframeFlag)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

static bool mouseDrag(Event &ev)
{
	if (Mouse::isDown(Mouse::LEFT))
		Renderer::instance().getActiveCamera().updateRotation((float) ev.dx, (float) ev.dy);
	if (Mouse::isDown(Mouse::RIGHT))
		Renderer::instance().getActiveCamera().updateZoom((float) ev.dy);
	if (Mouse::isDown(Mouse::MIDDLE))
		Renderer::instance().getActiveCamera().updatePan((float) ev.dx, (float) ev.dy);
	return false;
}

static bool keyPress(App &app, Event &ev)
{
	switch (ev.key)
	{
	case Keyboard::ESCAPE:
	case Keyboard::q:
		exit(EXIT_SUCCESS);
		break;

	case Keyboard::p:
		//toggleWireframe();
		app.saveImg();
		break;

	case Keyboard::LEFT:
		app.loadPrevMesh();
		break;

	case Keyboard::RIGHT:
		app.loadNextMesh();
		break;

	case Keyboard::UP:
		app.useNextLighting();
		break;

	case Keyboard::DOWN:
		app.usePrevLighting();
		break;

	case Keyboard::l:
		app.setLightPos();
		break;

	case Keyboard::b:
		app.runBenchmarks();
		//app.playAnim();
		break;

	case Keyboard::c:
		std::cout << Renderer::instance().getActiveCamera() << "\n";
		break;

	case Keyboard::r:
		app.reloadShaders();
		break;

	case Keyboard::o:
		app.saveData();
		break;

	case Keyboard::h:
		app.toggleHud();
		break;

	default:
		break;
	}

	return false;
}


int main(int argc, char **argv)
{
	UNUSED(argc);
	UNUSED(argv);

	// Cleanup code.
	std::atexit([]()-> void {
		Rendering::Renderer::instance().clear();
		Window::destroyWindow();
	});

#if !USE_FBO
	Window::createWindow(0, 0, 512, 512);
#else
	Window::createWindow(0, 0, fboWidth, fboHeight);
#endif
	Renderer::instance().init();

	App app;
	auto &camera = Renderer::instance().addCamera("main", Camera(Camera::Type::PERSP));

#if USE_FBO
	Texture2D colorBuffer;
	RenderBuffer depthBuffer;
	FrameBuffer fbo(fboWidth, fboHeight);

	fbo.create(GL_RGBA, &colorBuffer, &depthBuffer, true);
#endif

#if 0
	#if 0
		// Planes front.
		camera.setZoom(2.0f);
		camera.update(0.1f);
	#else
		// Planes back.
		camera.setEulerRot({0.0f, -Math::pi, 0.0f});
		camera.setZoom(3.5f);
		camera.update(0.1f);
	#endif
#else
	#if 1
		// Atrium balcony view.
		camera.setPos({-0.5f, -0.8f, 0.0f});
		camera.setEulerRot({-0.209439f, 0.0f, 0.0f});
		camera.setZoom(6.68847f);
		camera.update(0.1f);
	#elif 0
		// Atrium outside ortho z view (only works at 1024 res).
		camera.setType(Camera::Type::ORTHO);
		camera.setDistance(0, 1024);
		camera.setPos({-10, -10, 10});
		camera.setZoom(51.2);
		camera.update(0.1f);
	#else
		// Powerplant view.
		camera.setPos({-4,5.75,0});
		camera.setEulerRot({0.00872726,1.54461,0});
		camera.setZoom(8.93749);
		camera.update(0.1f);
	#endif
#endif

	// Hairball light view.
#if 0
	camera.setPos({-0.1f, 0.05f, 0.0f});
	camera.setEulerRot({-0.235619f, 0.0349003f ,0});
	camera.setZoom(11.0367f);
	camera.update(0.1f);
#endif

#if 0
	EventHandler::addHandler(Event::Type::KEY_PRESSED, &keyPress);
	EventHandler::addHandler(Event::Type::MOUSE_DRAG, &mouseDrag);
#endif

	app.init();

	bool finished = false;
	while (!finished)
	{
		float dt = Window::getDeltaTime();

		// First approach for event handling, using callback functions provided above.
		//finished = Window::handleEvents();
#if 1
		// Alternative approach, loop through the events and handle each in the switch statement.
		Window::processEvents();
		for (auto &ev : EventHandler::events)
		{
			switch (ev.type)
			{
			case Event::Type::WINDOW_RESIZED:
				Platform::Window::setWindowSize(ev);
				break;

			case Event::Type::KEY_PRESSED:
				keyPress(app, ev);
				break;

			case Event::Type::MOUSE_DRAG:
				mouseDrag(ev);
				break;

			default:
				break;
			}
		}
#endif
#if 0
		// Yet another alternative approach, just ask the keyboard and mouse if something has happened.
		Window::processEvents();
		if (Keyboard::isPressed(Keyboard::ESCAPE) || Keyboard::isPressed(Keyboard::q))
			exit(EXIT_SUCCESS);
		if (Keyboard::isPressed(Keyboard::p))
		{
			DebugControls::wireframeFlag = !DebugControls::wireframeFlag;
			DebugControls::toggleWireframe();
		}
		if (Keyboard::isPressed(Keyboard::c))
		{
			DebugControls::cullFlag = !DebugControls::cullFlag;
			DebugControls::toggleCulling();
		}
		if (Mouse::isDown(Mouse::LEFT))
			Renderer::instance().getActiveCamera().updateRotation(Mouse::dx, Mouse::dy);
		if (Mouse::isDown(Mouse::RIGHT))
			Renderer::instance().getActiveCamera().updateZoom(Mouse::dy);
		if (Mouse::isDown(Mouse::MIDDLE))
			Renderer::instance().getActiveCamera().updatePan(Mouse::dx, Mouse::dy);
#endif
		update(app, dt);

	#if USE_FBO
		fbo.bind();
	#endif
		render(app);
	#if USE_FBO
		fbo.unbind();
		fbo.blit();
	#endif

		CHECKERRORS;

		Window::swapBuffers();
	}

	Window::destroyWindow();

	return EXIT_SUCCESS;
}

