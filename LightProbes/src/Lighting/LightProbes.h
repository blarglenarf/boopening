#pragma once

#include "Lighting.h"

#include "../../../Renderer/src/RenderObject/Shader.h"

class LightProbes : public Lighting
{
private:
	//Rendering::Bloom bloom;

	Rendering::Shader basicShader;

public:
	LightProbes() : Lighting() {}
	virtual ~LightProbes() {}

	virtual void init(App *app) override { (void) (app); }

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLighting(App *app) override;

	virtual void reloadShaders(App *app) override;
};

