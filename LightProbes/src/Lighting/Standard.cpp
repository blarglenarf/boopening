#include "Standard.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

using namespace Rendering;


void Standard::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	//bloom.resize(camera.getWidth(), camera.getHeight());

	//bloom.beginCapture();

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	basicShader.bind();
	basicShader.setUniform("mvMatrix", camera.getInverse());
	basicShader.setUniform("pMatrix", camera.getProjection());
	basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	basicShader.setUniform("viewport", camera.getViewport());
	basicShader.setUniform("invPMatrix", camera.getInverseProj());
	app->getGpuMesh().render();
	basicShader.unbind();

	glPopAttrib();

	//bloom.endCapture();

	// Blur the bright texture and composite.
	//bloom.applyBlur();
	//bloom.composite();

	//bloom.debugRenderColor();
	//bloom.debugRenderBright();
	//bloom.debugRenderBlur(3);
}

void Standard::update(float dt)
{
	(void) (dt);
}

void Standard::useLighting(App *app)
{
	reloadShaders(app);
}

void Standard::reloadShaders(App *app)
{
	(void) (app);

	auto vertBasic = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto fragBasic = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.create(&vertBasic, &fragBasic);
}

