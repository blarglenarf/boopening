#pragma once

class App;

class Lighting
{
public:
	enum Type
	{
		STANDARD,
		LIGHT_PROBES,
	};

public:
	Lighting() {}
	virtual ~Lighting() {}

	virtual void init(App* app) = 0;

	virtual void render(App* app) = 0;
	virtual void update(float dt) = 0;

	virtual void useLighting(App* app) = 0;

	virtual void setLightPos(App* app) { (void) (app); }

	virtual void reloadShaders(App* app) = 0;
	virtual void saveData(App* app) { (void) (app); }
};

