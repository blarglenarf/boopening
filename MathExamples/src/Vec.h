#pragma once

#if 0

#include "FixedPoint.h"

#include <cmath>

namespace Math
{
	template <typename T, int size>
	class vec
	{
	public:
		T t[size];

	public:
		vec()
		{
			for (int i = 0; i < size; i++)
				t[i] = 0;
		}

		vec(T t)
		{
			for (int i = 0; i < size; i++)
				this->t[i] = t;
		}

		vec(T *t)
		{
			for (int i = 0; i < size; i++)
				this->t[i] = t[i];
		}

		vec(const vec &v)
		{
			for (int i = 0; i < size; i++)
				t[i] = v.t[i];
		}

		vec &operator = (const vec &v)
		{
			for (int i = 0; i < size; i++)
				t[i] = v.t[i];
			return *this;
		}

		vec operator + (const vec &v1) const
		{
			vec v;
			for (int i = 0; i < size; i++)
				v.t[i] = t[i] + v1.t[i];
			return v;
		}

		vec &operator += (const vec &v1)
		{
			for (int i = 0; i < size; i++)
				t[i] += v1.t[i];
			return *this;
		}

		vec &operator - ()
		{
			for (int i = 0; i < size; i++)
				t[i] = -t[i];
			return *this;
		}

		vec operator - (const vec &v1) const
		{
			vec v;
			for (int i = 0; i < size; i++)
				v.t[i] = t[i] - v1.t[i];
			return v;
		}

		vec &operator -= (const vec &v1)
		{
			for (int i = 0; i < size; i++)
				t[i] -= v1.t[i];
			return *this;
		}

		vec operator * (const vec &v1) const
		{
			vec v;
			for (int i = 0; i < size; i++)
				v.t[i] = t[i] * v1.t[i];
			return v;
		}

		vec operator * (T val) const
		{
			vec v;
			for (int i = 0; i < size; i++)
				v.t[i] = t[i] * val;
			return v;
		}

		vec &operator *= (const vec &v1)
		{
			for (int i = 0; i < size; i++)
				t[i] *= v1.t[i];
			return *this;
		}

		vec &operator *= (T val)
		{
			for (int i = 0; i < size; i++)
				t[i] *= val;
			return *this;
		}

		vec operator / (const vec &v1) const
		{
			vec v;
			for (int i = 0; i < size; i++)
				v.t[i] = t[i] / v1.t[i];
			return v;
		}

		vec operator / (T val) const
		{
			vec v;
			for (int i = 0; i < size; i++)
				v.t[i] = t[i] / val;
			return v;
		}

		void operator /= (const vec &v1)
		{
			for (int i = 0; i < size; i++)
				t[i] /= v1.t[i];
		}

		void operator /= (T val)
		{
			for (int i = 0; i < size; i++)
				t[i] /= val;
		}

		bool operator == (const vec &v1) const
		{
			for (int i = 0; i < size; i++)
				if (t[i] != v1.t[i])
					return false;
			return true;
		}

		bool operator != (const vec &v1) const
		{
			return !(*this == v1);
		}

		static T dot(const vec &v1, const vec &v2)
		{
			T val = 0;
			for (int i = 0; i < size; i++)
				val += v1.t[i] * v2.t[i];
			return val;
		}

		T dot(const vec &v) const
		{
			T val = 0;
			for (int i = 0; i < size; i++)
				val += t[i] * v.t[i];
			return val;
		}

		T lenSq() const
		{
			return dot(*this);
		}

		T len() const
		{
			return sqrt(lenSq());
		}

		vec &toLen(T len)
		{
			return *this *= (len / this->len());
		}

		vec toLen(T len) const
		{
			return *this * (len / this->len());
		}

		vec getLen(T len) const
		{
			return *this * (len / this->len());
		}

		vec &normalize()
		{
			if (this->lenSq() == 0)
				return *this;
			*this /= len();
			return *this;
		}

		vec normalize() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}

		vec unit() const
		{
			if (this->lenSq() == 0)
				return *this;
			return *this / len();
		}

		static T distanceSq(const vec &v1, const vec &v2)
		{
			return (v1 - v2).lenSq();
		}

		static T distance(const vec &v1, const vec &v2)
		{
			return (v1 - v2).len();
		}

		T distanceSq(const vec &v) const
		{
			return (*this - v).lenSq();
		}

		T distance(const vec &v) const
		{
			return (*this - v).len();
		}

		vec &reflect(const vec &normal)
		{
			*this = *this - (normal * ((T) 2.0 * dot(normal, *this)));
			return *this;
		}

		operator T *() const { return (T*) t; }
	};

	template <typename T>
	class vec2 : public vec<T, 2>
	{
	public:
		vec2() : vec<T, 2>() {}

		vec2(const vec2 &v) : vec<T, 2>(v) {}

		vec2(const vec<T, 2> &v) : vec<T, 2>(v) {}

		vec2(T u1, T u2)
		{
			this->t[0] = u1;
			this->t[1] = u2;
		}

		vec2 &operator = (const vec2 &v)
		{
			for (int i = 0; i < 2; i++)
				this->t[i] = v.t[i];
			return *this;
		}

		T &x() { return this->t[0]; }
		const T &x() const { return this->t[0]; }

		T &y() { return this->t[1]; }
		const T &y() const { return this->t[1]; }

		T &width() { return this->t[0]; }
		const T &width() const { return this->t[0]; }

		T &height() { return this->t[1]; }
		const T &height() const { return this->t[1]; }

		vec2 yx() const { return vec2(this->t[1], this->t[0]); }
	};

	template <typename T>
	class vec3 : public vec<T, 3>
	{
	public:
		vec3() : vec<T, 3>() {}

		vec3(const vec3 &v) : vec<T, 3>(v) {}

		vec3(const vec<T, 3> &v) : vec<T, 3>(v) {}

		vec3(T u1, T u2, T u3)
		{
			this->t[0] = u1;
			this->t[1] = u2;
			this->t[2] = u3;
		}

		vec3(const vec2<T> &v)
		{
			this->t[0] = v.t[0];
			this->t[1] = v.t[1];
			this->t[2] = 0;
		}

		vec3(const vec2<T> &v, T u)
		{
			this->t[0] = v.t[0];
			this->t[1] = v.t[1];
			this->t[2] = u;
		}

		vec3(T u, const vec2<T> &v)
		{
			this->t[0] = u;
			this->t[1] = v.t[0];
			this->t[2] = v.t[1];
		}

		vec3 &operator = (const vec3 &v)
		{
			for (int i = 0; i < 3; i++)
				this->t[i] = v.t[i];
			return *this;
		}

		static vec3 cross(const vec3 &v1, const vec3 &v2)
		{
			vec3 v;
			v.t[0] = v1.t[1] * v2.t[2] - v1.t[2] * v2.t[1];
			v.t[1] = v1.t[2] * v2.t[0] - v1.t[0] * v2.t[2];
			v.t[2] = v1.t[0] * v2.t[1] - v1.t[1] * v2.t[0];
			return v;
		}

		vec3 cross(const vec3 &v2) const
		{
			vec3 v;
			v.t[0] = this->t[1] * v2.t[2] - this->t[2] * v2.t[1];
			v.t[1] = this->t[2] * v2.t[0] - this->t[0] * v2.t[2];
			v.t[2] = this->t[0] * v2.t[1] - this->t[1] * v2.t[0];
			return v;
		}

		T &x() { return this->t[0]; }
		const T &x() const { return this->t[0]; }

		T &y() { return this->t[1]; }
		const T &y() const { return this->t[1]; }

		T &z() { return this->t[2]; }
		const T &z() const { return this->t[2]; }

		vec2<T> xy() const { return vec2<T>(this->t[0], this->t[1]); }
		vec2<T> yz() const { return vec2<T>(this->t[1], this->t[2]); }
		vec2<T> xz() const { return vec2<T>(this->t[0], this->t[2]); }

		vec3 zyx() const { return vec3(this->t[2], this->t[1], this->t[0]); }

		vec3 rgb() const { return vec3(this->t[0], this->t[1], this->t[2]); }
		vec3 bgr() const { return vec3(this->t[2], this->t[1], this->t[0]); }
	};

	template <typename T>
	class vec4 : public vec<T, 4>
	{
	public:
		vec4() : vec<T, 4>() {}

		vec4(const vec4 &v) : vec<T, 4>(v) {}

		vec4(const vec<T, 4> &v) : vec<T, 4>(v) {}

		vec4(T u1, T u2, T u3, T u4)
		{
			this->t[0] = u1;
			this->t[1] = u2;
			this->t[2] = u3;
			this->t[3] = u4;
		}

		vec4(const vec2<T> &v)
		{
			this->t[0] = v.t[0];
			this->t[1] = v.t[1];
			this->t[2] = 0;
			this->t[3] = 0;
		}

		vec4(const vec2<T> &v, T u1)
		{
			this->t[0] = v.t[0];
			this->t[1] = v.t[1];
			this->t[2] = u1;
			this->t[3] = 0;
		}

		vec4(const vec2<T> &v, T u1, T u2)
		{
			this->t[0] = v.t[0];
			this->t[1] = v.t[1];
			this->t[2] = u1;
			this->t[3] = u2;
		}

		vec4(const vec2<T> &v1, const vec2<T> &v2)
		{
			this->t[0] = v1.t[0];
			this->t[1] = v1.t[1];
			this->t[2] = v2.t[0];
			this->t[3] = v2.t[1];
		}

		vec4(T u1, const vec2<T> &v)
		{
			this->t[0] = u1;
			this->t[1] = v.t[0];
			this->t[2] = v.t[1];
			this->t[3] = 0;
		}

		vec4(T u1, T u2, const vec2<T> &v)
		{
			this->t[0] = u1;
			this->t[1] = u2;
			this->t[2] = v.t[0];
			this->t[3] = v.t[1];
		}

		vec4(const vec3<T> &v)
		{
			this->t[0] = v.t[0];
			this->t[1] = v.t[1];
			this->t[2] = v.t[2];
			this->t[3] = 0;
		}

		vec4(const vec3<T> &v, T u1)
		{
			this->t[0] = v.t[0];
			this->t[1] = v.t[1];
			this->t[2] = v.t[2];
			this->t[3] = u1;
		}

		vec4(T u1, const vec3<T> &v)
		{
			this->t[0] = u1;
			this->t[1] = v.t[0];
			this->t[2] = v.t[1];
			this->t[3] = v.t[2];
		}

		vec4 &operator = (const vec4 &v)
		{
			for (int i = 0; i < 4; i++)
				this->t[i] = v.t[i];
			return *this;
		}

		T &x() { return this->t[0]; }
		const T &x() const { return this->t[0]; }

		T &y() { return this->t[1]; }
		const T &y() const { return this->t[1]; }

		T &z() { return this->t[2]; }
		const T &z() const { return this->t[2]; }

		T &w() { return this->t[3]; }
		const T &w() const { return this->t[3]; }

		T &width() { return this->t[2]; }
		const T &width() const { return this->t[2]; }

		T &height() { return this->t[3]; }
		const T &height() const { return this->t[3]; }

		vec2<T> xy() const { return vec2<T>(this->t[0], this->t[1]); }
		vec2<T> yz() const { return vec2<T>(this->t[1], this->t[2]); }
		vec2<T> xz() const { return vec2<T>(this->t[0], this->t[2]); }
		vec2<T> zw() const { return vec2<T>(this->t[2], this->t[3]); }

		vec3<T> xyz() const { return vec3<T>(this->t[0], this->t[1], this->t[2]); }
		vec3<T> rgb() const { return vec3<T>(this->t[0], this->t[1], this->t[2]); }
		vec3<T> bgr() const { return vec3<T>(this->t[2], this->t[1], this->t[0]); }

		vec4 wxyz() const { return vec4(this->t[3], this->t[0], this->t[1], this->t[2]); }
		vec4 rgba() const { return vec4(this->t[0], this->t[1], this->t[2], this->t[3]); }
		vec4 bgra() const { return vec4(this->t[2], this->t[1], this->t[0], this->t[3]); }
	};

	typedef vec2<float> vec2f;
	typedef vec2<double> vec2d;
	typedef vec2<int> vec2i;
	typedef vec2<fixed> vec2fi;

	typedef vec3<float> vec3f;
	typedef vec3<double> vec3d;
	typedef vec3<int> vec3i;
	typedef vec3<fixed> vec3fi;

	typedef vec4<float> vec4f;
	typedef vec4<double> vec4d;
	typedef vec4<int> vec4i;
	typedef vec4<fixed> vec4fi;
}

template <typename T, int size>
std::ostream &operator << (std::ostream &out, Math::vec<T, size> v)
{
	for (int i = 0; i < size - 1; i++)
		out << v.t[i] << ", ";
	out << v.t[size - 1];
	return out;
}

#endif
