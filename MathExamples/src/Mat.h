#pragma once

#if 0

#include "Vec.h"
#include "FixedPoint.h"

#include <cmath>

namespace Math
{
	template <typename T, int cols, int rows>
	class mat
	{
	public:
		vec<T, rows> t[cols];

	public:
		mat() {}

		mat(const mat &m)
		{
			for (int i = 0; i < cols; i++)
				t[i] = m.t[i];
		}

		mat(T *t)
		{
			int k = 0;
			for (int i = 0; i < cols; i++)
				for (int j = 0; j < rows; j++)
					this->t[i].t[j] = t[k++];
		}

		mat &operator = (const mat &m)
		{
			for (int i = 0; i < cols; i++)
				t[i] = m.t[i];
			return *this;
		}

		mat operator * (const mat &m1) const
		{
			// TODO: might need optimising.
			mat m;
			for (int i = 0; i < cols; i++)
			{
				auto tVec = vec<T, rows>();
				for (int j = 0; j < rows; j++)
					tVec[j] = m1.t[j].t[i];

				for (int j = 0; j < rows; j++)
					m.t[j].t[i] = vec<T, rows>::dot(t[j], tVec);
			}
			return m;
		}

		void operator *= (const mat &m1)
		{
			mat m = *this * m1;
			*this = m;
		}

		vec<T, rows> operator * (const vec<T, rows> &v) const
		{
			vec<T, rows> v1, v2;
			for (int i = 0; i < cols; i++)
			{
				for (int j = 0; j < rows; j++)
					v2.t[j] = t[j].t[i];
				v1.t[i] = vec<T, rows>::dot(v2, v);
			}
			return v1;
		}

		mat operator * (T u) const
		{
			mat m;
			for (int i = 0; i < cols; i++)
				m.t[i] = t[i] * u;
			return m;
		}

		void operator *= (T u)
		{
			for (int i = 0; i < cols; i++)
				t[i] *= u;
		}

		mat operator + (const mat &m1) const
		{
			mat m;
			for (int i = 0; i < cols; i++)
				m.t[i] = t[i] + m1.t[i];
			return m;
		}

		void operator += (const mat &m1)
		{
			for (int i = 0; i < cols; i++)
				t[i] += m1.t[i];
		}

		mat operator - (const mat &m1) const
		{
			mat m;
			for (int i = 0; i < cols; i++)
				m.t[i] = t[i] - m1.t[i];
			return m;
		}

		void operator -= (const mat &m1)
		{
			for (int i = 0; i < cols; i++)
				t[i] -= m1.t[i];
		}

		mat &identity()
		{
			for (int i = 0; i < cols; i++)
				for (int j = 0; j < rows; j++)
					if (i == j)
						t[i].t[j] = 1;
					else
						t[i].t[j] = 0;
			return *this;
		}

		mat transpose() const
		{
			mat m;
			for (int i = 0; i < cols; i++)
				for (int j = 0; j < rows; j++)
					m.t[j].t[i] = t[i].t[j];
			return m;
		}

		virtual T det() const { return 0; }

		virtual mat inverse() const
		{
			T d = det();

			if (d == 0)
				return *this;

			return detReciprocal() * ((T) 1.0 / d);
		}

		bool operator == (const mat &m) const
		{
			for (int i = 0; i < cols; i++)
				if (m.t[i] != t[i])
					return false;
			return true;
		}

		bool operator != (const mat &m) const
		{
			return !(*this == m);
		}

		operator T *() { return (T*) t; }
		operator const T *() const { return (T*) t; }

	protected:
		virtual mat detReciprocal() const { return *this; }
	};

	template <typename T>
	class mat2 : public mat<T, 2, 2>
	{
	public:
		mat2() : mat<T, 2, 2>() {}

		mat2(const mat2 &m) : mat<T, 2, 2>(m) {}

		mat2(const mat<T, 2, 2> &m) : mat<T, 2, 2>(m) {}

		mat2(T t1, T t2, T t3, T t4)
		{
			this->t[0].t[0] = t1;
			this->t[0].t[1] = t2;
			this->t[1].t[0] = t3;
			this->t[1].t[1] = t4;
		}

		mat2(const vec2<T> &v1, const vec2<T> &v2)
		{
			this->t[0] = v1;
			this->t[1] = v2;
		}

		virtual T det() const
		{
			return this->t[0].t[0] * this->t[1].t[1] - this->t[1].t[0] * this->t[0].t[1];
		}

	protected:
		virtual mat<T, 2, 2> detReciprocal() const
		{
			return mat2(this->t[1].t[1], -this->t[0].t[1], -this->t[1].t[0], this->t[0].t[0]);
		}
	};

	template <typename T>
	class mat3 : public mat<T, 3, 3>
	{
	public:
		mat3() : mat<T, 3, 3>() {}

		mat3(const mat3 &m) : mat<T, 3, 3>(m) {}

		mat3(const mat<T, 3, 3> &m) : mat<T, 3, 3>(m) {}

		mat3(T t1, T t2, T t3, T t4, T t5, T t6, T t7, T t8, T t9)
		{
			this->t[0].t[0] = t1; this->t[0].t[1] = t2; this->t[0].t[2] = t3;
			this->t[1].t[0] = t4; this->t[1].t[1] = t5; this->t[1].t[2] = t6;
			this->t[2].t[0] = t7; this->t[2].t[1] = t8; this->t[2].t[2] = t9;
		}

		mat3(const vec3<T> &v1, const vec3<T> &v2, const vec3<T> &v3)
		{
			this->t[0] = v1;
			this->t[1] = v2;
			this->t[2] = v3;
		}

		virtual T det() const
		{
			return this->t[0].t[0] * this->t[1].t[1] * this->t[2].t[2] + this->t[1].t[0] * this->t[2].t[1] * this->t[0].t[2] + this->t[2].t[0] * this->t[0].t[1] * this->t[1].t[2] -
				this->t[2].t[0] * this->t[1].t[1] * this->t[0].t[2] - this->t[1].t[0] * this->t[0].t[1] * this->t[2].t[2] - this->t[0].t[0] * this->t[2].t[1] * this->t[1].t[2];
		}

	protected:
		virtual mat<T, 3, 3> detReciprocal() const
		{
			return mat3(this->t[1].t[1] * this->t[2].t[2] - this->t[2].t[1] * this->t[1].t[2], this->t[2].t[1] * this->t[0].t[2] - this->t[0].t[1] * this->t[2].t[2], this->t[0].t[1] * this->t[1].t[2] - this->t[1].t[1] * this->t[0].t[2],
						this->t[2].t[0] * this->t[1].t[2] - this->t[1].t[0] * this->t[2].t[2], this->t[0].t[0] * this->t[2].t[2] - this->t[2].t[0] * this->t[0].t[2], this->t[0].t[2] * this->t[1].t[0] - this->t[0].t[0] * this->t[1].t[2],
						this->t[1].t[0] * this->t[2].t[1] - this->t[2].t[0] * this->t[1].t[1], this->t[2].t[0] * this->t[0].t[1] - this->t[0].t[0] * this->t[2].t[1], this->t[0].t[0] * this->t[1].t[1] - this->t[1].t[0] * this->t[0].t[1]);
		}
	};

	template <typename T>
	class mat4 : public mat<T, 4, 4>
	{
	public:
		mat4() : mat<T, 4, 4>() {}

		mat4(const mat4 &m) : mat<T, 4, 4>(m) {}

		mat4(const mat<T, 4, 4> &m) : mat<T, 4, 4>(m) {}

		mat4(T t1, T t2, T t3, T t4, T t5, T t6, T t7, T t8, T t9, T t10, T t11, T t12, T t13, T t14, T t15, T t16)
		{
			this->t[0].t[0] = t1;  this->t[0].t[1] = t2;  this->t[0].t[2] = t3;  this->t[0].t[3] = t4;
			this->t[1].t[0] = t5;  this->t[1].t[1] = t6;  this->t[1].t[2] = t7;  this->t[1].t[3] = t8;
			this->t[2].t[0] = t9;  this->t[2].t[1] = t10; this->t[2].t[2] = t11; this->t[2].t[3] = t12;
			this->t[3].t[0] = t13; this->t[3].t[1] = t14; this->t[3].t[2] = t15; this->t[3].t[3] = t16;
		}

		mat4(const vec4<T> &v1, const vec4<T> &v2, const vec4<T> &v3, const vec4<T> &v4)
		{
			this->t[0] = v1;
			this->t[1] = v2;
			this->t[2] = v3;
			this->t[3] = v4;
		}

		mat4(const T m[16])
		{
			int k = 0;
			for (int i = 0; i < 4; i++)
				for (int j = 0; j < 4; j++)
					this->t[i].t[j] = m[k++];
		}

		virtual T det() const
		{
			// TODO: reduce amount of calculation
			return this->t[0].t[0]*this->t[1].t[1]*this->t[2].t[2]*this->t[3].t[3] + this->t[0].t[0]*this->t[1].t[2]*this->t[2].t[3]*this->t[3].t[1] + this->t[0].t[0]*this->t[1].t[3]*this->t[2].t[1]*this->t[3].t[2] +
				this->t[0].t[1]*this->t[1].t[0]*this->t[2].t[3]*this->t[3].t[2] + this->t[0].t[1]*this->t[1].t[2]*this->t[2].t[0]*this->t[3].t[3] + this->t[0].t[1]*this->t[1].t[3]*this->t[2].t[2]*this->t[3].t[0] +
				this->t[0].t[2]*this->t[1].t[0]*this->t[2].t[1]*this->t[3].t[3] + this->t[0].t[2]*this->t[1].t[1]*this->t[2].t[3]*this->t[3].t[0] + this->t[0].t[2]*this->t[1].t[3]*this->t[2].t[0]*this->t[3].t[1] +
				this->t[0].t[3]*this->t[1].t[0]*this->t[2].t[2]*this->t[3].t[1] + this->t[0].t[3]*this->t[1].t[1]*this->t[2].t[0]*this->t[3].t[2] + this->t[0].t[3]*this->t[1].t[2]*this->t[2].t[1]*this->t[3].t[0] -
				this->t[0].t[0]*this->t[1].t[1]*this->t[2].t[3]*this->t[3].t[2] - this->t[0].t[0]*this->t[1].t[2]*this->t[2].t[1]*this->t[3].t[3] - this->t[0].t[0]*this->t[1].t[3]*this->t[2].t[2]*this->t[3].t[1] -
				this->t[0].t[1]*this->t[1].t[0]*this->t[2].t[2]*this->t[3].t[3] - this->t[0].t[1]*this->t[1].t[2]*this->t[2].t[3]*this->t[3].t[0] - this->t[0].t[1]*this->t[1].t[3]*this->t[2].t[0]*this->t[3].t[2] -
				this->t[0].t[2]*this->t[1].t[0]*this->t[2].t[3]*this->t[3].t[1] - this->t[0].t[2]*this->t[1].t[1]*this->t[2].t[0]*this->t[3].t[3] - this->t[0].t[2]*this->t[1].t[3]*this->t[2].t[1]*this->t[3].t[0] -
				this->t[0].t[3]*this->t[1].t[0]*this->t[2].t[1]*this->t[3].t[2] - this->t[0].t[3]*this->t[1].t[1]*this->t[2].t[2]*this->t[3].t[0] - this->t[0].t[3]*this->t[1].t[2]*this->t[2].t[0]*this->t[3].t[1];
		}

		static mat4 getIdentity()
		{
			return mat4(1, 0, 0, 0,
						0, 1, 0, 0,
						0, 0, 1, 0,
						0, 0, 0, 1);
		}

		static mat4 getTranslate(T x, T y, T z)
		{
			return mat4(1, 0, 0, 0,
						0, 1, 0, 0,
						0, 0, 1, 0,
						x, y, z, 1);
		}

		static mat4 getTranslate(const vec3<T> &v)
		{
			return getTranslate(v.x(), v.y(), v.z());
		}

		mat4 &translate(T x, T y, T z)
		{
			*this *= getTranslate(x, y, z);
			return *this;
		}

		mat4 &translate(const vec3<T> &v)
		{
			*this *= getTranslate(v);
			return *this;
		}

		static mat4 getScale(T x, T y, T z)
		{
			return mat4(x, 0, 0, 0,
						0, y, 0, 0,
						0, 0, z, 0,
						0, 0, 0, 1);
		}

		static mat4 getScale(const vec3<T> &v)
		{
			return getScale(v.x(), v.y(), v.z());
		}

		mat4 &scale(T x, T y, T z)
		{
			*this *= getScale(x, y, z);
			return *this;
		}

		mat4 &scale(const vec3<T> &v)
		{
			return getScale(v.x(), v.y(), v.z());
		}

		static mat4 getRotateX(T angle)
		{
			return mat4(1, 0, 0, 0,
						0, cos(angle), sin(angle), 0,
						0, -sin(angle), cos(angle), 0,
						0, 0, 0, 1);
		}

		static mat4 getRotateY(T angle)
		{
			return mat4(cos(angle), 0, -sin(angle), 0,
						0, 1, 0, 0,
						sin(angle), 0, cos(angle), 0,
						0, 0, 0, 1);
		}

		static mat4 getRotateZ(T angle)
		{
			return mat4(cos(angle), sin(angle), 0, 0,
						-sin(angle), cos(angle), 0, 0,
						0, 0, 1, 0,
						0, 0, 0, 1);
		}

		mat4 &rotateX(T angle)
		{
			*this *= getRotateX(angle);
			return *this;
		}

		mat4 &rotateY(T angle)
		{
			*this *= getRotateY(angle);
			return *this;
		}

		mat4 &rotateZ(T angle)
		{
			*this *= getRotateZ(angle);
			return *this;
		}

		static mat4 getOrtho(T left, T right, T bottom, T top, T zNear, T zFar)
		{
			T rl = right - left;
			T tb = top - bottom;
			T fn = zFar - zNear;

			T tx = (right + left) / rl;
			T ty = (top + bottom) / tb;
			T tz = (zFar + zNear) / fn;

			return mat4((T) 2.0 / rl, 0, 0, 0,
						0, (T) 2.0 / tb, 0, 0,
						0, 0, (T) -2.0 / fn, 0,
						-tx, -ty, -tz, 1);
		}

		static mat4 getPerspective(T fovy, T aspect, T zNear, T zFar)
		{
			T f = (T) 1.0 / tan(fovy / (T) 2.0);

			return mat4(f / aspect, 0, 0, 0,
						0, f, 0, 0,
						0, 0, (zFar + zNear) / (zNear - zFar), -1,
						0, 0, ((T) 2.0 * zFar * zNear) / (zNear - zFar), 0);
		}

		static mat4 getDxPerspective(T fovy, T aspect, T zNear, T zFar) // TODO: test me!
		{
			T f = (T) 1.0 / tan(fovy / (T) 2.0);

			return mat4(f / aspect, 0, 0, 0,
						0, f, 0, 0,
						0, 0, (zFar + zNear) / (zFar - zNear), -1,
						0, 0, ((T) 2.0 * zFar * -zNear) / (zFar - zNear), 0);
		}

		static vec3<T> project(const vec3<T> &pos, const mat4<T> &model, const mat4<T> &proj, const vec4i &view)
		{
			mat4<T> m = model * proj;
			vec4<T> v = m * vec4<T>(pos, 1);

			v.x() = view.t[0] + (view.t[2] * (v.x() / v.w() + 1)) / (T) 2.0;
			v.y() = view.t[1] + (view.t[3] * (v.y() / v.w() + 1)) / (T) 2.0;
			v.z() = (v.z() / v.w() + 1) / (T) 2.0;

			return vec3<T>(v.x(), v.y(), v.z());
		}

		static vec3<T> unProject(const vec3<T> &pos, const mat4<T> &model, const mat4<T> &proj, const vec4i &view)
		{
			vec4<T> v, v1;

			v.x() = (((T) 2.0 * (pos.x() - view.t[0])) / view.t[2]) - 1;
			v.y() = (((T) 2.0 * (pos.y() - view.t[1])) / view.t[3]) - 1;
			v.z() = (T) 2.0 * pos.z() - 1;
			v.w() = 1;

			mat4<T> m = model * proj;
				
			m = m.inverse();
			v = m * v;
			v.w() = (T) 1.0 / v.w();

			vec3<T> tmp(v.x() * v.w(), v.y() * v.w(), v.z() * v.w());
			return tmp;
		}

		static vec3<T> unProjectZ(T x, T y, const vec3<T> &pos, const mat4<T> &model, const mat4<T> &proj, const vec4i &view)
		{
			vec3<T> newPos = project(pos, model, proj, view);
			newPos = unProject(vec3<T>(x, y, newPos.z()), model, proj, view);
			return newPos;
		}

		// TODO: see if this works:
		/*mat4.invert = function(out, a) {
			var a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
				a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
				a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
				a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],

				b00 = a00 * a11 - a01 * a10,
				b01 = a00 * a12 - a02 * a10,
				b02 = a00 * a13 - a03 * a10,
				b03 = a01 * a12 - a02 * a11,
				b04 = a01 * a13 - a03 * a11,
				b05 = a02 * a13 - a03 * a12,
				b06 = a20 * a31 - a21 * a30,
				b07 = a20 * a32 - a22 * a30,
				b08 = a20 * a33 - a23 * a30,
				b09 = a21 * a32 - a22 * a31,
				b10 = a21 * a33 - a23 * a31,
				b11 = a22 * a33 - a23 * a32,

				// Calculate the determinant
				det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

			if (!det) {
				return null;
			}
			det = 1.0 / det;

			out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
			out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
			out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
			out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
			out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
			out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
			out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
			out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
			out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
			out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
			out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
			out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
			out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
			out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
			out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
			out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

			return out;
		};*/

		virtual mat4 inverse()
		{
			mat4 newMat;

			#define SWAP_ROWS(a, b) { T *_tmp = a; (a)=(b); (b)=_tmp; }
			#define MAT(m,r,c) (m)[(c)*4+(r)]

			T *m = (T*) &this->t[0].t[0];
			T *out = &newMat.t[0].t[0];

			T wtmp[4][8];
			T m0, m1, m2, m3, s;
			T *r0, *r1, *r2, *r3;
			r0 = wtmp[0], r1 = wtmp[1], r2 = wtmp[2], r3 = wtmp[3];
			r0[0] = MAT(m, 0, 0), r0[1] = MAT(m, 0, 1),
			r0[2] = MAT(m, 0, 2), r0[3] = MAT(m, 0, 3),
			r0[4] = 1.0, r0[5] = r0[6] = r0[7] = 0.0,
			r1[0] = MAT(m, 1, 0), r1[1] = MAT(m, 1, 1),
			r1[2] = MAT(m, 1, 2), r1[3] = MAT(m, 1, 3),
			r1[5] = 1.0, r1[4] = r1[6] = r1[7] = 0.0,
			r2[0] = MAT(m, 2, 0), r2[1] = MAT(m, 2, 1),
			r2[2] = MAT(m, 2, 2), r2[3] = MAT(m, 2, 3),
			r2[6] = 1.0, r2[4] = r2[5] = r2[7] = 0.0,
			r3[0] = MAT(m, 3, 0), r3[1] = MAT(m, 3, 1),
			r3[2] = MAT(m, 3, 2), r3[3] = MAT(m, 3, 3),
			r3[7] = 1.0, r3[4] = r3[5] = r3[6] = 0.0;
			/* choose pivot - or die */
			if (fabsf(r3[0]) > fabsf(r2[0]))
				SWAP_ROWS(r3, r2);
			if (fabsf(r2[0]) > fabsf(r1[0]))
				SWAP_ROWS(r2, r1);
			if (fabsf(r1[0]) > fabsf(r0[0]))
				SWAP_ROWS(r1, r0);
			if (0.0 == r0[0])
				return *this;
			/* eliminate first variable     */
			m1 = r1[0] / r0[0];
			m2 = r2[0] / r0[0];
			m3 = r3[0] / r0[0];
			s = r0[1];
			r1[1] -= m1 * s;
			r2[1] -= m2 * s;
			r3[1] -= m3 * s;
			s = r0[2];
			r1[2] -= m1 * s;
			r2[2] -= m2 * s;
			r3[2] -= m3 * s;
			s = r0[3];
			r1[3] -= m1 * s;
			r2[3] -= m2 * s;
			r3[3] -= m3 * s;
			s = r0[4];
			if (s != 0.0) {
				r1[4] -= m1 * s;
				r2[4] -= m2 * s;
				r3[4] -= m3 * s;
			}
			s = r0[5];
			if (s != 0.0) {
				r1[5] -= m1 * s;
				r2[5] -= m2 * s;
				r3[5] -= m3 * s;
			}
			s = r0[6];
			if (s != 0.0) {
				r1[6] -= m1 * s;
				r2[6] -= m2 * s;
				r3[6] -= m3 * s;
			}
			s = r0[7];
			if (s != 0.0) {
				r1[7] -= m1 * s;
				r2[7] -= m2 * s;
				r3[7] -= m3 * s;
			}
			/* choose pivot - or die */
			if (fabsf(r3[1]) > fabsf(r2[1]))
				SWAP_ROWS(r3, r2);
			if (fabsf(r2[1]) > fabsf(r1[1]))
				SWAP_ROWS(r2, r1);
			if (0.0 == r1[1])
				return *this;
			/* eliminate second variable */
			m2 = r2[1] / r1[1];
			m3 = r3[1] / r1[1];
			r2[2] -= m2 * r1[2];
			r3[2] -= m3 * r1[2];
			r2[3] -= m2 * r1[3];
			r3[3] -= m3 * r1[3];
			s = r1[4];
			if (0.0 != s) {
				r2[4] -= m2 * s;
				r3[4] -= m3 * s;
			}
			s = r1[5];
			if (0.0 != s) {
				r2[5] -= m2 * s;
				r3[5] -= m3 * s;
			}
			s = r1[6];
			if (0.0 != s) {
				r2[6] -= m2 * s;
				r3[6] -= m3 * s;
			}
			s = r1[7];
			if (0.0 != s) {
				r2[7] -= m2 * s;
				r3[7] -= m3 * s;
			}
			/* choose pivot - or die */
			if (fabsf(r3[2]) > fabsf(r2[2]))
				SWAP_ROWS(r3, r2);
			if (0.0 == r2[2])
				return *this;
			/* eliminate third variable */
			m3 = r3[2] / r2[2];
			r3[3] -= m3 * r2[3], r3[4] -= m3 * r2[4],
			r3[5] -= m3 * r2[5], r3[6] -= m3 * r2[6], r3[7] -= m3 * r2[7];
			/* last check */
			if (0.0 == r3[3])
				return *this;
			s = (T) 1.0 / r3[3];             /* now back substitute row 3 */
			r3[4] *= s;
			r3[5] *= s;
			r3[6] *= s;
			r3[7] *= s;
			m2 = r2[3];                  /* now back substitute row 2 */
			s = (T) 1.0 / r2[2];
			r2[4] = s * (r2[4] - r3[4] * m2), r2[5] = s * (r2[5] - r3[5] * m2),
			r2[6] = s * (r2[6] - r3[6] * m2), r2[7] = s * (r2[7] - r3[7] * m2);
			m1 = r1[3];
			r1[4] -= r3[4] * m1, r1[5] -= r3[5] * m1,
			r1[6] -= r3[6] * m1, r1[7] -= r3[7] * m1;
			m0 = r0[3];
			r0[4] -= r3[4] * m0, r0[5] -= r3[5] * m0,
			r0[6] -= r3[6] * m0, r0[7] -= r3[7] * m0;
			m1 = r1[2];                  /* now back substitute row 1 */
			s = (T) 1.0 / r1[1];
			r1[4] = s * (r1[4] - r2[4] * m1), r1[5] = s * (r1[5] - r2[5] * m1),
			r1[6] = s * (r1[6] - r2[6] * m1), r1[7] = s * (r1[7] - r2[7] * m1);
			m0 = r0[2];
			r0[4] -= r2[4] * m0, r0[5] -= r2[5] * m0,
			r0[6] -= r2[6] * m0, r0[7] -= r2[7] * m0;
			m0 = r0[1];                  /* now back substitute row 0 */
			s = (T) 1.0 / r0[0];
			r0[4] = s * (r0[4] - r1[4] * m0), r0[5] = s * (r0[5] - r1[5] * m0),
			r0[6] = s * (r0[6] - r1[6] * m0), r0[7] = s * (r0[7] - r1[7] * m0);
			MAT(out, 0, 0) = r0[4];
			MAT(out, 0, 1) = r0[5], MAT(out, 0, 2) = r0[6];
			MAT(out, 0, 3) = r0[7], MAT(out, 1, 0) = r1[4];
			MAT(out, 1, 1) = r1[5], MAT(out, 1, 2) = r1[6];
			MAT(out, 1, 3) = r1[7], MAT(out, 2, 0) = r2[4];
			MAT(out, 2, 1) = r2[5], MAT(out, 2, 2) = r2[6];
			MAT(out, 2, 3) = r2[7], MAT(out, 3, 0) = r3[4];
			MAT(out, 3, 1) = r3[5], MAT(out, 3, 2) = r3[6];
			MAT(out, 3, 3) = r3[7];
			return newMat;

			#undef MAT
			#undef SWAP_ROWS
		}

	protected:
		virtual mat<T, 4, 4> detReciprocal() const
		{
			// TODO: reduce amount of calculation, use LU decomposition instead perhaps?
			return mat4(
				this->t[1].t[1]*this->t[2].t[2]*this->t[3].t[3]+this->t[2].t[1]*this->t[3].t[2]*this->t[1].t[3]+this->t[3].t[1]*this->t[1].t[2]*this->t[2].t[3]-this->t[1].t[1]*this->t[3].t[2]*this->t[2].t[3]-this->t[2].t[1]*this->t[1].t[2]*this->t[3].t[3]-this->t[3].t[1]*this->t[2].t[2]*this->t[1].t[3],
				this->t[0].t[1]*this->t[3].t[2]*this->t[2].t[3]+this->t[2].t[1]*this->t[0].t[2]*this->t[3].t[3]+this->t[3].t[1]*this->t[2].t[2]*this->t[0].t[3]-this->t[0].t[1]*this->t[2].t[2]*this->t[3].t[3]-this->t[2].t[1]*this->t[3].t[2]*this->t[0].t[3]-this->t[3].t[1]*this->t[0].t[2]*this->t[2].t[3],
				this->t[0].t[1]*this->t[1].t[2]*this->t[3].t[3]+this->t[1].t[1]*this->t[3].t[2]*this->t[0].t[3]+this->t[3].t[1]*this->t[0].t[2]*this->t[1].t[3]-this->t[0].t[1]*this->t[3].t[2]*this->t[1].t[3]-this->t[1].t[1]*this->t[0].t[2]*this->t[3].t[3]-this->t[3].t[1]*this->t[1].t[2]*this->t[0].t[3],
				this->t[0].t[1]*this->t[2].t[2]*this->t[1].t[3]+this->t[1].t[1]*this->t[0].t[2]*this->t[2].t[3]+this->t[2].t[1]*this->t[1].t[2]*this->t[0].t[3]-this->t[0].t[1]*this->t[1].t[2]*this->t[2].t[3]-this->t[1].t[1]*this->t[2].t[2]*this->t[0].t[3]-this->t[2].t[1]*this->t[0].t[2]*this->t[1].t[3],
				this->t[1].t[0]*this->t[3].t[2]*this->t[2].t[3]+this->t[2].t[0]*this->t[1].t[2]*this->t[3].t[3]+this->t[3].t[0]*this->t[2].t[2]*this->t[1].t[3]-this->t[1].t[0]*this->t[2].t[2]*this->t[3].t[3]-this->t[2].t[0]*this->t[3].t[2]*this->t[1].t[3]-this->t[3].t[0]*this->t[1].t[2]*this->t[2].t[3],
				this->t[0].t[0]*this->t[2].t[2]*this->t[3].t[3]+this->t[2].t[0]*this->t[3].t[2]*this->t[0].t[3]+this->t[3].t[0]*this->t[0].t[2]*this->t[2].t[3]-this->t[0].t[0]*this->t[3].t[2]*this->t[2].t[3]-this->t[2].t[0]*this->t[0].t[2]*this->t[3].t[3]-this->t[3].t[0]*this->t[2].t[2]*this->t[0].t[3],
				this->t[0].t[0]*this->t[3].t[2]*this->t[1].t[3]+this->t[1].t[0]*this->t[0].t[2]*this->t[3].t[3]+this->t[3].t[0]*this->t[1].t[2]*this->t[0].t[3]-this->t[0].t[0]*this->t[1].t[2]*this->t[3].t[3]-this->t[1].t[0]*this->t[3].t[2]*this->t[0].t[3]-this->t[3].t[0]*this->t[0].t[2]*this->t[1].t[3],
				this->t[0].t[0]*this->t[1].t[2]*this->t[2].t[3]+this->t[1].t[0]*this->t[2].t[2]*this->t[0].t[3]+this->t[2].t[0]*this->t[0].t[2]*this->t[1].t[3]-this->t[0].t[0]*this->t[2].t[2]*this->t[1].t[3]-this->t[1].t[0]*this->t[0].t[2]*this->t[2].t[3]-this->t[2].t[0]*this->t[1].t[2]*this->t[0].t[3],
				this->t[1].t[0]*this->t[2].t[1]*this->t[3].t[3]+this->t[2].t[0]*this->t[3].t[1]*this->t[1].t[3]+this->t[3].t[0]*this->t[1].t[1]*this->t[2].t[3]-this->t[1].t[0]*this->t[3].t[1]*this->t[2].t[3]-this->t[2].t[0]*this->t[1].t[1]*this->t[3].t[3]-this->t[3].t[0]*this->t[2].t[1]*this->t[1].t[3],
				this->t[0].t[0]*this->t[3].t[1]*this->t[2].t[3]+this->t[2].t[0]*this->t[0].t[1]*this->t[3].t[3]+this->t[3].t[0]*this->t[2].t[1]*this->t[0].t[3]-this->t[0].t[0]*this->t[2].t[1]*this->t[3].t[3]-this->t[2].t[0]*this->t[3].t[1]*this->t[0].t[3]-this->t[3].t[0]*this->t[0].t[1]*this->t[2].t[3],
				this->t[0].t[0]*this->t[1].t[1]*this->t[3].t[3]+this->t[1].t[0]*this->t[3].t[1]*this->t[0].t[3]+this->t[2].t[0]*this->t[0].t[1]*this->t[1].t[3]-this->t[0].t[0]*this->t[3].t[1]*this->t[1].t[3]-this->t[1].t[0]*this->t[0].t[1]*this->t[3].t[3]-this->t[3].t[0]*this->t[1].t[1]*this->t[0].t[3],
				this->t[0].t[0]*this->t[2].t[1]*this->t[1].t[3]+this->t[1].t[0]*this->t[0].t[1]*this->t[2].t[3]+this->t[2].t[0]*this->t[1].t[1]*this->t[0].t[3]-this->t[0].t[0]*this->t[1].t[1]*this->t[2].t[3]-this->t[1].t[0]*this->t[2].t[1]*this->t[0].t[3]-this->t[2].t[0]*this->t[0].t[1]*this->t[1].t[3],
				this->t[1].t[0]*this->t[3].t[1]*this->t[2].t[2]+this->t[2].t[0]*this->t[1].t[1]*this->t[3].t[2]+this->t[3].t[0]*this->t[2].t[1]*this->t[1].t[2]-this->t[1].t[0]*this->t[2].t[1]*this->t[3].t[2]-this->t[2].t[0]*this->t[3].t[1]*this->t[1].t[2]-this->t[3].t[0]*this->t[1].t[1]*this->t[2].t[2],
				this->t[0].t[0]*this->t[2].t[1]*this->t[3].t[2]+this->t[2].t[0]*this->t[3].t[1]*this->t[0].t[2]+this->t[3].t[0]*this->t[0].t[1]*this->t[2].t[2]-this->t[0].t[0]*this->t[3].t[1]*this->t[2].t[2]-this->t[2].t[0]*this->t[0].t[1]*this->t[3].t[2]-this->t[3].t[0]*this->t[2].t[1]*this->t[0].t[2],
				this->t[0].t[0]*this->t[3].t[1]*this->t[1].t[2]+this->t[1].t[0]*this->t[0].t[1]*this->t[3].t[2]+this->t[3].t[0]*this->t[1].t[1]*this->t[0].t[2]-this->t[0].t[0]*this->t[1].t[1]*this->t[3].t[2]-this->t[1].t[0]*this->t[3].t[1]*this->t[0].t[2]-this->t[3].t[0]*this->t[0].t[1]*this->t[1].t[2],
				this->t[0].t[0]*this->t[1].t[1]*this->t[2].t[2]+this->t[1].t[0]*this->t[2].t[1]*this->t[0].t[2]+this->t[2].t[0]*this->t[0].t[1]*this->t[1].t[2]-this->t[0].t[0]*this->t[2].t[1]*this->t[1].t[2]-this->t[1].t[0]*this->t[0].t[1]*this->t[2].t[2]-this->t[2].t[0]*this->t[1].t[1]*this->t[0].t[2]);
		}
	};

	typedef mat2<float> mat2f;
	typedef mat2<double> mat2d;
	typedef mat2<int> mat2i;
	typedef mat2<fixed> mat2fi;
	typedef mat3<float> mat3f;
	typedef mat3<double> mat3d;
	typedef mat3<int> mat3i;
	typedef mat3<fixed> mat3fi;
	typedef mat4<float> mat4f;
	typedef mat4<double> mat4d;
	typedef mat4<int> mat4i;
	typedef mat4<fixed> mat4fi;
}

template <typename T, int cols, int rows>
std::ostream &operator << (std::ostream &out, Math::mat<T, cols, rows> v)
{
	for (int i = 0; i < cols - 1; i++)
		out << v.t[i] << "\n";
	out << v.t[cols - 1];
	return out;
}

#endif
