#include "VecTest.h"

#include <Vector/VectorCommon.h>

#include <cassert>

using namespace Math;

static void testVec2()
{
	ivec2 v1;
	vec2 v2;
	dvec2 v3;

	assert(v1 == ivec2(0, 0));
	assert(v2 == vec2(0.0f, 0.0f));
	assert(v3 == dvec2(0.0, 0.0));

	v1.x = 2;
	int i = v1.x;

	assert(i == 2);
	assert(v1 == ivec2(2, 0));

	v2.x = 2;
	float f = v2.x;

	assert(f == 2);
	assert(v2 == vec2(2.0f, 0.0f));

	v3.y = 2;
	double d = v3.y;

	assert(d == 2);
	assert(v3 == dvec2(0.0, 2.0));

	v1 = v1.yx;
	v2 = v2.yx;
	v3 = v3.yx;

	ivec2 v11(v1);
	vec2 v21(v2);
	dvec2 v31(v3);
}

static void testVec3()
{
	ivec3 v1;
	vec3 v2;
	dvec3 v3;

	assert(v1 == ivec3(0, 0, 0));
	assert(v2 == vec3(0.0f, 0.0f, 0.0f));
	assert(v3 == dvec3(0.0, 0.0, 0.0));

	v1.x = 2;
	int i = v1.x;

	assert(i == 2);
	assert(v1 == ivec3(2, 0, 0));

	v2.x = 2;
	float f = v2.x;

	assert(f == 2);
	assert(v2 == vec3(2.0f, 0.0f, 0.0f));

	v3.z = 2;
	double d = v3.z;

	assert(d == 2);
	assert(v3 == dvec3(0.0, 0.0, 2.0));

	vec2 v2f = v2.xy;
	assert(v2f == vec2(2.0f, 0.0f));

	ivec3 v11(v1);
	vec3 v21(v2);
	dvec3 v31(v3);

	vec3 c1(1.0f, 0.0f, 0.0f);
	vec3 c2(0.0f, 1.0f, 0.0f);
	vec3 c3 = cross(c1, c2);

	assert(c3 == vec3(0.0f, 0.0f, 1.0f));

	vec3 c4(-1.0f, 0.0f, 0.0f);
	vec3 c5(0.0f, 0.0f, -1.0f);
	vec3 c6 = cross(c4, c5);

	assert(c6 == vec3(0.0f, -1.0f, 0.0f));
}

static void testVec4()
{
	ivec4 v1;
	vec4 v2;
	dvec4 v3;

	assert(v1 == ivec4(0, 0, 0, 0));
	assert(v2 == vec4(0.0f, 0.0f, 0.0f, 0.0f));
	assert(v3 == dvec4(0.0, 0.0, 0.0, 0.0));

	v1.x = 2;
	int i = v1.x;

	assert(i == 2);
	assert(v1 == ivec4(2, 0, 0, 0));

	v2.y = 2;
	float f = v2.y;

	assert(f == 2);
	assert(v2 == vec4(0.0f, 2.0f, 0.0f, 0.0f));

	v3.z = 2;
	double d = v3.z;

	assert(d == 2);
	assert(v3 == dvec4(0.0, 0.0, 2.0, 0.0));

	vec3 v2f = v2.xyz;
	assert(v2f == vec3(0.0f, 2.0f, 0.0f));

	ivec4 v11(v1);
	vec4 v21(v2);
	dvec4 v31(v3);
}

static void testOperators()
{
	vec4 v1 = vec4(1.0f, 1.0f, 1.0f, 1.0f) + vec4(2.0f, 1.0f, 3.0f, 4.0f);
	assert(v1 == vec4(3.0f, 2.0f, 4.0f, 5.0f));

	vec4 v2 = vec4(1.0f, 1.0f, 1.0f, 1.0f) - vec4(2.0f, 1.0f, 3.0f, 4.0f);
	assert(v2 == vec4(-1.0f, 0.0f, -2.0f, -3.0f));

	vec4 v3 = vec4(1.0f, 1.0f, 2.0f, 1.0f) * vec4(2.0f, 1.0f, 3.0f, 4.0f);
	assert(v3 == vec4(2.0f, 1.0f, 6.0f, 4.0f));

	vec4 v4 = vec4(1.0f, 1.0f, 3.0f, 1.0f) / vec4(2.0f, 1.0f, 4.0f, 4.0f);
	assert(v4 == vec4(0.5f, 1.0f, 0.75f, 0.25f));
}

static void testFunctions()
{
	float f = dot(vec4(2, 2, 1, 3), vec4(1, 2, 3, 2));
	assert(f == 15);

	float lenSq = vec4(0, 0, 3, 0).lenSq();
	assert(lenSq == 9);

	float len = vec4(0, 3, 0, 0).len();
	assert(len == 3);

	// lerp and friends

	vec2 v2 = vec2(1, 1).reflect(vec2(0, 1));
	assert(v2 == vec2(1, -1));
}


void testVectors()
{
	testVec2();
	testVec3();
	testVec4();
	testOperators();
	testFunctions();
}

