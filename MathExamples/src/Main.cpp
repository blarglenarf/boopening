#include "FixedPointTest.h"
#include "VecTest.h"
#include "MatTest.h"
#include "Experimental.h"

#include <cstdlib>
#include <iostream>

#define UNUSED(x) (void)(x)

int main(int argc, char **argv)
{
	UNUSED(argc);
	UNUSED(argv);

	testFixedPoint();
	testVectors();
	testMatrices();

	experimentalTest();

	std::cout << "Passed all maths tests\n";

	return EXIT_SUCCESS;
}

