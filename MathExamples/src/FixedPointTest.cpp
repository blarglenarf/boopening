#include "FixedPointTest.h"

#include <MathCommon.h>
#include <Timer.h>

#include <cassert>
#include <iostream>

using namespace Math;
using namespace Utils;

static void testAddInt()
{
	fixed f1 = 2;
	fixed f2 = 1;
	fixed expected1 = 3;
	fixed result1 = f1 + f2;
	assert(result1 == expected1);

	fixed f3 = 2;
	fixed result2 = f3 + 2;
	fixed expected2 = 4;
	assert(result2 == expected2);

	fixed f4 = 2;
	fixed result3 = (fixed) 3 + f4;
	fixed expected3 = 5;
	assert(result3 == expected3);

	fixed result4 = 3 + 3;
	fixed expected4 = 6;
	assert(result4 == expected4);

	fixed result5 = 3;
	int expected5 = 3;
	assert(result5 == expected5);
}

static void testSubInt()
{
	fixed f1 = 2;
	fixed f2 = 1;
	fixed expected1 = 1;
	fixed result1 = f1 - f2;
	assert(result1 == expected1);

	fixed f3 = 4;
	fixed result2 = f3 - 2;
	fixed expected2 = 2;
	assert(result2 == expected2);

	fixed f4 = 3;
	fixed result3 = (fixed) 6 - f4;
	fixed expected3 = 3;
	assert(result3 == expected3);

	fixed result4 = 8 - 4;
	fixed expected4 = 4;
	assert(result4 == expected4);

	fixed result5 = 3;
	int expected5 = 3;
	assert(result5 == expected5);
}

static void testMultInt()
{
	fixed f1 = 2;
	fixed f2 = 1;
	fixed expected1 = 2;
	fixed result1 = f1 * f2;
	assert(result1 == expected1);

	fixed f3 = 2;
	fixed result2 = f3 * -2;
	fixed expected2 = -4;
	assert(result2 == expected2);

	fixed f4 = 2;
	fixed result3 = (fixed) -2 * f4;
	fixed expected3 = -4;
	assert(result3 == expected3);

	fixed result4 = 2 * 3;
	fixed expected4 = 6;
	assert(result4 == expected4);

	fixed result5 = 3;
	int expected5 = 3;
	assert(result5 == expected5);
}

static void testDivInt()
{
	fixed f1 = 6;
	fixed f2 = 2;
	fixed expected1 = 3;
	fixed result1 = f1 / f2;
	assert(result1 == expected1);

	fixed f3 = 8;
	fixed result2 = f3 / -2;
	fixed expected2 = -4;
	assert(result2 == expected2);

	fixed f4 = 7;
	fixed result3 = (fixed) -21 / f4;
	fixed expected3 = -3;
	assert(result3 == expected3);

	fixed result4 = 15 / 3;
	fixed expected4 = 5;
	assert(result4 == expected4);

	fixed result5 = 3;
	int expected5 = 3;
	assert(result5 == expected5);
}

static void testAddDouble()
{
	fixed f1 = 2.8;
	fixed f2 = 1.7;
	fixed expected1 = 4.5;
	fixed result1 = f1 + f2;
	assert(result1 == expected1);

	fixed f3 = 2.5;
	fixed result2 = f3 + 2.5;
	fixed expected2 = 5.0;
	assert(result2 == expected2);

	fixed f4 = 2.0;
	fixed result3 = (fixed) 3.0 + f4;
	fixed expected3 = 5.0;
	assert(result3 == expected3);

	fixed result4 = 3.0 + 3.0;
	fixed expected4 = 6.0;
	assert(result4 == expected4);

	fixed result5 = 3.001;
	double expected5 = 3.0;
	assert(result5 != expected5);
}

static void testSubDouble()
{
	fixed f1 = 2.5;
	fixed f2 = 1.0;
	fixed expected1 = 1.5;
	fixed result1 = f1 - f2;
	assert(result1 == expected1);

	fixed f3 = 5.7;
	fixed result2 = f3 - 2.3;
	fixed expected2 = 3.4;
	assert(result2 == expected2);

	fixed f4 = 3.2;
	fixed result3 = (fixed) 6.0 - f4;
	fixed expected3 = 2.8;
	assert(result3 == expected3);

	fixed result4 = 9.9 - 4.6;
	fixed expected4 = 5.3;
	assert(result4 == expected4);

	fixed result5 = 3.6;
	double expected5 = 3.6;
	assert(result5 == expected5);
}

static void testMulDouble()
{
	fixed f1 = 3.0;
	fixed f2 = 0.5;
	fixed expected1 = 1.5;
	fixed result1 = f1 * f2;
	assert(result1 == expected1);

	fixed f3 = 2.2;
	fixed result2 = f3 * -2.0;
	fixed expected2 = -4.4;
	assert(result2 == expected2);

	fixed f4 = 3.7;
	fixed result3 = (fixed) -5.6 * f4;
	fixed expected3 = -20.72;
	assert(result3 == expected3);

	fixed result4 = 15.78 * 260.39;
	fixed expected4 = 4108.9542;
	assert(result4 == expected4);

	fixed result5 = 3.0;
	double expected5 = 3.0;
	assert(result5 == expected5);
}

static void testDivDouble()
{
	fixed f1 = 6.6;
	fixed f2 = 2.7;
	fixed expected1 = 2.4444444;
	fixed result1 = f1 / f2;
	assert(result1 == expected1);

	fixed f3 = 128.96;
	fixed result2 = f3 / -54.64;
	fixed expected2 = -2.3601756954;
	assert(result2 == expected2);

	fixed f4 = 189.067;
	fixed result3 = (fixed) -72.098 / f4;
	fixed expected3 = -0.3813357169;
	assert(result3 == expected3);

	fixed result4 = 0.894 / 2.16;
	fixed expected4 = 0.41388;
	assert(result4 == expected4);

	fixed result5 = 3.0;
	double expected5 = 3.0;
	assert(result5 == expected5);
}

static void testAddFloat()
{
	fixed f1 = 2.8f;
	fixed f2 = 1.7f;
	fixed expected1 = 4.5f;
	fixed result1 = f1 + f2;
	assert(result1 == expected1);

	fixed f3 = 2.5f;
	fixed result2 = f3 + 2.5f;
	fixed expected2 = 5.0f;
	assert(result2 == expected2);

	fixed f4 = 2.0f;
	fixed result3 = (fixed) 3.0f + f4;
	fixed expected3 = 5.0f;
	assert(result3 == expected3);

	fixed result4 = 3.0f + 3.0f;
	fixed expected4 = 6.0f;
	assert(result4 == expected4);

	fixed result5 = 3.001f;
	double expected5 = 3.0f;
	assert(result5 != expected5);
}

static void testSubFloat()
{
	fixed f1 = 2.5f;
	fixed f2 = 1.0f;
	fixed expected1 = 1.5f;
	fixed result1 = f1 - f2;
	assert(result1 == expected1);

	fixed f3 = 5.7f;
	fixed result2 = f3 - 2.3f;
	fixed expected2 = 3.4f;
	assert(result2 == expected2);

	fixed f4 = 3.2f;
	fixed result3 = (fixed) 6.0f - f4;
	fixed expected3 = 2.8f;
	assert(result3 == expected3);

	fixed result4 = 9.9f - 4.6f;
	fixed expected4 = 5.3f;
	assert(result4 == expected4);

	fixed result5 = 3.6f;
	double expected5 = 3.6f;
	assert(result5 == expected5);
}

static void testMulFloat()
{
	fixed f1 = 3.0f;
	fixed f2 = 0.5f;
	fixed expected1 = 1.5f;
	fixed result1 = f1 * f2;
	assert(result1 == expected1);

	fixed f3 = 2.2f;
	fixed result2 = f3 * -2.0f;
	fixed expected2 = -4.4f;
	assert(result2 == expected2);

	fixed f4 = 3.7f;
	fixed result3 = (fixed) -5.6f * f4;
	fixed expected3 = -20.72f;
	assert(result3 == expected3);

	fixed result4 = 15.78f * 260.39f;
	fixed expected4 = 4108.9542f;
	assert(result4 == expected4);

	fixed result5 = 3.0f;
	float expected5 = 3.0f;
	assert(result5 == expected5);
}

static void testDivFloat()
{
	fixed f1 = 6.6f;
	fixed f2 = 2.7f;
	fixed expected1 = 2.4444444f;
	fixed result1 = f1 / f2;
	assert(result1 == expected1);

	fixed f3 = 128.96f;
	fixed result2 = f3 / -54.64f;
	fixed expected2 = -2.3601756954f;
	assert(result2 == expected2);

	fixed f4 = 189.067f;
	fixed result3 = (fixed) -72.098f / f4;
	fixed expected3 = -0.3813357169f;
	assert(result3 == expected3);

	fixed result4 = 0.894f / 2.16f;
	fixed expected4 = 0.41388f;
	assert(result4 == expected4);

	fixed result5 = 3.0f;
	float expected5 = 3.0f;
	assert(result5 == expected5);
}

static void testConvert()
{
	fixed f1 = 7;
	int expected1i = 7;
	float expected1f = 7.0f;
	double expected1d = 7.0;

	assert(f1.toInt() == expected1i);
	assert(f1.toFloat() == expected1f);
	assert(f1.toDouble() == expected1d);

	fixed f2 = 89.2785;
	int expected2i = 89;
	float expected2f = 89.2785f;
	double expected2d = 89.2785;

	int result2i = f2.toInt();
	float result2f = f2.toFloat();
	double result2d = f2.toDouble();

	assert(result2i == expected2i);
	assert((result2f - expected2f) <= 0.0001 && (result2f - expected2f) >= -0.0001);
	assert((result2d - expected2d) <= 0.0001 && (result2d - expected2d) >= -0.0001);
}

static void testSpeed()
{
	Timer timer;

	timer.time();
	float f = 0.1f;
	for (float i = 0; i < 10000000; i += 1)
		f = f * i;
	int t1 = (int) timer.time();

	timer.time();
	double d = 0.1;
	for (double i = 0; i < 10000000; i += 1.0)
		d = d * i;
	int t2 = (int) timer.time();

	timer.time();
	fixed fi = 0.1;
	for (fixed i = 0; i < 10000000; i += 1)
		fi = fi * i;
	int t3 = (int) timer.time();

 	//assert(t2 < t1);
	std::cout << "Single floating point arithmetic: " << t1 << "ms, " << f << "\n";
	std::cout << "Double floating point arithmetic: " << t2 << "ms, " << d << "\n";
	std::cout << "Fixed point arithmetic: " << t3 << "ms, " << fi << "\n";
}


void testFixedPoint()
{
	testAddInt();
	testSubInt();
	testMultInt();
	testDivInt();
	testAddDouble();
	testSubDouble();
	testMulDouble();
	testDivDouble();
	testAddFloat();
	testSubFloat();
	testMulFloat();
	testDivFloat();
	testConvert();
	testSpeed();
}
