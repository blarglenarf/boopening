#include "Experimental.h"

#include <Vector/Vec2.h>

#include <iostream>
#include <cassert>

using namespace Math;

void experimentalTest()
{
	vec2 v;
	v.xx = vec2(1, 1);
	std::cout << v << "\n";
	v.yx += vec2(2, 4);
	std::cout << v << "\n";
	auto d = v.yx.dot(v);
	std::cout << d << "\n";
	v += v.yx;
	std::cout << v << "\n";
	v += vec2(2, 1);
	std::cout << v.yx << "\n";

	// TODO: test all the new vector stuff

	// TODO: test the mat2 stuff, if it works, make mat3/4
}

