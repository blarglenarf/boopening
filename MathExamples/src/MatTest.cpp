#include "MatTest.h"

//#include <MathCommon.h>
#include <Matrix/MatrixCommon.h>

#include <cassert>

#define M_PI 3.14159265359

using namespace Math;

static void testMat2()
{
	mat2 m1(1, 2, 3, 4);
	assert(m1.det() == -2);

	mat2 m2(1, 0, -4, 3);
	assert(m2.det() == 3);

	mat2 m3(2, -1, 1, 3);
	assert(m3.det() == 7);

	mat2 m4 = mat2(4, 3, 3, 2).invert();
	assert(m4 == mat2(-2, 3, 3, -4));
}

static void testMat3()
{
	mat3 m1(-2, -1, 2, 2, 1, 0, 3, 3, -1);
	assert(m1.det() == 6);

	mat3 m2(1, 0, 0, 2, -4, 3, 3, 1, -1);
	assert(m2.det() == 1);

	mat3 m3(5, 0, 2, -2, 3, 0, 1, -1, 7);
	assert(m3.det() == 103);

	mat3 m4 = mat3(1, 1, 1, 3, 4, 3, 3, 3, 4).invert();
	assert(m4 == mat3(7, -1, -1, -3, 1, 0, -3, 0, 1));
}

static void testMat4()
{
	mat4 m1(1, 5, 0, 2, 3, 1, 1, -1, -2, 0, 0, 0, 1, -1, -2, 3);
	assert(m1.det() == -6);

	mat4 m2(2, 0, 0, 2, 1, 2, 2, 0, 4, 1, 1, 1, 2, 2, 1, 0);
	assert(m2.det() == -10);

	mat4 m3(0, -3, 1, 2, 7, 2, 0, -4, 1, -1, 0, -2, -5, 1, 2, 0);
	assert(m3.det() == 146);

	mat4 m4 = mat4(1, 0, 0, 1, 2, 1, 0, 1, 1, 1, 2, 0, 3, 0, 1, 2).invert();
	assert(m4 == mat4(-2.5, 0.5, -0.5, 1, 1.5, 0.5, 0.5, -1, 0.5, -0.5, 0.5, 0, 3.5, -0.5, 0.5, -1));
}

static void testFunctions()
{
	mat4 m1(1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 6, 7, 8);
	assert(m1.transpose() == mat4(1, 5, 1, 5, 2, 6, 2, 6, 3, 7, 3, 7, 4, 8, 4, 8));

	assert(mat2().identity() == mat2(1, 0, 0, 1));

	mat4 persp = getPerspective(75.0f, 1.0f, 1.0f, 10.0f);
	assert(mat4(-4.9557562f, 0, 0, 0, 0, -4.9557562f, 0, 0, 0, 0, -1.2222222f, -1, 0, 0, -2.2222223f, 0) == persp);

	mat4 ortho = getOrtho(0.0f, 10.0f, 0.0f, 10.0f, -1.0f, 1.0f);
	assert(mat4(0.2f, 0, 0, 0, 0, 0.2f, 0, 0, 0, 0, -1, 0, -1, -1, 0, 1) == ortho);

	mat4 trans = getTranslate(1.0f, 2.0f, 3.0f);
	assert(trans == mat4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 2, 3, 1));

	mat4 scale = getScale(1.0f, 2.0f, 3.0f);
	assert(scale == mat4(1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 3, 0, 0, 0, 0, 1));

	mat4 rotX = getRotateX(45 * (float) M_PI / 180.0f);
	assert(rotX == mat4(1, 0, 0, 0, 0, 0.70710677f, 0.70710677f, 0, 0, -0.70710677f, 0.70710677f, 0, 0, 0, 0, 1));

	mat4 rotY = getRotateY(45 * (float) M_PI / 180.0f);
	assert(rotY == mat4(0.70710677f, 0, -0.70710677f, 0, 0, 1, 0, 0, 0.70710677f, 0, 0.70710677f, 0, 0, 0, 0, 1));

	mat4 rotZ = getRotateZ(45 * (float) M_PI / 180.0f);
	assert(rotZ == mat4(0.70710677f, 0.70710677f, 0, 0, -0.70710677f, 0.70710677f, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1));

	vec3 p = project(vec3(0.5f, 0.5f, 0), mat4().identity(), mat4().identity(), ivec4(0, 0, 100, 100));
	assert(p == vec3(75, 75, 0.5f));

	vec3 u = unProject(vec3(75, 75, 0), mat4().identity(), mat4().identity(), ivec4(0, 0, 100, 100));
	assert(u == vec3(0.5f, 0.5f, -1));
}


void testMatrices()
{
	testMat2();
	testMat3();
	testMat4();
	testFunctions();
}
