#version 430

#define CAPTURE_TO_LFB 0
#define OPAQUE 0

readonly buffer LightColors
{
	float lightColors[];
};

#if CAPTURE_TO_LFB && !OPAQUE
	#import "lfb"

	LFB_DEC(lfb, vec2);
#endif

in ParticleData
{
	vec2 texCoord;
	vec3 esFrag;
	flat uint id;
} VertexIn;

uniform sampler2D particleTex;

#include "../utils.glsl"

out vec4 fragColor;


void main()
{
	fragColor = vec4(0);

	vec4 lightColor = floatToRGBA8(lightColors[VertexIn.id]);
	vec4 texColor = texture2D(particleTex, VertexIn.texCoord);

	vec2 centreCoord = vec2(0.5, 0.5);
	float d = max(0.0, 0.75 - distance(centreCoord, VertexIn.texCoord));

	vec3 centreCol = vec3(1.0);

	vec4 color = vec4(texColor.xyz * lightColor.xyz + (centreCol * d), min(texColor.w, 0.3));

#if CAPTURE_TO_LFB && !OPAQUE
	int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	LFB_ADD_DATA(lfb, pixel, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
#else
	fragColor = color;
#endif
}

