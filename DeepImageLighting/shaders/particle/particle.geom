#version 430

#define PARTICLE_SIZE 0.02

layout(points) in;

layout(triangle_strip, max_vertices = 4) out;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

in LightData
{
	vec4 esVert;
	flat uint id;
	flat vec3 vel;
	//flat float radius;
	//flat vec4 dir;
} VertexIn[1];

out ParticleData
{
	vec2 texCoord;
	vec3 esFrag;
	flat uint id;
} VertexOut;

#define VERT(v) \
	esVert = m * vec4(verts[v].xyz, 1.0); \
	VertexOut.esFrag = esVert.xyz; \
	VertexOut.texCoord = texes[v]; \
	VertexOut.id = VertexIn[0].id; \
	gl_Position = pMatrix * esVert; \
	EmitVertex();


mat4 getRot(vec3 vel)
{
	vec3 up = vec3(0, 0, 1);
	vec3 xAxis = normalize(cross(vel, up));
	vec3 yAxis = normalize(cross(xAxis, vel));

	mat4 m;

	m[0] = vec4(xAxis.x, xAxis.y, xAxis.z, 0);
	m[1] = vec4(yAxis.x, yAxis.y, yAxis.z, 0);
	m[2] = vec4(vel.x, vel.y, vel.z, 0);
	m[3] = vec4(VertexIn[0].esVert.xyz, 1);

	return m;
}


void main()
{
	float size = PARTICLE_SIZE;
	float stretch = 0.06;

	vec4 esVert;

	mat4 m = getRot(normalize(VertexIn[0].vel));
	m = mvMatrix * m;

	// TODO: This now needs to be billboarded according to the camera.

	vec4 verts[4];
	verts[0] = vec4(size, 0, -size - stretch, 1);
	verts[1] = vec4(-size, 0, -size - stretch, 1);
	verts[2] = vec4(-size, 0, size + stretch, 1);
	verts[3] = vec4(size, 0, size + stretch, 1);

	vec2 texes[4];
	texes[0] = vec2(1, 0);
	texes[1] = vec2(0, 0);
	texes[2] = vec2(0, 1);
	texes[3] = vec2(1, 1);

	VERT(1);
	VERT(2);
	VERT(0);
	VERT(3);
	EndPrimitive();
}

