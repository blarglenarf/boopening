#version 430

layout(location = 0) in vec4 vertex;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

uniform int idOffset;

buffer LightVelocities
{
	vec4 lightVelocities[];
};

out LightData
{
	vec4 esVert;
	flat uint id;
	flat vec3 vel;
	//flat float radius;
	//flat vec4 dir;
} VertexOut;

void main()
{
	vec4 osVert = vec4(vertex.xyz, 1.0);
	vec4 esVert = mvMatrix * osVert;

	VertexOut.esVert = osVert;
	VertexOut.id = gl_VertexID + idOffset;
	VertexOut.vel = lightVelocities[gl_VertexID + idOffset].xyz;
}

