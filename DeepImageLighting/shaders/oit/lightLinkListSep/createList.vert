#version 430

layout(location = 0) in vec3 vertex;

out vec2 vTexCoord;

void main()
{
	gl_Position = vec4(vertex, 1);
}

