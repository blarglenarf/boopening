#version 430

#extension GL_NV_gpu_shader5 : enable

#define LIGHT_8 0
#define GRID_CELL_SIZE 1
#define CLUSTERS 1

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#include "../../utils.glsl"


uniform sampler2DArray lightDepths;

out vec4 color;

void main()
{
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Pick the light to draw.
	int lightID = 0;

#if LIGHT_8
	vec2 texColor = texelFetch(lightDepths, ivec3(gridCoord, int(lightID)), 0).xy;
	int frontCluster = int(texColor.x * 255.0);
	int backCluster = int(texColor.y * 255.0);

	if (frontCluster > 0 || backCluster > 0)
	{
		// Clusters had 1 added to them, so 0 means no light at the cluster.
		frontCluster = max(0, frontCluster - 1);
		backCluster = max(0, backCluster - 1);

		color = vec4(float(frontCluster) / float(CLUSTERS), float(backCluster) / float(CLUSTERS), 0, 1);
	}
#else
	vec2 texColor = texelFetch(lightDepths, ivec3(gridCoord, int(lightID)), 0).xy;
	float frontDepth = texColor.x;
	float backDepth = texColor.y;

	if (frontDepth > 0 || backDepth > 0)
	{
		// Convert front/back into cluster space.
		int frontCluster = min(CLUSTERS - 1, int(frontDepth / (MAX_EYE_Z / float(CLUSTERS - 1))));
		int backCluster = min(CLUSTERS - 1, int(backDepth / (MAX_EYE_Z / float(CLUSTERS - 1))));

		color = vec4(float(frontCluster) / float(CLUSTERS), float(backCluster) / float(CLUSTERS), 0, 1);
	}
#endif
	else
		color = vec4(0, 0, 0.5, 1);
}

