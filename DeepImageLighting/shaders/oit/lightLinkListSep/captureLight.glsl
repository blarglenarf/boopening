#ifndef CAPTURE_LIGHT
#define CAPTURE_LIGHT

#define CONSERVATIVE_RASTER 0
#define CLEAR_DEPTH 0

layout(points) in;
layout(triangle_strip, max_vertices = 60) out;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

uniform uint indexOffset;

in LightData
{
	vec4 osVert;
	flat uint id;
	flat float radius;
#if CONSERVATIVE_RASTER
	flat vec2 rot;
#endif
} VertexIn[1];

out VertexData
{
	float depth;
#if CONSERVATIVE_RASTER && !CLEAR_DEPTH
	flat vec3 v0;
	flat vec3 v1;
	flat vec3 v2;
#endif
} VertexOut;

// TODO: look at icosphere code from http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html.
// TODO: normalise vertices to make it more sphere-like.

#if CONSERVATIVE_RASTER && !CLEAR_DEPTH
	// Render to layer specified by the id.
	#define TRI(p1, p2, p3) \
		lightID = int(VertexIn[0].id); \
		esVert = mvMatrix * vec4(verts[p1] + VertexIn[0].osVert.xyz, 1); \
		VertexOut.depth = -esVert.z; \
		VertexOut.v0 = vec3(esVert.x, esVert.y, esVert.z); \
		gl_Layer = lightID % 2048; \
		gl_Position = pMatrix * esVert; \
		EmitVertex(); \
		esVert = mvMatrix * vec4(verts[p2] + VertexIn[0].osVert.xyz, 1); \
		VertexOut.depth = -esVert.z; \
		VertexOut.v1 = vec3(esVert.x, esVert.y, esVert.z); \
		gl_Layer = lightID % 2048; \
		gl_Position = pMatrix * esVert; \
		EmitVertex(); \
		esVert = mvMatrix * vec4(verts[p3] + VertexIn[0].osVert.xyz, 1); \
		VertexOut.depth = -esVert.z; \
		VertexOut.v2 = vec3(esVert.x, esVert.y, esVert.z); \
		gl_Layer = lightID % 2048; \
		gl_Position = pMatrix * esVert; \
		EmitVertex(); \
		EndPrimitive();
#else
	// Render to layer specified by the id.
	#define TRI(v1, v2, v3) \
		lightID = int(VertexIn[0].id); \
		esVert = mvMatrix * vec4(verts[v1] + VertexIn[0].osVert.xyz, 1); \
		VertexOut.depth = -esVert.z; \
		gl_Layer = lightID % 2048; \
		gl_Position = pMatrix * esVert; \
		EmitVertex(); \
		esVert = mvMatrix * vec4(verts[v2] + VertexIn[0].osVert.xyz, 1); \
		VertexOut.depth = -esVert.z; \
		gl_Layer = lightID % 2048; \
		gl_Position = pMatrix * esVert; \
		EmitVertex(); \
		esVert = mvMatrix * vec4(verts[v3] + VertexIn[0].osVert.xyz, 1); \
		VertexOut.depth = -esVert.z; \
		gl_Layer = lightID % 2048; \
		gl_Position = pMatrix * esVert; \
		EmitVertex(); \
		EndPrimitive();
#endif


void main()
{
	float size = VertexIn[0].radius * 1.1;

	vec4 esVert = vec4(0.0);
	int lightID = 0;

	// To get this working correctly, conservative rasterization has to be used, so min blend equation is needed with
	// inverted max depth for back facing triangles, however that doesn't work with GL_RG8 render targets, so GL_RG32
	// render targets have to be used instead, but those are sloooooow so instead a really large cone needs to be
	// used instead, but that's slower than a normal sized sphere. WTF am I supposed to do here?
	// Also until conservative rasterization works properly, cones are fucked, so I can't use them.

	// First draw spheres.
#if CONSERVATIVE_RASTER
	if (indexOffset == 0)
#else
	if (indexOffset == 0 || indexOffset != 0)
#endif
	{
		vec3 verts[12];
		verts[0]  = vec3(-size,  size,  0);
		verts[1]  = vec3( size,  size,  0);
		verts[2]  = vec3(-size, -size,  0);
		verts[3]  = vec3( size, -size,  0);
		verts[4]  = vec3( 0, -size,  size);
		verts[5]  = vec3( 0,  size,  size);
		verts[6]  = vec3( 0, -size, -size);
		verts[7]  = vec3( 0,  size, -size);
		verts[8]  = vec3( size,  0, -size);
		verts[9]  = vec3( size,  0,  size);
		verts[10] = vec3(-size,  0, -size);
		verts[11] = vec3(-size,  0,  size);

		// Create 20 triangles of the icosahedron.

		// 5 faces around point 0.
		TRI(0, 11, 5);
		TRI(0, 5, 1);
		TRI(0, 1, 7);
		TRI(0, 7, 10);
		TRI(0, 10, 11);

		// 5 adjacent faces.
		TRI(1, 5, 9);
		TRI(5, 11, 4);
		TRI(11, 10, 2);
		TRI(10, 7, 6);
		TRI(7, 1, 8);

		// 5 faces around point 3.
		TRI(3, 9, 4);
		TRI(3, 4, 2);
		TRI(3, 2, 6);
		TRI(3, 6, 8);
		TRI(3, 8, 9);

		// 5 adjacent faces.
		TRI(4, 9, 5);
		TRI(2, 4, 11);
		TRI(6, 2, 10);
		TRI(8, 6, 7);
		TRI(9, 8, 1);
	}
#if CONSERVATIVE_RASTER
	// Then draw cones.
	else
	{
		vec3 verts[5];

		// Just a pyramid.
		verts[0] = vec3(0, 0, 0);
		verts[1] = vec3(-size, size,  size);
		verts[2] = vec3(-size, size, -size);
		verts[3] = vec3( size, size, -size);
		verts[4] = vec3( size, size,  size);

		// Construct rotation matrix using given x and z rotations.
		float cosX = cos(VertexIn[0].rot.x);
		float cosZ = cos(VertexIn[0].rot.y);
		float sinX = sin(VertexIn[0].rot.x);
		float sinZ = sin(VertexIn[0].rot.y);

	#if 0
		// rot(X) * rot(Z).
		#if 1
			// Row major.
			mat4 rot = mat4(vec4(cosZ, -sinZ, 0, 0),
							vec4(cosX * sinZ, cosX * cosZ, -sinX, 0),
							vec4(sinX * sinZ, cosZ * sinX, cosX, 0),
							vec4(0, 0, 0, 1));
		#else
			// Column major.
			mat4 rot = mat4(vec4(cosZ, -sinZ, 0, 0),
							vec4(cosX * sinZ, cosX * cosZ, -sinX, 0),
							vec4(sinX * sinZ, cosZ * sinX, cosX, 0),
							vec4(0, 0, 0, 1));
		#endif
	#else
		// rot(Z) * rot(X).
		#if 1
			// Row major.
			mat4 rot = mat4(vec4(cosZ, -cosX * sinZ, sinX * sinZ, 0),
							vec4(sinZ, cosX * cosZ, -cosZ * sinX, 0),
							vec4(0, sinX, cosX, 0),
							vec4(0, 0, 0, 1));
		#else
			// Column major.
			mat4 rot = mat4(vec4(cosZ, sinZ, 0, 0),
							vec4(-cosX * sinZ, cosX * cosZ, sinX, 0),
							vec4(sinX * sinZ, -cosZ * sinX, cosX, 0),
							vec4(0, 0, 0, 1));
		#endif
	#endif

		// Rotate vertices using rotation matrix.
		verts[1] = (rot * vec4(verts[1], 1.0)).xyz;
		verts[2] = (rot * vec4(verts[2], 1.0)).xyz;
		verts[3] = (rot * vec4(verts[3], 1.0)).xyz;
		verts[4] = (rot * vec4(verts[4], 1.0)).xyz;

		// Join vertices to make a pyramid (currently clockwise).

		// Front.
		TRI(0, 4, 1);

		// Back.
		TRI(0, 2, 3);

		// Left.
		TRI(0, 1, 2);

		// Right.
		TRI(0, 3, 4);

		// Cap.
		TRI(1, 4, 2);
		TRI(2, 4, 3);
	}
#endif
}

#endif
