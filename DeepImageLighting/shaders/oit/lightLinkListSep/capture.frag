#version 430

#define GRID_CELL_SIZE 1
#define CLUSTERS 1
#define OPAQUE 0
#define USE_G_BUFFER 0
#define LINEARISED 0

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "lfb"

#if LINEARISED
	#import "lfb_L"
#else
	#import "lfb_LL"
#endif

#include "../../utils.glsl"

#if !OPAQUE
	LFB_DEC(lfb, vec2);
#endif

#if LINEARISED
	LFB_DEC_L(light, uint);
#else
	LFB_DEC_LL(light, uint);
#endif

#if USE_G_BUFFER
	uniform ivec4 viewport;
	uniform mat4 mvMatrix;
	uniform mat4 invPMatrix;
	uniform uint nSphereLights;

	// Uniforms for the g-buffer textures.
	uniform sampler2D depthTex;
	uniform sampler2D normalTex;
	uniform sampler2D materialTex;
#else
	uniform MaterialBlock
	{
		vec4 ambient;
		vec4 diffuse;
		vec4 specular;
		float shininess;
	} Material;

	in VertexData
	{
		vec3 normal;
		vec3 esFrag;
		vec2 texCoord;
	} VertexIn;

	uniform uint nSphereLights;
	uniform mat4 mvMatrix;

	uniform bool texFlag;
	uniform sampler2D diffuseTex;
	uniform sampler2D normalTex;
#endif

#if 0
	uniform int opaqueFlag;
	uniform sampler2D depthTexture;
#endif

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

#include "../../light/light.glsl"

out vec4 fragColor;


void main()
{
#if 0
	// Depth pre-pass rendering -- seems to have no effect.
	if (opaqueFlag == 1)
	{
		float inDepth = -VertexIn.esFrag.z;
		float preDepth = -texelFetch(depthTexture, ivec2(gl_FragCoord.xy), 0).x;

		if (inDepth > preDepth + 0.2)
		{
			discard;
			return;
		}
	}
#endif

	vec4 color = vec4(0);

#if OPAQUE
	color.w = 1.0;
#endif

#if USE_G_BUFFER
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	color = getAmbient(material, 0.2);
#else
	vec3 esFrag = VertexIn.esFrag;
	vec3 normal = VertexIn.normal;
	color = getAmbient(0.2);
	#if OPAQUE
		if (color.w == 0)
		{
			discard;
			return;
		}
	#endif
#endif

	vec3 viewDir = normalize(-esFrag);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);
#if LINEARISED
	int fragIndex = LFB_GET_SIZE_L(light).x * int(gridCoord.y) + int(gridCoord.x);
#else
	int fragIndex = LFB_GET_SIZE_LL(light).x * int(gridCoord.y) + int(gridCoord.x);
#endif

	// Apply lighting for all lights at given pixel.
#if LINEARISED
	for (LFB_ITER_BEGIN_L(light, fragIndex); LFB_ITER_CHECK_L(light); LFB_ITER_INC_L(light))
#else
	for (LFB_ITER_BEGIN_LL(light, fragIndex); LFB_ITER_CHECK_LL(light); LFB_ITER_INC_LL(light))
#endif
	{
	#if LINEARISED
		uint id = LFB_GET_DATA_L(light);
	#else
		uint id = LFB_GET_DATA_LL(light);
	#endif
		//int id = int(floatBitsToUint(f));

		// Sphere.
		if (id < nSphereLights)
		{
			vec4 lightPos = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
			float lightRadius = sphereLightRadii[id];
			float dist = distance(lightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			vec4 lightColor = floatToRGBA8(lightColors[id]);
		#if USE_G_BUFFER
			float att;
			color += sphereLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, lightRadius, dist, true, att);
		#else
			color += sphereLight(lightColor, lightPos, esFrag, normal, viewDir, lightRadius, dist);
		#endif
		}

		// Cone.
		else
		{
			vec4 lightPos = mvMatrix * vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			float lightRadius = coneLightDist[id - nSphereLights];
			float dist = distance(lightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
		#if USE_G_BUFFER
			float att;
			color += coneLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist, att);
		#else
			float att;
			color += coneLight(lightColor, lightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist, att);
		#endif
		}
	}

#if LIGHT_COUNT_DEBUG && 0
	uint count = LIGHT_COUNT_AT(fragIndex);
	float dc = float(count) / 64.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

#if !OPAQUE
	int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	LFB_ADD_DATA(lfb, pixel, vec2(rgba8ToFloat(color), -esFrag.z));
#endif

#if OPAQUE
	fragColor = color;
#else
	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
#endif
}

