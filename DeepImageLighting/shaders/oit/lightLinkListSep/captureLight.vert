#version 430

#define CONSERVATIVE_RASTER 0

layout(location = 0) in vec4 vertex;

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

#if CONSERVATIVE_RASTER
	readonly buffer ConeLightRot
	{
		vec2 coneLightRot[];
	};
#else
	readonly buffer ConeLightDir
	{
		vec4 coneLightDir[];
	};
#endif


out LightData
{
	vec4 osVert;
	flat uint id;
	flat float radius;
#if CONSERVATIVE_RASTER
	flat vec2 rot;
#endif
} VertexOut;

uniform uint indexOffset;

void main()
{
	VertexOut.osVert = vertex;
	VertexOut.id = uint(gl_VertexID + indexOffset);

	// Sphere or cone?
	if (indexOffset == 0)
	{
		VertexOut.radius = sphereLightRadii[gl_VertexID];
	}
	else
	{
		// Just make a bounding sphere around the cone for now.
		VertexOut.radius = coneLightDist[gl_VertexID];
	#if CONSERVATIVE_RASTER
		VertexOut.rot = coneLightRot[gl_VertexID];
	#else
		vec3 coneDir = normalize(coneLightDir[gl_VertexID].xyz);
		VertexOut.radius *= 0.5;
		VertexOut.osVert.xyz += coneDir * VertexOut.radius;
		VertexOut.radius *= 1.3;
	#endif
	}
}

