#version 430

#define GRID_CELL_SIZE 1
#define OPAQUE 0
#define USE_G_BUFFER 0

#define STORE_LIGHT_DATA 0

// TODO: Check assembly to make sure it's working properly. Also it doesn't render cone lights :(
#define USE_REGISTERS 0

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "lfb"
#import "cluster"

#include "../../utils.glsl"

#if USE_REGISTERS
	#define BLOCK_SIZE 4
	vec4 positionRegisters[BLOCK_SIZE];
	float radiiRegisters[BLOCK_SIZE];
	float colorRegisters[BLOCK_SIZE];
#endif

#if !OPAQUE
	LFB_DEC(lfb, vec2);
#endif

#if STORE_LIGHT_DATA
	CLUSTER_DEC(cluster, vec2);
#else
	CLUSTER_DEC(cluster, uint);
#endif

#if USE_G_BUFFER
	uniform ivec4 viewport;
	uniform mat4 mvMatrix;
	uniform mat4 invPMatrix;
	uniform uint nSphereLights;

	// Uniforms for the g-buffer textures.
	uniform sampler2D depthTex;
	uniform sampler2D normalTex;
	uniform sampler2D materialTex;
#else
	uniform MaterialBlock
	{
		vec4 ambient;
		vec4 diffuse;
		vec4 specular;
		float shininess;
	} Material;

	in VertexData
	{
		vec3 normal;
		vec3 esFrag;
		vec2 texCoord;
	} VertexIn;

	uniform uint nSphereLights;
	uniform mat4 mvMatrix;

	uniform bool texFlag;
	uniform sampler2D diffuseTex;
	uniform sampler2D normalTex;
#endif

#if 0
	uniform int opaqueFlag;
	uniform sampler2D depthTexture;
#endif

#if !STORE_LIGHT_DATA
	readonly buffer SphereLightPositions
	{
		vec4 sphereLightPositions[];
	};

	readonly buffer SphereLightRadii
	{
		float sphereLightRadii[];
	};

	readonly buffer ConeLightDist
	{
		float coneLightDist[];
	};

	readonly buffer ConeLightDir
	{
		vec4 coneLightDir[];
	};

	readonly buffer ConeLightPositions
	{
		vec4 coneLightPositions[];
	};

	readonly buffer LightColors
	{
		float lightColors[];
	};
#endif

#include "../../light/light.glsl"

out vec4 fragColor;



void main()
{
#if 0
	// Depth pre-pass rendering -- seems to have no effect.
	if (opaqueFlag == 1)
	{
		float inDepth = -VertexIn.esFrag.z;
		float preDepth = -texelFetch(depthTexture, ivec2(gl_FragCoord.xy), 0).x;

		if (inDepth > preDepth + 0.2)
		{
			discard;
			return;
		}
	}
#endif

	vec4 color = vec4(0);

#if OPAQUE
	color.w = 1.0;
#endif

#if USE_G_BUFFER
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	color = getAmbient(material, 0.2);
#else
	vec3 esFrag = VertexIn.esFrag;
	vec3 normal = VertexIn.normal;
	color = getAmbient(0.2);
	#if OPAQUE
		if (color.w == 0)
		{
			discard;
			return;
		}
	#endif
#endif

	vec3 viewDir = normalize(-esFrag);

	// Convert fragment into cluster space.
	int cluster = CLUSTER_GET(-esFrag.z, MAX_EYE_Z);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Get fragIndex in cluster space.
	int fragIndex = CLUSTER_GET_INDEX(cluster, cluster, gridCoord);

#if USE_REGISTERS
	CLUSTER_ITER_BEGIN(cluster, fragIndex);

	#if STORE_LIGHT_DATA	
		vec2 data;
		#define LOAD_LIGHT(i) \
			if (CLUSTER_ITER_CHECK(cluster)) \
			{ \
				data = CLUSTER_GET_DATA(cluster); \
				positionRegisters[i] = CLUSTER_GET_POS(cluster); \
				radiiRegisters[i] = data.x; \
				colorRegisters[i] = data.y; \
				clusterSize++; \
				CLUSTER_ITER_INC(cluster); \
			}
	#else
		#define LOAD_LIGHT(i) \
			if (CLUSTER_ITER_CHECK(cluster)) \
			{ \
				uint id = CLUSTER_GET_DATA(cluster); \
				positionRegisters[i] = sphereLightPositions[id].xyz; \
				radiiRegisters[i] = sphereLightRadii[id]; \
				colorRegisters[i] = lightColors[id]; \
				clusterSize++; \
				CLUSTER_ITER_INC(cluster); \
			}
	#endif

	int canary = 1000;
	int clusterSize = 0;
	while (CLUSTER_ITER_CHECK(cluster))
	{
		if (canary-- < 0)
			break;

		clusterSize = 0;
		LOAD_LIGHT(0);
		LOAD_LIGHT(1);
		#if BLOCK_SIZE > 2
			LOAD_LIGHT(2);
			LOAD_LIGHT(3);
		#endif
		#if BLOCK_SIZE > 4
			LOAD_LIGHT(4);
			LOAD_LIGHT(5);
			LOAD_LIGHT(6);
			LOAD_LIGHT(7);
		#endif
		#if BLOCK_SIZE > 8
			LOAD_LIGHT(8);
			LOAD_LIGHT(9);
			LOAD_LIGHT(10);
			LOAD_LIGHT(11);
			LOAD_LIGHT(12);
			LOAD_LIGHT(13);
			LOAD_LIGHT(14);
			LOAD_LIGHT(15);
		#endif
		if (clusterSize == 0)
			break;
	// TODO: Handle cone lights...
	#if USE_G_BUFFER
		#define APPLY_LIGHT(i) \
			if (i < clusterSize) \
			{ \
				float dist = distance(positionRegisters[i].xyz, esFrag); \
				if (dist <= radiiRegisters[i]) \
				{ \
					vec4 lightColor = floatToRGBA8(colorRegisters[i]); \
					float att; \
					color += sphereLight(material.xyz, lightColor, positionRegisters[i], esFrag, normal, viewDir, radiiRegisters[i], dist, true, att); \
				} \
			}
	#else
		#define APPLY_LIGHT(i) \
			if (i < clusterSize) \
			{ \
				float dist = distance(positionRegisters[i].xyz, esFrag); \
				if (dist <= radiiRegisters[i]) \
				{ \
					vec4 lightColor = floatToRGBA8(colorRegisters[i]); \
					color += sphereLight(lightColor, positionRegisters[i], esFrag, normal, viewDir, radiiRegisters[i], dist); \
				} \
			}
	#endif
		APPLY_LIGHT(0);
		APPLY_LIGHT(1);
		#if BLOCK_SIZE > 2
			APPLY_LIGHT(2);
			APPLY_LIGHT(3);
		#endif
		#if BLOCK_SIZE > 4
			APPLY_LIGHT(4);
			APPLY_LIGHT(5);
			APPLY_LIGHT(6);
			APPLY_LIGHT(7);
		#endif
		#if BLOCK_SIZE > 8
			LOAD_LIGHT(8);
			LOAD_LIGHT(9);
			LOAD_LIGHT(10);
			LOAD_LIGHT(11);
			LOAD_LIGHT(12);
			LOAD_LIGHT(13);
			LOAD_LIGHT(14);
			LOAD_LIGHT(15);
		#endif
	}
#else
	// TODO: Load lights into blocks of registers, and process a block at a time.
	// Apply lighting for all lights at given cluster.
	for (CLUSTER_ITER_BEGIN(cluster, fragIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
	{
	#if STORE_LIGHT_DATA
		vec2 data = CLUSTER_GET_DATA(cluster);
		vec4 lightPos = CLUSTER_GET_POS(cluster);
		vec4 lightDir = CLUSTER_GET_DIR(cluster);
		float lightRadius = data.x;
		vec4 lightColor = floatToRGBA8(data.y);
	#else
		uint id = CLUSTER_GET_DATA(cluster);
	#endif

		// Sphere.
	#if STORE_LIGHT_DATA
		if (lightDir.w == 0)
	#else
		if (id < nSphereLights)
	#endif
		{
		#if !STORE_LIGHT_DATA
			vec4 lightPos = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
			float lightRadius = sphereLightRadii[id];
		#endif
			float dist = distance(lightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

		#if !STORE_LIGHT_DATA
			vec4 lightColor = floatToRGBA8(lightColors[id]);
		#endif
		#if USE_G_BUFFER
			float att;
			color += sphereLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, lightRadius, dist, true, att);
		#else
			color += sphereLight(lightColor, lightPos, esFrag, normal, viewDir, lightRadius, dist);
		#endif
		}
		// Cone.
		else
		{
		#if !STORE_LIGHT_DATA
			vec4 lightPos = mvMatrix * vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			float lightRadius = coneLightDist[id - nSphereLights];
		#endif
			float dist = distance(lightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

		#if STORE_LIGHT_DATA
			vec4 coneDir = vec4(lightDir.xyz, 0.0);
			float aperture = lightDir.w;
		#else
			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
		#endif
		#if USE_G_BUFFER
			float att;
			color += coneLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist, att);
		#else
			float att;
			color += coneLight(lightColor, lightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist, att);
		#endif
		}
	}
#endif

#if LIGHT_COUNT_DEBUG && 0
	uint count = LIGHT_COUNT_AT(fragIndex);
	float dc = float(count) / 64.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

#if !OPAQUE
	int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	LFB_ADD_DATA(lfb, pixel, vec2(rgba8ToFloat(color), -esFrag.z));
#endif

#if OPAQUE
	fragColor = color;
#else
	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
#endif
}

