#version 430

#define USE_G_BUFFER 0

#if USE_G_BUFFER
	layout(location = 0) in vec3 vertex;

	void main()
	{
		gl_Position = vec4(vertex.xyz, 1.0);
	}
#else
	layout(location = 0) in vec3 vertex;
	layout(location = 1) in vec3 normal;
	layout(location = 2) in vec2 texCoord;
	layout(location = 3) in vec4 tangent;

	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;
	uniform mat3 nMatrix;

	out VertexData
	{
		vec3 normal;
		vec3 esFrag;
		vec2 texCoord;
	} VertexOut;

	void main()
	{
		vec4 osVert = vec4(vertex, 1.0);
		vec4 esVert = mvMatrix * osVert;
		vec4 csVert = pMatrix * esVert;

		VertexOut.esFrag = esVert.xyz;
		VertexOut.normal = nMatrix * normalize(normal);
		VertexOut.texCoord = texCoord;

		gl_Position = csVert;
	}
#endif

