#version 430

#define LIGHT_8 0
#define CLUSTERS 1

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#define CONSERVATIVE_RASTER 0

#if CONSERVATIVE_RASTER
	#include "../../utils.glsl"
	#include "../../lfb/clusterMask.glsl"
#endif

in VertexData
{
	float depth;
#if CONSERVATIVE_RASTER
	flat vec3 v0;
	flat vec3 v1;
	flat vec3 v2;
#endif
} VertexIn;

#if CONSERVATIVE_RASTER
	uniform ivec2 tileSize;
	uniform ivec4 viewport;
	uniform mat4 invPMatrix;
#endif

out vec2 color;


// Just render colour here, red for front, green for back.
void main()
{
	float depth = VertexIn.depth;

#if CONSERVATIVE_RASTER
	vec2 zRange = triangleClusterIntersection(tileSize, viewport, invPMatrix, gl_FragCoord.xy, VertexIn.v0, VertexIn.v1, VertexIn.v2);
	depth = gl_FrontFacing ? zRange.x : zRange.y;
#endif

#if LIGHT_8
	#if CONSERVATIVE_RASTER
		if (gl_FrontFacing)
		{
			// Cluster space is inverted.
			int cluster = min(CLUSTERS - 1, (CLUSTERS - 1) - int(depth / (MAX_EYE_Z / float(CLUSTERS))));
			color = vec2(float(cluster + 1) / 255.0, 0);
		}
		else
		{
			int cluster = min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1))));
			color = vec2(0, float(cluster + 1) / 255.0);
		}
	#else
		int cluster = min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1))));
		if (gl_FrontFacing)
			color = vec2(float(cluster + 1) / 255.0, 0);
		else
			color = vec2(0, float(cluster + 1) / 255.0);
	#endif
#else
	#if 1
		if (gl_FrontFacing)
			color = vec2(VertexIn.depth, 0);
		else
			color = vec2(0, VertexIn.depth);
	#else
		if (gl_FrontFacing)
		{
			// Cluster space is inverted.
			int cluster = min(CLUSTERS - 1, (CLUSTERS - 1) - int(depth / (MAX_EYE_Z / float(CLUSTERS))));
			color = vec2(float(cluster + 1) / 255.0, 0);
		}
		else
		{
			int cluster = min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1))));
			color = vec2(0, float(cluster + 1) / 255.0);
		}
	#endif
#endif
}

