#version 430

#extension GL_NV_gpu_shader5 : enable

#define MAX_LIGHTS 1
#define GRID_CELL_SIZE 1
#define CLUSTER_CULL 0
#define MASK_SIZE 1
#define MAX_EYE_Z 30.0

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 1

#include "../../utils.glsl"

#import "cluster"

CLUSTER_DEC(cluster);

#if CLUSTER_CULL
	buffer ClusterMasks
	{
		uint clusterMasks[];
	};

	bool checkBit(int fragIndex, int cluster)
	{
		int maskIndex = cluster / (MASK_SIZE * 32);
		int clusterIndex = cluster % 32;
		return (clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
	}
#endif


uniform sampler2DArray lightDepths[MAX_LIGHTS / 2048];

// This is executed once per tile per light.
void main()
{
	// Number of threads is width * height * MAX_LIGHTS.
	int lightID = gl_VertexID / int(CLUSTER_GET_SIZE(cluster).x * CLUSTER_GET_SIZE(cluster).y);
	int fragIndex = gl_VertexID - (int(CLUSTER_GET_SIZE(cluster).x * CLUSTER_GET_SIZE(cluster).y) * lightID);

	ivec2 gridCoord = ivec2(fragIndex % CLUSTER_GET_SIZE(cluster).x, fragIndex / CLUSTER_GET_SIZE(cluster).x);

	int pixel = CLUSTER_GET_SIZE(cluster).x * gridCoord.y + gridCoord.x;

	vec2 texColor = texelFetch(lightDepths[lightID / 2048], ivec3(gridCoord, int(lightID % 2048)), 0).xy;

	if (texColor.x > 0 || texColor.y > 0)
	{
		float frontDepth = texColor.x;
		float backDepth = texColor.y;

		// Loop through all clusters between front and back, and add the lightID to those lists.
		int frontCluster = int(texColor.x * 255.0);
		int backCluster = int(texColor.y * 255.0);

		frontCluster = max(0, frontCluster - 1);
		backCluster = max(0, backCluster - 1);

		// Add id's to each cluster.
		for (int j = frontCluster; j <= backCluster; j++)
		{
		#if CLUSTER_CULL
			if (!checkBit(pixel, j))
				continue;
		#endif
			// Get fragIndex in cluster space.
			int fragIndex = CLUSTER_GET_INDEX(cluster, j, gridCoord);
			CLUSTER_ADD_DATA(cluster, fragIndex, lightID);
		}
	}
}

