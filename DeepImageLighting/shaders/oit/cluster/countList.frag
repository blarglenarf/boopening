#version 430

#extension GL_NV_gpu_shader5 : enable

#define LIGHT_8 0
#define MAX_LIGHTS 1
#define GRID_CELL_SIZE 1
#define CLUSTER_CULL 0
#define MASK_SIZE 1
#define MAX_EYE_Z 30.0
#define CONSERVATIVE_RASTER 0
#define CLUSTERS 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 1

#include "../../utils.glsl"

#if CLUSTER_CULL
	buffer ClusterMasks
	{
		uint clusterMasks[];
	};

	bool checkBit(int fragIndex, int cluster)
	{
		int maskIndex = cluster / (MASK_SIZE * 32);
		int clusterIndex = cluster % 32;
		return (clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
	}
#endif

buffer Offsets
{
	uint offsets[];
};

uniform ivec2 size;

#if MAX_LIGHTS >= 2048
	uniform sampler2DArray lightDepths[MAX_LIGHTS / 2048];
#else
	uniform sampler2DArray lightDepths[1];
#endif

#define CLUSTER_GET_INDEX(gridCoord, cluster) int(gridCoord.y) * size.x * CLUSTERS + int(gridCoord.x) * CLUSTERS + cluster

out vec4 fragColor;

void main()
{
	fragColor = vec4(1);

	ivec2 gridCoord = ivec2(gl_FragCoord.xy);
	int pixel = size.x * gridCoord.y + gridCoord.x;

	for (uint i = 0; i < MAX_LIGHTS; i++)
	{
		vec2 texColor = texelFetch(lightDepths[i / 2048], ivec3(gridCoord, int(i % 2048)), 0).xy;
	#if LIGHT_8
		// Clusters had 1 added to them, so 0 means no light at the cluster.
		if (texColor.x != 0 || texColor.y != 0)
		{
			int frontCluster = int(texColor.x * 255.0);
			int backCluster = int(texColor.y * 255.0);

		#if CONSERVATIVE_RASTER
			frontCluster = max(0, (CLUSTERS - 1) - (frontCluster - 1));
		#else
			frontCluster = max(0, frontCluster - 1);
		#endif
			backCluster = max(0, backCluster - 1);

			// Add id's to each cluster.
			for (int j = frontCluster; j <= backCluster; j++)
			{
			#if CLUSTER_CULL
				if (!checkBit(pixel, j))
					continue;
			#endif
				int fragIndex = CLUSTER_GET_INDEX(gridCoord, j);
				atomicAdd(offsets[fragIndex], 1);
			}
		}
	#else
		float frontDepth = texColor.x;
		float backDepth = texColor.y;

		if (frontDepth != 0 || backDepth != 0)
		{
		#if 1
			// Convert front/back into cluster space.
			int frontCluster = min(CLUSTERS - 1, int(frontDepth / (MAX_EYE_Z / float(CLUSTERS - 1))));
			int backCluster = min(CLUSTERS - 1, int(backDepth / (MAX_EYE_Z / float(CLUSTERS - 1))));
		#else
			// Convert front/back into cluster space.
			int frontCluster = int(texColor.x * 255.0);
			int backCluster = int(texColor.y * 255.0);

			frontCluster = max(0, (CLUSTERS - 1) - (frontCluster - 1));
			backCluster = max(0, backCluster - 1);
		#endif

			// Add id's to each cluster.
			for (int j = frontCluster; j <= backCluster; j++)
			{
			#if CLUSTER_CULL
				if (!checkBit(pixel, j))
					continue;
			#endif
				int fragIndex = CLUSTER_GET_INDEX(gridCoord, j);
				atomicAdd(clusters[fragIndex], 1);
			}
		}
	#endif
	}
}

