#version 430

// Billboarded quad or sphere?
// Note: sphere with back or front face culling seems to be faster.
#define USE_QUAD 0
#define MAX_EYE_Z 30.0

layout(location = 0) in vec4 vertex;

uniform uint indexOffset;

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

out LightData
{
#if USE_QUAD
	vec4 esVert;
#else
	vec4 osVert;
#endif
	flat uint id;
	flat float radius;
} VertexOut;

#if USE_QUAD
	uniform mat4 mvMatrix;
#endif

void main()
{
#if USE_QUAD
	VertexOut.esVert = mvMatrix * vec4(vertex.xyz, 1.0);
#else
	VertexOut.osVert = vec4(vertex.xyz, 1.0);
#endif
	VertexOut.id = uint(gl_VertexID + indexOffset);

	// Sphere or cone?
	if (indexOffset == 0)
		VertexOut.radius = sphereLightRadii[gl_VertexID];
	else
		VertexOut.radius = coneLightDist[gl_VertexID];

#if USE_QUAD
	// Light is outside the frustum range but overlaps the frustum?
	if (-VertexOut.esVert.z < 0 && -VertexOut.esVert.z + VertexOut.radius >= 0)
		VertexOut.esVert.z = -0.1;
	else if (-VertexOut.esVert.z > MAX_EYE_Z && -VertexOut.esVert.z - VertexOut.radius <= MAX_EYE_Z)
		VertexOut.esVert.z = -(MAX_EYE_Z - 0.1);
#endif
}

