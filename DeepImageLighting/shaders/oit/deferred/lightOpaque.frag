#version 430

#define DEPTH_ONLY 0

#include "../../utils.glsl"


readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer LightColors
{
	float lightColors[];
};

in VertexData
{
	vec3 esVert;
	flat uint id;
} VertexIn;


uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;

uniform uint indexOffset;

out vec4 fragColor;



// FIXME: Specular and shininess are hard-coded.
vec4 sphereLight(vec3 material, vec4 lightColor, vec4 lightPos, vec3 esFrag, vec3 normal, vec3 viewDir, float lightRadius, float dist)
{
	vec3 lightDir = normalize(lightPos.xyz - esFrag);
	//float cosTheta = clamp(dot(VertexIn.normal, lightDir), 0.0, 1.0);
	float cosTheta = dot(normal, lightDir);

	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	//float dp = max(cosTheta, 0.0);
	float dp = clamp(cosTheta, 0.0, 1.0);

	if (dp > 0.0)
	{
		//vec4 lightColor = floatToRGBA8(lightColors[id]);
		diffuse = material * dp * lightColor.xyz;
		vec3 reflection = reflect(-lightDir, normal);
		//float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
		float nDotH = abs(dot(viewDir, normalize(reflection)));
		float intensity = pow(nDotH, 128);
		specular = intensity * lightColor.xyz;

		float att = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
		//att = 1.0f;
		att *= att;
		return vec4(att * (diffuse + specular), 0);
	}
	return vec4(0.0);
}

vec4 coneLight(vec3 material, vec4 lightColor, vec4 lightPos, vec3 esFrag, vec3 normal, vec3 viewDir, vec3 coneDir, float aperture, float lightRadius, float dist)
{
	vec3 lightDir = normalize(lightPos.xyz - esFrag);
	vec3 spotDir = normalize(coneDir); // TODO: may need to negate this.

	// Inside the cone?
	float theta = dot(-lightDir, spotDir);
	if (acos(theta) > aperture)
		return vec4(0.0);

	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float cosTheta = dot(normal, lightDir);
	float dp = clamp(cosTheta, 0.0, 1.0);

	//if (dp > 0.0)
	//{
		//vec4 lightColor = floatToRGBA8(lightColors[id]);
		//diffuse = Material.diffuseMat.xyz * dp * lightColor.xyz;
		diffuse = material * dp * lightColor.xyz;
		vec3 reflection = reflect(-lightDir, normal);
		//float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
		float nDotH = abs(dot(viewDir, normalize(reflection)));
		float intensity = pow(nDotH, 128);
		specular = intensity * lightColor.xyz;

		float cosAp = cos(aperture);
		float angularAtt = max(0, (theta - cosAp) / (1.0 - cosAp));
		float radialAtt = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
		//float radialAtt = 1.0 / (dist * dist);
		//att = 1.0f;
		float att = angularAtt * radialAtt;
		return vec4(att * (diffuse + specular), 0.0);
		//return vec4(0.0);
	//}
}


void main()
{
#if DEPTH_ONLY
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
#else
	vec3 esFrag = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).xyz;
#endif

#if 1
	// Early out, light is behind geometry (effectively the same as stencil deferred, but not-shit).
	if (-VertexIn.esVert.z > -esFrag.z)
	{
		discard;
		return;
	}
#endif

#if 0
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	color = getAmbient(material, 0.2);
#endif
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);

	vec4 color = vec4(0.0, 0.0, 0.0, 0.0);

	vec3 viewDir = normalize(-esFrag.xyz);
	normal = normalize(normal);

	vec4 ambient = vec4(material.xyz * 0.2, material.w);
	//color = ambient;

	// TODO: Directional light maybe?

	// Sphere light.
	if (indexOffset == 0)
	{
		float radius = sphereLightRadii[VertexIn.id];
		vec4 lightPos = mvMatrix * vec4(sphereLightPositions[VertexIn.id].xyz, 1.0);
		float dist = distance(lightPos.xyz, esFrag);

		if (dist > radius)
		{
			discard;
			return;
		}

		vec4 lightColor = floatToRGBA8(lightColors[VertexIn.id]);
		color += sphereLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, radius, dist);
	}

	// Cone light.
	else
	{
		float radius = coneLightDist[VertexIn.id - indexOffset];
		vec4 lightPos = mvMatrix * vec4(coneLightPositions[VertexIn.id - indexOffset].xyz, 1.0);
		float dist = distance(lightPos.xyz, esFrag);

		if (dist > radius)
		{
			discard;
			return;
		}

		vec4 coneDir = mvMatrix * vec4(coneLightDir[VertexIn.id - indexOffset].xyz, 0.0);
		float aperture = coneLightDir[VertexIn.id - indexOffset].w;

		vec4 lightColor = floatToRGBA8(lightColors[VertexIn.id]);
		color += coneLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, radius, dist);
	}

	fragColor = color;
}

