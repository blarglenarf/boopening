#version 430

#define GRID_CELL_SIZE 1
#define OPAQUE 0
#define DEPTH_ONLY 0
#define USE_G_BUFFER 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "cluster"

#include "../../utils.glsl"

#include "../../light/light.glsl"

CLUSTER_DEC(cluster);

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform uint nSphereLights;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

out vec4 fragColor;



void main()
{
#if DEPTH_ONLY
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
#else
	vec3 esFrag = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).xyz;
#endif

	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);

	vec4 color = vec4(0.0, 0.0, 0.0, material.w);

	vec3 viewDir = normalize(-esFrag.xyz);
	normal = normalize(normal);

	color.xyz = getAmbient(material.xyz);

	// Convert fragment into cluster space.
	int cluster = CLUSTER_GET(-esFrag.z, MAX_EYE_Z);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Get fragIndex in cluster space.
	int fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, cluster);

	// Apply lighting for all lights at given cluster.
	for (CLUSTER_ITER_BEGIN(cluster, fragIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
	{
		uint id = CLUSTER_GET_DATA(cluster);

		// Sphere.
		if (id < nSphereLights)
		{
			vec4 lightPos = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
			float lightRadius = sphereLightRadii[id];
			float dist = distance(lightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			color += sphereLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, lightRadius, dist);
		}

		// Cone.
		else
		{
			vec4 lightPos = mvMatrix * vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			float lightRadius = coneLightDist[id - nSphereLights];
			float dist = distance(lightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
			color += coneLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		}
	}

	fragColor = color;
}

