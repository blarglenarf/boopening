#version 430

#define DIRECTIONAL_LIGHT 1

#include "../../utils.glsl"

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

out vec4 fragColor;


void main()
{
	vec3 viewDir = normalize(-VertexIn.esFrag);

	vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
	vec3 ambient = Material.ambient.xyz * 0.4;
	if (texFlag)
	{
		vec3 texColor = texture2D(diffuseTex, VertexIn.texCoord).xyz;
		ambient = texColor * 0.4;
	}
	color.xyz = ambient;

#if DIRECTIONAL_LIGHT && 0
	//vec3 lightDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);
	vec3 lightDir = vec3(0, 0, 1);
	float cosTheta = clamp(dot(normal, lightDir), 0.0, 1.0);

	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float dp = max(cosTheta, 0.0);

	if (dp > 0.0)
	{
		diffuse = Material.diffuse.xyz * dp;
		vec3 reflection = reflect(-lightDir, normal);
		float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
		float intensity = pow(nDotH, Material.shininess);
		specular = Material.specular.xyz * intensity;
		color += vec4(diffuse, 0);
	}
#endif

	//LFB_ADD_DATA_L(light, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(1.0);
}

