#version 430

#define LIGHT_COUNT_DEBUG 0
#define COUNT_LIGHT_FRAGS 0
#define PACK_LIGHT_DATA 0

#import "lfb"
#import "lfb_L"

#include "../../utils.glsl"

LFB_DEC(lfb);

#if PACK_LIGHT_DATA
	LFB_DEC_L_1(light);
#else
	LFB_DEC_L(light);
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform mat4 mvMatrix;
uniform uint nSphereLights;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

#include "../../light/light.glsl"

#if COUNT_LIGHT_FRAGS
	buffer FailCount
	{
		uint failCount;
	};

	buffer PassCount
	{
		uint passCount;
	};
#endif

out vec4 fragColor;


void main()
{
	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	vec4 color = vec4(0.0, 0.0, 0.0, 0.2);
	color.xyz = getAmbient();
	color += directionLight(viewDir, normal);

	// Composite light volumes.
#if PACK_LIGHT_DATA
	int fragIndex = LFB_GET_SIZE_L_1(light).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#else
	int fragIndex = LFB_GET_SIZE_L(light).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	// Apply lighting for all lights at given pixel.
#if PACK_LIGHT_DATA
	for (LFB_ITER_BEGIN_L_1(light, fragIndex); LFB_ITER_CHECK_L_1(light); LFB_ITER_INC_L_1(light))
#else
	for (LFB_ITER_BEGIN_L(light, fragIndex); LFB_ITER_CHECK_L(light); LFB_ITER_INC_L(light))
#endif
	{
	#if PACK_LIGHT_DATA
		float f = LFB_GET_DATA_L_1(light);
		int id = int(floatBitsToUint(f));
	#else
		vec2 f = LFB_GET_DATA_L(light);
		int id = int(floatBitsToUint(f.x));
	#endif

		// Sphere.
		if (id < nSphereLights)
		{
			vec4 lightPos = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
			float lightRadius = sphereLightRadii[id];
			float dist = distance(lightPos.xyz, VertexIn.esFrag);

			if (dist > lightRadius)
			{
			#if COUNT_LIGHT_FRAGS
				// Increment a global counter, to see how many fragments fail.
				atomicAdd(failCount, 1);
			#endif
				continue;
			}

		#if COUNT_LIGHT_FRAGS
			atomicAdd(passCount, 1);
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			color += sphereLight(lightColor, lightPos, VertexIn.esFrag, normal, viewDir, lightRadius, dist);
		}

		// Cone.
		else
		{
			vec4 lightPos = mvMatrix * vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			float lightRadius = coneLightDist[id - nSphereLights];
			float dist = distance(lightPos.xyz, VertexIn.esFrag);

			if (dist > lightRadius)
			{
			#if COUNT_LIGHT_FRAGS
				// Increment a global counter, to see how many fragments fail.
				atomicAdd(failCount, 1);
			#endif
				continue;
			}

		#if COUNT_LIGHT_FRAGS
			atomicAdd(passCount, 1);
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
			color += coneLight(lightColor, lightPos, VertexIn.esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		}
	}

#if LIGHT_COUNT_DEBUG
	#if PACK_LIGHT_DATA
		uint count = LFB_COUNT_AT_L_1(light, fragIndex);
	#else
		uint count = LFB_COUNT_AT_L(light, fragIndex);
	#endif
	float dc = float(count) / 64.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

	LFB_ADD_DATA(lfb, gl_FragCoord.xy, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(1.0);
}

