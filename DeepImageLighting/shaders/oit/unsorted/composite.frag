#version 430

#define DRAW_GEOMETRY 0

// This shader only here for light visualisation.

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;

#import "lfb_L"

#include "../../utils.glsl"

// Deferred rendering, either just rendering light volumes or compositing with geometry.
LFB_DEC_L(light);

#if DRAW_GEOMETRY
	#import "lfb"

	LFB_DEC(lfb);
#endif

void main()
{
	fragColor = vec4(1.0);

	// Composite light volumes.
	int lightPixel = LFB_GET_SIZE_L(light).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

#if DRAW_GEOMETRY
	// Need to read from both lfb's simultaneously and composite from them in order.
	int lfbPixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	vec2 currLightFrag = vec2(0);
	vec2 currLfbFrag = vec2(0);

	LFB_ITER_BEGIN_L(light, lightPixel);
	LFB_ITER_BEGIN(lfb, lfbPixel);

	bool lightFlag = false, lfbFlag = false;

	int canary = 1000;
	while ((LFB_ITER_CHECK_L(light)) || (LFB_ITER_CHECK(lfb)))
	{
		if (canary-- < 0)
			break;

		// TODO: Don't iterate this if the lfb fragment was used.
		if (LFB_ITER_CHECK_L(light) && !lfbFlag)
		{
			currLightFrag = LFB_GET_DATA_L(light);
			LFB_ITER_INC_L(light);
		}
		else if (!(LFB_ITER_CHECK_L(light)))
			currLightFrag.y = -1000;

		// TODO: Don't iterate this if the light fragment was used.
		if (LFB_ITER_CHECK(lfb) && !lightFlag)
		{
			currLfbFrag = LFB_GET_DATA(lfb);
			LFB_ITER_INC(lfb);
		}
		else if (!(LFB_ITER_CHECK(lfb)))
			currLfbFrag.y = -1000;

		vec2 f = vec2(0);
		if (currLightFrag.y >= currLfbFrag.y)
		{
			f = currLightFrag;
			lightFlag = true;
			lfbFlag = false;
		}
		else
		{
			f = currLfbFrag;
			lfbFlag = true;
			lightFlag = false;
		}
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#else
	// Read from global memory and composite.
	for (LFB_ITER_BEGIN_L(light, lightPixel); LFB_ITER_CHECK_L(light); LFB_ITER_INC_L(light))
	{
		vec2 f = LFB_GET_DATA_L(light);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif
}

