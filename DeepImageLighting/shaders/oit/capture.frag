#version 430

#import "lfb"

#include "../utils.glsl"

#define DIRECTIONAL_LIGHT 0
#define OPAQUE 0

#if !OPAQUE
	LFB_DEC(lfb, vec2);
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform uint nSphereLights;
uniform uint nConeLights;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer LightColors
{
	float lightColors[];
};


#include "../light/light.glsl"

out vec4 fragColor;


void main()
{
	vec4 color = vec4(0);

	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	color = getAmbient(0.2);
	#if OPAQUE
		color.w = 1.0;
	#endif

	#if DIRECTIONAL_LIGHT
		color += directionLight(vec4(0.5, 0.5, 0.5, 1), vec3(0, 0, 1), viewDir, normal);
	#endif

	for (int i = 0; i < nSphereLights; i++)
	{
		float lightRadius = sphereLightRadii[i];
		vec4 lightPos = mvMatrix * vec4(sphereLightPositions[i].xyz, 1.0);
		float dist = distance(lightPos.xyz, VertexIn.esFrag);

		if (dist > lightRadius)
			continue;

		vec4 lightColor = floatToRGBA8(lightColors[i]);

		color += sphereLight(lightColor, lightPos, VertexIn.esFrag, normal, viewDir, lightRadius, dist);
	}

	for (int i = 0; i < nConeLights; i++)
	{
		float lightRadius = coneLightDist[i];
		vec4 lightPos = mvMatrix * vec4(coneLightPositions[i].xyz, 1.0);
		float dist = distance(lightPos.xyz, VertexIn.esFrag);

		if (dist > lightRadius)
			continue;

		vec4 coneDir = mvMatrix * vec4(coneLightDir[i].xyz, 0.0);
		float aperture = coneLightDir[i].w;

		vec4 lightColor = floatToRGBA8(lightColors[nSphereLights + i]);

		float att;
		color += coneLight(lightColor, lightPos, VertexIn.esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist, att);
	}

	//color.xyz = normal.xyz;
	//color.xyz = vec3(1.0 + VertexIn.esFrag.z / 30.0);
	//color.xyz = Material.diffuse.xyz;

	#if !OPAQUE
		int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
		LFB_ADD_DATA(lfb, pixel, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
	#endif

// Old capture normals stuff that used to be used in the deferred deep images.
#if 0
	vec4 d = vec4(VertexIn.normal, 0.2);

	// TODO: come up with a better way of packing this!!!
	// Convert to coords between 0 and 1, then pack into a float.
	d.x = (d.x + 1.0) * 0.5;
	d.y = (d.y + 1.0) * 0.5;
	d.z = (d.z + 1.0) * 0.5;
	color = d;
	#if !OPAQUE
		LFB_ADD_DATA(lfb, vec2(rgba8ToFloat(d), -VertexIn.esFrag.z));
	#endif
#endif

#if OPAQUE
	fragColor = color;
#else
	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
#endif
}

