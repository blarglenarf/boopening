#version 430

#define CLUSTER_CULL 0
#define CLUSTERS 1
#define MAX_EYE_Z 30.0
#define THREADS_PER_TILE 32

uniform uint nSphereLights;
uniform uint nConeLights;
uniform mat4 mvMatrix;
uniform ivec2 size;

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

#if 0
readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};
#endif

readonly buffer FrustumPlanes
{
	vec4 frustumPlanes[];
};

buffer Offsets
{
	uint offsets[];
};

#if CLUSTER_CULL
	readonly buffer ClusterMasks
	{
		uint clusterMasks[];
	};

	int getCluster(float depth)
	{
		return min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1))));
	}

	uint getMask(float minZ, float maxZ)
	{
		if (minZ < 0)
			minZ = 0;
		if (maxZ < 0)
			maxZ = 0;
		int minCluster = max(getCluster(minZ) - 1, 0);
		int maxCluster = min(getCluster(maxZ) + 1, CLUSTERS - 1);
		uint lightMask = (0xFFFFFFFFU << minCluster) & (0xFFFFFFFFU >> (CLUSTERS - maxCluster));
		return lightMask;
	}
#endif


bool isInFrustum(vec4 plane, vec3 pos, float radius)
{
	// If distance is > radius, then we are outside, otherwise we either intersect or are inside.
	return dot(plane.xyz, pos) + plane.w <= radius;
}


void main()
{
	// 32 threads per frustum.
	int frustum = gl_VertexID / THREADS_PER_TILE;
	int start = gl_VertexID % THREADS_PER_TILE;

	// Basically just here to avoid the uniform not found msg.
#if 1
	ivec2 frustumPos = ivec2(frustum / size.x, frustum % size.y);
	if (frustumPos.x > size.x || frustumPos.y > size.y)
		return;
#endif

#if CLUSTER_CULL
	uint tileMask = clusterMasks[frustum];
#endif

	// Check all lights for intersection with the frustum, each thread checks every 32nd light past its start.
	for (uint id = start; id < nSphereLights + nConeLights; id += THREADS_PER_TILE)
	{
		vec4 osPos = id < nSphereLights ? sphereLightPositions[id] : coneLightPositions[id - nSphereLights];
		vec4 esPos = mvMatrix * vec4(osPos.xyz, 1.0);

		float radius = id < nSphereLights ? sphereLightRadii[id] : coneLightDist[id - nSphereLights];

		float front = -esPos.z - radius;
		float back = -esPos.z + radius;

		// Outside the depth range?
		if (back < 0 || front > MAX_EYE_Z)
			continue;

	#if CLUSTER_CULL
		uint lightMask = getMask(front, back);
		if ((lightMask & tileMask) == 0)
		{
			continue;
		}
	#endif

	#if 0
		// If we're a cone, use the centre of the cone, and half its radius.
		if (id >= nSphereLights)
		{
			radius *= 0.5;
			esPos.xyz += normalize(coneLightDir[id - nSphereLights].xyz) * radius;
			radius *= 1.3;
		}
	#endif

		// Each frustumPlane contains left, right, top, bottom, in that order.
		bool outside = false;
		for (int j = 0; j < 4; j++)
		{
			if (!isInFrustum(frustumPlanes[frustum * 4 + j], esPos.xyz, radius))
			{
				outside = true;
				break;
			}
		}

		// No need to check front and back, since we already checked the depth range.
		if (!outside)
		{
			// Increment the count for valid tiles.
			atomicAdd(offsets[frustum], 1);
		}
	}
}

