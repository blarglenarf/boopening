#version 430

#define LIGHT_DRAW 0
#define GRID_CELL_SIZE 1
#define LIGHT_COUNT_DEBUG 0
#define COUNT_LIGHT_FRAGS 0
#define PACK_LIGHT_DATA 0

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;

#import "lfb"
#import "lfb_L"

#include "../../utils.glsl"

// Deferred rendering, either just rendering light volumes or compositing with geometry.
#if LIGHT_DRAW
	LFB_DEC_L(light);
#else
	// Deferred rendering, using both geometry and lights to composite.
	LFB_DEC(lfb);

	#if PACK_LIGHT_DATA
		LFB_DEC_L_1(light);
	#else
		LFB_DEC_L(light);
	#endif

	uniform MaterialBlock
	{
		vec4 ambient;
		vec4 diffuse;
		vec4 specular;
		float shininess;
	} Material;

	uniform uint nSphereLights;
	uniform ivec4 viewport;
	uniform mat4 mvMatrix;
	uniform mat4 invPMatrix;

	uniform bool texFlag;
	uniform sampler2D diffuseTex;
	uniform sampler2D normalTex;

	readonly buffer SphereLightRadii
	{
		float sphereLightRadii[];
	};

	readonly buffer SphereLightPositions
	{
		vec4 sphereLightPositions[];
	};

	readonly buffer ConeLightDist
	{
		float coneLightDist[];
	};

	readonly buffer ConeLightDir
	{
		vec4 coneLightDir[];
	};

	readonly buffer ConeLightPositions
	{
		vec4 coneLightPositions[];
	};

	readonly buffer LightColors
	{
		float lightColors[];
	};

	#include "../../light/light.glsl"

	vec4 getEyeFromWindow(vec3 p)
	{
		vec3 ndcPos;
		ndcPos.xy = ((p.xy - viewport.xy) / viewport.zw) * 2.0f - 1.0f;
		ndcPos.z = 1.0f;

		vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0f);
		eyeDir /= eyeDir.w;
		vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0f);
		return eyePos;
	}

	// For BMA.
	#define COMPOSITE_LOCAL 0
	#define MAX_FRAGS 512
	#define MAX_FRAGS_OVERRIDE 0

	#if MAX_FRAGS_OVERRIDE != 0
		#define _MAX_FRAGS MAX_FRAGS_OVERRIDE
	#else
		#define _MAX_FRAGS MAX_FRAGS
	#endif

	// Assuming no more than _MAX_FRAGS / 2 number of lights will apply to any one fragment.
	//#define MAX_CURR_LIGHTS _MAX_FRAGS / 2
	#define MAX_CURR_LIGHTS 16
	int currLightIDs[MAX_CURR_LIGHTS]; // FIXME: could possibly do this just with 2 bytes instead of 4, should reduce occupancy issues when there are lots of lights.

	#if COUNT_LIGHT_FRAGS
		buffer FailCount
		{
			uint failCount;
		};

		buffer PassCount
		{
			uint passCount;
		};
	#endif

	// Keep track of what lights are being applied.
	//float lightRadius = 0.75f;
	int minLight = 0;
	int maxLight = 0;

	vec2 testLights(float depth, vec2 currLightFrag)
	{
		// Check what lights need to be applied to the fragment.
	#if PACK_LIGHT_DATA
		if (LFB_ITER_CHECK_L_1(light))
	#else
		if (LFB_ITER_CHECK_L(light))
	#endif
		{
			int id = int(floatBitsToUint(currLightFrag.x));

			// If frag depth < light depth, increment currLight and either add or delete it from currLightIDs.
		#if PACK_LIGHT_DATA
			while (LFB_ITER_CHECK_L_1(light) && depth < currLightFrag.y)
		#else
			while (LFB_ITER_CHECK_L(light) && depth < currLightFrag.y)
		#endif
			{
				// TODO: Come up with a better stepping algorithm than this... might not be possible :(
			#if 1
				// In case you want to keep track of all the lights rather than stepping through them.
				bool found = false;
				int m = maxLight;
				for (int j = 0; j < m; j++)
				{
					if (currLightIDs[j] == id)
					{
						found = true;
				#if 0
						for (int k = j; k < m - 1; k++)
						{
							currLightIDs[k] = currLightIDs[k + 1];
						}
				#else
						currLightIDs[j] = currLightIDs[m - 1];
						if (j == m - 1)
							currLightIDs[j] = -1;
						j--;
						m--;
				#endif
						maxLight--;
						break;
					}
				}
				if (!found && maxLight < MAX_CURR_LIGHTS)
				{
					currLightIDs[maxLight] = id;
					maxLight++;
				}
			#else
				// Only need to check the first one since lights are in sorted order.
				if (MAX_CURR_LIGHTS > minLight && currLightIDs[minLight] == id)
					minLight++;
				else if (maxLight < MAX_CURR_LIGHTS)
				{
					currLightIDs[maxLight] = id;
					maxLight++;
				}
			#endif

			#if PACK_LIGHT_DATA
				LFB_ITER_INC_L_1(light);
				if (LFB_ITER_CHECK_L_1(light))
				{
					currLightFrag = decodeLight(LFB_GET_DATA_L_1(light));
					id = int(floatBitsToUint(currLightFrag.x));
				}
			#else
				LFB_ITER_INC_L(light);
				if (LFB_ITER_CHECK_L(light))
				{
					currLightFrag = LFB_GET_DATA_L(light);
					id = int(floatBitsToUint(currLightFrag.x));
				}
			#endif
			}
		}

		return currLightFrag;
	}

	// TODO: figure out if light colour can be accumulated during light iteration, rather than iterating through them for every fragment. <-- Don't think this is possible.
	vec4 applyLighting(vec2 f)
	{
		// Get the normal vector.
		vec4 d = floatToRGBA8(f.x);
		vec3 n = d.xyz;

		// Coords are 0 to 1, convert to -1 to 1.
		n.x = n.x * 2.0 - 1.0;
		n.y = n.y * 2.0 - 1.0;
		n.z = n.z * 2.0 - 1.0;

		vec4 color = vec4(0.0);

		// Now apply lighting from all lights in currLightIDs.
		vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, -f.y)).xyz;
		vec3 viewDir = normalize(-esFrag);

		for (int j = minLight; j < maxLight; j++)
		{
			int id = currLightIDs[j];
			if (id == -1)
				continue;

			// Sphere.
			if (id < nSphereLights)
			{
				vec4 lightPos = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
				float lightRadius = sphereLightRadii[id];
				float dist = distance(lightPos.xyz, esFrag);

				if (dist > lightRadius)
				{
				#if COUNT_LIGHT_FRAGS
					// Increment a global counter, to see how many fragments fail.
					atomicAdd(failCount, 1);
				#endif
					continue;
				}

			#if COUNT_LIGHT_FRAGS
				atomicAdd(passCount, 1);
			#endif

				vec4 lightColor = floatToRGBA8(lightColors[id]);
				color += sphereLight(lightColor, lightPos, esFrag, n, viewDir, lightRadius, dist);
			}

			// Cone.
			else
			{
				vec4 lightPos = mvMatrix * vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
				float lightRadius = coneLightDist[id - nSphereLights];
				float dist = distance(lightPos.xyz, esFrag);

				if (dist > lightRadius)
				{
				#if COUNT_LIGHT_FRAGS
					// Increment a global counter, to see how many fragments fail.
					atomicAdd(failCount, 1);
				#endif
					continue;
				}

			#if COUNT_LIGHT_FRAGS
				atomicAdd(passCount, 1);
			#endif

				vec4 lightColor = floatToRGBA8(lightColors[id]);
				vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
				float aperture = coneLightDir[id - nSphereLights].w;
				color += coneLight(lightColor, lightPos, esFrag, n, viewDir, coneDir.xyz, aperture, lightRadius, dist);
			}
		}

		return color;
	}
#endif

void main()
{
	fragColor = vec4(0.0);

#if !LIGHT_DRAW
	// Composite using both geometry and light images.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);
	int lfbPixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	#if PACK_LIGHT_DATA
		int lightPixel = LFB_GET_SIZE_L_1(light).x * int(gridCoord.y) + int(gridCoord.x);
	#else
		int lightPixel = LFB_GET_SIZE_L(light).x * int(gridCoord.y) + int(gridCoord.x);
	#endif

	// Initialise current lights.
	for (int i = 0; i < MAX_CURR_LIGHTS; i++)
		currLightIDs[i] = -1;

	vec2 currLightFrag = vec2(0.0);
	#if PACK_LIGHT_DATA
		LFB_ITER_BEGIN_L_1(light, lightPixel);
		if (LFB_ITER_CHECK_L_1(light))
			currLightFrag = decodeLight(LFB_GET_DATA_L_1(light));
	#else
		LFB_ITER_BEGIN_L(light, lightPixel);
		if (LFB_ITER_CHECK_L(light))
			currLightFrag = LFB_GET_DATA_L(light);
	#endif

	// Read from global memory and composite.
	for (LFB_ITER_BEGIN(lfb, lfbPixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		vec2 f = LFB_GET_DATA(lfb);

		vec4 color = vec4(0.0, 0.0, 0.0, 0.2);

		// Go through lights and perform image space collision test.
		currLightFrag = testLights(f.y, currLightFrag);
		color += applyLighting(f);

		fragColor.rgb = mix(fragColor.rgb, color.rgb, color.a);
	}

	#if LIGHT_COUNT_DEBUG
		#if PACK_LIGHT_DATA
			uint count = LFB_COUNT_AT_L_1(light, lightPixel);
		#else
			uint count = LFB_COUNT_AT_L(light, lightPixel);
		#endif
		float dc = float(count) / 128.0;
		dc = sqrt(dc);
		fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
	#endif

#else
	// Composite light volumes.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);
	int pixel = LFB_GET_SIZE_L(light).x * int(gridCoord.y) + int(gridCoord.x);

	// Read from global memory and composite.
	for (LFB_ITER_BEGIN_L(light, pixel); LFB_ITER_CHECK_L(light); LFB_ITER_INC_L(light))
	{
		vec2 f = LFB_GET_DATA_L(light);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif
}

