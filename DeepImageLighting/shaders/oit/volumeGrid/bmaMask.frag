#version 430

#define GRID_CELL_SIZE 1
#define PACK_LIGHT_DATA 0

#import "lfb_L"

#include "../../utils.glsl"

#if PACK_LIGHT_DATA
	LFB_DEC_L_1(LFB_NAME);
#else
	LFB_DEC_L(LFB_NAME);
#endif

uniform int interval;

#define DEBUG 0

#if DEBUG
	out vec4 fragColor;
#endif

void main()
{
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);
#if PACK_LIGHT_DATA
	int pixel = LFB_GET_SIZE_L_1(LFB_NAME).x * int(gridCoord.y) + int(gridCoord.x);
	int fragCount = int(LFB_COUNT_AT_L_1(LFB_NAME, pixel));
#else
	int pixel = LFB_GET_SIZE_L(LFB_NAME).x * int(gridCoord.y) + int(gridCoord.x);
	int fragCount = int(LFB_COUNT_AT_L(LFB_NAME, pixel));
#endif

#if DEBUG
	if (fragCount == 0)
		fragColor = vec4(0.5, 0.0, 0.0, 1.0);
	else if (fragCount <= interval)
		fragColor = vec4(debugColLog(fragCount), 1.0);
	else
		fragColor = vec4(debugColLog(fragCount), 1.0);
#else
	if (fragCount <= interval)
		discard;
#endif
}

