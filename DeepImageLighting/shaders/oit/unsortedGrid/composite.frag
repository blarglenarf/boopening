#version 430

// This shader only here for light visualisation.

#define GRID_CELL_SIZE 1

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;

#import "lfb_L"

#include "../../utils.glsl"

// Deferred rendering, either just rendering light volumes or compositing with geometry.
LFB_DEC_L(light);

void main()
{
	fragColor = vec4(0.0);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);
	int pixel = LFB_GET_SIZE_L(light).x * int(gridCoord.y) + int(gridCoord.x);

	// Read from global memory and composite.
	for (LFB_ITER_BEGIN_L(light, pixel); LFB_ITER_CHECK_L(light); LFB_ITER_INC_L(light))
	{
		vec2 f = LFB_GET_DATA_L(light);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
}

