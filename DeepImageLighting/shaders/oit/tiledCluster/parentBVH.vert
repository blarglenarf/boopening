#version 430

#define THREADS_PER_CLUSTER 1

buffer BVH
{
	float bvh[];
};

uniform uint bvhParents;
uniform uint bvhSize;
uniform int start;


void main()
{
	int i = gl_VertexID + start;
	int child = THREADS_PER_CLUSTER * i + 1;
	if (child >= bvhSize)
		return;

	float depthMin = 10000, depthMax = -10000;
	for (int j = 0; j < THREADS_PER_CLUSTER; j++)
	{
		uint minPos = (child + j) * 2;
		uint maxPos = (child + j) * 2 + 1;
		if (minPos >= bvhSize * 2 || maxPos >= bvhSize * 2)
			continue;
		float minZ = bvh[minPos];
		float maxZ = bvh[maxPos];
		if (minZ < depthMin && minZ != 0) // FIXME: Cheap dirty hack which will fail for value=0.
			depthMin = minZ;
		if (maxZ > depthMax && maxZ != 0) // FIXME: Cheap dirty hack which will fail for value=0.
			depthMax = maxZ;
	}
	if (depthMin != 10000)
		bvh[i * 2] = depthMin;
	if (depthMax != -10000)
		bvh[i * 2 + 1] = depthMax;
}

