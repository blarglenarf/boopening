#version 430

#define THREADS_PER_CLUSTER 1

struct LightKey
{
	uint id;
	float z;
};

readonly buffer LightKeys
{
	LightKey lightKeys[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

buffer BVH
{
	float bvh[];
};

uniform uint nSphereLights;
uniform uint nConeLights;
uniform uint bvhParents;
uniform uint bvhSize;


void main()
{
	uint i = uint(gl_VertexID) * THREADS_PER_CLUSTER;
	if (i > nSphereLights + nConeLights)
		return;

	float depthMin = 10000, depthMax = -10000;
	for (uint j = 0; j < THREADS_PER_CLUSTER && (i + j) < (nSphereLights + nConeLights); j++)
	{
		uint lightID = lightKeys[i + j].id;
		float minZ = 20000, maxZ = -20000;
		if (lightID >= nSphereLights)
		{
			float radius = coneLightDist[lightID - nSphereLights];
			minZ = lightKeys[i + j].z - radius * 1.2f;
			maxZ = lightKeys[i + j].z + radius * 1.2f;
		}
		else
		{
			float radius = sphereLightRadii[lightID];
			minZ = lightKeys[i + j].z - radius * 1.2f;
			maxZ = lightKeys[i + j].z + radius * 1.2f;
		}
		if (minZ < depthMin)
			depthMin = minZ;
		if (maxZ > depthMax)
			depthMax = maxZ;
	}
	uint loc = bvhParents + (i / THREADS_PER_CLUSTER);
	if (loc * 2 < bvhSize * 2)
		bvh[loc * 2] = depthMin;
	if (loc * 2 + 1 < bvhSize * 2)
		bvh[loc * 2 + 1] = depthMax;
}

