#version 430

#define CLUSTER_CULL 0
#define CLUSTERS 1
#define MAX_EYE_Z 30.0
#define MASK_SIZE 1
#define THREADS_PER_CLUSTER 32

#import "cluster"

CLUSTER_DEC(light, uint);

uniform uint nSphereLights;
uniform uint nConeLights;
uniform mat4 mvMatrix;

uniform uint bvhParents;
uniform uint bvhSize;

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer FrustumPlanes
{
	vec4 frustumPlanes[];
};

readonly buffer LightIDs
{
	uint lightIDs[];
};

readonly buffer BVH
{
	float bvh[];
};


#if CLUSTER_CULL
	readonly buffer ClusterMasks
	{
		uint clusterMasks[];
	};

	int getCluster(float depth)
	{
		return min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1))));
	}

	uint getMask(float minZ, float maxZ)
	{
		if (minZ < 0)
			minZ = 0;
		if (maxZ < 0)
			maxZ = 0;
		int minCluster = max(getCluster(minZ) - 1, 0);
		int maxCluster = min(getCluster(maxZ) + 1, CLUSTERS - 1);
		uint lightMask = (0xFFFFFFFFU << minCluster) & (0xFFFFFFFFU >> (CLUSTERS - maxCluster));
		return lightMask;
	}

	bool checkBit(int fragIndex, int cluster)
	{
		int maskIndex = cluster / (MASK_SIZE * 32);
		int clusterIndex = cluster % 32;
		return (clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
	}
#endif


bool isInFrustum(vec4 plane, vec3 pos, float radius)
{
	// If distance is > radius, then we are outside, otherwise we either intersect or are inside.
	return dot(plane.xyz, pos) + plane.w <= radius;
}

int traverseBVH(float clusterMin, float clusterMax, bool left)
{
	int parent = 0;

	// Root node has only 1, rest have THREADS_PER_CLUSTER.
	int nodesInBlock = 1;

	while (parent < bvhSize)
	{
		// Left traversal, start at the first node, and sweep right, jump into the first intersecting node.
		if (left)
		{
			// If no depth ranges in this group intersect, then there isn't an intersection.
			bool found = false;
			int child = 0;
			for (int i = 0; i < nodesInBlock; i++)
			{
				float depthMin = bvh[(parent + i) * 2];
				float depthMax = bvh[(parent + i) * 2 + 1];

				if (!(depthMax < clusterMin || depthMin > clusterMax))
				{
					// Depth first traversal.
					found = true;
					child = (parent + i) * THREADS_PER_CLUSTER + 1;

					// If child exists, traverse into it.
					if (child < bvhSize)
					{
						parent = child;
						break;
					}

					// Otherwise it doesn't exist, and we've found the depth range.
					else
						return parent + i;
				}
			}

			// If we checked all nodes and found no intersection, exit.
			if (!found)
				return -1;
		}

		// Right traversal, start at the last node, and sweep left, jump into the first intersecting node.
		else
		{
			// If no depth ranges in this group intersect, then there isn't an intersection.
			bool found = false;
			int child = 0;
			for (int i = nodesInBlock - 1; i >= 0; i--)
			{
				float depthMin = bvh[(parent + i) * 2];
				float depthMax = bvh[(parent + i) * 2 + 1];

				// If there are only zeroes, then the depth range isn't populated. Go to the next one.
				if (depthMin == 0 && depthMax == 0)
					continue;

				if (!(depthMax < clusterMin || depthMin > clusterMax))
				{
					// Depth first traversal.
					found = true;
					child = (parent + i) * THREADS_PER_CLUSTER + 1;

					// If child exists, traverse into it.
					if (child < bvhSize)
					{
						parent = child;
						break;
					}

					// Otherwise it doesn't exist, and we've found the depth range.
					else
						return parent + i;
				}
			}

			// If we checked all nodes and found no intersection, exit.
			if (!found)
				return -1;
		}

		nodesInBlock = THREADS_PER_CLUSTER;
	}

	// This shouldn't happen.
	return -1;
}


void main()
{
	// 32 threads per tile.
	int frustum = gl_VertexID / THREADS_PER_CLUSTER;

#if CLUSTER_CULL	
	uint tileMask = clusterMasks[frustum];
#endif

// Basically just here to avoid the uniform not found msg.
#if 1
	ivec2 tiles = CLUSTER_GET_SIZE(light);
	ivec2 frustumPos = ivec2(frustum / tiles.x, frustum % tiles.y);
	if (frustumPos.x > tiles.x || frustumPos.y > tiles.y)
		return;
#endif

	// Iterate through clusters in tile.
	for (int cluster = 0; cluster < CLUSTERS; cluster++)
	{
	#if CLUSTER_CULL
		// Don't bother if cluster contains no geometry.
		if (!checkBit(frustum, cluster))
			continue;
	#endif

		int clusterIndex = frustum * CLUSTERS + cluster;
		float minDepth = (float(cluster) / float(CLUSTERS)) * MAX_EYE_Z;
		float maxDepth = (float(cluster + 1) / float(CLUSTERS)) * MAX_EYE_Z;

		// Traverse the bvh to find the first and last intersecting depth ranges.
		// TODO: No longer necessary, can just use a sliding window of depth ranges.
		int leftParent = traverseBVH(minDepth, maxDepth, true);
		int rightParent = traverseBVH(minDepth, maxDepth, false);
		if (leftParent == -1 || rightParent == -1)
			continue;

		// Get the range associated with the parents.
		if (THREADS_PER_CLUSTER * leftParent + 1 < bvhSize || THREADS_PER_CLUSTER * rightParent + 1 < bvhSize)
			return;
		uint start = (leftParent - bvhParents) * THREADS_PER_CLUSTER + (gl_VertexID % THREADS_PER_CLUSTER);
		uint end = (rightParent - bvhParents) * THREADS_PER_CLUSTER + THREADS_PER_CLUSTER;

		// Check all lights for intersection with the frustum, each thread checks every 32nd light past its start.
		for (uint id = start; id < end; id += THREADS_PER_CLUSTER)
		{
			uint lightID = lightIDs[id];
			vec4 osPos = lightID < nSphereLights ? sphereLightPositions[lightID] : coneLightPositions[lightID - nSphereLights];
			vec4 esPos = mvMatrix * vec4(osPos.xyz, 1.0);

			float radius = lightID < nSphereLights ? sphereLightRadii[lightID] : coneLightDist[lightID - nSphereLights];

			float front = -esPos.z - radius;
			float back = -esPos.z + radius;

			// Outside the depth range?
			if (back < minDepth || front > maxDepth)
				continue;

		#if CLUSTER_CULL
			// This should never actually happen, since we know the cluster exists, and the light is inside the depth range.
			uint lightMask = getMask(front, back);
			if ((lightMask & tileMask) == 0)
			{
				continue;
			}
		#endif

			// Each frustumPlane contains left, right, top, bottom, in that order.
			bool outside = false;
			for (int j = 0; j < 4; j++)
			{
				if (!isInFrustum(frustumPlanes[frustum * 4 + j], esPos.xyz, radius))
				{
					outside = true;
					break;
				}
			}

			if (!outside)
			{
				CLUSTER_ADD_DATA(light, clusterIndex, lightID);
			}
		}
	}
}

