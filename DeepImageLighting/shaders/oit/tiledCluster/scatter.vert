#version 430

#define CLUSTER_CULL 0
#define CLUSTERS 1
#define MAX_EYE_Z 30.0
#define MASK_SIZE 1

#import "cluster"

CLUSTER_DEC(light, uint);

uniform uint nSphereLights;
uniform mat4 mvMatrix;
uniform mat4 pMatrix;

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer FrustumPlanes
{
	vec4 frustumPlanes[];
};


#if CLUSTER_CULL
	readonly buffer ClusterMasks
	{
		uint clusterMasks[];
	};

	int getCluster(float depth)
	{
		return min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1))));
	}

	uint getMask(float minZ, float maxZ)
	{
		if (minZ < 0)
			minZ = 0;
		if (maxZ < 0)
			maxZ = 0;
		int minCluster = max(getCluster(minZ) - 1, 0);
		int maxCluster = min(getCluster(maxZ) + 1, CLUSTERS - 1);
		uint lightMask = (0xFFFFFFFFU << minCluster) & (0xFFFFFFFFU >> (CLUSTERS - maxCluster));
		return lightMask;
	}

	bool checkMask(uint lightMask, int tile)
	{
		uint tileMask = clusterMasks[tile];
		uint mask = lightMask & tileMask;
		return mask != 0;
	}

	bool checkBit(int fragIndex, int cluster)
	{
		int maskIndex = cluster / (MASK_SIZE * 32);
		int clusterIndex = cluster % 32;
		return (clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
	}
#endif


bool isInFrustum(vec4 plane, vec3 pos, float radius)
{
	// If distance is > radius, then we are outside, otherwise we either intersect or are inside.
	return dot(plane.xyz, pos) + plane.w <= radius;
}

bool checkFrustum(vec4 esPos, float radius, int width, int x, int y)
{
	int tile = width * y + x;

	// Each frustumPlane contains left, right, top, bottom, in that order.
	for (int j = 0; j < 4; j++)
	{
		if (!isInFrustum(frustumPlanes[tile * 4 + j], esPos.xyz, radius))
		{
			return false;
		}
	}
	return true;
}


void main()
{
	int id = gl_VertexID;

	vec4 osPos = id < nSphereLights ? sphereLightPositions[id] : coneLightPositions[id - nSphereLights];
	vec4 esPos = mvMatrix * vec4(osPos.xyz, 1.0);

	float radius = id < nSphereLights ? sphereLightRadii[id] : coneLightDist[id - nSphereLights];

	float front = -esPos.z - radius;
	float back = -esPos.z + radius;

#if CLUSTER_CULL
	uint lightMask = getMask(front, back);
#endif

	// Outside the depth range?
	if (back < 0 || front > MAX_EYE_Z)
		return;

	ivec2 tiles = CLUSTER_GET_SIZE(light);

	// Needed so we can determine what tiles are intersected.
	vec4 csPos = pMatrix * esPos;
	vec2 ssPos = ((vec2(csPos.xy / csPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(tiles.x, tiles.y);

	int centreX = clamp(int(ssPos.x), 0, tiles.x - 1);
	int centreY = clamp(int(ssPos.y), 0, tiles.y - 1);

	int minX = -1, maxX = -1;
	int minY = -1, maxY = -1;

	// Start with centre frustum, go up, then down to find max/min y extents.
	for (int y = centreY; y < tiles.y; y++)
	{
		if (checkFrustum(esPos, radius, tiles.x, centreX, y))
			maxY = y;
		else
			break;
	}
	for (int y = centreY; y >= 0; y--)
	{
		if (checkFrustum(esPos, radius, tiles.x, centreX, y))
			minY = y;
		else
			break;
	}

	// Then from centre frustum, go right then left to find max/min x extents.
	for (int x = centreX; x < tiles.x; x++)
	{
		if (checkFrustum(esPos, radius, tiles.x, x, centreY))
			maxX = x;
		else
			break;
	}
	for (int x = centreX; x >= 0; x--)
	{
		if (checkFrustum(esPos, radius, tiles.x, x, centreY))
			minX = x;
		else
			break;
	}

	if (minX == -1 || minY == -1 || maxX == -1 || minY == -1)
		return;

	int minCluster = CLUSTER_GET(front, MAX_EYE_Z);
	int maxCluster = CLUSTER_GET(back, MAX_EYE_Z);

	for (int y = minY; y <= maxY; y++)
	{
		for (int x = minX; x <= maxX; x++)
		{
		#if CLUSTER_CULL
			if (!checkMask(lightMask, tiles.x * y + x))
			{
				continue;
			}
		#endif

			// Go through clusters between front and back.
			for (int cluster = minCluster; cluster <= maxCluster; cluster++)
			{
				int clusterIndex = CLUSTER_GET_INDEX(light, ivec2(x, y), cluster);
			#if CLUSTER_CULL
				if (!checkBit(tiles.x * y + x, cluster))
				{
					continue;
				}
			#endif
				CLUSTER_ADD_DATA(light, clusterIndex, id);
			}
		}
	}
}

