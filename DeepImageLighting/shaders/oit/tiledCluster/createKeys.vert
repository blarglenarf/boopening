#version 430

struct LightKey
{
	uint id;
	float z;
};

buffer LightKeys
{
	LightKey lightKeys[];
};

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

uniform uint nSphereLights;
uniform mat4 mvMatrix;


void main()
{
	uint id = uint(gl_VertexID);

	vec4 esVert;
	if (id < nSphereLights)
		esVert = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
	else
		esVert = mvMatrix * vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);

	lightKeys[id].id = id;
	lightKeys[id].z = -esVert.z;
}

