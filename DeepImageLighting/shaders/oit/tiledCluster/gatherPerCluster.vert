#version 430

#define CLUSTER_CULL 0
#define CLUSTERS 1
#define MAX_EYE_Z 30.0
#define MASK_SIZE 1
#define THREADS_PER_CLUSTER 1
#define USE_BVH 1

#include "../../utils.glsl"

#import "cluster"

CLUSTER_DEC(light, uint);

uniform uint nSphereLights;
uniform uint nConeLights;
uniform mat4 mvMatrix;

uniform mat4 invPMatrix;
uniform ivec4 viewport;
uniform mat4 invMVMatrix;

uniform uint bvhParents;
uniform uint bvhSize;

#if USE_BVH
	readonly buffer LightPositions
	{
		vec4 lightPositions[];
	};

	readonly buffer LightRadii
	{
		float lightRadii[];
	};
#else
	readonly buffer SphereLightPositions
	{
		vec4 sphereLightPositions[];
	};

	readonly buffer SphereLightRadii
	{
		float sphereLightRadii[];
	};

	readonly buffer ConeLightPositions
	{
		vec4 coneLightPositions[];
	};

	readonly buffer ConeLightDist
	{
		float coneLightDist[];
	};
#endif

readonly buffer FrustumPlanes
{
	vec4 frustumPlanes[];
};

readonly buffer LightIDs
{
	uint lightIDs[];
};

readonly buffer BVH
{
	float bvh[];
};


#if CLUSTER_CULL
	readonly buffer ClusterIndices
	{
		uint clusterIndices[];
	};

	readonly buffer ClusterMasks
	{
		uint clusterMasks[];
	};

	int getCluster(float depth)
	{
		return min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1))));
	}

	uint getMask(float minZ, float maxZ)
	{
		if (minZ < 0)
			minZ = 0;
		if (maxZ < 0)
			maxZ = 0;
		int minCluster = max(getCluster(minZ) - 1, 0);
		int maxCluster = min(getCluster(maxZ) + 1, CLUSTERS - 1);
		uint lightMask = (0xFFFFFFFFU << minCluster) & (0xFFFFFFFFU >> (CLUSTERS - maxCluster));
		return lightMask;
	}

	bool checkBit(int fragIndex, int cluster)
	{
		int maskIndex = cluster / (MASK_SIZE * 32);
		int clusterIndex = cluster % 32;
		return (clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
	}
#endif


bool isInFrustum(vec4 plane, vec3 pos, float radius)
{
	// If distance is > radius, then we are outside, otherwise we either intersect or are inside.
	return dot(plane.xyz, pos) + plane.w <= radius;
}

int traverseBVH(float clusterMin, float clusterMax, bool left)
{
	int parent = 0;

	// Root node has only 1, rest have THREADS_PER_CLUSTER.
	int nodesInBlock = 1;

	while (parent < bvhSize)
	{
		// Left traversal, start at the first node, and sweep right, jump into the first intersecting node.
		if (left)
		{
			// If no depth ranges in this group intersect, then there isn't an intersection.
			bool found = false;
			int child = 0;
			for (int i = 0; i < nodesInBlock; i++)
			{
				float depthMin = bvh[(parent + i) * 2];
				float depthMax = bvh[(parent + i) * 2 + 1];

				if (!(depthMax < clusterMin || depthMin > clusterMax))
				{
					// Depth first traversal.
					found = true;
					child = (parent + i) * THREADS_PER_CLUSTER + 1;

					// If child exists, traverse into it.
					if (child < bvhSize)
					{
						parent = child;
						break;
					}

					// Otherwise it doesn't exist, and we've found the depth range.
					else
						return parent + i;
				}
			}

			// If we checked all nodes and found no intersection, exit.
			if (!found)
				return -1;
		}

		// Right traversal, start at the last node, and sweep left, jump into the first intersecting node.
		else
		{
			// If no depth ranges in this group intersect, then there isn't an intersection.
			bool found = false;
			int child = 0;
			for (int i = nodesInBlock - 1; i >= 0; i--)
			{
				float depthMin = bvh[(parent + i) * 2];
				float depthMax = bvh[(parent + i) * 2 + 1];

				// If there are only zeroes, then the depth range isn't populated. Go to the next one.
				if (depthMin == 0 && depthMax == 0)
					continue;

				if (!(depthMax < clusterMin || depthMin > clusterMax))
				{
					// Depth first traversal.
					found = true;
					child = (parent + i) * THREADS_PER_CLUSTER + 1;

					// If child exists, traverse into it.
					if (child < bvhSize)
					{
						parent = child;
						break;
					}

					// Otherwise it doesn't exist, and we've found the depth range.
					else
						return parent + i;
				}
			}

			// If we checked all nodes and found no intersection, exit.
			if (!found)
				return -1;
		}

		nodesInBlock = THREADS_PER_CLUSTER;
	}

	// This shouldn't happen.
	return -1;
}


bool aabbSphere(vec3 aabbMin, vec3 aabbMax, vec3 sphere, float radius)
{
	float dMin = 0.0;

	if (sphere.x < aabbMin.x)
		dMin += (sphere.x - aabbMin.x) * (sphere.x - aabbMin.x);
	else if (sphere.x > aabbMax.x)
		dMin += (sphere.x - aabbMax.x) * (sphere.x - aabbMax.x);
	if (sphere.y < aabbMin.y)
		dMin += (sphere.y - aabbMin.y) * (sphere.y - aabbMin.y);
	else if (sphere.y > aabbMax.y)
		dMin += (sphere.y - aabbMax.y) * (sphere.y - aabbMax.y);
	if (sphere.z < aabbMin.z)
		dMin += (sphere.z - aabbMin.z) * (sphere.z - aabbMin.z);
	else if (sphere.z > aabbMax.z)
		dMin += (sphere.z - aabbMax.z) * (sphere.z - aabbMax.z);
	return dMin <= (radius * radius);
}


void main()
{
	// 32 threads per cluster.
#if CLUSTER_CULL
	int clusterIndex = int(clusterIndices[gl_VertexID / THREADS_PER_CLUSTER]);
	int frustum = clusterIndex / CLUSTERS;
	int cluster = clusterIndex - (frustum * CLUSTERS);
#else
	int frustum = gl_VertexID / CLUSTERS / THREADS_PER_CLUSTER;
	int cluster = (gl_VertexID - (frustum * CLUSTERS * THREADS_PER_CLUSTER)) / THREADS_PER_CLUSTER;
	int clusterIndex = frustum * CLUSTERS + cluster;
#endif

#if CLUSTER_CULL
	// Don't bother if cluster contains no geometry.
	// Cluster should always contain geometry however, since we're only rendering for active clusters.
	if (!checkBit(frustum, cluster))
		return;
#endif

	float minDepth = (float(cluster) / float(CLUSTERS)) * MAX_EYE_Z;
	float maxDepth = (float(cluster + 1) / float(CLUSTERS)) * MAX_EYE_Z;

	// Basically just here to avoid the uniform not found msg.
#if 1
	ivec2 tiles = CLUSTER_GET_SIZE(light);
	ivec2 frustumPos = ivec2(frustum % tiles.x, frustum / tiles.x);
	if (frustumPos.x > tiles.x || frustumPos.y > tiles.y)
		return;
#endif

#if CLUSTER_CULL && 0
	uint tileMask = clusterMasks[frustum];
#endif

	// TODO: store the lights locally, and add them to the lfb all at once (maybe, only if this ends up being too slow).

#if USE_BVH
	// Traverse the bvh to find the first and last intersecting depth ranges.
	int leftParent = traverseBVH(minDepth, maxDepth, true);
	int rightParent = traverseBVH(minDepth, maxDepth, false);
	if (leftParent == -1 || rightParent == -1)
		return;

	// Get the range associated with the parents.
	if (THREADS_PER_CLUSTER * leftParent + 1 < bvhSize || THREADS_PER_CLUSTER * rightParent + 1 < bvhSize)
		return;
	uint start = (leftParent - bvhParents) * THREADS_PER_CLUSTER + (gl_VertexID % THREADS_PER_CLUSTER);
	uint end = (rightParent - bvhParents) * THREADS_PER_CLUSTER + THREADS_PER_CLUSTER;
#else
	uint start = gl_VertexID % THREADS_PER_CLUSTER;
	uint end = nSphereLights + nConeLights;
#endif

	// Check all lights for intersection with the frustum, each thread checks every 32nd light past its start.
	for (uint id = start; id < end; id += THREADS_PER_CLUSTER)
	{
	#if USE_BVH
		uint lightID = lightIDs[id];
		vec4 osPos = lightPositions[id];
		vec4 esPos = mvMatrix * vec4(osPos.xyz, 1.0);
		float radius = lightRadii[id];
	#else
		uint lightID = id;
		vec4 osPos = lightID < nSphereLights ? sphereLightPositions[lightID] : coneLightPositions[lightID - nSphereLights];
		vec4 esPos = mvMatrix * vec4(osPos.xyz, 1.0);
		float radius = lightID < nSphereLights ? sphereLightRadii[lightID] : coneLightDist[lightID - nSphereLights];
	#endif

		// Coarse test of geometry min/max z against min/max light depth.
		float front = -esPos.z - radius;
		float back = -esPos.z + radius;

		if (back < minDepth || front > maxDepth)
			continue;

#if 0
		// I found this approach to be *really* slow, because eye-space aabb is some massive volume covering half the screen.
		// Instead of all that other crap, just going to do an aabb-sphere collision test.
		vec4 aabbMin = getEyeFromWindow(vec3(frustumPos.x, frustumPos.y, -1), viewport, invPMatrix);
		vec4 aabbMax = getEyeFromWindow(vec3(frustumPos.x + 1, frustumPos.y + 1, 1), viewport, invPMatrix);
		aabbMin /= aabbMin.w;
		aabbMax /= aabbMax.w;
		aabbMin.z = -maxDepth;
		aabbMax.z = -minDepth;
		bool outside = aabbSphere(aabbMin.xyz, aabbMax.xyz, esPos.xyz, radius);
		if (!outside)
		{
			CLUSTER_ADD_DATA(light, lightID, clusterIndex);
		}
#else
	#if CLUSTER_CULL && 0
		// This should never actually happen, since we know the cluster exists, and the light is inside the depth range.
		uint lightMask = getMask(front, back);
		if ((lightMask & tileMask) == 0)
		{
			continue;
		}
	#endif

	#if 0
		// If we're a cone, use the centre of the cone, and half its radius.
		if (id >= nSphereLights)
		{
			radius *= 0.5;
			esLightPos += normalize(VertexIn.dir.xyz) * radius;
			radius *= 1.3;
		}
	#endif

	#if 1
		// Each frustumPlane contains left, right, top, bottom, in that order. This is where bulk of performance comes from.
		bool outside = false;
		for (int j = 0; j < 4; j++)
		{
			if (!isInFrustum(frustumPlanes[frustum * 4 + j], esPos.xyz, radius))
			{
				outside = true;
				break;
			}
		}

		if (outside)
			continue;
	#endif

	#if 0
		// Can apply the same depth refinement as hybrid lighting, seems to not improve performance... weird.
		vec3 dir = normalize(getEyeFromWindow(vec3(frustumPos.x, frustumPos.y, -30.0), viewport, invPMatrix).xyz);

		if (!sphereIntersection(esPos.xyz, radius, dir, front, back))
			continue;
		if (back < minDepth || front > maxDepth)
			continue;
	#endif

		CLUSTER_ADD_DATA(light, clusterIndex, lightID);
#endif
	}
}

