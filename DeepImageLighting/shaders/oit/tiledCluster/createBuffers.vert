#version 430

struct LightKey
{
	uint id;
	float z;
};

buffer LightPositions
{
	vec4 lightPositions[];
};

buffer LightRadii
{
	float lightRadii[];
};

buffer LightIDs
{
	uint lightIDs[];
};

readonly buffer LightKeys
{
	LightKey lightKeys[];
};

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

uniform uint nSphereLights;


void main()
{
	uint i = uint(gl_VertexID);

	uint id = lightKeys[i].id;
	lightIDs[i] = id;
	if (id < nSphereLights)
	{
		lightPositions[i] = sphereLightPositions[id];
		lightRadii[i] = sphereLightRadii[id];
	}
	else
	{
		lightPositions[i] = coneLightPositions[id - nSphereLights];
		lightRadii[i] = coneLightDist[id - nSphereLights];
	}
}

