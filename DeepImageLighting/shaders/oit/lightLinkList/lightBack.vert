#version 430

layout(location = 0) in vec4 vertex;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

out VertexData
{
	float esDepth;
} VertexOut;

void main()
{
	vec4 osVert = vec4(vertex.xyz, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

	VertexOut.esDepth = -esVert.z;

	gl_Position = csVert;
}

