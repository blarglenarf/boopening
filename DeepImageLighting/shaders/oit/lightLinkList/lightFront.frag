#version 430

#extension GL_NV_gpu_shader5 : enable

#define DEPTH_16 1

#include "../../utils.glsl"

in VertexData
{
	float esDepth;
} VertexIn;

coherent buffer TmpLightDepths
{
#if DEPTH_16
	uint tmpLightDepths[];
#else
	//uvec2 tmpLightDepths[];
	uint64_t tmpLightDepths[];
#endif
};

out vec4 fragColor;

uniform int id;
uniform ivec2 size;


void main()
{
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

#if !DEPTH_16
	// Separate uint for id and depth.
	uint frontDepth = floatBitsToUint(VertexIn.esDepth);
	uint64_t boundsInfo = uint64_t(uint(id) << 32) | uint64_t(frontDepth);
	tmpLightDepths[pixel] = boundsInfo;
#else
	// Encode the light index in the upper 16 bits and the linear depth in the lower 16.
	uint frontDepth = f32ToF16(VertexIn.esDepth);
	uint boundsInfo = (uint(id) << 16) | frontDepth;
	tmpLightDepths[pixel] = boundsInfo;
	//atomicExchange(tmpLightDepths[pixel], boundsInfo);
#endif

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0);
}

