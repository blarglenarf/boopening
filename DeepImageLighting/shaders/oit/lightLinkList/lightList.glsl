#ifndef LIGHT_LIST_GLSL
#define LIGHT_LIST_GLSL

struct LightIterator
{
	uint node;
};

// No extra atomic counters, since binding must be hard coded...
// Just in case the link list lfb is currently being used.
#if USE_ATOMIC_COUNTER
uniform layout(binding = 0, offset = 0) atomic_uint lightFragCount;
#endif

coherent buffer LightHeadPtrs
{
	uint lightHeadPtrs[];
};

coherent buffer LightNextPtrs
{
	uint lightNextPtrs[];
};

// If using DEPTH_16, first float is light id, second is min/max depth with 16 bits for each.
// Otherwise, first float is light id, then min depth, then max depth.
buffer LightData
{
#if DEPTH_16
	vec2 lightData[];
#else
	vec4 lightData[];
#endif
};

LightIterator lightIter;
uniform ivec2 lightSize;
uniform uint lightFragAlloc;

#define LIGHT_ITER_BEGIN(pixel) lightIter.node = lightHeadPtrs[pixel]
#define LIGHT_ITER_INC() lightIter.node = lightNextPtrs[lightIter.node]
#define LIGHT_ITER_CHECK() lightIter.node != 0

#if USE_ATOMIC_COUNTER

#define LIGHT_ADD_DATA(frag) \
	uint currFrag = atomicCounterIncrement(lightFragCount); \
	if (currFrag < lightFragAlloc) \
	{ \
		int pixel = lightSize.x * int(gl_FragCoord.y) + int(gl_FragCoord.x); \
		uint currHead = atomicExchange(lightHeadPtrs[pixel], currFrag); \
		lightNextPtrs[currFrag] = currHead; \
		lightData[currFrag] = frag; \
	}

#endif

#define LIGHT_GET_DATA() lightData[lightIter.node]
#define LIGHT_GET_SIZE() lightSize

#endif

