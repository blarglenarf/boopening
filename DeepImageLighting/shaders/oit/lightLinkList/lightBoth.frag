#version 430

#extension GL_NV_gpu_shader5 : enable

#define DEPTH_16 1
#define DEPTH_LAYERS 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 1

#include "../../utils.glsl"

#include "lightList.glsl"

in VertexData
{
	float esDepth;
	flat int id;
} VertexIn;

coherent buffer TmpLightDepths
{
#if DEPTH_16
	uint tmpLightDepths[];
#else
	//uvec2 tmpLightDepths[];
	uint64_t tmpLightDepths[];
#endif
};

out vec4 fragColor;

uniform ivec2 size;


void main()
{
	// Need to give tmpLightDepths multiple layers.
	uint bufferSize = size.x * size.y;
	uint bufferIndex = uint(VertexIn.id) % DEPTH_LAYERS;
	uint bufferOffset = bufferIndex * bufferSize;

	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

#if !DEPTH_16
	uint depth = floatBitsToUint(VertexIn.esDepth);
	uint64_t newBoundsInfo = uint64_t(VertexIn.id << 32) | uint64_t(depth);
	uint64_t storedBoundsInfo = 0;

	// Need 64 bit atomic operations for this.
	storedBoundsInfo = atomicExchange(tmpLightDepths[pixel + bufferOffset], newBoundsInfo);

	uint storedIndex = uint(storedBoundsInfo >> 32);
	float storedDepth = uintBitsToFloat(uint(storedBoundsInfo & 0xFFFFFFFFU));
#else
	uint depth = f32ToF16(VertexIn.esDepth);
	uint newBoundsInfo = (uint(VertexIn.id) << 16) | depth;

	// Load content that has already been written (if any).
	uint storedBoundsInfo = atomicExchange(tmpLightDepths[pixel + bufferOffset], newBoundsInfo);

	// Decode stored light data.
	uint storedIndex = storedBoundsInfo >> 16;
	float storedDepth = f16ToF32(storedBoundsInfo & 0xFFFFU);
#endif

	// Check if both front and back faces were processed.
	if (storedIndex != VertexIn.id)
	{
		fragColor = vec4(0);
		return;
	}

	// Clear it now that we're done using it.
#if !DEPTH_16
	// Need 64 bit atomic operations for this.
	atomicExchange(tmpLightDepths[pixel + bufferOffset], 0);
#else
	atomicExchange(tmpLightDepths[pixel + bufferOffset], 0);
#endif

	float frontDepth = min(VertexIn.esDepth, storedDepth);
	float backDepth = max(VertexIn.esDepth, storedDepth);

#if !DEPTH_16
	vec4 data = vec4(uintBitsToFloat(VertexIn.id), frontDepth, backDepth, 0);
	LIGHT_ADD_DATA(data);
#else
	uint uFrontDepth = f32ToF16(frontDepth);
	uint uBackDepth = f32ToF16(backDepth);
	uint uDepth = (uFrontDepth << 16) | uBackDepth;

	vec2 data = vec2(uintBitsToFloat(VertexIn.id), uintBitsToFloat(uDepth));
	LIGHT_ADD_DATA(data);
#endif

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0);
}

