#version 430

layout(location = 0) in vec4 vertex;

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

out LightData
{
	vec3 osVert;
	flat int id;
	flat float radius;
} VertexOut;

void main()
{
	vec3 osVert = vertex.xyz;
	VertexOut.osVert = osVert.xyz;
	VertexOut.id = int(vertex.w);
	VertexOut.radius = sphereLightRadii[int(vertex.w)];
}

