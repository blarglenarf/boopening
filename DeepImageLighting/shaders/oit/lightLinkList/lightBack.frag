#version 430

#extension GL_NV_gpu_shader5 : enable

#define DEPTH_16 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 1

#include "../../utils.glsl"

#include "lightList.glsl"

in VertexData
{
	float esDepth;
} VertexIn;

coherent buffer TmpLightDepths
{
#if DEPTH_16
	uint tmpLightDepths[];
#else
	//uvec2 tmpLightDepths[];
	uint64_t tmpLightDepths[];
#endif
};

out vec4 fragColor;

uniform int id;
uniform ivec2 size;


void main()
{
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	float backDepth = VertexIn.esDepth;

	// Decode the stored light index and stored light depth.
#if !DEPTH_16
	uint64_t boundsInfo = tmpLightDepths[pixel];
	int frontIndex = int(boundsInfo >> 32);
	float frontDepth = uintBitsToFloat(uint(boundsInfo & 0xFFFFFFFFU));
	tmpLightDepths[pixel] = 0;
#else
	uint boundsInfo = tmpLightDepths[pixel];
	tmpLightDepths[pixel] = 0;

	int frontIndex = int(boundsInfo >> 16);
	uint uFrontDepth = boundsInfo & 0xFFFFU;
	float frontDepth = f16ToF32(uFrontDepth);
#endif

	// Check if both front and back faces were processed, if not then front face was culled by near clipping plane.
	// Don't bother with the software depth test, since all geometry being tested is transparent.
	if (frontIndex != id)
	{
		frontDepth = 0;
	#if DEPTH_16
		uFrontDepth = 0;
	#endif
	}

#if !DEPTH_16
	vec4 data = vec4(uintBitsToFloat(id), frontDepth, backDepth, 0);
	LIGHT_ADD_DATA(data);
#else
	// Depth packed as two separate 16-bit values.
	uint uBackDepth = f32ToF16(backDepth);
	uint uDepth = (uFrontDepth << 16) | uBackDepth;
	vec2 data = vec2(uintBitsToFloat(id), uintBitsToFloat(uDepth));
	LIGHT_ADD_DATA(data);
#endif

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0);
}

