#version 430

#define DEPTH_16 1
#define LIGHT_COUNT_DEBUG 0
#define COUNT_LIGHT_FRAGS 0
#define GRID_CELL_SIZE 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 0

#import "lfb"

#include "../../utils.glsl"
#include "lightList.glsl"

LFB_DEC(lfb);

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

#include "../../sphereLight/testLight.glsl"

out vec4 fragColor;


void main()
{
	//float lightRadius = 0.75f;

	vec3 viewDir = normalize(-VertexIn.esFrag);

	vec4 color = vec4(0.0, 0.0, 0.0, 0.2);
	color.xyz = getAmbient();

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);
	int fragIndex = LIGHT_GET_SIZE().x * int(gridCoord.y) + int(gridCoord.x);

	// Apply lighting for all lights at given pixel.
	for (LIGHT_ITER_BEGIN(fragIndex); LIGHT_ITER_CHECK(); LIGHT_ITER_INC())
	{
	#if !DEPTH_16
		vec4 f = LIGHT_GET_DATA();
		int id = int(floatBitsToUint(f.x));
		float frontDepth = f.y;
		float backDepth = f.z;
	#else
		vec2 f = LIGHT_GET_DATA();
		int id = int(floatBitsToUint(f.x));
		uint boundsInfo = floatBitsToUint(f.y);
		uint uFrontDepth = boundsInfo >> 16;
		uint uBackDepth = boundsInfo & 0xFFFFU;

		float frontDepth = f16ToF32(uFrontDepth);
		float backDepth = f16ToF32(uBackDepth);
	#endif

		if (-VertexIn.esFrag.z < frontDepth || -VertexIn.esFrag.z > backDepth)
			continue;

		vec4 lightPos = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
		float lightRadius = sphereLightRadii[id];
		float dist = distance(lightPos.xyz, VertexIn.esFrag);

		if (dist > lightRadius)
		{
			continue;
		}

		vec4 lightColor = floatToRGBA8(lightColors[id]);
		float att;
		color += sphereLight(lightColor, lightPos, VertexIn.esFrag, VertexIn.normal, viewDir, lightRadius, dist, true, att);
	}

#if LIGHT_COUNT_DEBUG && 0
	uint count = LIGHT_COUNT_AT(fragIndex);
	float dc = float(count) / 64.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

	LFB_ADD_DATA(lfb, gl_FragCoord.xy, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
}

