#version 430

#define LIGHT_COUNT_DEBUG 0
#define COUNT_LIGHT_FRAGS 0
#define MAX_CURR_LIGHTS 10
#define GRID_CELL_SIZE 1
#define PACK_LIGHT_DATA 0
#define BINARY_SEARCH 0 // Binary searching seems to slow things down :(

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;

#import "lfb"
#import "lfb_L"

#include "../../utils.glsl"

LFB_DEC(lfb);

#if PACK_LIGHT_DATA
	LFB_DEC_L_1(light);
#else
	LFB_DEC_L(light);
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

uniform uint nSphereLights;
uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

#include "../../light/light.glsl"

vec4 getEyeFromWindow(vec3 p)
{
	vec3 ndcPos;
	ndcPos.xy = ((p.xy - viewport.xy) / viewport.zw) * 2.0f - 1.0f;
	ndcPos.z = 1.0f;

	vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0f);
	eyeDir /= eyeDir.w;
	vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0f);
	return eyePos;
}

#define MAX_FRAGS 0
#define MAX_FRAGS_OVERRIDE 0

#if MAX_FRAGS_OVERRIDE != 0
	#define _MAX_FRAGS MAX_FRAGS_OVERRIDE
#else
	#define _MAX_FRAGS MAX_FRAGS
#endif

vec2 frags[_MAX_FRAGS];
int currLightIDs[MAX_CURR_LIGHTS];

#if COUNT_LIGHT_FRAGS
	buffer FailCount
	{
		uint failCount;
	};

	buffer PassCount
	{
		uint passCount;
	};
#endif

// Keep track of what lights are being applied.
int maxLight = 0;
vec2 currLightFrag;

void testLights(int pixel, float depth)
{
	// Check what lights need to be applied to the fragment.
#if PACK_LIGHT_DATA
	if (LFB_ITER_CHECK_L_1(light))
#else
	if (LFB_ITER_CHECK_L(light))
#endif
	{
		int id = int(floatBitsToUint(currLightFrag.x));

		// If frag depth < light depth, increment maxLight and either add or delete it from currLightIDs.
	#if PACK_LIGHT_DATA
		while (LFB_ITER_CHECK_L_1(light) && depth < currLightFrag.y)
	#else
		while (LFB_ITER_CHECK_L(light) && depth < currLightFrag.y)
	#endif
		{
			bool found = false;
		#if BINARY_SEARCH
			// Binary search to find the light id, requires keeping the list in sorted order.
			int lo = 0;
			int hi = maxLight - 1;
			while (lo <= hi)
			{
				int mid = (lo + hi) / 2;
				//int mid = lo + ((hi - lo) / 2);
				if (currLightIDs[mid] > id)
					hi = mid - 1;
				else if (currLightIDs[mid] < id)
					lo = mid + 1;
				else
				{
					// Remove the light, keeping currLights in sorted order.
					found = true;
					for (int j = mid; j < maxLight - 1; j++)
						currLightIDs[j] = currLightIDs[j + 1];
					maxLight--;
					break;
				}
			}
		#else
			// Otherwise iterate through the whole list.
			for (int j = 0; j < maxLight; j++)
			{
				if (currLightIDs[j] == id)
				{
					found = true;
					// In case you want to keep the lights in sorted order.
				#if 0
					for (int k = j; k < maxLight - 1; k++)
						currLightIDs[k] = currLightIDs[k + 1];
				#else
					currLightIDs[j] = currLightIDs[maxLight - 1];
				#endif
					maxLight--;
					break;
				}
			}
		#endif

		#if BINARY_SEARCH
			// If binary searching then insertion sort the id.
			if (!found && maxLight < MAX_CURR_LIGHTS)
			{
				int j = maxLight - 1;
				while (j >= 0 && currLightIDs[j] > id)
				{
					currLightIDs[j + 1] = currLightIDs[j];
					j--;
				}
				currLightIDs[j + 1] = id;
				maxLight++;
			}
		#else
			// Otherwise just add to the end.
			if (!found && maxLight < MAX_CURR_LIGHTS)
				currLightIDs[maxLight++] = id;
		#endif

		#if PACK_LIGHT_DATA
			// TODO: instead of just incrementing to the next light, binary search to find the next one.
			LFB_ITER_INC_L_1(light);
			if (LFB_ITER_CHECK_L_1(light))
			{
				currLightFrag = decodeLight(LFB_GET_DATA_L_1(light));
				id = int(floatBitsToUint(currLightFrag.x));
			}
		#else
			// TODO: instead of just incrementing to the next light, binary search to find the next one.
			LFB_ITER_INC_L(light);
			if (LFB_ITER_CHECK_L(light))
			{
				currLightFrag = LFB_GET_DATA_L(light);
				id = int(floatBitsToUint(currLightFrag.x));
			}
		#endif
		}
	}
}

void applyLighting(vec2 f, int pixel)
{
	// Get the normal vector.
	vec4 d = floatToRGBA8(f.x);
	vec3 n = d.xyz;

	// Coords are 0 to 1, convert to -1 to 1.
	n.x = n.x * 2.0 - 1.0;
	n.y = n.y * 2.0 - 1.0;
	n.z = n.z * 2.0 - 1.0;

	vec4 color = vec4(0.0, 0.0, 0.0, 0.2);

	// Go through lights and perform image space collision test.
	testLights(pixel, f.y);

	// Now apply lighting from all lights between min and max.
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, -f.y)).xyz;
	vec3 viewDir = normalize(-esFrag);

	//for (LFB_ITER_SET(light, pixel, minLight); LFB_ITER_CHECK_MAX(light, maxLight); LFB_ITER_INC(light))
	for (int i = 0; i < maxLight; i++)
	{
		int id = currLightIDs[i];
		if (id == -1)
			continue;

		// Sphere.
		if (id < nSphereLights)
		{
			vec4 lightPos = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
			float lightRadius = sphereLightRadii[id];
			float dist = distance(lightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
			#if COUNT_LIGHT_FRAGS
				// Increment a global counter, to see how many fragments fail.
				atomicAdd(failCount, 1);
			#endif
				continue;
			}

		#if COUNT_LIGHT_FRAGS
			atomicAdd(passCount, 1);
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			color += sphereLight(lightColor, lightPos, esFrag, n, viewDir, lightRadius, dist);
		}

		// Cone.
		else
		{
			vec4 lightPos = mvMatrix * vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			float lightRadius = coneLightDist[id - nSphereLights];
			float dist = distance(lightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
			#if COUNT_LIGHT_FRAGS
				// Increment a global counter, to see how many fragments fail.
				atomicAdd(failCount, 1);
			#endif
				continue;
			}

		#if COUNT_LIGHT_FRAGS
			atomicAdd(passCount, 1);
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
			color += coneLight(lightColor, lightPos, esFrag, n, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		}
	}

	fragColor.rgb = mix(fragColor.rgb, color.rgb, color.a);
}


#include "sort.glsl"


void main()
{
	fragColor = vec4(0.0);

	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);
	int lfbPixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#if PACK_LIGHT_DATA
	int lightPixel = LFB_GET_SIZE_L_1(light).x * int(gridCoord.y) + int(gridCoord.x);
#else
	int lightPixel = LFB_GET_SIZE_L(light).x * int(gridCoord.y) + int(gridCoord.x);
#endif

	currLightFrag = vec2(0.0);
#if PACK_LIGHT_DATA
	LFB_ITER_BEGIN_L_1(light, lightPixel);
	if (LFB_ITER_CHECK_L_1(light))
		currLightFrag = decodeLight(LFB_GET_DATA_L_1(light));
#else
	LFB_ITER_BEGIN_L(light, lightPixel);
	if (LFB_ITER_CHECK_L(light))
		currLightFrag = LFB_GET_DATA_L(light);
#endif

	// Load into local memory and sort.
	int fragCount = sort(lfbPixel, lightPixel);

#if _MAX_FRAGS <= 32 // Dirty hack to account for rbs not sorting in place.
	for (int i = fragCount - 1; i >= 0; i--)
		applyLighting(frags[i], lightPixel);
#endif

#if LIGHT_COUNT_DEBUG
	#if PACK_LIGHT_DATA
		uint count = LFB_COUNT_AT_L_1(light, lightPixel);
	#else
		uint count = LFB_COUNT_AT_L(light, lightPixel);
	#endif
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

