#version 430

#extension GL_NV_gpu_shader5 : enable

#define CLUSTER_CULL 0
#define MASK_SIZE 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 1

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "lfb_L"
#import "cluster_LL"

LFB_DEC(light);
CLUSTER_DEC(cluster);

#include "../../utils.glsl"

#if CLUSTER_CULL
	buffer ClusterMasks
	{
		uint clusterMasks[];
	};
#endif

#define MAX_CURR_LIGHTS 16
uint currLightIDs[MAX_CURR_LIGHTS];

out vec4 color;



#if CLUSTER_CULL
	bool checkBit(int pixel, int cluster)
	{
		int maskIndex = cluster / (MASK_SIZE * 32);
		int clusterIndex = cluster % 32;
		return (clusterMasks[pixel * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
	}
#endif

void main()
{
	for (int i = 0; i < MAX_CURR_LIGHTS; i++)
		currLightIDs[i] = -1;
	int maxLight = 0;

	// Iterate through all light fragments for the given pixel.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy);
	int lightPixel = LFB_GET_SIZE(light).x * gridCoord.y + gridCoord.x;

	vec2 currLightFrag = vec2(0.0);
	int currCluster = -1;

	for (LFB_ITER_BEGIN(light, lightPixel); LFB_ITER_CHECK(light); LFB_ITER_INC(light))
	{
		currLightFrag = LFB_GET_DATA(light);
		uint id = floatBitsToUint(currLightFrag.x);

		// First light in the list, add it.
		if (currCluster == -1)
		{
			currCluster = CLUSTER_GET(currLightFrag.y, MAX_EYE_Z);
			currLightIDs[maxLight++] = id;
			continue;
		}

		int fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, currCluster);
		int newCluster = CLUSTER_GET(currLightFrag.y, MAX_EYE_Z);

		// When entering a new cluster, add all current light ids to the previous cluster.
		if (newCluster != currCluster)
		{
			for (int i = currCluster; i > newCluster; i--)
			{
				fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, i);
				for (int j = 0; j < maxLight; j++)
				{
				#if CLUSTER_CULL
					if (checkBit(lightPixel, i))
					{
						CLUSTER_ADD_DATA(cluster, currLightIDs[j], fragIndex);
					}
				#else
					CLUSTER_ADD_DATA(cluster, currLightIDs[j], fragIndex);
				#endif
				}
			}

			currCluster = newCluster;
			fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, currCluster);
		}

		// Check if it's in the list, and either add or remove it.
		bool found = false;
		int m = maxLight;

		for (int j = 0; j < m; j++)
		{
			if (currLightIDs[j] == id)
			{
				// Leaving the light, but its id still needs to be added to the cluster.
			#if CLUSTER_CULL
				if (checkBit(lightPixel, currCluster))
				{
					CLUSTER_ADD_DATA(cluster, id, fragIndex);
				}
			#else
				CLUSTER_ADD_DATA(cluster, id, fragIndex);
			#endif

				found = true;
				currLightIDs[j] = currLightIDs[m - 1];
				if (j == m - 1)
					currLightIDs[j] = -1;
				j--;
				m--;
				maxLight--;
				break;
			}
		}
		if (!found && maxLight < MAX_CURR_LIGHTS)
		{
			currLightIDs[maxLight] = id;
			maxLight++;
		}
	}

	// Now add the rest to the last cluster.
	int fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, currCluster);
	for (int i = 0; i < maxLight; i++)
	{
	#if CLUSTER_CULL
		if (checkBit(lightPixel, currCluster))
		{
			CLUSTER_ADD_DATA(cluster, currLightIDs[i], fragIndex);
		}
	#else
		CLUSTER_ADD_DATA(cluster, currLightIDs[i], fragIndex);
	#endif
	}

	color = vec4(0);
}

