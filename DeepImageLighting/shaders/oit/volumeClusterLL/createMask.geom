#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

#if 0

in TmpData
{
	vec4 esVert;
	vec3 normal;
} VertexIn[3];

out VertexData
{
	vec3 esFrag;
	vec3 normal;
	flat vec4 aabb;
} VertexOut;

uniform ivec4 viewport;

vec3 extrude(in vec4 prev, in vec4 curr, in vec4 next, in vec2 hPixel)
{	
	vec3 plane[2];

	plane[0] = cross(curr.xyw - prev.xyw, prev.xyw);
	plane[1] = cross(next.xyw - curr.xyw, curr.xyw);
	plane[0].z -= dot(hPixel, abs(plane[0].xy));
	plane[1].z -= dot(hPixel, abs(plane[1].xy));
	return cross(plane[0], plane[1]);
}

void main()
{
	// Pixel size in clip space.
	vec2 pixel = vec2(1.0 / float(viewport.z), 1.0 / float(viewport.w));
	vec2 hPixel = pixel / 2.0;

	vec4 csVertA = gl_in[0].gl_Position;
	vec4 csVertB = gl_in[1].gl_Position;
	vec4 csVertC = gl_in[2].gl_Position;

	vec2 ssVertA = (csVertA.xy / csVertA.w + vec2(1, 1)) * viewport.zw / 2.0;
	vec2 ssVertB = (csVertB.xy / csVertB.w + vec2(1, 1)) * viewport.zw / 2.0;
	vec2 ssVertC = (csVertC.xy / csVertC.w + vec2(1, 1)) * viewport.zw / 2.0;

	vec4 aabb;
	aabb.xy = min(min(ssVertA.xy, ssVertB.xy), ssVertC.xy);
	aabb.zw = max(max(ssVertA.xy, ssVertB.xy), ssVertC.xy);
	aabb.xy -= 1;
	aabb.zw += 1;

	csVertA.xyw = extrude(csVertC, csVertA, csVertB, hPixel);
	csVertB.xyw = extrude(csVertA, csVertB, csVertC, hPixel);
	csVertC.xyw = extrude(csVertB, csVertC, csVertA, hPixel);

	VertexOut.aabb = aabb;
	VertexOut.esFrag = VertexIn[0].esVert.xyz;
	VertexOut.normal = VertexIn[0].normal;
	gl_Position = csVertA;
	EmitVertex();

	VertexOut.aabb = aabb;
	VertexOut.esFrag = VertexIn[1].esVert.xyz;
	VertexOut.normal = VertexIn[1].normal;
	gl_Position = csVertB;
	EmitVertex();

	VertexOut.aabb = aabb;
	VertexOut.esFrag = VertexIn[2].esVert.xyz;
	VertexOut.normal = VertexIn[2].normal;
	gl_Position = csVertC;
	EmitVertex();

	EndPrimitive();
}

#else

// Alternative technique, can't quite get it to work with conservative depth though :(
const float root_2_on_2 = 0.707106781;

in TmpData
{
	vec4 esVert;
	vec3 normal;
} VertexIn[3];

out VertexData
{
	vec3 esFrag;
	vec3 normal;
	flat vec4 aabb;
} VertexOut;

uniform ivec4 viewport;

uniform mat4 invPMatrix;
uniform mat4 pMatrix;

void main()
{
	// Pixel size in clip space.
	vec2 pixel = vec2(1.0 / float(viewport.z), 1.0 / float(viewport.w));
	vec2 hPixel = pixel / 2.0;

	vec4 csVertA = gl_in[0].gl_Position;
	vec4 csVertB = gl_in[1].gl_Position;
	vec4 csVertC = gl_in[2].gl_Position;

	if (csVertA.w != 0)
		csVertA /= csVertA.w;
	if (csVertB.w != 0)
		csVertB /= csVertB.w;
	if (csVertC.w != 0)
		csVertC /= csVertC.w;

	vec2 ssVertA = (csVertA.xy + vec2(1, 1)) * viewport.zw / 2.0;
	vec2 ssVertB = (csVertB.xy + vec2(1, 1)) * viewport.zw / 2.0;
	vec2 ssVertC = (csVertC.xy + vec2(1, 1)) * viewport.zw / 2.0;

	vec4 esVertA = VertexIn[0].esVert;
	vec4 esVertB = VertexIn[1].esVert;
	vec4 esVertC = VertexIn[2].esVert;

	// Extrude eye space z rather than clip space z.
	csVertA.z = esVertA.z;
	csVertB.z = esVertB.z;
	csVertC.z = esVertC.z;

	// Screen space bounding box. // FIXME: this causes problems with triangles that intersect the frustum.
	vec4 aabb;
	aabb.xy = min(min(ssVertA.xy, ssVertB.xy), ssVertC.xy);
	aabb.zw = max(max(ssVertA.xy, ssVertB.xy), ssVertC.xy);
	aabb.xy -= 1;
	aabb.zw += 1;

	// Vectors along edges.
	vec3 tu = vec3(csVertB.xy - csVertA.xy, esVertB.z - esVertA.z);
	vec3 tv = vec3(csVertC.xy - csVertA.xy, esVertC.z - esVertA.z);
	vec3 tw = vec3(csVertC.xy - csVertB.xy, esVertC.z - esVertB.z);

	// Normalized edge vectors.
	vec3 nu = tu / length(tu.xy);
	vec3 nv = tv / length(tv.xy);
	vec3 nw = tw / length(tw.xy);

	// Calculate distances to shift vertices for expanding edges.
	float uv = dot(nu.xy, nv.xy);
	float uw = dot(nu.xy, nw.xy);
	float vw = dot(nv.xy, nw.xy);

	// Degenerate triangle.
	if (uv == 0 || uw == 0 || vw == 0)
		return;

	float o = length(hPixel) * root_2_on_2;
	float ha = o / sqrt(1.0 - uv * uv);
	float hb = o / sqrt(1.0 - uw * uw);
	float hc = o / sqrt(1.0 - vw * vw);

	// Expand edges.
	csVertA.xyz -= (nu + nv) * ha;
	csVertB.xyz += (nu - nw) * hb;
	csVertC.xyz += (nv + nw) * hc;

	csVertA.w = -csVertA.z;
	csVertB.w = -csVertB.z;
	csVertC.w = -csVertC.z;

	// Get clip space z back.
	csVertA.z = pMatrix[2][2] * csVertA.z + pMatrix[2][3];
	csVertB.z = pMatrix[2][2] * csVertB.z + pMatrix[2][3];
	csVertC.z = pMatrix[2][2] * csVertC.z + pMatrix[2][3];

	// Un-normalize.
	csVertA.xy *= csVertA.w;
	csVertB.xy *= csVertB.w;
	csVertC.xy *= csVertC.w;

	// Get the eye space vertices back.
	esVertA = invPMatrix * csVertA;
	esVertB = invPMatrix * csVertB;
	esVertC = invPMatrix * csVertC;

	VertexOut.aabb = aabb;
	VertexOut.esFrag = esVertA.xyz;
	VertexOut.normal = VertexIn[0].normal;
	gl_Position = csVertA;
	EmitVertex();

	VertexOut.aabb = aabb;
	VertexOut.esFrag = esVertB.xyz;
	VertexOut.normal = VertexIn[1].normal;
	gl_Position = csVertB;
	EmitVertex();

	VertexOut.aabb = aabb;
	VertexOut.esFrag = esVertC.xyz;
	VertexOut.normal = VertexIn[2].normal;
	gl_Position = csVertC;
	EmitVertex();

	EndPrimitive();
}
#endif

