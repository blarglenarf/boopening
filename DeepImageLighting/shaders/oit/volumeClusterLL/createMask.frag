#version 430

#define CLUSTERS 1
#define GRID_CELL_SIZE 1
#define MASK_SIZE 1
#define CONSERVATIVE_DEBUG 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#if CONSERVATIVE_DEBUG
	#include "../../dirLight/testLight.glsl"
#endif

#include "../../utils.glsl"

in VertexData
{
	vec3 esFrag;
	vec3 normal;
	flat vec4 aabb;
} VertexIn;

buffer ClusterMasks
{
	uint clusterMasks[];
};

uniform ivec2 size;
uniform mat4 invPMatrix;
uniform ivec4 viewport;

out vec4 fragColor;

void main()
{
#if 1
	// Discard anything outside the aabb of the triangle.
	//if (gl_FragCoord.xy < VertexIn.aabb.xy || gl_FragCoord.xy > VertexIn.aabb.zw)
	if (!(gl_FragCoord.x >= VertexIn.aabb.x && gl_FragCoord.y >= VertexIn.aabb.y && gl_FragCoord.x <= VertexIn.aabb.z && gl_FragCoord.y <= VertexIn.aabb.w))
	{
		discard;
		return;
	}
#endif

#if 0
	// FIXME: Not quite correct due to curvature at edges of the frustum. Instead of doing this you need to raycast against fragment corners!
	//float grad = abs(dFdx(VertexIn.esFrag.z)) + abs(dFdy(VertexIn.esFrag.z)); // FIXME: these functions are very slow :(
	float grad = 0.5;
	float minZ = -VertexIn.esFrag.z - grad;
	float maxZ = -VertexIn.esFrag.z + grad;
#else
	// TODO: Project all 4 corners onto the triangle.
	float minZ = -VertexIn.esFrag.z;
	float maxZ = -VertexIn.esFrag.z;

	// Raycasting against fragment corners.
	// FIXME: these fragment corners need to be projected onto the triangle.
	// TODO: could potentially only do this against two corners, based on normal of the fragment... maybe.
	float x = gl_FragCoord.x - 0.5;
	float y = gl_FragCoord.y - 0.5;

	// FIXME: may need a proper normal vector based on the extruded vertices...
	for (int i = 0; i <= 1; i++)
	{
		for (int j = 0; j <= 1; j++)
		{
			vec3 dir = normalize(getEyeFromWindow(vec3(x, y, -30.0), viewport, invPMatrix).xyz);
			float d = dot(VertexIn.esFrag, VertexIn.normal) / dot(dir, VertexIn.normal);
			float z = (-dir * d).z;
			if (z < minZ)
				minZ = z;
			if (z > maxZ)
				maxZ = z;

			y += 1.0;
		}
		x += 1.0;
	}
#endif

	// If we're rendering at full-res, then divide by GRID_CELL_SIZE, otherwise just use the fragCoord.
	//vec2 gridCoord = gl_FragCoord.xy / GRID_CELL_SIZE;
	vec2 gridCoord = gl_FragCoord.xy;

#if 1
	// Get min and max clusters based on conservative depth.
	int minCluster = min(CLUSTERS - 1, int(minZ / (MAX_EYE_Z / float(CLUSTERS - 1))));
	int maxCluster = min(CLUSTERS - 1, int(maxZ / (MAX_EYE_Z / float(CLUSTERS - 1))));

	minCluster = max(0, minCluster - 1);
	maxCluster = min(CLUSTERS - 1, maxCluster + 1);

	int canary = 0;
	for (int i = minCluster; i <= maxCluster; i++)
	{
		int fragIndex = int(gridCoord.y) * size.x + int(gridCoord.x);

		// Set mask for cluster to 1 (has to be done atomically), if it's already 1 don't bother.
		int maskIndex = i / (MASK_SIZE * 32);
		int clusterIndex = i % 32;

		if ((clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) == 0)
			atomicOr(clusterMasks[fragIndex * MASK_SIZE + maskIndex], uint(1 << clusterIndex));

		if (canary++ > CLUSTERS - 2)
			break;
	}
#else
	// Convert fragment into cluster space.
	int cluster = min(CLUSTERS - 1, int(-VertexIn.esFrag.z / (MAX_EYE_Z / float(CLUSTERS - 1))));

	int fragIndex = int(gridCoord.y) * size.x + int(gridCoord.x);

	// Set mask for cluster to 1 (has to be done atomically), if it's already 1 don't bother.
	int maskIndex = cluster / (MASK_SIZE * 32);
	int clusterIndex = cluster % 32;

	if ((clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) == 0)
		atomicOr(clusterMasks[fragIndex * MASK_SIZE + maskIndex], uint(1 << clusterIndex));
#endif

#if CONSERVATIVE_DEBUG
	// TODO: test using the min and max z values.
	vec3 viewDir = normalize(-VertexIn.esFrag);
	fragColor = dirLight(vec4(1.0), viewDir, VertexIn.esFrag, VertexIn.normal, viewDir);
#else
	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
#endif
}

