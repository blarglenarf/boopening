#version 430

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;

out TmpData
{
	vec4 esVert;
	vec3 normal;
} VertexOut;

void main()
{
	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

	VertexOut.esVert = esVert;
	VertexOut.normal = normalize(normal) * nMatrix;

	gl_Position = csVert;
}

