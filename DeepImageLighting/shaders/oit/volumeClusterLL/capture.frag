#version 430

#define GRID_CELL_SIZE 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "lfb"
#import "cluster_LL"

#include "../../utils.glsl"

LFB_DEC(lfb);
CLUSTER_DEC(cluster);

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform uint nSphereLights;
uniform mat4 mvMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

#include "../../light/light.glsl"

out vec4 fragColor;


void main()
{
	//float lightRadius = 0.75f;

	vec3 viewDir = normalize(-VertexIn.esFrag);

	vec4 color = vec4(0.0, 0.0, 0.0, 0.2);
	color.xyz = getAmbient();

	// Convert fragment into cluster space.
	int cluster = CLUSTER_GET(-VertexIn.esFrag.z, MAX_EYE_Z);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Get fragIndex in cluster space.
	int fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, cluster);

	// Apply lighting for all lights at given cluster.
	for (CLUSTER_ITER_BEGIN(cluster, fragIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
	{
		uint id = CLUSTER_GET_DATA(cluster);

		// Sphere.
		if (id < nSphereLights)
		{
			vec4 lightPos = mvMatrix * vec4(sphereLightPositions[id].xyz, 1.0);
			float lightRadius = sphereLightRadii[id];
			float dist = distance(lightPos.xyz, VertexIn.esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			color += sphereLight(lightColor, lightPos, VertexIn.esFrag, VertexIn.normal, viewDir, lightRadius, dist);
		}

		// Cone.
		else
		{
			vec4 lightPos = mvMatrix * vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			float lightRadius = coneLightDist[id - nSphereLights];
			float dist = distance(lightPos.xyz, VertexIn.esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
			color += coneLight(lightColor, lightPos, VertexIn.esFrag, VertexIn.normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		}
	}

#if LIGHT_COUNT_DEBUG && 0
	uint count = LIGHT_COUNT_AT(fragIndex);
	float dc = float(count) / 64.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

	LFB_ADD_DATA(lfb, gl_FragCoord.xy, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
}

