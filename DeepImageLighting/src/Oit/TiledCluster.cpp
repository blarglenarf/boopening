#include "TiledCluster.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"
#include "../../../Utils/src/Timer.h"

// For sorting using cuda.
#if _CUDA
	#include "LightSort.h"
#endif

#include <string>
#include <iostream>
#include <cassert>
#include <algorithm>

#define MAX_FRAGS 512
#define LIGHT_COUNT_DEBUG 0
#define GATHER 1 // FIXME: Scattering needs updating.
#define CLUSTER_CULL 1
#define GATHER_PER_CLUSTER 1 // FIXME: Gather per tile needs updating.
#define USE_BVH 1
#define THREADS_PER_CLUSTER 32
#define CLUSTER_BLOCK_SIZE 32
#define TEST_FRUSTUMS 0
#define LINEARISED 0


using namespace Rendering;
using namespace Math;


static vec4 getEyeFromWindow(vec3 p, vec4 viewport, mat4 invPMatrix)
{
	vec3 ndcPos;
	ndcPos.xy = ((p.xy - viewport.xy) / viewport.zw) * 2.0f - 1.0f;
	//ndcPos.xy = ((p.xy * 2.0f) - (viewport.xy * 2.0f)) / (viewport.zw) - 1.0f;
	ndcPos.z = 1.0f;
	//std::cout << "ndc: " << ndcPos << "\n";

	vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0f);
	eyeDir /= eyeDir.w;
	vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0f);
	return eyePos;
}

#if TEST_FRUSTUMS
	static vec4 clipToEye(vec4 clip, mat4 invPMatrix)
	{
		vec4 view = invPMatrix * clip;
		view = view / view.w;
		return view;
	}

	static vec4 screenToEye(vec4 screen, vec4 viewport, mat4 invPMatrix)
	{
		vec2 texCoord = screen.xy / viewport.zw;
		vec4 clip = vec4(vec2(texCoord.x, 1.0f - texCoord.y) * 2.0f - 1.0f, screen.z, screen.w);
		return clipToEye(clip, invPMatrix);
	}
#endif

static vec4 computePlane(vec3 p0, vec3 p1, vec3 p2)
{
#if 1
	vec3 v0 = p1 - p0;
	vec3 v2 = p2 - p0;
#else
	vec3 v0 = p0 - p1;
	vec3 v2 = p0 - p2;
#endif
	vec3 n = cross(v0, v2);
	float l = n.len();

	vec4 plane(n.x, n.y, n.z, 0.0f);
	plane /= l;

	// Compute the distance to the origin.
	plane.w = p2.dot(plane.xyz);
	//std::cout << plane.w << "\n";
	//plane.w = 0.0;

	return plane;
}

#if TEST_FRUSTUMS
	// Function to test light intersections with the frustums, just to make sure they're correct.
	static bool isInFrustum(vec4 plane, vec3 pos, float radius)
	{
		//return dot(plane.xyz, pos) + plane.w <= radius;
		float dist = pos.dot(plane.xyz) + plane.w;

		// If distance is < -radius, then we are outside, otherwise we either intersect or are inside.
		if (dist < -radius)
			return false;
		return true;
	}
#endif




void TiledCluster::createLightBVH(App *app)
{
	Utils::Timer timer;

	// Can avoid sorting if the scene hasn't changed, but I need to test performance of the whole thing.
	auto &camera = Renderer::instance().getActiveCamera();
#if 0
	if (prevMvMat == camera.getInverse() && prevNLights == (app->getSphereLightPositions().size() + app->getConeLightPositions().size()) && !createBVHFlag)
		return;
#endif

	prevMvMat = camera.getInverse();

	auto mvMat = Renderer::instance().getActiveCamera().getInverse();

	size_t nSphereLights = app->getNSphereLights();
	size_t nConeLights = app->getNConeLights();
	size_t nLights = nSphereLights + nConeLights;

	app->getProfiler().start("BVH");

#if _CUDA
	// Create the array of light keys for sorting.
	if (nLights != prevNLights)
	{
		destroyLightKeysCudaResource();
		lightKeysBuffer.create(nullptr, sizeof(LightKey) * nLights);
		lightKeysBuffer.bind();
		createLightKeysCudaResource(lightKeysBuffer.getObject());
		lightKeysBuffer.unbind();
	}
#endif

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	createKeysShader.bind();
	createKeysShader.setUniform("LightKeys", &lightKeysBuffer);
	createKeysShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());
	createKeysShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
	createKeysShader.setUniform("nSphereLights", (unsigned int) nSphereLights);
	createKeysShader.setUniform("mvMatrix", mvMat);
	glDrawArrays(GL_POINTS, 0, (GLsizei) nLights);
	createKeysShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glPopAttrib();

	// Sort the light keys based on eye space z.
#if _CUDA
	// Cuda sort.
	lightKeysBuffer.bind();
	sortLightKeysThrust(nLights);
	lightKeysBuffer.unbind();
#else
	// FIXME: Make this work properly.
	// CPU sort.
	/*std::vector<unsigned int> lightIDs(nSphereLights + nConeLights);
	for (unsigned int i = 0; i < lightIDs.size(); i++)
		lightIDs[i] = i;
	std::sort(lightIDs.begin(), lightIDs.end(),
		[nSphereLights, &sphereLights, &coneLights](unsigned int a, unsigned int b)
		{
			float zA = a >= nSphereLights ? coneLights[a - nSphereLights].z : sphereLights[a].z;
			float zB = b >= nSphereLights ? coneLights[b - nSphereLights].z : sphereLights[b].z;
			return zB > zA;
		});*/
#endif

	// Have consolidated both light arrays into a single sorted array for better memory coherence.
	if (nLights != prevNLights)
	{
		lightIDBuffer.create(nullptr, nLights * sizeof(unsigned int));
		lightPosBuffer.create(nullptr, nLights * sizeof(Math::vec4));
		lightRadiiBuffer.create(nullptr, nLights * sizeof(float));
	}

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	createBufferShader.bind();
	createBufferShader.setUniform("nSphereLights", (unsigned int) nSphereLights);
	createBufferShader.setUniform("LightPositions", &lightPosBuffer);
	createBufferShader.setUniform("LightRadii", &lightRadiiBuffer);
	createBufferShader.setUniform("LightIDs", &lightIDBuffer);
	createBufferShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());
	createBufferShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	createBufferShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
	createBufferShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	glDrawArrays(GL_POINTS, 0, (GLsizei) nLights);
	createBufferShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glPopAttrib();

	// Now create another buffer containing depth ranges for each node in the BVH.
	//size_t pow2IDs = Math::isPower2(nLights) ? nLights : Math::nextPower2(nLights);
	int nLevels = (int) ceil(log(nLights / THREADS_PER_CLUSTER) / log(THREADS_PER_CLUSTER)) + 1;

	// By going 1 level less, we get the number of parent nodes.
	bvhParents = (int) (pow(THREADS_PER_CLUSTER, nLevels - 1) - 1) / (THREADS_PER_CLUSTER - 1);

	// Plus the number of leaf nodes.
	bvhSize = bvhParents + (unsigned int) std::max(1U, (unsigned int) (nLights / THREADS_PER_CLUSTER));

	// Each group of 32 lights gets a depth range, then each group of groups etc.

	// First zero the bvh buffer.
	bvhBuffer.create(nullptr, sizeof(float) * bvhSize * 2);
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	zeroBVHShader.bind();
	zeroBVHShader.setUniform("BVH", &bvhBuffer);
	glDrawArrays(GL_POINTS, 0, bvhSize * 2);
	zeroBVHShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glPopAttrib();

	// Then create the lower level of the bvh.
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	createBVHShader.bind();
	createBVHShader.setUniform("nSphereLights", (unsigned int) nSphereLights);
	createBVHShader.setUniform("nConeLights", (unsigned int) nConeLights);
	createBVHShader.setUniform("bvhParents", bvhParents);
	createBVHShader.setUniform("bvhSize", bvhSize);
	createBVHShader.setUniform("LightKeys", &lightKeysBuffer);
	createBVHShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	createBVHShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	createBVHShader.setUniform("BVH", &bvhBuffer);
	glDrawArrays(GL_POINTS, 0, (unsigned int) ceil((float) nLights / (float) THREADS_PER_CLUSTER));
	createBVHShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glPopAttrib();

	// Now the parents.
	// Needs to be populated from the bottom level up.
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	parentBVHShader.bind();
	parentBVHShader.setUniform("bvhParents", bvhParents);
	parentBVHShader.setUniform("bvhSize", bvhSize);
	parentBVHShader.setUniform("BVH", &bvhBuffer);
	unsigned int currParents = bvhParents;
	for (int level = nLevels - 2; level >= 0; level--)
	{
		// How many parents at this level and what is their start position?
		int start = (int) (pow(THREADS_PER_CLUSTER, level) - 1) / (THREADS_PER_CLUSTER - 1);
		int nThreads = currParents - start;
		currParents -= nThreads;
		parentBVHShader.setUniform("start", start);
		glDrawArrays(GL_POINTS, 0, nThreads);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}
	parentBVHShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glPopAttrib();

	app->getProfiler().time("BVH");

	createBVHFlag = false;
	prevNLights = nSphereLights + nConeLights;
}

void TiledCluster::countClusters(App *app)
{
	(void) (app);

	auto &camera = Renderer::instance().getActiveCamera();

	cluster.beginCountFrags();

	// Count the number of frags for capture.
	countLightShader.bind();
	countLightShader.setUniform("mvMatrix", camera.getInverse());
	countLightShader.setUniform("size", Math::ivec2(width, height));
	countLightShader.setUniform("FrustumPlanes", &frustumPlanesBuffer);
#if GATHER
	#if GATHER_PER_CLUSTER && CLUSTER_CULL
		countLightShader.setUniform("ClusterIndices", mask.getCompactIndexBuffer());
	#endif
	#if USE_BVH
		countLightShader.setUniform("LightPositions", &lightPosBuffer);
		countLightShader.setUniform("LightRadii", &lightRadiiBuffer);
		countLightShader.setUniform("LightIDs", &lightIDBuffer);
		countLightShader.setUniform("BVH", &bvhBuffer);
		countLightShader.setUniform("bvhParents", bvhParents);
		countLightShader.setUniform("bvhSize", bvhSize);
	#else
		countLightShader.setUniform("nSphereLights", (unsigned int) app->getNSphereLights());
		countLightShader.setUniform("nConeLights", (unsigned int) app->getNConeLights());
		countLightShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());
		countLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		countLightShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
		countLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
		countLightShader.setUniform("pMatrix", camera.getProjection());
	#endif
#else
	countLightShader.setUniform("nSphereLights", (unsigned int) app->getNSphereLights());
	countLightShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());
	countLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	countLightShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
	countLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	countLightShader.setUniform("pMatrix", camera.getProjection());
#endif
#if CLUSTER_CULL
	mask.setUniforms(&countLightShader);
#endif

	cluster.setCountUniforms(&countLightShader);
#if GATHER
	// If gather per cluster is used, 32 threads per cluster per frustum, otherwise 32 threads per tile.
	#if GATHER_PER_CLUSTER && CLUSTER_CULL
		// Only spawn threads for occupied clusters.
		glDrawArrays(GL_POINTS, 0, mask.getCompactIndexSize() * THREADS_PER_CLUSTER);
	#else
		glDrawArrays(GL_POINTS, 0, width * height * THREADS_PER_CLUSTER);
	#endif
#else
	// If scatter is used, 1 thread per light.
	glDrawArrays(GL_POINTS, 0, app->getNSphereLights() + app->getNConeLights());
#endif
	countLightShader.unbind();
	cluster.endCountFrags();
}

void TiledCluster::createClusters(App *app)
{
	(void) (app);

	auto &camera = Renderer::instance().getActiveCamera();

	cluster.beginComposite();
	captureLightShader.bind();
	cluster.setUniforms(&captureLightShader);
#if CLUSTER_CULL
	mask.setUniforms(&captureLightShader);
#endif

	captureLightShader.setUniform("mvMatrix", camera.getInverse());
	captureLightShader.setUniform("FrustumPlanes", &frustumPlanesBuffer);
#if GATHER
	#if GATHER_PER_CLUSTER && CLUSTER_CULL
		captureLightShader.setUniform("ClusterIndices", mask.getCompactIndexBuffer());
	#endif
	#if USE_BVH
		captureLightShader.setUniform("LightPositions", &lightPosBuffer);
		captureLightShader.setUniform("LightRadii", &lightRadiiBuffer);
		captureLightShader.setUniform("LightIDs", &lightIDBuffer);
		captureLightShader.setUniform("BVH", &bvhBuffer);
		captureLightShader.setUniform("bvhParents", bvhParents);
		captureLightShader.setUniform("bvhSize", bvhSize);
	#else
		captureLightShader.setUniform("nSphereLights", (unsigned int) app->getNSphereLights());
		captureLightShader.setUniform("nConeLights", (unsigned int) app->getNConeLights());
		captureLightShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());
		captureLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		captureLightShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
		captureLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
		captureLightShader.setUniform("pMatrix", camera.getProjection());
	#endif
#else
	captureLightShader.setUniform("nSphereLights", (unsigned int) app->getNSphereLights());
	captureLightShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());
	captureLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureLightShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
	captureLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	captureLightShader.setUniform("pMatrix", camera.getProjection());
	captureLightShader.setUniform("viewport", camera.getViewport());
	captureLightShader.setUniform("invPMatrix", camera.getInverseProj());
	captureLightShader.setUniform("invMVMatrix", camera.getTransform());
#endif

	// Capture fragments into the lfb.
#if GATHER
	// If gather per cluster is used, 32 threads per cluster per frustum, otherwise 32 threads per tile.
	#if GATHER_PER_CLUSTER && CLUSTER_CULL
		// Only spawn threads for occupied clusters.
		glDrawArrays(GL_POINTS, 0, mask.getCompactIndexSize() * THREADS_PER_CLUSTER);
	#else
		glDrawArrays(GL_POINTS, 0, width * height * THREADS_PER_CLUSTER);
	#endif
#else
	// If scatter is used, 1 thread per light.
	glDrawArrays(GL_POINTS, 0, app->getNSphereLights() + app->getNConeLights());
#endif

	captureLightShader.unbind();
	cluster.endComposite();
}


TiledCluster::~TiledCluster()
{
#if _CUDA
	destroyLightKeysCudaResource();
#endif
}

void TiledCluster::init()
{
#if GATHER
	#if GATHER_PER_CLUSTER
		auto lightCount = ShaderSourceCache::getShader("countTiledCluster").loadFromFile("shaders/oit/tiledCluster/countGatherPerCluster.vert");
	#else
		auto lightCount = ShaderSourceCache::getShader("countTiledCluster").loadFromFile("shaders/oit/tiledCluster/countGatherPerTile.vert");
	#endif
#else
	auto lightCount = ShaderSourceCache::getShader("countTiledCluster").loadFromFile("shaders/oit/tiledCluster/countScatter.vert");
#endif

	lightCount.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCount.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCount.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCount.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCount.setDefine("THREADS_PER_CLUSTER", Utils::toString(THREADS_PER_CLUSTER));
	lightCount.setDefine("USE_BVH", Utils::toString(USE_BVH));
	countLightShader.create(&lightCount);

	auto vertSrc = ShaderSourceCache::getShader("createKeys").loadFromFile("shaders/oit/tiledCluster/createKeys.vert");
	createKeysShader.create(&vertSrc);
	vertSrc = ShaderSourceCache::getShader("createBuffers").loadFromFile("shaders/oit/tiledCluster/createBuffers.vert");
	createBufferShader.create(&vertSrc);
	vertSrc = ShaderSourceCache::getShader("zeroBVH").loadFromFile("shaders/oit/tiledCluster/zeroBVH.vert");
	zeroBVHShader.create(&vertSrc);
	vertSrc = ShaderSourceCache::getShader("createBVH").loadFromFile("shaders/oit/tiledCluster/createBVH.vert");
	vertSrc.setDefine("THREADS_PER_CLUSTER", Utils::toString(THREADS_PER_CLUSTER));
	createBVHShader.create(&vertSrc);
	vertSrc = ShaderSourceCache::getShader("parentBVH").loadFromFile("shaders/oit/tiledCluster/parentBVH.vert");
	vertSrc.setDefine("THREADS_PER_CLUSTER", Utils::toString(THREADS_PER_CLUSTER));
	parentBVHShader.create(&vertSrc);
}


void TiledCluster::createClusterMask(App *app)
{
	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	app->getProfiler().start("Cluster Mask");

	if (gBufferFlag)
	{
		mask.createMaskFromDepths(gBuffer.getDepthTexture());
	}
	else
	{
		// Render geometry.
		mask.beginCreateMask();
		app->getGpuMesh().render(false);
		mask.endCreateMask();
	}

	app->getProfiler().time("Cluster Mask");

	if (opaqueFlag == 1)
		glPopAttrib();
}

void TiledCluster::resizeFrustumBuffer()
{
	int nFrustums = width * height;
	int nPlanes = nFrustums * 4;

	frustumPlanes.resize(nPlanes);

	frustumPlanesBuffer.release();
	frustumPlanesBuffer.create(&frustumPlanes[0], frustumPlanes.size() * sizeof(vec4));
}

// Still only need a 2D grid of frustums.
void TiledCluster::createFrustums(App *app)
{
	// FIXME: I think these frustums are not actually correct...
	app->getProfiler().start("Create Frustums");

	// TODO: create these on the GPU, spawn a thread for each frustum.
	// Actually, constructing these on the GPU seems to be the least of my problems.

	mat4 invPMatrix = Renderer::instance().getActiveCamera().getProjection().getInverse();
	ivec4 view = Renderer::instance().getActiveCamera().getViewport();
	vec4 viewport(view.x, view.y, view.z, view.w);
	vec3 eyePos(0, 0, 0);

	int tile = 0;
	//vec2 centre((width) / 2.0, (height) / 2.0);
	//float maxDist = centre.distanceSq(vec2(0, 0));
	vec2 tileSize(viewport.z / width, viewport.w / height);

	// Eye-space corners (bottom-left, top-left, bottom-right, top-right).
	vec4 ssCorner[4];
	ssCorner[0] = vec4(view.x, view.y, -1, 1);
	ssCorner[1] = vec4(view.x, view.y + view.w, -1, 1);
	ssCorner[2] = vec4(view.x + view.z, view.y, -1, 1);
	ssCorner[3] = vec4(view.x + view.z, view.y + view.w, -1, 1);
	vec3 esCorner[4];
	for (int i = 0; i < 4; i++)
		esCorner[i] = getEyeFromWindow(ssCorner[i].xyz, viewport, invPMatrix).xyz;

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			if (x != tile % width || y != tile / width)
				std::cout << "Bad frustums!\n";
			//float dist = centre.distanceSq(vec2(x + 1, y + 1));
			//float w = dist / maxDist;

			// Four points on the far clipping plane.
			vec4 ssPos[4];
			ssPos[0] = vec4(x * tileSize.x, y * tileSize.y, -1, 1);
			ssPos[1] = vec4((x + 1) * tileSize.x, y * tileSize.y, -1, 1);
			ssPos[2] = vec4(x * tileSize.x, (y + 1) * tileSize.y, -1, 1);
			ssPos[3] = vec4((x + 1) * tileSize.x, (y + 1) * tileSize.y, -1, 1);

			vec3 esPos[4];
			for (int i = 0; i < 4; i++)
				esPos[i] = getEyeFromWindow(ssPos[i].xyz, viewport, invPMatrix).xyz;

			// Frustum planes from eye space points, planes are left, right, top, bottom.
			frustumPlanes[tile * 4 + 0] = computePlane(eyePos, esPos[2], esPos[0]);
			frustumPlanes[tile * 4 + 1] = computePlane(eyePos, esPos[1], esPos[3]);
			frustumPlanes[tile * 4 + 2] = computePlane(eyePos, esPos[0], esPos[1]);
			frustumPlanes[tile * 4 + 3] = computePlane(eyePos, esPos[3], esPos[2]);

			//std::cout << "left: " << esCorner[0].x << ", " << esPos[0].x << "\n";
			//std::cout << "right: " << esCorner[3].x << ", " << esPos[3].x << "\n";
			//std::cout << "top: " << esCorner[3].y << ", " << esPos[3].y << "\n";
			//std::cout << "bottom: " << esCorner[0].y << ", " << esPos[0].y << "\n";

			// TODO: w value based on distance from centre. Centre is 1, edge is 0.
			// left distance.
			//frustumPlanes[tile * 4 + 0].w = (esCorner[0].x - esPos[0].x);
			//frustumPlanes[tile * 4 + 0].w = fabsf(esCorner[0].x) - (fabsf(esCorner[0].x) - fabsf(esPos[0].x));
			// right distance.
			//frustumPlanes[tile * 4 + 1].w = (esCorner[3].x - esPos[3].x);
			//frustumPlanes[tile * 4 + 1].w = fabsf(esCorner[3].x) - (fabsf(esCorner[3].x) - fabsf(esPos[3].x));
			// top distance.
			//frustumPlanes[tile * 4 + 2].w = (esCorner[3].y - esPos[3].y);
			//frustumPlanes[tile * 4 + 2].w = fabsf(esCorner[3].y) - (fabsf(esCorner[3].y) - fabsf(esPos[3].y));
			// bottom distance.
			//frustumPlanes[tile * 4 + 3].w = (esCorner[0].y - esPos[0].y);
			//frustumPlanes[tile * 4 + 3].w = fabsf(esCorner[0].y) - (fabsf(esCorner[0].y) - fabsf(esPos[0].y));
			//for (int i = 0; i < 4; i++)
			//	frustumPlanes[tile * 4 + i].w = w;
			tile++;
			//std::cout << "x/y: " << x << ", " << y << "\n";
			//std::cout << "tile: " << tile << ", x/y: " << tile % width << ", " << tile / width << "\n";
		}
	}

#if TEST_FRUSTUMS
	std::vector<std::vector<int>> lightLists(width * height);
	// Test all the frustums to see what lights intersect them.
	for (size_t l = 0; l < app->getNSphereLights(); l++)
	{
		auto &light = app->getSphereLightPositions()[l];
		light.w = 1.0f;
		light = Renderer::instance().getActiveCamera().getInverse() * light;

		auto radius = app->getSphereLightRadii()[l];
		bool inside = true;
		for (size_t i = 0; i < frustumPlanes.size(); i += 4)
		{
			for (size_t j = 0; j < 4; j++)
			{
				if (!isInFrustum(frustumPlanes[i + j], light.xyz, radius))
				{
					inside = false;
					break;
				}
			}

			if (inside)
				lightLists[i / 4].push_back(l);
		}
	}
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int tile = width * y + x;
			std::cout << x << ", " << y << ": ";
			for (auto id : lightLists[tile])
				std::cout << id << ", ";
			std::cout << "\n";
		}
	}
	exit(0);
#endif

	frustumPlanesBuffer.bufferData(&frustumPlanes[0], frustumPlanes.size() * sizeof(vec4));

	app->getProfiler().time("Create Frustums");
}

void TiledCluster::captureLighting(App *app)
{
	glPushAttrib(GL_ENABLE_BIT);

	glEnable(GL_RASTERIZER_DISCARD);

#if GATHER && USE_BVH
	// First thing we do is create the light BVH by sorting light ids based on eye space z.
	createLightBVH(app);
#endif

	app->getProfiler().start("Light Capture");

	// Gather approach spawns a warp per tile, testing all lights for intersection with the tile and testing the depth mask.
	// Scatter approach spawns thread per light, testing which tiles the light intersects and the depth mask.

	if (cluster.getType() == Cluster::LINEARIZED)
		countClusters(app);

	// Capture fragments into the lfb.
	cluster.beginCapture();
	createClusters(app);

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (cluster.endCapture())
	{
		cluster.beginCapture();
		createClusters(app);
		if (cluster.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	glPopAttrib();

	app->getProfiler().time("Light Capture");
}

void TiledCluster::render(App *app)
{
	if (gBufferFlag && opaqueFlag != 1)
		return;

	auto &camera = Renderer::instance().getActiveCamera();

	if (gBufferFlag)
		gBuffer.resize(camera.getWidth(), camera.getHeight());

	// Re-allocate cluster data if the camera has resized.
	if (width != (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE) || height != (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE))
	{
		width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
		height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

		resizeFrustumBuffer();
		cluster.resize(width, height);

	#if CLUSTER_CULL
		mask.resize(width, height);
	#endif
	}

	createFrustums(app);

	if (gBufferFlag)
	{
		gBuffer.beginCapture();
		app->captureGeometry(nullptr, nullptr, &gBuffer.getCaptureShader(), false);
		gBuffer.endCapture();
	}

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	createClusterMask(app);
#endif

	// Capture lighting first (no need to sort).
	captureLighting(app);

	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	// Capture geometry, using light information.
	// Capture geometry, using light clusters to apply lighting.
	if (gBufferFlag)
	{
		app->getProfiler().start("Light Deferred");

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		captureGeometryShader.bind();
		gBuffer.bindTextures();
		gBuffer.setUniforms(&captureGeometryShader);
		app->setCameraUniforms(&captureGeometryShader);
		app->setLightUniforms(&captureGeometryShader);
		#if CLUSTER_CULL
			app->getDepthMask().setUniforms(&captureGeometryShader);
		#endif
		cluster.setUniforms(&captureGeometryShader);

		Renderer::instance().drawQuad();

		gBuffer.unbindTextures();
		captureGeometryShader.unbind();

		app->getProfiler().time("Light Deferred");
	}
	else
		app->captureGeometry(nullptr, &cluster, &captureGeometryShader);

	if (opaqueFlag == 1)
		glPopAttrib();
	else
		// Sort and composite as normal.
		app->sortGeometry();
}

void TiledCluster::update(float dt)
{
	(void) (dt);
}

void TiledCluster::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	createBVHFlag = true;
	prevNLights = 0;
	prevMvMat = Math::mat4();

	auto &camera = Renderer::instance().getActiveCamera();
	width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

	resizeFrustumBuffer();
	createLightBVH(app);

	// Normally you'd want this to be linearized, but light capture is so slow here that it's best to use a link list.
	//cluster.setType(type, "light", fragSize);
#if !LINEARISED
	cluster.setType(Rendering::Cluster::LINK_LIST, "light", CLUSTERS, false, Cluster::UINT, true);
#else
	cluster.setType(Rendering::Cluster::LINEARIZED, "light", CLUSTERS, false, Cluster::UINT, true, 64, 1);
#endif
	//cluster.setType(Rendering::Cluster::LINEARIZED, "light", CLUSTERS);
	//cluster.setMaxFrags(MAX_FRAGS);

	cluster.setProfiler(&app->getProfiler());
	if (gBufferFlag)
		gBuffer.setProfiler(&app->getProfiler());

	// Different shaders for gather or scatter.
#if GATHER
	#if GATHER_PER_CLUSTER
		auto vertLight = ShaderSourceCache::getShader("capTiledCluster").loadFromFile("shaders/oit/tiledCluster/gatherPerCluster.vert");
	#else
		auto vertLight = ShaderSourceCache::getShader("capTiledCluster").loadFromFile("shaders/oit/tiledCluster/gatherPerTile.vert");
	#endif
#else
	auto vertLight = ShaderSourceCache::getShader("capTiledCluster").loadFromFile("shaders/oit/tiledCluster/scatter.vert");
#endif
	vertLight.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	vertLight.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	vertLight.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	vertLight.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	vertLight.setDefine("THREADS_PER_CLUSTER", Utils::toString(THREADS_PER_CLUSTER));
	vertLight.setDefine("USE_BVH", Utils::toString(USE_BVH));

	captureLightShader.release();
	captureLightShader.create(&vertLight);

	// Shader for capturing geometry, applies lighting during capture.
	auto vertCapture = ShaderSourceCache::getShader("captureTiledClusterVert").loadFromFile("shaders/oit/tiledCluster/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureTiledClusterFrag").loadFromFile("shaders/oit/tiledCluster/capture.frag");
	fragCapture.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	fragCapture.setDefine("LIGHT_COUNT_DEBUG", Utils::toString(LIGHT_COUNT_DEBUG));
	fragCapture.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	fragCapture.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	if (gBufferFlag)
	{
		vertCapture.setDefine("USE_G_BUFFER", "1");
		fragCapture.setDefine("USE_G_BUFFER", "1");
	}

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);

	cluster.resize(width, height);
#if CLUSTER_CULL
	#if GATHER && GATHER_PER_CLUSTER
		mask.saveClusterIndices(true);
	#endif
	mask.setProfiler(&app->getProfiler());
	mask.regen(CLUSTERS, MAX_EYE_Z, GRID_CELL_SIZE, false);
	mask.resize(width, height);
	mask.useDepthPrePass(opaqueFlag == 1);
#endif
}

void TiledCluster::saveLFB(App *app)
{
	auto s = cluster.getStrSorted();
	Utils::File f("lightTiledClusterLFB.txt");
	f.write(s);

	app->saveGeometryLFB();
}

