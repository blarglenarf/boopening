#if 0

#include "VolumeClusterLL.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>


// Capture and sort low-res light image.
// Step through light fragments, maintaining an active list, and for each cluster, save the active list of light ids.
// At full res, during capture, iterate through light ids for that cluster and apply lighting.
// Save light ids only for clusters that contain geometry.
// TODO: determine cluster space using farthest fragment, which can be obtained during cluster culling.

#define USE_BMA 1
#define MAX_FRAGS 512
#define CLUSTERS 32
#define MASK_SIZE (CLUSTERS / 32)
#define CLUSTER_CULL 1
#define PROFILE_MEMORY 1
#define CONSERVATIVE_DEBUG 0

using namespace Rendering;


static std::string getZeroMaskShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer ClusterMasks\
	{\
		uint clusterMasks[];\
	};\
	\
	void main()\
	{\
		clusterMasks[gl_VertexID] = 0;\
	}\
	";
}



void VolumeClusterLL::createClusters()
{
	createClusterShader.bind();
	cluster.setUniforms(&createClusterShader);
#if CLUSTER_CULL
	createClusterShader.setUniform("ClusterMasks", &clusterMasks);
#endif
	lightLFB.beginComposite(&createClusterShader);

	Renderer::instance().drawQuad();

	lightLFB.endComposite();
	createClusterShader.unbind();
}



void VolumeClusterLL::init()
{
	auto lightCountVert = ShaderSourceCache::getShader("countVolumeClusterLLVert").loadFromFile("shaders/volumeLight/countLight.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countVolumeClusterLLGeom").loadFromFile("shaders/volumeLight/countLight.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countVolumeClusterLLFrag").loadFromFile("shaders/volumeLight/countLight.frag");
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);
}

void VolumeClusterLL::createClusterMask(App *app)
{
	app->getProfiler().start("Cluster Mask");

	// Zero the cluster masks.
	if (!zeroMaskShader.isGenerated())
	{
		auto zeroSrc = ShaderSource("zeroMaskLFB_LL", getZeroMaskShaderSrc());
		zeroMaskShader.create(&zeroSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// One thread per pixel.
	glEnable(GL_RASTERIZER_DISCARD);
	zeroMaskShader.bind();
	zeroMaskShader.setUniform("ClusterMasks", &clusterMasks);
	glDrawArrays(GL_POINTS, 0, width * height);
	zeroMaskShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Now create the masks.
	auto &camera = Renderer::instance().getActiveCamera();

	// Have to render high-res geometry I guess :(
	// TODO: Not really, can render at low res with conservative rasterization.
	app->setTmpViewport(width, height);

	createMaskShader.bind();

	createMaskShader.setUniform("mvMatrix", camera.getInverse());
	createMaskShader.setUniform("pMatrix", camera.getProjection());
	createMaskShader.setUniform("size", Math::ivec2(width, height));
	createMaskShader.setUniform("viewport", camera.getViewport());
	createMaskShader.setUniform("invPMatrix", camera.getInverseProj());
	createMaskShader.setUniform("ClusterMasks", &clusterMasks);
#if CONSERVATIVE_DEBUG
	createMaskShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
#endif

	// Render geometry.
	glEnable(GL_DEPTH_TEST);
	app->getGpuMesh().render(false);
	glDisable(GL_DEPTH_TEST);

	createMaskShader.unbind();

	app->restoreViewport();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().time("Cluster Mask");
}

void VolumeClusterLL::captureLighting(App *app)
{
	app->getProfiler().start("Light Capture");

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_CULL_FACE);

	auto &camera = Renderer::instance().getActiveCamera();

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	lightLFB.resize(width, height);

	if (lightLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		lightLFB.beginCountFrags(&countLightShader);
		countLightShader.setUniform("mvMatrix", camera.getInverse());
		countLightShader.setUniform("pMatrix", camera.getProjection());
		countLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		countLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
		countLightShader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());
		countLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		countLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();
		lightLFB.endCountFrags(&countLightShader);
	}

	captureLightShader.bind();
	captureLightShader.setUniform("mvMatrix", camera.getInverse());
	captureLightShader.setUniform("pMatrix", camera.getProjection());
	captureLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	captureLightShader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());

	// Capture fragments into the lfb.
	lightLFB.beginCapture(&captureLightShader);
	captureLightShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (lightLFB.endCapture())
	{
		lightLFB.beginCapture(&captureLightShader);
		captureLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();

		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	captureLightShader.unbind();

	// Restore original size.
	app->restoreViewport();

	glPopAttrib();

	app->getProfiler().time("Light Capture");
}

void VolumeClusterLL::sortLighting(App *app)
{
	app->getProfiler().start("Light Sort");

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	lightBMA.createMask(&lightLFB);
	lightBMA.sort(&lightLFB);
#else
	// Sort and composite normally.
	sortLightShader.bind();
	lightLFB.composite(&sortLightShader); // TODO: change this to a sort method.
	sortLightShader.unbind();
#endif

	// Restore original size.
	app->restoreViewport();

	app->getProfiler().time("Light Sort");
}

void VolumeClusterLL::createLightClusters(App *app)
{
	app->getProfiler().start("Light Cluster");

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	cluster.beginCapture();
	createClusters();
	if (cluster.endCapture())
	{
		cluster.beginCapture();
		createClusters();
		cluster.endCapture();
	}

	// Restore original size.
	app->restoreViewport();

	app->getProfiler().time("Light Cluster");
}

void VolumeClusterLL::captureGeometry(App *app)
{
	app->getProfiler().start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();
	auto &geometryLFB = app->getGeometryLfb();
	auto &gpuMesh = app->getGpuMesh();

	geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (geometryLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags();
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh.render(false);
		geometryLFB.endCountFrags();
	}

	captureGeometryShader.bind();
	captureGeometryShader.setUniform("mvMatrix", camera.getInverse());
	captureGeometryShader.setUniform("pMatrix", camera.getProjection());
	captureGeometryShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	captureGeometryShader.setUniform("nSphereLights", (unsigned int) app->getNSphereLights());

	captureGeometryShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureGeometryShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());

	captureGeometryShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
	captureGeometryShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	captureGeometryShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());

	captureGeometryShader.setUniform("LightColors", &app->getLightColorBuffer());

	// Capture fragments into the lfb using light link list to apply lighting.
	geometryLFB.beginCapture(&captureGeometryShader);
	cluster.setUniforms(&captureGeometryShader, false);
	gpuMesh.render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (geometryLFB.endCapture())
	{
		geometryLFB.beginCapture(&captureGeometryShader);
		cluster.setUniforms(&captureGeometryShader, false);
		gpuMesh.render();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}
	captureGeometryShader.unbind();

	app->getProfiler().time("Geometry Capture");

#if PROFILE_MEMORY
	unsigned int lightSize = (unsigned int) (lightLFB.getFragAlloc() * lightLFB.getFragSize() * sizeof(float));
	app->getProfiler().setBufferSize("lightDepths", lightSize + (width * height * sizeof(unsigned int)));
#endif
}

void VolumeClusterLL::sortGeometry(App *app)
{
	app->getProfiler().start("Geometry Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	geometryBMA.createMask(&app->getGeometryLfb());
	geometryBMA.sort(&app->getGeometryLfb());
#else
	// Sort and composite normally.
	sortGeometryShader.bind();
	app->getGeometryLfb().composite(&sortGeometryShader); // TODO: change this to a sort method.
	sortGeometryShader.unbind();
#endif

	app->getProfiler().time("Geometry Sort");

	app->getProfiler().start("Composite");
	app->getProfiler().time("Composite");
}

void VolumeClusterLL::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Re-allocate cluster data if the camera has resized.
	if (width != camera.getWidth() / GRID_CELL_SIZE || height != camera.getHeight() / GRID_CELL_SIZE)
	{
		width = camera.getWidth() / GRID_CELL_SIZE;
		height = camera.getHeight() / GRID_CELL_SIZE;
		cluster.resize(width, height);
	#if CLUSTER_CULL
		clusterMasks.create(nullptr, width * height * sizeof(unsigned int) * MASK_SIZE);
		#if PROFILE_MEMORY
			app->getProfiler().setBufferSize("clusterMasks", width * height * sizeof(unsigned int) * MASK_SIZE);
		#endif
	#endif
	}

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	createClusterMask(app);
	#if CONSERVATIVE_DEBUG
		return;
	#endif
#endif

	// Create sorted light image.
	captureLighting(app);
	sortLighting(app);

	// Create light clusters.
	createLightClusters(app);

	// Capture geometry, using light clusters to apply lighting.
	captureGeometry(app);

	// Sort and composite as normal.
	sortGeometry(app);
}

void VolumeClusterLL::update(float dt)
{
	(void) (dt);
}

void VolumeClusterLL::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	int fragSize = 2;

	auto &camera = Renderer::instance().getActiveCamera();
	width = camera.getWidth() / GRID_CELL_SIZE;
	height = camera.getHeight() / GRID_CELL_SIZE;

	cluster.setType(Cluster::LINK_LIST, "cluster", CLUSTERS);
	cluster.setProfiler(&app->getProfiler());

	//lightLFB.setType(type, "light", fragSize);
	lightLFB.setType(Rendering::LFB::LINEARIZED, "light", fragSize, false);
	lightLFB.setMaxFrags(MAX_FRAGS);
	lightLFB.setProfiler(&app->getProfiler());

	// Shader for capturing light fragments.
	auto vertLightCap = ShaderSourceCache::getShader("capVolumeClusterLLVert").loadFromFile("shaders/volumeLight/captureLight.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capVolumeClusterLLGeom").loadFromFile("shaders/volumeLight/captureLight.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capVolumeClusterLLFrag").loadFromFile("shaders/volumeLight/captureLight.frag");

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);

#if !USE_BMA
	// Shader for sorting light fragments (not used if you're using BMA).
	auto vertLightSort = ShaderSourceCache::getShader("sortVolumeClusterLLVert").loadFromFile("shaders/oit/sort.vert");
	auto fragLightSort = ShaderSourceCache::getShader("sortVolumeClusterLLFrag").loadFromFile("shaders/oit/sortL.frag");
	fragLightSort.setDefine("MAX_FRAGS", Utils::toString(lightLFB.getMaxFrags()));
	fragLightSort.setDefine("COMPOSITE_LOCAL", Utils::toString(0));
	fragLightSort.setDefine("FRAG_SIZE", Utils::toString(fragSize));
	fragLightSort.replace("LFB_NAME", "light_L");

	sortLightShader.release();
	sortLightShader.create(&vertLightSort, &fragLightSort);
#endif

	// Shader for creating light clusters.
	auto vertCluster = ShaderSourceCache::getShader("volumeClusterLLVert").loadFromFile("shaders/oit/volumeClusterLL/createCluster.vert");
	auto fragCluster = ShaderSourceCache::getShader("volumeClusterLLFrag").loadFromFile("shaders/oit/volumeClusterLL/createCluster.frag");
	fragCluster.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragCluster.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));

	createClusterShader.release();
	createClusterShader.create(&vertCluster, &fragCluster);

	// Shader for capturing geometry, applies lighting during capture.
	auto &geomCapVert = ShaderSourceCache::getShader("volumeClusterLLCapVert").loadFromFile("shaders/oit/volumeClusterLL/capture.vert");
	auto &geomCapFrag = ShaderSourceCache::getShader("volumeClusterLLCapFrag").loadFromFile("shaders/oit/volumeClusterLL/capture.frag");
	geomCapFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));

	captureGeometryShader.release();
	captureGeometryShader.create(&geomCapVert, &geomCapFrag);

#if !USE_BMA
	// Shader for sorting geometry, composites locally, not used if using BMA.
	auto vertSort = ShaderSourceCache::getShader("clusterGeoVert").loadFromFile("shaders/sort/sort.vert");
	auto fragSort = ShaderSourceCache::getShader("clusterGeoFrag").loadFromFile("shaders/sort/sort.frag");
	fragSort.setDefine("MAX_FRAGS", Utils::toString(app->getGeometryLfb().getMaxFrags()));
	fragSort.setDefine("COMPOSITE_LOCAL", Utils::toString(1));
	fragSort.replace("LFB_NAME", "lfb");

	sortGeometryShader.release();
	sortGeometryShader.create(&vertSort, &fragSort);
#endif

#if CLUSTER_CULL
	// Shader for creating cluster masks, so lights don't have to be added to clusters that contain no geometry.
	auto vertMask = ShaderSourceCache::getShader("clusterLLMaskVert").loadFromFile("shaders/oit/volumeClusterLL/createMask.vert");
	auto geomMask = ShaderSourceCache::getShader("clusterLLMaskGeom").loadFromFile("shaders/oit/volumeClusterLL/createMask.geom");
	auto fragMask = ShaderSourceCache::getShader("clusterLLMaskFrag").loadFromFile("shaders/oit/volumeClusterLL/createMask.frag");
	fragMask.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	fragMask.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	fragMask.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragMask.setDefine("CONSERVATIVE_DEBUG", Utils::toString(CONSERVATIVE_DEBUG));

	createMaskShader.release();
	createMaskShader.create(&vertMask, &fragMask, &geomMask);
#endif

#if USE_BMA
	// Init BMA mask and sort shaders.
	lightBMA.createMaskShader("light", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMaskL.frag", {{"FRAG_SIZE", Utils::toString(fragSize)}});
	lightBMA.createShaders(&lightLFB, "shaders/sort/sort.vert", "shaders/sort/sortL.frag", {{"COMPOSITE_LOCAL", Utils::toString(0)}, {"FRAG_SIZE", Utils::toString(fragSize)}});

	geometryBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
	geometryBMA.createShaders(&app->getGeometryLfb(), "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}});
#endif

	cluster.resize(width, height);
#if CLUSTER_CULL
	clusterMasks.create(nullptr, width * height * sizeof(unsigned int) * MASK_SIZE);
#endif
}

void VolumeClusterLL::saveLFB(App *app)
{
	(void) (app);

	auto s = cluster.getStrSorted();
	Utils::File f("volumeClusterLL.txt");
	f.writeLine(s);
}

#endif

