#include "RayClusterLAI.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>
#include <cassert>


#define MAX_FRAGS 512

#define STORE_LIGHT_DATA 0

#define CLUSTER_CULL 1
#define PROFILE_MEMORY 1
#define LINEARISED 1
#define INTERLEAVED 0

using namespace Rendering;



void RayClusterLAI::countClusters(App *app)
{
	app->getProfiler().start("Cluster Count");

	cluster.beginCountFrags();

	countClusterShader.bind();

	app->setLightUniforms(&countClusterShader);
	app->setCameraUniforms(&countClusterShader);
	countClusterShader.setUniform("size", Math::ivec2(width, height));

#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&countClusterShader);
#endif

	cluster.setCountUniforms(&countClusterShader);

	// Render all the lights.
	countClusterShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	countClusterShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	countClusterShader.unbind();

	app->getProfiler().time("Cluster Count");

	app->getProfiler().start("Cluster Prefix");

	cluster.endCountFrags();

	app->getProfiler().time("Cluster Prefix");
}

void RayClusterLAI::createClusters(App *app)
{
	cluster.beginComposite();
	createClusterShader.bind();
	cluster.setUniforms(&createClusterShader);
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&createClusterShader);
#endif

	app->setLightUniforms(&createClusterShader);
	app->setCameraUniforms(&createClusterShader);
	createClusterShader.setUniform("size", Math::ivec2(width, height));

	// Render all the lights.
	createClusterShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	createClusterShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	createClusterShader.unbind();
	cluster.endComposite();
}



void RayClusterLAI::init()
{
	auto clusterCountVert = ShaderSourceCache::getShader("countRayClusterVert").loadFromFile("shaders/light/countLightRay.vert");
	auto clusterCountGeom = ShaderSourceCache::getShader("countRayClusterGeom").loadFromFile("shaders/light/countLightRay.geom");
	auto clusterCountFrag = ShaderSourceCache::getShader("countRayClusterFrag").loadFromFile("shaders/light/countLightRay.frag");
	clusterCountVert.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCountGeom.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	clusterCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	clusterCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	clusterCountFrag.setDefine("USE_CLUSTER", "1");
	clusterCountFrag.setDefine("ATOMIC_ADD", "1");
	countClusterShader.create(&clusterCountVert, &clusterCountFrag, &clusterCountGeom);
}

void RayClusterLAI::captureLighting(App *app)
{
	// Capture lights as billboarded quads, and raycast for front/back depths, then add to clusters.

	glPushAttrib(GL_ENABLE_BIT);

	// Disable if using billboarded quads, enable if using light outlines.
	glDisable(GL_CULL_FACE);
	//glEnable(GL_CULL_FACE);

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	// Now store the ids in their clusters.
	app->getProfiler().start("Light Capture");

	if (cluster.getType() != Cluster::LINK_LIST)
		countClusters(app);

	// This can only repeat if using link lists.
	cluster.beginCapture();
	createClusters(app);
	if (cluster.endCapture())
	{
		cluster.beginCapture();
		createClusters(app);
		if (cluster.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	app->getProfiler().time("Light Capture");

	// Restore original size.
	app->restoreViewport();

	glPopAttrib();
}

void RayClusterLAI::render(App *app)
{
	if (gBufferFlag && opaqueFlag != 1)
		return;

	auto &camera = Renderer::instance().getActiveCamera();

	if (gBufferFlag)
		gBuffer.resize(camera.getWidth(), camera.getHeight());

	// Re-allocate cluster data if the camera has resized.
	if (width != ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE) || height != ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE))
	{
		width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
		height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);
		cluster.resize(width, height);

	#if CLUSTER_CULL
		app->getDepthMask().resize(width, height);
	#endif
	}

	if (gBufferFlag)
	{
		gBuffer.beginCapture();
		app->captureGeometry(nullptr, nullptr, &gBuffer.getCaptureShader(), false);
		gBuffer.endCapture();
	}

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	if (gBufferFlag)
		app->createDepthMask(gBuffer.getDepthTexture());
	else
		app->createDepthMask();
#endif

	// Capture lights directly into the uniform grid, no need for separate capture/sort/create.
	captureLighting(app);

	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	// Capture geometry, using light clusters to apply lighting.
	if (gBufferFlag)
	{
		app->getProfiler().start("Light Deferred");

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		captureGeometryShader.bind();
		gBuffer.bindTextures();
		gBuffer.setUniforms(&captureGeometryShader);
		app->setCameraUniforms(&captureGeometryShader);
		app->setLightUniforms(&captureGeometryShader);
#if CLUSTER_CULL
		app->getDepthMask().setUniforms(&captureGeometryShader);
#endif
		cluster.setUniforms(&captureGeometryShader);

		Renderer::instance().drawQuad();

		gBuffer.unbindTextures();
		captureGeometryShader.unbind();

		app->getProfiler().time("Light Deferred");
	}
	else
		//app->captureGeometryAndParticles(nullptr, &cluster, &captureGeometryShader);
		app->captureGeometry(nullptr, &cluster, &captureGeometryShader);

	if (opaqueFlag == 1)
		glPopAttrib();
	else
		// Sort and composite as normal.
		app->sortGeometry();
}

void RayClusterLAI::update(float dt)
{
	(void) (dt);
}

void RayClusterLAI::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	auto &camera = Renderer::instance().getActiveCamera();
	width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

#if LINEARISED
	#if STORE_LIGHT_DATA
		cluster.setType(Cluster::LINEARIZED, "cluster", CLUSTERS, true, Cluster::VEC2, true);
	#else
		cluster.setType(Cluster::LINEARIZED, "cluster", CLUSTERS, false, Cluster::UINT, true);
	#endif
#elif INTERLEAVED
	#if STORE_LIGHT_DATA
		cluster.setType(Cluster::COALESCED, "cluster", CLUSTERS, true, Cluster::VEC2, true);
	#else
		cluster.setType(Cluster::COALESCED, "cluster", CLUSTERS, false, Cluster::UINT, true);
	#endif
#else
	#if STORE_LIGHT_DATA
		cluster.setType(Cluster::LINK_LIST, "cluster", CLUSTERS, true, Cluster::VEC2, true);
	#else
		cluster.setType(Cluster::LINK_LIST, "cluster", CLUSTERS, false, Cluster::UINT, true);
	#endif
#endif

	cluster.setProfiler(&app->getProfiler());
	if (gBufferFlag)
		gBuffer.setProfiler(&app->getProfiler());

	// Shader for capturing light clusters.
	auto vertCluster = ShaderSourceCache::getShader("rayClusterVert").loadFromFile("shaders/light/captureLightRay.vert");
	auto geomCluster = ShaderSourceCache::getShader("rayClusterGeom").loadFromFile("shaders/light/captureLightRay.geom");
	auto fragCluster = ShaderSourceCache::getShader("rayClusterFrag").loadFromFile("shaders/light/captureLightRay.frag");
	vertCluster.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	geomCluster.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	fragCluster.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	fragCluster.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragCluster.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragCluster.setDefine("STORE_LIGHT_DATA", Utils::toString(STORE_LIGHT_DATA));
	fragCluster.setDefine("USE_CLUSTER", "1");
	fragCluster.setDefine("ATOMIC_ADD", "1");

	createClusterShader.release();
	createClusterShader.create(&vertCluster, &fragCluster, &geomCluster);

	// Shader for capturing geometry, applies lighting during capture.
	auto &geomCapVert = ShaderSourceCache::getShader("rayClusterCapVert").loadFromFile("shaders/oit/rayCluster/capture.vert");
	auto &geomCapFrag = ShaderSourceCache::getShader("rayClusterCapFrag").loadFromFile("shaders/oit/rayCluster/capture.frag");
	geomCapFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	geomCapFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	geomCapFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	geomCapFrag.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	geomCapFrag.setDefine("STORE_LIGHT_DATA", Utils::toString(STORE_LIGHT_DATA));
	if (gBufferFlag)
	{
		geomCapVert.setDefine("USE_G_BUFFER", "1");
		geomCapFrag.setDefine("USE_G_BUFFER", "1");
	}

	captureGeometryShader.release();
	captureGeometryShader.create(&geomCapVert, &geomCapFrag);

	auto str = captureGeometryShader.getBinary();
	Utils::File f("captureShader.txt");
	f.write(str);

	cluster.resize(width, height);
}

void RayClusterLAI::saveLFB(App *app)
{
	(void) (app);

	//auto s = cluster.getStrSorted();
	auto s = app->getDepthMask().getStr();
	Utils::File f("rayCluster.txt");
	f.write(s);
}

