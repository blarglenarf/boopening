#pragma once

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"
#include "../../../Renderer/src/RenderObject/Texture.h"
#include "../../../Renderer/src/LFB/Cluster.h"
#include "../../../Renderer/src/Lighting/GBuffer.h"

#include <vector>

class PracticalCluster : public Oit
{
private:
	int width;
	int height;

	Rendering::Shader captureLightShader;
	Rendering::Shader countListShader;
	Rendering::Shader createListShader;
	Rendering::Shader createListComputeShader;

	Rendering::Shader clearLightShader;

	Rendering::Shader captureGeometryShader;

	Rendering::Shader debugLightShader;

	std::vector<Rendering::Texture2DArray*> lightDepthTextures;
	std::vector<Rendering::FrameBuffer*> lightDepthBuffers;
	std::vector<int> textureUniforms;

	Rendering::Cluster cluster;

	Rendering::GBuffer gBuffer;
	bool gBufferFlag;

private:
	void allocStorageBuffers(App *app);

	void drawLights(App *app, bool clearFlag = false);
	void countLightList(App *app);
	void createLightList(App *app);
	void clearLightDepths(App *app);

	void captureLightList(App *app);

	void setLightRes(App *app);

	void drawLightClusters(App *app);

public:
	PracticalCluster(int opaqueFlag = 0, bool gBufferFlag = false) : Oit(opaqueFlag), width(0), height(0), gBufferFlag(gBufferFlag) {}
	virtual ~PracticalCluster();

	virtual void init() override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

