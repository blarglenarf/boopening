#pragma once

#include "Oit.h"

#include "../../../Renderer/src/LFB/Cluster.h"
#include "../../../Renderer/src/LFB/ClusterMask.h"
#include "../../../Renderer/src/Lighting/GBuffer.h"

#include <vector>

class TiledCluster : public Oit
{
private:
	Rendering::Cluster cluster;

	Rendering::Shader createKeysShader;
	Rendering::Shader createBufferShader;
	Rendering::Shader zeroBVHShader;
	Rendering::Shader createBVHShader;
	Rendering::Shader parentBVHShader;

	Rendering::Shader captureLightShader;
	Rendering::Shader countLightShader;
	Rendering::Shader captureGeometryShader;

	Rendering::StorageBuffer frustumPlanesBuffer;

	Rendering::StorageBuffer lightIDBuffer;
	Rendering::StorageBuffer lightKeysBuffer;
	Rendering::StorageBuffer lightPosBuffer;
	Rendering::StorageBuffer lightRadiiBuffer;
	Rendering::StorageBuffer bvhBuffer;

	Rendering::ClusterMask mask;

	int width;
	int height;

	unsigned int bvhParents;
	unsigned int bvhSize;

	std::vector<Math::vec4> frustumPlanes;

	Math::mat4 prevMvMat;
	size_t prevNLights;

	// For the deferred version.
	Rendering::GBuffer gBuffer;
	bool gBufferFlag;

	bool createBVHFlag;

private:
	void createLightBVH(App *app);

	void countClusters(App *app);
	void createClusters(App *app);

public:
	TiledCluster(int opaqueFlag = 0, bool gBufferFlag = false) : Oit(opaqueFlag), width(0), height(0), bvhParents(0), bvhSize(0), prevNLights(0), gBufferFlag(gBufferFlag), createBVHFlag(true) {}
	virtual ~TiledCluster();

	virtual void init() override;

	void createClusterMask(App *app);
	void resizeFrustumBuffer();
	void createFrustums(App *app);

	void captureLighting(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

