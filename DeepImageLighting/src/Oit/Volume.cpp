#include "Volume.h"

#if 0

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>

#define USE_BMA 1
#define COMPOSITE_BMA 0
#define MAX_FRAGS 512
#define COMPOSITE_LOCAL 1
#define LIGHT_DRAW 1
#define LIGHT_COUNT_DEBUG 0
#define COUNT_LIGHT_FRAGS 0
#define PACK_LIGHT_DATA 0
#define LIGHT_IMPLICIT 1

// Note: culling can only be done with implicit lighting.
#define CLUSTER_CULL 0
#define CLUSTERS 32
#define MASK_SIZE (CLUSTERS / 32)
#define MAX_EYE_Z 30.0

using namespace Rendering;

void Volume::init()
{
#if LIGHT_IMPLICIT
	auto lightCountVert = ShaderSourceCache::getShader("countVolumeVert").loadFromFile("shaders/volumeLight/countLightRay.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countVolumeGeom").loadFromFile("shaders/volumeLight/countLightRay.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countVolumeFrag").loadFromFile("shaders/volumeLight/countLightRay.frag");
	lightCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCountFrag.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);
#else
	auto lightCountVert = ShaderSourceCache::getShader("countVolumeVert").loadFromFile("shaders/volumeLight/countLight.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countVolumeGeom").loadFromFile("shaders/volumeLight/countLight.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countVolumeFrag").loadFromFile("shaders/volumeLight/countLight.frag");
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);
#endif
}

void Volume::createClusterMask(App *app)
{
	app->getProfiler().start("Cluster Mask");

	mask.beginCreateMask();

	// Render geometry.
	app->getGpuMesh().render();
	
	mask.endCreateMask();

	app->getProfiler().time("Cluster Mask");
}

void Volume::captureLighting(App *app)
{
	app->getProfiler().start("Light Capture");

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_CULL_FACE);

	auto &camera = Renderer::instance().getActiveCamera();

	lightLFB.resize(camera.getWidth(), camera.getHeight());

	if (lightLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		lightLFB.beginCountFrags(&countLightShader);
		countLightShader.setUniform("mvMatrix", camera.getInverse());
		countLightShader.setUniform("pMatrix", camera.getProjection());
		countLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		countLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	#if LIGHT_IMPLICIT
		countLightShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
	#else
		countLightShader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());
	#endif
	#if CLUSTER_CULL
		mask.setUniforms(&countLightShader);
	#endif
		countLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		countLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();
		lightLFB.endCountFrags(&countLightShader);
	}

	captureLightShader.bind();
	captureLightShader.setUniform("mvMatrix", camera.getInverse());
	captureLightShader.setUniform("pMatrix", camera.getProjection());
	captureLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
#if LIGHT_IMPLICIT
	captureLightShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
#else
	captureLightShader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());
#endif
#if CLUSTER_CULL
	mask.setUniforms(&captureLightShader);
#endif

#if LIGHT_DRAW
	captureLightShader.setUniform("LightColors", &app->getLightColorBuffer());
#endif

	// Capture fragments into the lfb.
	lightLFB.beginCapture(&captureLightShader);
	captureLightShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (lightLFB.endCapture())
	{
		lightLFB.beginCapture(&captureLightShader);
		captureLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();

		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	captureLightShader.unbind();

	glPopAttrib();

	app->getProfiler().time("Light Capture");
}

void Volume::sortLighting(App *app)
{
	app->getProfiler().start("Light Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	lightBMA.createMask(&lightLFB);
	lightBMA.sort(&lightLFB);
#else
	// Sort and composite normally.
	sortLightShader.bind();
	lightLFB.composite(&sortLightShader); // TODO: change this to a sort method.
	sortLightShader.unbind();
#endif

	app->getProfiler().time("Light Sort");
}

void Volume::composite(App *app)
{
	app->getProfiler().start("Composite");

	auto &camera = Renderer::instance().getActiveCamera();

#if COUNT_LIGHT_FRAGS
	unsigned int zero = 0;
	passCount.bufferData(&zero, sizeof(zero));
	failCount.bufferData(&zero, sizeof(zero));
#endif

	// Now composite using both geometry and lighting.
	glPushAttrib(GL_POLYGON_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Can composite using BMA, or not.
#if !COMPOSITE_BMA
	compositeShader.bind();
	compositeShader.setUniform("viewport", camera.getViewport());
	compositeShader.setUniform("mvMatrix", camera.getInverse());
	compositeShader.setUniform("invPMatrix", camera.getProjection().getInverse());
	compositeShader.setUniform("nSphereLights", (unsigned int) app->getNSphereLights());

	compositeShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	compositeShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());

	compositeShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
	compositeShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	compositeShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());

	compositeShader.setUniform("LightColors", &app->getLightColorBuffer());

	#if COUNT_LIGHT_FRAGS
		compositeShader.setUniform("PassCount", &passCount);
		compositeShader.setUniform("FailCount", &failCount);
	#endif

	app->getGeometryLfb().beginComposite(&compositeShader);
	lightLFB.beginComposite(&compositeShader);
	Renderer::instance().drawQuad();
	app->getGeometryLfb().endComposite();
	lightLFB.endComposite();

	compositeShader.unbind();
#else
	auto &intervals = compositeBMA.getIntervals();

	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
		intervals[i].shader->bind();
		app->getGeometryLfb().setUniforms(intervals[i].shader);
		lightLFB.setUniforms(intervals[i].shader);

		intervals[i].shader->setUniform("viewport", camera.getViewport());
		intervals[i].shader->setUniform("mvMatrix", camera.getInverse());
		intervals[i].shader->setUniform("invPMatrix", camera.getProjection().getInverse());
		intervals[i].shader->setUniform("nSphereLights", (unsigned int) app->getNSphereLights());

		intervals[i].shader->setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		intervals[i].shader->setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());

		intervals[i].shader->setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
		intervals[i].shader->setUniform("ConeLightDist", &app->getConeLightDistBuffer());
		intervals[i].shader->setUniform("ConeLightDir", &app->getConeLightDirBuffer());

		intervals[i].shader->setUniform("LightColors", &app->getLightColorBuffer());

	#if COUNT_LIGHT_FRAGS
		intervals[i].shader->setUniform("PassCount", &passCount);
		intervals[i].shader->setUniform("FailCount", &failCount);
	#endif

		Rendering::Renderer::instance().drawQuad();

		// Final pass for pixels that aren't covered by any lights.
		if (i == 0)
		{
			glStencilFunc(GL_GREATER, 1, 0xFF);
			Rendering::Renderer::instance().drawQuad();
		}

		intervals[i].shader->unbind();
	}

	glDisable(GL_STENCIL_TEST);
#endif
	
	glPopAttrib();

#if COUNT_LIGHT_FRAGS
	unsigned int pass = *((unsigned int*) passCount.read());
	unsigned int fail = *((unsigned int*) failCount.read());

	float total = (float) (pass + fail);

	std::cout << "Total: " << pass + fail << ", Pass: " << pass << ", Fail: " << fail << ", % Pass: "  << ((float) pass / total) * 100.0f << ", % Fail: " << ((float) fail / total) * 100.0f << "\n";
#endif

	app->getProfiler().time("Composite");
}

void Volume::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Re-allocate cluster data if the camera has resized.
	mask.resize(camera.getWidth(), camera.getHeight());

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	createClusterMask(app);
#endif

#if !LIGHT_DRAW
	app->captureGeometry(nullptr, nullptr, nullptr, false, true);
	app->sortGeometry();
#endif

	captureLighting(app);

#if LIGHT_DRAW
	glPushAttrib(GL_POLYGON_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	sortLighting(app);

	#if !COMPOSITE_LOCAL
		// Read the result back from the lfb and composite.
		app->getProfiler().start("Composite");
		compositeShader.bind();
		lightLFB.composite(&compositeShader);
		compositeShader.unbind();
		app->getProfiler().time("Composite");
	#endif

	glPopAttrib();
#else
	sortLighting(app);
	composite(app);
#endif
}

void Volume::update(float dt)
{
	(void) (dt);
}

void Volume::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

#if LIGHT_DRAW || !PACK_LIGHT_DATA
	int fragSize = 2;
#else
	int fragSize = 1;
#endif

	//lightLFB.setType(type, "light", fragSize);
	lightLFB.setType(Rendering::LFB::LINEARIZED, "light", fragSize, false);
	lightLFB.setMaxFrags(MAX_FRAGS);

#if LIGHT_IMPLICIT
	auto vertLightCap = ShaderSourceCache::getShader("capVolumeVert").loadFromFile("shaders/volumeLight/captureLightRay.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capVolumeGeom").loadFromFile("shaders/volumeLight/captureLightRay.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capVolumeFrag").loadFromFile("shaders/volumeLight/captureLightRay.frag");
	fragLightCap.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	fragLightCap.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));
	fragLightCap.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragLightCap.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	fragLightCap.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragLightCap.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	fragLightCap.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);
#else
	auto vertLightCap = ShaderSourceCache::getShader("capVolumeVert").loadFromFile("shaders/volumeLight/captureLight.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capVolumeGeom").loadFromFile("shaders/volumeLight/captureLight.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capVolumeFrag").loadFromFile("shaders/volumeLight/captureLight.frag");
	fragLightCap.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	fragLightCap.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);
#endif

#if !USE_BMA
	auto vertLightSort = ShaderSourceCache::getShader("sortVolumeVert").loadFromFile("shaders/sort/sort.vert");
	auto fragLightSort = ShaderSourceCache::getShader("sortVolumeFrag").loadFromFile("shaders/sort/sortL.frag");
	fragLightSort.setDefine("MAX_FRAGS", Utils::toString(lightLFB.getMaxFrags()));
	fragLightSort.setDefine("COMPOSITE_LOCAL", Utils::toString(COMPOSITE_LOCAL));
	fragLightSort.setDefine("FRAG_SIZE", Utils::toString(fragSize));
	//fragLightSort.replace("LFB_NAME", "light");
	fragLightSort.replace("LFB_NAME", "light_L");

	sortLightShader.release();
	sortLightShader.create(&vertLightSort, &fragLightSort);
#endif

	auto vertComp = ShaderSourceCache::getShader("compVolumeVert").loadFromFile("shaders/oit/volume/composite.vert");
	auto fragComp = ShaderSourceCache::getShader("compVolumeFrag").loadFromFile("shaders/oit/volume/composite.frag");
	fragComp.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	fragComp.setDefine("LIGHT_COUNT_DEBUG", Utils::toString(LIGHT_COUNT_DEBUG));
	fragComp.setDefine("COUNT_LIGHT_FRAGS", Utils::toString(COUNT_LIGHT_FRAGS));
	fragComp.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));

	compositeShader.release();
	compositeShader.create(&vertComp, &fragComp);

#if USE_BMA
	// Init BMA mask shaders.
	lightBMA.createMaskShader("light", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMaskL.frag", {{"FRAG_SIZE", Utils::toString(fragSize)}});
	lightBMA.createShaders(&lightLFB, "shaders/sort/sort.vert", "shaders/sort/sortL.frag", {{"COMPOSITE_LOCAL", Utils::toString(COMPOSITE_LOCAL)}, {"FRAG_SIZE", Utils::toString(fragSize)}});
#endif
#if COMPOSITE_BMA
	compositeBMA.createShaders(&lightLFB, "shaders/oit/volume/composite.vert", "shaders/oit/volume/composite.frag",
		{{"LIGHT_COUNT_DEBUG", Utils::toString(LIGHT_COUNT_DEBUG)}, {"COUNT_LIGHT_FRAGS", Utils::toString(COUNT_LIGHT_FRAGS)}, {"PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA)}});
#endif

#if CLUSTER_CULL
	mask.regen(CLUSTERS, MAX_EYE_Z, 1);
	//mask.resize(width, height);
#endif

#if COUNT_LIGHT_FRAGS
	passCount.release();
	passCount.create(nullptr, sizeof(unsigned int));

	failCount.release();
	failCount.create(nullptr, sizeof(unsigned int));
#endif
}

void Volume::saveLFB(App *app)
{
	auto s = lightLFB.getStrSorted(true);
	Utils::File f("lightVolumeLFB.txt");
	f.writeLine(s);

	app->saveGeometryLFB();
}

#endif

