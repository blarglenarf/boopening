#pragma once

#include "Oit.h"

#include "../../../Renderer/src/LFB/Cluster.h"
#include "../../../Renderer/src/Lighting/GBuffer.h"

class RayClusterLLF : public Oit
{
private:
	Rendering::Shader captureGeometryShader;

	Rendering::Cluster cluster;

	// For the deferred version.
	Rendering::GBuffer gBuffer;
	bool gBufferFlag;

	// For constructing and storing the clusters.
	int width;
	int height;

	Rendering::Shader countClusterShader;
	Rendering::Shader createClusterShader;

private:
	void countClusters(App *app);
	void createClusters(App *app);

public:
	RayClusterLLF(int opaqueFlag = 0, bool gBufferFlag = false) : Oit(opaqueFlag), gBufferFlag(gBufferFlag), width(0), height(0) {}
	virtual ~RayClusterLLF() {}

	virtual void init() override;

	void captureLighting(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

