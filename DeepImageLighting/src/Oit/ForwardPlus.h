#pragma once

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"
#include "../../../Renderer/src/Lighting/GBuffer.h"

#include <vector>

class ForwardPlus : public Oit
{
private:
	Rendering::LFB lightLFB;

	Rendering::Shader captureLightShader;
	Rendering::Shader countLightShader;

	Rendering::Shader captureGeometryShader;

	Rendering::StorageBuffer frustumPlanesBuffer;

	// For the deferred version.
	Rendering::GBuffer gBuffer;
	bool gBufferFlag;

	int width;
	int height;

	std::vector<Math::vec4> frustumPlanes;

private:
	void countLists(App *app);
	void createLists(App *app);

public:
	ForwardPlus(int opaqueFlag = 0, bool gBufferFlag = false) : Oit(opaqueFlag), gBufferFlag(gBufferFlag), width(0), height(0) {}
	virtual ~ForwardPlus() {}

	virtual void init() override;

	void resizeFrustumBuffer();
	void createFrustums(App *app);

	void captureLighting(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

