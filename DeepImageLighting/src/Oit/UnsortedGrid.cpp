#include "UnsortedGrid.h"

#if 0

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>

#define USE_BMA 1
#define MAX_FRAGS 512
#define LIGHT_DRAW 0
#define LIGHT_COUNT_DEBUG 0
#define COUNT_LIGHT_FRAGS 0
#define PACK_LIGHT_DATA 0
#define CLUSTER_CULL 1

using namespace Rendering;

void UnsortedGrid::init()
{
	auto lightCountVert = ShaderSourceCache::getShader("countUnsortedGridVert").loadFromFile("shaders/quadLight/countLight.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countUnsortedGridGeom").loadFromFile("shaders/quadLight/countLight.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countUnsortedGridFrag").loadFromFile("shaders/quadLight/countLight.frag");
	lightCountGeom.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);
}

void UnsortedGrid::captureLighting(App *app)
{
	app->getProfiler().start("Light Capture");

	// TODO: use conservative rasterization.
	glPushAttrib(GL_ENABLE_BIT);

	auto &camera = Renderer::instance().getActiveCamera();

	// Resize based on grid size.
	app->setTmpViewport(camera.getWidth() / GRID_CELL_SIZE, camera.getHeight() / GRID_CELL_SIZE);

	lightLFB.resize(camera.getWidth(), camera.getHeight());

	if (lightLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		lightLFB.beginCountFrags(&countLightShader);
		countLightShader.setUniform("mvMatrix", camera.getInverse());
		countLightShader.setUniform("pMatrix", camera.getProjection());
		countLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		countLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
		countLightShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
	#if CLUSTER_CULL
		app->getDepthMask().setUniforms(&countLightShader);
	#endif
		countLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		countLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();
		lightLFB.endCountFrags(&countLightShader);
	}

	captureLightShader.bind();
	captureLightShader.setUniform("mvMatrix", camera.getInverse());
	captureLightShader.setUniform("pMatrix", camera.getProjection());
	captureLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	captureLightShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&captureLightShader);
#endif

#if LIGHT_DRAW
	captureLightShader.setUniform("LightColors", &app->getLightColorBuffer());
#endif

	// Capture fragments into the lfb.
	lightLFB.beginCapture(&captureLightShader);
	captureLightShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (lightLFB.endCapture())
	{
		std::cout << "resizing\n";

		lightLFB.beginCapture(&captureLightShader);
		captureLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();

		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	captureLightShader.unbind();

	// Restore original size.
	app->restoreViewport();

	glPopAttrib();

	app->getProfiler().time("Light Capture");
}

void UnsortedGrid::sortLighting(App *app)
{
	app->getProfiler().start("Light Sort");

	auto &camera = Renderer::instance().getActiveCamera();

	// Resize based on grid size.
	app->setTmpViewport(camera.getWidth() / GRID_CELL_SIZE, camera.getHeight() / GRID_CELL_SIZE);

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	lightBMA.createMask(&lightLFB);
	lightBMA.sort(&lightLFB);
#else
	// Sort and composite normally.
	sortLightShader.bind();
	lightLFB.composite(&sortLightShader); // TODO: change this to a sort method.
	sortLightShader.unbind();
#endif

	// Restore to original size.
	app->restoreViewport();

	app->getProfiler().time("Light Sort");
}

void UnsortedGrid::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Re-allocate cluster data if the camera has resized.
	if (width != camera.getWidth() / GRID_CELL_SIZE || height != camera.getHeight() / GRID_CELL_SIZE)
	{
		width = camera.getWidth() / GRID_CELL_SIZE;
		height = camera.getHeight() / GRID_CELL_SIZE;

		app->getDepthMask().resize(width, height);
	}

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	app->createDepthMask();
#endif

	// Capture lighting first (no need to sort).
	captureLighting(app);

#if LIGHT_DRAW
	glPushAttrib(GL_POLYGON_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	sortLighting(app);

	// Read the result back from the lfb and composite.
	app->getProfiler().start("Composite");
	compositeShader.bind();
	lightLFB.composite(&compositeShader);
	compositeShader.unbind();
	app->getProfiler().time("Composite");

	glPopAttrib();
#else
	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	// Capture geometry, using light information.
	app->captureGeometry(&lightLFB, nullptr, &captureGeometryShader);

	if (opaqueFlag == 1)
	{
		glPopAttrib();
		return;
	}

	// Sort and composite (no need to write to global memory).
	app->sortGeometry();
#endif
}

void UnsortedGrid::update(float dt)
{
	(void) (dt);
}

void UnsortedGrid::useLFB(App *app, Rendering::LFB::LFBType type)
{
	// TODO: only use linearised lfb for light image.
	(void) (app);
	(void) (type);

#if LIGHT_DRAW || !PACK_LIGHT_DATA
	int fragSize = 2;
#else
	int fragSize = 1;
#endif

	auto &camera = Renderer::instance().getActiveCamera();
	width = camera.getWidth() / GRID_CELL_SIZE;
	height = camera.getHeight() / GRID_CELL_SIZE;

	// Always use linearized for the lights.
	//lightLFB.setType(type, "light", fragSize);
	lightLFB.setType(Rendering::LFB::LINEARIZED, "light", fragSize, false);
	lightLFB.setMaxFrags(MAX_FRAGS);
	lightLFB.setProfiler(&app->getProfiler());

	auto vertLightCap = ShaderSourceCache::getShader("capUnsortedGridVert").loadFromFile("shaders/quadLight/captureLight.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capUnsortedGridGeom").loadFromFile("shaders/quadLight/captureLight.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capUnsortedGridFrag").loadFromFile("shaders/quadLight/captureLight.frag");
	geomLightCap.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragLightCap.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	fragLightCap.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));
	fragLightCap.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragLightCap.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	fragLightCap.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragLightCap.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);

#if LIGHT_DRAW
	// Sorting not necessary for composite, but used for light visualisation.
	auto vertLightSort = ShaderSourceCache::getShader("sortUnsortedGridVert").loadFromFile("shaders/sort/sort.vert");
	auto fragLightSort = ShaderSourceCache::getShader("sortUnsortedGridFrag").loadFromFile("shaders/sort/sortL.frag");
	fragLightSort.setDefine("MAX_FRAGS", Utils::toString(lightLFB.getMaxFrags()));
	fragLightSort.setDefine("COMPOSITE_LOCAL", Utils::toString(0));
	//fragLightSort.replace("LFB_NAME", "light");
	fragLightSort.replace("LFB_NAME", "light_L");

	sortLightShader.release();
	sortLightShader.create(&vertLightSort, &fragLightSort);

	// Composite shader only used for light visualisation.
	auto vertComp = ShaderSourceCache::getShader("compUnsortedGridVert").loadFromFile("shaders/oit/unsortedGrid/composite.vert");
	auto fragComp = ShaderSourceCache::getShader("compUnsortedGridFrag").loadFromFile("shaders/oit/unsortedGrid/composite.frag");
	fragComp.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));

	compositeShader.release();
	compositeShader.create(&vertComp, &fragComp);
#endif

	// Shader for capturing geometry, applies lighting during capture.
	auto vertCapture = ShaderSourceCache::getShader("captureUnsortedGridVert").loadFromFile("shaders/oit/unsortedGrid/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureUnsortedGridFrag").loadFromFile("shaders/oit/unsortedGrid/capture.frag");
	fragCapture.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	fragCapture.setDefine("LIGHT_COUNT_DEBUG", Utils::toString(LIGHT_COUNT_DEBUG));
	fragCapture.setDefine("COUNT_LIGHT_FRAGS", Utils::toString(COUNT_LIGHT_FRAGS));
	fragCapture.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);

#if LIGHT_DRAW
	lightBMA.createMaskShader("light", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMaskL.frag");
	lightBMA.createShaders(&lightLFB, "shaders/sort/sort.vert", "shaders/sort/sortL.frag");
#endif

#if COUNT_LIGHT_FRAGS
	passCount.release();
	passCount.create(nullptr, sizeof(unsigned int));

	failCount.release();
	failCount.create(nullptr, sizeof(unsigned int));
#endif
}

void UnsortedGrid::saveLFB(App *app)
{
	auto s = lightLFB.getStrSorted(true);
	Utils::File f("lightUnsortedGridLFB.txt");
	f.writeLine(s);

	app->saveGeometryLFB();
}

#endif

