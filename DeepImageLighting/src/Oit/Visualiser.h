#pragma once

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"

#include <vector>

class Visualiser : public Oit
{
private:
	Rendering::LFB lightLFB;

	Rendering::Shader captureLightShader;
	Rendering::Shader countLightShader;
	Rendering::Shader compositeShader;

	Rendering::Shader captureGeometryShader;

	Rendering::BMA lightBMA;

	std::vector<Math::vec4> frustumPlanes;

private:
	void createFrustums();
	void drawFrustums();

	void countLights(App *app);
	void createLights(App *app);

	void captureLighting(App *app);
	void sortLighting(App *app);

	void composite(App *app);

public:
	Visualiser(int opaqueFlag = 0) : Oit(opaqueFlag) {}
	virtual ~Visualiser() {}

	virtual void init() override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;
	virtual void reloadShaders(App *app) override;

	virtual void saveLFB(App *app) override;
};

