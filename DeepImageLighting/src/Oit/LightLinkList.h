#pragma once

#if 0

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"
#include "../../../Renderer/src/RenderObject/GPUBuffer.h"

#include <vector>

class LightLinkList : public Oit
{
private:
	std::vector<int> intersectLightIDs;
	std::vector<Math::vec4> intersectLightPositions;
	size_t nIntersect;

	std::vector<Math::vec4> insideLightPositions;
	size_t nInside;

	int width;
	int height;

	Rendering::VertexArrayObject vaoLight;
	Rendering::VertexBuffer vboLight;

	Rendering::Shader zeroShader;

	Rendering::Shader lightFrontShader;
	Rendering::Shader lightBackShader;
	Rendering::Shader lightBothShader;

	Rendering::Shader captureGeometryShader;
	Rendering::Shader sortGeometryShader;

	// For constructing and storing the light link list.
	unsigned int lightFragAlloc;

	Rendering::AtomicBuffer lightFragCount;
	Rendering::StorageBuffer tmpLightDepths; // TODO: could replace this with a texture/fbo instead (only if rendering a light at a time), but that seems to be slower.
	Rendering::StorageBuffer lightHeadPtrs;
	Rendering::StorageBuffer lightNextPtrs;
	Rendering::StorageBuffer lightData;

	Rendering::BMA geometryBMA;

private:
	void createLightVAO();

	void allocStorageBuffers();
	void zeroBuffers();

	void captureVisibleLights(App *app);
	void captureLightList(App *app);

	void frustumCullLights(App *app);
	void drawLight(size_t index);

	void setLightListUniforms(Rendering::Shader *shader, bool fragAlloc = true);

	std::string getLightListStr();

	void captureGeometry(App *app);
	void sortGeometry(App *app);

	void setLightRes();

public:
	LightLinkList() : Oit(), nIntersect(0), nInside(0), width(0), height(0), lightFragAlloc(0) {}
	virtual ~LightLinkList() {}

	virtual void init() override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

#endif

