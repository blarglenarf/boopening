#include "Deferred.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

#define DEPTH_ONLY 1

using namespace Rendering;


void Deferred::init()
{
	auto lightOpaqueVert = ShaderSourceCache::getShader("lightDefOpaqueVert").loadFromFile("shaders/oit/deferred/lightOpaque.vert");
	auto lightOpaqueFrag = ShaderSourceCache::getShader("lightDefOpaqueFrag").loadFromFile("shaders/oit/deferred/lightOpaque.frag");
	auto lightOpaqueGeom = ShaderSourceCache::getShader("lightDefOpaqueGeom").loadFromFile("shaders/oit/deferred/lightOpaque.geom");
	lightOpaqueGeom.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightOpaqueFrag.setDefine("DEPTH_ONLY", Utils::toString(DEPTH_ONLY));
	lightOpaqueShader.create(&lightOpaqueVert, &lightOpaqueFrag, &lightOpaqueGeom);

	// TODO: Create the captureLFBShader.
	// TODO: Create the lightLFBShader.
}

void Deferred::renderStandard(App *app)
{
	// Additive blending into the framebuffer.
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	// Either disable or enable face culling if we're rendering billboarded quads or not.
	//glDisable(GL_CULL_FACE);
	glEnable(GL_CULL_FACE);

	// Render lights and composite.
	lightOpaqueShader.bind();

	// Bind g-buffer textures.
	gBuffer.bindTextures();
	gBuffer.setUniforms(&lightOpaqueShader);

	// Bind any other necessary information.
	app->setLightUniforms(&lightOpaqueShader);
	app->setCameraUniforms(&lightOpaqueShader);

	// Render all the lights.
	lightOpaqueShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	lightOpaqueShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	gBuffer.unbindTextures();
	lightOpaqueShader.unbind();
}

#if 0
// This is crazy slow, and completely unnecessary since I have an early out in the light fragment shader.
void Deferred::renderStencil(App *app)
{
	// FIXME: Really should be using the depth/stencil buffers in the g-buffer, but for now we'll render geometry as normal to setup the depth buffer.
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	app->captureGeometry(nullptr, nullptr, nullptr, false);

	// Setup the stencil operation to use (always pass the stencil test, depth is the only test that matters).
	glEnable(GL_STENCIL_TEST);
    glClearStencil(0);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	// Additive blending into the framebuffer.
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	// Always render front faces only.
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// Lights need to read from the depth buffer, but not write.
	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);

	// Render lights and composite.
	lightOpaqueShader.bind();

	// Bind g-buffer textures.
	gBuffer.bindTextures();
	gBuffer.setUniforms(&lightOpaqueShader);

	// Bind any other necessary information.
	app->setLightUniforms(&lightOpaqueShader);
	app->setCameraUniforms(&lightOpaqueShader);

	// Render the lights one at a time.
	auto &sphereLightVao = app->getVaoSphereLights();
	auto &coneLightVao = app->getVaoConeLights();
	auto nSphereLights = app->getNSphereLights();
	auto nConeLights = app->getNConeLights();

	// Start with point lights.
	lightOpaqueShader.setUniform("indexOffset", (unsigned int) 0);
	sphereLightVao.bind();
	for (size_t i = 0; i < nSphereLights; i++)
	{
		// TODO: There are 8 bits to play with in the stencil buffer, can do 8 lights at once.

		// Render the stencil mask.
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glClear(GL_STENCIL_BUFFER_BIT);
		glStencilFunc(GL_ALWAYS, 1, 0xff);
		sphereLightVao.renderRange(i, 1);

		// TODO: Render the stencil mask for debugging.

		// Now render only to stenciled regions.
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glStencilFunc(GL_EQUAL, 1, 0xff);
		sphereLightVao.renderRange(i, 1);
	}
	sphereLightVao.unbind();

	// Now do the same for cone lights.
	lightOpaqueShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	coneLightVao.bind();
	for (size_t i = 0; i < nConeLights; i++)
	{
		// TODO: There are 8 bits to play with in the stencil buffer, can do 8 lights at once.

		// Render the stencil mask.
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glClear(GL_STENCIL_BUFFER_BIT);
		glStencilFunc(GL_ALWAYS, 1, 0xff);
		coneLightVao.renderRange(i, 1);

		// TODO: Render the stencil mask for debugging.

		// Now render only to stenciled regions.
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glStencilFunc(GL_EQUAL, 1, 0xff);
		coneLightVao.renderRange(i, 1);
	}
	coneLightVao.unbind();

	gBuffer.unbindTextures();
	lightOpaqueShader.unbind();

	glDisable(GL_STENCIL_TEST);
	glCullFace(GL_BACK);
}
#endif

void Deferred::render(App *app)
{
	if (opaqueFlag != 1)
		return;

	auto &camera = Renderer::instance().getActiveCamera();
	gBuffer.resize(camera.getWidth(), camera.getHeight());

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Opaque rendering.
	if (opaqueFlag == 1)
	{
		gBuffer.beginCapture();
		app->captureGeometry(nullptr, nullptr, &gBuffer.getCaptureShader(), false);
		gBuffer.endCapture();

		app->getProfiler().start("Light Deferred");

		renderStandard(app);

		// Now apply ambient color.
		gBuffer.applyAmbient();

		app->getProfiler().time("Light Deferred");
	}

	// Transparency.
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		// TODO: Capture data into the lfb.
		//app->captureGeometry(nullptr, nullptr, &captureLFBShader);

		// TODO: Sort lfb data.

		// TODO: Composite.
	}

	glPopAttrib();
}

void Deferred::update(float dt)
{
	(void) (dt);
}

void Deferred::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	gBuffer.setProfiler(&app->getProfiler());

	auto &camera = Renderer::instance().getActiveCamera();
	gBuffer.resize(camera.getWidth(), camera.getHeight());
}

void Deferred::saveLFB(App *app)
{
	app->saveGeometryLFB();
}

