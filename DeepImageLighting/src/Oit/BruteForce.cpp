#include "BruteForce.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

#define COMPOSITE_LOCAL 0

using namespace Rendering;

void BruteForce::render(App *app)
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	
	app->captureGeometry();

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	app->sortGeometry(false);

	// Read the result back from the lfb and composite.
#if !COMPOSITE_LOCAL
	app->getProfiler().start("Composite");
	compositeShader.bind();
	app->getGeometryLfb().composite(&compositeShader);
	compositeShader.unbind();
	app->getProfiler().time("Composite");
#endif

	glPopAttrib();
}

void BruteForce::update(float dt)
{
	(void) (dt);
}

void BruteForce::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
		auto vertComp = ShaderSourceCache::getShader("compBruteVert").loadFromFile("shaders/oit/bruteForce/composite.vert");
		auto fragComp = ShaderSourceCache::getShader("compBruteFrag").loadFromFile("shaders/oit/bruteForce/composite.frag");

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp);
	}
}

void BruteForce::saveLFB(App *app)
{
	app->saveGeometryLFB();
}

