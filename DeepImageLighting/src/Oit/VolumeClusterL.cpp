#if 0

#include "VolumeClusterL.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>
#include <cassert>


// Capture and sort low-res light image.
// Step through light fragments, maintaining an active list, and for each cluster, save the active list of light ids.
// To ensure only as much memory is used as is necessary, store light counts for each cluster (same as linearized lfb).
// At full res, during capture, iterate through light ids for that cluster and apply lighting.
// Save light ids only for clusters that contain geometry.
// TODO: determine cluster space using farthest fragment, which can be obtained during cluster culling.

#define USE_BMA 1
#define MAX_FRAGS 512
#define CLUSTERS 32
#define MASK_SIZE (CLUSTERS / 32)
#define MAX_EYE_Z 30.0
#define CLUSTER_CULL 1
#define PROFILE_MEMORY 1

#define LIGHT_OVERALLOCATE 1.2
#define LIGHT_UNDERALLOCATE 0.5

using namespace Rendering;



void VolumeClusterL::countClusters(App *app)
{
	(void) (app);

	app->getProfiler().start("Cluster Count");

	if (!countClusterShader.isGenerated())
	{
		auto countClusterVert = ShaderSourceCache::getShader("countClusterVert").loadFromFile("shaders/oit/volumeClusterL/countCluster.vert");
		auto countClusterFrag = ShaderSourceCache::getShader("countClusterFrag").loadFromFile("shaders/oit/volumeClusterL/countCluster.frag");
		countClusterFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
		countClusterFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
		countClusterFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
		countClusterFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
		
		countClusterShader.create(&countClusterVert, &countClusterFrag);
	}

	countClusterShader.bind();

	//cluster.setUniforms(&countClusterShader);
#if CLUSTER_CULL
	mask.setUniforms(&countClusterShader);
#endif

	countClusterShader.setUniform("size", Math::ivec2(width, height));

	// Need to bind the light lfb uniforms as well.
	lightLFB.setUniforms(&countClusterShader);

	cluster.beginCountFrags(&countClusterShader);

	// Render fullscreen quad, and count the clusters here.
	Renderer::instance().drawQuad();

	countClusterShader.unbind();

	app->getProfiler().time("Cluster Count");

	app->getProfiler().start("Cluster Prefix");
	//cluster.prefixSums();
	cluster.endCountFrags();
	app->getProfiler().time("Cluster Prefix");
}

void VolumeClusterL::createClusters()
{
	createClusterShader.bind();
	cluster.beginComposite(&createClusterShader);
#if CLUSTER_CULL
	mask.setUniforms(&createClusterShader);
#endif
	lightLFB.beginComposite(&createClusterShader);

	Renderer::instance().drawQuad();

	lightLFB.endComposite();
	cluster.endComposite();
	createClusterShader.unbind();
}



void VolumeClusterL::init()
{
	auto lightCountVert = ShaderSourceCache::getShader("countVolumeClusterLVert").loadFromFile("shaders/volumeLight/countLight.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countVolumeClusterLGeom").loadFromFile("shaders/volumeLight/countLight.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countVolumeClusterLFrag").loadFromFile("shaders/volumeLight/countLight.frag");
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);
}

void VolumeClusterL::createClusterMask(App *app)
{
	app->getProfiler().start("Cluster Mask");

	mask.beginCreateMask();

	// Render geometry.
	app->getGpuMesh().render(false);
	
	mask.endCreateMask();

	app->getProfiler().time("Cluster Mask");
}

void VolumeClusterL::captureLighting(App *app)
{
	app->getProfiler().start("Light Capture");

	// TODO: use conservative rasterization.
	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_CULL_FACE);

	auto &camera = Renderer::instance().getActiveCamera();

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	lightLFB.resize(width, height);

	if (lightLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		lightLFB.beginCountFrags(&countLightShader);
		countLightShader.setUniform("mvMatrix", camera.getInverse());
		countLightShader.setUniform("pMatrix", camera.getProjection());
		countLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		countLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
		countLightShader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());
		countLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		countLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();
		lightLFB.endCountFrags(&countLightShader);
	}

	captureLightShader.bind();
	captureLightShader.setUniform("mvMatrix", camera.getInverse());
	captureLightShader.setUniform("pMatrix", camera.getProjection());
	captureLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	captureLightShader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());

	// Capture fragments into the lfb.
	lightLFB.beginCapture(&captureLightShader);
	captureLightShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (lightLFB.endCapture())
	{
		lightLFB.beginCapture(&captureLightShader);
		captureLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();

		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	captureLightShader.unbind();

	// Restore original size.
	app->restoreViewport();

	glPopAttrib();

	app->getProfiler().time("Light Capture");
}

void VolumeClusterL::sortLighting(App *app)
{
	app->getProfiler().start("Light Sort");

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	lightBMA.createMask(&lightLFB);
	lightBMA.sort(&lightLFB);
#else
	// Sort and composite normally.
	sortLightShader.bind();
	lightLFB.composite(&sortLightShader); // TODO: change this to a sort method.
	sortLightShader.unbind();
#endif

	// Restore original size.
	app->restoreViewport();

	app->getProfiler().time("Light Sort");
}

void VolumeClusterL::createLightClusters(App *app)
{
	// Resize based on grid size.
	app->setTmpViewport(width, height);

	// Zero offsets (one per cluster) and count light ids per cluster.
	//app->getProfiler().start("Cluster Zero");
	//cluster.zeroBuffers();
	//app->getProfiler().time("Cluster Zero");

	countClusters(app);

	app->getProfiler().start("Light Cluster");

	// Now store the ids in their clusters.
	createClusters();

	app->getProfiler().time("Light Cluster");

	// Restore original size.
	app->restoreViewport();
}

void VolumeClusterL::captureGeometry(App *app)
{
	app->getProfiler().start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();
	auto &geometryLFB = app->getGeometryLfb();
	auto &gpuMesh = app->getGpuMesh();

	geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (geometryLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags();
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh.render(false);
		geometryLFB.endCountFrags();
	}

	captureGeometryShader.bind();
	captureGeometryShader.setUniform("mvMatrix", camera.getInverse());
	captureGeometryShader.setUniform("pMatrix", camera.getProjection());
	captureGeometryShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	captureGeometryShader.setUniform("nSphereLights", (unsigned int) app->getNSphereLights());

	captureGeometryShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureGeometryShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());

	captureGeometryShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
	captureGeometryShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	captureGeometryShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());

	captureGeometryShader.setUniform("LightColors", &app->getLightColorBuffer());

	// Capture fragments into the lfb using light link list to apply lighting.
	geometryLFB.beginCapture(&captureGeometryShader);
	cluster.setUniforms(&captureGeometryShader);
	gpuMesh.render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (geometryLFB.endCapture())
	{
		geometryLFB.beginCapture(&captureGeometryShader);
		cluster.setUniforms(&captureGeometryShader);
		gpuMesh.render();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}
	captureGeometryShader.unbind();

	CHECKERRORS;

	app->getProfiler().time("Geometry Capture");

#if PROFILE_MEMORY
	unsigned int lightSize = (unsigned int) (lightLFB.getFragAlloc() * lightLFB.getFragSize() * sizeof(float));
	app->getProfiler().setBufferSize("lightDepths", lightSize + (width * height * sizeof(unsigned int)));
#endif
}

void VolumeClusterL::sortGeometry(App *app)
{
	app->getProfiler().start("Geometry Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	geometryBMA.createMask(&app->getGeometryLfb());
	geometryBMA.sort(&app->getGeometryLfb());
#else
	// Sort and composite normally.
	sortGeometryShader.bind();
	app->getGeometryLfb().composite(&sortGeometryShader); // TODO: change this to a sort method.
	sortGeometryShader.unbind();
#endif

	app->getProfiler().time("Geometry Sort");

	app->getProfiler().start("Composite");
	app->getProfiler().time("Composite");
}

void VolumeClusterL::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Re-allocate cluster data if the camera has resized.
	if (width != camera.getWidth() / GRID_CELL_SIZE || height != camera.getHeight() / GRID_CELL_SIZE)
	{
		width = camera.getWidth() / GRID_CELL_SIZE;
		height = camera.getHeight() / GRID_CELL_SIZE;
		cluster.resize(width, height);

	#if CLUSTER_CULL
		mask.resize(width, height);
	#endif
	}

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	createClusterMask(app);
#endif

	// Create sorted light image.
	captureLighting(app);
	sortLighting(app);

	// Create light clusters.
	createLightClusters(app);

	// Capture geometry, using light clusters to apply lighting.
	captureGeometry(app);

	// Sort and composite as normal.
	sortGeometry(app);
}

void VolumeClusterL::update(float dt)
{
	(void) (dt);
}

void VolumeClusterL::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	int fragSize = 2;

	auto &camera = Renderer::instance().getActiveCamera();
	width = camera.getWidth() / GRID_CELL_SIZE;
	height = camera.getHeight() / GRID_CELL_SIZE;

	//lightLFB.setType(type, "light", fragSize);
	lightLFB.setType(Rendering::LFB::LINEARIZED, "light", fragSize, false);
	lightLFB.setMaxFrags(MAX_FRAGS);
	lightLFB.setProfiler(&app->getProfiler());

	cluster.setType(Cluster::LINEARIZED, "cluster", CLUSTERS);
	cluster.setProfiler(&app->getProfiler());

	// Shader for capturing light fragments.
	auto vertLightCap = ShaderSourceCache::getShader("capVolumeClusterLVert").loadFromFile("shaders/volumeLight/captureLight.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capVolumeClusterLGeom").loadFromFile("shaders/volumeLight/captureLight.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capVolumeClusterLFrag").loadFromFile("shaders/volumeLight/captureLight.frag");

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);

#if !USE_BMA
	// Shader for sorting light fragments (not used if you're using BMA).
	auto vertLightSort = ShaderSourceCache::getShader("sortVolumeClusterLVert").loadFromFile("shaders/sort/sort.vert");
	auto fragLightSort = ShaderSourceCache::getShader("sortVolumeClusterLFrag").loadFromFile("shaders/sort/sortL.frag");
	fragLightSort.setDefine("MAX_FRAGS", Utils::toString(lightLFB.getMaxFrags()));
	fragLightSort.setDefine("COMPOSITE_LOCAL", Utils::toString(0));
	fragLightSort.setDefine("FRAG_SIZE", Utils::toString(fragSize));
	fragLightSort.replace("LFB_NAME", "light_L");

	sortLightShader.release();
	sortLightShader.create(&vertLightSort, &fragLightSort);
#endif

	// Shader for creating light clusters.
	auto vertCluster = ShaderSourceCache::getShader("volumeClusterLVert").loadFromFile("shaders/oit/volumeClusterL/createCluster.vert");
	auto fragCluster = ShaderSourceCache::getShader("volumeClusterLFrag").loadFromFile("shaders/oit/volumeClusterL/createCluster.frag");
	fragCluster.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragCluster.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragCluster.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	createClusterShader.release();
	createClusterShader.create(&vertCluster, &fragCluster);

	// Shader for capturing geometry, applies lighting during capture.
	auto &geomCapVert = ShaderSourceCache::getShader("volumeClusterLCapVert").loadFromFile("shaders/oit/volumeClusterL/capture.vert");
	auto &geomCapFrag = ShaderSourceCache::getShader("volumeClusterLCapFrag").loadFromFile("shaders/oit/volumeClusterL/capture.frag");
	geomCapFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	geomCapFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));

	captureGeometryShader.release();
	captureGeometryShader.create(&geomCapVert, &geomCapFrag);

#if !USE_BMA
	// Shader for sorting geometry, composites locally, not used if using BMA.
	auto vertSort = ShaderSourceCache::getShader("clusterGeoVert").loadFromFile("shaders/sort/sort.vert");
	auto fragSort = ShaderSourceCache::getShader("clusterGeoFrag").loadFromFile("shaders/sort/sort.frag");
	fragSort.setDefine("MAX_FRAGS", Utils::toString(app->getGeometryLfb().getMaxFrags()));
	fragSort.setDefine("COMPOSITE_LOCAL", Utils::toString(1));
	fragSort.replace("LFB_NAME", "lfb");

	sortGeometryShader.release();
	sortGeometryShader.create(&vertSort, &fragSort);
#endif

#if USE_BMA
	// Init BMA mask and sort shaders.
	lightBMA.createMaskShader("light", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMaskL.frag", {{"FRAG_SIZE", Utils::toString(fragSize)}});
	lightBMA.createShaders(&lightLFB, "shaders/sort/sort.vert", "shaders/sort/sortL.frag", {{"COMPOSITE_LOCAL", Utils::toString(0)}, {"FRAG_SIZE", Utils::toString(fragSize)}});

	geometryBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
	geometryBMA.createShaders(&app->getGeometryLfb(), "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}});
#endif

	cluster.resize(width, height);
#if CLUSTER_CULL
	mask.setProfiler(&app->getProfiler());
	mask.regen(CLUSTERS, MAX_EYE_Z, GRID_CELL_SIZE);
	mask.resize(width, height);
#endif
}

void VolumeClusterL::saveLFB(App *app)
{
	(void) (app);

	auto s = cluster.getStrSorted();
	Utils::File f("volumeClusterL.txt");
	f.writeLine(s);
}

#endif

