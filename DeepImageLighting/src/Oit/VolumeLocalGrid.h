#pragma once

#if 0

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"
#include "../../../Renderer/src/LFB/ClusterMask.h"

class VolumeLocalGrid : public Oit
{
private:
	Rendering::LFB lightLFB;

	Rendering::Shader captureLightShader;
	Rendering::Shader sortLightShader;
	Rendering::Shader countLightShader;
	Rendering::Shader sortGeometryShader;

	Rendering::BMA lightBMA;
	Rendering::BMA geometryBMA;

	Rendering::StorageBuffer passCount;
	Rendering::StorageBuffer failCount;
	
	Rendering::ClusterMask mask;

	int width;
	int height;

private:
	void sortWithBMA(App *app);

public:
	VolumeLocalGrid() : Oit(), width(0), height(0) {}
	virtual ~VolumeLocalGrid() {}

	virtual void init() override;

	void createClusterMask(App *app);

	void captureLighting(App *app);
	void sortLighting(App *app);

	void sortGeometry(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

#endif

