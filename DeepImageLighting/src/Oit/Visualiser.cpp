#include "Visualiser.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <iostream>

#define MAX_FRAGS 512
#define GEO_DRAW 1
#define LIGHT_DRAW 1

using namespace Rendering;
using namespace Math;


static vec4 getEyeFromWindow(vec3 p, vec4 viewport, mat4 invPMatrix)
{
	vec3 ndcPos;
	ndcPos.xy = ((p.xy - viewport.xy) / viewport.zw) * 2.0f - 1.0f;
	//ndcPos.xy = ((p.xy * 2.0f) - (viewport.xy * 2.0f)) / (viewport.zw) - 1.0f;
	ndcPos.z = 1.0f;
	//std::cout << "ndc: " << ndcPos << "\n";

	vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0f);
	eyeDir /= eyeDir.w;
	vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0f);
	return eyePos;
}

static vec4 computePlane(vec3 p0, vec3 p1, vec3 p2)
{
#if 1
	vec3 v0 = p1 - p0;
	vec3 v2 = p2 - p0;
#else
	vec3 v0 = p0 - p1;
	vec3 v2 = p0 - p2;
#endif
	vec3 n = cross(v0, v2);
	float l = n.len();

	vec4 plane(n.x, n.y, n.z, 1.0f);
	plane /= l;
	plane.w = p1.dot(plane.xyz);

	return plane;
}

#if TEST_FRUSTUMS
	static vec4 clipToEye(vec4 clip, mat4 invPMatrix)
	{
		vec4 view = invPMatrix * clip;
		view = view / view.w;
		return view;
	}

	static vec4 screenToEye(vec4 screen, vec4 viewport, mat4 invPMatrix)
	{
		vec2 texCoord = screen.xy / viewport.zw;
		vec4 clip = vec4(vec2(texCoord.x, 1.0f - texCoord.y) * 2.0f - 1.0f, screen.z, screen.w);
		return clipToEye(clip, invPMatrix);
	}
#endif


void Visualiser::createFrustums()
{
	int width = 10;
	int height = 10;

	mat4 mvMatrix = Renderer::instance().getActiveCamera().getInverse(); (void) (mvMatrix);
	mat4 invMvMatrix = mvMatrix.getInverse(); (void) (invMvMatrix);
	mat4 invPMatrix = Renderer::instance().getActiveCamera().getProjection().getInverse(); (void) (invPMatrix);
	ivec4 view = Renderer::instance().getActiveCamera().getViewport(); (void) (view);
	vec4 viewport(view.x, view.y, view.z, view.w);
	vec3 eyePos(0, 0, 0); (void) (eyePos);

	int nTiles = width * height; (void) (nTiles);
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			int tile = width * y + x;
			vec2 tileSize(viewport.z / width, viewport.w / height);

		#if 0
			// Step sizes are in clip space, get them into eye space.
			//vec2 negativeStep = (2.0f * vec2(tile, tile)) / vec2(nTiles, nTiles);
			//vec2 positiveStep = (2.0f * vec2(tile + 1, tile + 1)) / vec2(nTiles, nTiles);

			//vec2 negativeStep = (2.0f * vec2(x * tileSize.x, y * tileSize.y)) / vec2(viewport.z, viewport.w);
			//vec2 positiveStep = (2.0f * vec2((x + 1) * tileSize.x, (y + 1) * tileSize.y)) / vec2(viewport.z, viewport.w);
			//negativeStep = (invPMatrix * vec4(negativeStep, 0, 1)).xy;
			//positiveStep = (invPMatrix * vec4(positiveStep, 0, 1)).xy;

			vec4 negativeStep = invMvMatrix * getEyeFromWindow(vec3(x * tileSize.x, y * tileSize.y, 1), viewport, invPMatrix);
			vec4 positiveStep = invMvMatrix * getEyeFromWindow(vec3((x + 1) * tileSize.x, (y + 1) * tileSize.y, 1), viewport, invPMatrix);

			// Frustum planes are left, right, top, bottom.
			frustumPlanes[tile * 4] = vec4(1, 0, 0, 1.0f - negativeStep.x);
			frustumPlanes[tile * 4 + 1] = vec4(-1, 0, 0, -1.0f + positiveStep.x);
			frustumPlanes[tile * 4 + 2] = vec4(0, -1, 0, -1.0f + positiveStep.y);
			frustumPlanes[tile * 4 + 3] = vec4(0, 1, 0, 1.0f - negativeStep.y);

			// Convert to eye space.
			for (int i = 0; i < 4; i++)
			{
				frustumPlanes[tile * 4 + i] = mvMatrix * frustumPlanes[tile * 4 + i];
				frustumPlanes[tile * 4 + i] /= vec3(frustumPlanes[tile * 4 + i].xyz).len();
			}
		#else
			// Four points on the far clipping plane.
			vec4 ssPos[4];
			ssPos[0] = vec4(x * tileSize.x, y * tileSize.y, -1, 1);
			ssPos[1] = vec4((x + 1) * tileSize.x, y * tileSize.y, -1, 1);
			ssPos[2] = vec4(x * tileSize.x, (y + 1) * tileSize.y, -1, 1);
			ssPos[3] = vec4((x + 1) * tileSize.x, (y + 1) * tileSize.y, -1, 1);

			vec3 esPos[4];
			for (int i = 0; i < 4; i++)
				esPos[i] = getEyeFromWindow(ssPos[i].xyz, viewport, invPMatrix).xyz;

			// Frustum planes from eye space points, planes are left, right, top, bottom.
			frustumPlanes[tile * 4] = computePlane(eyePos, esPos[2], esPos[0]);
			frustumPlanes[tile * 4 + 1] = computePlane(eyePos, esPos[1], esPos[3]);
			frustumPlanes[tile * 4 + 2] = computePlane(eyePos, esPos[0], esPos[1]);
			frustumPlanes[tile * 4 + 3] = computePlane(eyePos, esPos[3], esPos[2]);

			#if 0
				// Need to update w values.
				// FIXME: Seems to be incorrect...
				vec2 negStep = (2.0f * vec2(x * tileSize.x, y * tileSize.y)) / vec2(viewport.z, viewport.w);
				vec2 posStep = (2.0f * vec2((x + 1) * tileSize.x, (y + 1) * tileSize.y)) / vec2(viewport.z, viewport.w);
				negStep = (invPMatrix * vec4(negStep, -1, 1)).xy;
				posStep = (invPMatrix * vec4(posStep, -1, 1)).xy;

				frustumPlanes[tile * 4].w = 1.0f - negStep.x;
				frustumPlanes[tile * 4 + 1].w = -1.0f + posStep.x;
				frustumPlanes[tile * 4 + 2].w = -1.0f + posStep.y;
				frustumPlanes[tile * 4 + 3].w = 1.0f - negStep.y;
				frustumPlanes[tile * 4].w /= frustumPlanes[tile * 4].xyz.len();
				frustumPlanes[tile * 4 + 1].w /= frustumPlanes[tile * 4 + 1].xyz.len();
				frustumPlanes[tile * 4 + 2].w /= frustumPlanes[tile * 4 + 2].xyz.len();
				frustumPlanes[tile * 4 + 3].w /= frustumPlanes[tile * 4 + 3].xyz.len();
			#endif
		#endif
		}
	}
}

void Visualiser::drawFrustums()
{
	int width = 10;
	int height = 10;

	mat4 mvMatrix = Renderer::instance().getActiveCamera().getInverse(); (void) (mvMatrix);
	mat4 invMvMatrix = mvMatrix.getInverse(); (void) (invMvMatrix);
	mat4 invPMatrix = Renderer::instance().getActiveCamera().getProjection().getInverse(); (void) (invPMatrix);
	ivec4 view = Renderer::instance().getActiveCamera().getViewport(); (void) (view);
	vec4 viewport(view.x, view.y, view.z, view.w);
	vec3 eyePos(0, 0, 0); (void) (eyePos);

	int nTiles = width * height; (void) (nTiles);

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			int tile = width * y + x;
			vec2 tileSize(viewport.z / width, viewport.w / height);

			auto &left = frustumPlanes[tile * 4];
			auto &right = frustumPlanes[tile * 4 + 1];
			auto &top = frustumPlanes[tile * 4 + 2];
			auto &bottom = frustumPlanes[tile * 4 + 3];
			
	
			(void) (tileSize);
			(void) (left);
			(void) (right);
			(void) (top);
			(void) (bottom);

			// TODO: Draw these...
		}
	}
}

void Visualiser::countLights(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Count the number of frags for capture.
	lightLFB.beginCountFrags();
	countLightShader.bind();
	app->setCameraUniforms(&countLightShader);
	app->setLightUniforms(&countLightShader);
	countLightShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&countLightShader);
#endif
	lightLFB.setCountUniforms(&countLightShader);
	countLightShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	countLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	countLightShader.unbind();
	lightLFB.endCountFrags();
}

void Visualiser::createLights(App *app)
{
	captureLightShader.bind();

	lightLFB.setUniforms(&captureLightShader, false);
	app->setCameraUniforms(&captureLightShader);
	app->setLightUniforms(&captureLightShader);
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&captureLightShader);
#endif

	captureLightShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	captureLightShader.unbind();
}

void Visualiser::captureLighting(App *app)
{
	app->getProfiler().start("Light Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	lightLFB.resize(camera.getWidth(), camera.getHeight());

	if (lightLFB.getType() == LFB::LINEARIZED)
		countLights(app);

	lightLFB.beginCapture();
	createLights(app);
	if (lightLFB.endCapture())
	{
		lightLFB.beginCapture();
		createLights(app);
		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	app->getProfiler().time("Light Capture");
}

void Visualiser::sortLighting(App *app)
{
	app->getProfiler().start("Light Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
	lightBMA.createMask(&lightLFB);
	lightBMA.sort(&lightLFB);

	app->getProfiler().time("Light Sort");
}

void Visualiser::composite(App *app)
{
	(void) (app);

	// Composite for light and geometry drawing.
	compositeShader.bind();

#if GEO_DRAW
	app->getGeometryLfb().beginComposite();
#endif
#if LIGHT_DRAW
	lightLFB.beginComposite();
#endif

#if GEO_DRAW
	app->getGeometryLfb().setUniforms(&compositeShader, false);
#endif
#if LIGHT_DRAW
	lightLFB.setUniforms(&compositeShader, false);
#endif

	Renderer::instance().drawQuad();

#if GEO_DRAW
	app->getGeometryLfb().endComposite();
#endif
#if LIGHT_DRAW
	lightLFB.endComposite();
#endif

	compositeShader.unbind();
}


void Visualiser::init()
{
}

void Visualiser::render(App *app)
{
#if CLUSTER_CULL
	auto &camera = Renderer::instance().getActiveCamera();

	// Re-allocate cluster data if the camera has resized.
	app->getDepthMask().resize(camera.getWidth(), camera.getHeight());

	// Create masks to determine which clusters are active.
	app->createDepthMask();
#endif

	if (opaqueFlag == 1)
	{
		// FIXME: Can't really do anything here, it's not supposed to be opaque...
		return;

		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		// Capture geometry, using light information.
		app->captureGeometry(&lightLFB, nullptr, &captureGeometryShader);

		glPopAttrib();
	}
	else
	{
		glPushAttrib(GL_POLYGON_BIT | GL_ENABLE_BIT);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		// Capture and sort lighting, saving back to global memory.
	#if LIGHT_DRAW
		captureLighting(app);
		sortLighting(app);
	#endif

		// Then the geometry, saving to global memory.
	#if GEO_DRAW
		app->captureGeometry(&lightLFB, nullptr, &captureGeometryShader);
		app->sortGeometry(false);
	#endif

		// Lastly, composite.
		composite(app);

		glPopAttrib();
	}
}

void Visualiser::update(float dt)
{
	(void) (dt);
}

void Visualiser::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	// Always use linearized for the lights.
	//lightLFB.setType(type, "light", fragSize);
	lightLFB.setType(Rendering::LFB::LINEARIZED, "light", LFB::VEC2, false);
	lightLFB.setMaxFrags(MAX_FRAGS);
	lightLFB.setProfiler(&app->getProfiler());

	reloadShaders(app);
}

void Visualiser::reloadShaders(App *app)
{
	(void) (app);

	auto vertLightCap = ShaderSourceCache::getShader("capVisualVert").loadFromFile("shaders/light/captureLightRay.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capVisualGeom").loadFromFile("shaders/light/captureLightRay.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capVisualFrag").loadFromFile("shaders/light/captureLightRay.frag");
	geomLightCap.setDefine("CLUSTER_CULL", "0");
	fragLightCap.setDefine("CLUSTER_CULL", "0");
	fragLightCap.setDefine("USE_CLUSTER", "0");
	fragLightCap.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	fragLightCap.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	fragLightCap.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragLightCap.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	auto lightCountVert = ShaderSourceCache::getShader("countVisualVert").loadFromFile("shaders/light/countLightRay.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countVisualGeom").loadFromFile("shaders/light/countLightRay.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countVisualFrag").loadFromFile("shaders/light/countLightRay.frag");
	lightCountGeom.setDefine("CLUSTER_CULL", "0");
	lightCountFrag.setDefine("CLUSTER_CULL", "0");
	lightCountFrag.setDefine("USE_CLUSTER", "0");
	lightCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCountFrag.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);

	// Shader for capturing geometry.
	auto vertCapture = ShaderSourceCache::getShader("captureVisualVert").loadFromFile("shaders/oit/visual/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureVisualFrag").loadFromFile("shaders/oit/visual/capture.frag");
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);

	// Composite shader.
	auto vertComp = ShaderSourceCache::getShader("compVisualVert").loadFromFile("shaders/oit/visual/composite.vert");
	auto fragComp = ShaderSourceCache::getShader("compVisualFrag").loadFromFile("shaders/oit/visual/composite.frag");
	fragComp.setDefine("GEO_DRAW", Utils::toString(GEO_DRAW));
	fragComp.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));

	compositeShader.release();
	compositeShader.create(&vertComp, &fragComp);

	// Sorting light fragments.
	lightBMA.createMaskShader("light", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMaskL.frag");
	lightBMA.createShaders(&lightLFB, "shaders/sort/sort.vert", "shaders/sort/sortL.frag");
}

void Visualiser::saveLFB(App *app)
{
	auto s = lightLFB.getStrSorted();
	Utils::File f("lightVisualLFB.txt");
	f.write(s);

	app->saveGeometryLFB();
}

