#if 0

#include "LightLinkList.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"
#include "../../../Utils/src/File.h"

#include <string>

//
// Essentially an implementation of Abdul Bezrati's Light Link List paper, with a few modifications.
// Original paper at:
// http://proquest.safaribooksonline.com/book/programming/graphics/9781482264623/chapter-iii-1-real-time-lighting-via-light-linked-list/chapter_iii1_realtime_lighting#X2ludGVybmFsX0J2ZGVwRmxhc2hSZWFkZXI/eG1saWQ9OTc4MTQ4MjI2NDYyMy8xODc=
// DirectX+hlsl sample code at:
// https://github.com/JavaCoolDude/Advanced-Lighting
//

#define USE_BMA 1
#define MAX_FRAGS 512
#define DEPTH_LAYERS 512

// Need 64 bit atomic operations if you set this to 0.
#define DEPTH_16 1

#define LIGHT_OVERALLOCATE 1.2
#define LIGHT_UNDERALLOCATE 0.5

using namespace Rendering;

#if !DEPTH_16
static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	#define DEPTH_LAYERS " + Utils::toString(DEPTH_LAYERS) + "\n\
	\
	buffer HeadPtrs\
	{\
		uint headPtrs[];\
	};\
	\
	buffer TmpLightDepths\
	{\
		uvec2 tmpLightDepths[];\
	};\
	\
	void main()\
	{\
		headPtrs[gl_VertexID] = 0;\
		for (int i = 0; i < DEPTH_LAYERS; i++)\
		{\
			tmpLightDepths[gl_VertexID * i] = uvec2(0);\
		}\
	}\
	";
}
#else
static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	#define DEPTH_LAYERS " + Utils::toString(DEPTH_LAYERS) + "\n\
	\
	buffer HeadPtrs\
	{\
		uint headPtrs[];\
	};\
	\
	buffer TmpLightDepths\
	{\
		uint tmpLightDepths[];\
	};\
	\
	void main()\
	{\
		headPtrs[gl_VertexID] = 0;\
		for (int i = 0; i < DEPTH_LAYERS; i++)\
		{\
			tmpLightDepths[gl_VertexID * i] = 0;\
		}\
	}\
	";
}
#endif


// TODO: handle different size lights.
void LightLinkList::createLightVAO()
{
	#define TRI(i1, i2, i3) indices.push_back(i1); indices.push_back(i2); indices.push_back(i3);

	float size = 0.48f;
	if (GRID_CELL_SIZE == 8)
		size = 0.5f;
	else if (GRID_CELL_SIZE == 16)
		size = 0.6f;
	else if (GRID_CELL_SIZE == 32)
		size = 0.8f;

	float t = 1.618033f * size;

	std::vector<Math::vec4> vertices(12);
	std::vector<unsigned int> indices;
	indices.reserve(60);

	vertices[0]  = Math::vec4(-size,  t,  0, 1);
	vertices[1]  = Math::vec4( size,  t,  0, 1);
	vertices[2]  = Math::vec4(-size, -t,  0, 1);
	vertices[3]  = Math::vec4( size, -t,  0, 1);
	vertices[4]  = Math::vec4( 0, -size,  t, 1);
	vertices[5]  = Math::vec4( 0,  size,  t, 1);
	vertices[6]  = Math::vec4( 0, -size, -t, 1);
	vertices[7]  = Math::vec4( 0,  size, -t, 1);
	vertices[8]  = Math::vec4( t,  0, -size, 1);
	vertices[9]  = Math::vec4( t,  0,  size, 1);
	vertices[10] = Math::vec4(-t,  0, -size, 1);
	vertices[11] = Math::vec4(-t,  0,  size, 1);

	// 5 faces around point 0.
	TRI(0, 11, 5);
	TRI(0, 5, 1);
	TRI(0, 1, 7);
	TRI(0, 7, 10);
	TRI(0, 10, 11);

	// 5 adjacent faces.
	TRI(1, 5, 9);
	TRI(5, 11, 4);
	TRI(11, 10, 2);
	TRI(10, 7, 6);
	TRI(7, 1, 8);

	// 5 faces around point 3.
	TRI(3, 9, 4);
	TRI(3, 4, 2);
	TRI(3, 2, 6);
	TRI(3, 6, 8);
	TRI(3, 8, 9);

	// 5 adjacent faces.
	TRI(4, 9, 5);
	TRI(2, 4, 11);
	TRI(6, 2, 10);
	TRI(8, 6, 7);
	TRI(9, 8, 1);

	vaoLight.create(VertexFormat({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} }), &vertices[0], sizeof(vertices[0]) * vertices.size(), sizeof(vertices[0]), &indices[0], indices.size());
}

void LightLinkList::frustumCullLights(App *app)
{
	auto &lightPositions = app->getSphereLightPositions();
	auto &lightRadii = app->getSphereLightRadii();
	auto &camera = Renderer::instance().getActiveCamera();

	if (lightPositions.size() != intersectLightPositions.size())
	{
		intersectLightIDs.resize(lightPositions.size());
		intersectLightPositions.resize(lightPositions.size());
	}
	if (lightPositions.size() != insideLightPositions.size())
		insideLightPositions.resize(lightPositions.size());

	nIntersect = 0;
	nInside = 0;
	for (size_t i = 0; i < lightPositions.size(); i++)
	{
		auto col = camera.isInFrustum(lightPositions[i].xyz, lightRadii[i]);
		if (col == Camera::OUT)
			continue;
	#if DEPTH_16
		else if (col == Camera::IN)
		{
			insideLightPositions[nInside] = lightPositions[i];
			insideLightPositions[nInside].w = (float) i; // TODO: something other than this...
			nInside++;
		}
		else
		{
			intersectLightPositions[nIntersect] = lightPositions[i];
			intersectLightIDs[nIntersect] = (int) i;
			nIntersect++;
		}
	#else
		intersectLightPositions[nIntersect] = lightPositions[i];
		intersectLightIDs[nIntersect] = i;
		nIntersect++;
	#endif
	}
}

void LightLinkList::allocStorageBuffers()
{
	// Add multiple layers to the tmp depths buffer.
#if !DEPTH_16
	tmpLightDepths.create(nullptr, width * height * sizeof(unsigned int) * 2 * DEPTH_LAYERS);
#else
	tmpLightDepths.create(nullptr, width * height * sizeof(unsigned int) * DEPTH_LAYERS);
#endif
	lightHeadPtrs.create(nullptr, width * height * sizeof(unsigned int));

	if (!lightFragCount.isGenerated())
	{
		// Have to start at one since zero is being used as null.
		unsigned int one = 1;
		lightFragCount.create(&one, sizeof(one));
	}

	// For now just allocate 5 frags per pixel for the lfb, this will dynamically resize later.
	if (lightFragAlloc == 0)
		lightFragAlloc = width * height * 5;

	// No need to buffer the nextPtrs or data if they've already been created, if more are needed we handle it after capture.
	if (!lightNextPtrs.isGenerated())
		lightNextPtrs.create(nullptr, lightFragAlloc * sizeof(unsigned int));

	// Pack fragment data into two floats (requires 32 bits for id, and 16 bits for each of front and back depth).
#if !DEPTH_16
	if (!lightData.isGenerated())
		lightData.create(nullptr, lightFragAlloc * 4 * sizeof(float));
#else
	if (!lightData.isGenerated())
		lightData.create(nullptr, lightFragAlloc * 2 * sizeof(float));
#endif
}

void LightLinkList::zeroBuffers()
{
	if (!zeroShader.isGenerated())
	{
		auto zeroSrc = ShaderSource("zeroLFB_LL", getZeroShaderSrc());
		zeroShader.create(&zeroSrc);
	}

	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("HeadPtrs", &lightHeadPtrs);
	zeroShader.setUniform("TmpLightDepths", &tmpLightDepths);
	glDrawArrays(GL_POINTS, 0, width * height);
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);
}

void LightLinkList::captureVisibleLights(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	if (nInside > 0)
		vaoLight.bufferData(VertexFormat({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} }), &insideLightPositions[0], nInside * sizeof(Math::vec4), sizeof(Math::vec4));

	// Zero the head pointers.
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	zeroBuffers();
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Render all visible, non-intersecting lights at once.
	if (nInside > 0)
	{
		lightBothShader.bind();
		setLightListUniforms(&lightBothShader);
		lightBothShader.setUniform("mvMatrix", camera.getInverse());
		lightBothShader.setUniform("pMatrix", camera.getProjection());
		lightBothShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
		lightBothShader.setUniform("TmpLightDepths", &tmpLightDepths);
		lightBothShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	
		vaoLight.render();
		lightBothShader.unbind();

		glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	// Render intersecting lights individually, front faces, then back faces.
	if (nIntersect > 0)
	{
		// For each light, save front depth, then back depth, then save to light link list.
		for (size_t i = 0; i < nIntersect; i++)
			drawLight(i);

		glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}
}

void LightLinkList::captureLightList(App *app)
{
	app->getProfiler().start("Light Capture");

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	// Frustum culling of all lights.
	frustumCullLights(app);

	captureVisibleLights(app);

	// It's possible that not enough (or far too many) frags were allocated, if so, reallocate.
	unsigned int totalFrags = *((unsigned int*) lightFragCount.read());

	bool resizeFlag = false;
	if (totalFrags > lightFragAlloc || totalFrags < lightFragAlloc * LIGHT_UNDERALLOCATE)
	{
		lightFragAlloc = (unsigned int) (totalFrags * LIGHT_OVERALLOCATE);
		resizeFlag = true;
	}
	
	if (resizeFlag)
	{
		lightNextPtrs.bufferData(nullptr, lightFragAlloc * sizeof(unsigned int));
	#if !DEPTH_16
		lightData.bufferData(nullptr, lightFragAlloc * 4 * sizeof(float));
	#else
		lightData.bufferData(nullptr, lightFragAlloc * 2 * sizeof(float));
	#endif
	}

	totalFrags = 1;
	lightFragCount.bufferData(&totalFrags, sizeof(totalFrags));

	// Zero the buffers again and re-render the visible lights if necessary.
	if (resizeFlag)
		captureVisibleLights(app);

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Restore original size.
	app->restoreViewport();

	app->getProfiler().time("Light Capture");

	app->getProfiler().start("Light Sort");
	app->getProfiler().time("Light Sort");
}

void LightLinkList::drawLight(size_t index)
{
	auto &pos = intersectLightPositions[index];
	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glEnable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	// Get light's modelview matrix.
	auto mv = Math::getTranslate(pos.x, pos.y, pos.z) * camera.getInverse();

	// Render light with back face culling enabled, and store front depth.
	lightFrontShader.bind();
	lightFrontShader.setUniform("mvMatrix", mv);
	lightFrontShader.setUniform("pMatrix", camera.getProjection());
	lightFrontShader.setUniform("size", Math::ivec2(width, height));
	lightFrontShader.setUniform("id", intersectLightIDs[index]);
	lightFrontShader.setUniform("TmpLightDepths", &tmpLightDepths);

	glCullFace(GL_BACK);
	vaoLight.render();
	lightFrontShader.unbind();

	// FIXME: Manually setting front depth to zero if it hasn't been written yet, not sure if that's good or bad.
	//glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Re-render light with front face culling enabled, store back depth and add to link list.
	lightBackShader.bind();
	setLightListUniforms(&lightBackShader);
	lightBackShader.setUniform("mvMatrix", mv);
	lightBackShader.setUniform("pMatrix", camera.getProjection());
	lightBackShader.setUniform("size", Math::ivec2(width, height));
	lightBackShader.setUniform("id", intersectLightIDs[index]);
	lightBackShader.setUniform("TmpLightDepths", &tmpLightDepths);

	glCullFace(GL_FRONT);
	vaoLight.render();
	lightBackShader.unbind();
	
	//glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glPopAttrib();
}

void LightLinkList::setLightListUniforms(Rendering::Shader *shader, bool fragAlloc)
{
	// Just in case the link list lfb is currently being used.
	if (fragAlloc)
		shader->setUniform("lightFragCount", &lightFragCount);
	shader->setUniform("LightHeadPtrs", &lightHeadPtrs);
	shader->setUniform("LightNextPtrs", &lightNextPtrs);
	shader->setUniform("LightData", &lightData);
	shader->setUniform("lightSize", Math::ivec2(width, height));
	if (fragAlloc)
		shader->setUniform("lightFragAlloc", lightFragAlloc);
}

std::string LightLinkList::getLightListStr()
{
	std::stringstream s;

#if 0
// Example output:

// 16-bit precision.
496,100: 26, 3.63867, 3.67383; 
497,100: 26, 3.61523, 3.6875; 
498,100: 26, 3.5918, 3.70117; 
499,100: 26, 3.57031, 3.71484; 
500,100: 26, 3.54688, 3.72852; 
501,100: 26, 3.52539, 3.74219; 
502,100: 26, 3.50391, 3.75586; 
503,100: 26, 3.48242, 3.76953; 
504,100: 26, 3.46094, 3.7832; 
505,100: 26, 3.43945, 3.79688; 
506,100: 26, 3.41992, 3.8125; 
507,100: 26, 3.39844, 3.82617; 
508,100: 26, 3.37891, 3.8418; 
509,100: 26, 3.35938, 3.85547; 
510,100: 26, 3.33789, 3.87109; 
511,100: 26, 3.31836, 3.88477; 

// 32-bit precision.
496,100: 26, 3.63954, 3.67446; 
497,100: 26, 3.61635, 3.68787; 
498,100: 26, 3.59345, 3.70138; 
499,100: 26, 3.57083, 3.71499; 
500,100: 26, 3.5485, 3.7287; 
501,100: 26, 3.52645, 3.74251; 
502,100: 26, 3.50467, 3.75643; 
503,100: 26, 3.48316, 3.77044; 
504,100: 26, 3.46191, 3.78457; 
505,100: 26, 3.44092, 3.7988; 
506,100: 26, 3.42018, 3.81313; 
507,100: 26, 3.39969, 3.82758; 
508,100: 26, 3.37944, 3.84213; 
509,100: 26, 3.35943, 3.8568; 
510,100: 26, 3.33966, 3.87158; 
511,100: 26, 3.32012, 3.88647; 
#endif

	// TODO: test me!
	unsigned int *headData = (unsigned int*) lightHeadPtrs.read();
	unsigned int *nextData = (unsigned int*) lightNextPtrs.read();
	Math::vec2 *fragData = (Math::vec2*) lightData.read();

	int maxFrags = 0;

	s << "\n";

	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			int currMaxFrags = 0;

			int pixel = y * width + x;
			unsigned int node = headData[pixel];

			s << x << "," << y << ": ";

			while (node != 0)
			{
				float fID = fragData[node].x;
				unsigned int id = Utils::floatBitsToUint(fID);
			#if DEPTH_16
				unsigned int boundsInfo = Utils::floatBitsToUint(fragData[node].y);
				unsigned int uFrontDepth = boundsInfo >> 16;
				unsigned int uBackDepth = boundsInfo & 0xFFFFU;
				float frontDepth = Utils::f16ToF32(uFrontDepth);
				float backDepth = Utils::f16ToF32(uBackDepth);

				s << id << ", " << frontDepth << ", " << backDepth << "; ";
			#else
				// TODO: implement me!
				s << id << ", " << fragData[node].y << "; ";
			#endif
				node = nextData[node];
				currMaxFrags++;
			}

			if (currMaxFrags > maxFrags)
				maxFrags = currMaxFrags;

			s << "\n";
		}
	}
	s << "\n\n\n";

	return std::string("Max Light frags: ") + Utils::toString(maxFrags) + s.str();
}

void LightLinkList::captureGeometry(App *app)
{
	app->getProfiler().start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();
	auto &geometryLFB = app->getGeometryLfb();
	auto &gpuMesh = app->getGpuMesh();

	geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (geometryLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags();
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh.render(false);
		geometryLFB.endCountFrags();
	}

	captureGeometryShader.bind();
	captureGeometryShader.setUniform("mvMatrix", camera.getInverse());
	captureGeometryShader.setUniform("pMatrix", camera.getProjection());
	captureGeometryShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	captureGeometryShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureGeometryShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());
	captureGeometryShader.setUniform("LightColors", &app->getLightColorBuffer());

	// Capture fragments into the lfb using light link list to apply lighting.
	geometryLFB.beginCapture(&captureGeometryShader);
	setLightListUniforms(&captureGeometryShader, false);
	gpuMesh.render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (geometryLFB.endCapture())
	{
		geometryLFB.beginCapture(&captureGeometryShader);
		setLightListUniforms(&captureGeometryShader, false);
		gpuMesh.render();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}
	captureGeometryShader.unbind();

	app->getProfiler().time("Geometry Capture");
}

void LightLinkList::sortGeometry(App *app)
{
	app->getProfiler().start("Geometry Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	geometryBMA.createMask(&app->getGeometryLfb());
	geometryBMA.sort(&app->getGeometryLfb());
#else
	// Sort and composite normally.
	sortGeometryShader.bind();
	app->getGeometryLfb().composite(&sortGeometryShader); // TODO: change this to a sort method.
	sortGeometryShader.unbind();
#endif

	app->getProfiler().time("Geometry Sort");

	app->getProfiler().start("Composite");
	app->getProfiler().time("Composite");
}

void LightLinkList::setLightRes()
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Re-allocate storage buffer data if the camera has resized.
	if (width != camera.getWidth() / GRID_CELL_SIZE || height != camera.getHeight() / GRID_CELL_SIZE)
	{
		width = camera.getWidth() / GRID_CELL_SIZE;
		height = camera.getHeight() / GRID_CELL_SIZE;

		allocStorageBuffers();
	}
}



void LightLinkList::init()
{
	createLightVAO();
}

void LightLinkList::render(App *app)
{
	setLightRes();

	// Create the light link list.
	captureLightList(app);

	// Capture geometry, applying lights from light link list.
	captureGeometry(app);

	glPushAttrib(GL_POLYGON_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Sort geometry, compositing during sorting, either with or without BMA.
	sortGeometry(app);

	glPopAttrib();
}

void LightLinkList::update(float dt)
{
	(void) (dt);
}

void LightLinkList::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	auto &camera = Renderer::instance().getActiveCamera();
	width = camera.getWidth() / GRID_CELL_SIZE;
	height = camera.getHeight() / GRID_CELL_SIZE;

	// Lights front faces of lights that intersect the frustum.
	auto &lightFrontVert = ShaderSourceCache::getShader("lightListFrontVert").loadFromFile("shaders/oit/lightLinkList/lightFront.vert");
	auto &lightFrontFrag = ShaderSourceCache::getShader("lightListFrontFrag").loadFromFile("shaders/oit/lightLinkList/lightFront.frag");
	lightFrontFrag.setDefine("DEPTH_16", Utils::toString(DEPTH_16));

	lightFrontShader.release();
	lightFrontShader.create(&lightFrontVert, &lightFrontFrag);

	// Lights back faces of lights that intersect the frustum.
	auto &lightBackVert = ShaderSourceCache::getShader("lightListBackVert").loadFromFile("shaders/oit/lightLinkList/lightBack.vert");
	auto &lightBackFrag = ShaderSourceCache::getShader("lightListBackFrag").loadFromFile("shaders/oit/lightLinkList/lightBack.frag");
	lightBackFrag.setDefine("DEPTH_16", Utils::toString(DEPTH_16));

	lightBackShader.release();
	lightBackShader.create(&lightBackVert, &lightBackFrag);

	// Lights both front and back faces of lights that are fully inside the frustum.
	auto &lightBothVert = ShaderSourceCache::getShader("lightListBothVert").loadFromFile("shaders/oit/lightLinkList/lightBoth.vert");
	auto &lightBothFrag = ShaderSourceCache::getShader("lightListBothFrag").loadFromFile("shaders/oit/lightLinkList/lightBoth.frag");
	auto &lightBothGeom = ShaderSourceCache::getShader("lightListBothGeom").loadFromFile("shaders/oit/lightLinkList/lightBoth.geom");
	lightBothGeom.setDefine("CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	lightBothFrag.setDefine("DEPTH_16", Utils::toString(DEPTH_16));
	lightBothFrag.setDefine("DEPTH_LAYERS", Utils::toString(DEPTH_LAYERS));

	lightBothShader.release();
	lightBothShader.create(&lightBothVert, &lightBothFrag, &lightBothGeom);

	// Shader for capturing geometry, applies lighting during capture.
	auto &geomCapVert = ShaderSourceCache::getShader("lightListCapVert").loadFromFile("shaders/oit/lightLinkList/capture.vert");
	auto &geomCapFrag = ShaderSourceCache::getShader("lightListCapFrag").loadFromFile("shaders/oit/lightLinkList/capture.frag");
	geomCapFrag.setDefine("DEPTH_16", Utils::toString(DEPTH_16));
	geomCapFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));

	captureGeometryShader.release();
	captureGeometryShader.create(&geomCapVert, &geomCapFrag);

	// Shader for sorting geometry, composites locally.
	auto vertSort = ShaderSourceCache::getShader("sortListGridGeoVert").loadFromFile("shaders/sort/sort.vert");
	auto fragSort = ShaderSourceCache::getShader("sortListGridGeoFrag").loadFromFile("shaders/sort/sort.frag");
	fragSort.setDefine("MAX_FRAGS", Utils::toString(app->getGeometryLfb().getMaxFrags()));
	fragSort.setDefine("COMPOSITE_LOCAL", Utils::toString(1));
	fragSort.replace("LFB_NAME", "lfb");

	sortGeometryShader.release();
	sortGeometryShader.create(&vertSort, &fragSort);

#if USE_BMA
	geometryBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
	geometryBMA.createShaders(&app->getGeometryLfb(), "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}});
#endif

	// TODO: buffer this when necessary.
	vaoLight.setMode(GL_POINTS);
	//vboLight.create(VertexFormat({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} }), &insideLightPositions[0], nInside * sizeof(Math::vec4), sizeof(Math::vec4));
	vaoLight.create(VertexFormat({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} }), nullptr, nInside * sizeof(Math::vec4), sizeof(Math::vec4));
	glEnableVertexAttribArray(0);

	allocStorageBuffers();
}

void LightLinkList::saveLFB(App *app)
{
	(void) (app);
#if 0
	// This just here to test 32 to 16 bit floating point packing.
	float t = -13.455425;
	unsigned int id = 6;
	unsigned int u = Utils::f32ToF16(t);
	Utils::printBinary(u);
	unsigned int d = id << 16 | u;
	Utils::printBinary(d);

	unsigned int newID = d >> 16;
	unsigned int newU = d & 0xFFFFU;
	float newF = Utils::f16ToF32(newU);

	std::cout << t << ", " << id << ": " << newF << ", " << newID << "\n";
#endif
	auto s = getLightListStr();
	Utils::File f("lightLinkList.txt");
	f.writeLine(s);

	//app->saveGeometryLFB();
}

#endif

