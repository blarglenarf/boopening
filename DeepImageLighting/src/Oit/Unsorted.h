#pragma once

#if 0

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"
#include "../../../Renderer/src/RenderObject/Texture.h"

class Unsorted : public Oit
{
private:
	Rendering::LFB lightLFB;

	Rendering::Shader captureLightShader;
	Rendering::Shader sortLightShader;
	Rendering::Shader countLightShader;
	Rendering::Shader compositeShader;

	Rendering::Shader captureGeometryShader;

	Rendering::BMA lightBMA;

	Rendering::StorageBuffer passCount;
	Rendering::StorageBuffer failCount;

public:
	Unsorted(int opaqueFlag = 0) : Oit(opaqueFlag) {}
	virtual ~Unsorted() {}

	virtual void init() override;

	void captureLighting(App *app);
	void sortLighting(App *app);

	void sortGeometry(App *app);

	void composite(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

#endif

