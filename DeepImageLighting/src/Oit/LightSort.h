#pragma once

#if 0

#include <vector>

struct LightKey
{
	unsigned int id;
	float z;
};

// This one is slow since it has to copy data to/from the GPU.
void sortLightKeysThrust(std::vector<LightKey> &lightKeys);

// Note: the opengl buffer has to be bound before calling this function.
void createLightKeysCudaResource(unsigned int lightKeysBuffer);
void destroyLightKeysCudaResource();

// Note: the opengl buffer has to be bound before calling this function.
void sortLightKeysThrust(size_t nLightKeys);

#endif

