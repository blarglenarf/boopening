#include "VolumeLocalGrid.h"

#if 0

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>

#define USE_BMA 1
#define MAX_FRAGS 512
#define LIGHT_COUNT_DEBUG 0
#define COUNT_LIGHT_FRAGS 0
#define DRAW_LIGHT 0
#define PACK_LIGHT_DATA 0
#define LIGHT_IMPLICIT 1
#define CLUSTER_CULL 1
#define CLUSTERS 32
#define MASK_SIZE (CLUSTERS / 32)
#define MAX_EYE_Z 30.0

using namespace Rendering;



void VolumeLocalGrid::sortWithBMA(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	auto &intervals = geometryBMA.getIntervals();

	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
		intervals[i].shader->bind();
		app->getGeometryLfb().setUniforms(intervals[i].shader);
		lightLFB.setUniforms(intervals[i].shader);

		intervals[i].shader->setUniform("viewport", camera.getViewport());
		intervals[i].shader->setUniform("mvMatrix", camera.getInverse());
		intervals[i].shader->setUniform("invPMatrix", camera.getProjection().getInverse());
		intervals[i].shader->setUniform("nSphereLights", (unsigned int) app->getNSphereLights());

		intervals[i].shader->setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		intervals[i].shader->setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());

		intervals[i].shader->setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
		intervals[i].shader->setUniform("ConeLightDist", &app->getConeLightDistBuffer());
		intervals[i].shader->setUniform("ConeLightDir", &app->getConeLightDirBuffer());

		intervals[i].shader->setUniform("LightColors", &app->getLightColorBuffer());

	#if COUNT_LIGHT_FRAGS
		intervals[i].shader->setUniform("PassCount", &passCount);
		intervals[i].shader->setUniform("FailCount", &failCount);
	#endif

		Rendering::Renderer::instance().drawQuad();

		intervals[i].shader->unbind();
	}

	glDisable(GL_STENCIL_TEST);
}



void VolumeLocalGrid::init()
{
#if LIGHT_IMPLICIT
	auto lightCountVert = ShaderSourceCache::getShader("countVolumeLocalGridVert").loadFromFile("shaders/volumeLight/countLightRay.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countVolumeLocalGridGeom").loadFromFile("shaders/volumeLight/countLightRay.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countVolumeLocalGridFrag").loadFromFile("shaders/volumeLight/countLightRay.frag");
	lightCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);
#else
	auto lightCountVert = ShaderSourceCache::getShader("countVolumeLocalGridVert").loadFromFile("shaders/volumeLight/countLight.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countVolumeLocalGridGeom").loadFromFile("shaders/volumeLight/countLight.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countVolumeLocalGridFrag").loadFromFile("shaders/volumeLight/countLight.frag");
	lightCountGeom.setDefine("CONSERVATIVE_RASTER", Utils::toString(0));
	lightCountFrag.setDefine("CONSERVATIVE_RASTER", Utils::toString(0));
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);
#endif
}

void VolumeLocalGrid::createClusterMask(App *app)
{
	app->getProfiler().start("Cluster Mask");

	mask.beginCreateMask();

	// Render geometry.
	app->getGpuMesh().render();
	
	mask.endCreateMask();

	app->getProfiler().time("Cluster Mask");
}

void VolumeLocalGrid::captureLighting(App *app)
{
	app->getProfiler().start("Light Capture");

	// TODO: use conservative rasterization.
	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_CULL_FACE);

	auto &camera = Renderer::instance().getActiveCamera();

	// Resize based on grid size.
	app->setTmpViewport(camera.getWidth() / GRID_CELL_SIZE, camera.getHeight() / GRID_CELL_SIZE);

	lightLFB.resize(camera.getWidth(), camera.getHeight());

	if (lightLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		lightLFB.beginCountFrags(&countLightShader);
		countLightShader.setUniform("mvMatrix", camera.getInverse());
		countLightShader.setUniform("pMatrix", camera.getProjection());
		countLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		countLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	#if LIGHT_IMPLICIT
		countLightShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
	#else
		countLightShader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());
	#endif
	#if CLUSTER_CULL
		mask.setUniforms(&countLightShader);
	#endif
		countLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		countLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();
		lightLFB.endCountFrags(&countLightShader);
	}

	captureLightShader.bind();
	captureLightShader.setUniform("mvMatrix", camera.getInverse());
	captureLightShader.setUniform("pMatrix", camera.getProjection());
	captureLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
#if LIGHT_IMPLICIT
	captureLightShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
#else
	captureLightShader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());
#endif
#if CLUSTER_CULL
	mask.setUniforms(&captureLightShader);
#endif

	// Capture fragments into the lfb.
	lightLFB.beginCapture(&captureLightShader);
	captureLightShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (lightLFB.endCapture())
	{
		lightLFB.beginCapture(&captureLightShader);
		captureLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();

		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	captureLightShader.unbind();

	glPopAttrib();

	app->restoreViewport();

	app->getProfiler().time("Light Capture");
}

void VolumeLocalGrid::sortLighting(App *app)
{
	app->getProfiler().start("Light Sort");

	auto &camera = Renderer::instance().getActiveCamera();

	// Resize based on grid size.
	app->setTmpViewport(camera.getWidth() / GRID_CELL_SIZE, camera.getHeight() / GRID_CELL_SIZE);

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	lightBMA.createMask(&lightLFB);
	lightBMA.sort(&lightLFB);
#else
	// Sort and composite normally.
	sortLightShader.bind();
	lightLFB.composite(&sortLightShader); // TODO: change this to a sort method.
	sortLightShader.unbind();
#endif

	app->restoreViewport();

	app->getProfiler().time("Light Sort");
}

void VolumeLocalGrid::sortGeometry(App *app)
{
	app->getProfiler().start("Geometry Sort");

#if COUNT_LIGHT_FRAGS
	unsigned int zero = 0;
	passCount.bufferData(&zero, sizeof(zero));
	failCount.bufferData(&zero, sizeof(zero));
#endif

	// Sort geometry, and composite using light data in global memory.
	glPushAttrib(GL_POLYGON_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Can composite using BMA, or not.
#if !USE_BMA
	auto &camera = Renderer::instance().getActiveCamera();

	sortGeometryShader.bind();
	sortGeometryShader.setUniform("viewport", camera.getViewport());
	sortGeometryShader.setUniform("mvMatrix", camera.getInverse());
	sortGeometryShader.setUniform("invPMatrix", camera.getProjection().getInverse());
	sortGeometryShader.setUniform("nSphereLights", (unsigned int) app->getNSphereLights());
	sortGeometryShader.setUniform("MaterialBlock", &app->getMeshMatUniform());

	sortGeometryShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	sortGeometryShader.setUniform("SphereLightPositions", &app->getSphereLightPosBuffer());

	sortGeometryShader.setUniform("ConeLightPositions", &app->getConeLightPosBuffer());
	sortGeometryShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	sortGeometryShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());

	sortGeometryShader.setUniform("LightColors", &app->getLightColorBuffer());

	#if COUNT_LIGHT_FRAGS
		sortGeometryShader.setUniform("PassCount", &passCount);
		sortGeometryShader.setUniform("FailCount", &failCount);
	#endif

	app->getGeometryLfb().beginComposite(&sortGeometryShader);
	lightLFB.beginComposite(&sortGeometryShader);
	Renderer::instance().drawQuad();
	app->getGeometryLfb().endComposite();
	lightLFB.endComposite();

	sortGeometryShader.unbind();
#else
	geometryBMA.createMask(&app->getGeometryLfb());
	sortWithBMA(app);
#endif
	
	glPopAttrib();

#if COUNT_LIGHT_FRAGS
	unsigned int pass = *((unsigned int*) passCount.read());
	unsigned int fail = *((unsigned int*) failCount.read());

	float total = (float) (pass + fail);

	std::cout << "Total: " << pass + fail << ", Pass: " << pass << ", Fail: " << fail << ", % Pass: "  << ((float) pass / total) * 100.0f << ", % Fail: " << ((float) fail / total) * 100.0f << "\n";
#endif

	app->getProfiler().time("Geometry Sort");

	app->getProfiler().start("Composite");
	app->getProfiler().time("Composite");
}

void VolumeLocalGrid::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Re-allocate cluster data if the camera has resized.
	if (width != camera.getWidth() / GRID_CELL_SIZE || height != camera.getHeight() / GRID_CELL_SIZE)
	{
		width = camera.getWidth() / GRID_CELL_SIZE;
		height = camera.getHeight() / GRID_CELL_SIZE;

		mask.resize(width, height);
	}

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	createClusterMask(app);
#endif

	// Capture lighting first.
	captureLighting(app);
	sortLighting(app);

	app->captureGeometry(nullptr, nullptr, nullptr, true);

	// Composite during sort by reading light data from global memory.
	sortGeometry(app);
}

void VolumeLocalGrid::update(float dt)
{
	(void) (dt);
}

void VolumeLocalGrid::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

#if LIGHT_DRAW || !PACK_LIGHT_DATA
	int fragSize = 2;
#else
	int fragSize = 1;
#endif

	auto &camera = Renderer::instance().getActiveCamera();
	width = camera.getWidth() / GRID_CELL_SIZE;
	height = camera.getHeight() / GRID_CELL_SIZE;

	// Always use linearized for the lights.
	//lightLFB.setType(type, "light", fragSize);
	lightLFB.setType(Rendering::LFB::LINEARIZED, "light", fragSize, false);
	lightLFB.setMaxFrags(MAX_FRAGS);

#if LIGHT_IMPLICIT
	auto vertLightCap = ShaderSourceCache::getShader("capVolumeLocalGridVert").loadFromFile("shaders/volumeLight/captureLightRay.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capVolumeLocalGridGeom").loadFromFile("shaders/volumeLight/captureLightRay.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capVolumeLocalGridFrag").loadFromFile("shaders/volumeLight/captureLightRay.frag");
	fragLightCap.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));
	fragLightCap.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragLightCap.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	fragLightCap.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragLightCap.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);
#else
	auto vertLightCap = ShaderSourceCache::getShader("capVolumeLocalGridVert").loadFromFile("shaders/volumeLight/captureLight.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capVolumeLocalGridGeom").loadFromFile("shaders/volumeLight/captureLight.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capVolumeLocalGridFrag").loadFromFile("shaders/volumeLight/captureLight.frag");
	fragLightCap.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);
#endif

#if !USE_BMA
	auto vertLightSort = ShaderSourceCache::getShader("sortVolumeLocalGridVert").loadFromFile("shaders/sort/sort.vert");
	auto fragLightSort = ShaderSourceCache::getShader("sortVolumeLocalGridFrag").loadFromFile("shaders/sort/sortL.frag");
	fragLightSort.setDefine("MAX_FRAGS", Utils::toString(lightLFB.getMaxFrags()));
	fragLightSort.setDefine("FRAG_SIZE", Utils::toString(fragSize));
	//fragLightSort.replace("LFB_NAME", "light");
	fragLightSort.replace("LFB_NAME", "light_L");

	sortLightShader.release();
	sortLightShader.create(&vertLightSort, &fragLightSort);

	auto vertComp = ShaderSourceCache::getShader("compVolumeLocalGridVert").loadFromFile("shaders/oit/volumeLocalGrid/sort.vert");
	auto fragComp = ShaderSourceCache::getShader("compVolumeLocalGridFrag").loadFromFile("shaders/oit/volumeLocalGrid/sort.frag");
	fragComp.setDefine("MAX_FRAGS", Utils::toString(app->getGeometryLfb().getMaxFrags()));
	fragComp.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	fragComp.setDefine("LIGHT_COUNT_DEBUG", Utils::toString(LIGHT_COUNT_DEBUG));
	fragComp.setDefine("COUNT_LIGHT_FRAGS", Utils::toString(COUNT_LIGHT_FRAGS));
	fragComp.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));
	fragComp.replace("LFB_NAME", "lfb");

	sortGeometryShader.release();
	sortGeometryShader.create(&vertComp, &fragComp);
#endif

#if USE_BMA
	// Init BMA mask shaders.
	lightBMA.createMaskShader("light", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMaskL.frag", {{"FRAG_SIZE", Utils::toString(fragSize)}});
	lightBMA.createShaders(&lightLFB, "shaders/sort/sort.vert", "shaders/sort/sortL.frag", {{"FRAG_SIZE", Utils::toString(fragSize)}});

	geometryBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
	geometryBMA.createShaders(&app->getGeometryLfb(), "shaders/oit/volumeLocalGrid/sort.vert", "shaders/oit/volumeLocalGrid/sort.frag",
		{{"GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE)}, {"LIGHT_COUNT_DEBUG", Utils::toString(LIGHT_COUNT_DEBUG)},
		{"COUNT_LIGHT_FRAGS", Utils::toString(COUNT_LIGHT_FRAGS)}, {"PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA)}});
#endif

#if CLUSTER_CULL
	mask.regen(CLUSTERS, MAX_EYE_Z, GRID_CELL_SIZE);
	mask.resize(width, height);
#endif

#if COUNT_LIGHT_FRAGS
	passCount.release();
	passCount.create(nullptr, sizeof(unsigned int));

	failCount.release();
	failCount.create(nullptr, sizeof(unsigned int));
#endif
}

void VolumeLocalGrid::saveLFB(App *app)
{
	auto s = lightLFB.getStrSorted(true);
	Utils::File f("lightVolumeLocalGridLFB.txt");
	f.writeLine(s);

	app->saveGeometryLFB();
}

#endif

