#pragma once

#if 0

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"
#include "../../../Renderer/src/LFB/ClusterMask.h"

class VolumeLocal : public Oit
{
private:
	Rendering::LFB lightLFB;

	Rendering::Shader captureLightShader;
	Rendering::Shader sortLightShader;
	Rendering::Shader countLightShader;
	Rendering::Shader sortGeometryShader;

	Rendering::BMA lightBMA;
	Rendering::BMA geometryBMA;

	Rendering::StorageBuffer passCount;
	Rendering::StorageBuffer failCount;

	Rendering::ClusterMask mask;

private:
	void sortWithBMA(App *app);

public:
	VolumeLocal() : Oit() {}
	virtual ~VolumeLocal() {}

	virtual void init() override;

	void createClusterMask(App *app);

	void captureLighting(App *app);
	void sortLighting(App *app);

	void sortGeometry(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

#endif

