#include "LightSort.h"

#if 0

#include "../../../Renderer/src/GL.h"

#include <cuda_runtime.h>
#include <cuda.h>
#include <cudaGL.h>
#include <cuda_gl_interop.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/sort.h>

struct LightKeyComp
{
	__host__ __device__
	bool operator()(const LightKey &a, const LightKey &b)
	{
		return b.z > a.z;
	}
};

cudaGraphicsResource *resource = NULL;
LightKey *devPtr = NULL;
size_t size = 0;

void sortLightKeysThrust(std::vector<LightKey> &lightKeys)
{
	// Copy the lightKeys to the GPU.
	thrust::device_vector<LightKey> deviceKeys(lightKeys.size());
	thrust::copy(lightKeys.begin(), lightKeys.end(), deviceKeys.begin());

	thrust::sort(deviceKeys.begin(), deviceKeys.end(), LightKeyComp());

	// Get the lightKeys back from the GPU.
	thrust::copy(deviceKeys.begin(), deviceKeys.end(), lightKeys.begin());
}

void createLightKeysCudaResource(unsigned int lightKeysBuffer)
{
	cudaGraphicsGLRegisterBuffer(&resource, lightKeysBuffer, cudaGraphicsMapFlagsNone);
}

void destroyLightKeysCudaResource()
{
	if (resource != NULL)
		cudaGraphicsUnregisterResource(resource);
	resource = NULL;
}

void sortLightKeysThrust(size_t nLightKeys)
{
	// FIXME: Probably shouldn't do this here...
	//cudaGLSetGLDevice(0);
	cudaGraphicsMapResources(1, &resource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**) &devPtr, &size, resource);
	if (devPtr != NULL)
	{
		thrust::device_ptr<LightKey> deviceKeys = thrust::device_pointer_cast(devPtr);
		thrust::sort(deviceKeys, deviceKeys + nLightKeys, LightKeyComp());
	}
	cudaGraphicsUnmapResources(1, &resource, NULL);
}

#endif

