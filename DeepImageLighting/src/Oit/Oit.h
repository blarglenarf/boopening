#pragma once

#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/LFB/LFB.h"

class App;

class Oit
{
public:
	enum LightType
	{
		BRUTE_FORCE,
		FORWARD_UNSORTED,
		FORWARD_UNSORTED_GRID,
		DEFERRED_VOLUME,
		DEFERRED_VOLUME_GRID,
		DEFERRED_VOLUME_LOCAL,
		DEFERRED_VOLUME_LOCAL_GRID,
		VOLUME_CLUSTER_LL,
		VOLUME_CLUSTER_L,
		LIGHT_LINK_LIST,
		LIGHT_LINK_LIST_SEP,
		PRACTICAL_CLUSTER_FORWARD,
		PRACTICAL_CLUSTER_DEFERRED,
		RAY_CLUSTER_FORWARD,
		RAY_CLUSTER_DEFERRED,
		RAY_CLUSTER_LLI_FORWARD,
		RAY_CLUSTER_LLI_DEFERRED,
		RAY_CLUSTER_LAI_FORWARD,
		RAY_CLUSTER_LAI_DEFERRED,
		RAY_CLUSTER_LLF_FORWARD,
		RAY_CLUSTER_LLF_DEFERRED,
		RAY_CLUSTER_LAF_FORWARD,
		RAY_CLUSTER_LAF_DEFERRED,
		FORWARD_PLUS_FORWARD,
		FORWARD_PLUS_DEFERRED,
		TILED_CLUSTER_FORWARD,
		TILED_CLUSTER_DEFERRED,
		LIGHT_LINK_LIST_FORWARD,
		LIGHT_LINK_LIST_DEFERRED,
		DEFERRED,
		VISUALISER
	};

protected:
	int opaqueFlag;

public:
	Oit(int opaqueFlag = 0) : opaqueFlag(opaqueFlag) {}
	virtual ~Oit() {}

	virtual void init() = 0;

	virtual void render(App *app) = 0;
	virtual void update(float dt) = 0;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) = 0;
	virtual void reloadShaders(App *app) { (void) (app); }

	virtual void saveLFB(App *app) = 0;
};

