#pragma once

#if 0

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"

class UnsortedGrid : public Oit
{
private:
	Rendering::LFB lightLFB;

	Rendering::Shader captureLightShader;
	Rendering::Shader sortLightShader;
	Rendering::Shader countLightShader;
	Rendering::Shader compositeShader;

	Rendering::Shader captureGeometryShader;

	Rendering::BMA lightBMA;

	Rendering::StorageBuffer passCount;
	Rendering::StorageBuffer failCount;

	int width;
	int height;

public:
	UnsortedGrid(int opaqueFlag = 0) : Oit(opaqueFlag), width(0), height(0) {}
	virtual ~UnsortedGrid() {}

	virtual void init() override;

	void captureLighting(App *app);
	void sortLighting(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

#endif

