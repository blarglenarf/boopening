#include "PracticalCluster.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"
#include "../../../Utils/src/File.h"

#include <string>

//
// Implementation of Clustered shading, using a separate texture for each light.
//

// Cluster space is determined by near and far planes of the frustum <-- I think, it could also be determined by nearest and farthest fragment.


#define USE_BMA 1
#define MAX_FRAGS 512
#define LIST_COMPUTE 0 // FIXME: Needs updating.
#define LIGHT_DRAW 0
#define LIGHT_8 1
#define PROFILE_MEMORY 1
#define FULL_CLEAR 0
#define CLUSTER_CULL 1
#define CONSERVATIVE_RASTER 0
#define LINEARISED 0

using namespace Rendering;


void PracticalCluster::allocStorageBuffers(App *app)
{
	(void) (app);

	// 2d texture arrays to store temporary depths and frambuffer objects to render to them.
	for (auto *b : lightDepthBuffers)
		delete b;
	for (auto *t : lightDepthTextures)
		delete t;

	if (app->getNLights() < 2048)
	{
		lightDepthBuffers.resize(1);
		lightDepthTextures.resize(1);
	}
	else
	{
		lightDepthBuffers.resize(app->getNLights() / 2048);
		lightDepthTextures.resize(app->getNLights() / 2048);
	}

	for (size_t i = 0; i < lightDepthBuffers.size(); i++)
	{
		lightDepthTextures[i] = new Texture2DArray();
		lightDepthBuffers[i] = new FrameBuffer();

		lightDepthBuffers[i]->setSize(width, height, app->getNLights() > 2048 ? 2048 : app->getNLights());
	#if LIGHT_8
		lightDepthBuffers[i]->create(GL_RG8, lightDepthTextures[i]);
	#else
		lightDepthBuffers[i]->create(GL_RG32F, lightDepthTextures[i]);
	#endif
	}

	// Clear the light textures.
	for (auto *b : lightDepthBuffers)
	{
		b->bind();
		glClear(GL_COLOR_BUFFER_BIT);
		b->unbind();
	}

	if (app->getNLights() < 2048)
		textureUniforms.resize(1);
	else
		textureUniforms.resize(app->getNLights() / 2048);
	for (size_t i = 0; i < textureUniforms.size(); i++)
		textureUniforms[i] = (int) i;

	// Realloc cluster memory.
	cluster.resize(width, height);

#if PROFILE_MEMORY
	auto &profiler = app->getProfiler();
	profiler.setBufferSize("lightDepths", app->getNLights() * 2 * width * height); // 8 bit version only.
#endif
}

void PracticalCluster::drawLights(App *app, bool clearFlag)
{
	// Draw lights, each into its own 2d texture.
	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

#if CONSERVATIVE_RASTER
	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	if (!clearFlag)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
	#if CONSERVATIVE_RASTER
		glBlendEquation(GL_MAX);
	#else
		glBlendEquation(GL_FUNC_ADD);
	#endif
	}

	auto &camera = Renderer::instance().getActiveCamera();

	// Resize based on grid size.
#if CONSERVATIVE_RASTER
	Math::ivec2 tileSize(ceil((float) camera.getWidth() / (float) width), ceil((float) camera.getHeight() / (float) height));
	Math::ivec4 viewport = camera.getViewport();
	Math::mat4 invPmatrix = camera.getInverseProj();
#endif
	app->setTmpViewport(width, height);

	auto &shader = clearFlag ? clearLightShader : captureLightShader;

	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	shader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
#if CONSERVATIVE_RASTER
	shader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());
	shader.setUniform("tileSize", tileSize);
	shader.setUniform("viewport", viewport);
	shader.setUniform("invPMatrix", invPmatrix);
#else
	shader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
#endif

	size_t coneOffset = 0;

	for (size_t i = 0; i < lightDepthBuffers.size(); i++)
	{
		// Render in groups of 2048 lights.
		lightDepthBuffers[i]->bind();

		if (app->getNLights() > 2048)
		{
			// Based on current i value, render sphere lights first, then cone lights.
			size_t curr = (i + 1) * 2048;
			if (curr <= app->getNSphereLights())
			{
				shader.setUniform("indexOffset", (unsigned int) 0);
				app->getVaoSphereLights().renderRange((int) i * 2048, 2048);
			}
			else
			{
				if (coneOffset == 0)
					coneOffset = i;
				int sphereCount = (int) app->getNSphereLights() - ((int) i * 2048);

				// Need to render more spheres or not?
				if (sphereCount > 0)
				{
					shader.setUniform("indexOffset", (unsigned int) 0);
					app->getVaoSphereLights().renderRange((int) i * 2048, sphereCount);
					shader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
					app->getVaoConeLights().renderRange((int) (i - coneOffset) * 2048, 2048 - sphereCount);
				}
				else
				{
					shader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
					app->getVaoConeLights().renderRange((int) (i - coneOffset) * 2048, 2048);
				}
			}
		}
		else
		{
			shader.setUniform("indexOffset", (unsigned int) 0);
			app->getVaoSphereLights().render();
			shader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
			app->getVaoConeLights().render();
		}

		lightDepthBuffers[i]->unbind();
	}

	shader.unbind();

	// Restore original size.
	app->restoreViewport();

#if CONSERVATIVE_RASTER
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	glPopAttrib();
}

void PracticalCluster::countLightList(App *app)
{
	(void) (app);

#if LIST_COMPUTE
	glEnable(GL_RASTERIZER_DISCARD);
#endif

	for (size_t i = 0; i < lightDepthTextures.size(); i++)
	{
		lightDepthTextures[i]->bind();
		textureUniforms[i] = lightDepthTextures[i]->getActiveNumber();
	}

	cluster.beginCountFrags();

	#if LIST_COMPUTE
		countListComputeShader.bind();
		cluster.setCountUniforms(&countListComputeShader);
		countListComputeShader.setUniform("size", Math::ivec2(width, height));
		auto loc = countListComputeShader.getUniform("lightDepths");
		#if CLUSTER_CULL
			app->getDepthMask().setUniforms(&countListComputeShader);
		#endif
		//createListComputeShader.setUniform("lightDepths", somethingGoesHere);
		glUniform1iv(loc, textureUniforms.size(), &textureUniforms[0]);
	#else
		countListShader.bind();
		cluster.setCountUniforms(&countListShader);
		countListShader.setUniform("size", Math::ivec2(width, height));
		auto loc = countListShader.getUniform("lightDepths");
		#if CLUSTER_CULL
			app->getDepthMask().setUniforms(&countListShader);
		#endif
		//countListShader.setUniform("lightDepths", somethingGoesHere);
		glUniform1iv(loc, (GLsizei) textureUniforms.size(), &textureUniforms[0]);
	#endif

	// Either just one per pixel, or one per pixel per light.
	#if LIST_COMPUTE
		glDrawArrays(GL_POINTS, 0, width * height * app->getNLights());
		countListComputeShader.unbind();
	#else
		Renderer::instance().drawQuad();
		countListShader.unbind();
	#endif

	cluster.endCountFrags();

	for (auto *t : lightDepthTextures)
		t->unbind();

#if LIST_COMPUTE
	glDisable(GL_RASTERIZER_DISCARD);
#endif
}

void PracticalCluster::createLightList(App *app)
{
	(void) (app);

#if LIST_COMPUTE
	glEnable(GL_RASTERIZER_DISCARD);
#endif

	for (size_t i = 0; i < lightDepthTextures.size(); i++)
	{
		lightDepthTextures[i]->bind();
		textureUniforms[i] = lightDepthTextures[i]->getActiveNumber();
	}

	cluster.beginComposite();

	#if LIST_COMPUTE
		createListComputeShader.bind();
		cluster.setUniforms(&createListComputeShader);
		auto loc = createListComputeShader.getUniform("lightDepths");
		#if CLUSTER_CULL
			app->getDepthMask().setUniforms(&createListComputeShader);
		#endif
		//createListComputeShader.setUniform("lightDepths", somethingGoesHere);
		glUniform1iv(loc, textureUniforms.size(), &textureUniforms[0]);
	#else
		createListShader.bind();
		cluster.setUniforms(&createListShader);
		auto loc = createListShader.getUniform("lightDepths");
		#if CLUSTER_CULL
			app->getDepthMask().setUniforms(&createListShader);
		#endif
		//createListShader.setUniform("lightDepths", somethingGoesHere);
		glUniform1iv(loc, (GLsizei) textureUniforms.size(), &textureUniforms[0]);
	#endif

	// Either just one per pixel, or one per pixel per light.
	#if LIST_COMPUTE
		glDrawArrays(GL_POINTS, 0, width * height * app->getNLights());
		createListComputeShader.unbind();
	#else
		Renderer::instance().drawQuad();
		createListShader.unbind();
	#endif

	cluster.endComposite();

	for (auto *t : lightDepthTextures)
		t->unbind();

#if LIST_COMPUTE
	glDisable(GL_RASTERIZER_DISCARD);
#endif
}

void PracticalCluster::clearLightDepths(App *app)
{
#if FULL_CLEAR
	// Either clear the entire buffer, which is slow.
	(void) (app);

	for (auto *b : lightDepthBuffers)
	{
		b->bind();
		glClear(GL_COLOR_BUFFER_BIT);
		b->unbind();
	}
#else
	// Or re-render the lights, setting the values to zero, which is fast.
	drawLights(app, true);
#endif
}

void PracticalCluster::captureLightList(App *app)
{
	app->getProfiler().start("Light Draw");
	drawLights(app);
	app->getProfiler().time("Light Draw");

#if LIGHT_DRAW
	return;
#endif

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	// Instead of one thread per tile, it can be one thread per tile per light, where each thread only looks up data for its own light id and adds to those clusters.
	// In practice that approach seems to be slower than one thread per tile however.
	// TODO: Could possibly do one warp per tile instead of one thread per tile?
	app->getProfiler().start("Light Capture");

	if (cluster.getType() == Cluster::LINEARIZED)
		countLightList(app);

	cluster.beginCapture();
	createLightList(app);
	if (cluster.endCapture())
	{
		cluster.beginCapture();
		createLightList(app);
		if (cluster.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	app->getProfiler().time("Light Capture");

	// Restore original size.
	app->restoreViewport();

	// Instead of completely clearing these, redraw after constructing the list, zero-ing everything.
	app->getProfiler().start("Light Depth Clear");
	clearLightDepths(app);
	app->getProfiler().time("Light Depth Clear");
}

void PracticalCluster::setLightRes(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	if (gBufferFlag)
		gBuffer.resize(camera.getWidth(), camera.getHeight());

	// Re-allocate storage buffer data if the camera has resized.
	if (width != ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE) || height != ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE))
	{
		width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
		height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

		allocStorageBuffers(app);
	#if CLUSTER_CULL
		app->getDepthMask().resize(width, height);
	#endif
	}
}

void PracticalCluster::drawLightClusters(App *app)
{
	debugLightShader.bind();
	lightDepthTextures[0]->bind();
	debugLightShader.setUniform("lightDepths", lightDepthTextures[0]);
	Renderer::instance().drawQuad();
	lightDepthTextures[0]->unbind();
	debugLightShader.unbind();

	clearLightDepths(app);
}



PracticalCluster::~PracticalCluster()
{
	for (auto *b : lightDepthBuffers)
		delete b;
	for (auto *t : lightDepthTextures)
		delete t;
}

void PracticalCluster::init()
{
#if 0
	auto clusterCountVert = ShaderSourceCache::getShader("countClusterVert").loadFromFile("shaders/volumeLight/countLightRay.vert");
	auto clusterCountGeom = ShaderSourceCache::getShader("countClusterGeom").loadFromFile("shaders/volumeLight/countLightRay.geom");
	auto clusterCountFrag = ShaderSourceCache::getShader("countClusterFrag").loadFromFile("shaders/volumeLight/countLightRay.frag");
	clusterCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	clusterCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	clusterCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	clusterCountFrag.setDefine("USE_CLUSTER", "1");
	clusterCountFrag.setDefine("ATOMIC_ADD", "1");
	countClusterShader.create(&clusterCountVert, &clusterCountFrag, &clusterCountGeom);
#endif
}

void PracticalCluster::render(App *app)
{
	if (gBufferFlag && opaqueFlag != 1)
		return;

	setLightRes(app);

	if (gBufferFlag)
	{
		gBuffer.beginCapture();
		app->captureGeometry(nullptr, nullptr, &gBuffer.getCaptureShader(), false);
		gBuffer.endCapture();
	}

#if CLUSTER_CULL
	if (gBufferFlag)
		app->createDepthMask(gBuffer.getDepthTexture());
	else
		app->createDepthMask();
#endif

	captureLightList(app);

#if LIGHT_DRAW
	drawLightClusters(app);
#else

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	// Capture geometry, applying lights from clusters.
	if (gBufferFlag)
	{
		app->getProfiler().start("Light Deferred");

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		captureGeometryShader.bind();
		gBuffer.bindTextures();
		gBuffer.setUniforms(&captureGeometryShader);
		app->setCameraUniforms(&captureGeometryShader);
		app->setLightUniforms(&captureGeometryShader);
		#if CLUSTER_CULL
			app->getDepthMask().setUniforms(&captureGeometryShader);
		#endif
		cluster.setUniforms(&captureGeometryShader);

		Renderer::instance().drawQuad();

		gBuffer.unbindTextures();
		captureGeometryShader.unbind();

		app->getProfiler().time("Light Deferred");
	}
	else
		app->captureGeometry(nullptr, &cluster, &captureGeometryShader);

	if (opaqueFlag == 1)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Sort geometry, compositing during sorting, either with or without BMA.
	app->sortGeometry();

	glPopAttrib();
#endif
}

void PracticalCluster::update(float dt)
{
	(void) (dt);
}

void PracticalCluster::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	auto &camera = Renderer::instance().getActiveCamera();
	width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

#if LINEARISED
	cluster.setType(Cluster::LINEARIZED, "cluster", CLUSTERS, false, Cluster::UINT, true);
#else
	cluster.setType(Cluster::LINK_LIST, "cluster", CLUSTERS, false, Cluster::UINT, true);
#endif

	cluster.setProfiler(&app->getProfiler());
	if (gBufferFlag)
		gBuffer.setProfiler(&app->getProfiler());

	// Shader for capturing lights to the 2d texture array.
	auto &lightCapVert = ShaderSourceCache::getShader("clusterCapLightVert").loadFromFile("shaders/oit/cluster/captureLight.vert");
	auto &lightCapGeom = ShaderSourceCache::getShader("clusterCapLightGeom").loadFromFile("shaders/oit/cluster/captureLight.geom");
	auto &lightCapFrag = ShaderSourceCache::getShader("clusterCapLightFrag").loadFromFile("shaders/oit/cluster/captureLight.frag");
	lightCapVert.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightCapGeom.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightCapFrag.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightCapFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCapFrag.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
	lightCapFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	captureLightShader.release();
	captureLightShader.create(&lightCapVert, &lightCapFrag, &lightCapGeom);

	// Shader for clearing the lights, because clearing the color buffer bit is very slow.
	auto &lightClearVert = ShaderSourceCache::getShader("clusterClearLightVert").loadFromFile("shaders/oit/cluster/captureLight.vert");
	auto &lightClearGeom = ShaderSourceCache::getShader("clusterClearLightGeom").loadFromFile("shaders/oit/cluster/captureLight.geom");
	auto &lightClearFrag = ShaderSourceCache::getShader("clusterClearLightFrag").loadFromFile("shaders/oit/cluster/clearLight.frag");
	lightClearVert.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightClearGeom.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightClearGeom.setDefine("CLEAR_DEPTH", "1");
	lightClearFrag.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
	lightClearFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	clearLightShader.release();
	clearLightShader.create(&lightClearVert, &lightClearFrag, &lightClearGeom);

#if !LIST_COMPUTE
	// Shader for creating the light link list.
	auto &lightCreateVert = ShaderSourceCache::getShader("clusterCreateVert").loadFromFile("shaders/oit/cluster/createList.vert");
	auto &lightCreateFrag = ShaderSourceCache::getShader("clusterCreateFrag").loadFromFile("shaders/oit/cluster/createList.frag");
	lightCreateFrag.setDefine("MAX_LIGHTS", Utils::toString(app->getNLights()));
	lightCreateFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	lightCreateFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCreateFrag.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
	lightCreateFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCreateFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCreateFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCreateFrag.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));

	createListShader.release();
	createListShader.create(&lightCreateVert, &lightCreateFrag);

	// Shader for counting the light link list.
	auto &lightCountVert = ShaderSourceCache::getShader("clusterCountVert").loadFromFile("shaders/oit/cluster/createList.vert");
	auto &lightCountFrag = ShaderSourceCache::getShader("clusterCountFrag").loadFromFile("shaders/oit/cluster/countList.frag");
	lightCountFrag.setDefine("MAX_LIGHTS", Utils::toString(app->getNLights()));
	lightCountFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	lightCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCountFrag.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
	lightCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCountFrag.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));

	countListShader.release();
	countListShader.create(&lightCountVert, &lightCountFrag);
#else
	// Another shader for creating the light link list, one thread per grid cell per cluster per light.
	auto &lightCreateCompute = ShaderSourceCache::getShader("clusterCreateCompute").loadFromFile("shaders/oit/cluster/createListCompute.vert");
	lightCreateCompute.setDefine("MAX_LIGHTS", Utils::toString(app->getNLights()));
	lightCreateCompute.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	lightCreateCompute.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCreateCompute.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCreateCompute.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCreateCompute.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	createListComputeShader.release();
	createListComputeShader.create(&lightCreateCompute);
#endif

	// Shader for capturing geometry, applies lighting during capture.
	auto &geomCapVert = ShaderSourceCache::getShader("clusterCapVert").loadFromFile("shaders/oit/cluster/capture.vert");
	auto &geomCapFrag = ShaderSourceCache::getShader("clusterCapFrag").loadFromFile("shaders/oit/cluster/capture.frag");
	geomCapFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	geomCapFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	geomCapFrag.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	geomCapFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	if (gBufferFlag)
	{
		geomCapVert.setDefine("USE_G_BUFFER", "1");
		geomCapFrag.setDefine("USE_G_BUFFER", "1");
	}

	captureGeometryShader.release();
	captureGeometryShader.create(&geomCapVert, &geomCapFrag);

	if (opaqueFlag == 0)
	{
	#if LIGHT_DRAW
		// Shader for drawing the lights, for debugging pruposes.
		auto vertDebug = ShaderSourceCache::getShader("clusterDebugVert").loadFromFile("shaders/oit/cluster/drawLight.vert");
		auto fragDebug = ShaderSourceCache::getShader("clusterDebugFrag").loadFromFile("shaders/oit/cluster/drawLight.frag");
		fragDebug.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
		fragDebug.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
		fragDebug.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
		fragDebug.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

		debugLightShader.release();
		debugLightShader.create(&vertDebug, &fragDebug);
	#endif
	}

	cluster.resize(width, height);
	allocStorageBuffers(app);
}

void PracticalCluster::saveLFB(App *app)
{
	(void) (app);

	auto s = cluster.getStrSorted();
	Utils::File f("practicalCluster.txt");
	f.write(s);
}

