#pragma once

#include "Oit.h"

#include "../../../Renderer/src/Lighting/GBuffer.h"

class Deferred : public Oit
{
private:
	Rendering::Shader lightOpaqueShader;

	Rendering::Shader captureLFBShader;
	Rendering::Shader lightLFBShader;

	Rendering::GBuffer gBuffer;

private:
	void renderStandard(App *app);

public:
	Deferred(int opaqueFlag = 0) : Oit(opaqueFlag) {}
	virtual ~Deferred() {}

	virtual void init() override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

