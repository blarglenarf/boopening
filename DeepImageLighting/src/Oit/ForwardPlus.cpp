#include "ForwardPlus.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>

#define MAX_FRAGS 512
#define LIGHT_COUNT_DEBUG 0
#define GATHER 1 // FIXME: Scattering needs updating.
#define CLUSTER_CULL 1
#define THREADS_PER_TILE 32
#define TEST_FRUSTUMS 0
#define LINEARISED 0

using namespace Rendering;
using namespace Math;


static vec4 getEyeFromWindow(vec3 p, vec4 viewport, mat4 invPMatrix)
{
	vec3 ndcPos;
	ndcPos.xy = ((p.xy - viewport.xy) / viewport.zw) * 2.0f - 1.0f;
	//ndcPos.xy = ((p.xy * 2.0f) - (viewport.xy * 2.0f)) / (viewport.zw) - 1.0f;
	ndcPos.z = 1.0f;
	//std::cout << "ndc: " << ndcPos << "\n";

	vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0f);
	eyeDir /= eyeDir.w;
	vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0f);
	return eyePos;
}

#if 0
	static vec4 clipToEye(vec4 clip, mat4 invPMatrix)
	{
		vec4 view = invPMatrix * clip;
		view = view / view.w;
		return view;
	}

	static vec4 screenToEye(vec4 screen, vec4 viewport, mat4 invPMatrix)
	{
		vec2 texCoord = screen.xy / viewport.zw;
		vec4 clip = vec4(vec2(texCoord.x, 1.0f - texCoord.y) * 2.0f - 1.0f, screen.z, screen.w);
		return clipToEye(clip, invPMatrix);
	}
#endif

static vec4 computePlane(vec3 p0, vec3 p1, vec3 p2)
{
#if 1
	vec3 v0 = p1 - p0;
	vec3 v2 = p2 - p0;
#else
	vec3 v0 = p0 - p1;
	vec3 v2 = p0 - p2;
#endif
	vec3 n = cross(v0, v2);
	float l = n.len();

	vec4 plane(n.x, n.y, n.z, 1.0f);
	plane /= l;
	plane.w = p1.dot(plane.xyz);
	//plane.w = 0.0;

	return plane;
}

#if TEST_FRUSTUMS
	// Function to test light intersections with the frustums, just to make sure they're correct.
	static bool isInFrustum(vec4 plane, vec3 pos, float radius)
	{
		float dist = pos.dot(plane.xyz) + plane.w;

		// If distance is < -radius, then we are outside, otherwise we either intersect or are inside.
		if (dist < -radius)
			return false;
		return true;
	}
#endif


void ForwardPlus::countLists(App *app)
{
	lightLFB.beginCountFrags();
	countLightShader.bind();
	app->setLightUniforms(&countLightShader);
	app->setCameraUniforms(&countLightShader);
	countLightShader.setUniform("FrustumPlanes", &frustumPlanesBuffer);
	countLightShader.setUniform("size", Math::ivec2(width, height));
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&countLightShader);
#endif
	lightLFB.setCountUniforms(&countLightShader);
#if GATHER
	// If gather is used, 32 threads per tile.
	glDrawArrays(GL_POINTS, 0, width * height * THREADS_PER_TILE);
#else
	// If scatter is used, 1 thread per light.
	glDrawArrays(GL_POINTS, 0, app->getNSphereLights() + app->getNConeLights());
#endif
	countLightShader.unbind();
	lightLFB.endCountFrags();
}

void ForwardPlus::createLists(App *app)
{
	captureLightShader.bind();
	lightLFB.setUniforms(&captureLightShader);
	app->setLightUniforms(&captureLightShader);
	app->setCameraUniforms(&captureLightShader);
	captureLightShader.setUniform("FrustumPlanes", &frustumPlanesBuffer);
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&captureLightShader);
#endif

#if GATHER
	// If gather is used, 32 threads per tile.
	glDrawArrays(GL_POINTS, 0, width * height * THREADS_PER_TILE);
#else
	// If scatter is used, 1 thread per light.
	glDrawArrays(GL_POINTS, 0, app->getNSphereLights() + app->getNConeLights());
#endif

	captureLightShader.unbind();
}


void ForwardPlus::init()
{
#if GATHER
	auto lightCount = ShaderSourceCache::getShader("countForwardPlus").loadFromFile("shaders/oit/forwardPlus/countGather.vert");
#else
	auto lightCount = ShaderSourceCache::getShader("countForwardPlus").loadFromFile("shaders/oit/forwardPlus/countScatter.vert");
#endif
	lightCount.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCount.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCount.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCount.setDefine("THREADS_PER_TILE", Utils::toString(THREADS_PER_TILE));
	countLightShader.create(&lightCount);
}

void ForwardPlus::resizeFrustumBuffer()
{
	int nFrustums = width * height;
	int nPlanes = nFrustums * 4;

	frustumPlanes.resize(nPlanes);

	frustumPlanesBuffer.release();
	frustumPlanesBuffer.create(&frustumPlanes[0], frustumPlanes.size() * sizeof(vec4));
}

#if 0
// Convert clip space coordinates to view space
float4 ClipToView( float4 clip )
{
    // View space position.
    float4 view = mul( InverseProjection, clip );
    // Perspecitive projection.
    view = view / view.w;

    return view;
}

// Convert screen space coordinates to view space.
float4 ScreenToView( float4 screen )
{
    // Convert to normalized texture coordinates
    float2 texCoord = screen.xy / ScreenDimensions;

    // Convert to clip space
    float4 clip = float4( float2( texCoord.x, 1.0f - texCoord.y ) * 2.0f - 1.0f, screen.z, screen.w );

    return ClipToView( clip );
}

// Compute a plane from 3 noncollinear points that form a triangle.
// This equation assumes a right-handed (counter-clockwise winding order) 
// coordinate system to determine the direction of the plane normal.
Plane ComputePlane( float3 p0, float3 p1, float3 p2 )
{
    Plane plane;

    float3 v0 = p1 - p0;
    float3 v2 = p2 - p0;

    plane.N = normalize( cross( v0, v2 ) );

    // Compute the distance to the origin using p0.
    plane.d = dot( plane.N, p0 );

    return plane;
}

// A kernel to compute frustums for the grid
// This kernel is executed once per grid cell. Each thread
// computes a frustum for a grid cell.
[numthreads( BLOCK_SIZE, BLOCK_SIZE, 1 )]
void CS_ComputeFrustums( ComputeShaderInput IN )
{
    // View space eye position is always at the origin.
    const float3 eyePos = float3( 0, 0, 0 );

    // Compute 4 points on the far clipping plane to use as the 
    // frustum vertices.
    float4 screenSpace[4];
    // Top left point
    screenSpace[0] = float4( IN.dispatchThreadID.xy * BLOCK_SIZE, -1.0f, 1.0f );
    // Top right point
    screenSpace[1] = float4( float2( IN.dispatchThreadID.x + 1, IN.dispatchThreadID.y ) * BLOCK_SIZE, -1.0f, 1.0f );
    // Bottom left point
    screenSpace[2] = float4( float2( IN.dispatchThreadID.x, IN.dispatchThreadID.y + 1 ) * BLOCK_SIZE, -1.0f, 1.0f );
    // Bottom right point
    screenSpace[3] = float4( float2( IN.dispatchThreadID.x + 1, IN.dispatchThreadID.y + 1 ) * BLOCK_SIZE, -1.0f, 1.0f );

    float3 viewSpace[4];
    // Now convert the screen space points to view space
    for ( int i = 0; i < 4; i++ )
    {
        viewSpace[i] = ScreenToView( screenSpace[i] ).xyz;
    }

    // Now build the frustum planes from the view space points
    Frustum frustum;

    // Left plane
    frustum.planes[0] = ComputePlane( eyePos, viewSpace[2], viewSpace[0] );
    // Right plane
    frustum.planes[1] = ComputePlane( eyePos, viewSpace[1], viewSpace[3] );
    // Top plane
    frustum.planes[2] = ComputePlane( eyePos, viewSpace[0], viewSpace[1] );
    // Bottom plane
    frustum.planes[3] = ComputePlane( eyePos, viewSpace[3], viewSpace[2] );

    // Store the computed frustum in global memory (if our thread ID is in bounds of the grid).
    if ( IN.dispatchThreadID.x < numThreads.x && IN.dispatchThreadID.y < numThreads.y )
    {
        uint index = IN.dispatchThreadID.x + ( IN.dispatchThreadID.y * numThreads.x );
        out_Frustums[index] = frustum;
    }
}


ivec2 tileID = ivec2(gl_WorkGroupID.xy);
ivec2 tileNumber = ivec2(gl_NumWorkGroups.xy);
uint index = tileID.y * tileNumber.x + tileID.x;

// Steps based on tile sale
vec2 negativeStep = (2.0 * vec2(tileID)) / vec2(tileNumber);
vec2 positiveStep = (2.0 * vec2(tileID + ivec2(1, 1))) / vec2(tileNumber);

// Set up starting values for planes using steps and min and max z values
frustumPlanes[0] = vec4(1.0, 0.0, 0.0, 1.0 - negativeStep.x); // Left
frustumPlanes[1] = vec4(-1.0, 0.0, 0.0, -1.0 + positiveStep.x); // Right
frustumPlanes[2] = vec4(0.0, 1.0, 0.0, 1.0 - negativeStep.y); // Bottom
frustumPlanes[3] = vec4(0.0, -1.0, 0.0, -1.0 + positiveStep.y); // Top
frustumPlanes[4] = vec4(0.0, 0.0, -1.0, -minDepth); // Near
frustumPlanes[5] = vec4(0.0, 0.0, 1.0, maxDepth); // Far

// Transform the first four planes
for (uint i = 0; i < 4; i++) {
	frustumPlanes[i] *= viewProjection;
	frustumPlanes[i] /= length(frustumPlanes[i].xyz);
}

// Transform the depth planes
frustumPlanes[4] *= view;
frustumPlanes[4] /= length(frustumPlanes[4].xyz);
frustumPlanes[5] *= view;
frustumPlanes[5] /= length(frustumPlanes[5].xyz);
#endif

void ForwardPlus::createFrustums(App *app)
{
	app->getProfiler().start("Create Frustums");

	// TODO: create these on the GPU, spawn a thread for each frustum.
	// Actually, constructing these on the GPU seems to be the least of my problems.

	mat4 mvMatrix = Renderer::instance().getActiveCamera().getInverse(); (void) (mvMatrix);
	mat4 invMvMatrix = mvMatrix.getInverse(); (void) (invMvMatrix);
	mat4 invPMatrix = Renderer::instance().getActiveCamera().getProjection().getInverse(); (void) (invPMatrix);
	ivec4 view = Renderer::instance().getActiveCamera().getViewport(); (void) (view);
	vec4 viewport(view.x, view.y, view.z, view.w);
	vec3 eyePos(0, 0, 0); (void) (eyePos);

	int nTiles = width * height; (void) (nTiles);
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			int tile = width * y + x;
			vec2 tileSize(viewport.z / width, viewport.w / height);

		#if 0
			// Step sizes are in clip space, get them into eye space.
			//vec2 negativeStep = (2.0f * vec2(tile, tile)) / vec2(nTiles, nTiles);
			//vec2 positiveStep = (2.0f * vec2(tile + 1, tile + 1)) / vec2(nTiles, nTiles);

			//vec2 negativeStep = (2.0f * vec2(x * tileSize.x, y * tileSize.y)) / vec2(viewport.z, viewport.w);
			//vec2 positiveStep = (2.0f * vec2((x + 1) * tileSize.x, (y + 1) * tileSize.y)) / vec2(viewport.z, viewport.w);
			//negativeStep = (invPMatrix * vec4(negativeStep, 0, 1)).xy;
			//positiveStep = (invPMatrix * vec4(positiveStep, 0, 1)).xy;

			vec4 negativeStep = invMvMatrix * getEyeFromWindow(vec3(x * tileSize.x, y * tileSize.y, 1), viewport, invPMatrix);
			vec4 positiveStep = invMvMatrix * getEyeFromWindow(vec3((x + 1) * tileSize.x, (y + 1) * tileSize.y, 1), viewport, invPMatrix);

			// Frustum planes are left, right, top, bottom.
			frustumPlanes[tile * 4] = vec4(1, 0, 0, 1.0f - negativeStep.x);
			frustumPlanes[tile * 4 + 1] = vec4(-1, 0, 0, -1.0f + positiveStep.x);
			frustumPlanes[tile * 4 + 2] = vec4(0, -1, 0, -1.0f + positiveStep.y);
			frustumPlanes[tile * 4 + 3] = vec4(0, 1, 0, 1.0f - negativeStep.y);

			// Convert to eye space.
			for (int i = 0; i < 4; i++)
			{
				frustumPlanes[tile * 4 + i] = mvMatrix * frustumPlanes[tile * 4 + i];
				frustumPlanes[tile * 4 + i] /= vec3(frustumPlanes[tile * 4 + i].xyz).len();
			}
		#else
			// Four points on the far clipping plane.
			vec4 ssPos[4];
			ssPos[0] = vec4(x * tileSize.x, y * tileSize.y, 1, 1);
			ssPos[1] = vec4((x + 1) * tileSize.x, y * tileSize.y, 1, 1);
			ssPos[2] = vec4(x * tileSize.x, (y + 1) * tileSize.y, 1, 1);
			ssPos[3] = vec4((x + 1) * tileSize.x, (y + 1) * tileSize.y, 1, 1);

			vec3 esPos[4];
			for (int i = 0; i < 4; i++)
				esPos[i] = getEyeFromWindow(ssPos[i].xyz, viewport, invPMatrix).xyz;

			// Frustum planes from eye space points, planes are left, right, top, bottom.
			frustumPlanes[tile * 4] = computePlane(eyePos, esPos[2], esPos[0]);
			frustumPlanes[tile * 4 + 1] = computePlane(eyePos, esPos[1], esPos[3]);
			frustumPlanes[tile * 4 + 2] = computePlane(eyePos, esPos[0], esPos[1]);
			frustumPlanes[tile * 4 + 3] = computePlane(eyePos, esPos[3], esPos[2]);

			#if 0
				// Need to update w values.
				// FIXME: Seems to be incorrect...
				vec2 negStep = (2.0f * vec2(x * tileSize.x, y * tileSize.y)) / vec2(viewport.z, viewport.w);
				vec2 posStep = (2.0f * vec2((x + 1) * tileSize.x, (y + 1) * tileSize.y)) / vec2(viewport.z, viewport.w);
				negStep = (invPMatrix * vec4(negStep, -1, 1)).xy;
				posStep = (invPMatrix * vec4(posStep, -1, 1)).xy;

				frustumPlanes[tile * 4].w = 1.0f - negStep.x;
				frustumPlanes[tile * 4 + 1].w = -1.0f + posStep.x;
				frustumPlanes[tile * 4 + 2].w = -1.0f + posStep.y;
				frustumPlanes[tile * 4 + 3].w = 1.0f - negStep.y;
				frustumPlanes[tile * 4].w /= frustumPlanes[tile * 4].xyz.len();
				frustumPlanes[tile * 4 + 1].w /= frustumPlanes[tile * 4 + 1].xyz.len();
				frustumPlanes[tile * 4 + 2].w /= frustumPlanes[tile * 4 + 2].xyz.len();
				frustumPlanes[tile * 4 + 3].w /= frustumPlanes[tile * 4 + 3].xyz.len();
			#endif
		#endif
		}
	}

#if TEST_FRUSTUMS
	/*
	Ray Cluster (0,0): 3773; 2302; 2262; 2022; 1925; 849;
	Ray Cluster (16,16): 3180; 2786; 2532; 2529; 2022; 2018; 1935; 1928; 1925; 1916; 1893; 1890; 1743; 1706; 1591; 1508; 1503; 1375; 1169; 723; 695; 650; 431; 367; 283; 204; 13,204; 191; 62;
	Ray Cluster (31,31): 3715; 3256; 2407; 2022; 1925; 1306; 1064; 763; 620; 333; 230; 

	Forward+:
	(0,0) with plane.w = 0: 101; 536; 849; 1274; 1766; 1925; 2022;
	(0,0) with plane.w = -1: 1925
	(31,31) with plane.w = 0: 230; 275; 1400; 1883; 1925; 2022;
	(31,31) with plane.w = -1: 
	(16,16) with plane.w = 0: 695 1274 1508 1674 1893 1925 2022
	(16,16) with plane.w = -1:
	*/

	// Print all lights that intersect a specific frustum (in this case the centre).
	int x = 16;
	int y = 16;
	int tile = width * y + x;

	// Print eye space centre of frustum.
	vec3 esPos = getEyeFromWindow(vec3(x, y, 1.0), viewport, invPMatrix).xyz;
	std::cout << "frustum: " << x << ", " << y << "; " << esPos << "\n";

	for (size_t l = 0; l < app->getNSphereLights(); l++)
	{
		auto &light = app->getSphereLightPositions()[l];
		light.w = 1.0f;
		light = Renderer::instance().getActiveCamera().getInverse() * light;

		auto radius = app->getSphereLightRadii()[l];
		bool inside = true;
		for (size_t i = 0; i < 4; i++)
		{
			if (!isInFrustum(frustumPlanes[tile * 4 + i], light.xyz, radius))
			{
				inside = false;
				break;
			}

		}
		if (inside)
			std::cout << l << "; ";
	}
	std::cout << "\n";

#if 0
	std::vector<std::vector<int>> lightLists(width * height);
	// Test all the frustums to see what lights intersect them.
	for (size_t l = 0; l < app->getNSphereLights(); l++)
	{
		auto &light = app->getSphereLightPositions()[l];
		light.w = 1.0f;
		light = Renderer::instance().getActiveCamera().getInverse() * light;

		auto radius = app->getSphereLightRadii()[l];
		bool inside = true;
		for (size_t i = 0; i < frustumPlanes.size(); i += 4)
		{
			for (size_t j = 0; j < 4; j++)
			{
				if (!isInFrustum(frustumPlanes[i + j], light.xyz, radius))
				{
					inside = false;
					break;
				}
			}

			if (inside)
				lightLists[i / 4].push_back(l);
		}
	}
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			int tile = width * y + x;
			std::cout << x << ", " << y << ": ";
			for (auto id : lightLists[tile])
				std::cout << id << ", ";
			std::cout << "\n";
		}
	}
#endif

	exit(0);
#endif

	frustumPlanesBuffer.bufferData(&frustumPlanes[0], frustumPlanes.size() * sizeof(vec4));

	app->getProfiler().time("Create Frustums");
}

void ForwardPlus::captureLighting(App *app)
{
	app->getProfiler().start("Light Capture");

	glPushAttrib(GL_ENABLE_BIT);

	glEnable(GL_RASTERIZER_DISCARD);

	lightLFB.resize(width, height);

	// Gather approach spawns a warp per tile, testing all lights for intersection with the tile and testing the depth mask.
	// Scatter approach spawns thread per light, testing which tiles the light intersects and the depth mask.

	if (lightLFB.getType() == LFB::LINEARIZED)
		countLists(app);

	// Capture fragments into the lfb.
	lightLFB.beginCapture();
	createLists(app);
	if (lightLFB.endCapture())
	{
		lightLFB.beginCapture();
		createLists(app);
		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	glPopAttrib();

	app->getProfiler().time("Light Capture");
}

void ForwardPlus::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	if (gBufferFlag && opaqueFlag != 1)
		return;

	if (gBufferFlag)
		gBuffer.resize(camera.getWidth(), camera.getHeight());

	// Re-allocate cluster data if the camera has resized.
	if (width != ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE) || height != ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE))
	{
		width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
		height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

		resizeFrustumBuffer();
		app->getDepthMask().resize(width, height);
	}

	if (gBufferFlag)
	{
		gBuffer.beginCapture();
		app->captureGeometry(nullptr, nullptr, &gBuffer.getCaptureShader(), false);
		gBuffer.endCapture();
	}

	createFrustums(app);

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	if (gBufferFlag)
		app->createDepthMask(gBuffer.getDepthTexture());
	else
		app->createDepthMask();
#endif

	// Capture lighting first (no need to sort).
	captureLighting(app);

	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	// Capture geometry, using light information.
	if (gBufferFlag)
	{
		app->getProfiler().start("Light Deferred");

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		captureGeometryShader.bind();
		gBuffer.bindTextures();
		gBuffer.setUniforms(&captureGeometryShader);
		app->setCameraUniforms(&captureGeometryShader);
		app->setLightUniforms(&captureGeometryShader);
		#if CLUSTER_CULL
			app->getDepthMask().setUniforms(&captureGeometryShader);
		#endif
		lightLFB.setUniforms(&captureGeometryShader);

		Renderer::instance().drawQuad();

		gBuffer.unbindTextures();
		captureGeometryShader.unbind();

		app->getProfiler().time("Light Deferred");
	}
	else
		app->captureGeometry(&lightLFB, nullptr, &captureGeometryShader);

	if (opaqueFlag == 1)
	{
		glPopAttrib();
		return;
	}

	// Sort and composite (no need to write to global memory).
	app->sortGeometry();
}

void ForwardPlus::update(float dt)
{
	(void) (dt);
}

void ForwardPlus::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	auto &camera = Renderer::instance().getActiveCamera();
	width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

	resizeFrustumBuffer();

	// Normally you'd want this to be linearized, but light capture is so slow here that it's best to use a link list.
	//lightLFB.setType(type, "light", fragSize);
#if !LINEARISED
	lightLFB.setType(Rendering::LFB::LINK_LIST, "light", LFB::UINT, false);
#else
	lightLFB.setType(Rendering::LFB::LINEARIZED, "light", LFB::UINT, false);
#endif
	lightLFB.setMaxFrags(MAX_FRAGS);

	lightLFB.setProfiler(&app->getProfiler());
	if (gBufferFlag)
		gBuffer.setProfiler(&app->getProfiler());

	// Different shaders for gather or scatter.
#if GATHER
	auto vertLight = ShaderSourceCache::getShader("capForwardPlus").loadFromFile("shaders/oit/forwardPlus/gather.vert");
#else
	auto vertLight = ShaderSourceCache::getShader("capForwardPlus").loadFromFile("shaders/oit/forwardPlus/scatter.vert");
#endif
	vertLight.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	vertLight.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	vertLight.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	vertLight.setDefine("THREADS_PER_TILE", Utils::toString(THREADS_PER_TILE));
	vertLight.setDefine("LINEARISED", Utils::toString(LINEARISED));

	captureLightShader.release();
	captureLightShader.create(&vertLight);

	// Shader for capturing geometry, applies lighting during capture.
	auto vertCapture = ShaderSourceCache::getShader("captureForwardPlusVert").loadFromFile("shaders/oit/forwardPlus/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureForwardPlusFrag").loadFromFile("shaders/oit/forwardPlus/capture.frag");
	fragCapture.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	fragCapture.setDefine("LIGHT_COUNT_DEBUG", Utils::toString(LIGHT_COUNT_DEBUG));
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragCapture.setDefine("LINEARISED", Utils::toString(LINEARISED));
	if (gBufferFlag)
	{
		vertCapture.setDefine("USE_G_BUFFER", "1");
		fragCapture.setDefine("USE_G_BUFFER", "1");
	}

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);
}

void ForwardPlus::saveLFB(App *app)
{
	auto s = lightLFB.getStr();
	//auto s = app->getDepthMask().getStr();
	Utils::File f("lightForwardPlusLFB.txt");
	f.write(s);

	app->saveGeometryLFB();
}

