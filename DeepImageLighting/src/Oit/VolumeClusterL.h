#pragma once

#if 0

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"
#include "../../../Renderer/src/LFB/Cluster.h"
#include "../../../Renderer/src/LFB/ClusterMask.h"

class VolumeClusterL : public Oit
{
private:
	Rendering::LFB lightLFB;

	Rendering::Shader captureLightShader;
	Rendering::Shader sortLightShader;
	Rendering::Shader countLightShader;

	Rendering::Shader captureGeometryShader;
	Rendering::Shader sortGeometryShader;

	Rendering::BMA lightBMA;
	Rendering::BMA geometryBMA;

	Rendering::Cluster cluster;
	Rendering::ClusterMask mask;

	// For constructing and storing the clusters.
	int width;
	int height;

	Rendering::Shader countClusterShader;
	Rendering::Shader createClusterShader;

private:
	void countClusters(App *app);
	void createClusters();

public:
	VolumeClusterL() : Oit(), width(0), height(0) {}
	virtual ~VolumeClusterL() {}

	virtual void init() override;

	void createClusterMask(App *app);

	void captureLighting(App *app);
	void sortLighting(App *app);
	void createLightClusters(App *app);

	void captureGeometry(App *app);
	void sortGeometry(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

#endif

