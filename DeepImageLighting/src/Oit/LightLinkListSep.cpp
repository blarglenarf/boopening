#include "LightLinkListSep.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"
#include "../../../Utils/src/File.h"

#include <string>

//
// Implementation of Clustered shading, using a separate texture for each light.
//

// Cluster space is determined by near and far planes of the frustum <-- I think, it could also be determined by nearest and farthest fragment.


#define USE_BMA 1
#define MAX_FRAGS 512
#define LIGHT_DRAW 0
#define LIGHT_8 1
#define PROFILE_MEMORY 1
#define FULL_CLEAR 0
#define CLUSTER_CULL 1
#define CONSERVATIVE_RASTER 0
#define LINEARISED 0

using namespace Rendering;


void LightLinkListSep::allocStorageBuffers(App *app)
{
	(void) (app);

	// 2d texture arrays to store temporary depths and frambuffer objects to render to them.
	for (auto *b : lightDepthBuffers)
		delete b;
	for (auto *t : lightDepthTextures)
		delete t;

	if (app->getNLights() < 2048)
	{
		lightDepthBuffers.resize(1);
		lightDepthTextures.resize(1);
	}
	else
	{
		lightDepthBuffers.resize(app->getNLights() / 2048);
		lightDepthTextures.resize(app->getNLights() / 2048);
	}

	for (size_t i = 0; i < lightDepthBuffers.size(); i++)
	{
		lightDepthTextures[i] = new Texture2DArray();
		lightDepthBuffers[i] = new FrameBuffer();

		lightDepthBuffers[i]->setSize(width, height, app->getNLights() > 2048 ? 2048 : app->getNLights());
	#if LIGHT_8
		lightDepthBuffers[i]->create(GL_RG8, lightDepthTextures[i]);
	#else
		lightDepthBuffers[i]->create(GL_RG32F, lightDepthTextures[i]);
	#endif
	}

	// Clear the light textures.
	for (auto *b : lightDepthBuffers)
	{
		b->bind();
		glClear(GL_COLOR_BUFFER_BIT);
		b->unbind();
	}

	if (app->getNLights() < 2048)
		textureUniforms.resize(1);
	else
		textureUniforms.resize(app->getNLights() / 2048);
	for (size_t i = 0; i < textureUniforms.size(); i++)
		textureUniforms[i] = (int) i;

	// Realloc lfb memory.
	lightLFB.resize(width, height);

#if PROFILE_MEMORY
	auto &profiler = app->getProfiler();
	profiler.setBufferSize("lightDepths", app->getNLights() * 2 * width * height); // 8 bit version only.
#endif
}

void LightLinkListSep::drawLights(App *app, bool clearFlag)
{
	// Draw lights, each into its own 2d texture.
	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

#if CONSERVATIVE_RASTER
	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	if (!clearFlag)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);
	#if CONSERVATIVE_RASTER
		glBlendEquation(GL_MAX);
	#else
		glBlendEquation(GL_FUNC_ADD);
	#endif
	}

	auto &camera = Renderer::instance().getActiveCamera();

	// Resize based on grid size.
#if CONSERVATIVE_RASTER
	Math::ivec2 tileSize(ceil((float) camera.getWidth() / (float) width), ceil((float) camera.getHeight() / (float) height));
	Math::ivec4 viewport = camera.getViewport();
	Math::mat4 invPmatrix = camera.getInverseProj();
#endif
	app->setTmpViewport(width, height);

	auto &shader = clearFlag ? clearLightShader : captureLightShader;

	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	shader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
#if CONSERVATIVE_RASTER
	shader.setUniform("ConeLightRot", &app->getConeLightRotBuffer());
	shader.setUniform("tileSize", tileSize);
	shader.setUniform("viewport", viewport);
	shader.setUniform("invPMatrix", invPmatrix);
#else
	shader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
#endif

	size_t coneOffset = 0;

	for (size_t i = 0; i < lightDepthBuffers.size(); i++)
	{
		// Render in groups of 2048 lights.
		lightDepthBuffers[i]->bind();

		if (app->getNLights() > 2048)
		{
			// Based on current i value, render sphere lights first, then cone lights.
			size_t curr = (i + 1) * 2048;
			if (curr <= app->getNSphereLights())
			{
				shader.setUniform("indexOffset", (unsigned int) 0);
				app->getVaoSphereLights().renderRange((int) i * 2048, 2048);
			}
			else
			{
				if (coneOffset == 0)
					coneOffset = i;
				int sphereCount = (int) app->getNSphereLights() - ((int) i * 2048);

				// Need to render more spheres or not?
				if (sphereCount > 0)
				{
					shader.setUniform("indexOffset", (unsigned int) 0);
					app->getVaoSphereLights().renderRange((int) i * 2048, sphereCount);
					shader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
					app->getVaoConeLights().renderRange((int) (i - coneOffset) * 2048, 2048 - sphereCount);
				}
				else
				{
					shader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
					app->getVaoConeLights().renderRange((int) (i - coneOffset) * 2048, 2048);
				}
			}
		}
		else
		{
			shader.setUniform("indexOffset", (unsigned int) 0);
			app->getVaoSphereLights().render();
			shader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
			app->getVaoConeLights().render();
		}

		lightDepthBuffers[i]->unbind();
	}

	shader.unbind();

	// Restore original size.
	app->restoreViewport();

#if CONSERVATIVE_RASTER
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	glPopAttrib();
}

void LightLinkListSep::clearLightDepths(App *app)
{
#if FULL_CLEAR
	// Either clear the entire buffer, which is slow.
	(void) (app);

	for (auto *b : lightDepthBuffers)
	{
		b->bind();
		glClear(GL_COLOR_BUFFER_BIT);
		b->unbind();
	}
#else
	// Or re-render the lights, setting the values to zero, which is fast.
	drawLights(app, true);
#endif
}

void LightLinkListSep::countLists(App *app)
{
	lightLFB.beginCountFrags();
	countListShader.bind();
	lightLFB.setCountUniforms(&countListShader);
	app->setLightUniforms(&countListShader);
	app->setCameraUniforms(&countListShader);
	countListShader.setUniform("size", Math::ivec2(width, height));
	auto loc = countListShader.getUniform("lightDepths");
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&countListShader);
#endif
	glUniform1iv(loc, (GLsizei) textureUniforms.size(), &textureUniforms[0]);
	Renderer::instance().drawQuad();
	countListShader.unbind();
	lightLFB.endCountFrags();
}

void LightLinkListSep::createLists(App *app)
{
	createListShader.bind();
	lightLFB.setUniforms(&createListShader);
	app->setLightUniforms(&createListShader);
	app->setCameraUniforms(&createListShader);
	auto loc = createListShader.getUniform("lightDepths");
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&createListShader);
#endif
	glUniform1iv(loc, (GLsizei) textureUniforms.size(), &textureUniforms[0]);

	// Capture fragments into the lfb.
	Renderer::instance().drawQuad();
	createListShader.unbind();
}

void LightLinkListSep::captureLightList(App *app)
{
	app->getProfiler().start("Light Draw");
	drawLights(app);
	app->getProfiler().time("Light Draw");

#if LIGHT_DRAW
	return;
#endif

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	app->getProfiler().start("Light Capture");

	glPushAttrib(GL_ENABLE_BIT);

	glEnable(GL_RASTERIZER_DISCARD);

	lightLFB.resize(width, height);

	for (size_t i = 0; i < lightDepthTextures.size(); i++)
	{
		lightDepthTextures[i]->bind();
		textureUniforms[i] = lightDepthTextures[i]->getActiveNumber();
	}

	if (lightLFB.getType() == LFB::LINEARIZED)
		countLists(app);

	lightLFB.beginCapture();
	createLists(app);
	if (lightLFB.endCapture())
	{
		lightLFB.beginCapture();
		createLists(app);
		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	for (auto *t : lightDepthTextures)
		t->unbind();

	glPopAttrib();

	// Restore original size.
	app->restoreViewport();

	// Instead of completely clearing these, redraw after constructing the list, zero-ing everything.
	app->getProfiler().start("Light Depth Clear");
	clearLightDepths(app);
	app->getProfiler().time("Light Depth Clear");

	app->getProfiler().time("Light Capture");
}

void LightLinkListSep::setLightRes(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	if (gBufferFlag)
		gBuffer.resize(camera.getWidth(), camera.getHeight());

	// Re-allocate storage buffer data if the camera has resized.
	if (width != ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE) || height != ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE))
	{
		width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
		height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

		allocStorageBuffers(app);
	#if CLUSTER_CULL
		app->getDepthMask().resize(width, height);
	#endif
	}
}

void LightLinkListSep::drawLightClusters(App *app)
{
	debugLightShader.bind();
	lightDepthTextures[0]->bind();
	debugLightShader.setUniform("lightDepths", lightDepthTextures[0]);
	Renderer::instance().drawQuad();
	lightDepthTextures[0]->unbind();
	debugLightShader.unbind();

	clearLightDepths(app);
}



LightLinkListSep::~LightLinkListSep()
{
	for (auto *b : lightDepthBuffers)
		delete b;
	for (auto *t : lightDepthTextures)
		delete t;
}

void LightLinkListSep::init()
{
#if 0
	auto clusterCountVert = ShaderSourceCache::getShader("countClusterVert").loadFromFile("shaders/volumeLight/countLightRay.vert");
	auto clusterCountGeom = ShaderSourceCache::getShader("countClusterGeom").loadFromFile("shaders/volumeLight/countLightRay.geom");
	auto clusterCountFrag = ShaderSourceCache::getShader("countClusterFrag").loadFromFile("shaders/volumeLight/countLightRay.frag");
	clusterCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	clusterCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	clusterCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	clusterCountFrag.setDefine("USE_CLUSTER", "1");
	clusterCountFrag.setDefine("ATOMIC_ADD", "1");
	countClusterShader.create(&clusterCountVert, &clusterCountFrag, &clusterCountGeom);
#endif
}

void LightLinkListSep::render(App *app)
{
	if (gBufferFlag && opaqueFlag != 1)
		return;

	setLightRes(app);

	if (gBufferFlag)
	{
		gBuffer.beginCapture();
		app->captureGeometry(nullptr, nullptr, &gBuffer.getCaptureShader(), false);
		gBuffer.endCapture();
	}

#if CLUSTER_CULL
	if (gBufferFlag)
		app->createDepthMask(gBuffer.getDepthTexture());
	else
		app->createDepthMask();
#endif

	captureLightList(app);

#if LIGHT_DRAW
	drawLightClusters(app);
#else

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	// Capture geometry, applying lights from lists.
	if (gBufferFlag)
	{
		app->getProfiler().start("Light Deferred");

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		captureGeometryShader.bind();
		gBuffer.bindTextures();
		gBuffer.setUniforms(&captureGeometryShader);
		app->setCameraUniforms(&captureGeometryShader);
		app->setLightUniforms(&captureGeometryShader);
		#if CLUSTER_CULL
			app->getDepthMask().setUniforms(&captureGeometryShader);
		#endif
		lightLFB.setUniforms(&captureGeometryShader);

		Renderer::instance().drawQuad();

		gBuffer.unbindTextures();
		captureGeometryShader.unbind();

		app->getProfiler().time("Light Deferred");
	}
	else
		app->captureGeometry(&lightLFB, nullptr, &captureGeometryShader);

	if (opaqueFlag == 1)
	{
		glPopAttrib();
		return;
	}
	
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Sort geometry, compositing during sorting, either with or without BMA.
	app->sortGeometry();

	glPopAttrib();
#endif
}

void LightLinkListSep::update(float dt)
{
	(void) (dt);
}

void LightLinkListSep::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	auto &camera = Renderer::instance().getActiveCamera();
	width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

#if LINEARISED
	lightLFB.setType(LFB::LINEARIZED, "light", LFB::UINT, true);
#else
	lightLFB.setType(LFB::LINK_LIST, "light", LFB::UINT, true);
#endif

	lightLFB.setProfiler(&app->getProfiler());
	if (gBufferFlag)
		gBuffer.setProfiler(&app->getProfiler());

	// Shader for capturing lights to the 2d texture array.
	auto &lightCapVert = ShaderSourceCache::getShader("listSepCapLightVert").loadFromFile("shaders/oit/lightLinkListSep/captureLight.vert");
	auto &lightCapGeom = ShaderSourceCache::getShader("listSepCapLightGeom").loadFromFile("shaders/oit/lightLinkListSep/captureLight.geom");
	auto &lightCapFrag = ShaderSourceCache::getShader("listSepCapLightFrag").loadFromFile("shaders/oit/lightLinkListSep/captureLight.frag");
	lightCapVert.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightCapGeom.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightCapFrag.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightCapFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCapFrag.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
	lightCapFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	captureLightShader.release();
	captureLightShader.create(&lightCapVert, &lightCapFrag, &lightCapGeom);

	// Shader for clearing the lights, because clearing the color buffer bit is very slow.
	auto &lightClearVert = ShaderSourceCache::getShader("listSepClearLightVert").loadFromFile("shaders/oit/lightLinkListSep/captureLight.vert");
	auto &lightClearGeom = ShaderSourceCache::getShader("listSepClearLightGeom").loadFromFile("shaders/oit/lightLinkListSep/captureLight.geom");
	auto &lightClearFrag = ShaderSourceCache::getShader("listSepClearLightFrag").loadFromFile("shaders/oit/lightLinkListSep/clearLight.frag");
	lightClearVert.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightClearGeom.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));
	lightClearGeom.setDefine("CLEAR_DEPTH", "1");
	lightClearFrag.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
	lightClearFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

	clearLightShader.release();
	clearLightShader.create(&lightClearVert, &lightClearFrag, &lightClearGeom);

	// Shader for creating the light link list.
	auto &lightCreateVert = ShaderSourceCache::getShader("listSepCreateVert").loadFromFile("shaders/oit/lightLinkListSep/createList.vert");
	auto &lightCreateFrag = ShaderSourceCache::getShader("listSepCreateFrag").loadFromFile("shaders/oit/lightLinkListSep/createList.frag");
	lightCreateFrag.setDefine("MAX_LIGHTS", Utils::toString(app->getNLights()));
	lightCreateFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	lightCreateFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCreateFrag.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
	lightCreateFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCreateFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCreateFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCreateFrag.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));

	createListShader.release();
	createListShader.create(&lightCreateVert, &lightCreateFrag);

	// Shader for counting the light clink list.
	auto &lightCountVert = ShaderSourceCache::getShader("listSepCountVert").loadFromFile("shaders/oit/lightLinkListSep/createList.vert");
	auto &lightCountFrag = ShaderSourceCache::getShader("listSepCountFrag").loadFromFile("shaders/oit/lightLinkListSep/countList.frag");
	lightCountFrag.setDefine("MAX_LIGHTS", Utils::toString(app->getNLights()));
	lightCountFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	lightCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCountFrag.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
	lightCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCountFrag.setDefine("CONSERVATIVE_RASTER", Utils::toString(CONSERVATIVE_RASTER));

	countListShader.release();
	countListShader.create(&lightCountVert, &lightCountFrag);

	// Shader for capturing geometry, applies lighting during capture.
	auto &geomCapVert = ShaderSourceCache::getShader("listSepCapVert").loadFromFile("shaders/oit/lightLinkListSep/capture.vert");
	auto &geomCapFrag = ShaderSourceCache::getShader("listSepCapFrag").loadFromFile("shaders/oit/lightLinkListSep/capture.frag");
	geomCapFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	geomCapFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	geomCapFrag.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	geomCapFrag.setDefine("LINEARISED", Utils::toString(LINEARISED));
	geomCapFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	if (gBufferFlag)
	{
		geomCapVert.setDefine("USE_G_BUFFER", "1");
		geomCapFrag.setDefine("USE_G_BUFFER", "1");
	}

	captureGeometryShader.release();
	captureGeometryShader.create(&geomCapVert, &geomCapFrag);

	if (opaqueFlag == 0)
	{
	#if LIGHT_DRAW
		// Shader for drawing the lights, for debugging pruposes.
		auto vertDebug = ShaderSourceCache::getShader("listSepDebugVert").loadFromFile("shaders/oit/lightLinkListSep/drawLight.vert");
		auto fragDebug = ShaderSourceCache::getShader("listSepDebugFrag").loadFromFile("shaders/oit/lightLinkListSep/drawLight.frag");
		fragDebug.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
		fragDebug.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
		fragDebug.setDefine("LIGHT_8", Utils::toString(LIGHT_8));
		fragDebug.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));

		debugLightShader.release();
		debugLightShader.create(&vertDebug, &fragDebug);
	#endif
	}

	lightLFB.resize(width, height);
	allocStorageBuffers(app);
}

void LightLinkListSep::saveLFB(App *app)
{
	(void) (app);

	auto s = lightLFB.getStr();
	Utils::File f("lightLinkListSep.txt");
	f.write(s);
}

