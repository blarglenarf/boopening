#if 0

#include "Unsorted.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Renderer/src/LFB/LFBLinear.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>

#define USE_BMA 1
#define MAX_FRAGS 512
#define LIGHT_DRAW 0
#define LIGHT_DRAW_GEO 0
#define LIGHT_COUNT_DEBUG 0
#define COUNT_LIGHT_FRAGS 0
#define PACK_LIGHT_DATA 0
#define LIGHT_IMPLICIT 1
#define CLUSTER_CULL 0

using namespace Rendering;

void Unsorted::init()
{
#if LIGHT_IMPLICIT
	auto lightCountVert = ShaderSourceCache::getShader("countUnsortedVert").loadFromFile("shaders/volumeLight/countLightRay.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countUnsortedGeom").loadFromFile("shaders/volumeLight/countLightRay.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countUnsortedFrag").loadFromFile("shaders/volumeLight/countLightRay.frag");
#else
	auto lightCountVert = ShaderSourceCache::getShader("countUnsortedVert").loadFromFile("shaders/quadLight/countLight.vert");
	auto lightCountGeom = ShaderSourceCache::getShader("countUnsortedGeom").loadFromFile("shaders/quadLight/countLight.geom");
	auto lightCountFrag = ShaderSourceCache::getShader("countUnsortedFrag").loadFromFile("shaders/quadLight/countLight.frag");
#endif
	lightCountGeom.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	lightCountFrag.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	countLightShader.create(&lightCountVert, &lightCountFrag, &lightCountGeom);
}

void Unsorted::captureLighting(App *app)
{
	app->getProfiler().start("Light Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	lightLFB.resize(camera.getWidth(), camera.getHeight());

	if (lightLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		lightLFB.beginCountFrags(&countLightShader);
		countLightShader.setUniform("mvMatrix", camera.getInverse());
		countLightShader.setUniform("pMatrix", camera.getProjection());
		countLightShader.setUniform("viewport", camera.getViewport());
		countLightShader.setUniform("invPMatrix", camera.getInverseProj());
		countLightShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
		countLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
		countLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
		countLightShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
	#if CLUSTER_CULL
		app->getDepthMask().setUniforms(&countLightShader);
	#endif
		countLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		countLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();

		lightLFB.endCountFrags(&countLightShader);
	}

	captureLightShader.bind();
	captureLightShader.setUniform("mvMatrix", camera.getInverse());
	captureLightShader.setUniform("pMatrix", camera.getProjection());
	captureLightShader.setUniform("viewport", camera.getViewport());
	captureLightShader.setUniform("invPMatrix", camera.getInverseProj());
	captureLightShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
	captureLightShader.setUniform("SphereLightRadii", &app->getSphereLightRadiusBuffer());
	captureLightShader.setUniform("ConeLightDist", &app->getConeLightDistBuffer());
	captureLightShader.setUniform("ConeLightDir", &app->getConeLightDirBuffer());
#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&captureLightShader);
#endif

#if LIGHT_DRAW
	captureLightShader.setUniform("LightColors", &app->getLightColorBuffer());
#endif

	// Capture fragments into the lfb.
	lightLFB.beginCapture(&captureLightShader);
	captureLightShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (lightLFB.endCapture())
	{
		lightLFB.beginCapture(&captureLightShader);
		captureLightShader.setUniform("indexOffset", (unsigned int) 0);
		app->getVaoSphereLights().render();
		captureLightShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
		app->getVaoConeLights().render();

		if (lightLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	captureLightShader.unbind();

	app->getProfiler().time("Light Capture");
}

void Unsorted::sortLighting(App *app)
{
	app->getProfiler().start("Light Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	lightBMA.createMask(&lightLFB);
	lightBMA.sort(&lightLFB);
#else
	// Sort and composite normally.
	sortLightShader.bind();
	lightLFB.composite(&sortLightShader); // TODO: change this to a sort method.
	sortLightShader.unbind();
#endif

	app->getProfiler().time("Light Sort");
}

void Unsorted::composite(App *app)
{
	// Composite for light and geometry drawing.
	//auto &camera = Renderer::instance().getActiveCamera();

	compositeShader.bind();
	app->getGeometryLfb().beginComposite(&compositeShader);
	lightLFB.beginComposite(&compositeShader);
	Renderer::instance().drawQuad();
	app->getGeometryLfb().endComposite();
	lightLFB.endComposite();

	compositeShader.unbind();
}

void Unsorted::render(App *app)
{
#if CLUSTER_CULL
	auto &camera = Renderer::instance().getActiveCamera();

	// Re-allocate cluster data if the camera has resized.
	app->getDepthMask().resize(camera.getWidth(), camera.getHeight());

	// Create masks to determine which clusters are active.
	app->createDepthMask();
#endif

	// Capture lighting first (no need to sort).
	captureLighting(app);

#if LIGHT_DRAW
	glPushAttrib(GL_POLYGON_BIT | GL_ENABLE_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	sortLighting(app);

	#if LIGHT_DRAW_GEO
		// Now capture geometry into a separate lfb.
		app->captureGeometry(&lightLFB, nullptr, &captureGeometryShader);

		// Sort geometry and save it back to global memory.
		app->sortGeometry(false);

		// Composite using the light lfb and the geometry lfb.
		composite(app);
	#endif

	app->getProfiler().time("Composite");

	glPopAttrib();
#else
	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	// Capture geometry, using light information.
	app->captureGeometry(&lightLFB, nullptr, &captureGeometryShader);

	if (opaqueFlag == 1)
	{
		glPopAttrib();
		return;
	}

	// Sort and composite (no need to write to global memory).
	app->sortGeometry();
#endif
}

void Unsorted::update(float dt)
{
	(void) (dt);
}

void Unsorted::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

#if LIGHT_DRAW || !PACK_LIGHT_DATA
	int fragSize = 2;
#else
	int fragSize = 1;
#endif

	// Always use linearized for the lights.
	//lightLFB.setType(type, "light", fragSize);
	lightLFB.setType(Rendering::LFB::LINEARIZED, "light", fragSize, false);
	lightLFB.setMaxFrags(MAX_FRAGS);
	lightLFB.setProfiler(&app->getProfiler());

#if LIGHT_IMPLICIT
	auto vertLightCap = ShaderSourceCache::getShader("capUnsortedVert").loadFromFile("shaders/volumeLight/captureLightRay.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capUnsortedGeom").loadFromFile("shaders/volumeLight/captureLightRay.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capUnsortedFrag").loadFromFile("shaders/volumeLight/captureLightRay.frag");
#else
	auto vertLightCap = ShaderSourceCache::getShader("capUnsortedVert").loadFromFile("shaders/quadLight/captureLight.vert");
	auto geomLightCap = ShaderSourceCache::getShader("capUnsortedGeom").loadFromFile("shaders/quadLight/captureLight.geom");
	auto fragLightCap = ShaderSourceCache::getShader("capUnsortedFrag").loadFromFile("shaders/quadLight/captureLight.frag");
#endif
	geomLightCap.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragLightCap.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));
	fragLightCap.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));
	fragLightCap.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragLightCap.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	fragLightCap.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragLightCap.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	fragLightCap.setDefine("LIGHT_DRAW", Utils::toString(LIGHT_DRAW));

	captureLightShader.release();
	captureLightShader.create(&vertLightCap, &fragLightCap, &geomLightCap);

	// Sorting not necessary for composite, but used for light visualisation.
#if LIGHT_DRAW
	auto vertLightSort = ShaderSourceCache::getShader("sortUnsortedVert").loadFromFile("shaders/sort/sort.vert");
	auto fragLightSort = ShaderSourceCache::getShader("sortUnsortedFrag").loadFromFile("shaders/sort/sortL.frag");
	fragLightSort.setDefine("MAX_FRAGS", Utils::toString(lightLFB.getMaxFrags()));
	fragLightSort.setDefine("COMPOSITE_LOCAL", Utils::toString(0));
	//fragLightSort.replace("LFB_NAME", "light");
	fragLightSort.replace("LFB_NAME", "light_L");

	sortLightShader.release();
	sortLightShader.create(&vertLightSort, &fragLightSort);

	// Composite shader only used for light visualisation.
	auto vertComp = ShaderSourceCache::getShader("compUnsortedVert").loadFromFile("shaders/oit/unsorted/composite.vert");
	auto fragComp = ShaderSourceCache::getShader("compUnsortedFrag").loadFromFile("shaders/oit/unsorted/composite.frag");
	fragComp.setDefine("DRAW_GEOMETRY", Utils::toString(LIGHT_DRAW_GEO));

	compositeShader.release();
	compositeShader.create(&vertComp, &fragComp);
#endif

	// Shader for capturing geometry, applies lighting during capture.
	auto vertCapture = ShaderSourceCache::getShader("captureUnsortedVert").loadFromFile("shaders/oit/unsorted/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureUnsortedFrag").loadFromFile("shaders/oit/unsorted/capture.frag");
	fragCapture.setDefine("LIGHT_COUNT_DEBUG", Utils::toString(LIGHT_COUNT_DEBUG));
	fragCapture.setDefine("COUNT_LIGHT_FRAGS", Utils::toString(COUNT_LIGHT_FRAGS));
	fragCapture.setDefine("PACK_LIGHT_DATA", Utils::toString(PACK_LIGHT_DATA));
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);

#if LIGHT_DRAW
	lightBMA.createMaskShader("light", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMaskL.frag");
	lightBMA.createShaders(&lightLFB, "shaders/sort/sort.vert", "shaders/sort/sortL.frag");
#endif

#if COUNT_LIGHT_FRAGS
	passCount.release();
	passCount.create(nullptr, sizeof(unsigned int));

	failCount.release();
	failCount.create(nullptr, sizeof(unsigned int));
#endif
}

void Unsorted::saveLFB(App *app)
{
	auto s = lightLFB.getStrSorted(true);
	Utils::File f("lightUnsortedLFB.txt");
	f.writeLine(s);

	app->saveGeometryLFB();
}

#endif

