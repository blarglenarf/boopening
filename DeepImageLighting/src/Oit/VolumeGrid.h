#pragma once

#if 0

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"
#include "../../../Renderer/src/LFB/ClusterMask.h"

class VolumeGrid : public Oit
{
private:
	Rendering::LFB lightLFB;

	Rendering::Shader captureLightShader;
	Rendering::Shader sortLightShader;
	Rendering::Shader countLightShader;
	Rendering::Shader compositeShader;

	Rendering::BMA lightBMA;
	Rendering::BMA compositeBMA;

	Rendering::StorageBuffer passCount;
	Rendering::StorageBuffer failCount;

	Rendering::ClusterMask mask;

	int width;
	int height;

private:
	void compositeWithBMA(App *app);

public:
	VolumeGrid() : Oit(), width(0), height(0) {}
	virtual ~VolumeGrid() {}

	virtual void init() override;

	void createClusterMask(App *app);

	void captureLighting(App *app);
	void sortLighting(App *app);

	void composite(App *app);

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

#endif

