#pragma once

#define GRID_CELL_SIZE 16
#define CLUSTERS 32
#define MAX_EYE_Z 30.0
#define MASK_SIZE (CLUSTERS / 32)

#include "Oit/Oit.h"

#include "../../Renderer/src/LFB/LFB.h"
#include "../../Renderer/src/LFB/BMA.h"
#include "../../Renderer/src/LFB/Cluster.h"
#include "../../Renderer/src/LFB/ClusterMask.h"
#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/RenderResource/Mesh.h"
#include "../../Renderer/src/Profiler.h"

#include "../../Math/src/Vector/Vec4.h"

#include <vector>

class App
{
private:
	int currLFB;
	int currMesh;

	int opaqueFlag;

	Oit::LightType lightType;
	Oit *currOit;

	Rendering::LFB geometryLFB;
	Rendering::BMA geometryBMA;
	Rendering::BMA geometryLocalBMA;
	Rendering::ClusterMask mask;

	Rendering::Shader basicShader;
	Rendering::Shader sortGeometryShader;
	Rendering::Shader sortGeometryLocalShader;
	Rendering::Shader captureGeometryShader;
	Rendering::Shader particleShader;
	Rendering::Shader captureParticleShader;

	Rendering::GPUMesh gpuMesh;

	Rendering::Texture2D particleTex;

	Rendering::VertexArrayObject vaoSphereLights;
	Rendering::VertexArrayObject vaoConeLights;

	Rendering::StorageBuffer sphereLightPosBuffer;
	Rendering::StorageBuffer sphereLightRadiusBuffer;

	Rendering::StorageBuffer coneLightPosBuffer;
	Rendering::StorageBuffer coneLightDistBuffer;
	Rendering::StorageBuffer coneLightDirBuffer;
	Rendering::StorageBuffer coneLightRotBuffer;

	Rendering::StorageBuffer lightColorBuffer;
	Rendering::StorageBuffer lightVelBuffer;

	Rendering::Profiler profiler;

	std::vector<Math::vec4> sphereLightPositions;
	std::vector<float> sphereLightRadii;

	std::vector<Math::vec4> coneLightPositions;
	std::vector<float> coneLightDist;
	std::vector<Math::vec4> coneLightDir;
	std::vector<Math::vec2> coneLightRot;

	std::vector<Math::vec4> lightVectors;
	std::vector<float> lightCollisions;
	std::vector<float> lightColors;

	std::vector<Oit::LightType> lightTypes;
	int currLightType;

	int width;
	int height;

	int tmpWidth;
	int tmpHeight;

private:
	void createTestLight();
	void createLightsRandom(size_t sphereLights, size_t coneLights);
	void createLightsOnGeometry(Rendering::Mesh &mesh, size_t sphereLights, size_t coneLights);
	void createLightsForTeapot();
	void createLightBuffers();

	void setLightVelocities(float lightSpeed);
	void updateLightPositions(float dt);

	void drawLightParticles();

public:
	App() : currLFB(1), currMesh(0), opaqueFlag(0), lightType(Oit::BRUTE_FORCE), currOit(nullptr), currLightType(0), width(0), height(0), tmpWidth(0), tmpHeight(0) {}
	~App() {}

	void init();

	void setTmpViewport(int width, int height);
	void restoreViewport();

	void setLightUniforms(Rendering::Shader *shader);
	void setCameraUniforms(Rendering::Shader *shader);

	void captureGeometry(Rendering::LFB *lightLFB = nullptr, Rendering::Cluster *lightCluster = nullptr, Rendering::Shader *shader = nullptr, bool lightUniforms = true);
	void sortGeometry(bool composite = true);

	void captureGeometryAndParticles(Rendering::LFB *lightLFB = nullptr, Rendering::Cluster *lightCluster = nullptr, Rendering::Shader *shader = nullptr, bool lightUniforms = true);

	void createDepthMask(Rendering::Texture2D *depthTexture = nullptr);

	void render();
	void update(float dt);

	void runBenchmarks();

	void useMesh();
	void useLFB();
	void reloadShaders();

	void toggleTransparency();

	void loadNextMesh();
	void loadPrevMesh();

	void useNextLFB();
	void usePrevLFB();

	void doubleLights();
	void halveLights();

	void setNLights(size_t sphereLights, size_t coneLights);

	void cycleLighting();
	void useNextLighting();
	void usePrevLighting();
	void useLighting(Oit::LightType type);
	void playAnim();

	void saveGeometryLFB();
	void saveLFBData();

	Rendering::LFB &getGeometryLfb() { return geometryLFB; }
	Rendering::ClusterMask &getDepthMask() { return mask; }
	Rendering::GPUMesh &getGpuMesh() { return gpuMesh; }
	Rendering::VertexArrayObject &getVaoSphereLights() { return vaoSphereLights; }
	Rendering::VertexArrayObject &getVaoConeLights() { return vaoConeLights; }

	std::vector<Math::vec4> &getSphereLightPositions() { return sphereLightPositions; }
	std::vector<float> &getSphereLightRadii() { return sphereLightRadii; }

	std::vector<Math::vec4> &getConeLightPositions() { return coneLightPositions; }
	std::vector<float> &getConeLightDist() { return coneLightDist; }
	std::vector<Math::vec4> &getConeLightDir() { return coneLightDir; }
	std::vector<Math::vec2> &getConeLightRot() { return coneLightRot; }

	size_t getNSphereLights() const { return sphereLightPositions.size(); }
	size_t getNConeLights() const { return coneLightPositions.size(); }

	std::vector<float> &getLightColors() { return lightColors; }
	size_t getNLights() const { return lightColors.size(); }

	Rendering::StorageBuffer &getSphereLightPosBuffer() { return sphereLightPosBuffer; }
	Rendering::StorageBuffer &getSphereLightRadiusBuffer() { return sphereLightRadiusBuffer; }

	Rendering::StorageBuffer &getConeLightPosBuffer() { return coneLightPosBuffer; }
	Rendering::StorageBuffer &getConeLightDistBuffer() { return coneLightDistBuffer; }
	Rendering::StorageBuffer &getConeLightDirBuffer() { return coneLightDirBuffer; }
	Rendering::StorageBuffer &getConeLightRotBuffer() { return coneLightRotBuffer; }

	Rendering::StorageBuffer &getLightColorBuffer() { return lightColorBuffer; }

	Rendering::Profiler &getProfiler() { return profiler; }
};

