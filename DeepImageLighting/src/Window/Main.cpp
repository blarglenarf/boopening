#include <cstdlib>

#include "../App.h"

#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Platform/src/PlatformCommon.h"

#include <iostream>

#define UNUSED(x) (void)(x)

#define USE_FBO 0
#define USE_4K 0

using namespace Rendering;
using namespace Platform;

/*

Camera stuff

Atrium:

Balcony
type: PERSPECTIVE
pos: -0.5,-0.8,0
euler rot: -0.209439,0,0
quat rot: 0, 0, -0.104528, 0.994522
zoom: 6.68847
fov/near/far: 1.309, 0.01, 1000
viewport: 0,0,512,512


Outside
type: PERSPECTIVE
pos: 1.1,-1.95,0
euler rot: -0.392698,-0.366519,0
quat rot: 0.0355523, -0.178734, -0.191823, 0.964362
zoom: 23.4493
fov/near/far: 1.309, 0.01, 30
viewport: 0,0,512,512



Hairball
type: PERSPECTIVE
pos: -0.5,-0.8,0
euler rot: -0.0785394,-0.00872635,0
quat rot: 0.000171296, -0.0043598, -0.0392592, 0.99922
zoom: 16.4913
fov/near/far: 1.309, 0.01, 1000
viewport: 0,0,512,512



*/

// TODO: Resize the fbo when the window size changes perhaps?
#if USE_FBO
	#if USE_4K
		// 4k fbo.
		static int fboWidth = 4096;
		static int fboHeight = 2160;
	#else
		// 2k fbo
		static int fboWidth = 512;
		static int fboHeight = 512;
	#endif
#endif

bool wireframeFlag = false;


static void render(App &app)
{
#if USE_FBO
	Renderer::instance().getActiveCamera().setViewport(0, 0, fboWidth, fboHeight);
#else
	Renderer::instance().getActiveCamera().setViewport(0, 0, Window::getWidth(), Window::getHeight());
#endif
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glLoadIdentity();
	Renderer::instance().getActiveCamera().upload();

	app.render();
}

static void update(App &app, float dt)
{
	app.update(dt);

	auto &camera = Renderer::instance().getActiveCamera();
	camera.update(dt);
	//Renderer::instance().getActiveCamera().update(dt);

	if (Keyboard::isDown(Keyboard::w))
		camera.moveForward(dt);
	if (Keyboard::isDown(Keyboard::s))
		camera.moveBackward(dt);
	if (Keyboard::isDown(Keyboard::a))
		camera.moveLeft(dt);
	if (Keyboard::isDown(Keyboard::d))
		camera.moveRight(dt);
	if (Keyboard::isDown(Keyboard::SPACE))
		camera.moveUp(dt);
	if (Keyboard::isDown(Keyboard::LCTRL))
		camera.moveDown(dt);
}

static void toggleWireframe()
{
	wireframeFlag = !wireframeFlag;
	if (wireframeFlag)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

static bool mouseDrag(Event &ev)
{
	if (Mouse::isDown(Mouse::LEFT))
		Renderer::instance().getActiveCamera().updateRotation((float) ev.dx, (float) ev.dy);
	if (Mouse::isDown(Mouse::RIGHT))
		Renderer::instance().getActiveCamera().updateZoom((float) ev.dy);
	if (Mouse::isDown(Mouse::MIDDLE))
		Renderer::instance().getActiveCamera().updatePan((float) ev.dx, (float) ev.dy);
	return false;
}

static bool keyPress(App &app, Event &ev)
{
	switch (ev.key)
	{
	case Keyboard::ESCAPE:
	case Keyboard::q:
		exit(EXIT_SUCCESS);
		break;

	case Keyboard::p:
		toggleWireframe();
		break;

	case Keyboard::LEFT:
		//app.usePrevLFB();
		app.usePrevLighting();
		break;

	case Keyboard::RIGHT:
		//app.useNextLFB();
		app.useNextLighting();
		break;

	case Keyboard::UP:
		app.doubleLights();
		//app.loadNextMesh();
		break;

	case Keyboard::DOWN:
		app.halveLights();
		//app.loadPrevMesh();
		break;

	case Keyboard::t:
		app.toggleTransparency();
		break;

	case Keyboard::l:
		app.cycleLighting();
		break;

	case Keyboard::b:
		app.runBenchmarks();
		break;

	case Keyboard::c:
		std::cout << Renderer::instance().getActiveCamera() << "\n";
		break;

	case Keyboard::o:
		app.saveLFBData();
		break;

	case Keyboard::m:
		app.loadNextMesh();
		break;

	case Keyboard::k:
		app.playAnim();
		break;

	case Keyboard::r:
		app.reloadShaders();
		break;

	default:
		break;
	}

	return false;
}


int main(int argc, char **argv)
{
	UNUSED(argc);
	UNUSED(argv);

	// Cleanup code.
	std::atexit([]()-> void {
		Rendering::Renderer::instance().clear();
		Window::destroyWindow();
	});

	Window::createWindow(0, 0, 512, 512);
	//Window::createWindow(fboWidth, fboHeight);
	Renderer::instance().init();

	App app;
	auto &camera = Renderer::instance().addCamera("main", Camera(Camera::Type::PERSP));

#if USE_FBO
	Texture2D colorBuffer;
	RenderBuffer depthBuffer;
	FrameBuffer fbo(fboWidth, fboHeight);

	fbo.create(GL_RGBA, &colorBuffer, &depthBuffer, true);
#endif

#if 1
	// Atrium balcony view.
	camera.setPos({-0.5f, -0.8f, 0.0f});
	camera.setEulerRot({-0.209439f, 0.0f, 0.0f});
	camera.setZoom(6.68847f);
	camera.update(0.1f);
#else
	// Teapot view.
	camera.setPos({0.3f, 3.2f, 0.0f});
	camera.setEulerRot({-0.226892f, -0.0349066f, 0.0f});
	camera.setZoom(10.4929f);
	camera.update(0.1f);
	// Atrium outside view.
	//camera.setPos({1.1f, -1.95f, 0.0f});
	//camera.setEulerRot({-0.392698f, -0.366519f, 0.0f});
	//camera.setZoom(23.4493f);
	//camera.update(0.1f);
#endif

	// Hairball light view.
#if 0
	camera.setPos({-0.5f, -0.8f, 0.0f});
	camera.setEulerRot({-0.0785394f, -0.00872635f, 0.0f});
	camera.setZoom(16.4913f);
	camera.update(0.1f);
#endif

#if 0
	EventHandler::addHandler(Event::Type::KEY_PRESSED, &keyPress);
	EventHandler::addHandler(Event::Type::MOUSE_DRAG, &mouseDrag);
#endif

	app.init();

	bool finished = false;
	while (!finished)
	{
		float dt = Window::getDeltaTime();

		// First approach for event handling, using callback functions provided above.
		//finished = Window::handleEvents();
#if 1
		// Alternative approach, loop through the events and handle each in the switch statement.
		Window::processEvents();
		for (auto &ev : EventHandler::events)
		{
			switch (ev.type)
			{
			case Event::Type::WINDOW_RESIZED:
				Platform::Window::setWindowSize(ev);
				break;

			case Event::Type::KEY_PRESSED:
				keyPress(app, ev);
				break;

			case Event::Type::MOUSE_DRAG:
				mouseDrag(ev);
				break;

			default:
				break;
			}
		}
#endif
#if 0
		// Yet another alternative approach, just ask the keyboard and mouse if something has happened.
		Window::processEvents();
		if (Keyboard::isPressed(Keyboard::ESCAPE) || Keyboard::isPressed(Keyboard::q))
			exit(EXIT_SUCCESS);
		if (Keyboard::isPressed(Keyboard::p))
		{
			DebugControls::wireframeFlag = !DebugControls::wireframeFlag;
			DebugControls::toggleWireframe();
		}
		if (Keyboard::isPressed(Keyboard::c))
		{
			DebugControls::cullFlag = !DebugControls::cullFlag;
			DebugControls::toggleCulling();
		}
		if (Mouse::isDown(Mouse::LEFT))
			Renderer::instance().getActiveCamera().updateRotation(Mouse::dx, Mouse::dy);
		if (Mouse::isDown(Mouse::RIGHT))
			Renderer::instance().getActiveCamera().updateZoom(Mouse::dy);
		if (Mouse::isDown(Mouse::MIDDLE))
			Renderer::instance().getActiveCamera().updatePan(Mouse::dx, Mouse::dy);
#endif
		update(app, dt);

	#if USE_FBO
		fbo.bind();
	#endif
		render(app);
	#if USE_FBO
		fbo.unbind();
		fbo.blit();
	#endif

		CHECKERRORS;

		Window::swapBuffers();
	}

	Window::destroyWindow();

	return EXIT_SUCCESS;
}

