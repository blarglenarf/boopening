#include "App.h"

#include "Oit/Oit.h"
#include "Oit/BruteForce.h"
#include "Oit/Unsorted.h"
#include "Oit/UnsortedGrid.h"
#include "Oit/Volume.h"
#include "Oit/VolumeGrid.h"
#include "Oit/VolumeLocal.h"
#include "Oit/VolumeLocalGrid.h"
#include "Oit/LightLinkList.h"
#include "Oit/LightLinkListSep.h"
#include "Oit/PracticalCluster.h"
#include "Oit/VolumeClusterLL.h"
#include "Oit/VolumeClusterL.h"
#include "Oit/RayCluster.h"
#include "Oit/ForwardPlus.h"
#include "Oit/TiledCluster.h"
#include "Oit/Deferred.h"
#include "Oit/Visualiser.h"

#include "Oit/RayClusterLLI.h"
#include "Oit/RayClusterLAI.h"
#include "Oit/RayClusterLLF.h"
#include "Oit/RayClusterLAF.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <iomanip>


// TODO: Today's stuff.
// TODO: Switching between storing light data vs indices.
// TODO: Using registers when storing light data vs indices (any difference at all?).
// TODO: Benchmarking of various scenes using both of these approaches.
// TODO: Compare light build times of ourr stuff vs clustered shading.
// TODO: Compare render time with light indices vs light data.
// TODO: Finally, add this stuff to the paper, probably a couple more pages.

// Main things to try, or think about trying:
// STOP PROCRASTINATING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// TODO: Just get three sets of results now, for three scenes (one should probably be animated, and one with lights on the geometry).
// TODO: Position lights at random places on each triangle, not on vertices (may not matter in the rungholt scene).
// TODO: May need to update benchmarking code to make the above possible.
// TODO: Throw the new results in.
// DONE: Maybe consider using a different z range for light gridding (either make it dynamic, or less than 30).

// TODO: Remove animated lights (maybe, or maybe only compare them for some scenes).
// TODO: Figure out why lightUniforms bool causes deferred rendering problems on line 792ish.
// TODO: Frustum-cull lights.
// TODO: Improve clustered shading so that it uses correct depths.

// DONE: Compare linearised vs linked list vs blocked interleaved light grids.
// TODO: Have a key press to switch between each of these.
// TODO: Try 64 clusters instead of 32.
// DONE: Blocked memory reads/writes for the light grid.
// TODO: Is indexing by tile worth it?
// TODO: Another expansion could be per 3d grid-cell geometry min/max depths and comparing lights against those.
// TODO: Finally, depth-partition geometry for OIT.

// Combine the particles with the deep image.
// Billboard the particles according to the camera.
// Integrate the particles while drawing them, so we don't need to keep re-buffering light data.
// Stop re-buffering light data every frame, only do it when necessary.
// Particle rendering when using deferred lighting.
// Give the particles a bit of motion blur so they look semi-ok.
// Add collision detection.
// But if I do that, how do I handle collision detection?
// Throw the dragon mesh in there too maybe?
// If there's time then add normal mapping, otherwise ignore it.
// Make a pretty video and call it a day.
// Count shader for clusters -> max grid cell count per pixel.
// Capture geometry into the clusters.
// Cluster BMA.
// Rename the RefinedBMA project to Experimental or something, and implement low-res conservative oit.
// Remove unnecessary repetition in the paper.
// Come up with better figures.
// Make all vectors pass by value instead of by ref.
// Look into frustum raycasting.
// Shadows for many lights.
// Use proper glsl compute shaders instead of vertex shaders for everything.
// Frustum cull lights on all techniques, so we only ever deal with lights that are visible.
// Get to work on vulkan.
// Get rid of all the Utils::toString stuff, replace with std::to_string.
// Rename from OIT to Lighting please.
// Need to render during resize (this only seems to be a problem on windows).
// Cap frustum/tile depth ranges to a min/max depth range, not to 0/30.
// Clean up code as much as possible! Including shaders! <-- Pretty much an ongoing task.
// Clean up the cuda stuff in the makefile.
//
//
// Try out vulkan.
// Ongoing: Maybe move lightColors and lightPositions from the fragment to the vertex shader where possible.
// VolumeClusterLL is still using the old mask method for testing conservative rasterization, maybe have the new method account for this?
// Use camera.getInverseProj method instead of recalculating it each frame for the different deferred lighting techniques.
// Test with different scenes -- not sure if this is really necessary, since OIT dominates times of powerplant/hairball.
//
//
// Extra stuff:
// Normal mapping, texture filtering, anti-aliasing.
// Try out different anti-aliasing techniques, see how performance and image quality is affected.
// Make a forest with fireflies scene.
// Make a city scene.
// Find out what the problem is with these profiler memory errors.
// Fix framebuffer stack and get rid of shader stack.
// Get proper line numbers from error reporting in shaders.
// Improved serialization (offsetof).
// Add a version that doesn't apply any lighting (except default directional) for comparison.
// A bunch of camera positions for different scenes.
// Decide where the static currently bound shader can be used instead of passing shaders around.
// Add a RenderTarget to Texture.cpp/h to simplify render to texture stuff.
//
//
// Lighting stuff:
// http://i.imgur.com/quosAAS.gifv
// https://i.kinja-img.com/gawker-media/image/upload/s--xYdUYsYC--/1274487890424217890.jpg
// http://il7.picdn.net/shutterstock/videos/12805235/thumb/3.jpg
//
// Remember: nvidia-smi -q


using namespace Math;
using namespace Rendering;

// TODO: Get rid of this!
std::string currLightStr;
Flythrough anim;
Mesh mesh;
Font testFont;
float tpf = 0;


#define MAX_FRAGS 512

#define TEST_LIGHT 0
#define TEAPOT_LIGHTS 0
#define RANDOM_LIGHTS 1
#define GEO_LIGHTS 0
#define ANIMATE_LIGHTS 0
#define INIT_SPHERE_LIGHTS 1
#define INIT_CONE_LIGHTS 1

#define USE_BMA 1
#define DIRECTIONAL_LIGHT 1
#define USE_LINK_PAGES 0
#define TEST_EYESPACE_CONVERSION 0
#define SMALL_LIGHTS 1
#define MEDIUM_LIGHTS 0
#define LARGE_LIGHTS 0
#define MIXED_LIGHTS 0
#define DRAW_AXES 1
#define LIGHT_SPEED 2.0f
#define COLLISION_TIME 0.5f


#if TEST_EYESPACE_CONVERSION
	static vec4 getEyeFromWindow(vec3 p, vec4 viewport, mat4 pMatrix)
	{
		mat4 invPMatrix = pMatrix.getInverse();

		vec3 ndcPos;
		ndcPos.xy = ((p.xy - viewport.xy) / viewport.zw) * 2.0f - 1.0f;
		//ndcPos.xy = ((p.xy * 2.0f) - (viewport.xy * 2.0f)) / (viewport.zw) - 1.0f;
		ndcPos.z = 1.0f;
		//std::cout << "ndc: " << ndcPos << "\n";

		vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0f);
		eyeDir /= eyeDir.w;
		vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0f);
		return eyePos;
	}

	static void testEyeSpace(mat4 pMatrix, mat4 mvMatrix)
	{
		std::cout << "Testing eye space conversions\n";

		vec4 viewport(0, 0, 512, 512);

		vec4 osVert(1, 0.5f, -10, 1);
		vec4 esVert = mvMatrix * osVert;
		vec4 csVert = pMatrix * esVert;

		vec3 ndcVert = csVert.xyz / csVert.w;
		vec3 imgVert = ndcVert / 2.0 + 0.5;
		//imgVert += 0.5f;
		imgVert.xy *= viewport.zw;
		imgVert.z = (ndcVert.z + 1.0) / 2.0;

		//vec4 test = pMatrix.getInverse() * vec4(ndcVert, 1.0);
		//test /= test.w;

		std::cout << "os: " << osVert << "; es: " << esVert << "; cs: " << csVert << "; ndc: " << ndcVert << "; ss: " << imgVert << "\n";

		vec4 esConvert = getEyeFromWindow(vec3(imgVert.xy, esVert.z), viewport, pMatrix);
		std::cout << "new eye: " << esConvert << "\n";
	}

	static void testConservativeDepth(mat4 pMatrix, mat4 mvMatrix, mat3 nMatrix)
	{
		std::cout << "Testing conservative depth\n";

		vec4 viewport(0, 0, 512, 512);

		// Object space triangle.
		vec4 osVertA(-10, -10, -15, 1), osVertB(0, 10, -15, 1), osVertC(10, -10, -15, 1);
		vec3 osNorm(0, 0, 1);
		vec3 esNorm = (nMatrix * osNorm).unit();

		// Eye space.
		vec4 esVertA = mvMatrix * osVertA;
		vec4 esVertB = mvMatrix * osVertB;
		vec4 esVertC = mvMatrix * osVertC;

		std::cout << "es: " << esVertA << "; " << esVertB << "; " << esVertC << "\n";
		std::cout << "n: " << esNorm << "\n";

		// Clip space.
		vec4 csVertA = pMatrix * esVertA;
		vec4 csVertB = pMatrix * esVertB;
		vec4 csVertC = pMatrix * esVertC;

		// NDC.
		vec3 ndcVertA = csVertA.xyz / csVertA.w;
		vec3 ndcVertB = csVertB.xyz / csVertB.w;
		vec3 ndcVertC = csVertC.xyz / csVertC.w;

		// Screen space.
		vec3 ssVertA = ndcVertA / 2.0 + 0.5;
		ssVertA.xy *= viewport.zw;
		ssVertA.z = (ndcVertA.z + 1.0) / 2.0;
		vec3 ssVertB = ndcVertB / 2.0 + 0.5;
		ssVertB.xy *= viewport.zw;
		ssVertB.z = (ndcVertB.z + 1.0) / 2.0;
		vec3 ssVertC = ndcVertC / 2.0 + 0.5;
		ssVertC.xy *= viewport.zw;
		ssVertC.z = (ndcVertC.z + 1.0) / 2.0;

		std::cout << "ss: " << ssVertA << "; " << ssVertB << "; " << ssVertC << "\n";

		// Line through ssVert.
		vec3 dirA = getEyeFromWindow(vec3(ssVertA.xy, -30.0), viewport, pMatrix).xyz.unit();
		vec3 dirB = getEyeFromWindow(vec3(ssVertB.xy, -30.0), viewport, pMatrix).xyz.unit();
		vec3 dirC = getEyeFromWindow(vec3(ssVertC.xy, -30.0), viewport, pMatrix).xyz.unit();

		std::cout << dirA << "; " << dirB << "; " << dirC << "\n";

		// Distance along line.
		float dA = dot<float>(esNorm, esVertA.xyz) / dot<float>(esNorm, dirA);
		float dB = dot<float>(esNorm, esVertB.xyz) / dot<float>(esNorm, dirB);
		float dC = dot<float>(esNorm, esVertC.xyz) / dot<float>(esNorm, dirC);

		std::cout << "d: " << vec3(dA, dB, dC) << "\n";

		float zA = (-dirA * dA).z;
		float zB = (-dirB * dB).z;
		float zC = (-dirC * dC).z;

		std::cout << "z: " << vec3(-esVertA.z, -esVertB.z, -esVertC.z) << "; " << vec3(zA, zB, zC) << "\n";
	}
#endif


static vec3 genPos(const vec3 &min, const vec3 &max)
{
	float x = Utils::getRand(min.x, max.x);
	float y = Utils::getRand(min.y, max.y);
	float z = Utils::getRand(min.z, max.z);
	return vec3(x, y, z);
}

static vec3 genColor()
{
	float r = Utils::getRand(0.0f, 1.0f);
	float g = Utils::getRand(0.0f, 1.0f);
	float b = Utils::getRand(0.0f, 1.0f);
	return vec3(r, g, b);
}

static float genRadius()
{
	// 5% probability > 1.0.
	// 10% probability > 0.5.
	float p = Utils::getRand(0.0f, 100.0f);
#if MIXED_LIGHTS
	if (p <= 5)
		return Utils::getRand(2.0f, 4.0f);
	if (p <= 15)
		return Utils::getRand(1.0f, 2.0f);
	else
		return Utils::getRand(0.2f, 1.0f);
#elif MEDIUM_LIGHTS
	if (p <= 5)
		return Utils::getRand(1.5f, 2.0f);
	if (p <= 15)
		return Utils::getRand(1.0f, 1.5f);
	else
		return Utils::getRand(0.5f, 1.0f);
#elif LARGE_LIGHTS
	if (p <= 5)
		return Utils::getRand(8.0f, 10.0f);
	if (p <= 15)
		return Utils::getRand(6.0f, 8.0f);
	else
		return Utils::getRand(4.0f, 6.0f);
#elif SMALL_LIGHTS
	if (p <= 5)
		return Utils::getRand(0.8f, 1.0f);
	if (p <= 15)
		return Utils::getRand(0.5f, 0.8f);
	else
		return Utils::getRand(0.2f, 0.5f);
#endif
}

static vec3 getRandomDir()
{
	vec3 dir(Utils::getRand(-1.0f, 1.0f), Utils::getRand(-1.0f, 1.0f), Utils::getRand(-1.0f, 1.0f));
	dir.normalize();
	return dir;
}

static void collideLight(vec4 &pos, vec4 &dir, float &colTime, float dt)
{
	colTime -= dt;
	if (colTime < 0)
		colTime = 0;
	if (colTime > 0)
		return;

	pos.x = clamp(pos.x, -10.0f, 10.0f);
	pos.y = clamp(pos.y, -5.0f, 5.0f);
	pos.z = clamp(pos.z, -10.0f, 10.0f);

	// FIXME: For now this just assumes we're using the atrium.
	// Which edge did we hit?
	if (pos.x <= -10)
	{
		dir.xyz.reflect(vec3(1, 0, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.x >= 10)
	{
		dir.xyz.reflect(vec3(-1, 0, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.y <= -5)
	{
		dir.xyz.reflect(vec3(0, 1, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.y >= 5)
	{
		dir.xyz.reflect(vec3(0, -1, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.z <= -10)
	{
		dir.xyz.reflect(vec3(0, 0, 1));
		colTime = COLLISION_TIME;
	}

	else if(pos.z >= 10)
	{
		dir.xyz.reflect(vec3(0, 0, -1));
		colTime = COLLISION_TIME;
	}
}


void App::createTestLight()
{
	sphereLightRadii.resize(1);
	sphereLightPositions.resize(1);

	lightColors.resize(1);

	sphereLightRadii[0] = 30.0;
	sphereLightPositions[0] = vec4(0.0, 0.0, 0.0, 0.0);
	lightColors[0] = Utils::rgba8ToFloat(vec4(1.0, 1.0, 1.0, 1.0));
}

void App::createLightsRandom(size_t sphereLights, size_t coneLights)
{
	sphereLightRadii.resize(sphereLights);
	sphereLightPositions.resize(sphereLights);

	coneLightPositions.resize(coneLights);
	coneLightDist.resize(coneLights);
	coneLightDir.resize(coneLights);
	coneLightRot.resize(coneLights);

	lightColors.resize(sphereLights + coneLights);

	Utils::seedRand(0);

	vec3 min(-10, -5, -10), max(10, 5, 10);

	// Type 0 for spheres.
	for (size_t i = 0; i < sphereLights; i++)
	{
		// Light id or type could be used in the fourth value, even the radius if you're only using point lights.
		sphereLightRadii[i] = genRadius();
		sphereLightPositions[i] = vec4(genPos(min, max), 0.0);
	}

	// Type 1 for cones.
	for (size_t i = 0; i < coneLights; i++)
	{
	#if 1
		coneLightPositions[i] = vec4(genPos(min, max), 1.0);
		coneLightDist[i] = genRadius();

		// Dir is x,y,z then aperture.
		// Use two angles for rotation (x then z, rotation around y is redundant), and get direction vector from that.
		vec2 rot(Utils::getRand(0.0, pi), Utils::getRand(0.0, pi));
		vec4 dir = Quat::fromEuler(rot.x, 0.0, rot.y).normalize().getMatrix() * vec4(0.0, 1.0, 0.0, 0.0);
		coneLightDir[i] = vec4(dir.xyz.unit(), (float) Utils::getRand(pi / 16.0, pi / 4.0));
		coneLightRot[i] = rot;
	#else
		coneLightPositions[i] = vec4(0.0, 0.0, 0.0, 1.0);
		coneLightDist[i] = 5.0;

		// Dir is x,y,z then aperture.
		// Use two angles for rotation (x then z, rotation around y is redundant), and get direction vector from that.
		vec2 rot(0.0, 0.0);
		vec4 dir = Quat::fromEuler(rot.x, 0.0, rot.y).normalize().getMatrix() * vec4(0.0, 1.0, 0.0, 0.0);
		coneLightDir[i] = vec4(dir.xyz.unit(), (float) pi / 4.0f);
		coneLightRot[i] = rot;
	#endif
	}

	for (size_t i = 0; i < sphereLights + coneLights; i++)
		lightColors[i] = Utils::rgba8ToFloat(vec4(genColor(), 1.0f));

	if (lightVectors.size() != lightColors.size())
	{
		lightVectors.resize(lightColors.size());
		lightCollisions.resize(lightColors.size(), 0.0f);
	}

	// TODO: RBS can be slightly faster if lights are encountered in roughly the same order, but it's not completely necessary (effects on performance vary).
}

void App::createLightsOnGeometry(Mesh &mesh, size_t sphereLights, size_t coneLights)
{
	sphereLightRadii.resize(sphereLights);
	sphereLightPositions.resize(sphereLights);

	coneLightPositions.resize(coneLights);
	coneLightDist.resize(coneLights);
	coneLightDir.resize(coneLights);
	coneLightRot.resize(coneLights);

	lightColors.resize(sphereLights + coneLights);

	Utils::seedRand(0);

	// Just pick vertices from the mesh at random and assign lights to those positions.
	size_t nVertices = mesh.getNumVertices();
	Vertex *vertices = mesh.getVertices();

	// TODO: Could alternatively pick centres of triangles instead of vertices (or just random positions on triangles)...

	// Type 0 for spheres.
	for (size_t i = 0; i < sphereLights; i++)
	{
		size_t index = Utils::getRand((size_t) 0, nVertices - 1);
		auto pos = vertices[index].pos;

		sphereLightRadii[i] = genRadius();
		sphereLightPositions[i] = vec4(pos, 0.0);
	}

	// Type 1 for cones.
	for (size_t i = 0; i < coneLights; i++)
	{
		size_t index = Utils::getRand((size_t) 0, nVertices - 1);
		auto pos = vertices[index].pos;

		coneLightPositions[i] = vec4(pos, 1.0);
		coneLightDist[i] = genRadius();

		// Dir is x,y,z then aperture.
		// Use two angles for rotation (x then z, rotation around y is redundant), and get direction vector from that.
		vec2 rot(Utils::getRand(0.0, pi), Utils::getRand(0.0, pi));
		vec4 dir = Quat::fromEuler(rot.x, 0.0, rot.y).normalize().getMatrix() * vec4(0.0, 1.0, 0.0, 0.0);
		coneLightDir[i] = vec4(dir.xyz.unit(), (float) Utils::getRand(pi / 16.0, pi / 4.0));
		coneLightRot[i] = rot;
	}

	for (size_t i = 0; i < sphereLights + coneLights; i++)
		lightColors[i] = Utils::rgba8ToFloat(vec4(genColor(), 1.0f));

	if (lightVectors.size() != lightColors.size())
	{
		lightVectors.resize(lightColors.size());
		lightCollisions.resize(lightColors.size(), 0.0f);
	}
}

void App::createLightsForTeapot()
{
	Utils::seedRand(2);

	// TODO: Think about adding some more lights perhaps?
	size_t sphereLights = 3;

	sphereLightRadii.resize(sphereLights);
	sphereLightPositions.resize(sphereLights);
	lightColors.resize(sphereLights);

	// Place lights manually.
	sphereLightRadii[0] = 2;
	sphereLightPositions[0] = vec4(2.3, 4, 3.0, 0);
	lightColors[0] = Utils::rgba8ToFloat(vec4(1, 0, 0, 1));

	sphereLightRadii[1] = 2;
	sphereLightPositions[1] = vec4(1, 2, 4.0, 0);
	lightColors[1] = Utils::rgba8ToFloat(vec4(0, 1, 0, 1));

	sphereLightRadii[2] = 2;
	sphereLightPositions[2] = vec4(-2, 3.7, 3.0, 0);
	lightColors[2] = Utils::rgba8ToFloat(vec4(0, 0, 1, 1));

	//sphereLightRadii[3] = 1;
	//sphereLightPositions[3] = vec4(0, 4, 3.2, 0);

	//sphereLightRadii[4] = 1;
	//sphereLightPositions[4] = vec4(1, 5, 1, 0);

	//sphereLightRadii[5] = 1;
	//sphereLightPositions[5] = vec4(-1.5, 2, 3.5, 0);

	//sphereLightRadii[6] = 1;
	//sphereLightPositions[6] = vec4(-1, 5, 1, 0);

#if 0
	Utils::seedRand(0);

	for (size_t i = 0; i < sphereLights; i++)
	{
		// Light id or type could be used in the fourth value, even the radius if you're only using point lights.
		sphereLightRadii[i] = Utils::getRand(0.5f, 1.0f);
		vec3 lightDir(Utils::getRand(0.0f, 1.0f), Utils::getRand(0.0f, 1.0f), Utils::getRand(0.0f, 1.0f));
		lightDir *= 3.0f;
		sphereLightPositions[i] = vec4(lightDir.x, lightDir.y, lightDir.z, 0.0);
	}
#endif

	//for (size_t i = 0; i < sphereLights; i++)
	//	lightColors[i] = Utils::rgba8ToFloat(vec4(genColor(), 1.0f));
}

void App::createLightBuffers()
{
	sphereLightPosBuffer.release();
	sphereLightRadiusBuffer.release();
	vaoSphereLights.release();

	coneLightPosBuffer.release();
	coneLightDistBuffer.release();
	coneLightDirBuffer.release();
	coneLightRotBuffer.release();
	vaoConeLights.release();

	lightColorBuffer.release();
	lightVelBuffer.release();

	if (!sphereLightPositions.empty())
	{
		sphereLightPosBuffer.create(&sphereLightPositions[0], sphereLightPositions.size() * sizeof(vec4));
		sphereLightRadiusBuffer.create(&sphereLightRadii[0], sphereLightRadii.size() * sizeof(float));

		vaoSphereLights.setMode(GL_POINTS);
		vaoSphereLights.create(VertexFormat({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} }), &sphereLightPositions[0], sphereLightPositions.size() * sizeof(vec4), sizeof(vec4));
	}

	if (!coneLightPositions.empty())
	{
		coneLightPosBuffer.create(&coneLightPositions[0], coneLightPositions.size() * sizeof(vec4));
		coneLightDistBuffer.create(&coneLightDist[0], coneLightDist.size() * sizeof(float));
		coneLightDirBuffer.create(&coneLightDir[0], coneLightDir.size() * sizeof(vec4));
		coneLightRotBuffer.create(&coneLightRot[0], coneLightRot.size() * sizeof(vec2));

		vaoConeLights.setMode(GL_POINTS);
		vaoConeLights.create(VertexFormat({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} }), &coneLightPositions[0], coneLightPositions.size() * sizeof(vec4), sizeof(vec4));
	}

	// Note: doing this without actually creating a vbo that uses it causes glDrawArrays (without any bound buffers) to
	// fail when drawing 256 points on my windows machine, but not at uni, and not when drawing > 256 points. No idea why.
	// glEnableVertexAttribArray(0);

	if (!lightColors.empty())
		lightColorBuffer.create(&lightColors[0], lightColors.size() * sizeof(float));
	if (!lightVectors.empty())
		lightVelBuffer.create(&lightVectors[0], lightVectors.size() * sizeof(vec4));
}

void App::setLightVelocities(float lightSpeed)
{
	lightVectors.resize(sphereLightPositions.size() + coneLightPositions.size());
	lightCollisions.resize(lightVectors.size(), 0.0f);

	for (size_t i = 0; i < sphereLightPositions.size(); i++)
		lightVectors[i] = vec4(getRandomDir() * lightSpeed, 0.0f);

	for (size_t i = 0; i < coneLightPositions.size(); i++)
		lightVectors[i + sphereLightPositions.size()] = vec4(getRandomDir() * lightSpeed, 0.0f);
}

void App::updateLightPositions(float dt)
{
	for (size_t i = 0; i < sphereLightPositions.size(); i++)
	{
		sphereLightPositions[i].xyz += lightVectors[i].xyz * dt;
		collideLight(sphereLightPositions[i], lightVectors[i], lightCollisions[i], dt);
	}

	for (size_t i = 0; i < coneLightPositions.size(); i++)
	{
		coneLightPositions[i].xyz += lightVectors[i + sphereLightPositions.size()].xyz * dt;
		collideLight(coneLightPositions[i], lightVectors[i + sphereLightPositions.size()], lightCollisions[i + sphereLightPositions.size()], dt);
	}

	if (sphereLightPosBuffer.isGenerated())
	{
		sphereLightPosBuffer.create(&sphereLightPositions[0], sphereLightPositions.size() * sizeof(vec4));
		vaoSphereLights.create(VertexFormat({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} }), &sphereLightPositions[0], sphereLightPositions.size() * sizeof(vec4), sizeof(vec4));
	}

	if (coneLightPosBuffer.isGenerated())
	{
		coneLightPosBuffer.create(&coneLightPositions[0], coneLightPositions.size() * sizeof(vec4));
		vaoConeLights.create(VertexFormat({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} }), &coneLightPositions[0], coneLightPositions.size() * sizeof(vec4), sizeof(vec4));
	}

	if (lightVelBuffer.isGenerated())
		lightVelBuffer.create(&lightVectors[0], lightVectors.size() * sizeof(vec4));
}

void App::drawLightParticles()
{
	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	particleTex.bind();
	particleShader.bind();

	particleShader.setUniform("LightColors", &lightColorBuffer);
	particleShader.setUniform("LightVelocities", &lightVelBuffer);
	particleShader.setUniform("mvMatrix", camera.getInverse());
	particleShader.setUniform("pMatrix", camera.getProjection());
	particleShader.setUniform("particleTex", &particleTex);

	particleShader.setUniform("idOffset", 0);
	vaoSphereLights.render();
	particleShader.setUniform("idOffset", (int) sphereLightPositions.size());
	vaoConeLights.render();

	particleShader.unbind();
	particleTex.unbind();

	glPopAttrib();
}



void App::init()
{
	auto &camera = Renderer::instance().getActiveCamera();
	(void) (camera);

#if 1
	// Rungholt camera view.
	camera.setPos({-1.06448f,-1.54686f,-1.21867f});
	camera.setEulerRot({-0.506145f,2.06821f,0});
	camera.setZoom(11.5824f);
	camera.update(0.1f);
#endif

# if 0
	// Another Rungholt camera view.
	camera.setPos({-0.5f, -0.8f, 0.0f});
	camera.setEulerRot({-0.401425f, 2.24275f, 0.0f});
	camera.setZoom(10.68f);
	camera.update(0.1f);
#endif

#if 0
	#if 1
		camera.setZoom(6.68847f);
		camera.setEulerRot(vec3(-0.209439f, 0, 0));
	#else
		camera.setZoom(6.97035);
		camera.setEulerRot(vec3(-0.0523594,0.855212,0));
	#endif
#endif

#if 0
	// Animating around the atrium.
	auto &interpolator = anim.getInterpolator(anim.addInterpolator(const_cast<vec3*>(&camera.getEulerRot()), 10, vec3(-0.0523594,0.855212,0)));
	interpolator.addKeyFrame(0, vec3(-0.0523594,0.855212,0));
	interpolator.addKeyFrame(10, vec3(-0.061086,-0.715585,0));
#endif

#if 0
	// Animated flythrough of rungholt.
	vec3 rungholtPos(4.45782f,0.292915f,-0.91246f);
	vec3 rungholtRot(0, 1.5708f, 0);
	float rungholtZoom = 0.015f;

	camera.setPos(rungholtPos);
	camera.setEulerRot(rungholtRot);
	camera.setZoom(rungholtZoom);
	camera.update(0.1f);

	auto &posInterp = anim.getInterpolator(anim.addInterpolator(const_cast<vec3*>(&camera.getPos()), 12, rungholtPos));
	auto &rotInterp = anim.getInterpolator(anim.addInterpolator(const_cast<vec3*>(&camera.getEulerRot()), 12, rungholtRot));

	// About 5 keyframes at 2 seconds each?
	posInterp.addKeyFrame(0, rungholtPos);
	rotInterp.addKeyFrame(0, rungholtRot);

	posInterp.addKeyFrame(2, vec3(0.992661f,0.292915f,-0.912451f));
	rotInterp.addKeyFrame(2, vec3(0,1.5708f,0));

	posInterp.addKeyFrame(4, vec3(0.299918,0.29815,-1.86604));
	rotInterp.addKeyFrame(4, vec3(-0.00872665,2.2253,0));

	posInterp.addKeyFrame(6, vec3(0.884898,1.43195,-3.56746));
	rotInterp.addKeyFrame(6, vec3(-0.497417,2.522,0));

	posInterp.addKeyFrame(8, vec3(0.879582,2.34563,-5.41952));
	rotInterp.addKeyFrame(8, vec3(-0.479964,2.94088,0));

	//posInterp.addKeyFrame(10, vec3(-0.677191,2.4267,-4.95954));
	//rotInterp.addKeyFrame(10, vec3(-0.305431,2.49582,0));

	posInterp.addKeyFrame(12, vec3(2.24625,6.20886,-8.2623));
	rotInterp.addKeyFrame(12, vec3(-0.584684,2.54819,0));

	//anim.setProfiler(&profiler);
	//anim.addProfilerTime("Light Capture");
	//anim.setCSVFilename("animTimes.csv");
#endif

#if 0
	// In case you want to save the animation I guess.
	anim.setAnimFilename("animations/anim.jpg");
#endif

	// TODO: Get rid of this!
	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");

	int maxLayers = 0;
	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxLayers);
	std::cout << "Max texture layers: " << maxLayers << "\n";

#if TEST_EYESPACE_CONVERSION
	// Just in case eye space conversion needs testing...
	testEyeSpace(Renderer::instance().getActiveCamera().getProjection(), Renderer::instance().getActiveCamera().getInverse());
	testConservativeDepth(Renderer::instance().getActiveCamera().getProjection(), Renderer::instance().getActiveCamera().getInverse(), Renderer::instance().getActiveCamera().getTransform().getMat3().transpose());
	testConeRotation();
#endif

#if TEST_LIGHT
	createTestLight();
#endif
#if TEAPOT_LIGHTS
	createLightsForTeapot();
#endif
#if RANDOM_LIGHTS
	createLightsRandom(INIT_SPHERE_LIGHTS, INIT_CONE_LIGHTS);
#endif
#if GEO_LIGHTS
	// No geometry loaded yet, throw in random lights.
	createLightsRandom(INIT_SPHERE_LIGHTS, INIT_CONE_LIGHTS);
#endif
	createLightBuffers();

	setLightVelocities(LIGHT_SPEED);

#if 0
	lightTypes = {Oit::VISUALISER, Oit::TILED_CLUSTER_FORWARD, Oit::TILED_CLUSTER_DEFERRED,
		Oit::FORWARD_PLUS_FORWARD, Oit::FORWARD_PLUS_DEFERRED, Oit::LIGHT_LINK_LIST_FORWARD, Oit::LIGHT_LINK_LIST_DEFERRED,
		Oit::PRACTICAL_CLUSTER_FORWARD, Oit::PRACTICAL_CLUSTER_DEFERRED, Oit::RAY_CLUSTER_FORWARD, Oit::RAY_CLUSTER_DEFERRED, Oit::DEFERRED};
#else
	lightTypes = {Oit::VISUALISER, Oit::TILED_CLUSTER_FORWARD, Oit::TILED_CLUSTER_DEFERRED,
		Oit::FORWARD_PLUS_FORWARD, Oit::FORWARD_PLUS_DEFERRED, Oit::LIGHT_LINK_LIST_FORWARD, Oit::LIGHT_LINK_LIST_DEFERRED,
		Oit::PRACTICAL_CLUSTER_FORWARD, Oit::PRACTICAL_CLUSTER_DEFERRED, Oit::RAY_CLUSTER_FORWARD, Oit::RAY_CLUSTER_DEFERRED,
		Oit::RAY_CLUSTER_LLI_FORWARD, Oit::RAY_CLUSTER_LLI_DEFERRED,
		Oit::RAY_CLUSTER_LAI_FORWARD, Oit::RAY_CLUSTER_LAI_DEFERRED,
		Oit::RAY_CLUSTER_LLF_FORWARD, Oit::RAY_CLUSTER_LLF_DEFERRED,
		Oit::RAY_CLUSTER_LAF_FORWARD, Oit::RAY_CLUSTER_LAF_DEFERRED,
		Oit::DEFERRED};
#endif

	//useNextLFB();
	//cycleLighting();
	useLighting(Oit::VISUALISER);
	loadNextMesh();

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glColor3f(1, 1, 1);

	auto vertBasic = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto geomBasic = ShaderSourceCache::getShader("basicGeom").loadFromFile("shaders/basic.geom");
	auto fragBasic = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.create(&vertBasic, &fragBasic, &geomBasic);

	auto vertPart = ShaderSourceCache::getShader("partVert").loadFromFile("shaders/particle/particle.vert");
	auto geomPart = ShaderSourceCache::getShader("partGeom").loadFromFile("shaders/particle/particle.geom");
	auto fragPart = ShaderSourceCache::getShader("partFrag").loadFromFile("shaders/particle/particle.frag");
	particleShader.create(&vertPart, &fragPart, &geomPart);

	Rendering::Image img;
	Resources::ImageLoader::load(&img, "images/particle.png");
	particleTex.create(img.getData(), img.getFormat(), img.getDimensions().x, img.getDimensions().y);

	//std::cout << basicShader.getBinary() << "\n";
}

void App::setTmpViewport(int width, int height)
{
	auto &camera = Renderer::instance().getActiveCamera();

	tmpWidth = camera.getWidth();
	tmpHeight = camera.getHeight();

	camera.setViewport(0, 0, width, height);
	camera.uploadViewport();
	camera.update(0);
}

void App::restoreViewport()
{
	auto &camera = Renderer::instance().getActiveCamera();

	camera.setViewport(0, 0, tmpWidth, tmpHeight);
	camera.uploadViewport();
	camera.update(0);
}

void App::setLightUniforms(Shader *shader)
{
	shader->setUniform("SphereLightPositions", &sphereLightPosBuffer);
	shader->setUniform("ConeLightPositions", &coneLightPosBuffer);

	shader->setUniform("SphereLightRadii", &sphereLightRadiusBuffer);
	shader->setUniform("ConeLightDist", &coneLightDistBuffer);
	shader->setUniform("ConeLightDir", &coneLightDirBuffer);

	shader->setUniform("LightColors", &lightColorBuffer);
	shader->setUniform("nSphereLights", (unsigned int) sphereLightPositions.size());
	shader->setUniform("nConeLights", (unsigned int) coneLightPositions.size());
}

void App::setCameraUniforms(Shader *shader)
{
	auto &camera = Renderer::instance().getActiveCamera();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	shader->setUniform("viewport", camera.getViewport());
	shader->setUniform("invPMatrix", camera.getInverseProj());
}

void App::captureGeometry(LFB *lightLFB, Cluster *lightCluster, Shader *shader, bool lightUniforms)
{
	if (!shader)
		shader = &captureGeometryShader;

	profiler.start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	if (opaqueFlag == 0)
		geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && geometryLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags();
		countShader.bind();
		geometryLFB.setCountUniforms(&countShader);
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh.render(false);
		countShader.unbind();
		geometryLFB.endCountFrags();
	}

	if (opaqueFlag == 0)
		geometryLFB.beginCapture();
	/*else
	{
		glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}*/

	shader->bind();

#if 0
	if (opaqueFlag == 1)
		mask.getDepthTexture()->bind();
#endif

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	if (opaqueFlag == 0)
		geometryLFB.setUniforms(shader);

#if 0
	shader->setUniform("opaqueFlag", opaqueFlag);
	if (opaqueFlag == 1)
		shader->setUniform("depthTexture", mask.getDepthTexture());
#endif

	if (lightUniforms)
		setLightUniforms(shader);

	// Capture fragments into the lfb.
	if (lightLFB)
		lightLFB->setUniforms(shader, false);
	if (lightCluster)
		lightCluster->setUniforms(shader, false);
	gpuMesh.render(); // FIXME: Figure out why lightUniforms bool causes deferred rendering problems.
	shader->unbind();

	//if (opaqueFlag == 1)
	//	glPopAttrib();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && geometryLFB.endCapture())
	{
		geometryLFB.beginCapture();
		shader->bind();
		shader->setUniform("mvMatrix", camera.getInverse());
		shader->setUniform("pMatrix", camera.getProjection());
		shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		geometryLFB.setUniforms(shader);
		if (lightLFB)
			lightLFB->setUniforms(shader, false);
		if (lightCluster)
			lightCluster->setUniforms(shader, false);
		gpuMesh.render(lightUniforms);
		shader->unbind();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

#if 0
	if (opaqueFlag == 1)
		mask.getDepthTexture()->unbind();
#endif

	// TODO: Profile memory usage perhaps?

	profiler.time("Geometry Capture");
}

void App::sortGeometry(bool composite)
{
	if (opaqueFlag == 1)
		return;

	profiler.start("Geometry Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	if (composite)
	{
		geometryLocalBMA.createMask(&geometryLFB);
		geometryLocalBMA.sort(&geometryLFB);
	}
	else
	{
		geometryBMA.createMask(&geometryLFB);
		geometryBMA.sort(&geometryLFB);
	}
#else
	// Sort and composite normally.
	if (composite)
	{
		sortGeometryLocalShader.bind();
		geometryLFB.composite(&sortGeometryLocalShader); // TODO: change this to a sort method.
		sortGeometryLocalShader.unbind();
	}
	else
	{
		sortGeometryShader.bind();
		geometryLFB.composite(&sortGeometryShader); // TODO: change this to a sort method.
		sortGeometryShader.unbind();
	}
#endif

	profiler.time("Geometry Sort");
}

void App::captureGeometryAndParticles(LFB *lightLFB, Cluster *lightCluster, Shader *shader, bool lightUniforms)
{
	if (!shader)
		shader = &captureGeometryShader;

	auto *partShader = &captureParticleShader;

	profiler.start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	if (opaqueFlag == 0)
		geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && geometryLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags();
		countShader.bind();
		geometryLFB.setCountUniforms(&countShader);
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh.render(false);
		countShader.unbind();
		// TODO: Now render the particles.
		geometryLFB.endCountFrags();
	}

	if (opaqueFlag == 0)
		geometryLFB.beginCapture();

	shader->bind();

#if 0
	if (opaqueFlag == 1)
		mask.getDepthTexture()->bind();
#endif

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	if (opaqueFlag == 0)
		geometryLFB.setUniforms(shader);

#if 0
	shader->setUniform("opaqueFlag", opaqueFlag);
	if (opaqueFlag == 1)
		shader->setUniform("depthTexture", mask.getDepthTexture());
#endif

	if (lightUniforms)
		setLightUniforms(shader);

	// Capture fragments into the lfb.
	if (lightLFB)
		lightLFB->setUniforms(shader, false);
	if (lightCluster)
		lightCluster->setUniforms(shader, false);
	gpuMesh.render();
	shader->unbind();

	// Now render the particles.
	partShader->bind();
	if (opaqueFlag == 0)
		geometryLFB.setUniforms(partShader);
	particleTex.bind();

	partShader->setUniform("LightColors", &lightColorBuffer);
	partShader->setUniform("LightVelocities", &lightVelBuffer);
	partShader->setUniform("mvMatrix", camera.getInverse());
	partShader->setUniform("pMatrix", camera.getProjection());
	partShader->setUniform("particleTex", &particleTex);

	partShader->setUniform("idOffset", 0);
	vaoSphereLights.render();
	partShader->setUniform("idOffset", (int) sphereLightPositions.size());
	vaoConeLights.render();

	particleTex.unbind();
	partShader->unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && geometryLFB.endCapture())
	{
		geometryLFB.beginCapture();
		shader->bind();
		shader->setUniform("mvMatrix", camera.getInverse());
		shader->setUniform("pMatrix", camera.getProjection());
		shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		geometryLFB.setUniforms(shader);
		if (lightLFB)
			lightLFB->setUniforms(shader, false);
		if (lightCluster)
			lightCluster->setUniforms(shader, false);
		gpuMesh.render(lightUniforms);
		shader->unbind();

		// Now render the particles.
		partShader->bind();
		geometryLFB.setUniforms(partShader);
		particleTex.bind();

		partShader->setUniform("LightColors", &lightColorBuffer);
		partShader->setUniform("LightVelocities", &lightVelBuffer);
		partShader->setUniform("mvMatrix", camera.getInverse());
		partShader->setUniform("pMatrix", camera.getProjection());
		partShader->setUniform("particleTex", &particleTex);

		partShader->setUniform("idOffset", 0);
		vaoSphereLights.render();
		partShader->setUniform("idOffset", (int) sphereLightPositions.size());
		vaoConeLights.render();

		particleTex.unbind();
		partShader->unbind();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

#if 0
	if (opaqueFlag == 1)
		mask.getDepthTexture()->unbind();
#endif

	// TODO: Profile memory usage perhaps?

	profiler.time("Geometry Capture");
}

void App::createDepthMask(Texture2D *depthTexture)
{
	profiler.start("Cluster Mask");

	if (depthTexture)
	{
		mask.createMaskFromDepths(depthTexture);
		profiler.time("Cluster Mask");
		return;
	}

	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	//auto &camera = Renderer::instance().getActiveCamera();
	//glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
	//setTmpViewport(camera.getWidth() / GRID_CELL_SIZE, camera.getHeight() / GRID_CELL_SIZE);

	mask.beginCreateMask();

	// Render geometry.
	gpuMesh.render(false);
	
	mask.endCreateMask();

	//glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
	//restoreViewport();

	if (opaqueFlag == 1)
		glPopAttrib();

	profiler.time("Cluster Mask");
}

void App::render()
{
	static float fpsUpdate = 0;

#if 0
	testFont.blit();
	return;
#endif

	profiler.begin();

	profiler.start("Total");

	// TODO: Render particles for all the lights... but what's the best way?
	// Wait a minute, I need these particles to be transparent... deep image or additive blending?

	if (currOit)
	{
		currOit->render(this);
	}
	else
	{
		auto &camera = Renderer::instance().getActiveCamera();

		// Opaque Rendering.
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		basicShader.bind();
		basicShader.setUniform("mvMatrix", camera.getInverse());
		basicShader.setUniform("pMatrix", camera.getProjection());
		basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		basicShader.setUniform("lightDir", vec3(0, 0, 1));
		gpuMesh.render();
		basicShader.unbind();

		glPopAttrib();
	}

	//drawLightParticles();

	profiler.time("Total");

#if 1
	float total = profiler.getTime("Total");
	float smoothing = 0.9f; // Larger => more smoothing.
	tpf = (tpf * smoothing) + (total * (1.0f - smoothing));
	float t = round(total);
	#if 0
		testFont.renderFromAtlas("Hybrid Lighting\nTime per frame(ms): " + Utils::toString(t) +
			"\nFrames per second (fps): " + Utils::toString(1000.0 / t) +
			"\nLights: " + Utils::toString(lightColors.size()), 10, 10, 0, 0, 1);
	#else
		testFont.renderFromAtlas("Technique: " + currLightStr +
			"\nTime per frame (ms): " + Utils::toString(t) +
			"\nFrames per second (fps): " + Utils::toString(1000.0 / t) +
			"\nLights: " + Utils::toString(lightColors.size()), 10, 10, 0, 0, 0, 1);
	#endif
#endif

	float dt = Platform::Window::getDeltaTime();
	fpsUpdate += dt;
	if (fpsUpdate > 2)
	{
		profiler.print();
		fpsUpdate = 0;
	}

#if DRAW_AXES
	Renderer::instance().drawAxes();
	Renderer::instance().drawAxes(Renderer::instance().getActiveCamera().getPos(), 2);
#endif
}

void App::update(float dt)
{
	if (anim.isAnimating())
	{
		dt = anim.update(dt);
		Renderer::instance().getActiveCamera().setDirty();
	}

	// Animate the lights as a particle system, have them bounce around when they hit the bounds.
#if ANIMATE_LIGHTS
	if (anim.isAnimating() && dt > 0)
		updateLightPositions(dt);
	else
		updateLightPositions(std::min(0.2f, dt));
#endif

	currOit->update(dt);
}

void App::runBenchmarks()
{
	Benchmark benchmark(&profiler, [this]() -> void { this->render(); } );
#define USE_FBO 0
#if USE_FBO
	int fboWidth = 4096, fboHeight = 2160;
	Texture2D colorBuffer;
	RenderBuffer depthBuffer;
	FrameBuffer fbo(fboWidth, fboHeight);
	fbo.create(GL_RGBA, &colorBuffer, &depthBuffer, true);
#endif

	// Set camera view for benchmark.
	/*auto &camera = Renderer::instance().getActiveCamera();
	camera.setPos({-0.5f, -0.8f, 0.0f});
	camera.setEulerRot({-0.209439,0,0});
	camera.setZoom(6.68847f);
	camera.update(0.1f);*/

#if 0
	std::vector<std::string> times = {"Create Frustums", "Cluster Mask", "Geometry Capture", "Geometry Sort", "Light Capture", "Light Cluster", "Light Depth Clear", "Light Draw", "Light List", "Total"};
	std::vector<std::string> data = {"lfbCounts", "lfbData", "lfbHeadPtrs", "lfbNextPtrs", "clusterData", "clusterHeadPtrs", "clusterNextPtrs", "clusterMasks", "lightDepths", "total"};
#else
	//std::vector<std::string> times = {"Cluster Count", "Cluster Mask", "Cluster compact", "Cluster prefix", "Create Frustums", "Geometry Capture", "Light Capture", "Light Depth Clear", "Light Draw", "Light Deferred", "Total"};
	//std::vector<std::string> data = {"clusterData", "clusterHeadPtrs", "clusterIndices", "clusterMasks", "clusterNextPtrs", "clusterOffsets", "clusterCounts", "lfbCounts", "lfbData", "lfbHeadPtrs", "lfbNextPtrs", "lightDepths", "g-buffer"};
	std::vector<std::string> times = {"Total", "Geometry Capture", "Light Capture", "Light Deferred"};
	std::vector<std::string> data = {"clusterTotalData", "clusterData", "clusterHeadPtrs", "clusterIndices", "clusterMasks", "clusterNextPtrs", "clusterOffsets", "clusterCounts", "lfbCounts", "lfbData", "lfbHeadPtrs", "lfbNextPtrs", "lightDepths", "g-buffer"};
#endif

	// Opaque distributions.
	size_t largeLightsMin = 32, largeLightsMax = 256, largeLightsStep = 16;
	size_t medLightsMin = 2048, medLightsMax = 32768, medLightsStep = 2048;
#if GEO_LIGHTS
	size_t smallLightsMin = 4096, smallLightsMax = 65536, smallLightsStep = 4096; // Lights on geometry.
#else
	size_t smallLightsMin = 128, smallLightsMax = 2097152, smallLightsStep = 131064; // Random lights.
#endif
	size_t mixedLightsMin = 4096, mixedLightsMax = 16384, mixedLightsStep = 2048;

	// Transparent distributions.
	if (opaqueFlag == 0)
	{
		largeLightsMin = 32; largeLightsMax = 256; largeLightsStep = 16;
		medLightsMin = 2048; medLightsMax = 32768; medLightsStep = 2048;
		smallLightsMin = 8192; smallLightsMax = 524288; smallLightsStep = 24576;
		mixedLightsMin = 2048; mixedLightsMax = 16384; mixedLightsStep = 2048;
	}

	(void) largeLightsMin; (void) largeLightsMax; (void) largeLightsStep;
	(void) medLightsMin; (void) medLightsMax; (void) medLightsStep;
	(void) smallLightsMin; (void) smallLightsMax; (void) smallLightsStep;
	(void) mixedLightsMin; (void) mixedLightsMax; (void) mixedLightsStep;

	size_t lightsMin = 0, lightsMax = 0, lightsStep = 0;

#if LARGE_LIGHTS
	lightsMin = largeLightsMin;
	lightsMax = largeLightsMax;
	lightsStep = largeLightsStep;
#elif MEDIUM_LIGHTS
	lightsMin = medLightsMin;
	lightsMax = medLightsMax;
	lightsStep = medLightsStep;
#elif SMALL_LIGHTS
	lightsMin = smallLightsMin;
	lightsMax = smallLightsMax;
	lightsStep = smallLightsStep;
#elif MIXED_LIGHTS
	lightsMin = mixedLightsMin;
	lightsMax = mixedLightsMax;
	lightsStep = mixedLightsStep;
#endif

	// Check for both linear and link list, against different numbers of lights and for opaque/transparent geometry.
	size_t nLights = lightsMin;

	float testTime = 10;

	std::string geoType = opaqueFlag == 0 ? "Transparent " : "Opaque ";
	int j = opaqueFlag;

#if 0
	// Atrium stuff, benchmarks for forward and deferred rendering, transparent and opaque.
	// Check different numbers of lights.
	for (; nLights <= lightsMax; nLights += lightsStep)
	{
		// Check linear and link list (unless opaque).
		for (int i = 1; i <= 1; i++)
		{
			std::string lfbType = i == 0 ? "Linear " : "Link List ";

			benchmark.addTest(geoType + lfbType + "DS " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					this->useLighting(Oit::DEFERRED);
					setNLights(nLights / 2, nLights / 2);
				}, testTime, times, data);
		#if 0
			benchmark.addTest(geoType + lfbType + "AT Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::FORWARD_PLUS_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "AT Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::FORWARD_PLUS_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "RT Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::LIGHT_LINK_LIST_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "RT Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::LIGHT_LINK_LIST_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "RC Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::PRACTICAL_CLUSTER_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "RC Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::PRACTICAL_CLUSTER_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "AC Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::TILED_CLUSTER_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "AC Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::TILED_CLUSTER_DEFERRED);
				}, testTime, times, data);
		#endif
		#if 0
			benchmark.addTest(geoType + lfbType + "HL Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_FORWARD);
				}, testTime, times, data);
			if (opaqueFlag == 1)
			{
				benchmark.addTest(geoType + lfbType + "HL Deferred " + Utils::toString(nLights) + " lights",
					[this, i, j, nLights]() -> void
					{
						this->currLFB = i;
						setNLights(nLights / 2, nLights / 2);
						this->useLighting(Oit::RAY_CLUSTER_DEFERRED);
					}, testTime, times, data);
			}
		#else
			benchmark.addTest(geoType + lfbType + "HL-LL-I Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LLI_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "HL-LL-I Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LLI_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "HL-LA-I Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LAI_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "HL-LA-I Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LAI_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "HL-LL-F Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LLF_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "HL-LL-F Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LLF_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "HL-LA-F Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LAF_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "HL-LA-F Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LAF_DEFERRED);
				}, testTime, times, data);
		#endif
		}
	}
#else
	// Rungholt stuff, only deferred and no transparency.
	for (; nLights <= lightsMax; nLights += lightsStep)
	{
		// Check linear and link list (unless opaque).
		for (int i = 1; i <= 1; i++)
		{
			std::string lfbType = i == 0 ? "Linear " : "Link List ";

			benchmark.addTest(geoType + lfbType + "DS " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					this->useLighting(Oit::DEFERRED);
					setNLights(nLights / 2, nLights / 2);
				}, testTime, times, data);

			/*benchmark.addTest(geoType + lfbType + "AT Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::FORWARD_PLUS_DEFERRED);
				}, testTime, times, data);*/

			/*benchmark.addTest(geoType + lfbType + "RT Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::LIGHT_LINK_LIST_DEFERRED);
				}, testTime, times, data);*/

			/*benchmark.addTest(geoType + lfbType + "RC Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::PRACTICAL_CLUSTER_DEFERRED);
				}, testTime, times, data);*/

			/*benchmark.addTest(geoType + lfbType + "AC Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::TILED_CLUSTER_DEFERRED);
				}, testTime, times, data);*/
		#if 0
			benchmark.addTest(geoType + lfbType + "HL Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_FORWARD);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "HL Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_DEFERRED);
				}, testTime, times, data);
		#else
			benchmark.addTest(geoType + lfbType + "HL-LL-I Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LLI_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "HL-LL-I Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LLI_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "HL-LA-I Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LAI_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "HL-LA-I Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LAI_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "HL-LL-F Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LLF_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "HL-LL-F Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LLF_DEFERRED);
				}, testTime, times, data);

			benchmark.addTest(geoType + lfbType + "HL-LA-F Forward " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LAF_FORWARD);
				}, testTime, times, data);
			benchmark.addTest(geoType + lfbType + "HL-LA-F Deferred " + Utils::toString(nLights) + " lights",
				[this, i, j, nLights]() -> void
				{
					this->currLFB = i;
					setNLights(nLights / 2, nLights / 2);
					this->useLighting(Oit::RAY_CLUSTER_LAF_DEFERRED);
				}, testTime, times, data);
		#endif
		}
	}
#endif

	// TODO: May need to try using link lists for the light lfb, to compare with linearized light lfb.

#if USE_FBO
	benchmark.setRenderFunc([this, &fbo]() -> void
#else
	benchmark.setRenderFunc([this]() -> void
#endif
	{
	#if USE_FBO
		fbo.bind();
		Renderer::instance().getActiveCamera().setViewport(0, 0, 4096, 2160);
	#else
		Renderer::instance().getActiveCamera().setViewport(0, 0, Platform::Window::getWidth(), Platform::Window::getHeight());
	#endif
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glLoadIdentity();
		Renderer::instance().getActiveCamera().update(1.0f);
		Renderer::instance().getActiveCamera().upload();
		this->render();
	#if USE_FBO
		fbo.unbind();
		fbo.blit();
	#endif
	});

	benchmark.run();
	benchmark.print();

	// Append data+time to the title, and save to a benchmarks folder.
	time_t t = time(0);
	struct tm buf;
	#if _WIN32
		localtime_s(&buf, &t);
	#else
		localtime_r(&t, &buf);
	#endif
	std::stringstream ss;
	ss << std::put_time(&buf, "-%d-%m-%Y-%H-%M");
	benchmark.writeCSV("benchmarks/benchmark" + ss.str() + ".csv", times, data);

	exit(0);
}

void App::useMesh()
{
	static std::vector<std::string> meshes = { "meshes/galleon.obj", "meshes/sponza/sponza.3ds", "meshes/rungholt/rungholt.obj", "meshes/teapot/teapot.obj", "meshes/powerplant/powerplant.ctm", "meshes/hairball.ctm" };

	if (currMesh > (int) meshes.size() - 1)
		currMesh = 0;
	else if (currMesh < 0)
		currMesh = (int) meshes.size() - 1;

	gpuMesh.release();

	std::cout << "Loading mesh: " << meshes[currMesh] << "\n";

	//Mesh mesh;
	mesh.clear();
	if (Resources::MeshLoader::load(&mesh, meshes[currMesh]))
	{
		if (meshes[currMesh] == "meshes/powerplant/powerplant.ctm")
			mesh.scale(vec3(0.0001f, 0.0001f, 0.0001f));
		else if (meshes[currMesh] == "meshes/rungholt/rungholt.obj")
			mesh.scale(vec3(0.04f, 0.04f, 0.04f));
		else if (meshes[currMesh] == "meshes/hairball.ctm")
			mesh.scale(vec3(-10, -10, -10), vec3(10, 10, 10));
		else if (meshes[currMesh] == "meshes/sponza/sponza.3ds")
			mesh.scale(vec3(-10, -5, -10), vec3(10.0f, 5.0f, 10.0f));
		else if (meshes[currMesh] == "meshes/galleon.obj")
			mesh.scale(vec3(0.2, 0.2, 0.2));
		else if (meshes[currMesh] == "meshes/teapot/teapot.obj")
			mesh.scale(vec3(2.0, 2.0, 2.0));

	#if GEO_LIGHTS
		// If we're using lights attached to geometry, then create them here.
		createLightsOnGeometry(mesh, sphereLightPositions.size(), coneLightPositions.size());
		createLightBuffers();
	#endif

		gpuMesh.create(&mesh);
	}
}

void App::useLFB()
{
	profiler.clear();

#if USE_LINK_PAGES
	static std::vector<LFB::LFBType> types = { LFB::LINK_PAGES, LFB::LINK_LIST, LFB::LINEARIZED };
	static std::vector<std::string> typeStrs = { "Link Pages", "Link List", "Linearized" };
#else
	#if 1
		static std::vector<LFB::LFBType> types = { LFB::LINEARIZED, LFB::LINK_LIST };
		static std::vector<std::string> typeStrs = { "Linearized", "Link List" };
	#else
		static std::vector<LFB::LFBType> types = { LFB::LINK_LIST, LFB::LINEARIZED };
		static std::vector<std::string> typeStrs = { "Link List", "Linearized" };
	#endif
#endif

	geometryLFB.release();

	delete currOit;
	currOit = nullptr;

	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();

	if (currLFB > (int) types.size() - 1)
		currLFB = 0;
	else if (currLFB < 0)
		currLFB = (int) types.size() - 1;
	LFB::LFBType type = types[currLFB];

	std::cout << "Point lights: " << sphereLightPositions.size() << ", Spotlights: " << coneLightPositions.size() << "\n";
	std::cout << "Using LFB: " << typeStrs[currLFB] << "\n";

	if (opaqueFlag == 0)
	{
		geometryLFB.setType(type, "lfb");
		geometryLFB.setMaxFrags(MAX_FRAGS);
		// geometryLFB.setProfiler(&profiler); // We're interested in memory usage of lighting, not geometry.
	}

	auto vertCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/oit/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/oit/capture.frag");
	fragCapture.setDefine("DIRECTIONAL_LIGHT", Utils::toString(DIRECTIONAL_LIGHT));
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);

	auto vertPart = ShaderSourceCache::getShader("captureVertPart").loadFromFile("shaders/particle/particle.vert");
	auto geomPart = ShaderSourceCache::getShader("captureGeomPart").loadFromFile("shaders/particle/particle.geom");
	auto fragPart = ShaderSourceCache::getShader("captureFragPart").loadFromFile("shaders/particle/particle.frag");
	fragPart.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragPart.setDefine("CAPTURE_TO_LFB", "1");

	captureParticleShader.release();
	captureParticleShader.create(&vertPart, &fragPart, &geomPart);

	if (opaqueFlag == 0)
	{
		// Sorts and writes to global memory.
		auto vertSort = ShaderSourceCache::getShader("sortVert").loadFromFile("shaders/sort/sort.vert");
		auto fragSort = ShaderSourceCache::getShader("sortFrag").loadFromFile("shaders/sort/sort.frag");
		fragSort.setDefine("MAX_FRAGS", Utils::toString(geometryLFB.getMaxFrags()));
		fragSort.setDefine("COMPOSITE_LOCAL", "0");
		fragSort.replace("LFB_NAME", "lfb");

		sortGeometryShader.release();
		sortGeometryShader.create(&vertSort, &fragSort);

		// Sorts and composites.
		auto vertLocalSort = ShaderSourceCache::getShader("sortLocalVert").loadFromFile("shaders/sort/sort.vert");
		auto fragLocalSort = ShaderSourceCache::getShader("sortLocalFrag").loadFromFile("shaders/sort/sort.frag");
		fragLocalSort.setDefine("MAX_FRAGS", Utils::toString(geometryLFB.getMaxFrags()));
		fragLocalSort.setDefine("COMPOSITE_LOCAL", "1");
		fragLocalSort.replace("LFB_NAME", "lfb");

		sortGeometryLocalShader.release();
		sortGeometryLocalShader.create(&vertLocalSort, &fragLocalSort);

		// BMA shaders that write to global memory.
		geometryBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
		geometryBMA.createShaders(&geometryLFB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "0"}});

		// BMA shaders that sort and composite.
		geometryLocalBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
		geometryLocalBMA.createShaders(&geometryLFB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}});
	}

	auto &camera = Renderer::instance().getActiveCamera();
	mask.setProfiler(&profiler);
	mask.regen(CLUSTERS, MAX_EYE_Z, GRID_CELL_SIZE);
	mask.resize((int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE), (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE));
	mask.useDepthPrePass(opaqueFlag == 1);

	switch (lightType)
	{
	case Oit::BRUTE_FORCE:
		currOit = new BruteForce(opaqueFlag);
		break;
#if 0
	case Oit::FORWARD_UNSORTED:
		currOit = new Unsorted(opaqueFlag);
		break;

	case Oit::FORWARD_UNSORTED_GRID:
		currOit = new UnsortedGrid(opaqueFlag);
		break;

	case Oit::DEFERRED_VOLUME:
		currOit = new Volume();
		break;

	case Oit::DEFERRED_VOLUME_GRID:
		currOit = new VolumeGrid();
		break;

	case Oit::DEFERRED_VOLUME_LOCAL:
		currOit = new VolumeLocal();
		break;

	case Oit::DEFERRED_VOLUME_LOCAL_GRID:
		currOit = new VolumeLocalGrid();
		break;

	case Oit::LIGHT_LINK_LIST:
		currOit = new LightLinkList();
		break;

	case Oit::LIGHT_LINK_LIST_SEP:
		currOit = new LightLinkListSep();
		break;
#endif
	case Oit::PRACTICAL_CLUSTER_FORWARD:
		currOit = new PracticalCluster(opaqueFlag, false);
		break;

	case Oit::PRACTICAL_CLUSTER_DEFERRED:
		currOit = new PracticalCluster(opaqueFlag, true);
		break;
#if 0
	case Oit::VOLUME_CLUSTER_LL:
		currOit = new VolumeClusterLL();
		break;

	case Oit::VOLUME_CLUSTER_L:
		currOit = new VolumeClusterL();
		break;
#endif
	case Oit::RAY_CLUSTER_FORWARD:
		currOit = new RayCluster(opaqueFlag, false);
		break;

	case Oit::RAY_CLUSTER_DEFERRED:
		currOit = new RayCluster(opaqueFlag, true);
		break;

	case Oit::RAY_CLUSTER_LLI_FORWARD:
		currOit = new RayClusterLLI(opaqueFlag, false);
		break;
	case Oit::RAY_CLUSTER_LLI_DEFERRED:
		currOit = new RayClusterLLI(opaqueFlag, true);
		break;
	case Oit::RAY_CLUSTER_LAI_FORWARD:
		currOit = new RayClusterLAI(opaqueFlag, false);
		break;
	case Oit::RAY_CLUSTER_LAI_DEFERRED:
		currOit = new RayClusterLAI(opaqueFlag, true);
		break;
	case Oit::RAY_CLUSTER_LLF_FORWARD:
		currOit = new RayClusterLLF(opaqueFlag, false);
		break;
	case Oit::RAY_CLUSTER_LLF_DEFERRED:
		currOit = new RayClusterLLF(opaqueFlag, true);
		break;
	case Oit::RAY_CLUSTER_LAF_FORWARD:
		currOit = new RayClusterLAF(opaqueFlag, false);
		break;
	case Oit::RAY_CLUSTER_LAF_DEFERRED:
		currOit = new RayClusterLAF(opaqueFlag, true);
		break;

	case Oit::FORWARD_PLUS_FORWARD:
		currOit = new ForwardPlus(opaqueFlag, false);
		break;

	case Oit::FORWARD_PLUS_DEFERRED:
		currOit = new ForwardPlus(opaqueFlag, true);
		break;

	case Oit::TILED_CLUSTER_FORWARD:
		currOit = new TiledCluster(opaqueFlag, false);
		break;

	case Oit::TILED_CLUSTER_DEFERRED:
		currOit = new TiledCluster(opaqueFlag, true);
		break;

	case Oit::LIGHT_LINK_LIST_FORWARD:
		currOit = new LightLinkListSep(opaqueFlag, false);
		break;

	case Oit::LIGHT_LINK_LIST_DEFERRED:
		currOit = new LightLinkListSep(opaqueFlag, true);
		break;

	case Oit::DEFERRED:
		currOit = new Deferred(opaqueFlag);
		break;

	case Oit::VISUALISER:
		currOit = new Visualiser(opaqueFlag);
		break;

	default:
		currOit = new BruteForce(opaqueFlag);
		break;
	}

	if (currOit)
	{
		currOit->init();
		currOit->useLFB(this, type);
	}
}

void App::reloadShaders()
{
	std::cout << "Reloading shaders\n";
	if (currOit)
		currOit->reloadShaders(this);
}

void App::toggleTransparency()
{
	opaqueFlag = opaqueFlag == 0 ? 1 : 0;
	useLFB();
}

void App::loadNextMesh()
{
	currMesh++;
	useMesh();
}

void App::loadPrevMesh()
{
	currMesh--;
	useMesh();
}

void App::useNextLFB()
{
	currLFB++;
	useLFB();
}

void App::usePrevLFB()
{
	currLFB--;
	useLFB();
}

void App::doubleLights()
{
	setNLights(sphereLightPositions.size() * 2, coneLightPositions.size() * 2);
}

void App::halveLights()
{
	setNLights(sphereLightPositions.size() / 2, coneLightPositions.size() / 2);
}

void App::setNLights(size_t sphereLights, size_t coneLights)
{
	(void) (sphereLights);
	(void) (coneLights);

#if RANDOM_LIGHTS
	createLightsRandom(sphereLights, coneLights);
#endif
#if GEO_LIGHTS
	createLightsOnGeometry(mesh, sphereLights, coneLights);
#endif
//#else
//	createLightsForTeapot();
//#endif
	setLightVelocities(LIGHT_SPEED);

	createLightBuffers();
	useLFB();
}

void App::cycleLighting()
{
	currLightType++;
	if (currLightType > (int) lightTypes.size() - 1)
		currLightType = 0;
	useLighting(lightTypes[currLightType]);
}

void App::useNextLighting()
{
	currLightType++;
	if (currLightType > (int) lightTypes.size() - 1)
		currLightType = 0;
	useLighting(lightTypes[currLightType]);
}

void App::usePrevLighting()
{
	currLightType--;
	if (currLightType < 0)
		currLightType = (int) lightTypes.size() - 1;
	useLighting(lightTypes[currLightType]);
}

void App::useLighting(Oit::LightType type)
{
	static std::map<Oit::LightType, std::string> lightTypes;
	if (lightTypes.empty())
	{
		lightTypes[Oit::BRUTE_FORCE] = "Brute Force";
		lightTypes[Oit::TILED_CLUSTER_FORWARD] = "Tiled Cluster Forward";
		lightTypes[Oit::TILED_CLUSTER_DEFERRED] = "Tiled Cluster Deferred";
		lightTypes[Oit::FORWARD_PLUS_FORWARD] = "Forward+ Forward";
		lightTypes[Oit::FORWARD_PLUS_DEFERRED] = "Forward+ Deferred";
		lightTypes[Oit::PRACTICAL_CLUSTER_FORWARD] = "Practical Cluster Forward";
		lightTypes[Oit::PRACTICAL_CLUSTER_DEFERRED] = "Practical Cluster Deferred";
		lightTypes[Oit::RAY_CLUSTER_FORWARD] = "Hybrid Cluster Forward";
		lightTypes[Oit::RAY_CLUSTER_DEFERRED] = "Hybrid Cluster Deferred";

		lightTypes[Oit::RAY_CLUSTER_LLI_FORWARD] = "Hybrid LLI Forward";
		lightTypes[Oit::RAY_CLUSTER_LLI_DEFERRED] = "Hybrid LLI Deferred";
		lightTypes[Oit::RAY_CLUSTER_LAI_FORWARD] = "Hybrid LAI Forward";
		lightTypes[Oit::RAY_CLUSTER_LAI_DEFERRED] = "Hybrid LAI Deferred";
		lightTypes[Oit::RAY_CLUSTER_LLF_FORWARD] = "Hybrid LLF Forward";
		lightTypes[Oit::RAY_CLUSTER_LLF_DEFERRED] = "Hybrid LLF Deferred";
		lightTypes[Oit::RAY_CLUSTER_LAF_FORWARD] = "Hybrid LAF Forward";
		lightTypes[Oit::RAY_CLUSTER_LAF_DEFERRED] = "Hybrid LAF Deferred";

		lightTypes[Oit::LIGHT_LINK_LIST_FORWARD] = "Light Link List Forward";
		lightTypes[Oit::LIGHT_LINK_LIST_DEFERRED] = "Light Link List Deferred";
		lightTypes[Oit::DEFERRED] = "Deferred";
		lightTypes[Oit::VISUALISER] = "Visualiser";
	}

	lightType = type;
	currLightStr = lightTypes[lightType];
	std::cout << "Using lighting: " << currLightStr << "\n";

	useLFB();
}

void App::playAnim()
{
	anim.play();
}

void App::saveGeometryLFB()
{
	if (opaqueFlag == 1)
		return;
	auto s = geometryLFB.getStr();
	Utils::File f("lfb.txt");
	f.write(s);
}

void App::saveLFBData()
{
	currOit->saveLFB(this);
}

