#version 450

#define LIGHT_RES 256
#define DEEP_RES 64
#define MAX_EYE_Z 30.0

// Note: There's not actually any incoming data.
//layout(location = 0) in vec4 vertex;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;


readonly buffer GlobalLightPos
{
	vec4 globalLightPos[];
};

readonly buffer GlobalLightDir
{
	vec4 globalLightDir[];
};


// Need to store light pos, dir and color.
out LightData
{
	flat vec4 esLightPos;
	vec4 esVert;
	vec4 csVert;
	flat vec3 dir;
	flat float col;
	flat float range;
	flat bool coneFlag;
	flat int lightIndex;
} VertexOut;



#include "../utils.glsl"



void main()
{
	int light = int(gl_VertexID);
	VertexOut.lightIndex = light;

	vec4 posData = globalLightPos[light];
	vec4 dirData = globalLightDir[light];

	vec4 osVert = vec4(posData.xyz, 1);
	vec4 osDir = vec4(dirData.xyz, 0);

	vec4 colData = floatToRGBA8(posData.w);
	float range = dirData.w;
	float type = colData.w;

	VertexOut.col = rgba8ToFloat(vec4(colData.xyz, 1));
	VertexOut.range = range;
	VertexOut.coneFlag = type > 0;

	vec4 esLightPos = mvMatrix * osVert;
	vec4 dir = mvMatrix * osDir;

	//vec4 dir = vec4(nMatrix * osDir.xyz, 0);
	//vec4 dir = mvMatrix * vec4(osDir.xyz, 0);

	vec4 esVert = mvMatrix * osVert;

	if (type > 0)
	{
	
		//VertexOut.esLightPos = esVert + dir * (range * 0.5);
		//VertexOut.range = range * 0.5;
		VertexOut.esLightPos = esVert;
	}
	else
		VertexOut.esLightPos = esVert;

#if 1
	// Light is outside the frustum range but overlaps the frustum?
	if (-esVert.z < 0 && -esVert.z + range >= 0)
		esVert.z = -0.1;
	else if (-esVert.z > MAX_EYE_Z && -esVert.z - range <= MAX_EYE_Z)
		esVert.z = -(MAX_EYE_Z - 0.1);
#endif

	vec4 csVert = pMatrix * esVert;

	VertexOut.esVert = esVert;
	VertexOut.csVert = csVert;
	VertexOut.dir = dir.xyz;
}

