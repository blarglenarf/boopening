#version 450

#define CLUSTER_POS_RES 256
#define STORE_LIGHT_DATA 1
#define GET_COUNTS 1

// Note: There's not actually any incoming data.
//layout(location = 0) in vec4 vertex;

#if GET_COUNTS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	#import "cluster"

	// Stored data is: (vec4: pos), (vec4: dir), (float: col).
	CLUSTER_DEC(clusterPos, float);
#endif



readonly buffer GlobalLightPos
{
	vec4 globalLightPos[];
};

readonly buffer GlobalLightDir
{
	vec4 globalLightDir[];
};



#include "../utils.glsl"

#include "../lfb/tiles.glsl"



int getClusterIndex(vec2 coord, float cluster)
{
	return int(coord.y) * CLUSTER_POS_RES * CLUSTER_POS_RES + int(coord.x) * CLUSTER_POS_RES + int(cluster);
}


void main()
{
	int light = int(gl_VertexID);

	vec4 posData = globalLightPos[light];
	vec4 dirData = globalLightDir[light];

	vec4 osVert = vec4(posData.xyz, 1);
	vec4 osDir = vec4(dirData.xyz, 0);

	vec4 colData = floatToRGBA8(posData.w);
	float range = dirData.w;
	float type = colData.w;

	float col = rgba8ToFloat(vec4(colData.xyz, 1));
	bool coneFlag = type > 0;
	vec3 dir = osDir.xyz;

	vec3 pos = posData.xyz;

	vec3 minPos = vec3(-10, -5, -10);
	vec3 maxPos = vec3(10, 5, 10);

	vec3 gridPos = (pos - minPos) / (maxPos - minPos) * vec3(CLUSTER_POS_RES);

#if GET_COUNTS
	int clusterIndex = getClusterIndex(gridPos.xy, gridPos.z);
	atomicAdd(offsets[clusterIndex], 1);
#else
	int clusterIndex = CLUSTER_GET_INDEX(clusterPos, int(gridPos.z), gridPos.xy);

	// These are in oject space.
	CLUSTER_ADD_DATA(clusterPos, clusterIndex, col, vec4(pos, coneFlag ? 1 : 0), vec4(dir, range));
#endif


#if 0
	// TODO: Data being stored:
#if GET_COUNTS
	int clusterIndex = getClusterIndex(gl_FragCoord.xy, cluster);
	atomicAdd(offsets[clusterIndex], 1);
#else
	int fragIndex = CLUSTER_GET_INDEX(cluster, cluster, gl_FragCoord.xy);

	// 0 for point light, 1 for cone light.
	// Storing data, or indices?
	//#if STORE_LIGHT_DATA
		CLUSTER_ADD_DATA(cluster, fragIndex, VertexIn.col, vec4(VertexIn.esLightPos.xyz, VertexIn.coneFlag ? 1 : 0), vec4(VertexIn.dir, VertexIn.range));
	//#else
	//	CLUSTER_ADD_DATA(cluster, fragIndex, VertexIn.lightIndex);
	//#endif
#endif



	VertexOut.lightIndex = light;

	vec4 posData = globalLightPos[light];
	vec4 dirData = globalLightDir[light];

	vec4 osVert = vec4(posData.xyz, 1);
	vec4 osDir = vec4(dirData.xyz, 0);

	vec4 colData = floatToRGBA8(posData.w);
	float range = dirData.w;
	float type = colData.w;

	VertexOut.col = rgba8ToFloat(vec4(colData.xyz, 1));
	VertexOut.range = range;
	VertexOut.coneFlag = type > 0;

	vec4 esLightPos = mvMatrix * osVert;
	vec4 dir = mvMatrix * osDir;

	//vec4 dir = vec4(nMatrix * osDir.xyz, 0);
	//vec4 dir = mvMatrix * vec4(osDir.xyz, 0);

	vec4 esVert = mvMatrix * osVert;

	if (type > 0)
	{
	
		//VertexOut.esLightPos = esVert + dir * (range * 0.5);
		//VertexOut.range = range * 0.5;
		VertexOut.esLightPos = esVert;
	}
	else
		VertexOut.esLightPos = esVert;

#if 1
	// Light is outside the frustum range but overlaps the frustum?
	if (-esVert.z < 0 && -esVert.z + range >= 0)
		esVert.z = -0.1;
	else if (-esVert.z > MAX_EYE_Z && -esVert.z - range <= MAX_EYE_Z)
		esVert.z = -(MAX_EYE_Z - 0.1);
#endif

	vec4 csVert = pMatrix * esVert;

	VertexOut.esVert = esVert;
	VertexOut.csVert = csVert;
	VertexOut.dir = dir.xyz;
#endif
}

