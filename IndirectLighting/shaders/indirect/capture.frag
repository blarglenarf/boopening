#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define BUILD_BRUTE_FORCE 0

#define INDEX_BY_TILE 0
#define GET_COUNTS 0

#if !GET_COUNTS
	#import "lfb"
#endif

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#include "index.glsl"

#define LFB_NAME lfb
#define DEEP_RES 64
#define N_DEEPS 3

#define DEEP_COLS 1
#define DEEP_ROWS 1

#if GET_COUNTS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	LFB_DEC(LFB_NAME, float);
#endif


uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;


#if BUILD_BRUTE_FORCE
	in VertexData
	{
		vec3 esFrag;
		vec2 texCoord;
	} VertexIn;

	uniform int deepIndex;
#else
	in VertexData
	{
		vec3 osFrag;
		vec2 texCoord;
	} VertexIn;


	// Storage buffer of matrices.
	readonly buffer DeepCams
	{
		mat4 deepCams[];
	};
#endif


uniform bool texFlag;
uniform bool normFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;


#include "../light/light.glsl"


out vec4 fragColor;



void main()
{
	//vec4 color = vec4(1);
	vec4 color = getAmbient(0.3);
	float f = rgba8ToFloat(color);

	// Need the size of an individual image.
	ivec2 imgSize = ivec2(DEEP_RES);
	ivec2 deepSize = imgSize * ivec2(DEEP_COLS, DEEP_ROWS);

#if BUILD_BRUTE_FORCE
	//vec2 ssFrag = getScreenFromClip(vec4(VertexIn.esFrag, 1.0), vec2(imgSize.x, imgSize.y));
	int pixel = pixelIndex(deepIndex, ivec2(gl_FragCoord.xy), deepSize, DEEP_COLS, DEEP_RES);

	#if GET_COUNTS
		atomicAdd(offsets[pixel], 1);
	#else
		//LFB_ADD_DATA(LFB_NAME, pixel, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
		LFB_ADD_DATA(LFB_NAME, pixel, VertexIn.esFrag.z);
		//LFB_ADD_DATA(LFB_NAME, pixel, vec2(f, esFrag.z));
	#endif
#else
	// Fragment may cover many different pixels, these need to be calculated for each direction.
	// FIXME: This can result in a lot of duplication of fragments across neighbouring pixels. Not sure how to fix this.
	for (int i = 0; i < N_DEEPS; i++)
	{
		//vec4 esFrag = mvMatrix0 * vec4(VertexIn.osFrag, 1);
		// Note: this camera had a ridiculous amount of scaling applied to it so that the scene would fit.
		vec4 esFrag = deepCams[i] * vec4(VertexIn.osFrag, 1);
		vec2 ssFrag = getScreenFromClip(esFrag, vec2(imgSize.x, imgSize.y));

		int pixel = pixelIndex(i, ivec2(ssFrag), deepSize, DEEP_COLS, DEEP_RES);

	#if GET_COUNTS
		atomicAdd(offsets[pixel], 1);
	#else
		//LFB_ADD_DATA(LFB_NAME, pixel, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
		LFB_ADD_DATA(LFB_NAME, pixel, esFrag.z);
		//LFB_ADD_DATA(LFB_NAME, pixel, vec2(f, esFrag.z));
	#endif
	}
#endif

	//color = getAmbient(0.3); // Not sure if this is correct now or not...
	discard;

	fragColor = color;
}

