#version 430

#define CLUSTERS 32
#define MASK_SIZE 1
#define MAX_EYE_Z 30.0

#define GET_COUNTS 0

#define ATOMIC_ADD 1
#define STORE_LIGHT_DATA 1

#include "../utils.glsl"

#if GET_COUNTS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	#import "cluster"

	// Stored data is: (vec4: pos), (vec4: dir), (float: col).
	// Or it's just the light's index.
	#if STORE_LIGHT_DATA
		CLUSTER_DEC(cluster, float);
	#else
		CLUSTER_DEC(cluster, uint);
	#endif
#endif


readonly buffer ClusterMasks
{
	uint clusterMasks[];
};


in VertexData
{
	flat vec4 esLightPos;
	flat vec4 csLightPos;
	flat vec3 dir;
	flat float col;
	flat float range;
	flat bool coneFlag;
	flat int lightIndex;
} VertexIn;


uniform ivec4 viewport;
uniform mat4 invPMatrix;
uniform mat4 pMatrix;

uniform ivec2 size;

out vec4 fragColor;



int getCluster(float depth)
{
	return max(0, min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1)))));
}

int getClusterIndex(vec2 fragCoord, int cluster)
{
	return int(fragCoord.y) * size.x * CLUSTERS + int(fragCoord.x) * CLUSTERS + cluster;
}



int getFragIndex()
{
	ivec2 gridCoord = ivec2(gl_FragCoord.xy);
	return size.x * gridCoord.y + gridCoord.x;
}

bool checkBit(int fragIndex, int cluster)
{
	int maskIndex = cluster / (MASK_SIZE * 32);
	int clusterIndex = cluster % 32;
	return (clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
}

bool checkBits(float minZ, float maxZ)
{
	int fragIndex = getFragIndex();

	int cluster1 = getCluster(minZ);
	int cluster2 = getCluster(maxZ);

	for (int cluster = cluster1; cluster <= cluster2; cluster++)
		if (checkBit(fragIndex, cluster))
			return true;
	return false;
}

vec2 getClosestPoint(vec2 p)
{
	vec2 ssPos = gl_FragCoord.xy;

	if (p.x < gl_FragCoord.x - 0.5)
		ssPos.x -= 0.5;
	else if (p.x > gl_FragCoord.x + 0.5)
		ssPos.x += 0.5;
	else
		ssPos.x = p.x;
	if (p.y < gl_FragCoord.y - 0.5)
		ssPos.y -= 0.5;
	else if (p.y > gl_FragCoord.y + 0.5)
		ssPos.y += 0.5;
	else
		ssPos.y = p.y;

	return ssPos;
}


// Count two fragments, one for front and one for back.
void main()
{
	float frontDepth = MAX_EYE_Z, backDepth = 0;

	//fragColor = vec4(1);
	//return;

	// We need to pick the point in the tile closest to the centre.
	vec3 esLightPos = VertexIn.esLightPos.xyz;
	vec4 csLightPos = VertexIn.csLightPos;
	float radius = VertexIn.range;

#if 0
	// Pick position closest to screen space centre of the sphere.
	vec2 ssLightPos = getScreenFromClip(csLightPos, vec2(size.x, size.y));

	// If the ssLightPos is inside this pixel, use it, otherwise push closer to it.
	vec2 ssPos = getClosestPoint(ssLightPos);

	vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -MAX_EYE_Z), viewport, invPMatrix).xyz);
	if (!sphereIntersection(esLightPos, radius, dir, frontDepth, backDepth))
	{
		discard;
		return;
	}
#else
	if (!VertexIn.coneFlag)
	{
		// Pick position closest to screen space centre of the sphere.
		vec2 ssLightPos = getScreenFromClip(csLightPos, vec2(size.x, size.y));

		// If the ssLightPos is inside this pixel, use it, otherwise push closer to it.
		vec2 ssPos = getClosestPoint(ssLightPos);

		vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -MAX_EYE_Z), viewport, invPMatrix).xyz);
		if (!sphereIntersection(esLightPos, radius, dir, frontDepth, backDepth))
		{
			discard;
			return;
		}
	}
	else
	{
		//float aperture = 0.392699;
		float aperture = 0.785398;

		// 1: Cone facing away from us (tip close, end far).
		// 2: Cone facing toward us (tip far, end close).

	#if 0
		// TODO: Pick two intersection points, one closest to tip, and one closest to end cap.
		// TODO: Pick min/max depth from these four points.
		vec2 ssTip = getScreenFromClip(csLightPos, vec2(size.x, size.y));
		vec2 ssCap = getScreenFromClip(pMatrix * vec4(esLightPos + VertexIn.dir * radius, 1), vec2(size.x, size.y));

		ssTip = getClosestPoint(ssTip);
		ssCap = getClosestPoint(ssCap);

		vec3 dir1 = normalize(getEyeFromWindow(vec3(ssTip, -MAX_EYE_Z), viewport, invPMatrix).xyz);
		vec3 dir2 = normalize(getEyeFromWindow(vec3(ssCap, -MAX_EYE_Z), viewport, invPMatrix).xyz);
		float d1, d2, d3, d4;
		bool b1 = coneIntersection(esLightPos, radius * 15, dir1, VertexIn.dir, aperture, d1, d2);
		bool b2 = coneIntersection(esLightPos, radius * 15, dir2, VertexIn.dir, aperture, d3, d4);
		if (!b1 && !b2)
		{
			discard;
			return;
		}
		if (b1)
		{
			frontDepth = min(d1, d2);
			backDepth = max(d1, d2);
		}
		if (b2)
		{
			frontDepth = min(frontDepth, min(d3, d4));
			backDepth = max(backDepth, max(d3, d4));
		}
	#elif 0
		vec3 dir = normalize(getEyeFromWindow(vec3(gl_FragCoord.xy, -MAX_EYE_Z), viewport, invPMatrix).xyz);
		if (!coneIntersection(esLightPos, radius, dir, VertexIn.dir, aperture, frontDepth, backDepth))
		{
			discard;
			return;
		}
	#else
		// TODO: What about a hemisphere?
		// Pick position closest to screen space centre of the sphere.
		vec2 ssLightPos = getScreenFromClip(csLightPos, vec2(size.x, size.y));

		// If the ssLightPos is inside this pixel, use it, otherwise push closer to it.
		vec2 ssPos = getClosestPoint(ssLightPos);

		vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -MAX_EYE_Z), viewport, invPMatrix).xyz);
		if (!sphereIntersection(esLightPos, radius, dir, frontDepth, backDepth))
		{
			discard;
			return;
		}
	#endif
	}
#endif

	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	int frontCluster = getCluster(frontDepth);
	int backCluster = getCluster(backDepth);

	for (int cluster = frontCluster; cluster <= backCluster; cluster++)
	{
		if (!checkBit(pixel, cluster))
			continue;

		#if GET_COUNTS
			int clusterIndex = getClusterIndex(gl_FragCoord.xy, cluster);
			atomicAdd(offsets[clusterIndex], 1);
		#else
			int fragIndex = CLUSTER_GET_INDEX(cluster, cluster, gl_FragCoord.xy);

			// 0 for point light, 1 for cone light.
			// Storing data, or indices?
			#if STORE_LIGHT_DATA
				CLUSTER_ADD_DATA(cluster, fragIndex, VertexIn.col, vec4(VertexIn.esLightPos.xyz, VertexIn.coneFlag ? 1 : 0), vec4(VertexIn.dir, VertexIn.range));
			#else
				CLUSTER_ADD_DATA(cluster, fragIndex, VertexIn.lightIndex);
			#endif
		#endif
	}
}

