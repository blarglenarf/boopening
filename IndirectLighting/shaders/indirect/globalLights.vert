#version 450

#define LIGHT_RES 256
#define DEEP_RES 64
#define MAX_EYE_Z 30.0

#define N_DEEPS 3

#define DEEP_COLS 1
#define DEEP_ROWS 1

#define PIXEL_STEPPING 0

#include "../utils.glsl"

#include "index.glsl"

// Note: There's not actually any incoming data.
//layout(location = 0) in vec4 vertex;

uniform layout(binding = 0, offset = 0) atomic_uint lightCount;

#import "lfb"

uniform sampler2D dirTex;
uniform sampler2D colTex;
uniform sampler2D posTex;
uniform sampler2D lodTex;
uniform sampler2D normTex;

uniform float minDist;
uniform float maxDist;
uniform float dist;
uniform int div;
uniform int lod;

uniform int stacks;
uniform int slices;

uniform int dirIndex;

uniform vec3 sampleDir0;
uniform vec3 sampleDir1;
uniform vec3 sampleDir2;
uniform vec3 sampleDir3;
uniform vec3 sampleDir4;
uniform vec3 sampleDir5;

uniform bool coneFlag;

uniform int lightAlloc;


vec3 coneDirs[6];
float coneRanges[6];


readonly buffer DeepCams
{
	mat4 deepCams[];
};


// Store color and id in the fourth variable.
coherent buffer GlobalLightPos
{
	vec4 globalLightPos[];
};

// Store range in the fourth variable.
coherent buffer GlobalLightDir
{
	vec4 globalLightDir[];
};



#include "../utils.glsl"

#include "../lfb/tiles.glsl"

#include "index.glsl"


LFB_DEC(lfb, float);


#if 0
int getDeepImgIndex(vec3 osDir)
{
	float p = acos(osDir.y);
	float t = atan(osDir.z, osDir.x);
	if (t < 0)
		t += 2.0 * PI;

	int st = int(round((p / (0.5 * PI)) * float(stacks)));
	int sl = int(round((t / (2.0 * PI)) * float(slices)));

	int index = sl * (stacks + 1) + st;
	return index;
}

int getPixel(vec4 esPos)
{
	//vec4 esPos = deepCams[index] * vec4(osPos, 1);

	// Projection is identity matrix.
	vec2 ssPos = getScreenFromClip(esPos, vec2(DEEP_RES, DEEP_RES));

	int index = int(int(ssPos.y) * DEEP_RES + int(ssPos.x));
	return index;
}
#endif
float calcLightRange(vec3 osPos, vec3 osDir)
{
	float delta = 0.1;

	// If osDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
#if 1
	if (osDir.y < 0)
	{
		reverseFlag = true;
		osDir = -osDir;
	}
#endif
	//int index = getDeepImgIndex(osDir);
	int index = getDeepImgIndex(osDir, stacks, slices);
	//if (reverseFlag)
	//	osDir = -osDir;

	ivec2 imgSize = ivec2(DEEP_RES);
	ivec2 deepSize = imgSize * ivec2(DEEP_COLS, DEEP_ROWS);

	vec4 esPos = deepCams[index] * vec4(osPos, 1);
	vec2 ssFrag = getScreenFromClip(esPos, vec2(imgSize.x, imgSize.y));
	int pixel = pixelIndex(index, ivec2(ssFrag), deepSize, DEEP_COLS, DEEP_RES);

	// Step through the fragment list to find the next fragment after the light.
	// Note: I scaled by 1/0.06 when rendering the multi-view deep image. Undo that here.
	float d = esPos.z / 0.06;

	// Stepping direction is reversed if reverseFlag is true.
	for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			float z = LFB_GET_DATA_REV(lfb) / 0.06;
			if (d + delta < z)
				return -(d - z);
		}
		else
		{
			float z = LFB_GET_DATA(lfb) / 0.06;
			if (z < d - delta)
				return -(z - d);
		}
	}

	// Didn't hit anything. Return a smallish light.
	return 0.0;
}


void calcConeDirs(vec3 lightPos, vec3 dir)
{
	for (int i = 0; i < 6; i++)
		coneDirs[i] = dir;

	vec3 up = dir.y * dir.y > 0.95 ? vec3(0, 0, 1) : vec3(0, 1, 0);
	vec3 right = cross(dir, up);
	up = cross(dir, right);
	normalize(up);
	normalize(right);

	//coneDirs[0] += sampleDir0 * right + sampleDir0 * up;
	coneDirs[0] = dir;
	coneDirs[1] += sampleDir1 * right + sampleDir1 * up;
	coneDirs[2] += sampleDir2 * right + sampleDir2 * up;
	coneDirs[3] += sampleDir3 * right + sampleDir3 * up;
	coneDirs[4] += sampleDir4 * right + sampleDir4 * up;
	coneDirs[5] += sampleDir5 * right + sampleDir5 * up;

	for (int i = 0; i < 6; i++)
		normalize(coneDirs[i]);

	for (int i = 0; i < 6; i++)
		coneRanges[i] = calcLightRange(lightPos.xyz, coneDirs[i]);
}


void main()
{
	//vec4 osVert = vec4(vertex.xyz, 1.0);
	int light = int(gl_VertexID);

	// These could be the other way around, shouldn't make a difference though.
	ivec2 texCoord = ivec2(0);
	texCoord.x = light % (LIGHT_RES / div);
	texCoord.y = light / (LIGHT_RES / div);

	//VertexOut.exists = true;

	vec4 osVert = vec4(0);

	vec4 lightDir = vec4(texelFetch(dirTex, texCoord, lod).xyz, 0);
	vec4 lightCol = vec4(texelFetch(colTex, texCoord, lod).xyz, 1);
	vec4 lightPos = vec4(texelFetch(posTex, texCoord, lod).xyz, 1);
	float lightDist = texelFetch(lodTex, texCoord, lod).x;
	vec4 normal = vec4(texelFetch(normTex, texCoord, lod).xyz, 0);

	vec4 osDir = normal;
	//vec4 osDir = lightDir;
	float r = 0;

	// Needed for samples not present in the texture. May want to find a better way (like using dir perhaps).
	if (lightPos.x == 0 && lightPos.y == 0 && lightPos.z == 0)
		return;
	if (lightCol.x == 0 && lightCol.y == 0 && lightCol.z == 0)
		return;

	if (coneFlag)
	{
		//return;
		// Two options: try spawning a light at the intersection point, or just create a cone light.
		float lightDist = float(texelFetch(lodTex, texCoord, lod).x);
		if (lightDist < minDist || lightDist > maxDist)
			return;

		// Kill using russian roulette.
		// FIXME: Make sure this random function is actuall random, and gives a decent distribution!!!
		float f = random(vec3(texCoord.x, texCoord.y, light), 1);
		//if (f > 0.05)
		//	return;
	#if 1
		calcConeDirs(lightPos.xyz, osDir.xyz);
		float lightRange = (coneRanges[0] + coneRanges[1] + coneRanges[2] + coneRanges[3] + coneRanges[4] + coneRanges[5]) / 6.0;
		//float lightRange = max(coneRanges[0], max(coneRanges[1], max(coneRanges[2], max(coneRanges[3], max(coneRanges[4], coneRanges[5])))));
		if (lightRange < 0.1)
			return;
	#endif
		if (lightRange > 2 && f > 0.05)
			return;
		lightPos.xyz += osDir.xyz * lightRange * 0.5;
		//lightRange = 1.5;
		//lightRange *= 0.9;
		osVert = lightPos;
		r = lightRange;
	}
	else
	{
		//return;
		// TODO: Either have this be an actual lod texture or change the name.
		float lightDist = float(texelFetch(lodTex, texCoord, lod).x);
		if (lightDist < minDist || lightDist > maxDist)
			return;
		lightCol.xyz = vec3(1);

		osVert = lightPos;
		r = dist * lightDist / minDist;
	}

	uint index = atomicCounterIncrement(lightCount);
	if (index < lightAlloc)
	{
		float c = rgba8ToFloat(vec4(lightCol.xyz, coneFlag ? 1 : 0));
		vec4 p = vec4(osVert.xyz, c);
		vec4 d = vec4(osDir.xyz, r);

		globalLightPos[index] = p;
		globalLightDir[index] = d; // Not strictly necessary for point lights.
	}
}

