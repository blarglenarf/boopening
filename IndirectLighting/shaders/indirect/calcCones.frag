#version 430

#define LIGHT_RES 256

in vec2 texCoord;

uniform sampler2D dirTex;
uniform sampler2D colTex;
uniform sampler2D posTex;

uniform int div;

buffer TestDir
{
	vec4 testDir[];
};

buffer TestCol
{
	vec4 testCol[];
};

buffer TestPos
{
	vec4 testPos[];
};

//out vec4 fragColor;


void main()
{
	ivec2 coord = ivec2(gl_FragCoord.xy);

	int fragCoord = coord.y * (LIGHT_RES / div) + coord.x;

	vec4 avgDir = vec4(0);
	vec4 avgCol = vec4(0);
	vec4 avgPos = vec4(0);

	// FIXME: For now, just avg, but we'll probably need something smarter (maybe).
	float amt = 0;
	for (int x = coord.x * div; x < (coord.x + 1) * div; x++)
	{
		for (int y = coord.y * div; y < (coord.y + 1) * div; y++)
		{
			vec4 dir = vec4(texelFetch(dirTex, ivec2(x, y), 0).xyz, 0);
			vec4 col = vec4(texelFetch(colTex, ivec2(x, y), 0).xyz, 1);
			vec4 pos = vec4(texelFetch(posTex, ivec2(x, y), 0).xyz, 1);
			amt += 1;

			avgDir += dir;
			avgCol += col;
			avgPos += pos;
		}
	}

	avgDir /= amt; avgDir.w = 0;
	avgCol /= amt; avgCol.w = 1;
	avgPos /= amt; avgPos.w = 1;

	testDir[fragCoord] = avgDir;
	testCol[fragCoord] = avgCol;
	testPos[fragCoord] = avgPos;

	//fragColor = vec4(color.rgb, 1.0);
}

