#version 450

// Uniforms for the g-buffer textures.
uniform sampler2D tex;

in vec2 texCoord;

out vec4 fragColor;


void main()
{
	vec4 color = texture2D(tex, texCoord);
	if (color.w > 1)
		color.xyz /= color.w;
	color.w = 1;

	fragColor = color;
}

