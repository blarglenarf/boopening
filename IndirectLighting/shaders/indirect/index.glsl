#ifndef INDEX_GLSL
#define INDEX_GLSL

#define BASIC_INDEXING 0
#define INDEX_BY_TILE 0


int pixelIndex(int deepIndex, ivec2 ssFrag, ivec2 deepSize, int deepCols, int deepRes)
{
#if BASIC_INDEXING
	#if INDEX_BY_TILE
		int pixel = tilesIndex(imgSize, ivec2(INDEX_TILE_SIZE), ivec2(ssFrag));
	#else
		int pixel = deepRes * int(ssFrag.y) + int(ssFrag.x);
	#endif
	int deepOffset = deepIndex * deepRes * deepRes;
	pixel += deepOffset;
#else
	// Deep image offset based on cols and rows.
	ivec2 deepCoord = ivec2(deepIndex % deepCols, deepIndex / deepCols);
	ivec2 pixelCoord = ivec2(ssFrag.x + deepCoord.x * deepRes, ssFrag.y + deepCoord.y * deepRes);
	int pixel = deepSize.x * pixelCoord.y + pixelCoord.x;
#endif
	return pixel;
}

vec3 calcDeepDir(vec3 osDir, int stacks, int slices)
{
	float p = acos(osDir.y);
	float t = atan(osDir.z, osDir.x);
	if (t < 0)
		t += 2.0 * PI;

	int st = int(round((p / (0.5 * PI)) * float(stacks)));
	int sl = int(round((t / (2.0 * PI)) * float(slices)));

	float theta = (float(sl) / float(slices)) * 2.0 * PI;
	float phi = (float(st) / float(stacks)) * 0.5 * PI;

	float x = cos(theta) * sin(phi);
	float z = sin(theta) * sin(phi);
	float y = cos(phi);

	return vec3(x, y, z);
}

int getDeepImgIndex(vec3 osDir, int stacks, int slices)
{
	float p = acos(osDir.y);
	float t = atan(osDir.z, osDir.x);
	if (t < 0)
		t += 2.0 * PI;

	int st = int(round((p / (0.5 * PI)) * float(stacks)));
	int sl = int(round((t / (2.0 * PI)) * float(slices)));

	//calcDeepDir(st, sl, reverseFlag);

	int index = sl * (stacks + 1) + st;
	return index;
}

int getPixel(ivec2 ssPos, int deepIndex, int deepRes, int deepCols, int deepRows)
{
	ivec2 imgSize = ivec2(deepRes);
	ivec2 deepSize = imgSize * ivec2(deepCols, deepRows);

	int pixel = pixelIndex(deepIndex, ivec2(ssPos), deepSize, deepCols, deepRes);

	return pixel;
}


#if 0
int tilesIndex(ivec2 vpSize, ivec2 tileSize, ivec2 pixelCoord)
{
#if 0
	// Passthrough.
	return pixelCoord.y * vpSize.x + pixelCoord.x;
#endif	

	// Standard matrix within tile.
#if 1
	ivec2 indexBase = pixelCoord / tileSize;
	ivec2 indexTile = pixelCoord - indexBase * tileSize; 
	return tileSize.y * (indexBase.y * vpSize.x + indexBase.x * tileSize.x) + indexTile.y * tileSize.x + indexTile.x;
#endif

	// Procedural kepler within tile.
#if 0
	ivec2 base = pixelCoord / ivec2(2, 8);
	ivec2 tile = pixelCoord % ivec2(2, 8);
	return base.y * lfbInfolfb.size.x * 8 + base.x * 8 * 2 + tile.y * 2 + tile.x;
#endif
}

ivec2 reverseTilesIndex(ivec2 vpSize, ivec2 tileSize, int width, int index)
{
	// Account for padding at the end of each row.
	index += (vpSize.x - width) * index / width;

	// Standard matrix within tile.
#if 1
	int tilesx = vpSize.x / tileSize.x;
	int n = tileSize.x * tileSize.y;
	int tile = index / n;
	int tileIndex = index % n;
	return ivec2(tile % tilesx, tile / tilesx) * tileSize + ivec2(tileIndex % tileSize.x, tileIndex / tileSize.x);
#endif
}
#endif

#endif

