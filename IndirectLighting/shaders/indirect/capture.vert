#version 430

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex;
layout(location = 3) in vec3 tangent;

out vec3 osVert;
out vec2 texCoord;

void main()
{
#if 0
	// TODO: Transforms will need to be done in the geometry shader.
	vec3 binormal = cross(normal, tangent);

	vec3 T = normalize(nMatrix * tangent);
	vec3 B = normalize(nMatrix * binormal);
	vec3 N = normalize(nMatrix * normal);
	mat3 TBN = mat3(T, B, N);
	VertexOut.tbn = TBN;

	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

    VertexOut.esFrag = esVert.xyz;
	VertexOut.normal = nMatrix * normalize(normal);
	VertexOut.texCoord = texCoord;

	gl_Position = csVert;
#endif
	osVert = vertex;
	texCoord = tex;
}

