#version 430

layout(triangles) in;

// TODO: This is probably a reasonable use-case for instanced rendering.

// TODO: Ideally need to output 6 triangles (2 for each face of the cubemap).
layout(triangle_strip, max_vertices = 3) out;

uniform int texLayer;

// Could do this in the vertex shader alternatively.
uniform mat4 mvMatrix;
uniform mat4 pMatrix;

in VertexData
{
	vec3 osVert;
} VertexIn[3];


out FragData
{
	float esDepth;
} FragOut; // Hehehe.


void main()
{
	for (int i = 0; i < 3; i++)
	{
		vec4 esVert = mvMatrix * vec4(VertexIn[i].osVert, 1);
		vec4 csVert = pMatrix * esVert;
		FragOut.esDepth = -esVert.z;
		gl_Layer = texLayer;
		gl_Position = csVert;
		EmitVertex();
	}
	EndPrimitive();
}

