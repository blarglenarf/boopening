#version 430

in vec2 texCoord;

uniform int vplID;

uniform sampler2DArray cubemapDepths;
//uniform sampler2D cubemapDepths;

out vec4 fragColor;


void main()
{
	float depth = texelFetch(cubemapDepths, ivec3(gl_FragCoord.xy, vplID), 0).x;
	//float depth = texelFetch(cubemapDepths, ivec2(gl_FragCoord.xy), 0).x;
	//float depth = texture2D(cubemapDepths, texCoord).x;

	fragColor = vec4(depth, depth, depth, 1.0);
}

