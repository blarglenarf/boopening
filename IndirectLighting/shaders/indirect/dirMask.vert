#version 450

#define ZERO_MASK 0

#define DEEP_RES 64

coherent buffer DirMask
{
	uint dirMask[];
};


#if !ZERO_MASK

coherent buffer PixMask
{
	uint pixMask[];
};

// TODO: Need to get this data directly from the rsm. If we get it from here then it's too late.
// TODO: Alternatively store lightDirs and positions first, then save an array of range values afterward.
// TODO: That means culling needs to be done in a separate step.
readonly buffer LightDirs
{
	vec4 lightDirs[];
};

readonly buffer LightPositions
{
	vec4 lightPositions[];
};

readonly buffer DeepCams
{
	mat4 deepCams[];
};


uniform vec3 sampleDir0;
uniform vec3 sampleDir1;
uniform vec3 sampleDir2;
uniform vec3 sampleDir3;
uniform vec3 sampleDir4;
uniform vec3 sampleDir5;


uniform int stacks;
uniform int slices;


vec3 coneDirs[6];


#include "../utils.glsl"


void calcConeDirs(vec3 dir)
{
	for (int i = 0; i < 6; i++)
		coneDirs[i] = dir;

	vec3 up = dir.y * dir.y > 0.95 ? vec3(0, 0, 1) : vec3(0, 1, 0);
	vec3 right = cross(dir, up);
	up = cross(dir, right);
	normalize(up);
	normalize(right);

	//coneDirs[0] += sampleDir0 * right + sampleDir0 * up;
	coneDirs[0] = dir;
	coneDirs[1] += sampleDir1 * right + sampleDir1 * up;
	coneDirs[2] += sampleDir2 * right + sampleDir2 * up;
	coneDirs[3] += sampleDir3 * right + sampleDir3 * up;
	coneDirs[4] += sampleDir4 * right + sampleDir4 * up;
	coneDirs[5] += sampleDir5 * right + sampleDir5 * up;

	for (int i = 0; i < 6; i++)
		normalize(coneDirs[i]);

	//for (int i = 0; i < 6; i++)
	//	coneRanges[i] = calcLightRange(lightPos.xyz, coneDirs[i]);
}


int getDeepImgIndex(vec3 osDir)
{
	float p = acos(osDir.y);
	float t = atan(osDir.z, osDir.x);
	if (t < 0)
		t += 2.0 * PI;

	int st = int(round((p / (0.5 * PI)) * float(stacks)));
	int sl = int(round((t / (2.0 * PI)) * float(slices)));

	//calcDeepDir(st, sl, reverseFlag);

	int index = sl * (stacks + 1) + st;
	return index;
}

int getPixel(vec4 esPos)
{
	//vec4 esPos = deepCams[index] * vec4(osPos, 1);

	// Projection is identity matrix.
	vec2 ssPos = getScreenFromClip(esPos, vec2(DEEP_RES, DEEP_RES));

	int index = int(int(ssPos.y) * DEEP_RES + int(ssPos.x));
	return index;
}


#endif


void main()
{
#if ZERO_MASK
	dirMask[gl_VertexID] = 0;
#else

	int lightIndex = gl_VertexID;

	vec3 dir = lightDirs[lightIndex].xyz;
	vec3 pos = lightPositions[lightIndex].xyz;

	// We also need to take into account the different cone directions.
	calcConeDirs(dir);

	for (int i = 0; i < 6; i++)
	{
		if (coneDirs[i].y < 0)
			coneDirs[i] = -coneDirs[i];

		int deepIndex = getDeepImgIndex(coneDirs[i]);

		// Set mask for dir to 1 (has to be done atomically), if it's already 1 don't bother.
		int maskIndex = deepIndex / 32;
		int bitIndex = deepIndex % 32;

		uint mask = 1 << bitIndex;

		if ((dirMask[maskIndex] & mask) == 0)
		{
			atomicOr(dirMask[maskIndex], mask);
		}

		// Now what about the mask for that particular pixel?
		vec4 esPos = deepCams[deepIndex] * vec4(pos, 1);

		int pixel = getPixel(esPos);
		ivec2 imgSize = ivec2(DEEP_RES, DEEP_RES);
		int layerOffset = deepIndex * imgSize.x * imgSize.y;
		int deepPixel = pixel + layerOffset;

		maskIndex = deepPixel / 32;
		bitIndex = deepPixel % 32;

		mask = 1 << bitIndex;

		if ((pixMask[maskIndex] & mask) == 0)
		{
			atomicOr(pixMask[maskIndex], mask);
		}
	}
#endif
}

