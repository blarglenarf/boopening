#version 450

#if 1

layout(location = 0) in vec3 vertex;

void main()
{
	gl_Position = vec4(vertex.xyz, 1.0);
}

#else

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;


uniform mat4 mvMatrix;
uniform mat3 nMatrix;
uniform mat4 pMatrix;


out VertexData
{
	vec3 esFrag;
	vec3 normal;
	vec2 texCoord;
} VertexOut;


void main()
{
	//vec3 binormal = cross(normal, tangent);

	//vec3 T = normalize(nMatrix * tangent);
	//vec3 B = normalize(nMatrix * binormal);
	//vec3 N = normalize(nMatrix * normal);
	//mat3 TBN = mat3(T, B, N);
	//VertexOut.tbn = TBN;

	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

    VertexOut.esFrag = esVert.xyz;
	VertexOut.normal = nMatrix * normalize(normal);
	VertexOut.texCoord = texCoord;

	gl_Position = csVert;
}

#endif

