#version 450

// Billboarded quad or sphere?
// Note: Billboarded quads seem to be much faster, even though they result in more pixels covered.
#define MAX_EYE_Z 30.0

layout(points) in;

layout(triangle_strip, max_vertices = 4) out;

in LightData
{
	flat vec4 esLightPos;
	vec4 esVert;
	vec4 csVert;
	flat vec3 dir;
	flat float col;
	flat float range;
	flat bool coneFlag;
	flat int lightIndex;
} VertexIn[1];

out VertexData
{
	flat vec4 esLightPos;
	flat vec4 csLightPos;
	flat vec3 dir;
	flat float col;
	flat float range;
	flat bool coneFlag;
	flat int lightIndex;
} VertexOut;


uniform mat4 mvMatrix;
uniform mat4 pMatrix;


#define VERT(v) \
	esVert = vec4(verts[v] + VertexIn[0].esVert.xyz, 1); \
	VertexOut.esLightPos = VertexIn[0].esLightPos; \
	VertexOut.csLightPos = VertexIn[0].csVert; \
	VertexOut.dir = VertexIn[0].dir.xyz; \
	VertexOut.col = VertexIn[0].col; \
	VertexOut.range = VertexIn[0].range; \
	VertexOut.coneFlag = VertexIn[0].coneFlag; \
	VertexOut.lightIndex = VertexIn[0].lightIndex; \
	gl_Position = pMatrix * esVert; \
	EmitVertex();


#define PERSPECTIVE_BIAS 1


void main()
{
	// Need to add some leeway to account for perspective.
	// Also need to add some leeway to account for grid sizes.
	float size = VertexIn[0].range;
	vec4 esPos = VertexIn[0].esVert;

#if PERSPECTIVE_BIAS
	#if 0
		vec3 conePos = VertexIn[0].esVert.xyz;
		vec3 coneDir = normalize(VertexIn[0].dir.xyz);
		float coneRad = VertexIn[0].esVert.w;
		conePos = VertexIn[0].esVert.xyz + (coneDir * coneRad);
		esPos = vec4(conePos, 1);
		size = coneRad * 1.5;
	#else
		size *= 2.5;
	#endif
#endif

	vec4 esVert;

	vec3 verts[4];
	verts[0] = vec3(size, -size, 0);
	verts[1] = vec3(-size, -size, 0);
	verts[2] = vec3(-size, size, 0);
	verts[3] = vec3(size, size, 0);

	VERT(1);
	VERT(2);
	VERT(0);
	VERT(3);
	EndPrimitive();
}

