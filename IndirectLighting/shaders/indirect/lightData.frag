#version 450

in VertexData
{
	vec3 normal;
	vec3 osFrag;
	vec3 esFrag;
	vec2 texCoord;
	mat3 tbn;
} VertexIn;

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

uniform vec3 lightPos;

uniform bool texFlag;
uniform bool normFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;


#include "../utils.glsl"


vec4 getAmbient()
{
	vec4 ambient = vec4(Material.ambient.xyz * 0.4, Material.ambient.w);
	if (texFlag)
	{
		vec4 texColor = texture2D(diffuseTex, VertexIn.texCoord);
		ambient = texColor * 0.4;
		ambient.w = min(Material.ambient.w, texColor.w);
	}
	ambient.w = min(ambient.w, 1);
	return ambient;
}


out vec4 fragColor[5];


void main()
{
	// TODO: Is the light dir the same as the view dir, or is the light dir just 0,0,1?
	//vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);
#if 0
	// FIXME: My normals are in object space, so I'm pretty sure this won't work.
	if (normFlag)
	{
		normal = texture2D(normalTex, VertexIn.texCoord).rgb;
		normal = normalize(normal * 2.0 - 1.0);
		normal = normalize(VertexIn.tbn * normal);
	}
#endif
	// First: reflection vectors.
	vec3 viewDir = normalize(VertexIn.osFrag - lightPos);
	vec4 reflected = vec4(normalize(reflect(viewDir, normal)), 0);

	// Second: geometry colors.
	vec4 ambient = getAmbient();

	// Third: geometry positions.
	vec4 position = vec4(VertexIn.osFrag, 1);

	// Offset pos very slightly in dir of normal.
	//position.xyz += normal * 0.1;

	// Direction texture.
	fragColor[0] = reflected;

	// Color texture.
	fragColor[1] = ambient;

	// Position texture.
	fragColor[2] = position;

	// LOD texture.
	//fragColor[3] = vec4(distance(VertexIn.osFrag, lightPos.xyz));
	fragColor[3] = vec4(-VertexIn.esFrag.z);

	// Worth storing normals?
	fragColor[4] = vec4(normal, 0);
}

