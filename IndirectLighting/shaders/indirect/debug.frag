#version 430

in vec2 texCoord;

uniform sampler2D dirTex;
uniform sampler2D colorTex;
uniform sampler2D posTex;

out vec4 fragColor;


void main()
{
	//vec4 color = texture2D(dirTex, texCoord);
	//vec4 color = texture2D(colorTex, texCoord);
	vec4 color = texture2D(posTex, texCoord);

	fragColor = vec4(color.rgb, 1.0);
}

