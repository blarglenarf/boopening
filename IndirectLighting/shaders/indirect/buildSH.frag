#version 450

#define GRID_CELL_SIZE 1
#define STORE_LIGHT_DATA 1
#define CLUSTERS 32

#define MAX_EYE_Z 30.0

#define INTERPOLATE_SH 1
#define INTERPOLATE_2D 0

#include "../utils.glsl"


#import "cluster"

CLUSTER_DEC(cluster, float);


#if 0
#include "../light/light.glsl"
#endif


// Window size.
uniform vec2 size;

// Cluster grid size.
uniform ivec2 cSize;

// For converting grid coords into eye space.
uniform ivec4 viewport;
uniform mat4 invPMatrix;

// Going to read depth values directly from the g-buffer.
// TODO: Alternative is to store per-cell front/back depths or just use the middle of each grid cell.
uniform sampler2D depthTex;

#if INTERPOLATE_SH
	#if INTERPOLATE_2D
	out vec4 color[8]; // TODO: Why can't I have 9?
	vec3 shResult[9];
	#else
	uniform layout(rgba32f) image3D shTex0;
	uniform layout(rgba32f) image3D shTex1;
	uniform layout(rgba32f) image3D shTex2;
	uniform layout(rgba32f) image3D shTex3;
	uniform layout(rgba32f) image3D shTex4;
	uniform layout(rgba32f) image3D shTex5;
	uniform layout(rgba32f) image3D shTex6;
	uniform layout(rgba32f) image3D shTex7;
	//uniform layout(rgba32f) image3D shTex8;
	#endif
#else
	coherent buffer SHBuffer
	{
		vec4 shBuffer[];
	};
#endif


vec3 shVals[9];

void createSHVals(vec3 lightDir, vec3 lightCol, float intensity)
{
	// Spherical harmonics generation: https://seblagarde.wordpress.com/2012/01/08/pi-or-not-to-pi-in-game-lighting-equation/
	// Moar: http://silviojemma.com/public/papers/lighting/spherical-harmonic-lighting.pdf
	// Moar: https://gamedev.stackexchange.com/questions/132042/is-using-spherical-harmonics-still-viable-as-a-technique-nowadays

	lightCol *= (3.14159 * intensity);

	// Band 0 (1.0).
	float b0 = 1.0;
	shVals[0] += 0.282095 * lightCol * b0;

	// Band 1 (2.0 / 3.0).
	float b1 = (2.0 / 3.0);
	shVals[1] += -0.488603 * lightDir.y * lightCol * b1;
	shVals[2] += 0.488603 * lightDir.z * lightCol * b1;
	shVals[3] += -0.488603 * lightDir.x * lightCol * b1;

	// Band 2 (1.0 / 4.0).
	float b2 = (1.0 / 4.0);
	shVals[4] += 1.092548 * lightDir.x * lightDir.y * lightCol * b2;
	shVals[5] += -1.092548 * lightDir.y * lightDir.z * lightCol * b2;
	shVals[6] += 0.315392 * (3.0 * lightDir.z * lightDir.z - 1.0) * lightCol * b2;
	shVals[7] += -1.092548 * lightDir.x * lightDir.z * lightCol * b2;
	shVals[8] += 0.546274 * (lightDir.x * lightDir.x - lightDir.y * lightDir.y) * lightCol * b2;
}



void main()
{
	//int tileIndex = gl_VertexID;
	//vec2 cPos = vec2(tileIndex % cSize.x, tileIndex / cSize.x);
	vec2 cPos = gl_FragCoord.xy;
	int tileIndex = int(cSize.x) * int(cPos.y) + int(cPos.x);

	vec4 col = vec4(0);
	vec4 pos = vec4(0);

#if INTERPOLATE_SH && INTERPOLATE_2D
	for (int i = 0; i < 9; i++)
		shResult[i] = vec3(0);
#endif

	// For now, just go through all 3D cells in this tile.
	// TODO: Could have a warp per-tile, and each thread handles a different cluster.
	for (int cell = 0; cell < CLUSTERS; cell++)
	{
		int cellIndex = tileIndex * CLUSTERS + cell;

		// First zero everything.
		for (int i = 0; i < 9; i++)
		{
			shVals[i] = vec3(0);
		#if !INTERPOLATE_SH
			shBuffer[cellIndex * 9 + i] = vec4(0);
		#endif
		}

		// Store data for each corner.
		vec2 ssBL = cPos.xy * vec2(GRID_CELL_SIZE); // Bottom left.
		vec2 ssTL = ssBL + vec2(0, GRID_CELL_SIZE); // Top left.
		vec2 ssBR = ssBL + vec2(GRID_CELL_SIZE, 0); // Bottom right.
		vec2 ssTR = ssBR + vec2(0, GRID_CELL_SIZE); // Top right.
		vec2 ssM = ssBL + vec2(GRID_CELL_SIZE / 2.0); // Middle.

		float depthBL = texelFetch(depthTex, ivec2(ssBL), 0).x;
		float depthTL = texelFetch(depthTex, ivec2(ssTL), 0).x;
		float depthBR = texelFetch(depthTex, ivec2(ssBR), 0).x;
		float depthTR = texelFetch(depthTex, ivec2(ssTR), 0).x;
		float depthM = texelFetch(depthTex, ivec2(ssM), 0).x;

		// Eye-space corners.
		//vec3 esBL = getEyeFromWindow(vec3(ssBL.x, ssBL.y, depthBL), viewport, invPMatrix).xyz;
		//vec3 esTL = getEyeFromWindow(vec3(ssTL.x, ssTL.y, depthTL), viewport, invPMatrix).xyz;
		//vec3 esBR = getEyeFromWindow(vec3(ssBR.x, ssBR.y, depthBR), viewport, invPMatrix).xyz;
		//vec3 esTR = getEyeFromWindow(vec3(ssTR.x, ssTR.y, depthTR), viewport, invPMatrix).xyz;
		//vec3 esM = getEyeFromWindow(vec3(ssM.x, ssM.y, depthM), viewport, invPMatrix).xyz;

		// Ideally we pick the middle depth, but if that's no good, grab one of the corners.
		// TODO: Make sure we pick a depth inside the grid cell.
		float minDepth = float(cell) / float(CLUSTERS) * float(-MAX_EYE_Z);
		float maxDepth = (float(cell) + 1) / float(CLUSTERS) * float(-MAX_EYE_Z);

		float depth = depthM;
		ivec2 ssPos = ivec2(ssM);
		if (depth == 0 || depth > minDepth || depth < maxDepth)
		{
			depth = depthBL;
			ssPos = ivec2(ssBL);
		}
		if (depth == 0 || depth > minDepth || depth < maxDepth)
		{
			depth = depthTL;
			ssPos = ivec2(ssTL);
		}
		if (depth == 0 || depth > minDepth || depth < maxDepth)
		{
			depth = depthBR;
			ssPos = ivec2(ssBR);
		}
		if (depth == 0 || depth > minDepth || depth < maxDepth)
		{
			depth = depthTR;
			ssPos = ivec2(ssTR);
		}
		if (depth > minDepth || depth < maxDepth)
		{
			//depth = (minDepth + maxDepth) / 2.0;
			//ssPos = ivec2(ssM);
		}

		ssPos = ivec2(ssM);
		depth = float(cell) / float(CLUSTERS) * float(-MAX_EYE_Z);
		vec3 esPos = getEyeFromWindow(vec3(ssPos.x, ssPos.y, depth), viewport, invPMatrix).xyz;

		// TODO: Reduce overall code size.
		for (CLUSTER_ITER_BEGIN(cluster, cellIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
		{
			// Cluster stores full light data or index?
			float data = CLUSTER_GET_DATA(cluster);
			vec4 lightPos = CLUSTER_GET_POS(cluster);
			vec4 lightDirRef = CLUSTER_GET_DIR(cluster);
			vec4 lightColor = floatToRGBA8(data);
			float coneFlag = lightPos.w;
			float lightRadius = lightDirRef.w;
			lightPos.w = 1;
			lightDirRef.w = 0;

			float dist = distance(lightPos.xyz, esPos);
			float att = clamp(1.0 / (dist * dist), 0.0, 1.0);
			if (dist > lightRadius)
				continue;
			vec3 dir = normalize(lightPos.xyz - esPos);

			createSHVals(dir, lightColor.xyz, att);

		#if 0
			float distA = distance(lightPos.xyz, esBL);
			float distB = distance(lightPos.xyz, esTL);
			float distC = distance(lightPos.xyz, esBR);
			float distD = distance(lightPos.xyz, esTR);
			float distM = distance(lightPos.xyz, esM);
			float iA = clamp(1.0 / (distA * distA), 0.0, 1.0);
			float iB = clamp(1.0 / (distB * distB), 0.0, 1.0);
			float iC = clamp(1.0 / (distC * distC), 0.0, 1.0);
			float iD = clamp(1.0 / (distD * distD), 0.0, 1.0);

			// Pretty sure this should never happen.
			if (distA > lightRadius && distB > lightRadius && distC > lightRadius && distD > lightRadius && distM > lightRadius)
				continue;

			vec3 dirA = normalize(lightPos.xyz - esBL);
			vec3 dirB = normalize(lightPos.xyz - esTL);
			vec3 dirC = normalize(lightPos.xyz - esBR);
			vec3 dirD = normalize(lightPos.xyz - esTR);

			createSHVals(0, dirA, lightColor.xyz, iA);
			createSHVals(1, dirB, lightColor.xyz, iB);
			createSHVals(2, dirC, lightColor.xyz, iC);
			createSHVals(3, dirD, lightColor.xyz, iD);
		#endif
		}

		// Write the result somewhere.
	#if INTERPOLATE_SH
		#if INTERPOLATE_2D
		for (int i = 0; i < 9; i++)
			shResult[i] += shVals[i];
		#else
		ivec3 coord = ivec3(cPos.x, cPos.y, cell);

		// FIXME: Band 3 is being ignored for some reason, not saying it's the g-buffer or anything...
		imageStore(shTex0, coord, vec4(shVals[0], 0));
		imageStore(shTex1, coord, vec4(shVals[1], 0));
		imageStore(shTex2, coord, vec4(shVals[2], 0));
		imageStore(shTex3, coord, vec4(shVals[3], 0));
		imageStore(shTex4, coord, vec4(shVals[4], 0));
		imageStore(shTex5, coord, vec4(shVals[5], 0));
		imageStore(shTex6, coord, vec4(shVals[6], 0));
		imageStore(shTex7, coord, vec4(shVals[7], 0));
		//imageStore(shTex8, coord, vec4(shVals[8], 0));
		#endif
	#else
		shBuffer[cellIndex * 9 + 0] = vec4(shVals[0], 0);
		shBuffer[cellIndex * 9 + 1] = vec4(shVals[1], 0);
		shBuffer[cellIndex * 9 + 2] = vec4(shVals[2], 0);
		shBuffer[cellIndex * 9 + 3] = vec4(shVals[3], 0);
		shBuffer[cellIndex * 9 + 4] = vec4(shVals[4], 0);
		shBuffer[cellIndex * 9 + 5] = vec4(shVals[5], 0);
		shBuffer[cellIndex * 9 + 6] = vec4(shVals[6], 0);
		shBuffer[cellIndex * 9 + 7] = vec4(shVals[7], 0);
		shBuffer[cellIndex * 9 + 8] = vec4(shVals[8], 0);
	#endif
	}

#if INTERPOLATE_SH && INTERPOLATE_2D
	for (int i = 0; i < 8; i++)
		color[i] = vec4(shResult[i], 0);
#endif
}

