#version 450

#define USE_SPHERICAL_HARMONICS 0
#define INTERPOLATE_SH 0
#define INTERPOLATE_2D 0

#define INDIRECT_SHADOWS 0
#define DEEP_SHADOWS 0

#define GRID_CELL_SIZE 1
#define STORE_LIGHT_DATA 1
#define USE_G_BUFFER 1

#define DEEP_RES 64

#define N_DEEPS 3

#define DEEP_COLS 1
#define DEEP_ROWS 1

// TODO: Try using the deep image to see what geometry is in shadow.

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#include "../utils.glsl"

#include "index.glsl"


#import "cluster"


#if STORE_LIGHT_DATA
	CLUSTER_DEC(cluster, float);
#else
	CLUSTER_DEC(cluster, uint);

	readonly buffer GlobalLightPos
	{
		vec4 globalLightPos[];
	};

	// FIXME: Necessary?
	readonly buffer GlobalLightDir
	{
		vec4 globalLightDir[];
	};
#endif


uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;

#if DEEP_SHADOWS
	readonly buffer LightMatrices
	{
		mat4 lightMatrices[];
	};

	// Whether this is shadow or not depends on how the texture array was built (ie, using GL_DEPTH_COMPONENT etc).
	uniform sampler2DArrayShadow lightDepths;
#endif

#if USE_SPHERICAL_HARMONICS
	#if INTERPOLATE_SH
		#if INTERPOLATE_2D
			#define TYPE sampler2D
		#else
			#define TYPE sampler3D
		#endif
		uniform TYPE shTex0;
		uniform TYPE shTex1;
		uniform TYPE shTex2;
		uniform TYPE shTex3;
		uniform TYPE shTex4;
		uniform TYPE shTex5;
		uniform TYPE shTex6;
		uniform TYPE shTex7;
		//uniform TYPE shTex8;
	#else
		readonly buffer SHBuffer
		{
			vec4 shBuffer[];
		};
	#endif
#endif


#if INDIRECT_SHADOWS
	#import "lfb"

	uniform int stacks;
	uniform int slices;

	readonly buffer DeepCams
	{
		mat4 deepCams[];
	};

	LFB_DEC(lfb, float);
#endif



#include "../light/light.glsl"

out vec4 fragColor;


#if USE_SPHERICAL_HARMONICS
	vec3 shVals[9];

	void applySHVals(vec3 normal, vec3 material)
	{
		// Final color.
		vec3 result = vec3(0);

		// Band 0.
		result += 0.282095 * shVals[0].xyz;

		// Band 1.
		result += -0.488603 * normal.y * shVals[1].xyz;
		result += 0.488603 * normal.z * shVals[2].xyz;
		result += -0.488603 * normal.x * shVals[3].xyz;

		// Band 2.
		result += 1.092548 * normal.x * normal.y * shVals[4].xyz;
		result += -1.092548 * normal.y * normal.z * shVals[5].xyz;
		result += 0.315392 * (3.0 * normal.z * normal.z - 1.0) * shVals[6].xyz;
		result += -1.092548 * normal.x * normal.z * shVals[7].xyz;
		// FIXME: I don't actually have a 9th value when interpolation.
		//result += 0.546274 * (normal.x * normal.x - normal.y * normal.y) * shVals[8].xyz;

		result *= material.xyz / 3.14159;

		// Multiplying by pi at the end more closely matches the other diffuse calculation.
		//result *= 3.14159;

		fragColor.xyz += result;
	}
#endif


#if INDIRECT_SHADOWS
	int getDeepImgIndex(vec3 osDir, bool reverseFlag)
	{
		float p = acos(osDir.y);
		float t = atan(osDir.z, osDir.x);
		if (t < 0)
			t += 2.0 * PI;

		int st = int(round((p / (0.5 * PI)) * float(stacks)));
		int sl = int(round((t / (2.0 * PI)) * float(slices)));

		//calcDeepDir(st, sl, reverseFlag);

		int index = sl * (stacks + 1) + st;
		return index;
	}
#if 0
	int getPixel(vec4 esPos)
	{
		//vec4 esPos = deepCams[index] * vec4(osPos, 1);

		// Projection is identity matrix.
		vec2 ssPos = getScreenFromClip(esPos, vec2(DEEP_RES, DEEP_RES));

		int index = int(int(ssPos.y) * DEEP_RES + int(ssPos.x));
		return index;
	}
#endif
	float calcLightRange(vec3 osPos, vec3 osDir)
	{
		// FIXME: This seems to be necessary due to there being way more fragments than necessary. Fix this!
		float delta = 0.3;

		// If osDir.y is negative, ray casting is reversed.
		bool reverseFlag = false;
	#if 1
		if (osDir.y < 0)
		{
			osDir = -osDir;
			reverseFlag = true;
		}
	#endif
		int index = getDeepImgIndex(osDir, reverseFlag);

		// Needs to be offset based on which deep image it is (since they're all in the one image).ivec2 imgSize = ivec2(DEEP_RES);
		ivec2 deepSize = imgSize * ivec2(DEEP_COLS, DEEP_ROWS);
		ivec2 imgSize = ivec2(DEEP_RES, DEEP_RES);

		vec4 esPos = deepCams[index] * vec4(osPos, 1);
		vec2 ssFrag = getScreenFromClip(esPos, vec2(imgSize.x, imgSize.y));

		int pixel = pixelIndex(i, ivec2(ssFrag), deepSize, DEEP_COLS, DEEP_RES);

		// Step through the fragment list to find the next fragment after the light.
		// Note: I scaled by 1/0.06 when rendering the multi-view deep image. Undo that here.
		float d = esPos.z / 0.06;

		// Stepping direction is reversed if reverseFlag is true.
		for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
		{
			// Get the fragment after the light.
			if (reverseFlag)
			{
			#if 1
				float z = LFB_GET_DATA_REV(lfb) / 0.06;
				//z /= 1.5;
				//vec2 data = LFB_GET_DATA_REV(lfb);
				//float z = data.y / 0.06;
				//vec3 pos = osPos + osDir * ((z / 0.06) + 10.0 * 1.05 + 11);
				if (d + delta < z)
					return -(d - z);
			#endif
				//return 0.0;
			}
			else
			{
			#if 1
				float z = LFB_GET_DATA(lfb) / 0.06;
				//z /= 1.5;
				//vec2 data = LFB_GET_DATA(lfb);
				//float z = data.y / 0.06;
				//vec3 pos = osPos + osDir * ((z / 0.06) + 10.0 * 1.05 + 11);
				if (z < d - delta)
					return -(z - d);
			#endif
				//return 0.0;
			}
		}

		// Didn't hit anything. Return a smallish light.
		return 2.0;
	}
#endif




void main()
{
	// Two possible approaches:
	// 1. Either apply all lights from the light grid.
	// 2. Or apply the spherical harmonics function.

	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	//fragColor = getAmbient(material, 0.2);
	//return;

#if DEEP_SHADOWS
	vec3 osFrag = (invMvMatrix * vec4(esFrag, 1)).xyz;
#endif

	vec3 viewDir = normalize(-esFrag);

	// Convert fragment into cluster space.
	int cluster = CLUSTER_GET(-esFrag.z, MAX_EYE_Z);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Get fragIndex in cluster space.
	int fragIndex = CLUSTER_GET_INDEX(cluster, cluster, gridCoord);

	vec4 testLightPos = vec4(0.5, 0.5, 0.5, 1);
	vec4 esLightPos = mvMatrix * vec4(testLightPos.xyz, 1);

	float u = gl_FragCoord.x / viewport.z;
	float v = gl_FragCoord.y / viewport.w;
	float w = (-depth / MAX_EYE_Z);
	//float w = 0;

	int count = 0;
	float directAtt = 0;
	float indirectAtt = 0;
	float directBright = 0.05;
	float indirectBright = 1.0;

	vec4 directColor = vec4(0);
	vec4 indirectColor = vec4(0);
	float att = 0;
	vec3 lightDir = esLightPos.xyz - esFrag.xyz;
	//fragColor += directionLight(material.xyz, vec4(1.0), lightDir, viewDir, normal) * 0.6;
	//return;

#if USE_SPHERICAL_HARMONICS
	#if INTERPOLATE_SH
		#if INTERPOLATE_2D
			vec2 coord = vec2(u, v);
			shVals[0] = texture2D(shTex0, coord).xyz;
			shVals[1] = texture2D(shTex1, coord).xyz;
			shVals[2] = texture2D(shTex2, coord).xyz;
			shVals[3] = texture2D(shTex3, coord).xyz;
			shVals[4] = texture2D(shTex4, coord).xyz;
			shVals[5] = texture2D(shTex5, coord).xyz;
			shVals[6] = texture2D(shTex6, coord).xyz;
			shVals[7] = texture2D(shTex7, coord).xyz;
			//shVals[8] = texture2D(shTex8, coord).xyz;
		#else
			vec3 coord = vec3(u, v, w);
			shVals[0] = texture(shTex0, coord).xyz;
			shVals[1] = texture(shTex1, coord).xyz;
			shVals[2] = texture(shTex2, coord).xyz;
			shVals[3] = texture(shTex3, coord).xyz;
			shVals[4] = texture(shTex4, coord).xyz;
			shVals[5] = texture(shTex5, coord).xyz;
			shVals[6] = texture(shTex6, coord).xyz;
			shVals[7] = texture(shTex7, coord).xyz;
			//shVals[8] = texture(shTex8, coord).xyz;
		#endif
		//fragColor = vec4(shVals[7], 1);
		//return;
	#else
		for (int i = 0; i < 9; i++)
			shVals[i] = shBuffer[fragIndex * 9 + i].xyz;
	#endif
	//fragColor = vec4(shVals[0], 1);

	applySHVals(normal.xyz, material.xyz);

	//fragColor.xyz = clamp(fragColor.xyz, vec3(0), vec3(1));
#else
	// Apply lighting for all lights at given cluster.
	for (CLUSTER_ITER_BEGIN(cluster, fragIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
	{
	#if STORE_LIGHT_DATA
		// Cluster stores full light data or index?
		float data = CLUSTER_GET_DATA(cluster);
		vec4 lightPos = CLUSTER_GET_POS(cluster);
		vec4 lightDir = CLUSTER_GET_DIR(cluster);
		vec4 lightColor = floatToRGBA8(data);
		float coneFlag = lightPos.w;
	#else
		uint id = CLUSTER_GET_DATA(cluster);
		vec4 lightPos = globalLightPos[id];
		vec4 lightDir = globalLightDir[id];
		float data = lightPos.w;
		lightPos.xyz = (mvMatrix * vec4(lightPos.xyz, 1)).xyz;
		lightDir.xyz = (mvMatrix * vec4(lightDir.xyz, 0)).xyz;
		vec4 lightColor = floatToRGBA8(data);
		float coneFlag = lightColor.w;
	#endif
		float lightRadius = lightDir.w;
		lightPos.w = 1;
		lightDir.w = 0;

		//if (dot(lightDir.xyz, esFrag - lightPos.xyz) < 0)
		//	continue;

		count++;
		float dist = distance(lightPos.xyz, esFrag);
		if ((coneFlag > 0 && dist < 0.1) || dist > lightRadius)
		{
			//count++;
			continue;
		}
		//lightPos.xyz -= lightDir.xyz * (lightRadius * 0.26);
		//count++;

	#if INDIRECT_SHADOWS
		// Anything between the light and the fragment?
		float fDist = calcLightRange(esFrag, lightPos.xyz - esFrag);
		// TODO: If fDist is less than dist then we're definitely in shadow.
		if (fDist < dist - 0.3)
			continue;
	#endif

	#if DEEP_SHADOWS
		// FIXME: You'll need multiple texture arrays bound, and you'll need to lookup the shadow map from the correct one.
		if (id >= 2048)
		{
			discard;
			return;
		}
	#endif

		vec4 coneDir = vec4(lightDir.xyz, 0.0);
		if (coneFlag > 0)
		{
			float visibility = 1.0;

		#if DEEP_SHADOWS
			// FIXME: For now, can only apply deep shadows if we're storing light indices.
			vec3 shadowFrag = (lightMatrices[id] * vec4(osFrag, 1)).xyz;

				// TODO: Need to lookup a cubemap rather than a single texture.
			#if 1
				visibility = 1.0;
				visibility -= texture(lightDepths, vec4(shadowFrag.xyz, id)).x;
			#else
				visibility = 0.3;
				visibility += texture(lightDepths, vec4(shadowFrag.xyz, id)).x;
			#endif
		#endif

			// TODO: Can also lookup the deep image to see if there is geometry between the light and the fragment.
			//float aperture = 0.392699;
			//float aperture = 0.785398;
			float att = 0;
			//indirectColor += coneLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist, att) * indirectBright;
			indirectColor += visibility * sphereLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, lightRadius, dist, false, att) * indirectBright;
			indirectAtt += att;
			//count++;
		}
		else
		{
			float att = 0;
			directColor += sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, lightRadius, dist, true, att) * directBright;
			directAtt += att;
			//count++;
		}
	}

	// Not sure if this is actually desirable or not...
	if (directAtt * directBright >= 1.0)
		directColor.xyz /= (directAtt * directBright);

	// Need to make sure indirect light doesn't make parts of the scene too bright...
	if (indirectAtt * indirectBright >= 1.0)
		indirectColor.xyz /= ((indirectAtt * indirectBright) / 1.0);

	fragColor.xyz += directColor.xyz;
	fragColor.xyz += indirectColor.xyz;
#endif

#if 0
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

