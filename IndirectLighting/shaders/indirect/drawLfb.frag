#version 450

#define DEEP_RES 64

#define N_DEEPS 3

#define DEEP_COLS 1
#define DEEP_ROWS 1

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

uniform int deepIndex;
uniform int pixelCheck;

out vec4 fragColor;

#import "lfb"

#include "../utils.glsl"

#include "index.glsl"

// Just standard forward rendering.
LFB_DEC(LFB_NAME, float);

void main()
{
	fragColor = vec4(1.0);

	ivec2 imgSize = ivec2(DEEP_RES);
	ivec2 deepSize = imgSize * ivec2(DEEP_COLS, DEEP_ROWS);

	int pixel = pixelIndex(deepIndex, ivec2(gl_FragCoord.xy), deepSize, DEEP_COLS, DEEP_RES);

#if 0
	if (pixel == pixelCheck)
	{
		fragColor = vec4(1, 0, 0, 1);
		return;
	}
#endif

	// Read from global memory and composite.
	// Might as well double check that it's in sorted order.
	float depthCheck = 1000;
	for (LFB_ITER_BEGIN(LFB_NAME, pixel); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		vec4 col = vec4(0, 0, 1, 0.3);
		float f = LFB_GET_DATA(LFB_NAME);
		//vec2 f = LFB_GET_DATA(LFB_NAME);
		//vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, 0.1);
	#if 1
		if (f > depthCheck)
		{
			fragColor = vec4(1, 0, 0, 1);
			return;
		}
		depthCheck = f;
	#endif
	}
}

