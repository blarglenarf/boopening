#version 430

// Shader seems pretty basic. Not sure what else I'd really need to do here.
#define MAX_EYE_Z 30.0

in FragData
{
	float esDepth;
} FragIn;

out float depth;

void main()
{
	//depth = (MAX_EYE_Z - FragIn.esDepth) / MAX_EYE_Z;
	depth = gl_FragCoord.z;
}

