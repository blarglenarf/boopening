#version 450

#define MAX_EYE_Z 30.0

// TODO: Instead of using this grid cell size, we could do 4x4 or 16x16, reading the list of lights a few times per-tile to save the final sh texture.
#define GRID_CELL_SIZE 32.0

#define INTERPOLATE_SH 1


uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 inMvMatrix;
uniform ivec4 viewport;

uniform mat4 fullInv;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;

//uniform ivec2 size;

//out vec4 color;


#include "../utils.glsl"


#if INTERPOLATE_SH
	uniform layout(rgba32f) image3D shTex0;
	uniform layout(rgba32f) image3D shTex1;
	uniform layout(rgba32f) image3D shTex2;
	uniform layout(rgba32f) image3D shTex3;
	uniform layout(rgba32f) image3D shTex4;
	uniform layout(rgba32f) image3D shTex5;
	uniform layout(rgba32f) image3D shTex6;
	uniform layout(rgba32f) image3D shTex7;
	//uniform layout(rgba32f) image3D shTex8;
#else
	coherent buffer SHBuffer
	{
		vec4 shBuffer[];
	};
#endif


// One set per-tile.
vec3 shVals[9];


vec4 lightColors[4];
vec4 lightPositions[4];
vec4 esLightPositions[4];

float att[4];
vec3 lightDirs[4];


void createSHVals(vec3 lightDir, vec3 lightCol, float intensity)
{
	lightCol *= (3.14159 * intensity);

	// This is done per-light.

	// Project lighting environment, convolve with cosine lobe.
	// TODO: This array may be unnecessary.
	vec3 shLight[9];

	// Band 0 (1.0).
	float b0 = 1.0;
	shLight[0] = 0.282095 * lightCol * b0;

	// Band 1 (2.0 / 3.0).
	float b1 = (2.0 / 3.0);
	shLight[1] = -0.488603 * lightDir.y * lightCol * b1;
	shLight[2] = 0.488603 * lightDir.z * lightCol * b1;
	shLight[3] = -0.488603 * lightDir.x * lightCol * b1;

	// Band 2 (1.0 / 4.0).
	float b2 = (1.0 / 4.0);
	shLight[4] = 1.092548 * lightDir.x * lightDir.y * lightCol * b2;
	shLight[5] = -1.092548 * lightDir.y * lightDir.z * lightCol * b2;
	shLight[6] = 0.315392 * (3.0 * lightDir.z * lightDir.z - 1.0) * lightCol * b2;
	shLight[7] = -1.092548 * lightDir.x * lightDir.z * lightCol * b2;
	shLight[8] = 0.546274 * (lightDir.x * lightDir.x - lightDir.y * lightDir.y) * lightCol * b2;

	// Adding each individual light's contribution.
	shVals[0] += shLight[0];
	shVals[1] += shLight[1];
	shVals[2] += shLight[2];
	shVals[3] += shLight[3];
	shVals[4] += shLight[4];
	shVals[5] += shLight[5];
	shVals[6] += shLight[6];
	shVals[7] += shLight[7];
	shVals[8] += shLight[8];
}

void clear()
{
	for (int i = 0; i < 9; i++)
	{
		shVals[i] = vec3(0);
	#if 0
		shBuffer[i] = vec4(0);
	#endif
	}

	// TODO: Clear the grid cells for this tile in the sh textures.
}


void main()
{
	// Gives pretty conclusive proof that single per-cell values are not sufficient.
	// Only alternative is a set of interpolated values per-cell.

	//color = vec4(0.0);

	// TODO: Should I just pick the middle of the grid cell or choose from the available depths?
	//float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;

	int index = viewport.z * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	// Per-fragment spherical harmonics seems correct. Or at least, correct-ish.

	// Just apply some random lighting.
	//vec3 material = vec3(1, 1, 1);

	lightColors[0] = vec4(1, 0, 0, 1);
	lightColors[1] = vec4(0, 1, 0, 1);
	lightColors[2] = vec4(0, 0, 1, 1);
	lightColors[3] = vec4(1, 0, 1, 1);

	// TODO: What if the lights are further away?
	lightPositions[0] = vec4(1, 1, 1, 1);
	lightPositions[1] = vec4(-1, -1, -1, 1);
	lightPositions[2] = vec4(-1, -1, 1, 1);
	lightPositions[3] = vec4(1, 1, -1, 1);

	// Get fragIndex, convert that to a coordinate.
	//int gridWidth = int(ceil(float(viewport.z) / GRID_CELL_SIZE));
	//int tileIndex = gridWidth * int(gl_FragCoord.y / GRID_CELL_SIZE) + int(gl_FragCoord.x / GRID_CELL_SIZE);
	//vec2 gridPos = vec2(tileIndex % gridWidth, tileIndex / gridWidth);
	ivec2 gridPos = ivec2(gl_FragCoord.xy);
	//ivec2 ssM = gridPos;

	ivec2 ssBL = gridPos * ivec2(GRID_CELL_SIZE); // Bottom left.
	ivec2 ssTL = ssBL + ivec2(0, GRID_CELL_SIZE); // Top left.
	ivec2 ssBR = ssBL + ivec2(GRID_CELL_SIZE, 0); // Bottom right.
	ivec2 ssTR = ssBL + ivec2(GRID_CELL_SIZE); // Top right.
	ivec2 ssM = ssBL + ivec2(GRID_CELL_SIZE / 2);

	float depthBL = texelFetch(depthTex, ssBL, 0).x;
	float depthTL = texelFetch(depthTex, ssTL, 0).x;
	float depthBR = texelFetch(depthTex, ssBR, 0).x;
	float depthTR = texelFetch(depthTex, ssTR, 0).x;
	float depthM = texelFetch(depthTex, ssM, 0).x;

	// Ideally we pick the middle depth, but if that's no good, grab one of the corners.
	float depth = depthM;
	ivec2 ssPos = ssM;
	if (depth == 0)
	{
		depth = depthBL;
		ssPos = ssBL;
	}
	if (depth == 0)
	{
		depth = depthTL;
		ssPos = ssTL;
	}
	if (depth == 0)
	{
		depth = depthBR;
		ssPos = ssBR;
	}
	if (depth == 0)
	{
		depth = depthTR;
		ssPos = ssTR;
	}

	ivec4 fullView = viewport * GRID_CELL_SIZE;
	vec3 esPos = getEyeFromWindow(vec3(ssPos.x, ssPos.y, depth), fullView, fullInv).xyz;

	clear();

	for (int i = 0; i < 4; i++)
	{
		vec4 lightPos = mvMatrix * lightPositions[i];
		vec3 lightCol = lightColors[i].xyz;
		float lightRadius = 2.0;

		float dist = distance(lightPos.xyz, esPos);
		float att = clamp(1.0 / (dist * dist), 0.0, 1.0);

		//if (dist > lightRadius)
		//	continue;

		vec3 dir = normalize(lightPos.xyz - esPos);

		createSHVals(dir, lightCol, att);
	}

#if INTERPOLATE_SH
	ivec3 coord = ivec3(gl_FragCoord.x, gl_FragCoord.y, -depth / MAX_EYE_Z);

	// FIXME: Band 3 is being ignored for some reason, not saying it's the g-buffer or anything...
	imageStore(shTex0, coord, vec4(shVals[0], 0));
	imageStore(shTex1, coord, vec4(shVals[1], 0));
	imageStore(shTex2, coord, vec4(shVals[2], 0));
	imageStore(shTex3, coord, vec4(shVals[3], 0));
	imageStore(shTex4, coord, vec4(shVals[4], 0));
	imageStore(shTex5, coord, vec4(shVals[5], 0));
	imageStore(shTex6, coord, vec4(shVals[6], 0));
	imageStore(shTex7, coord, vec4(shVals[7], 0));
	//imageStore(shTex8, coord, vec4(shVals[8], 0));
#else
	for (int i = 0; i < 9; i++)
		shBuffer[index * 9 + i] = vec4(shVals[i], 0);
#endif
}


