#version 430

#define LIGHT_RES 256

uniform sampler2D dirTex;
uniform sampler2D posTex;

buffer LightPositions
{
	vec4 lightPositions[];
};

buffer LightDirections
{
	vec4 lightDirections[];
};


void main()
{
	int light = int(gl_VertexID);

	// These could be the other way around, shouldn't make a difference though.
	int lightX = light % LIGHT_RES;
	int lightY = light / LIGHT_RES;
	ivec2 texCoord = ivec2(lightX, lightY);

	vec4 lightDir = vec4(texelFetch(dirTex, texCoord, 0).xyz, 0);
	//vec3 lightCol = vec4(texelFetch(colorTex, texCoord, 0).xyz, 1);
	vec4 lightPos = vec4(texelFetch(posTex, texCoord, 0).xyz, 1);

	lightPositions[light] = lightPos;
	lightDirections[light] = lightDir;
}

