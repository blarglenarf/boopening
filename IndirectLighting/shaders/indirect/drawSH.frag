#version 450

#define MAX_EYE_Z 30.0

#define GRID_CELL_SIZE 32.0

#define INTERPOLATE_SH 1


uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 inMvMatrix;
uniform ivec4 viewport;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;

#if INTERPOLATE_SH
	uniform sampler3D shTex0;
	uniform sampler3D shTex1;
	uniform sampler3D shTex2;
	uniform sampler3D shTex3;
	uniform sampler3D shTex4;
	uniform sampler3D shTex5;
	uniform sampler3D shTex6;
	uniform sampler3D shTex7;
	//uniform sampler3D shTex8;
#else
	coherent buffer SHBuffer
	{
		vec4 shBuffer[];
	};
#endif

uniform ivec2 size;

out vec4 color;


#include "../utils.glsl"


vec3 shVals[9];


void applySHVals(vec3 normal, vec3 material)
{
	// Final color.
	vec3 result = vec3(0);

	// Band 0.
	result += 0.282095 * shVals[0].xyz;

	// Band 1.
	result += -0.488603 * normal.y * shVals[1].xyz;
	result += 0.488603 * normal.z * shVals[2].xyz;
	result += -0.488603 * normal.x * shVals[3].xyz;

	// Band 2.
	result += 1.092548 * normal.x * normal.y * shVals[4].xyz;
	result += -1.092548 * normal.y * normal.z * shVals[5].xyz;
	result += 0.315392 * (3.0 * normal.z * normal.z - 1.0) * shVals[6].xyz;
	result += -1.092548 * normal.x * normal.z * shVals[7].xyz;
	// Can't use this atm when interpolating since there's no 9th texture.
	//result += 0.546274 * (normal.x * normal.x - normal.y * normal.y) * shVals[8].xyz;

	color.xyz = result * material.xyz / 3.14159;

	// Multiplying by pi at the end more closely matches the other diffuse calculation.
	color.xyz *= 3.14159;
}


void main()
{
	color = vec4(0, 0, 0, 1);

	float u = gl_FragCoord.x / float(viewport.z);
	float v = gl_FragCoord.y / float(viewport.w);

	float depth = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).x;
	float w = -depth / MAX_EYE_Z;

	vec2 tile = gl_FragCoord.xy / vec2(GRID_CELL_SIZE);
	int shIndex = int(size.x) * int(tile.y) + int(tile.x);
	//int fragIndex = int(viewport.z) * int(gl_FragCoord.y) + int(gl_FragCoord.x);

#if INTERPOLATE_SH
	shVals[0] = texture(shTex0, vec3(u, v, w)).xyz;
	shVals[1] = texture(shTex1, vec3(u, v, w)).xyz;
	shVals[2] = texture(shTex2, vec3(u, v, w)).xyz;
	shVals[3] = texture(shTex3, vec3(u, v, w)).xyz;
	shVals[4] = texture(shTex4, vec3(u, v, w)).xyz;
	shVals[5] = texture(shTex5, vec3(u, v, w)).xyz;
	shVals[6] = texture(shTex6, vec3(u, v, w)).xyz;
	shVals[7] = texture(shTex7, vec3(u, v, w)).xyz;
	//shVals[8] = texture(shTex8, vec3(u, v, 0)).xyz;
#else
	for (int i = 0; i < 9; i++)
		shVals[i] = shBuffer[shIndex * 9 + i].xyz;
#endif

	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec3 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0).xyz;

	//float v = (gl_FragCoord.y - (tile.y * GRID_CELL_SIZE)) / float(GRID_CELL_SIZE); // Bottom.
	//float u = (gl_FragCoord.x - (tile.x * GRID_CELL_SIZE)) / float(GRID_CELL_SIZE); // Left.
	//vec2 bl = tile * vec2(GRID_CELL_SIZE);
	//float u = (gl_FragCoord.x - bl.x) / float(GRID_CELL_SIZE);
	//float v = (gl_FragCoord.y - bl.y) / float(GRID_CELL_SIZE);

	applySHVals(normal, material);
	color.xyz *= 2.0;
}

