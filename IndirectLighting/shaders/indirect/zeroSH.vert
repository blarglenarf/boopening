#version 450

#define INTERPOLATE_2D 0

uniform ivec3 size;

#if INTERPOLATE_2D
	#define TYPE image2D
#else
	#define TYPE image3D
#endif

uniform layout(rgba32f) TYPE shTex0;
uniform layout(rgba32f) TYPE shTex1;
uniform layout(rgba32f) TYPE shTex2;
uniform layout(rgba32f) TYPE shTex3;
uniform layout(rgba32f) TYPE shTex4;
uniform layout(rgba32f) TYPE shTex5;
uniform layout(rgba32f) TYPE shTex6;
uniform layout(rgba32f) TYPE shTex7;
//uniform layout(rgba32f) TYPE shTex8;

void main()
{
	int index = gl_VertexID;

	vec2 cPos = vec2(index % size.x, index / size.x);

#if INTERPOLATE_2D
	ivec2 coord = ivec2(cPos.x, cPos.y);
#else
	for (int z = 0; z < size.z; z++)
	{
		ivec3 coord = ivec3(cPos.x, cPos.y, z);
#endif
		imageStore(shTex0, coord, vec4(0));
		imageStore(shTex1, coord, vec4(0));
		imageStore(shTex2, coord, vec4(0));
		imageStore(shTex3, coord, vec4(0));
		imageStore(shTex4, coord, vec4(0));
		imageStore(shTex5, coord, vec4(0));
		imageStore(shTex6, coord, vec4(0));
		imageStore(shTex7, coord, vec4(0));
		//imageStore(shTex8, coord, vec4(0));
#if !INTERPOLATE_2D
	}
#endif
}

