#version 430

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex;
layout(location = 3) in vec3 tangent;

out vec3 osVert;

out VertexData
{
	vec3 osVert;
} VertexOut;

void main()
{
	VertexOut.osVert = vertex;
}

