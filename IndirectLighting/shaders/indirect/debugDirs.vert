#version 430

#define LIGHT_RES 64
#define DEEP_RES 64

#define N_DEEPS 3

#define DEEP_COLS 1
#define DEEP_ROWS 1

#import "lfb"

out vec4 dir;
out vec4 pos;
out vec4 col;

out vec4 deepDir;
out float range;


//uniform sampler2D dirTex;
//uniform sampler2D colTex;
//uniform sampler2D posTex;

uniform int stacks;
uniform int slices;


readonly buffer DeepCams
{
	mat4 deepCams[];
};


// TODO: Read data from these buffers instead, so we can see what we're actually dealing with.
readonly buffer GlobalLightPos
{
	vec4 globalLightPos[];
};

readonly buffer GlobalLightDir
{
	vec4 globalLightDir[];
};



#include "../utils.glsl"

#include "../lfb/tiles.glsl"

#include "index.glsl"


LFB_DEC(lfb, float);


#if 0
void calcDeepDir(int stack, int slice, bool reverseFlag)
{
	float theta = (float(slice) / float(slices)) * 2.0 * PI;
	float phi = (float(stack) / float(stacks)) * 0.5 * PI;

	float x = cos(theta) * sin(phi);
	float z = sin(theta) * sin(phi);
	float y = cos(phi);

	if (!reverseFlag)
	{
		x = -x;
		y = -y;
		z = -z;
	}

	deepDir = vec4(x, y, z, 0);
}


int getDeepImgIndex(vec3 osDir, bool reverseFlag)
{
	float p = acos(osDir.y);
	float t = atan(osDir.z, osDir.x);
	if (t < 0)
		t += 2.0 * PI;

	int st = int(round((p / (0.5 * PI)) * float(stacks)));
	int sl = int(round((t / (2.0 * PI)) * float(slices)));

	calcDeepDir(st, sl, reverseFlag);

	int index = sl * (stacks + 1) + st;
	return index;
}

int getPixel(vec4 esPos)
{
	//vec4 esPos = deepCams[index] * vec4(osPos, 1);

	// Projection is identity matrix.
	vec2 ssPos = getScreenFromClip(esPos, vec2(DEEP_RES, DEEP_RES));

	int index = int(int(ssPos.y) * DEEP_RES + int(ssPos.x));
	return index;
}
#endif

float calcLightRange(vec3 osPos, vec3 osDir)
{
	// FIXME: This seems to be necessary due to there being way more fragments than necessary. Fix this!
	float delta = 1.0;

	// If osDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
#if 1
	if (osDir.y < 0)
	{
		osDir = -osDir;
		reverseFlag = true;
	}
#endif
	//int index = getDeepImgIndex(osDir);
	int index = getDeepImgIndex(osDir, stacks, slices);
	//if (reverseFlag)
	//	osDir = -osDir;

	ivec2 imgSize = ivec2(DEEP_RES);
	ivec2 deepSize = imgSize * ivec2(DEEP_COLS, DEEP_ROWS);

	vec4 esPos = deepCams[index] * vec4(osPos, 1);
	vec2 ssFrag = getScreenFromClip(esPos, vec2(imgSize.x, imgSize.y));
	int pixel = pixelIndex(index, ivec2(ssFrag), deepSize, DEEP_COLS, DEEP_RES);

	// Step through the fragment list to find the next fragment after the light.
	// Note: I scaled by 1/0.06 when rendering the multi-view deep image. Undo that here.
	float d = esPos.z / 0.06;

	// Stepping direction is reversed if reverseFlag is true (should that be the other way around?).
	float m = 0;

	// Note: This iterates from positive z to negative z, which is the same order as a negative direction vector.
	// FIXME: Figure out which one of these should step in reverse!
	for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			float f = LFB_GET_DATA_REV(lfb) / 0.06;
			if (d + delta < f)
				return (d - f);
			m = f;
		}
		else
		{
			float f = LFB_GET_DATA(lfb) / 0.06;
			if (f < d - delta)
				return (f - d);
			m = f;
		}
	}

	// We reached the end :(
	// TODO: Ideally this means no light needs to be spawned here.
	return 0.0;
}



void main()
{
	int light = int(gl_VertexID);

	// These could be the other way around, shouldn't make a difference though.
	//int lightX = light % LIGHT_RES;
	//int lightY = light / LIGHT_RES;
	//ivec2 texCoord = ivec2(lightX, lightY);
	//dir = vec4(texelFetch(dirTex, texCoord, 0).xyz, 0);
	//pos = vec4(texelFetch(posTex, texCoord, 0).xyz, 1);
	//col = vec4(texelFetch(colTex, texCoord, 0).xyz, 1);

	dir = globalLightDir[light];
	pos = globalLightPos[light];
	float tmpRange = -dir.w;

	dir.w = 0;
	pos.w = 1;

	range = calcLightRange(pos.xyz, dir.xyz);
	//range = tmpRange;

	col = vec4(1, 0, 0, 1);
}

