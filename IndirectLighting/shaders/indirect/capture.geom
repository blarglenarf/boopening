#version 450

#define BUILD_BRUTE_FORCE 0

layout(triangles) in;

// One triangle per view dir is too much.
layout(triangle_strip, max_vertices = 3) out;

in vec3 osVert[3];
in vec2 texCoord[3];

#if BUILD_BRUTE_FORCE
	readonly buffer DeepCams
	{
		mat4 deepCams[];
	};

	uniform int deepIndex;

	out VertexData
	{
		vec3 esFrag;
		vec2 texCoord;
		//vec3 normal;
	} VertexOut;
#else
	out VertexData
	{
		vec3 osFrag;
		vec2 texCoord;
		//vec3 normal;
	} VertexOut;
#endif


uniform mat4 mvMatrix;

#if 0
// TODO: These will need to be a buffer of matrices. Could alternatively calculate these in the shader.
uniform mat4 mvMatrix0;
uniform mat4 mvMatrix1;
uniform mat4 mvMatrix2;


// Projections are identity matrices.
#define TRI(mvMatrix, layer) \
	for (int i = 0; i < 3; i++) \
	{ \
		esVert = mvMatrix * vec4(osVert[i], 1); \
		VertexOut.esFrag = esVert.xyz; \
		VertexOut.texCoord = texCoord[i]; \
		gl_Position = esVert; \
		gl_Layer = layer; \
		EmitVertex(); \
	} \
	EndPrimitive();
#endif


void main()
{
	vec4 esVert;

#if BUILD_BRUTE_FORCE
	mat4 deepMat = deepCams[deepIndex];

	for (int i = 0; i < gl_in.length(); i++)
	{
		esVert = deepMat * vec4(osVert[i], 1);
		VertexOut.esFrag = esVert.xyz;
		VertexOut.texCoord = texCoord[i];
		gl_Position = esVert; // FIXME: This assumes an identity matrix with no projection.
		EmitVertex();
	}
	EndPrimitive();
#else
	vec3 osNorm = normalize(cross(osVert[0] - osVert[1], osVert[2] - osVert[1]));

	// Which axis does the primitive face?
	vec3 xAxis = vec3(1, 0, 0);
	vec3 yAxis = vec3(0, 1, 0);
	vec3 zAxis = vec3(0, 0, 1);

	// Swizzle the components based on which dot product is the largest.
	float dpX = abs(dot(xAxis, osNorm));
	float dpY = abs(dot(yAxis, osNorm));
	float dpZ = abs(dot(zAxis, osNorm));

	// X-axis major, swap x and z.
	bool xFlag = dpX > dpY && dpX > dpZ;

	// Y-axis major, swap y and z.
	bool yFlag = dpY > dpX && dpY > dpZ;

	for (int i = 0; i < gl_in.length(); i++)
	{
		vec4 osVertSwiz = vec4(osVert[i], 1);
		VertexOut.osFrag = osVertSwiz.xyz;
		VertexOut.texCoord = texCoord[i];

		// Note: This assumes the geometry is being rendered along the z-axis.
		if (xFlag)
			osVertSwiz.xyzw = osVertSwiz.zyxw;
		else if (yFlag)
			osVertSwiz.xyzw = osVertSwiz.xzyw;

		vec4 esVert = mvMatrix * osVertSwiz;
		gl_Position = esVert;
		//vec4 esVert = mvMatrix * osVert;
		//vec4 csVert = pMatrix * esVert;
		//gl_Position = csVert;
		EmitVertex();
	}
	EndPrimitive();
#endif
}

