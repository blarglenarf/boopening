#version 450

// Billboarded quad or sphere?
// Note: Billboarded quads seem to be much faster, even though they result in more pixels covered.
#define MAX_EYE_Z 30.0
#define LIGHT_DIST 3.0

layout(points) in;

layout(line_strip, max_vertices = 2) out;

in vec4 dir[1];
in vec4 pos[1];
in vec4 col[1];

out vec4 fragCol;


uniform mat4 mvMatrix;
uniform mat4 pMatrix;



void main()
{
	float size = LIGHT_DIST;
	vec4 end = pos[0] + dir[0] * size;

	vec4 esPos = mvMatrix * pos[0];
	vec4 csPos = pMatrix * esPos;

	vec4 esEnd = mvMatrix * end;
	vec4 csEnd = pMatrix * esEnd;

	fragCol = col[0];
	gl_Position = csPos;
	EmitVertex();

	fragCol = col[0];
	gl_Position = csEnd;
	EmitVertex();

	EndPrimitive();
}

