#version 450

#define GRID_CELL_SIZE 1
#define STORE_LIGHT_DATA 1
#define USE_G_BUFFER 1


// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "cluster"

#include "../utils.glsl"

CLUSTER_DEC(cluster, float);

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;


#include "../light/light.glsl"

out vec4 fragColor;



void main()
{
	vec4 color = vec4(0, 0, 0, 1);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	//color = getAmbient(material, 0.2);
	color = vec4(0);
	//fragColor = color;
	//return;

	vec3 viewDir = normalize(-esFrag);

	// Convert fragment into cluster space.
	int cluster = CLUSTER_GET(-esFrag.z, MAX_EYE_Z);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Get fragIndex in cluster space.
	int fragIndex = CLUSTER_GET_INDEX(cluster, cluster, gridCoord);

	vec4 testLightPos = vec4(1.27416, 5.84406, -0.638992, 1);
	vec4 esLightPos = mvMatrix * testLightPos;

	int count = 0;

	// Apply lighting for all lights at given cluster.
	for (CLUSTER_ITER_BEGIN(cluster, fragIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
	{
		float data = CLUSTER_GET_DATA(cluster);
		vec4 lightPos = CLUSTER_GET_POS(cluster);
		vec4 lightDir = CLUSTER_GET_DIR(cluster);
		vec4 lightColor = floatToRGBA8(data);
		float coneFlag = lightPos.w;
		float lightRadius = lightDir.w;
		lightPos.w = 1;
		lightDir.w = 0;

		// Cone.
		float dist = distance(lightPos.xyz, esFrag);

		if (dist > lightRadius)
		{
			//count++;
			continue;
		}
		count++;

		vec4 coneDir = vec4(lightDir.xyz, 0.0);
		if (coneFlag > 0)
		{
			//float aperture = 0.392699;
			float aperture = 0.785398;
			color += coneLight(material.xyz, lightColor, lightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist) * 0.5;
		}
		else
		{
			color += sphereLight(material.xyz, vec4(1), esLightPos, esFrag, normal, viewDir, lightRadius, dist) * 0.1;
		}
	}

#if 0
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

	fragColor = color;
}

