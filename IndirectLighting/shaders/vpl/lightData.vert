#version 430

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;
layout(location = 3) in vec3 tangent;

uniform vec3 lightPos;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;


out VertexData
{
	vec3 normal;
	vec3 osFrag; // TODO: Might want to change this to world space later.
	vec3 esFrag;
	vec2 texCoord;
	mat3 tbn;
} VertexOut;


void main()
{
	vec3 binormal = cross(normal, tangent);

	vec3 T = normalize(nMatrix * tangent);
	vec3 B = normalize(nMatrix * binormal);
	vec3 N = normalize(nMatrix * normal);
	mat3 TBN = mat3(T, B, N);
	VertexOut.tbn = TBN;

	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

	//vLightDir = lightDir;//(mvMatrix * vec4(lightPos, 0)).xyz;
	// FIXME: Normals need to be in object space!
	//VertexOut.normal = nMatrix * normalize(normal);
	VertexOut.normal = normalize(normal);
	VertexOut.osFrag = osVert.xyz;
	VertexOut.esFrag = esVert.xyz;

	VertexOut.texCoord = texCoord;

	gl_Position = csVert;
}

