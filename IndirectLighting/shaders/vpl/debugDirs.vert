#version 430

#define LIGHT_RES 64

uniform sampler2D dirTex;
uniform sampler2D colTex;
uniform sampler2D posTex;

out vec4 dir;
out vec4 pos;
out vec4 col;

void main()
{
	int light = int(gl_VertexID);

	// These could be the other way around, shouldn't make a difference though.
	int lightX = light % LIGHT_RES;
	int lightY = light / LIGHT_RES;
	ivec2 texCoord = ivec2(lightX, lightY);

	dir = vec4(texelFetch(dirTex, texCoord, 0).xyz, 0);
	pos = vec4(texelFetch(posTex, texCoord, 0).xyz, 1);
	//col = vec4(texelFetch(colTex, texCoord, 0).xyz, 1);
	col = vec4(1, 0, 0, 1);
}

