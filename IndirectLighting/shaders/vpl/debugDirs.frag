#version 430

in vec4 fragCol;

out vec4 fragColor;

void main()
{
	fragColor = fragCol;
}

