#pragma once

#include "Lighting/Lighting.h"

#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/RenderResource/Mesh.h"
#include "../../Renderer/src/Lighting/Bloom.h"
#include "../../Renderer/src/Profiler.h"
#include "../../Renderer/src/Camera.h"

class App
{
private:
	int currMesh;
	int currLighting;

	int tmpWidth;
	int tmpHeight;

	bool hudFlag;

	Rendering::Shader basicShader;

	Rendering::GPUMesh gpuMesh;

	Rendering::Profiler profiler;

	Lighting *lighting;

public:
	App() : currMesh(0), currLighting(0), tmpWidth(0), tmpHeight(0), hudFlag(true), lighting(nullptr) {}
	~App() {}

	void init();

	void setTmpViewport(int width, int height);
	void restoreViewport();

	void drawMesh();
	void drawMesh(Rendering::Camera &camera);

	void render();
	void update(float dt);

	int useMesh(int currMesh);
	void loadNextMesh();
	void loadPrevMesh();

	int useLighting(int currLighting);
	void useNextLighting();
	void usePrevLighting();

	void setLightPos();

	void reloadShaders();
	void saveData();

	void playAnim();
	void runBenchmarks();

	Rendering::GPUMesh &getGpuMesh() { return gpuMesh; }
	Rendering::Profiler &getProfiler() { return profiler; }

	void toggleHud() { hudFlag = !hudFlag; }
};

