#include "Indirect.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"
#include "../../../Utils/src/Random.h"

#include "../../../Resources/src/ResourcesCommon.h"
#include "../../../Platform/src/Window.h"

#include "../../../Renderer/src/LFB/LFBLinear.h"
#include "../../../Renderer/src/LFB/LFBList.h"

#include <string>
#include <cassert>
#include <bitset>

// TODO: Add shadow maps for each VPL. Compare visual quality with infinite range lights.


// TODO: Deep image masking. Use a mask of directions and pixels to determine which data should be saved in the deep image.
// TODO: Start with a huge number of possible directions.
// TODO: Figure out which directions are actually important, only save those.
// TODO: Of those directions, figure out which pixels are actually important, only save those.
// TODO: Can we index the data in some way that isn't per-pixel?
// TODO: First store an id per-direction, which gives the specific deep image present in the multi-view deep image.
// TODO: Second we need an index per-pixel. This may be too much data, may need to find an alternative.

// DONE: What happens when you store indices rather than full data?
// TODO: Get rid of the extra unnecessary data in the light grid (eg directions, we're not using those anymore).

// DONE: Animating light -- looks horrible!
// DONE: Animating geometry -- still get some flickering but not nearly as bad.
// TODO: Need to make sure russian roulette selects based on light position, so the same positions will typically be kept.

// TODO: An actual brdf will hopefully remove the flickering artifacts... maybe...
// TODO: Spheres seem much better, but they light geometry that's behind...
// TODO: Can just stick to glossy reflections using a single ray direction... maybe?

// TODO: Z division based on per-tile depths, not an absolute depth value.
// TODO: Min/max depths can be retrieved from the g-buffer. Division then performed based on those.
// TODO: Not sure if that would actually improve performance or not.

// DONE: Contributions need to sum to 1.
// DONE: Overlapping cones giving different contributions.

// TODO: Add pixel stepping for few deep images, compare result both for performance and visually.

#define LIGHT_RES 256
#define BASIC_INDEXING 1 // TODO: Figure out why non-basic indexing causes flickering. Potentially a big deal for raytracing!!!!
#define PIXEL_STEPPING 0
#define DEEP_RES 64
#define DEEP_MAX_RES 1024
#define N_DEEPS 144 // Note: There are more than 144 directions, since it's actually (stacks + 1) * slices.
#define BUILD_BRUTE_FORCE 0
#define CONSERVATIVE 0
//#define N_DEEPS 9

#define GRID_CELL_SIZE 32
#define MAX_EYE_Z 30.0
#define CLUSTERS 32
#define MASK_SIZE (CLUSTERS / 32)
#define STORE_LIGHT_DATA 0
#define DEEP_MASKING 0
#define USE_POS_GRID 0
#define CLUSTER_POS_RES 64 // Once you get to a grid size of 64 you start to see some cells being shared, but not many.

#define DEEP_SHADOWS 0 // TODO: These aren't really deep shadows, are they?
#define DEEP_SHADOW_RES 512
#define CREATE_LIGHTS_ONCE 0 // Need to only build the global light data once for this to work.

#define DRAW_DEEPS 0
#define USE_SPHERICAL_HARMONICS 0
#define INTERPOLATE_SH 0
#define INTERPOLATE_2D 0
#define TEST_SPHERICAL_HARMONICS 0
#define TEST_LIGHT_VECTORS 0
#define INDIRECT_SHADOWS 0 // Drops the framerate to around 10fps, with a bunch of artifacts.
#define ANIM_LIGHT 0
#define LIGHT_SPEED 0.2
#define ANIM_SPHERES 0
#define DRAW_SPHERES 0
#define N_SPHERES 32
#define SPHERE_SIZE 0.5
#define SPHERE_SPEED 2.0
#define COLLISION_TIME 0.5

using namespace Rendering;
using namespace Math;


// FIXME: Seems to result in intersections not on the line.
bool testTriangleRayIntersection(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 end, vec3 &pos)
{
	// If we don't normalise this, then we know t values > 1 will be past the end point.
	vec3 d = (end - o);

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	vec3 h = d.cross(e2);
	float a = e1.dot(h);

	if (a > -0.00001 && a < 0.0001)
	{
		return false;
	}

	float f = 1.0f / a;
	vec3 s = o - v0;
	float u = f * s.dot(h);

	if (u < 0.0 || u > 1.0)
	{
		return false;
	}

	vec3 q = s.cross(e1);
	float v = f * d.dot(q);

	if (v < 0.0 || u + v > 1.0)
	{
		return false;
	}

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * e2.dot(q);

	// Intersection.
	if (t > 0.00001 && t < 1)
	{
		pos = o + (d * t);
		return true;
	}

	// Line intersection but not ray intersection.
	else
	{
		return false;
	}

	return false;
}

vec4 calcDeepDir(int stack, int slice, bool reverseFlag)
{
	int stacks = (int) sqrtf(N_DEEPS);
	int slices = (int) sqrtf(N_DEEPS);

	float theta = (float(slice) / float(slices)) * 2.0f * (float) M_PI;
	float phi = (float(stack) / float(stacks)) * 0.5f * (float) M_PI;

	float x = cos(theta) * sin(phi);
	float z = sin(theta) * sin(phi);
	float y = cos(phi);

	if (reverseFlag)
	{
		x = -x;
		y = -y;
		z = -z;
	}

	vec4 deepDir = vec4(x, y, z, 0);
	return deepDir;
}

std::pair<int, vec4> getDeepImgIndex(vec3 osDir, bool reverseFlag)
{
	int stacks = (int) sqrtf(N_DEEPS);
	int slices = (int) sqrtf(N_DEEPS);

	float p = acosf(osDir.y);
	float t = atan2(osDir.z, osDir.x);
	if (t < 0)
		t += 2.0f * (float) M_PI;

	int st = int(round((p / (0.5f * (float) M_PI)) * float(stacks)));
	int sl = int(round((t / (2.0f * (float) M_PI)) * float(slices)));

	vec4 deepDir = calcDeepDir(st, sl, reverseFlag);

	int index = sl * (stacks + 1) + st;
	return {index, deepDir};
}

vec2 getScreenFromClip(vec4 csPos, vec2 size)
{
	return vec2(((vec2(csPos.xy / csPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(size.x, size.y));
}

#if 0
int getPixel(vec4 esPos)
{
	//vec4 esPos = deepCams[index] * vec4(osPos, 1);

	// Projection is identity matrix.
	vec2 ssPos = getScreenFromClip(esPos, vec2(DEEP_RES, DEEP_RES));

	int index = int((int) ssPos.y * DEEP_RES + (int) ssPos.x);
	return index;
}
#endif

vec4 getEyeFromWindow(vec3 p, ivec4 viewport, mat4 invPMatrix)
{
	vec3 ndcPos;
	ndcPos.xy = ((p.xy - vec2(viewport.x, viewport.y)) / vec2(viewport.z, viewport.w)) * 2.0 - 1.0;
	//ndcPos.z = 1.0;
	ndcPos.z = p.z;

	vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0);
	eyeDir /= eyeDir.w;
	vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0);
	return eyePos;
}

static void collide(vec3 &pos, vec3 &dir, float &colTime, float dt)
{
	colTime -= dt;
	if (colTime < 0)
		colTime = 0;
	if (colTime > 0)
		return;

	pos.x = clamp(pos.x, -10.0f, 10.0f);
	pos.y = clamp(pos.y, -5.0f, 5.0f);
	pos.z = clamp(pos.z, -10.0f, 10.0f);

	// FIXME: For now this just assumes we're using the atrium.
	// Which edge did we hit?
	if (pos.x <= -10)
	{
		dir.reflect(vec3(1, 0, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.x >= 10)
	{
		dir.reflect(vec3(-1, 0, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.y <= -5)
	{
		dir.reflect(vec3(0, 1, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.y >= 5)
	{
		dir.reflect(vec3(0, -1, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.z <= -10)
	{
		dir.reflect(vec3(0, 0, 1));
		colTime = COLLISION_TIME;
	}

	else if(pos.z >= 10)
	{
		dir.reflect(vec3(0, 0, -1));
		colTime = COLLISION_TIME;
	}
}



void Indirect::createCameras()
{
	stacks = (int) sqrt(N_DEEPS);
	slices = (int) sqrt(N_DEEPS);

	// FIXME: I've noticed that indexing is going up to 13x13 rather than 12x12...
	//deepCams.resize((stacks + 1) * slices);
	deepCams.resize((stacks + 1) * (slices + 1));

	std::vector<mat4> matrices;
	//matrices.resize((stacks + 1) * slices);
	matrices.resize((stacks + 1) * (slices + 1));

	int index = 0;
	for (int slice = 0; slice <= slices; slice++)
	{
		for (int stack = 0; stack <= stacks; stack++) // Need the bottom row.
		{
			float theta = ((float) slice / (float) slices) * 2.0f * (float) M_PI;
			float phi = ((float) stack / (float) stacks) * 0.5f * (float) M_PI;
			float x = cosf(theta) * sinf(phi);
			float z = sinf(theta) * sinf(phi);
			float y = cosf(phi);

			// TODO: Reposition the cameras so that no zooming is required.
			// TODO: If we don't need to zoom anything, we get much better precision and don't need to account for it in the fragment shader.
		#if 1
			deepCams[index].setType(Camera::Type::NORMAL);
			deepCams[index].setZoom(0.06f); // FIXME: This zoom doesn't cover everything :(
			deepCams[index].setViewport(0, 0, DEEP_RES, DEEP_RES);
			deepCams[index].setViewDir({x, y, z});
			deepCams[index].update(0.1f);
		#else
			float wsMin = -15;
			float wsMax = 15;
			float scale = (float) DEEP_RES / ((float) wsMax - (float) wsMin);

			deepCams[index].setType(Camera::Type::ORTHO);
			deepCams[index].setDistance(0, (float) DEEP_RES);
			deepCams[index].setPos({wsMin, wsMin, -wsMin});
			deepCams[index].setZoom(scale);
			deepCams[index].setViewport(0, 0, DEEP_RES, DEEP_RES);
			deepCams[index].setViewDir({x, y, z});
			deepCams[index].update(0.1f);
		#endif

			matrices[index] = deepCams[index].getInverse();

			index++;
		}
	}
#if 0
	for (size_t i = 0; i < deepCams.size(); i++)
	{
		deepCams[i].setType(Camera::Type::NORMAL);
		deepCams[i].setZoom(0.1f);
		deepCams[i].setViewport(0, 0, DEEP_RES, DEEP_RES);
		//deepCams[i].setViewDir({1, 0, 0});
	}
	deepCams[0].setViewDir({1, 0, 0});
	deepCams[1].setViewDir({0, 1, 0});
	deepCams[2].setViewDir({0, 0, 1});
	for (auto &cam : deepCams)
		cam.update(0.1f);
#endif

	deepCamBuffer.release();
	deepCamBuffer.create(&matrices[0], matrices.size() * sizeof(mat4));

#if 1
	deepCamZ.setType(Camera::Type::NORMAL);
	deepCamZ.setZoom(0.06f);
	deepCamZ.setViewport(0, 0, DEEP_RES, DEEP_RES);
	deepCamZ.update(0.1f);
#else
	float wsMin = -15;
	float wsMax = 15;
	float scale = (float) DEEP_RES / ((float) wsMax - (float) wsMin);

	deepCamZ.setType(Camera::Type::ORTHO);
	deepCamZ.setDistance(0, (float) DEEP_RES);
	deepCamZ.setPos({wsMin, wsMin, -wsMin});
	deepCamZ.setZoom(scale);
	deepCamZ.setViewport(0, 0, DEEP_RES, DEEP_RES);
	deepCamZ.update(0.1f);
#endif
}

void Indirect::drawCameraDirs(App *app)
{
	(void) (app);

	// Cycle through the different cameras and render to make sure they're all correct.
	static int i = 0;
	static float dt = 0;
	auto &cam = deepCams[i];

	dt += Platform::Window::getDeltaTime();
	if (dt > 2)
	{
		i++;
		if (i >= (int) deepCams.size())
			i = 0;
		dt = 0;
	}

	//auto &cam = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT | GL_CURRENT_BIT);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glPointSize(4);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glLoadMatrixf(cam.getProjection());

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glLoadMatrixf(cam.getInverse());

	glBegin(GL_POINTS);
#if 1
	// Top hemisphere, uniformly sampled.
	float currIndex = 0;
	Utils::seedRand(0);
	for (int slice = 0; slice < slices; slice++)
	{
		float r = Utils::getRand(0.0f, 1.0f);
		float g = Utils::getRand(0.0f, 1.0f);
		float b = Utils::getRand(0.0f, 1.0f);
		glColor3f(r, g, b);

		for (int stack = 0; stack <= stacks; stack++) // Need the bottom row.
		{
			float theta = ((float) slice / (float) slices) * 2.0f * (float) M_PI;
			float phi = ((float) stack / (float) stacks) * 0.5f * (float) M_PI;
			float x = cosf(theta) * sinf(phi);
			float z = sinf(theta) * sinf(phi);
			float y = cosf(phi);
			glVertex3f(x, y, z);
			//glVertex3f(-x, -y, -z); // For the bottom half.

			// Now calculate theta/phi from these values, make sure they're the same.
			// Then calculate the index based on that number.
			// Special case vector for (0, 1, 0) always gives t,p = 0,0 as others are irrelevant.
			// Note: If y is negative, negate the direction vector, then lookup.

			float p = acosf(y);
			float t = atan2f(z, x);
			if (t < 0)
				t += 2.0f * (float) M_PI;

			int st = (int) (round((p / (0.5 * M_PI)) * (float) stacks));
			int sl = (int) (round((t / (2.0 * M_PI)) * (float) slices));
			if (x != 0 && z != 0)
			{
				assert(sl == slice && st == stack);
				int index = sl * (stacks + 1) + st;
				assert(index == currIndex);
			}
			currIndex++;
		}
	}

#if 0
	// Now try looking up a particular index given a direction.
	// First convert the cartesian coordinate to a spherical one.
	float testTheta = Utils::getRand(0.0f, 2.0f * (float) M_PI);
	float testPhi = Utils::getRand(0.0f, 0.5f * (float) M_PI);

	float xp = cosf(testTheta) * sinf(testPhi);
	float zp = sinf(testTheta) * sinf(testPhi);
	float yp = cosf(testPhi);

	// Now compute the reverse.
	float phi = acosf(yp);
	float theta = atan2(zp, xp);
	if (theta < 0)
		theta += 2.0f * M_PI;

	std::cout << testTheta << ": " << theta << ", " << testPhi << ": " << phi << "\n";
#endif
#endif

	glEnd();

	// Draw geometry as well, to make sure it's correct?
	glColor3f(1, 1, 1);
	glEnable(GL_TEXTURE_2D);
	//glEnable(GL_LIGHTING);
	app->getGpuMesh().render();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
}

void Indirect::drawSpheres(App *app, Math::mat4 mvMatrix)
{
	(void) (app);

	// Simplest way without introducing new shader stuff is to give a new modelview matrix and draw each sphere infividually.
	// TODO: That seems to cause problems with stuff that expects the same mvMatrix for all rendered geometry.
	// TODO: What's the best way of fixing this?
	// TODO: It's probably best to just pass in a sphereOffset, and render based on that.
	auto *shader = Rendering::Shader::getBound();
	if (!shader)
	{
		std::cout << "no shader bound when drawing spheres\n";
		return;
	}

	for (size_t i = 0; i < spherePositions.size(); i++)
	{
		auto t = Math::getTranslate(spherePositions[i]);
		auto mv = t * mvMatrix;

		shader->setUniform("mvMatrix", mv);
		shader->setUniform("sphereOffset", spherePositions[i]);
		sphereMesh.render();
	}

	shader->setUniform("mvMatrix", mvMatrix);
}

void Indirect::drawSH(App *app)
{
	// Render the spherical harmonics grid to a 2D (or 3D) texture.
	static Mesh sphere = Mesh::getSphere();
	static GPUMesh mesh;
	if (!mesh.isGenerated())
		mesh.create(&sphere);

	static GBuffer tmpBuffer;

	app->getProfiler().start("Create SH");

	auto &camera = Renderer::instance().getActiveCamera();
	tmpBuffer.resize(camera.getWidth(), camera.getHeight());

	// First capture stuff to the g-buffer.
	auto &shader = gBuffer.getCaptureShader();
	tmpBuffer.beginCapture();
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	tmpBuffer.setUniforms(&shader);
	mesh.render();
	//app->getGpuMesh().render();
	shader.unbind();
	tmpBuffer.endCapture();

	// Ideally you'd first save the spherical harmonics grid to a storage buffer, but no need to do that
	mat4 fullInv = camera.getInverseProj();

	app->setTmpViewport(width, height);

	// TODO: Make sure you clear the sh texture first!

	testSHShader.bind();

	tmpBuffer.bindTextures();
	tmpBuffer.setUniforms(&testSHShader);

#if INTERPOLATE_SH
	for (int i = 0; i < 8; i++)
		shGrid[i].bind();

	// TODO: Figure out why only 4 of these work (it's probably because of the g-buffer textures).
	testSHShader.setUniform("shTex0", &shGrid[0]);
	testSHShader.setUniform("shTex1", &shGrid[1]);
	testSHShader.setUniform("shTex2", &shGrid[2]);
	testSHShader.setUniform("shTex3", &shGrid[3]);
	testSHShader.setUniform("shTex4", &shGrid[4]);
	testSHShader.setUniform("shTex5", &shGrid[5]);
	testSHShader.setUniform("shTex6", &shGrid[6]);
	testSHShader.setUniform("shTex7", &shGrid[7]);
#endif

	testSHShader.setUniform("mvMatrix", camera.getInverse());
	testSHShader.setUniform("pMatrix", camera.getProjection());
	testSHShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	testSHShader.setUniform("viewport", camera.getViewport());
	testSHShader.setUniform("invPMatrix", camera.getInverseProj());
	testSHShader.setUniform("invMvMatrix", camera.getTransform());
	testSHShader.setUniform("fullInv", fullInv);
#if !INTERPOLATE_SH
	testSHShader.setUniform("SHBuffer", &shBuffer);
#endif

	Renderer::instance().drawQuad();

	tmpBuffer.unbindTextures();
#if INTERPOLATE_SH
	for (int i = 0; i < 8; i++)
		shGrid[i].unbind();
#endif
	testSHShader.unbind();

	app->restoreViewport();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	app->getProfiler().time("Create SH");

	app->getProfiler().start("Draw SH");

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);

	// Now render at full res. What do we see?
	drawSHShader.bind();
	tmpBuffer.bindTextures();
#if INTERPOLATE_SH
	for (int i = 0; i < 8; i++)
		shGrid[i].bind();
#endif
	tmpBuffer.setUniforms(&drawSHShader);

	drawSHShader.setUniform("mvMatrix", camera.getInverse());
	drawSHShader.setUniform("pMatrix", camera.getProjection());
	drawSHShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	drawSHShader.setUniform("viewport", camera.getViewport());
	drawSHShader.setUniform("invPMatrix", camera.getInverseProj());
	drawSHShader.setUniform("invMvMatrix", camera.getTransform());
	drawSHShader.setUniform("size", Math::ivec2(width, height));
#if !INTERPOLATE_SH
	drawSHShader.setUniform("SHBuffer", &shBuffer);
#else
	drawSHShader.setUniform("shTex0", &shGrid[0]);
	drawSHShader.setUniform("shTex1", &shGrid[1]);
	drawSHShader.setUniform("shTex2", &shGrid[2]);
	drawSHShader.setUniform("shTex3", &shGrid[3]);
	drawSHShader.setUniform("shTex4", &shGrid[4]);
	drawSHShader.setUniform("shTex5", &shGrid[5]);
	drawSHShader.setUniform("shTex6", &shGrid[6]);
	drawSHShader.setUniform("shTex7", &shGrid[7]);
	//drawSHShader.setUniform("shTex8", &shGrid[8]);
#endif

	Renderer::instance().drawQuad();

	tmpBuffer.unbindTextures();
#if INTERPOLATE_SH
	for (int i = 0; i < 8; i++)
		shGrid[i].unbind();
#endif
	drawSHShader.unbind();

	glPopAttrib();

	app->getProfiler().time("Draw SH");
}

void Indirect::resizeSHTex(App *app)
{
	(void) (app);
#if INTERPOLATE_2D
	for (int i = 0; i < 8; i++)
		shTexes[i].release();
	shDepths.release();
	shFbo.release();

	shFbo.setSize(width, height);
	shFbo.create(&shDepths, true);

	for (int i = 0; i < 8; i++)
	{
		// Not sure if this is the best filtering to use.
		shTexes[i].create(nullptr, GL_RGBA32F, width, height, false);
		shTexes[i].setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	}

	shFbo.bind();
	for (int i = 0; i < 8; i++)
		shFbo.attachColorBuffer(&shTexes[i]);

	shFbo.setDrawBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shFbo.unbind();
#else
	// What kind of filtering do we use?
	for (int i = 0; i < 9; i++)
		shGrid[i].create(GL_RGBA32F, width, height, CLUSTERS);
#endif
}

void Indirect::initCubeMapBuffers(App *app)
{
	// TODO: Just stick with one texture array per direction.
	(void) (app);

	// Only initialise them once.
	if (!cubemapDepthBuffers.empty() || nLights <= 0)
		return;

	// 2d texture arrays to store temporary depths and frambuffer objects to render to them.
	for (auto *b : cubemapDepthBuffers)
		delete b;
	for (auto *t : cubemapDepthTextures)
		delete t;

	// TODO: We only care specifically about the shadow casting lights. Figure out which ones cast shadows and create the maps for those lights only.
	// TODO: Access the coneFlag value, stored in the last component of light color and accumulate.
	// TODO: Will probably need to store a list of indices of those specific lights, or maybe just the number of them since they should be stored first.

	// Hopefully nLights has already been calculated.
	cubemapDepthBuffers.resize(nLights / 2048 + 1);
	cubemapDepthTextures.resize(nLights / 2048 + 1);

	for (size_t i = 0; i < cubemapDepthBuffers.size(); i++)
	{
		cubemapDepthTextures[i] = new Texture2DArray();
		cubemapDepthBuffers[i] = new FrameBuffer();

		cubemapDepthBuffers[i]->setSize(DEEP_SHADOW_RES, DEEP_SHADOW_RES, nLights > 2048 ? 2048 : nLights);

		// Framebuffer needs to be created with a layered depth texture.
		cubemapDepthTextures[i]->create(GL_DEPTH_COMPONENT16, DEEP_SHADOW_RES, DEEP_SHADOW_RES, nLights > 2048 ? 2048 : nLights);
		cubemapDepthTextures[i]->setFilters(GL_CLAMP_TO_EDGE);

		cubemapDepthBuffers[i]->generate();
		cubemapDepthBuffers[i]->bind();
		cubemapDepthBuffers[i]->attachDepthBuffer(cubemapDepthTextures[i]);
		cubemapDepthBuffers[i]->unbind();

		cubemapDepthTextures[i]->bind();
		cubemapDepthTextures[i]->useAsShadowMap();
		cubemapDepthTextures[i]->unbind();

		//cubemapDepthBuffers[i]->create(GL_R32F, cubemapDepthTextures[i]);
	}

	cubemapUniforms.resize(nLights / 2048 + 1);
	for (size_t i = 0; i < cubemapUniforms.size(); i++)
		cubemapUniforms[i] = (int) i;
}

void Indirect::buildCubeMaps(App *app)
{
	app->getProfiler().start("Shadow maps");

	// Just do this once. We won't bother testing with animating lights/geometry.
	if (cubemapDepthTextures.empty())
	{
		Utils::Timer t;
		float f = 0;

		// nLights should already be set. Initialise the buffers.
		initCubeMapBuffers(app);

		vplMatrices.resize(nLights);

		// Read position and direction information from the appropriate storage buffers.
		vec4 *vplPositions = (vec4*) globalLightPos.read();
		vec4 *vplDirections = (vec4*) globalLightDir.read();
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		// For now, just render one texture per VPL rather than a full cubemap.

		// TODO: Do this over a number of frames to avoid crashing your computer...
		app->setTmpViewport(DEEP_SHADOW_RES, DEEP_SHADOW_RES);
		renderCubemapShader.bind();
	#if 0
		vplPos.resize(nLights);
		vplDir.resize(nLights);
	#endif
		for (int i = 0; i < (int) cubemapDepthBuffers.size(); i++)
		{
			// Bind the appropriate render target.
			cubemapDepthBuffers[i]->bind();
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			// TODO: Use instanced rendering instead.
			// If we have more than 2048 lights they need to be blocked up.
			int remainingLights = nLights % 2048;
			if (i * 2048 > nLights)
				remainingLights = 2048;

			for (int j = 0; j < remainingLights; j++)
			{
				int vplID = i * 2048 + j;

				// Need position and direction of each vpl for this to work...
				vec3 pos = vplPositions[vplID].xyz;
				vec3 dir = vplDirections[vplID].xyz;
			#if 0
				vplPos[vplID] = pos;
				vplDir[vplID] = dir;
			#endif
				// Am I supposed to set the view distance here? Not sure...
				// TODO: May want this to be offset slightly... maybe.
				Camera vplCam;
				//vplCam.setType(Camera::Type::PERSP);
				vplCam.setType(Camera::Type::ORTHO);
				vplCam.setPos(pos);
				vplCam.setViewDir(-dir);
				vplCam.setViewport(0, 0, DEEP_SHADOW_RES, DEEP_SHADOW_RES);
				vplCam.setOrtho(-5, 5, -5, 5);
				vplCam.setDistance(0, 20);
				vplCam.update(0.01f);

				vplMatrices[vplID] = vplCam.getInverse() * vplCam.getProjection();
				vplMatrices[vplID] = vplMatrices[vplID] * getBias<float>();

				// Texture layer we're rendering to.
				int texLayer = j;

				//renderCubemapShader.setUniform("vplIndex", vplIndex);
				renderCubemapShader.setUniform("texLayer", texLayer);

				// Set modelview/projection matrices for this VPL (If it's a cubemap then this would be somewhat different).
				renderCubemapShader.setUniform("mvMatrix", vplCam.getInverse());
				renderCubemapShader.setUniform("pMatrix", vplCam.getProjection());

				// Render the scene as normal (without any material information).
				app->getGpuMesh().render(false);

				f += t.time();
				if (f > 5)
				{
					std::cout << "VPL shadow maps built: " << ((float) vplID / (float) nLights) * 100.0f << "%\n";
					f = 0;
				}
			}

			cubemapDepthBuffers[i]->unbind();
		}
		renderCubemapShader.unbind();
		app->restoreViewport();
		std::cout << "nLights: " << nLights << "\n";
	}

	vplMatBuffer.create(&vplMatrices[0], nLights * sizeof(mat4));

#if 0
	static int vplID = 0;
	static float f = 0;
	f += Platform::Window::getDeltaTime();
	if (f >= 1)
	{
		vplID++;
		if (vplID >= nLights)
			vplID = 0;
		f = 0;
		std::cout << "switching vpl\n";
	}

	#if 0
		auto pos = vplPos[vplID];
		auto dir = vplDir[vplID];
		Camera cam;
		cam.setType(Camera::Type::ORTHO);
		cam.setPos(pos);
		cam.setViewDir(-dir);
		cam.setViewport(0, 0, DEEP_SHADOW_RES, DEEP_SHADOW_RES);
		cam.setOrtho(-5, 5, -5, 5);
		cam.setDistance(0, 30);
		cam.update(0.01f);
		app->drawMesh(cam);
	#else
		// Try rendering some of the texture layers and see what's in them (are they correct?).
		for (size_t i = 0; i < cubemapDepthTextures.size(); i++)
		{
			cubemapDepthTextures[i]->bind();
			cubemapUniforms[i] = cubemapDepthTextures[i]->getActiveNumber();
		}

		testCubemapShader.bind();

		testCubemapShader.setUniform("vplID", vplID);

		auto loc = testCubemapShader.getUniform("cubemapDepths");
		glUniform1iv(loc, cubemapUniforms.size(), &cubemapUniforms[0]);

		Renderer::instance().drawQuad();

		testCubemapShader.unbind();

		for (size_t i = 0; i < cubemapDepthTextures.size(); i++)
			cubemapDepthTextures[i]->unbind();
	#endif
#endif
	app->getProfiler().time("Shadow maps");
}

void Indirect::buildDeepDirMask(App *app)
{
	app->getProfiler().start("Dir mask");

	glEnable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	int nDirs = (int) ceil((float) (deepCams.size()) / 32.0f);

	float res = (float) DEEP_RES;
	int intPerImg = (int) ceil((res * res) / 32.0f);

	// Zero the mask.
	zeroDirMaskShader.bind();
	zeroDirMaskShader.setUniform("DirMask", &dirMaskBuffer);
	glDrawArrays(GL_POINTS, 0, nDirs);
	zeroDirMaskShader.setUniform("DirMask", &pixMaskBuffer);
	glDrawArrays(GL_POINTS, 0, (GLsizei) (intPerImg * deepCams.size()));
	zeroDirMaskShader.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	vec3 v[6];
	v[0] = vec3(0, 1, 0);
	v[1] = vec3(0.707107, 0.707107, 0);
	v[2] = vec3(0.218509, 0.707107, 0.672498);
	v[3] = vec3(-0.57206, 0.707107, 0.415628);
	v[4] = vec3(-0.572063, 0.707107, -0.415625);
	v[5] = vec3(0.218505, 0.707107, -0.672499);

	// Read data from the global dir buffer and atomicOr as necessary.
	buildDirMaskShader.bind();

	buildDirMaskShader.setUniform("DirMask", &dirMaskBuffer);
	buildDirMaskShader.setUniform("PixMask", &pixMaskBuffer);
	buildDirMaskShader.setUniform("LightDirs", &globalLightDir);
	buildDirMaskShader.setUniform("LightPositions", &globalLightPos);
	buildDirMaskShader.setUniform("DeepCams", &deepCamBuffer);
	buildDirMaskShader.setUniform("stacks", stacks);
	buildDirMaskShader.setUniform("slices", slices);

	buildDirMaskShader.setUniform("sampleDir0", v[0]);
	buildDirMaskShader.setUniform("sampleDir1", v[1]);
	buildDirMaskShader.setUniform("sampleDir2", v[2]);
	buildDirMaskShader.setUniform("sampleDir3", v[3]);
	buildDirMaskShader.setUniform("sampleDir4", v[4]);
	buildDirMaskShader.setUniform("sampleDir5", v[5]);

	glDrawArrays(GL_POINTS, 0, nLights);

	buildDirMaskShader.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glDisable(GL_RASTERIZER_DISCARD);

	app->getProfiler().time("Dir mask");
#if 1
	// How many bits are 'on' vs 'off'?
	unsigned int *uDir = (unsigned int*) dirMaskBuffer.read();
	unsigned int *uPix = (unsigned int*) pixMaskBuffer.read();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	int nDirBits = 0;
	for (int i = 0; i < nDirs; i++)
	{
		std::bitset<32> b(uDir[i]);
		nDirBits += (int) b.count();
		//std::cout << b << " ";
	}
	int nPixBits = 0;
	for (int i = 0; i < intPerImg * (int) deepCams.size(); i++)
	{
		std::bitset<32> b(uPix[i]);
		nPixBits += (int) b.count();
	}
	int maxDirs = (int) deepCams.size();
	int maxPixs = maxDirs * DEEP_RES * DEEP_RES;
	std::cout << "Directional bits: " << nDirBits << ", Maximum: " << maxDirs << ", Used: " << ((float) nDirBits / (float) maxDirs) * 100.0f << "\n";
	std::cout << "Positional bits: " << nPixBits << ", Maximum: " << maxPixs << ", Used: " << ((float) nPixBits / (float) maxPixs) * 100.0f << "\n";
#endif
}

void Indirect::buildDeepPixMask(App *app)
{
	(void) (app);

	// TODO: Not all pixels of every deep image are needed.
	// TODO: Figure out a way of determining the good pixels from the bad ones.
}

void Indirect::captureDeepImages(App *app)
{
	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	app->getProfiler().start("LFB create");
	app->setTmpViewport(DEEP_RES, DEEP_RES);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

#if CONSERVATIVE
	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	// One lfb per camera, currently arranged vertically.
#if BASIC_INDEXING
	lfb.resize(DEEP_RES, DEEP_RES * (int) deepCams.size()); // One lfb per camera, may want a square?
#else
	lfb.resize(deepCols * DEEP_RES, deepRows * DEEP_RES); // One lfb per camera, may want a square?
#endif

	if (lfb.getType() != LFB::LINK_LIST)
	{
		// Count the number of frags for capture.

		app->getProfiler().start("LFB count");

		lfb.beginCountFrags();
		countLfbShader.bind();
		lfb.setCountUniforms(&countLfbShader);
		countLfbShader.setUniform("mvMatrix", deepCamZ.getInverse());
		countLfbShader.setUniform("DeepCams", &deepCamBuffer);
	#if BUILD_BRUTE_FORCE
		for (int i = 0; i < N_DEEPS; i++)
		{
			countLfbShader.setUniform("deepIndex", i);
			app->getGpuMesh().render(false);
			glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		}
	#else
		app->getGpuMesh().render(false);
	#endif
	#if DRAW_SPHERES
		// FIXME: Doesn't work properly. The translation is now way too large for an ndc projection.
		// FIXME: At least I think that's what is happening. It may be caused by something else.
		drawSpheres(app, deepCamZ.getInverse());
	#endif
		countLfbShader.unbind();

		app->getProfiler().time("LFB count");

		app->getProfiler().start("LFB prefix");
		lfb.endCountFrags();
		app->getProfiler().time("LFB prefix");
	}

	app->getProfiler().start("LFB capture");

	lfb.beginCapture();

	captureLfbShader.bind();
	lfb.setUniforms(&captureLfbShader);
	captureLfbShader.setUniform("mvMatrix", deepCamZ.getInverse());
	captureLfbShader.setUniform("DeepCams", &deepCamBuffer);
#if BUILD_BRUTE_FORCE
	for (int i = 0; i < N_DEEPS; i++)
	{
		captureLfbShader.setUniform("deepIndex", i);
		app->getGpuMesh().render();
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}
#else
	app->getGpuMesh().render();
#endif
#if DRAW_SPHERES
	drawSpheres(app, deepCamZ.getInverse());
#endif
	captureLfbShader.unbind();
#if 1
	if (lfb.endCapture())
	{
		lfb.beginCapture();

		captureLfbShader.bind();
		lfb.setUniforms(&captureLfbShader);
		captureLfbShader.setUniform("mvMatrix", deepCamZ.getInverse());
		captureLfbShader.setUniform("DeepCams", &deepCamBuffer);
	#if BUILD_BRUTE_FORCE
		for (int i = 0; i < N_DEEPS; i++)
		{
			captureLfbShader.setUniform("deepIndex", i);
			app->getGpuMesh().render();
		}
	#else
		app->getGpuMesh().render();
	#endif
	#if DRAW_SPHERES
		drawSpheres(app, deepCamZ.getInverse());
	#endif
		captureLfbShader.unbind();

		if (lfb.endCapture())
			std::cout << "Something really bad happened.\n";
	}
#else
	lfb.endCapture();
#endif

	app->getProfiler().time("LFB capture");

	app->restoreViewport();
	app->getProfiler().time("LFB create");

#if CONSERVATIVE
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

#if 1
	// Now sort.
	app->getProfiler().start("LFB sort");

	sortFbo.bind();
#if BASIC_INDEXING
	app->setTmpViewport(DEEP_RES, DEEP_RES * (int) deepCams.size());
#else
	app->setTmpViewport(deepCols * DEEP_RES, deepRows * DEEP_RES);
#endif
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	bma.createMask(&lfb);
	bma.sort(&lfb);

	app->restoreViewport();
	sortFbo.unbind();

	app->getProfiler().time("LFB sort");
#endif

	glPopAttrib();
}

void Indirect::renderDeepImages(App *app)
{
	static int deepIndex = 0;
#if 0
	static float t = 0;
	float dt = Platform::Window::getDeltaTime();
	t += dt;
	if (t > 2)
	{
		t = 0;
		deepIndex++;
		if (deepIndex >= (int) deepCams.size())
			deepIndex = 0;
	}
#endif

	app->getProfiler().start("LFB draw");

	// TODO: Render each deep image separately rather than all at once.

	// Draw the contents of the deep images. So we can make sure they're correct.
#if BASIC_INDEXING
	app->setTmpViewport(DEEP_RES, DEEP_RES * (int) deepCams.size());
#else
	app->setTmpViewport(deepCols * DEEP_RES, deepRows * DEEP_RES);
#endif
	drawLfbShader.bind();
	//lfb.composite(&drawLfbShader);
	drawLfbShader.setUniform("deepIndex", deepIndex);
	drawLfbShader.setUniform("pixelCheck", -1);
	lfb.setUniforms(&drawLfbShader);
	Renderer::instance().drawQuad();
	drawLfbShader.unbind();
	app->restoreViewport();

	app->getProfiler().time("LFB draw");
}

void Indirect::renderDeepImage(App *app, int index, int pixel)
{
	(void) (app);

	drawLfbShader.bind();
	drawLfbShader.setUniform("deepIndex", index);
	drawLfbShader.setUniform("pixelCheck", pixel);
	lfb.setUniforms(&drawLfbShader);
	Renderer::instance().drawQuad();
	drawLfbShader.unbind();
}

void Indirect::drawLightVectors(App *app)
{
	(void) (app);

	auto &camera = Renderer::instance().getActiveCamera();

#if 0
	static float totalTime = 0;

	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT | GL_CURRENT_BIT);

	// Periodically pick a new seed.
	totalTime += Platform::Window::getDeltaTime();
	Utils::seedRand((int) (totalTime / 5.0f));

	float sx = Utils::getRand(-10.0f, 10.0f);
	float sz = Utils::getRand(-10.0f, 10.0f);
	float ex = Utils::getRand(-10.0f, 10.0f);
	float ez = Utils::getRand(-10.0f, 10.0f);

	vec3 osStart(sx, -10, sz);
	vec3 osEnd(ex, 10, ez);
	vec3 osDir = (osEnd - osStart).normalize();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glLoadMatrixf(camera.getProjection());

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glLoadMatrixf(camera.getInverse());

	// Determine which deep image index to use.
	// For now, just use one of the deep images and a single light direction (the top-down image).

	// TODO: Make sure the correct deep image and direction camera is used.
	bool reverseFlag = false;
	if (osDir.y < 0)
	{
		osDir = -osDir;
		reverseFlag = true;
		std::cout << "reversed! ";
	}
	auto p = getDeepImgIndex(osDir, reverseFlag);
	int index = p.first;
	int expectedIndex = 0;
	(void) (index);
	(void) (expectedIndex);
	std::cout << "index: " << index << "\n";
	if (index >= (int) deepCams.size())
	{
		std::cout << "FFFUUUUUUUUUUUUUUU!!!!!!!!!!\n";
		exit(0);
	}

	vec3 deepDir = p.second.xyz;
	vec3 deepStart = osStart;
	vec3 deepEnd = deepStart + (deepDir * 30.0);

	app->drawMesh();
	glDisable(GL_DEPTH_TEST);

	// Render the desired direction.
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3fv(&osStart.x);
	glVertex3fv(&osEnd.x);
	glEnd();

	// And the deep image direction.
	glBegin(GL_LINES);
	glColor3f(0, 1, 0);
	glVertex3fv(&deepStart.x);
	glVertex3fv(&deepEnd.x);
	glEnd();

	// Can't just assert that the indices are the same, need to draw the associated deep images.
	//renderDeepImage(app, index);
	//renderDeepImage(app, expectedIndex);
	//return;

	auto &deepCam = deepCams[index];

	vec4 esStart = deepCam.getInverse() * vec4(deepStart, 1);
	//vec4 esEnd = deepCam.getInverse() * vec4(deepEnd, 1);

	// Determine which deep image pixel to use.
	int pixel = getPixel(esStart);
	vec2 fragCoord(pixel % DEEP_RES, pixel / DEEP_RES);

	// TODO: pixel needs to be offset based on which deep image it is from.
	ivec2 imgSize = ivec2(DEEP_RES, DEEP_RES);
	int layerOffset = index * imgSize.x * imgSize.y;
	int lfbPixel = pixel + layerOffset;
	//renderDeepImage(app, index, pixel);

	// What is the count for that deep image pixel? Is it correct? How do I know if it's correct?
	unsigned int *offsets = (unsigned int*) ((LFBLinear*) lfb.getBase())->getOffsets()->read();
	//unsigned int count = offsets[pixel] - (pixel > 0 ? offsets[pixel - 1] : 0);
	//std::cout << "count: " << count << "\n";

	// Step through to determine the intersection point. Is it correct?
	// For now, render points at each of these locations.
	float *depths = (float*) ((LFBLinear*) lfb.getBase())->getData()->read();

	glPointSize(5);
	glBegin(GL_POINTS);
	glColor3f(1, 0, 0);
	for (unsigned int i = lfbPixel > 0 ? offsets[lfbPixel - 1] : 0; i < offsets[lfbPixel]; i++)
	{
		// This is a value between -1 and 1, needs to be converted into object space distances.
		float z = depths[i];
		//std::cout << z << "; ";
		// Cheap hack to convert without multiplying by the inverse modelview matrix.
		//vec3 pos = deepStart + deepDir * ((z / 0.06) + 10.0 * 1.05 + 11);

		// Get the eye space pos based on screen space pixel. This seems to give only the approximate position.
		vec4 esPos = getEyeFromWindow(vec3(fragCoord.x, fragCoord.y, z), ivec4(0, 0, DEEP_RES, DEEP_RES), mat4::getIdentity());
		//std::cout << esPos << "; ";

		// Convert back to object space.
		vec4 pos = deepCam.getTransform() * esPos;
		//std::cout << pos << "; ";
		glVertex3fv(&pos.x);
	}
	glEnd();
	//std::cout << "\n";

	// TODO: Now try for other direction vectors.
	// TODO: If everything is correct, put it into glsl.
	// TODO: If the glsl version works, we can try some raycasting.

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
#else
	
	vec3 v[6];
	v[0] = vec3(0, 1, 0);
	v[1] = vec3(0.707107, 0.707107, 0);
	v[2] = vec3(0.218509, 0.707107, 0.672498);
	v[3] = vec3(-0.57206, 0.707107, 0.415628);
	v[4] = vec3(-0.572063, 0.707107, -0.415625);
	v[5] = vec3(0.218505, 0.707107, -0.672499);

	debugDirShader.bind();

	debugDirShader.setUniform("GlobalLightPos", &globalLightPos);
	debugDirShader.setUniform("GlobalLightDir", &globalLightDir);
	debugDirShader.setUniform("mvMatrix", camera.getInverse());
	debugDirShader.setUniform("pMatrix", camera.getProjection());

	debugDirShader.setUniform("sampleDir0", v[0]);
	debugDirShader.setUniform("sampleDir1", v[1]);
	debugDirShader.setUniform("sampleDir2", v[2]);
	debugDirShader.setUniform("sampleDir3", v[3]);
	debugDirShader.setUniform("sampleDir4", v[4]);
	debugDirShader.setUniform("sampleDir5", v[5]);

	debugDirShader.setUniform("stacks", stacks);
	debugDirShader.setUniform("slices", slices);
	debugDirShader.setUniform("DeepCams", &deepCamBuffer);
	lfb.setUniforms(&debugDirShader);

	glDrawArrays(GL_POINTS, 0, nLights);

	debugDirShader.unbind();

	app->drawMesh();
#endif
}

void Indirect::drawConeVectors(App *app)
{
	(void) (app);

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT | GL_TRANSFORM_BIT);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glLoadMatrixf(camera.getInverse());
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glLoadMatrixf(camera.getProjection());

	Utils::seedRand(56);
	vec3 normal(Utils::getRand(-1.0f, 1.0f), Utils::getRand(-1.0f, 1.0f), Utils::getRand(-1.0f, 1.0f));
	//vec3 normal(0, 1, 0);
	normal.normalize();

	vec3 up = normal.y * normal.y > 0.95f ? vec3(0, 0, 1) : vec3(0, 1, 0);
	vec3 right = normal.cross(up);
	up = normal.cross(right);

	right.normalize();
	up.normalize();

	glBegin(GL_LINES);

	glColor3f(0, 1, 1);

	glVertex3f(0, 0, 0);
	glVertex3fv(&normal.x);

	//glVertex3f(0, 0, 0);
	//glVertex3fv(&right.x);

	//glVertex3f(0, 0, 0);
	//glVertex3fv(&up.x);

	glColor3f(1, 0, 1);

#if 0
	vec3 v[7];
	v[0] = vec3(0, 1, 0);
	v[1] = vec3(0.707107f, 0.707107f, 0.0f);
	v[2] = vec3(0.353553f, 0.707107f, 0.612373f);
	v[3] = vec3(-0.353552f, 0.707107f, 0.612373f);
	v[4] = vec3(-0.707107f, 0.707107f, 0.0f);
	v[5] = vec3(-0.353553f, 0.707107f, -0.612373f);
	v[6] = vec3(0.353553f, 0.707107f, -0.612373f);
#else
	vec3 v[6];
	v[0] = normal;
	v[1] = vec3(0.707107, 0.707107, 0);
	v[2] = vec3(0.218509, 0.707107, 0.672498);
	v[3] = vec3(-0.57206, 0.707107, 0.415628);
	v[4] = vec3(-0.572063, 0.707107, -0.415625);
	v[5] = vec3(0.218505, 0.707107, -0.672499);
#endif

#if 0
	for (int i = 0; i < 5; i++)
	{
		float phi = acosf(normal.y);
		float theta = atan2f(normal.z, normal.x);
		if (theta < 0)
			theta += 2.0 * 3.14159;
		phi += 0.785398;
		float dirFloat = (float) i;
		theta += (dirFloat / 5.0) * 2.0 * 3.14159;
		vec3 dir = vec3(cosf(theta) * sinf(phi), cosf(phi), sinf(theta) * sinf(phi));
		std::cout << dir << "; ";

		glVertex3f(0, 0, 0);
		glVertex3fv(&dir.x);
	}
	std::cout << "\n";
#else
	for (int i = 1; i < 6; i++)
	{
		//coneSampleDirection = surface.Normal;
		//coneSampleDirection += cVXGIConeSampleDirections[conei].x * right + cVXGIConeSampleDirections[conei].z * up;
		//coneSampleDirection = normalize(coneSampleDirection);

		// FIXME: Seems to give poor results for some normal vectors.
		vec3 coneDir = normal;
		coneDir += v[i] * right + v[i].z * up;
		coneDir.normalize();

		glVertex3f(0, 0, 0);
		glVertex3fv(&coneDir.x);
	}
#endif

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
}

void Indirect::countClusterPos(App *app)
{
	glEnable(GL_RASTERIZER_DISCARD);

	//auto &camera = Renderer::instance().getActiveCamera();
	app->getProfiler().start("Pos count");

	clusterPos.beginCountFrags();

	countPosShader.bind();

	// Lights stored based on object space xyz, camera data not needed.
	//countPosShader.setUniform("mvMatrix", camera.getInverse());
	//countPosShader.setUniform("pMatrix", camera.getProjection());
	//countPosShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	//countPosShader.setUniform("viewport", camera.getViewport());
	//countPosShader.setUniform("invPMatrix", camera.getInverseProj());

	countPosShader.setUniform("GlobalLightPos", &globalLightPos);
	countPosShader.setUniform("GlobalLightDir", &globalLightDir);

	//countPosShader.setUniform("size", Math::ivec2(width, height));

	//mask.setUniforms(&countPosShader);

	clusterPos.setCountUniforms(&countPosShader);

	glDrawArrays(GL_POINTS, 0, nLights);

	countPosShader.unbind();

	app->getProfiler().time("Pos count");

	glDisable(GL_RASTERIZER_DISCARD);

	app->getProfiler().start("Pos prefix");
	clusterPos.endCountFrags();
	app->getProfiler().time("Pos prefix");
}

void Indirect::createClusterPos(App *app)
{
	glEnable(GL_RASTERIZER_DISCARD);

	//auto &camera = Renderer::instance().getActiveCamera();
	app->getProfiler().start("Pos create");

	createPosShader.bind();

	// Lights stored based on object space xyz, camera data not needed.
	//createPosShader.setUniform("mvMatrix", camera.getInverse());
	//createPosShader.setUniform("pMatrix", camera.getProjection());
	//createPosShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	//createPosShader.setUniform("viewport", camera.getViewport());
	//createPosShader.setUniform("invPMatrix", camera.getInverseProj());

	createPosShader.setUniform("GlobalLightPos", &globalLightPos);
	createPosShader.setUniform("GlobalLightDir", &globalLightDir);

	//createPosShader.setUniform("size", Math::ivec2(width, height));

	//mask.setUniforms(&createClusterShader);

	clusterPos.setUniforms(&createPosShader);

	glDrawArrays(GL_POINTS, 0, nLights);

	createPosShader.unbind();

	app->getProfiler().time("Pos create");

	glDisable(GL_RASTERIZER_DISCARD);
}

void Indirect::createGlobalLightData(App *app)
{
	// TODO: How do I do the specular lights?
	// TODO: What happens when I choose positions based on visible screen-space geometry?

	//auto &camera = Renderer::instance().getActiveCamera();
	app->getProfiler().start("Global lights");
	
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	// Set the atomic counter to zero.
	unsigned int zero = 0;
	if (!lightCounter.isGenerated())
		lightCounter.create(&zero, sizeof(zero));
	else
		lightCounter.bufferData(&zero, sizeof(zero));

	// Resize the global buffers to some arbitrary size.
	// Fourth value of pos stores colour, and 0/1 for sphere/cone.
	// Fourth value of dir stores range.
	int maxLights = (int) ((LIGHT_RES * LIGHT_RES * 6) * 0.2);
	if (!globalLightPos.isGenerated())
		globalLightPos.create(nullptr, maxLights * sizeof(float) * 4);
	if (!globalLightDir.isGenerated())
		globalLightDir.create(nullptr, maxLights * sizeof(float) * 4);

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	// TODO: Include a way to resize the buffers if they are too small.
	// TODO: May be faster to count first, then resize.

	glEnable(GL_RASTERIZER_DISCARD);

	globalLightShader.bind();

	// Counter.
	globalLightShader.setUniform("lightCount", &lightCounter);

	lightDirs.bind();
	lightColors.bind();
	lightPositions.bind();
	lightLODs.bind();
	lightNormals.bind();

	// Reflective shadow map.
	globalLightShader.setUniform("dirTex", &lightDirs);
	globalLightShader.setUniform("colTex", &lightColors);
	globalLightShader.setUniform("posTex", &lightPositions);
	globalLightShader.setUniform("lodTex", &lightLODs);
	globalLightShader.setUniform("normTex", &lightNormals);

	// Global light data.
	globalLightShader.setUniform("GlobalLightPos", &globalLightPos);
	globalLightShader.setUniform("GlobalLightDir", &globalLightDir);
	globalLightShader.setUniform("lightAlloc", maxLights);

	// Deep image stuff.
	globalLightShader.setUniform("stacks", stacks);
	globalLightShader.setUniform("slices", slices);
	globalLightShader.setUniform("DeepCams", &deepCamBuffer);
	lfb.setUniforms(&globalLightShader);

	// Compute.

	globalLightShader.setUniform("coneFlag", false);
#if 1
	// Direct lighting component.
	// TODO: Picking samples based on viewpoint may be a better approach.
	// TODO: Come up with a formula for picking appropriate sizes based on frustum max z and distance.
	// 0: 256, 1: 128, 2: 64, 3: 32, 4: 16.
	float minDist = 8.0f;
	float maxDist = MAX_EYE_Z;
	float lightDist = 0.2f;
	for (int lod = 0; lod < 4; lod++)
	{
		int div = (int) pow(2, lod);
		globalLightShader.setUniform("minDist", minDist);
		globalLightShader.setUniform("maxDist", maxDist);
		globalLightShader.setUniform("dist", lightDist);
		globalLightShader.setUniform("div", div);
		globalLightShader.setUniform("lod", lod);

		glDrawArrays(GL_POINTS, 0, (LIGHT_RES / div) * (LIGHT_RES / div));

		maxDist = minDist;
		minDist /= 2.0;
		//lightDist *= 1.2;
	}

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
#endif

#if 1
	globalLightShader.setUniform("coneFlag", true);

	// Indirect lighting component.
	// TODO: Are the different cone directions really working as intended?
	vec3 v[6];
	v[0] = vec3(0, 1, 0);
	v[1] = vec3(0.707107, 0.707107, 0);
	v[2] = vec3(0.218509, 0.707107, 0.672498);
	v[3] = vec3(-0.57206, 0.707107, 0.415628);
	v[4] = vec3(-0.572063, 0.707107, -0.415625);
	v[5] = vec3(0.218505, 0.707107, -0.672499);

	minDist = 8.0f;
	maxDist = MAX_EYE_Z;
	lightDist = 0.2f;
	// TODO: Can we get away with fewer passes?
	for (int lod = 1; lod < 5; lod++)
	{
		int div = (int) pow(2, lod);
		globalLightShader.setUniform("minDist", minDist);
		globalLightShader.setUniform("maxDist", maxDist);
		globalLightShader.setUniform("dist", lightDist);
		globalLightShader.setUniform("div", div);
		globalLightShader.setUniform("lod", lod);
		globalLightShader.setUniform("sampleDir0", v[0]);
		globalLightShader.setUniform("sampleDir1", v[1]);
		globalLightShader.setUniform("sampleDir2", v[2]);
		globalLightShader.setUniform("sampleDir3", v[3]);
		globalLightShader.setUniform("sampleDir4", v[4]);
		globalLightShader.setUniform("sampleDir5", v[5]);

		glDrawArrays(GL_POINTS, 0, (LIGHT_RES / div) * (LIGHT_RES / div));

		maxDist = minDist;
		minDist /= 2.0;
		//lightDist *= 1.2;
	}
#endif
	lightDirs.unbind();
	lightColors.unbind();
	lightPositions.unbind();
	lightLODs.unbind();
	lightNormals.unbind();

	globalLightShader.unbind();

	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	app->getProfiler().time("Global lights");

	nLights = *((int*) lightCounter.read());

	//std::cout << "nLights: " << nLights << "\n";

	// TODO: If nLights is >= no. allocated, realloc and recompute.
#if 0
	// For testing:
	// How many lights did we end up with?
	std::cout << nLights << ", " << maxLights << "\n";

	// What data has actually been stored in the buffers?
	#if 0
		vec4 *posData = (vec4*) globalLightPos.read();
		vec4 *dirData = (vec4*) globalLightDir.read();
		for (int i = 0; i < 8000; i++)
		{
			std::cout << posData[i] << "; " << dirData[i] << "\n";
		}
	#endif
#endif
}

void Indirect::countClusters(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	app->getProfiler().start("Cluster count");

	cluster.beginCountFrags();

	countClusterShader.bind();

	countClusterShader.setUniform("mvMatrix", camera.getInverse());
	countClusterShader.setUniform("pMatrix", camera.getProjection());
	countClusterShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	countClusterShader.setUniform("viewport", camera.getViewport());
	countClusterShader.setUniform("invPMatrix", camera.getInverseProj());

	countClusterShader.setUniform("GlobalLightPos", &globalLightPos);
	countClusterShader.setUniform("GlobalLightDir", &globalLightDir);

	countClusterShader.setUniform("size", Math::ivec2(width, height));

	mask.setUniforms(&countClusterShader);

	cluster.setCountUniforms(&countClusterShader);

	glDrawArrays(GL_POINTS, 0, nLights);

	countClusterShader.unbind();

	app->getProfiler().time("Cluster count");

	app->getProfiler().start("Cluster prefix");
	cluster.endCountFrags();
	app->getProfiler().time("Cluster prefix");
}

void Indirect::createClusters(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	app->getProfiler().start("Cluster create");

	createClusterShader.bind();

	createClusterShader.setUniform("mvMatrix", camera.getInverse());
	createClusterShader.setUniform("pMatrix", camera.getProjection());
	createClusterShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	createClusterShader.setUniform("viewport", camera.getViewport());
	createClusterShader.setUniform("invPMatrix", camera.getInverseProj());

	createClusterShader.setUniform("GlobalLightPos", &globalLightPos);
	createClusterShader.setUniform("GlobalLightDir", &globalLightDir);

	createClusterShader.setUniform("size", Math::ivec2(width, height));

	mask.setUniforms(&createClusterShader);

	cluster.setUniforms(&createClusterShader);

	glDrawArrays(GL_POINTS, 0, nLights);

	createClusterShader.unbind();

	app->getProfiler().time("Cluster create");
}

void Indirect::captureLightData(App *app)
{
	auto &camera = lightCam;

	app->getProfiler().start("Capture light");

	// Needs to be rendered at lower res.
	app->setTmpViewport(LIGHT_RES, LIGHT_RES);

	// Render colours/depths/light dirs to a texture from light pov.
	// Textures: reflected vectors, geometry colours, geometry positions (can be calculated from just depths I guess).
	lightFbo.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	lightDataShader.bind();
	lightDataShader.setUniform("lightPos", mainLightPos);
	lightDataShader.setUniform("mvMatrix", camera.getInverse());
	lightDataShader.setUniform("pMatrix", camera.getProjection());
	lightDataShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	lightDataShader.setUniform("sphereOffset", vec3(0, 0, 0));
	app->getGpuMesh().render();
#if DRAW_SPHERES
	drawSpheres(app, camera.getInverse());
#endif
	lightDataShader.unbind();
	lightFbo.unbind();

	lightDirs.genMipmap();
	lightColors.genMipmap();
	lightPositions.genMipmap();
	lightLODs.genMipmap();
	lightNormals.genMipmap();

	app->restoreViewport();

	app->getProfiler().time("Capture light");
}

void Indirect::captureLightCamData(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	app->getProfiler().start("Capture camlight");

	// Needs to be rendered at lower res.
	app->setTmpViewport(LIGHT_RES, LIGHT_RES);

	// Render colours/depths/light dirs to a texture from light pov.
	// Textures: reflected vectors, geometry colours, geometry positions (can be calculated from just depths I guess).
	lightCamFbo.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	lightDataShader.bind();
	lightDataShader.setUniform("lightPos", mainLightPos);
	lightDataShader.setUniform("mvMatrix", camera.getInverse());
	lightDataShader.setUniform("pMatrix", camera.getProjection());
	lightDataShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	lightDataShader.setUniform("sphereOffset", vec3(0, 0, 0));
	app->getGpuMesh().render();
#if DRAW_SPHERES
	drawSpheres(app, camera.getInverse());
#endif
	lightDataShader.unbind();
	lightCamFbo.unbind();

	lightCamDirs.genMipmap();
	lightCamColors.genMipmap();
	lightCamPositions.genMipmap();
	lightCamLODs.genMipmap();
	lightCamNormals.genMipmap();

	app->restoreViewport();

	app->getProfiler().time("Capture camlight");
}

void Indirect::captureGeometry(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	auto &shader = gBuffer.getCaptureShader();

	app->getProfiler().start("Capture g-buffer");

	// Capture data into the g-buffer.
	gBuffer.beginCapture();
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	gBuffer.setUniforms(&shader);
	app->getGpuMesh().render();
#if DRAW_SPHERES
	drawSpheres(app, camera.getInverse());
#endif
	shader.unbind();
	gBuffer.endCapture();

	app->getProfiler().time("Capture g-buffer");
}

void Indirect::buildPosGrid(App *app)
{
	glPushAttrib(GL_ENABLE_BIT);

	// Now store the ids in their clusters.
	app->getProfiler().start("Pos grid build");

	if (clusterPos.getType() != Cluster::LINK_LIST)
		countClusterPos(app);

	// This can only repeat if using link lists.
	clusterPos.beginCapture();
	createClusterPos(app);
	if (clusterPos.endCapture())
	{
		clusterPos.beginCapture();
		createClusterPos(app);
		if (clusterPos.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	app->getProfiler().time("Pos grid build");

	glPopAttrib();
}

void Indirect::buildDepthMask(App *app)
{
	(void) (app);

	// Build 2.5D depth occpuancy mask of camera pov geometry.
	app->getProfiler().start("Cluster Mask");
	mask.createMaskFromDepths(gBuffer.getDepthTexture());
	app->getProfiler().time("Cluster Mask");
}

void Indirect::buildLightGrid(App *app)
{
	// Capture lights as billboarded quads, and raycast for front/back depths, then add to clusters.
	glPushAttrib(GL_ENABLE_BIT);

	// Disable if using billboarded quads, enable if using light outlines.
	glDisable(GL_CULL_FACE);

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	// Now store the ids in their clusters.
	app->getProfiler().start("Light grid build");

	if (cluster.getType() != Cluster::LINK_LIST)
		countClusters(app);

	// This can only repeat if using link lists.
	cluster.beginCapture();
	createClusters(app);
	if (cluster.endCapture())
	{
		cluster.beginCapture();
		createClusters(app);
		if (cluster.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	app->getProfiler().time("Light grid build");

	// Restore original size.
	app->restoreViewport();

	glPopAttrib();
}

void Indirect::zeroSH(App *app)
{
	(void) (app);

	app->getProfiler().start("Zero SH");
#if INTERPOLATE_2D
	// TODO: Do we really need to clear the textures here?
#else
	glEnable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	zeroSHShader.bind();

	for (int i = 0; i < 8; i++)
		shGrid[i].bind();

	zeroSHShader.setUniform("size", ivec3(width, height, CLUSTERS));
	zeroSHShader.setUniform("shTex0", &shGrid[0]);
	zeroSHShader.setUniform("shTex1", &shGrid[1]);
	zeroSHShader.setUniform("shTex2", &shGrid[2]);
	zeroSHShader.setUniform("shTex3", &shGrid[3]);
	zeroSHShader.setUniform("shTex4", &shGrid[4]);
	zeroSHShader.setUniform("shTex5", &shGrid[5]);
	zeroSHShader.setUniform("shTex6", &shGrid[6]);
	zeroSHShader.setUniform("shTex7", &shGrid[7]);
	//zeroSHShader.setUniform("shTex8", &shGrid[8]);

	glDrawArrays(GL_POINTS, 0, width * height);

	for (int i = 0; i < 8; i++)
		shGrid[i].unbind();

	zeroSHShader.unbind();

	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	glDisable(GL_RASTERIZER_DISCARD);
#endif
	app->getProfiler().time("Zero SH");
}

void Indirect::buildSH(App *app)
{
#if INTERPOLATE_SH
	zeroSH(app);
#endif

	//auto &camera = Renderer::instance().getActiveCamera();
	auto cam = Renderer::instance().getActiveCamera();

	app->getProfiler().start("Build SH");

	// TODO: One thread for each 2D tile or 3D cell?
	// TODO: Start with 2D, if that's too slow, try 3D.

	// TODO: Not sure if I should use a compute shader for this or not.

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	//glEnable(GL_RASTERIZER_DISCARD);
	app->setTmpViewport(width, height);

	cluster.beginComposite();

	// TODO: Can I get away with not using the g-buffer?
#if INTERPOLATE_SH && INTERPOLATE_2D
	shFbo.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#endif
	buildSHShader.bind();
	gBuffer.bindTextures(); // So I can access depth values.
	gBuffer.setUniforms(&buildSHShader);

	cluster.setUniforms(&buildSHShader);
#if INTERPOLATE_SH
	#if INTERPOLATE_2D
	// Don't think anything extra needs to be done here.
	#else
	// Bind the sh texture stuff here.
	for (int i = 0; i < 8; i++)
		shGrid[i].bind();
	buildSHShader.setUniform("shTex0", &shGrid[0]);
	buildSHShader.setUniform("shTex1", &shGrid[1]);
	buildSHShader.setUniform("shTex2", &shGrid[2]);
	buildSHShader.setUniform("shTex3", &shGrid[3]);
	buildSHShader.setUniform("shTex4", &shGrid[4]);
	buildSHShader.setUniform("shTex5", &shGrid[5]);
	buildSHShader.setUniform("shTex6", &shGrid[6]);
	buildSHShader.setUniform("shTex7", &shGrid[7]);
	//buildSHShader.setUniform("shTex8", &shGrid[8]);
	#endif
#else
	buildSHShader.setUniform("SHBuffer", &shBuffer);
#endif
	buildSHShader.setUniform("size", vec2(cam.getWidth(), cam.getHeight()));
	buildSHShader.setUniform("cSize", ivec2(width, height));
	buildSHShader.setUniform("viewport", cam.getViewport());
	buildSHShader.setUniform("invPMatrix", cam.getInverseProj());

	// One thread per-tile. Could go with one warp per-tile instead, may be faster.
	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();
#if INTERPOLATE_SH && !INTERPOLATE_2D
	for (int i = 0; i < 8; i++)
		shGrid[i].unbind();
#endif
	buildSHShader.unbind();
#if INTERPOLATE_SH && INTERPOLATE_2D
	shFbo.unbind();
#endif

	cluster.endComposite();

#if INTERPOLATE_SH && INTERPOLATE_2D	
	for (int i = 0; i < 8; i++)
		shTexes[i].genMipmap();
#endif

	//glDisable(GL_RASTERIZER_DISCARD);
	app->restoreViewport();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	app->getProfiler().time("Build SH");
}

void Indirect::composite(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Deferred rendering of the scene, applying lights from the grid.
	app->getProfiler().start("Composite");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	cluster.beginComposite();

	compositeShader.bind();
	gBuffer.bindTextures();

#if DEEP_SHADOWS
	// FIXME: For now, just binding the first 2d texture array.
	cubemapDepthTextures[0]->bind();
	compositeShader.setUniform("lightDepths", cubemapDepthTextures[0]);
#endif

	gBuffer.setUniforms(&compositeShader);

	// TODO: Bind the 2d texture arrays for deep shadows.
#if USE_SPHERICAL_HARMONICS
	#if INTERPOLATE_SH
		#if INTERPOLATE_2D
		for (int i = 0; i < 8; i++)
			shTexes[i].bind();
		compositeShader.setUniform("shTex0", &shTexes[0]);
		compositeShader.setUniform("shTex1", &shTexes[1]);
		compositeShader.setUniform("shTex2", &shTexes[2]);
		compositeShader.setUniform("shTex3", &shTexes[3]);
		compositeShader.setUniform("shTex4", &shTexes[4]);
		compositeShader.setUniform("shTex5", &shTexes[5]);
		compositeShader.setUniform("shTex6", &shTexes[6]);
		compositeShader.setUniform("shTex7", &shTexes[7]);
		//compositeShader.setUniform("shTex8", &shTexes[8]);
		#else
		for (int i = 0; i < 8; i++)
			shGrid[i].bind();
		compositeShader.setUniform("shTex0", &shGrid[0]);
		compositeShader.setUniform("shTex1", &shGrid[1]);
		compositeShader.setUniform("shTex2", &shGrid[2]);
		compositeShader.setUniform("shTex3", &shGrid[3]);
		compositeShader.setUniform("shTex4", &shGrid[4]);
		compositeShader.setUniform("shTex5", &shGrid[5]);
		compositeShader.setUniform("shTex6", &shGrid[6]);
		compositeShader.setUniform("shTex7", &shGrid[7]);
		//compositeShader.setUniform("shTex8", &shGrid[8]);
		#endif
	#else
		compositeShader.setUniform("SHBuffer", &shBuffer);
	#endif
#endif
	cluster.setUniforms(&compositeShader);

#if INDIRECT_SHADOWS
	compositeShader.setUniform("stacks", stacks);
	compositeShader.setUniform("slices", slices);
	compositeShader.setUniform("DeepCams", &deepCamBuffer);
	lfb.setUniforms(&compositeShader);
#endif

#if DEEP_SHADOWS
	compositeShader.setUniform("LightMatrices", &vplMatBuffer);
#endif

	compositeShader.setUniform("mainLightPos", mainLightPos);

	compositeShader.setUniform("mvMatrix", camera.getInverse());
	compositeShader.setUniform("pMatrix", camera.getProjection());
	compositeShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	compositeShader.setUniform("viewport", camera.getViewport());
	compositeShader.setUniform("invPMatrix", camera.getInverseProj());
	compositeShader.setUniform("invMvMatrix", camera.getTransform());

#if !STORE_LIGHT_DATA
	compositeShader.setUniform("GlobalLightPos", &globalLightPos);
	compositeShader.setUniform("GlobalLightDir", &globalLightDir);
#endif

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();
#if USE_SPHERICAL_HARMONICS && INTERPOLATE_SH
	#if INTERPOLATE_2D
	for (int i = 0; i < 8; i++)
		shTexes[i].unbind();
	#else
	for (int i = 0; i < 8; i++)
		shGrid[i].unbind();
	#endif
#endif

#if DEEP_SHADOWS
	cubemapDepthTextures[0]->unbind();
#endif

	compositeShader.unbind();

	cluster.endComposite();

	app->getProfiler().time("Composite");
}

void Indirect::animLight(float dt)
{
	float speed = (float) LIGHT_SPEED;

	mainLightPos += mainLightAnimDir * dt * speed;

	// Which edge did we hit?
	if (mainLightPos.x <= -10)
		mainLightAnimDir.reflect(vec3(1, 0, 0));

	else if (mainLightPos.x >= 10)
		mainLightAnimDir.reflect(vec3(-1, 0, 0));

	else if (mainLightPos.y <= -5)
		mainLightAnimDir.reflect(vec3(0, 1, 0));

	else if (mainLightPos.y >= 5)
		mainLightAnimDir.reflect(vec3(0, -1, 0));

	else if (mainLightPos.z <= -10)
		mainLightAnimDir.reflect(vec3(0, 0, 1));

	else if(mainLightPos.z >= 10)
		mainLightAnimDir.reflect(vec3(0, 0, -1));

	// FIXME: This can still go out of bounds...
	mainLightPos.x = clamp(mainLightPos.x, -10.0f, 10.0f);
	mainLightPos.y = clamp(mainLightPos.y, -5.0f, 5.0f);
	mainLightPos.z = clamp(mainLightPos.z, -10.0f, 10.0f);

	lightCam.setPos(mainLightPos);
	lightCam.setViewDir(-lightCam.getPos().unit());
	lightCam.update(0.01f);
}

void Indirect::animSpheres(float dt)
{
	float speed = SPHERE_SPEED;

	for (size_t i = 0; i < spherePositions.size(); i++)
	{
		spherePositions[i] += sphereDirections[i] * dt * speed;
		collide(spherePositions[i], sphereDirections[i], sphereCollisions[i], dt);
	}
}


void Indirect::init(App *app)
{
	// Good test scene: https://renderman.pixar.com/resources/RenderMan_20/images/figures.26/softshadfig1.gif
	auto &camera = Renderer::instance().getActiveCamera();

	mainLightPos = vec3(1.27416f, 5.84406f, -0.638992f);

#if 1
	// Atrium camera.
	camera.setPos({-0.0967992f, -2.74396f, -0.180925f});
	camera.setEulerRot({0.008727f, 3.10668f, 0.0f});
	camera.setZoom(5.9259f);
	camera.update(0.1f);

	lightCam.setType(Camera::Type::PERSP);
	lightCam.setPos(mainLightPos);
	lightCam.setViewDir(-lightCam.getPos().unit());
	lightCam.setViewport(0, 0, LIGHT_RES, LIGHT_RES);
	lightCam.update(0.01f);
#endif

#if 0
	// Rungholt camera.
	camera.setPos({2.90805,3.25054,3.62739});
	camera.setEulerRot({-0.706858f, 0.733033f, 0.0f});
	camera.setZoom(0.0f);
	camera.update(0.1f);

	// Rungholt light.
	lightCam.setType(Camera::Type::PERSP);
	lightCam.setPos({-0.110378f, 11.5089f, 0.107866f});
	lightCam.setViewDir({-0.0654282f, -0.996917f, 0.0433052f});
	lightCam.setZoom(0.0f);
	lightCam.update(0.1f);
#endif

	// Stuff for animation.
	mainLightAnimDir = vec3(Utils::getRand(-1.0f, 1.0f), 0.0f, Utils::getRand(-1.0f, 1.0f));

	int nSpheres = N_SPHERES;
	spherePositions.resize(nSpheres);
	sphereDirections.resize(nSpheres);
	sphereCollisions.resize(nSpheres);

	for (int i = 0; i < nSpheres; i++)
	{
		spherePositions[i] = vec3(Utils::getRand(-10.0f, 10.0f), Utils::getRand(-5.0f, 5.0f), Utils::getRand(-10.0f, 10.0f));
		sphereDirections[i] = vec3(Utils::getRand(-1.0f, 1.0f), Utils::getRand(-1.0f, 1.0f), Utils::getRand(-1.0f, 1.0f));
		sphereDirections[i].normalize();
		sphereCollisions[i] = 0;
	}

	// Don't forget the sphere mesh.
	auto sphere = Rendering::Mesh::getSphere();
	sphere.scale(vec3(SPHERE_SIZE, SPHERE_SIZE, SPHERE_SIZE));
	sphereMesh.create(&sphere);

	// Bunch of cameras for creating the directional deep images.
	createCameras();

	// How many deep image rows and columns do we need?
	int maxCols = DEEP_MAX_RES / DEEP_RES;
	deepCols = std::min((int) deepCams.size(), maxCols);
	deepRows = (int) deepCams.size() / deepCols;
	if (deepRows * DEEP_RES > DEEP_MAX_RES)
		std::cout << "Warning: CDI exceeds maximum texture size.\n";

	// Render target for the sort image, since it *will* be larger than the window size.
#if BASIC_INDEXING
	sortFbo.setSize(DEEP_RES, DEEP_RES * deepCams.size());
#else
	sortFbo.setSize(deepCols * DEEP_RES, deepRows * DEEP_RES);
#endif
	sortFbo.create(GL_RGBA, &sortTex, &sortDepth, true);

	// Deep image stuff.
	lfb.release();
	lfb.setType(LFB::LINEARIZED, "lfb");
	lfb.setDataType(LFB::FLOAT);
	lfb.setMaxFrags(512);
	lfb.setProfiler(&app->getProfiler());

	// Data needed for generating light grid.
	lightFbo.setSize(LIGHT_RES, LIGHT_RES);
	lightFbo.create(&lightDepths, true); // Could be false if you don't need stencils.

	lightDirs.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);
	lightColors.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);
	lightPositions.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);
	lightLODs.create(nullptr, GL_R32F, LIGHT_RES, LIGHT_RES, false);
	lightNormals.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);

	lightDirs.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightColors.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightPositions.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightLODs.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightNormals.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);

	lightFbo.bind();
	lightFbo.attachColorBuffer(&lightDirs);
	lightFbo.attachColorBuffer(&lightColors);
	lightFbo.attachColorBuffer(&lightPositions);
	lightFbo.attachColorBuffer(&lightLODs);
	lightFbo.attachColorBuffer(&lightNormals);
	lightFbo.setDrawBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lightFbo.unbind();
#if 0
	// Generate light fbo from the camera's perspective.
	lightCamFbo.setSize(LIGHT_RES, LIGHT_RES);
	lightCamFbo.create(&lightCamDepths, true); // Could be false if you don't need stencils.

	lightCamDirs.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);
	lightCamColors.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);
	lightCamPositions.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);
	lightCamLODs.create(nullptr, GL_R32F, LIGHT_RES, LIGHT_RES, false);
	lightCamNormals.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);

	lightCamDirs.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightCamColors.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightCamPositions.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightCamLODs.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightCamNormals.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);

	lightCamFbo.bind();
	lightCamFbo.attachColorBuffer(&lightCamDirs);
	lightCamFbo.attachColorBuffer(&lightCamColors);
	lightCamFbo.attachColorBuffer(&lightCamPositions);
	lightCamFbo.attachColorBuffer(&lightCamLODs);
	lightCamFbo.attachColorBuffer(&lightCamNormals);
	lightCamFbo.setDrawBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lightCamFbo.unbind();
#endif
}

void Indirect::render(App *app)
{
	lightFbo.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lightFbo.unbind();
#if 0
	lightCamFbo.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lightCamFbo.unbind();
#endif
	auto &camera = Renderer::instance().getActiveCamera();

	if (width != (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE) ||
		height != (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE))
	{
		width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
		height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);
		cluster.resize(width, height);
		mask.resize(width, height);

	#if USE_SPHERICAL_HARMONICS
		#if INTERPOLATE_SH
			resizeSHTex(app);
		#else
			shBuffer.create(nullptr, width * height * CLUSTERS * 9 * sizeof(Math::vec4));
		#endif
	#endif
	}

	gBuffer.resize(camera.getWidth(), camera.getHeight());

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

#if TEST_SPHERICAL_HARMONICS
	drawSH(app);
#else

#if DRAW_DEEPS
	captureDeepImages(app);
	renderDeepImages(app);
	glPopAttrib();
	return;
#endif

	//drawCameraDirs(app);
	static bool createOnce = false;
	if (!createOnce)
	{
		captureLightData(app);
		//captureLightCamData(app);

		captureDeepImages(app);

		createGlobalLightData(app);
	}
#if CREATE_LIGHTS_ONCE
	createOnce = true;
#endif

	captureGeometry(app);
	buildDepthMask(app);

#if DEEP_SHADOWS
	// Technically you shouldn't be relying on the CDI to do this, but meh.
	buildCubeMaps(app);
	//glPopAttrib();
	//return;
#endif

#if TEST_LIGHT_VECTORS
	drawLightVectors(app);
	glPopAttrib();
	return;
#endif
	//renderDeepImages(app);
	//drawConeVectors(app);

#if USE_POS_GRID
	buildPosGrid(app);
#endif

#if DEEP_MASKING
	buildDeepDirMask(app);
	buildDeepPixMask(app);
#endif

	buildLightGrid(app);

#if USE_SPHERICAL_HARMONICS
	buildSH(app);
#endif

	composite(app);
#endif
	glPopAttrib();
}

void Indirect::update(float dt)
{
	(void) (dt);
#if ANIM_LIGHT
	animLight(dt);
#endif
#if ANIM_SPHERES
	animSpheres(dt);
#endif
}

void Indirect::useLighting(App *app)
{
	app->getProfiler().clear();

	// To store the data, or the indices?
#if STORE_LIGHT_DATA
	cluster.setType(Cluster::LINEARIZED, "cluster", CLUSTERS, true, Cluster::FLOAT, true);
#else
	cluster.setType(Cluster::LINEARIZED, "cluster", CLUSTERS, false, Cluster::UINT, true);
#endif
	cluster.setProfiler(&app->getProfiler());
	gBuffer.setProfiler(&app->getProfiler());
	lfb.setProfiler(&app->getProfiler());

#if USE_POS_GRID
	clusterPos.setType(Cluster::LINEARIZED, "clusterPos", CLUSTER_POS_RES, true, Cluster::FLOAT, true);
	clusterPos.resize(CLUSTER_POS_RES, CLUSTER_POS_RES);
#endif

	auto &camera = Renderer::instance().getActiveCamera();
	width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

	cluster.resize(width, height);

	mask.setProfiler(&app->getProfiler());
	mask.regen(CLUSTERS, MAX_EYE_Z, GRID_CELL_SIZE);
	mask.resize(width, height);
	mask.useDepthPrePass(true);

//#if DEEP_SHADOWS
//	initCubeMapBuffers(app);
//#endif

#if USE_SPHERICAL_HARMONICS
	#if INTERPOLATE_SH
		resizeSHTex(app);
	#else
		shBuffer.create(nullptr, width * height * CLUSTERS * 9 * sizeof(Math::vec4));
	#endif
#endif

#if DEEP_MASKING
	int nDirs = (int) ceil((float) deepCams.size() / 32.0f);
	dirMaskBuffer.create(nullptr, nDirs * sizeof(unsigned int));

	// How many pixels?
	float res = (float) DEEP_RES;
	int intPerImg = (int) ceil((res * res) / 32.0f);
	pixMaskBuffer.create(nullptr, intPerImg * deepCams.size() * sizeof(unsigned int));
#endif

	reloadShaders(app);
}

void Indirect::setLightPos(App *app)
{
	(void) (app);

	// TODO: Set the light's position to the current camera position.
	// TODO: Set the view direction to the current view direction.
	// TODO: May as well print the details so we can hardcode values for benchmarking.

	auto &camera = Renderer::instance().getActiveCamera();

	auto mainLightPos = camera.getPos();
	auto lightDir = camera.getForward();

	lightCam.setPos(mainLightPos);
	lightCam.setViewDir(lightDir);
	lightCam.setViewport(0, 0, LIGHT_RES, LIGHT_RES);
	lightCam.update(0.01f);

	std::cout << "New light pos: " << mainLightPos << "\n";
	std::cout << "New light dir: " << lightDir << "\n";
}

void Indirect::reloadShaders(App *app)
{
	(void) (app);

	// Just basic rendering.
	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);

	// Capturing lfb data.
	auto vertCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/indirect/capture.vert");
	auto geomCapture = ShaderSourceCache::getShader("captureGeom").loadFromFile("shaders/indirect/capture.geom");
	auto fragCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/indirect/capture.frag");
	fragCapture.setDefine("INDEX_BY_TILE", "0");
	fragCapture.setDefine("LFB_FRAG_TYPE", "float");
	fragCapture.setDefine("USE_FLOAT", "1");
	fragCapture.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	fragCapture.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	fragCapture.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	fragCapture.setDefine("DEEP_COLS", Utils::toString(deepCols));
	fragCapture.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	fragCapture.setDefine("GET_COUNTS", "0");
	geomCapture.setDefine("BUILD_BRUTE_FORCE", Utils::toString(BUILD_BRUTE_FORCE));
	fragCapture.setDefine("BUILD_BRUTE_FORCE", Utils::toString(BUILD_BRUTE_FORCE));
	fragCapture.replace("LFB_NAME", "lfb");
	captureLfbShader.release();
	captureLfbShader.create(&vertCapture, &fragCapture, &geomCapture);

	auto vertCount = ShaderSourceCache::getShader("countVert").loadFromFile("shaders/indirect/capture.vert");
	auto geomCount = ShaderSourceCache::getShader("countGeom").loadFromFile("shaders/indirect/capture.geom");
	auto fragCount = ShaderSourceCache::getShader("countFrag").loadFromFile("shaders/indirect/capture.frag");
	fragCount.setDefine("INDEX_BY_TILE", "0");
	fragCount.setDefine("LFB_FRAG_TYPE", "float");
	fragCount.setDefine("USE_FLOAT", "1");
	fragCount.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	fragCount.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	fragCount.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	fragCount.setDefine("DEEP_COLS", Utils::toString(deepCols));
	fragCount.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	geomCount.setDefine("BUILD_BRUTE_FORCE", Utils::toString(BUILD_BRUTE_FORCE));
	fragCount.setDefine("BUILD_BRUTE_FORCE", Utils::toString(BUILD_BRUTE_FORCE));
	fragCount.setDefine("GET_COUNTS", "1");
	fragCount.replace("LFB_NAME", "lfb");
	countLfbShader.release();
	countLfbShader.create(&vertCount, &fragCount, &geomCount);

	// Deep image masking.
	auto zeroDirVert = ShaderSourceCache::getShader("zeroDirVert").loadFromFile("shaders/indirect/dirMask.vert");
	zeroDirVert.setDefine("ZERO_MASK", "1");
	zeroDirVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	zeroDirMaskShader.release();
	zeroDirMaskShader.create(&zeroDirVert);

	auto dirMaskVert = ShaderSourceCache::getShader("dirMaskVert").loadFromFile("shaders/indirect/dirMask.vert");
	dirMaskVert.setDefine("ZERO_MASK", "0");
	dirMaskVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	buildDirMaskShader.release();
	buildDirMaskShader.create(&dirMaskVert);

	// Sorting lfb data.
	// TODO: Figure out why the new sort approach sucks.
	bma.release();
	bma.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", "0"}});
	bma.createShaders(&lfb, "shaders/bitSort/sort.vert", "shaders/bitSort/sort.frag", {{"COMPOSITE_LOCAL", "0"}, {"INDEX_BY_TILE", "0"},
		{"USE_FLOAT", "1"}, {"LFB_FRAG_TYPE", "float"}, {"SINGLE_VAL", "1"}});
	bma.setProfiler(&app->getProfiler());
	//geometryLocalBMA.setProfiler(&profiler);

#if 0
	// So I can see what's going on.

	auto vertLocalSort = ShaderSourceCache::getShader("sortLocalVert").loadFromFile("shaders/bitSort/sort.vert");
	auto fragLocalSort = ShaderSourceCache::getShader("sortLocalFrag").loadFromFile("shaders/bitSort/sort.frag");
	fragLocalSort.setDefine("MAX_FRAGS", "512");
	fragLocalSort.setDefine("COMPOSITE_LOCAL", "0");
	fragLocalSort.setDefine("INDEX_BY_TILE", "0");
	fragLocalSort.setDefine("LFB_FRAG_TYPE", "float");
	fragLocalSort.setDefine("USE_FLOAT", "1");
	fragLocalSort.setDefine("SINGLE_VAL", "1");
	fragLocalSort.replace("LFB_NAME", "lfb");

	Shader tmpShader;
	tmpShader.create(&vertLocalSort, &fragLocalSort);
	Utils::File f("shader.txt");
	f.write(tmpShader.getBinary());
	f.close();
#endif

	// Drawing the lfb, for debugging.
	auto vertLfb = ShaderSourceCache::getShader("drawLfbVert").loadFromFile("shaders/indirect/drawLfb.vert");
	auto fragLfb = ShaderSourceCache::getShader("drawLfbFrag").loadFromFile("shaders/indirect/drawLfb.frag");
	fragLfb.replace("LFB_NAME", "lfb");
	fragLfb.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	fragLfb.setDefine("LIGHT_RES", Utils::toString(LIGHT_RES));
	fragLfb.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	fragLfb.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	fragLfb.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	fragLfb.setDefine("DEEP_COLS", Utils::toString(deepCols));
	fragLfb.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	fragLfb.setDefine("INDEX_BY_TILE", "0");
	drawLfbShader.release();
	drawLfbShader.create(&vertLfb, &fragLfb);

	// Creating a global list of all lights.
	// TODO: May want this to be a compute shader. Not sure.
	auto globalLightVert = ShaderSourceCache::getShader("globalLightVert").loadFromFile("shaders/indirect/globalLights.vert");
	globalLightVert.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	globalLightVert.setDefine("LIGHT_RES", Utils::toString(LIGHT_RES));
	globalLightVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	globalLightVert.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	globalLightVert.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	globalLightVert.setDefine("DEEP_COLS", Utils::toString(deepCols));
	globalLightVert.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	globalLightVert.setDefine("PIXEL_STEPPING", Utils::toString(PIXEL_STEPPING));
	globalLightVert.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	//globalLightVert.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	//globalLightVert.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	//globalLightVert.setDefine("GET_COUNTS", "0");
	globalLightShader.release();
	globalLightShader.create(&globalLightVert);

	// Capturing light data.
	auto lightDataVert = ShaderSourceCache::getShader("lightDataVert").loadFromFile("shaders/indirect/lightData.vert");
	auto lightDataFrag = ShaderSourceCache::getShader("lightDataFrag").loadFromFile("shaders/indirect/lightData.frag");
	lightDataShader.release();
	lightDataShader.create(&lightDataVert, &lightDataFrag);

	// Debug shader for rendering light data.
	auto debugVert = ShaderSourceCache::getShader("debugVert").loadFromFile("shaders/indirect/debug.vert");
	auto debugFrag = ShaderSourceCache::getShader("debugFrag").loadFromFile("shaders/indirect/debug.frag");
	debugShader.release();
	debugShader.create(&debugVert, &debugFrag);

	// Light grid shaders.
	auto clusterCountVert = ShaderSourceCache::getShader("clusterCountVert").loadFromFile("shaders/indirect/createLights.vert");
	auto clusterCountGeom = ShaderSourceCache::getShader("clusterCountGeom").loadFromFile("shaders/indirect/createLights.geom");
	auto clusterCountFrag = ShaderSourceCache::getShader("clusterCountFrag").loadFromFile("shaders/indirect/createLights.frag");
	clusterCountVert.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCountVert.setDefine("LIGHT_RES", Utils::toString(LIGHT_RES));
	clusterCountVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	clusterCountGeom.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCountFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	clusterCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	clusterCountFrag.setDefine("STORE_LIGHT_DATA", Utils::toString(STORE_LIGHT_DATA));
	clusterCountFrag.setDefine("GET_COUNTS", "1");
	countClusterShader.release();
	countClusterShader.create(&clusterCountVert, &clusterCountFrag, &clusterCountGeom);

	auto clusterCreateVert = ShaderSourceCache::getShader("clusterCreateVert").loadFromFile("shaders/indirect/createLights.vert");
	auto clusterCreateGeom = ShaderSourceCache::getShader("clusterCreateGeom").loadFromFile("shaders/indirect/createLights.geom");
	auto clusterCreateFrag = ShaderSourceCache::getShader("clusterCreateFrag").loadFromFile("shaders/indirect/createLights.frag");
	clusterCreateVert.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCreateVert.setDefine("LIGHT_RES", Utils::toString(LIGHT_RES));
	clusterCreateVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	clusterCreateGeom.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCreateFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	clusterCreateFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	clusterCreateFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	clusterCreateFrag.setDefine("STORE_LIGHT_DATA", Utils::toString(STORE_LIGHT_DATA));
	clusterCreateFrag.setDefine("GET_COUNTS", "0");
	createClusterShader.release();
	createClusterShader.create(&clusterCreateVert, &clusterCreateFrag, &clusterCreateGeom);

	// VPL shadow cubemaps.
	auto cubemapVert = ShaderSourceCache::getShader("cubemapVert").loadFromFile("shaders/indirect/vplCubemap.vert");
	auto cubemapGeom = ShaderSourceCache::getShader("cubemapGeom").loadFromFile("shaders/indirect/vplCubemap.geom");
	auto cubemapFrag = ShaderSourceCache::getShader("cubemapFrag").loadFromFile("shaders/indirect/vplCubemap.frag");
	cubemapFrag.setDefine("LIGHT_RES", Utils::toString(LIGHT_RES));
	cubemapFrag.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	cubemapFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	renderCubemapShader.release();
	renderCubemapShader.create(&cubemapVert, &cubemapFrag, &cubemapGeom);

	auto testCubeVert = ShaderSourceCache::getShader("testCubeVert").loadFromFile("shaders/indirect/testVplCubemap.vert");
	auto testCubeFrag = ShaderSourceCache::getShader("testCubeFrag").loadFromFile("shaders/indirect/testVplCubemap.frag");
	testCubemapShader.release();
	testCubemapShader.create(&testCubeVert, &testCubeFrag);

#if USE_POS_GRID
	// Global light positions.
	auto countPosVert = ShaderSourceCache::getShader("countPosVert").loadFromFile("shaders/indirect/clusterPos.vert");
	countPosVert.setDefine("CLUSTER_POS_RES", Utils::toString(CLUSTER_POS_RES));
	countPosVert.setDefine("GET_COUNTS", "1");
	countPosShader.release();
	countPosShader.create(&countPosVert);

	auto createPosVert = ShaderSourceCache::getShader("createPosVert").loadFromFile("shaders/indirect/clusterPos.vert");
	createPosVert.setDefine("CLUSTER_POS_RES", Utils::toString(CLUSTER_POS_RES));
	createPosVert.setDefine("GET_COUNTS", "0");
	createPosShader.release();
	createPosShader.create(&createPosVert);
#endif
	// Spherical harmonics shader.
	auto shVert = ShaderSourceCache::getShader("buildSHVert").loadFromFile("shaders/indirect/buildSH.vert");
	auto shFrag = ShaderSourceCache::getShader("buildSHFrag").loadFromFile("shaders/indirect/buildSH.frag");
	shFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	shFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	shFrag.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	shFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	shFrag.setDefine("INDEX_BY_TILE", "0");
	shFrag.setDefine("LFB_FRAG_TYPE", "float");
	shFrag.setDefine("USE_FLOAT", "1");
	shFrag.setDefine("INTERPOLATE_SH", Utils::toString(INTERPOLATE_SH));
	shFrag.setDefine("INTERPOLATE_2D", Utils::toString(INTERPOLATE_2D));
	buildSHShader.release();
	buildSHShader.create(&shVert, &shFrag);

	// Zeroing the spherical harmonics textures.
	auto shZero = ShaderSourceCache::getShader("zeroSHVert").loadFromFile("shaders/indirect/zeroSH.vert");
	shZero.setDefine("INTERPOLATE_2D", Utils::toString(INTERPOLATE_2D));
	zeroSHShader.release();
	zeroSHShader.create(&shZero);

	// Spherical harmonics debugging shaders.
	auto testSHVert = ShaderSourceCache::getShader("testSHVert").loadFromFile("shaders/indirect/testSH.vert");
	auto testSHFrag = ShaderSourceCache::getShader("testSHFrag").loadFromFile("shaders/indirect/testSH.frag");
	testSHFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	testSHFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	testSHFrag.setDefine("INTERPOLATE_SH", Utils::toString(INTERPOLATE_SH));
	testSHShader.release();
	testSHShader.create(&testSHVert, &testSHFrag);
	auto drawSHVert = ShaderSourceCache::getShader("drawSHVert").loadFromFile("shaders/indirect/drawSH.vert");
	auto drawSHFrag = ShaderSourceCache::getShader("drawSHFrag").loadFromFile("shaders/indirect/drawSH.frag");
	drawSHFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	drawSHFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	drawSHFrag.setDefine("INTERPOLATE_SH", Utils::toString(INTERPOLATE_SH));
	drawSHShader.release();
	drawSHShader.create(&drawSHVert, &drawSHFrag);

	// Composite shader.
	auto compositeVert = ShaderSourceCache::getShader("compositeVert").loadFromFile("shaders/indirect/composite.vert");
	auto compositeFrag = ShaderSourceCache::getShader("compositeFrag").loadFromFile("shaders/indirect/composite.frag");
	//compositeFrag.replace("LFB_NAME", "lfb");
	compositeFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	compositeFrag.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	compositeFrag.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	compositeFrag.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	compositeFrag.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	compositeFrag.setDefine("DEEP_COLS", Utils::toString(deepCols));
	compositeFrag.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	compositeFrag.setDefine("INDEX_BY_TILE", "0");
	compositeFrag.setDefine("LFB_FRAG_TYPE", "float");
	compositeFrag.setDefine("USE_FLOAT", "1");
	compositeFrag.setDefine("STORE_LIGHT_DATA", Utils::toString(STORE_LIGHT_DATA));
	compositeFrag.setDefine("USE_SPHERICAL_HARMONICS", Utils::toString(USE_SPHERICAL_HARMONICS));
	compositeFrag.setDefine("INTERPOLATE_SH", Utils::toString(INTERPOLATE_SH));
	compositeFrag.setDefine("INTERPOLATE_2D", Utils::toString(INTERPOLATE_2D));
	compositeFrag.setDefine("INDIRECT_SHADOWS", Utils::toString(INDIRECT_SHADOWS));
	compositeFrag.setDefine("DEEP_SHADOWS", Utils::toString(DEEP_SHADOWS));
	compositeShader.release();
	compositeShader.create(&compositeVert, &compositeFrag);

	// Debugging light directions.
	auto debugDirVert = ShaderSourceCache::getShader("debugDirVert").loadFromFile("shaders/indirect/debugDirs.vert");
	auto debugDirGeom = ShaderSourceCache::getShader("debugDirGeom").loadFromFile("shaders/indirect/debugDirs.geom");
	auto debugDirFrag = ShaderSourceCache::getShader("debugDirFrag").loadFromFile("shaders/indirect/debugDirs.frag");
	debugDirVert.setDefine("LIGHT_RES", Utils::toString(LIGHT_RES));
	debugDirVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	debugDirVert.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	debugDirVert.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	debugDirVert.setDefine("DEEP_COLS", Utils::toString(deepCols));
	debugDirVert.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	debugDirVert.setDefine("INDEX_BY_TILE", "0");
	debugDirShader.release();
	debugDirShader.create(&debugDirVert, &debugDirFrag, &debugDirGeom);
}

void Indirect::saveData(App *app)
{
	(void) (app);

	auto str = lfb.getStrSorted();
	Utils::File f("data.txt");
	f.write(str);
}

