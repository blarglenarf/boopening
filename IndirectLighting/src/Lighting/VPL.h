#pragma once

#include "Lighting.h"

#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Renderer/src/RenderObject/Texture.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/Lighting/GBuffer.h"
#include "../../../Renderer/src/Camera.h"

class VPL : public Lighting
{
private:
	//Rendering::Bloom bloom;
	Rendering::Camera lightCam;

	// Could simply use another gBuffer for light data.
	Rendering::FrameBuffer lightFbo;
	Rendering::RenderBuffer lightDepths;
	Rendering::Texture2D lightDirs;
	Rendering::Texture2D lightColors;
	Rendering::Texture2D lightPositions;
	Rendering::Texture2D lightLODs;

	Rendering::Shader lightDataShader;
	Rendering::Shader debugShader;

	Rendering::GBuffer gBuffer;

	Rendering::Shader compositeShader;

	Rendering::Shader basicShader;
	Rendering::Shader debugDirShader;

	std::vector<Rendering::FrameBuffer*> lightBuffers;
	std::vector<Rendering::Texture2DArray*> lightTexArrays;
	std::vector<Rendering::RenderBuffer*> lightDepthBuffers;

	Rendering::Shader lightDetermineShader;

private:
	void captureLightData(App *app);
	void captureGeometry(App *app);

	void determineLights(App *app);

	void composite(App *app);

	void drawLightVectors(App *app);

public:
	VPL() : Lighting() {}
	virtual ~VPL();

	virtual void init(App *app) override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLighting(App *app) override;

	virtual void reloadShaders(App *app) override;
	virtual void saveData(App *app) override;
};

