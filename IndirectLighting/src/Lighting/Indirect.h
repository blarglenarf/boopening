#pragma once

#include "Lighting.h"

#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Renderer/src/RenderObject/Texture.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/LFB/Cluster.h"
#include "../../../Renderer/src/LFB/ClusterMask.h"
#include "../../../Renderer/src/Lighting/GBuffer.h"
#include "../../../Renderer/src/Camera.h"

#include "../../../Renderer/src/LFB/LFB.h"
#include "../../../Renderer/src/LFB/BMA.h"

#include <vector>

class Indirect : public Lighting
{
private:
	//Rendering::Bloom bloom;
	Rendering::Camera lightCam;

	// For rendering multiple deep images simultaneously.
	Rendering::Camera deepCamZ;
	std::vector<Rendering::Camera> deepCams;
	Rendering::StorageBuffer deepCamBuffer;
	Rendering::FrameBuffer sortFbo;
	Rendering::Texture2D sortTex;
	Rendering::RenderBuffer sortDepth;

	// Need a deep image to render the geometry to.
	Rendering::LFB lfb;
	Rendering::BMA bma;
	Rendering::Shader captureLfbShader;
	Rendering::Shader countLfbShader;
	Rendering::Shader drawLfbShader;

	// Textures to store the cubemaps for each VPL.
	std::vector<Rendering::Texture2DArray*> cubemapDepthTextures;
	std::vector<Rendering::FrameBuffer*> cubemapDepthBuffers;
	std::vector<int> cubemapUniforms;
	std::vector<Math::mat4> vplMatrices;
	Rendering::StorageBuffer vplMatBuffer;
#if 0
	std::vector<Math::vec3> vplPos;
	std::vector<Math::vec3> vplDir;
#endif

	// Could simply use another gBuffer for light data.
	// Light data from the light's perspective.
	Rendering::FrameBuffer lightFbo;
	Rendering::RenderBuffer lightDepths;
	Rendering::Texture2D lightDirs;
	Rendering::Texture2D lightColors;
	Rendering::Texture2D lightPositions;
	Rendering::Texture2D lightLODs;
	Rendering::Texture2D lightNormals;

	// Light data from the camera's perspective.
	Rendering::FrameBuffer lightCamFbo;
	Rendering::RenderBuffer lightCamDepths;
	Rendering::Texture2D lightCamDirs;
	Rendering::Texture2D lightCamColors;
	Rendering::Texture2D lightCamPositions;
	Rendering::Texture2D lightCamLODs;
	Rendering::Texture2D lightCamNormals;

	// Textures for storing sh coefficients.
	Rendering::Texture3D shGrid[9]; // 3D grid.

	Rendering::FrameBuffer shFbo;
	Rendering::RenderBuffer shDepths;
	Rendering::Texture2D shTexes[9]; // 2D grid.

	Rendering::Shader lightDataShader;
	//Rendering::Shader lightCamDataShader;
	Rendering::Shader debugShader;

	Rendering::Cluster cluster;
	Rendering::ClusterMask mask;
	Rendering::GBuffer gBuffer;

	// TODO: What if we try to consolidate based on positions.
	// TODO: Cluster of light locations.
	// TODO: Then consolidate so each grid cell has only one position/colour/direction.
	Rendering::Cluster clusterPos;

	Rendering::Shader globalLightShader;

	Rendering::Shader countClusterShader;
	Rendering::Shader createClusterShader;

	Rendering::Shader countPosShader;
	Rendering::Shader createPosShader;

	// Shaders for VPL cubemaps.
	// TODO: Don't really need shaders to clear the cubmeaps, a glClear works fine, albeit slower.
	//Rendering::Shader clearCubemapShader;
	Rendering::Shader renderCubemapShader;
	Rendering::Shader testCubemapShader;

	// Spherical harmonics shaders.
	Rendering::Shader buildSHShader;
	Rendering::Shader zeroSHShader;
	Rendering::Shader testSHShader;
	Rendering::Shader drawSHShader;

	Rendering::Shader zeroDirMaskShader;
	Rendering::Shader buildDirMaskShader;

	Rendering::Shader compositeShader;

	Rendering::Shader basicShader;
	Rendering::Shader debugDirShader;

	// In case we want a global array of lights that can be indexed.
	Rendering::AtomicBuffer lightCounter;
	Rendering::StorageBuffer globalLightPos;
	Rendering::StorageBuffer globalLightDir;

	Rendering::StorageBuffer shBuffer;

	Rendering::StorageBuffer dirMaskBuffer;
	Rendering::StorageBuffer pixMaskBuffer;

	Math::vec3 mainLightPos;
	Math::vec3 mainLightAnimDir;

	std::vector<Math::vec3> spherePositions;
	std::vector<Math::vec3> sphereDirections;
	std::vector<float> sphereCollisions;

	Rendering::GPUMesh sphereMesh;

	int width;
	int height;

	int stacks;
	int slices;

	int deepCols;
	int deepRows;

	int nLights;

private:
	void createCameras();
	void drawCameraDirs(App *app);

	void drawSpheres(App *app, Math::mat4 mvMatrix);

	void drawSH(App *app);

	void resizeSHTex(App *app);

	void initCubeMapBuffers(App *app);
	void buildCubeMaps(App *app);

	void buildDeepDirMask(App *app);
	void buildDeepPixMask(App *app);

	void captureDeepImages(App *app);
	void renderDeepImages(App *app);
	void renderDeepImage(App *app, int index, int pixel = -1);

	void drawLightVectors(App *app);
	void drawConeVectors(App *app);

	void countClusterPos(App *app); // TODO: This doesn't have to be linearised, could just be linked list.
	void createClusterPos(App *app);
	void createGlobalLightData(App *app);

	void countClusters(App *app);
	void createClusters(App *app);

	void captureLightData(App *app);
	void captureLightCamData(App *app);
	void captureGeometry(App *app);

	void buildPosGrid(App *app);

	void buildDepthMask(App *app);
	void buildLightGrid(App *app);

	void zeroSH(App *app);
	void buildSH(App *app);

	void composite(App *app);

	void animLight(float dt);
	void animSpheres(float dt);

public:
	Indirect() : Lighting(), width(0), height(0), stacks(0), slices(0), deepCols(0), deepRows(0), nLights(0) {}
	virtual ~Indirect() {}

	virtual void init(App *app) override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLighting(App *app) override;
	virtual void setLightPos(App *app) override;

	virtual void reloadShaders(App *app) override;
	virtual void saveData(App *app) override;

	Rendering::Camera &getLightCam() { return lightCam; }
};

