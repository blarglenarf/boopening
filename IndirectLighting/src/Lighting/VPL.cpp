#include "VPL.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"

#include "../../../Resources/src/ResourcesCommon.h"

#include <string>


// TODO: Implement the determineLights function.
// TODO: Use info from that in the composite function.

// TODO: How about instead of rendering geometry using a VPL, we store a point cloud or deep image, and lookup data instead.
// TODO: Then if we generate ranges using stochastic light culling, we can compare against a modern VPL technique.

#define LIGHT_RES 256
#define DEBUG_DIRS 0

using namespace Rendering;
using namespace Math;


void VPL::captureLightData(App *app)
{
	auto &camera = lightCam;

	app->getProfiler().start("Capture light");

	// Needs to be rendered at lower res.
	app->setTmpViewport(LIGHT_RES, LIGHT_RES);

	// Render colours/depths/light dirs to a texture from light pov.
	// Textures: reflected vectors, geometry colours, geometry positions (can be calculated from just depths I guess).
	lightFbo.bind();
	lightDataShader.bind();
	lightDataShader.setUniform("lightPos", vec3(1.27416f, 5.84406f, -0.638992f));
	lightDataShader.setUniform("mvMatrix", camera.getInverse());
	lightDataShader.setUniform("pMatrix", camera.getProjection());
	lightDataShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	app->getGpuMesh().render();
	lightDataShader.unbind();
	lightFbo.unbind();

	lightDirs.genMipmap();
	lightColors.genMipmap();
	lightPositions.genMipmap();
	lightLODs.genMipmap();

	app->restoreViewport();

	app->getProfiler().time("Capture light");
}

void VPL::captureGeometry(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	auto &shader = gBuffer.getCaptureShader();

	app->getProfiler().start("Capture g-buffer");

	// Capture data into the g-buffer.
	gBuffer.beginCapture();
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	gBuffer.setUniforms(&shader);
	app->getGpuMesh().render();
	shader.unbind();
	gBuffer.endCapture();

	app->getProfiler().time("Capture g-buffer");
}

void VPL::determineLights(App *app)
{
	app->getProfiler().start("Determine lights");

	// TODO: Draw geometry from perspective of each light, what do you see?
	// TODO: Apply this directly to a storage buffer, using data from the g-buffer.
	// TODO: So we don't need a bunch of textures.

	// TODO: Otherwise save this data to a whole lot of textures (probably a vector of 2d texture arrays).
	// TODO: Means that we need to render geometry once for each texture array (lots of renders, not ideal).
	// TODO: This will be fairly slow, but accuracy is more important than speed for this technique.

	//auto &camera = Renderer::instance().getActiveCamera();

	app->setTmpViewport(LIGHT_RES, LIGHT_RES);

	lightDetermineShader.bind();

	lightDirs.bind();
	lightPositions.bind();

	lightDetermineShader.setUniform("lightDir", &lightDirs);
	lightDetermineShader.setUniform("lightPos", &lightPositions);

	// TODO: Does the projection matrix change for each light or not?

	// TODO: Could transform rendered pixels into camera screen space, see what's lit.
	for (int i = 0; i < (LIGHT_RES * LIGHT_RES) / 2048; i++)
	{
		lightBuffers[i]->bind();

		// TODO: Do I need to do more than this?
		app->getGpuMesh().render();

		lightBuffers[i]->unbind();
	}

	lightDirs.unbind();
	lightPositions.unbind();

	lightDetermineShader.unbind();

	// TODO: Need some way of debugging/verifying that this actually works.

	app->restoreViewport();

	app->getProfiler().time("Determine lights");
}

void VPL::composite(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Deferred rendering of the scene, applying lights from the grid.
	app->getProfiler().start("Composite");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	compositeShader.bind();
	gBuffer.bindTextures();
	gBuffer.setUniforms(&compositeShader);

	compositeShader.setUniform("mvMatrix", camera.getInverse());
	compositeShader.setUniform("pMatrix", camera.getProjection());
	compositeShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	compositeShader.setUniform("viewport", camera.getViewport());
	compositeShader.setUniform("invPMatrix", camera.getInverseProj());

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();
	compositeShader.unbind();

	app->getProfiler().time("Composite");
}

void VPL::drawLightVectors(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	debugDirShader.bind();
	lightDirs.bind();
	lightColors.bind();
	lightPositions.bind();
	debugDirShader.setUniform("dirTex", &lightDirs);
	debugDirShader.setUniform("colTex", &lightColors);
	debugDirShader.setUniform("posTex", &lightPositions);
	debugDirShader.setUniform("mvMatrix", camera.getInverse());
	debugDirShader.setUniform("pMatrix", camera.getProjection());
	glDrawArrays(GL_POINTS, 0, LIGHT_RES * LIGHT_RES);
	lightDirs.unbind();
	lightColors.unbind();
	lightPositions.unbind();
	debugDirShader.unbind();

	app->drawMesh();
}


VPL::~VPL()
{
	for (int i = 0; i < (LIGHT_RES * LIGHT_RES) / 2048; i++)
	{
		delete lightBuffers[i];
		delete lightTexArrays[i];
		delete lightDepthBuffers[i];
		lightBuffers[i] = nullptr;
		lightTexArrays[i] = nullptr;
		lightDepthBuffers[i] = nullptr;
	}
	lightBuffers.clear();
	lightTexArrays.clear();
	lightDepthBuffers.clear();
}

void VPL::init(App *app)
{
	(void) (app);

	// Good test scene: https://renderman.pixar.com/resources/RenderMan_20/images/figures.26/softshadfig1.gif
#if 1
	// Atrium high light:
	// pos: 1.27416,5.84406,-0.638992
	// euler rot: -1.22173,2.21657,0
	lightCam.setType(Camera::Type::PERSP);
	lightCam.setPos({1.27416f, 5.84406f, -0.638992f});
	lightCam.setEulerRot({-1.22173f, 2.21657f, 0.0f});
	lightCam.setViewport(0, 0, LIGHT_RES, LIGHT_RES);
	lightCam.update(0.01f);
#else
	// Galleon light:
	// pos: 0.633686,1.88747,0.667765
	// euler rot: -0.523599,-2.41728,0
	// zoom: 12.5413
	lightCam.setType(Camera::Type::PERSP);
	lightCam.setPos({-6.22926f, 11.4873f, -7.14018});
	lightCam.setEulerRot({-0.705858f, -2.46964f, 0.0f});
	lightCam.setViewport(0, 0, LIGHT_RES, LIGHT_RES);
	lightCam.update(0.01f);
#endif
	// Data needed for generating light grid.
	lightFbo.setSize(LIGHT_RES, LIGHT_RES);
	lightFbo.create(&lightDepths, true); // Could be false if you don't need stencils.

	lightDirs.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);
	lightColors.create(nullptr, GL_RGB, LIGHT_RES, LIGHT_RES, false);
	lightPositions.create(nullptr, GL_RGB32F, LIGHT_RES, LIGHT_RES, false);
	lightLODs.create(nullptr, GL_R32F, LIGHT_RES, LIGHT_RES, false);

	lightDirs.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightColors.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightPositions.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	lightLODs.setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);

	lightFbo.bind();
	lightFbo.attachColorBuffer(&lightDirs);
	lightFbo.attachColorBuffer(&lightColors);
	lightFbo.attachColorBuffer(&lightPositions);
	lightFbo.attachColorBuffer(&lightLODs);
	lightFbo.setDrawBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lightFbo.unbind();

	// For rendering geometry from perspective of each light.
	size_t nBuffers = (LIGHT_RES * LIGHT_RES) / 2048;
	lightBuffers.resize(nBuffers);
	lightTexArrays.resize(nBuffers);
	lightDepthBuffers.resize(nBuffers);

	for (size_t i = 0; i < nBuffers; i++)
	{
		lightBuffers[i] = new FrameBuffer();
		lightTexArrays[i] = new Texture2DArray();
		lightDepthBuffers[i] = new RenderBuffer();

		// Could use LIGHT_RES or actual screen resolution, not sure which is best.
		lightBuffers[i]->setSize(LIGHT_RES, LIGHT_RES, 2048); // Note: depth assumes there are 2048 lights per array.
		lightBuffers[i]->create(GL_R32F, lightTexArrays[i], lightDepthBuffers[i]);
	}
}

void VPL::render(App *app)
{
	lightFbo.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lightFbo.unbind();

	auto &camera = Renderer::instance().getActiveCamera();

	gBuffer.resize(camera.getWidth(), camera.getHeight());

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	captureLightData(app);

#if DEBUG_DIRS
	drawLightVectors(app);
#else
	captureGeometry(app);

	//determineLights(app);

	composite(app);
#endif

	glPopAttrib();
}

void VPL::update(float dt)
{
	(void) (dt);
}

void VPL::useLighting(App *app)
{
	gBuffer.setProfiler(&app->getProfiler());

	reloadShaders(app);
}

void VPL::reloadShaders(App *app)
{
	(void) (app);

	// Just basic rendering.
	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);

	// Capturing light data.
	auto lightDataVert = ShaderSourceCache::getShader("lightDataVert").loadFromFile("shaders/vpl/lightData.vert");
	auto lightDataFrag = ShaderSourceCache::getShader("lightDataFrag").loadFromFile("shaders/vpl/lightData.frag");
	lightDataShader.release();
	lightDataShader.create(&lightDataVert, &lightDataFrag);

	// Determining visible geometry for each light.
	auto lightDetermVert = ShaderSourceCache::getShader("lightDetermVert").loadFromFile("shaders/vpl/lightDeterm.vert");
	auto lightDetermFrag = ShaderSourceCache::getShader("lightDetermFrag").loadFromFile("shaders/vpl/lightDeterm.frag");
	auto lightDetermGeom = ShaderSourceCache::getShader("lightDetermGeom").loadFromFile("shaders/vpl/lightDeterm.geom");
	lightDetermineShader.release();
	lightDetermineShader.create(&lightDetermVert, &lightDetermFrag, &lightDetermGeom);

	// Debug shader for rendering light data.
	auto debugVert = ShaderSourceCache::getShader("debugVert").loadFromFile("shaders/vpl/debug.vert");
	auto debugFrag = ShaderSourceCache::getShader("debugFrag").loadFromFile("shaders/vpl/debug.frag");
	debugShader.release();
	debugShader.create(&debugVert, &debugFrag);

	// Composite shader.
	auto compositeVert = ShaderSourceCache::getShader("compositeVert").loadFromFile("shaders/vpl/composite.vert");
	auto compositeFrag = ShaderSourceCache::getShader("compositeFrag").loadFromFile("shaders/vpl/composite.frag");
	compositeShader.release();
	compositeShader.create(&compositeVert, &compositeFrag);

	// Debugging light directions.
	auto debugDirVert = ShaderSourceCache::getShader("debugDirVert").loadFromFile("shaders/vpl/debugDirs.vert");
	auto debugDirGeom = ShaderSourceCache::getShader("debugDirGeom").loadFromFile("shaders/vpl/debugDirs.geom");
	auto debugDirFrag = ShaderSourceCache::getShader("debugDirFrag").loadFromFile("shaders/vpl/debugDirs.frag");
	debugDirVert.setDefine("LIGHT_RES", Utils::toString(LIGHT_RES));
	debugDirShader.release();
	debugDirShader.create(&debugDirVert, &debugDirFrag, &debugDirGeom);
}

void VPL::saveData(App *app)
{
	(void) (app);
#if 0
	auto str = cluster.getStr();
	Utils::File f("data.txt");
	f.write(str);
#endif
}

