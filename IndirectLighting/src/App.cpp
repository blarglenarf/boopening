#include "App.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include "Lighting/Standard.h"
#include "Lighting/VPL.h"
#include "Lighting/Indirect.h"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <iomanip>

#define DRAW_FPS 1
#define DRAW_AXES 1


using namespace Math;
using namespace Rendering;


// TODO: Get rid of this!
std::string currLightStr;
Flythrough anim;
Mesh mesh;
Font testFont;
float tpf = 0;



void App::init()
{
	auto &camera = Renderer::instance().getActiveCamera();

	auto &interpolator = anim.getInterpolator(anim.addInterpolator(const_cast<vec3*>(&camera.getEulerRot()), 10, vec3(-0.0523594,0.855212,0)));
	interpolator.addKeyFrame(0, vec3(-0.0523594,0.855212,0));
	interpolator.addKeyFrame(10, vec3(-0.061086,-0.715585,0));

#if 0
	anim.setAnimFilename("animations/anim.jpg");
#endif

	// TODO: Get rid of this!
	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");

	profiler.clear();

	currLighting = useLighting(0);
	currMesh = useMesh(0);

	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);

	// Might as well set the initial camera pos.
	//auto &camera = Renderer::instance().getActiveCamera();
	camera.setPos({-0.85f, -0.1f, 0.0f});
	camera.setZoom(6.25342f);
	camera.update(0.1f);

#if 0
	// TODO: Use this camera for the different deep image cameras!!!!
	float wsMin = -15;
	float wsMax = 15;
	float scale = (float) 128 / ((float) wsMax - (float) wsMin);

	camera.setType(Camera::Type::ORTHO);
	camera.setDistance(0, (float) 128);
	camera.setPos({wsMin, wsMin, -wsMin});
	//camera.setViewDir({-1, -1, -1});
	camera.setZoom(scale);
	camera.setViewport(0, 0, 128, 128);
	camera.update(0.1f);
#endif
}

void App::setTmpViewport(int width, int height)
{
	auto &camera = Renderer::instance().getActiveCamera();

	tmpWidth = camera.getWidth();
	tmpHeight = camera.getHeight();

	camera.setViewport(0, 0, width, height);
	camera.uploadViewport();
	camera.update(0);
}

void App::restoreViewport()
{
	auto &camera = Renderer::instance().getActiveCamera();

	camera.setViewport(0, 0, tmpWidth, tmpHeight);
	camera.uploadViewport();
	camera.update(0);
}

void App::drawMesh()
{
	drawMesh(Renderer::instance().getActiveCamera());
}

void App::drawMesh(Rendering::Camera &camera)
{
	//auto &camera = Renderer::instance().getActiveCamera();
	//bloom.resize(camera.getWidth(), camera.getHeight());

	//bloom.beginCapture();

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	basicShader.bind();
	basicShader.setUniform("mvMatrix", camera.getInverse());
	basicShader.setUniform("pMatrix", camera.getProjection());
	basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	basicShader.setUniform("lightDir", Math::vec3(0, 0, 1));
	gpuMesh.render();
	basicShader.unbind();

	glPopAttrib();

	//bloom.endCapture();

	// Blur the bright texture and composite.
	//bloom.applyBlur();
	//bloom.composite();

	//bloom.debugRenderColor();
	//bloom.debugRenderBright();
	//bloom.debugRenderBlur(3);
}

void App::render()
{
	// TODO: Use deferred rendering for this, or maybe give an option for deferred or forward.
	static float fpsUpdate = 0;

	profiler.begin();
	profiler.start("Total");

	auto &camera = Renderer::instance().getActiveCamera();
	camera.upload();

	if (lighting)
		lighting->render(this);

	profiler.time("Total");

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);

#if DRAW_FPS
	if (hudFlag)
	{
		float total = profiler.getTime("Total");
		float smoothing = 0.9f; // Larger => more smoothing.
		tpf = (tpf * smoothing) + (total * (1.0f - smoothing));
		float t = round(total);
		#if 0
			testFont.renderFromAtlas("Hybrid Lighting\nTime per frame(ms): " + Utils::toString(t) +
				"\nFrames per second (fps): " + Utils::toString(1000.0 / t) +
				"\nLights: " + Utils::toString(lightColors.size()), 10, 10, 0, 0, 1);
		#else
			testFont.renderFromAtlas("Technique: " + currLightStr +
				"\nTime per frame (ms): " + Utils::toString(t) +
				"\nFrames per second (fps): " + Utils::toString(1000.0 / t), 10, 10, 0, 0, 0, 1);
		#endif
	}
#endif

	float dt = Platform::Window::getDeltaTime();
	fpsUpdate += dt;
	if (fpsUpdate > 2)
	{
		profiler.print();
		fpsUpdate = 0;
	}

#if 0
	if (t > 1000)
	{
		std::cout << "Slow frametime. Exiting.\n";
		exit(EXIT_SUCCESS);
	}
#endif

#if DRAW_AXES
	if (hudFlag)
	{
		Renderer::instance().drawAxes();
		Renderer::instance().drawAxes(Renderer::instance().getActiveCamera().getPos(), 2);
	}
#endif

	glPopAttrib();
}

void App::update(float dt)
{
	if (lighting)
		lighting->update(dt);

	if (anim.isAnimating())
	{
		anim.update(dt);
		Renderer::instance().getActiveCamera().setDirty();
	}
}


int App::useMesh(int currMesh)
{
	static std::vector<std::string> meshes = { "meshes/dragon.obj", "meshes/sponza/sponza.3ds", "meshes/rungholt/rungholt.obj", "meshes/livingroom/living_room.obj",
		"meshes/sibenik/sibenik.obj", "meshes/sanmiguel/san-miguel.obj", "meshes/galleon.obj" };

	if (currMesh > (int) meshes.size() - 1)
		currMesh = 0;
	else if (currMesh < 0)
		currMesh = (int) meshes.size() - 1;

	gpuMesh.release();

	std::cout << "Loading mesh: " << meshes[currMesh] << "\n";

	mesh.clear();
	if (Resources::MeshLoader::load(&mesh, meshes[currMesh]))
	{
		if (meshes[currMesh] == "meshes/powerplant/powerplant.ctm")
			mesh.scale(vec3(0.0001f, 0.0001f, 0.0001f));
		else if (meshes[currMesh] == "meshes/rungholt/rungholt.obj")
			mesh.scale(vec3(0.04f, 0.04f, 0.04f));
		else if (meshes[currMesh] == "meshes/sanmiguel/san-miguel.obj")
			mesh.scale(vec3(0.1f, 0.1f, 0.1f));
		else if (meshes[currMesh] == "meshes/sibenik/sibenik.obj")
			mesh.scale(vec3(0.5f, 0.5f, 0.5f));
		else if (meshes[currMesh] == "meshes/hairball.ctm")
			mesh.scale(vec3(-10, -10, -10), vec3(10, 10, 10));
		else if (meshes[currMesh] == "meshes/sponza/sponza.3ds")
		{
			mesh.scale(vec3(-10, -5, -10), vec3(10.0f, 5.0f, 10.0f));
			// Do you want to remove the cloth?
			//mesh = mesh.extractFromMaterials({"sponza16___Default"}, true);
		}
		else if (meshes[currMesh] == "meshes/livingroom/living_room.obj")
			mesh.scale(vec3(-10, -5, -10), vec3(10.0f, 5.0f, 10.0f));
		else if (meshes[currMesh] == "meshes/galleon.obj")
			mesh.scale(vec3(0.2, 0.2, 0.2));
		else if (meshes[currMesh] == "meshes/teapot/teapot.obj")
			mesh.scale(vec3(2.0, 2.0, 2.0));
		else if (meshes[currMesh] == "meshes/dragon.obj")
			mesh.scale(vec3(2.0, 2.0, 2.0));

		std::cout << "No. triangles: " << mesh.getNumIndices() / 3 << ", no. vertices: " << mesh.getNumVertices() << "\n";
		gpuMesh.create(&mesh);
	}

	return currMesh;
}

void App::loadNextMesh()
{
	currMesh = useMesh(currMesh + 1);
}

void App::loadPrevMesh()
{
	currMesh = useMesh(currMesh - 1);
}

int App::useLighting(int currLighting)
{
	static std::vector<std::string> lightStrs = { "standard", "Indirect"  };
	static std::vector<Lighting::Type> lightTypes = { Lighting::STANDARD, Lighting::INDIRECT };

	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();

	if (currLighting > (int) lightStrs.size() - 1)
		currLighting = 0;
	else if (currLighting < 0)
		currLighting = (int) lightStrs.size() - 1;

	std::cout << "Using lighting: " << lightStrs[currLighting] << "\n";

	if (lighting)
		delete lighting;
	Lighting::Type type = lightTypes[currLighting];

	switch (type)
	{
	case Lighting::STANDARD:
		lighting = new Standard();
		break;
	case Lighting::VPL:
		lighting = new VPL();
		break;
	case Lighting::INDIRECT:
		lighting = new Indirect();
		break;
	default:
		lighting = new Standard();
		break;
	}

	lighting->init(this);
	lighting->useLighting(this);

	return currLighting;
}

void App::useNextLighting()
{
	currLighting = useLighting(currLighting + 1);
}

void App::usePrevLighting()
{
	currLighting = useLighting(currLighting - 1);
}

void App::setLightPos()
{
	lighting->setLightPos(this);
}

void App::reloadShaders()
{
#if 0
	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();
#endif

	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);

	if (lighting)
	{
		std::cout << "Reloading shaders\n";
		lighting->reloadShaders(this);
	}
}

void App::saveData()
{
	if (lighting)
		lighting->saveData(this);
}

void App::playAnim()
{
	anim.play();
}

void App::runBenchmarks()
{
	Benchmark benchmark(&profiler, [this]() -> void { this->render(); } );

	// Set camera view for benchmark.
	auto &camera = Renderer::instance().getActiveCamera();

	std::vector<std::string> times = {"Capture g-buffer", "Capture light", "Global lights", "Light grid build", "Composite",
		"LFB create", "LFB sort", "Total"};
	std::vector<std::string> data = {"lfbTotal", "clusterTotalData", "g-buffer"};

	float testTime = 10;

	// Note: Assumes Atrium mesh is currently loaded, format is linked list and technique is stepwise.

	// Atrium Tests.
	// Atrium LL-S.
	benchmark.addTest("Atrium",
		[&camera]() -> void
		{
			camera.setPos({-0.0967992f, -2.74396f, -0.180925f});
			camera.setEulerRot({0.008727f, 3.10668f, 0.0f});
			camera.setZoom(5.9259f);
			camera.update(0.1f);
		}, testTime, times, data);

	// Powerplant tests.
	// Power LL-S.
	benchmark.addTest("Rungholt",
		[&camera, this]() -> void
		{
			// Rungholt camera.
			camera.setPos({2.90805,3.25054,3.62739});
			camera.setEulerRot({-0.706858f, 0.733033f, 0.0f});
			camera.setZoom(0.0f);
			camera.update(0.1f);

			// Rungholt light.
			auto &lightCam = ((Indirect*) this->lighting)->getLightCam();
			lightCam.setPos({-0.110378f, 11.5089f, 0.107866f});
			lightCam.setViewDir({-0.0654282f, -0.996917f, 0.0433052f});
			lightCam.setZoom(0.0f);
			lightCam.update(0.1f);

			this->loadNextMesh();
		}, testTime, times, data);

	benchmark.setRenderFunc([this]() -> void
	{
		Renderer::instance().getActiveCamera().setViewport(0, 0, Platform::Window::getWidth(), Platform::Window::getHeight());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glLoadIdentity();
		Renderer::instance().getActiveCamera().update(1.0f);
		Renderer::instance().getActiveCamera().upload();
		this->render();
	});

	benchmark.run();
	benchmark.print();

	// Append data+time to the title, and save to a benchmarks folder.
	time_t t = time(0);
	struct tm buf;
	#if _WIN32
		localtime_s(&buf, &t);
	#else
		localtime_r(&t, &buf);
	#endif
	std::stringstream ss;
	ss << std::put_time(&buf, "-%d-%m-%Y-%H-%M");
	benchmark.writeCSV("benchmarks/benchmark" + ss.str() + ".csv", times, data);

	exit(0);
}

