#pragma once

#include "GL.h"

#include <string>
#include <vector>
#include <map>
#include <functional>

namespace Rendering
{
	class Profiler;

	class Benchmark
	{
	private:
		struct Test
		{
			std::string name;
			std::function<void()> initFunc;
			double time;
			std::map<std::string, std::vector<double>> times;
			std::map<std::string, std::vector<double>> data;

			Test(const std::string &name = "", std::function<void()> initFunc = std::function<void()>(), double time = 0) : name(name), initFunc(initFunc), time(time) {}
		};

	private:
		std::vector<Test> tests;
		std::function<void()> renderFunc;

		double currTime;

		Profiler *profiler;

	public:
		Benchmark(Profiler *profiler = nullptr, std::function<void()> renderFunc = std::function<void()>()) : renderFunc(renderFunc), currTime(0), profiler(profiler) {}

		void clear() { tests.clear(); }

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }
		void setRenderFunc(std::function<void()> renderFunc) { this->renderFunc = renderFunc; }

		void addTest(const std::string &name, std::function<void()> initFunc, double time, std::vector<std::string> times, std::vector<std::string> data);

		void run();

		void print();
		void writeCSV(const std::string &filename, std::vector<std::string> &timeTitles, std::vector<std::string> &dataTitles);
	};
}

