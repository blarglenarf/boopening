#include "Benchmark.h"

#include "Profiler.h"
#include "../../Platform/src/Window.h"
#include "../../Utils/src/File.h"

#include <sstream>
#include <iostream>
#include <algorithm>

using namespace Rendering;


static double getMean(std::vector<double> &v)
{
	if (v.empty())
		return 0;
	double mean = 0;
	for (double d : v)
		mean += d;
	mean /= (double) v.size();
	return mean;
}

static double getMedian(std::vector<double> &v)
{
	if (v.empty())
		return 0;
	auto s = v;
	std::sort(s.begin(), s.end()); // Not really necessary to do a full sort, but I'm lazy.
	return s[s.size() / 2 - 1];
}
#if 0
static std::vector<double> getDeviations(std::vector<double> &v, double m)
{
	if (v.empty())
		return std::vector<double>();
	std::vector<double> deviations(v.size());

	for (size_t i = 0; i < v.size(); i++)
	{
		double d = v[i];
		deviations[i] = (d - m) * (d - m);
		deviations[i] = sqrt(deviations[i]);
	}
	return deviations;
}

static double getVariance(std::vector<double> &v)
{
	if (v.empty())
		return 0;
	double mean = getMean(v);
	double variance = 0;

	for (double d : v)
	{
		double deviation = (d - mean) * (d - mean);
		variance += deviation;
	}
	variance / (double) v.size(); // Note: if v is a random sample, need to divide by (v.size() - 1).
	return variance;
}

static double getStdDev(std::vector<double> &v)
{
	if (v.empty())
		return 0;
	double variance = getVariance(v);
	return sqrt(variance);
}
#endif



void Benchmark::addTest(const std::string &name, std::function<void()> initFunc, double time, std::vector<std::string> times, std::vector<std::string> data)
{
	Test t(name, initFunc, time);
	for (auto &timeName : times)
		t.times[timeName] = std::vector<double>();
	for (auto &dataName : data)
		t.data[dataName] = std::vector<double>();

	tests.push_back(t);
}

void Benchmark::run()
{
	if (tests.empty())
	{
		std::cout << "No tests to benchmark\n";
		return;
	}

	if (!profiler)
	{
		std::cout << "Profiler not set for benchmarking\n";
		return;
	}

	std::cout << "Running benchmarks\n";

	for (auto &test : tests)
	{
		std::cout << "Running test " << test.name << "\n";
		currTime = 0;

		// Run the test init function.
		test.initFunc();

		Platform::Window::swapBuffers();
		Platform::Window::resetDeltaTime();

		// Run the render func until currTime exceeds test time.
		while (currTime <= test.time)
		{
			renderFunc();
			currTime += Platform::Window::getDeltaTime();

			// Add profiler results to the benchmark.
			auto profileTimes = profiler->getTimes();
			for (auto &profTime : profileTimes)
				test.times[profTime.first].push_back(profTime.second);
			for (auto &it : test.data)
				it.second.push_back((double) profiler->getBufferSize(it.first) / 1000000.0);

			Platform::Window::swapBuffers();
		}
	}

	currTime = 0;

	std::cout << "Finished benchmarking\n";
}

void Benchmark::print()
{
	// TODO: Report the median, ignore outliers.
	std::cout << "Benchmark results\n\n";

	for (auto &test : tests)
	{
		std::cout << "Test " << test.name << "\n";
		std::cout << "Times: \n";

		for (auto &it : test.times)
			std::cout << it.first << ": " << getMean(it.second) << " ms\n";

		std::cout << "\nMemory: \n";

		for (auto &it : test.data)
			std::cout << it.first << ": " << getMean(it.second) << " mb\n";

		std::cout << "\n\n";
	}
}

void Benchmark::writeCSV(const std::string &filename, std::vector<std::string> &timeTitles, std::vector<std::string> &dataTitles)
{
	// TODO: Ignore outliers, and not really sure if I want the mean or median here...
	std::stringstream csv;

	// Firstly, what platform are we using?
	csv << (const char*) glGetString(GL_VENDOR) << " " << (const char*) glGetString(GL_RENDERER) << " " << (const char*) glGetString(GL_VERSION) << "\n";

	// Titles for each column.
	csv << "Test,";
	for (auto &title : timeTitles)
		csv << title << " (ms)" << ",";
	for (auto &title : dataTitles)
		csv << title << " (mb)" << ",";
	csv << "\n";

	// Now just throw out the data, if it needs reformatting to be more readable, that's what python is for.
	for (auto &test : tests)
	{
		csv << test.name << ",";
		for (auto &title : timeTitles)
			csv << getMedian(test.times[title]) << ",";
		for (auto &title : dataTitles)
			csv << getMedian(test.data[title]) << ",";
		csv << "\n";
	}

	Utils::File file(filename);
	file.write(csv.str());
}

