#pragma once

#define GL_GLEXT_PROTOTYPES

#include <GL/glew.h>

#if _WIN32
	#include <Windows.h>
#endif

#include <GL/gl.h>

#include <string>

#ifndef GL_CONSERVATIVE_RASTERIZATION_NV
#define GL_CONSERVATIVE_RASTERIZATION_NV 0x9346
#endif

#ifndef GL_FILL_RECTANGLE_NV
#define GL_FILL_RECTANGLE_NV             0x933C
#endif

#define CHECKERRORS Rendering::checkErrors(__FILE__, __LINE__, false)

namespace Rendering
{
	void logGLError(std::string msg);

	bool checkErrors(const char* filename, int line, bool silent);
}

#if _WIN32
	void *getGLFuncAddr(const char *name);
#endif

void printExtensionList();

