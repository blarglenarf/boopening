#pragma once

#include "../RenderObject/Shader.h"

#include <string>

namespace Rendering
{
	class Shader;
	class StorageBuffer;
	class Texture2D;
	class RenderBuffer;
	class FrameBuffer;
	class Profiler;

	class ClusterMask
	{
	private:
		ClusterMask(const ClusterMask &mask);
		ClusterMask operator = (const ClusterMask &mask);

	private:
		Shader zeroMaskShader;
		Shader zeroMaskDepthShader;
		Shader createMaskShader;
		Shader depthPrePassShader;
		Shader depthPrePassMaskShader;

		Shader countIndicesShader;
		Shader prefixIndicesShader;
		Shader compactIndicesShader;

		int width;
		int height;

		int prePassWidth;
		int prePassHeight;

		int clusters;
		int gridCellSize;
		float maxDepth;

		int maskSize;
		int clusterBlockSize;

		bool prePassFlag;
		bool saveIndexFlag;

		Texture2D *depthTexture;
		RenderBuffer *depthBuffer;
		FrameBuffer *depthFbo;

		StorageBuffer *masks;
		StorageBuffer *maskDepths;

		StorageBuffer *indexBuffer;
		StorageBuffer *offsetsBuffer;
		StorageBuffer *compactIndexBuffer;

		unsigned int prefixSumSize;
		unsigned int compactIndexSize;

		Profiler *profiler;

	public:
		ClusterMask(int clusters = 32, float maxDepth = 30.0, int gridCellSize = 1);
		~ClusterMask();

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }

		void regen(int clusters = 32, float maxDepth = 30.0, int gridCellSize = 1, bool regenShader = true);
		void resize(int width, int height);
		void useDepthPrePass(bool prePassFlag) { this->prePassFlag = prePassFlag; }
		void saveClusterIndices(bool saveIndexFlag) { this->saveIndexFlag = saveIndexFlag; }

		void setUniforms(Shader *shader, const std::string &name = "ClusterMasks");

		void zero();

		void compactClusterIndices();
		void prefixClusterIndices();

		void beginDepthPrePass();
		void endDepthPrePass();

		void beginCreateMask();
		void endCreateMask();

		void createMaskFromDepths(Texture2D *depths);

		int getClusters() const { return clusters; }
		int getGridCellSize() const { return gridCellSize; }
		float getMaxDepth() const { return maxDepth; }
		int getMaskSize() const { return maskSize; }
		unsigned int getCompactIndexSize() const { return compactIndexSize; }

		Rendering::Shader &getMaskShader() { return createMaskShader; }
		Rendering::Shader &getPrePassMaskShader() { return depthPrePassMaskShader; }

		std::string getStr();

		StorageBuffer *getMasks() { return masks; }
		StorageBuffer *getMaskDepths() { return maskDepths; }
		StorageBuffer *getCompactIndexBuffer() { return compactIndexBuffer; }
		Texture2D *getDepthTexture() { return depthTexture; }
	};
}

