#include "LFB.h"

#include "LFBBase.h"
#include "LFBList.h"
#include "LFBLinear.h"
#include "LFBCoalesced.h"
#include "../RenderObject/Shader.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Renderer.h"

#include <cassert>

using namespace Rendering;

LFB::LFB() : type(NONE), lfb(nullptr), visualiseShader(nullptr)
{
	visualiseShader = new Shader();
}

LFB::~LFB()
{
	release();

	delete visualiseShader;
	visualiseShader = nullptr;
}

void LFB::release()
{
	delete lfb;
	lfb = nullptr;
}

void LFB::overrideDefault()
{
	lfb->overrideDefault();
}

void LFB::setProfiler(Profiler *profiler)
{
	lfb->setProfiler(profiler);
}

void LFB::setBmaUniforms(Shader *shader)
{
	lfb->setBmaUniforms(shader);
}

void LFB::setUniforms(Shader *shader, bool fragAlloc, std::string nameOverride)
{
	lfb->setUniforms(shader, fragAlloc, nameOverride);
}

void LFB::setCountUniforms(Shader *shader)
{
	lfb->setCountUniforms(shader);
}

void LFB::resize(int width, int height)
{
	lfb->resize(width, height);
}

Shader &LFB::beginCountFrags(int indexByTile)
{
	return lfb->beginCountFrags(indexByTile);
}

void LFB::endCountFrags()
{
	lfb->endCountFrags();
}

void LFB::beginCapture()
{
	lfb->beginCapture();
}

bool LFB::endCapture()
{
	return lfb->endCapture();
}

void LFB::beginComposite()
{
	lfb->beginComposite();
}

void LFB::endComposite()
{
	lfb->endComposite();
}

void LFB::composite(Shader *shader)
{
	lfb->beginComposite();
	lfb->setUniforms(shader);
	Renderer::instance().drawQuad();
	lfb->endComposite();
}

void LFB::drawData(const Math::mat4 &origInvMvMatrix, const Math::mat4 &origInvPMatrix, const Math::ivec4 &origViewport, const Math::mat4 &mvMatrix, const std::string &lfbName)
{
	if (!visualiseShader)
		visualiseShader = new Shader();
	if (!visualiseShader->isGenerated())
	{
		auto vert = ShaderSourceCache::getShader("dataVert_L").loadFromFile("shaders/lfb/visualise.vert");
		auto geom = ShaderSourceCache::getShader("dataGeom_L").loadFromFile("shaders/lfb/visualise.geom");
		auto frag = ShaderSourceCache::getShader("dataFrag_L").loadFromFile("shaders/lfb/visualise.frag");
		geom.replace("LFB_NAME", lfbName);
		visualiseShader->create(&vert, &frag, &geom);
	}

	auto &camera = Renderer::instance().getActiveCamera();
	visualiseShader->bind();
	beginComposite();
	setUniforms(visualiseShader);
	visualiseShader->setUniform("origInvMvMatrix", origInvMvMatrix);
	visualiseShader->setUniform("origInvPMatrix", origInvPMatrix);
	visualiseShader->setUniform("origViewport", origViewport);
	visualiseShader->setUniform("mvMatrix", mvMatrix * camera.getInverse());
	visualiseShader->setUniform("pMatrix", camera.getProjection());
	glDrawArrays(GL_POINTS, 0, camera.getWidth() * camera.getHeight());
	endComposite();
	visualiseShader->unbind();
}

std::string LFB::getStr(bool ignoreEmpty)
{
	return lfb->getStr(ignoreEmpty);
}

std::string LFB::getStrSorted(bool ignoreEmpty)
{
	return lfb->getStrSorted(ignoreEmpty);
}

std::string LFB::getBinary()
{
	return lfb->getBinary();
}

std::string LFB::getSVG()
{
	return lfb->getSVG();
}

size_t LFB::getFragSize() const
{
	return lfb->getFragSize();
}

void LFB::setMaxFrags(int maxFrags)
{
	lfb->setMaxFrags(maxFrags);
}

int LFB::getMaxFrags() const
{
	return lfb->getMaxFrags();
}

void LFB::setType(LFBType type, const std::string &name, LFB::DataType dataType, bool overrideDefault)
{
	if (lfb != nullptr && type == this->type && lfb->getName() == name && lfb->getDataType() == dataType)
		return;

	delete lfb;
	lfb = nullptr;
	this->type = type;

	switch (type)
	{
	case LINK_LIST:
		lfb = new LFBList(name, dataType, overrideDefault);
		break;

	case LINEARIZED:
		lfb = new LFBLinear(name, dataType, overrideDefault);
		break;

	case COALESCED:
		lfb = new LFBCoalesced(name, dataType, overrideDefault);
		break;

	default:
		assert(false);
		break;
	}
}

void LFB::setDataType(DataType type)
{
	lfb->setDataType(type);
}

void LFB::setName(const std::string &name)
{
	lfb->setName(name);
}

const std::string &LFB::getName() const
{
	return lfb->getName();
}

unsigned int LFB::getFragAlloc() const
{
	return lfb->getFragAlloc();
}

unsigned int LFB::getWidth() const
{
	return lfb->getWidth();
}

unsigned int LFB::getHeight() const
{
	return lfb->getHeight();
}

size_t LFB::getFragSize(LFB::DataType type)
{
	switch (type)
	{
	case UINT:
	case INT:
	case FLOAT:
		return 1;
		break;
	case VEC2:
	case IVEC2:
	case UVEC2:
		return 2;
		break;
	case VEC4:
	case IVEC4:
	case UVEC4:
		return 4;
		break;
	default:
		break;
	}
	return 1;
}

