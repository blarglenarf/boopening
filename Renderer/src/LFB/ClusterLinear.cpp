#include "ClusterLinear.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Shader.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"

#include "../../../Math/src/MathCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <cassert>
#include <iostream>

using namespace Rendering;


template <typename T>
static std::string getStrTempl(StorageBuffer *clusterOffsets, StorageBuffer *clusterData, unsigned int clusters, unsigned int width, unsigned int height)
{
	std::stringstream s;

	// TODO: test me!
	unsigned int *offsetData = (unsigned int*) clusterOffsets->read();
	T *fragData = (T*) clusterData->read();

	int maxFrags = 0;

	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			int currMaxFrags = 0;

			s << x << "," << y << ": ";

			std::vector<std::pair<unsigned int, T>> clusterVector;

			for (unsigned int cluster = 0; cluster < clusters; cluster++)
			{
				// Convert the pixel into cluster space.
				int fragIndex = x * height * clusters + y * clusters + cluster;
				unsigned int node = fragIndex > 0 ? offsetData[fragIndex - 1] : 0;
				//unsigned int count = offsetData[fragIndex] - (fragIndex > 0 ? offsetData[fragIndex - 1] : 0);

				while (node < offsetData[fragIndex])
				{
					T t = fragData[node];
					clusterVector.push_back({cluster, t});
					//s << cluster << ", " << id << "; ";

					node++;
					currMaxFrags++;
				}
			}

			if (currMaxFrags > maxFrags)
				maxFrags = currMaxFrags;

			for (auto &f : clusterVector)
				s << f.first << "," << f.second << "; ";

			s << "\n";
		}
	}
	s << "\n\n\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + s.str();
}

template <typename T>
static std::string getStrSortedTempl(StorageBuffer *clusterOffsets, StorageBuffer *clusterData, unsigned int clusters, unsigned int width, unsigned int height)
{
	std::stringstream s;

	// TODO: test me!
	unsigned int *offsetData = (unsigned int*) clusterOffsets->read();
	T *fragData = (T*) clusterData->read();

	int maxFrags = 0;

	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			int currMaxFrags = 0;

			s << x << "," << y << ": ";

			std::vector<std::pair<unsigned int, T>> clusterVector;

			for (unsigned int cluster = 0; cluster < clusters; cluster++)
			{
				// Convert the pixel into cluster space.
				int fragIndex = x * height * clusters + y * clusters + cluster;
				unsigned int node = fragIndex > 0 ? offsetData[fragIndex - 1] : 0;
				//unsigned int count = offsetData[fragIndex] - (fragIndex > 0 ? offsetData[fragIndex - 1] : 0);

				while (node < offsetData[fragIndex])
				{
					T t = fragData[node];
					clusterVector.push_back({cluster, t});
					//s << cluster << ", " << id << "; ";

					node++;
					currMaxFrags++;
				}
			}

			if (currMaxFrags > maxFrags)
				maxFrags = currMaxFrags;

			std::sort(clusterVector.begin(), clusterVector.end(), [](const std::pair<unsigned int, T> &v, const std::pair<unsigned int, T> &u)
			{
				if (u.second == v.second)
					return u.first < v.first;
				return u.second < v.second;
			});
			for (auto &f : clusterVector)
				s << f.first << "," << f.second << "; ";

			s << "\n";
		}
	}
	s << "\n\n\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + s.str();
}


static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer Offsets\
	{\
		uint offsets[];\
	};\
	\
	void main()\
	{\
		offsets[gl_VertexID] = 0;\
	}\
	";
}

static void createLfbSrc(const std::string &name, unsigned int clusters, int useAtomics)
{
	auto &src = ShaderSourceCache::getShader(name).loadFromFile("shaders/lfb/clusterL.glsl");
	src.setDefine("CLUSTERS", Utils::toString(clusters));
	src.setDefine("ATOMIC_ADD", Utils::toString(useAtomics));
}



ClusterLinear::ClusterLinear(const std::string &name, unsigned int clusters, bool storeDataFlag, Cluster::DataType dataType, bool overrideDefault, int maxFrags, int useAtomics) : ClusterBase(name, clusters, storeDataFlag, dataType, maxFrags), prefixSumSize(0)
{
	clusterOffsets = new StorageBuffer();
	clusterPos = new StorageBuffer();
	clusterDir = new StorageBuffer();
	clusterData = new StorageBuffer();

	std::string lName = "cluster_L";

	if (!ShaderSourceCache::exists(lName))
		createLfbSrc(lName, clusters, useAtomics);

	//if (!ShaderSource::exists("lfb"))
	if (overrideDefault)
		createLfbSrc("cluster", clusters, useAtomics);
}

ClusterLinear::~ClusterLinear()
{
	delete clusterOffsets;
	delete clusterPos;
	delete clusterDir;
	delete clusterData;

	clusterOffsets = nullptr;
	clusterPos = nullptr;
	clusterDir = nullptr;
	clusterData = nullptr;

	zeroClusterShader.release();
	prefixClusterShader.release();
}

void ClusterLinear::setBmaUniforms(Shader *shader)
{
	//shader->setUniform("ClusterPixelCounts" + name, clusterPixelCounts);
	//shader->setUniform("clusterSize" + name, Math::ivec2(width, height));
	setUniforms(shader);
}

void ClusterLinear::setUniforms(Shader *shader, bool fragAlloc)
{
	(void) (fragAlloc);

	shader->setUniform("ClusterOffsets" + name, clusterOffsets);
	if (storeDataFlag)
	{
		shader->setUniform("ClusterPos" + name, clusterPos);
		shader->setUniform("ClusterDir" + name, clusterDir);
	}
	shader->setUniform("ClusterData" + name, clusterData);
	shader->setUniform("clusterSize" + name, Math::ivec2(width, height));
}

void ClusterLinear::setCountUniforms(Shader *shader)
{
	shader->setUniform("Offsets", clusterOffsets);
	shader->setUniform("size", Math::ivec2(width, height));
}

void ClusterLinear::resize(int width, int height)
{
	if ((int) this->width == width && (int) this->height == height)
		return;

	this->width = width;
	this->height = height;

	// Parallel prefix algorithm requires powers of 2, as well as an extra int.
	prefixSumSize = Math::nextPower2(width * height * clusters + 1);

	// We also need one more int to store the final count.
	clusterOffsets->create(nullptr, (prefixSumSize + 1) * sizeof(unsigned int));

	// If we're using a profiler, set count buffer size.
	if (profiler)
	{
		profiler->setBufferSize("clusterCounts", (prefixSumSize + 1) * sizeof(unsigned int));
	}
}

void ClusterLinear::zeroBuffers()
{
	if (!zeroClusterShader.isGenerated())
	{
		auto zeroSrc = ShaderSource("zeroCluster_L", getZeroShaderSrc());
		zeroClusterShader.create(&zeroSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Now one thread per pixel per cluster (eg 256 per pixel).
	glEnable(GL_RASTERIZER_DISCARD);
	zeroClusterShader.bind();
	zeroClusterShader.setUniform("Offsets", clusterOffsets);
	glDrawArrays(GL_POINTS, 0, prefixSumSize + 1);
	zeroClusterShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void ClusterLinear::prefixSums()
{
	// Now do the prefix sums and get the final id count.
	if (!prefixClusterShader.isGenerated())
	{
		auto prefixSrc = ShaderSourceCache::getShader("prefixCluster").loadFromFile("shaders/lfb/prefix.vert");
		prefixSrc.setDefine("STORAGE_BUFFERS", Utils::toString(1));
		prefixClusterShader.create(&prefixSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	unsigned int sumSize = prefixSumSize;
	assert(sumSize > 0);

	unsigned int positionOfTotal = sumSize - 1;

	glEnable(GL_RASTERIZER_DISCARD);

	prefixClusterShader.bind();
	prefixClusterShader.setUniform("Offsets", clusterOffsets);

	prefixClusterShader.setUniform("pass", 0);
	for (unsigned int step = 2; step <= sumSize; step <<= 1)
	{
		prefixClusterShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	// For saving the total sum of frags.
	TextureBuffer justOneInt(GL_R32UI);
	justOneInt.create(nullptr, sizeof(unsigned int));
	clusterOffsets->copy(&justOneInt, positionOfTotal * sizeof(unsigned int), 0, sizeof(unsigned int));

	// Save total sum of frags at the last location (past all the prefix sums).
	clusterOffsets->copy(clusterOffsets, positionOfTotal * sizeof(unsigned int), (positionOfTotal + 1) * sizeof(unsigned int), sizeof(unsigned int));

	// Value at positionOfTotal must be zero for next pass to correctly calculate prefix sums.
	unsigned int zero = 0;
	clusterOffsets->bufferSubData(&zero, positionOfTotal * sizeof(unsigned int), sizeof(unsigned int));

	prefixClusterShader.setUniform("pass", 1);
	for (unsigned int step = sumSize; step >= 2; step >>= 1)
	{
		prefixClusterShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	prefixClusterShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	auto prefixSumsTotal = *((unsigned int*) justOneInt.read());

	// Only realloc if the size is different.
	if (prefixSumsTotal != clusterFragAlloc)
	{
		clusterFragAlloc = prefixSumsTotal;
		size_t totalSize = 0;
		if (storeDataFlag)
		{
			clusterPos->create(nullptr, clusterFragAlloc * sizeof(float) * 4);
			clusterDir->create(nullptr, clusterFragAlloc * sizeof(float) * 4);
			totalSize += clusterFragAlloc * sizeof(float) * 8;
		}
		clusterData->create(nullptr, clusterFragAlloc * sizeof(float) * getFragSize());
		totalSize += clusterFragAlloc * sizeof(float) * getFragSize();

		if (profiler)
		{
			profiler->setBufferSize("clusterData", totalSize);
			profiler->setBufferSize("clusterTotalData", totalSize + (prefixSumSize + 1) * sizeof(unsigned int));
		}
	}
}

void ClusterLinear::beginCountFrags()
{
	zeroBuffers();
}

void ClusterLinear::endCountFrags()
{
	// Not much to do here either.
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	prefixSums();
}

void ClusterLinear::beginCapture()
{
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

bool ClusterLinear::endCapture()
{
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	return false;
}

void ClusterLinear::beginComposite()
{
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void ClusterLinear::endComposite()
{
	// Possibly not necessary, depending on what's done during composite.
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

std::string ClusterLinear::getStr()
{
	switch (dataType)
	{
	case Cluster::UINT:
		return getStrTempl<unsigned int>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::INT:
		return getStrTempl<int>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::FLOAT:
		return getStrTempl<float>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::VEC2:
		return getStrTempl<Math::vec2>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::IVEC2:
		return getStrTempl<Math::ivec2>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::UVEC2:
		return getStrTempl<Math::uvec2>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::VEC4:
		return getStrTempl<Math::vec4>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::IVEC4:
		return getStrTempl<Math::ivec4>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::UVEC4:
		return getStrTempl<Math::uvec4>(clusterOffsets, clusterData, clusters, width, height);
		break;
	default:
		break;
	}

	return "";
}

std::string ClusterLinear::getStrSorted()
{
	switch (dataType)
	{
	case Cluster::UINT:
		return getStrSortedTempl<unsigned int>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::INT:
		return getStrSortedTempl<int>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::FLOAT:
		return getStrSortedTempl<float>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::VEC2:
		return getStrSortedTempl<Math::vec2>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::IVEC2:
		return getStrSortedTempl<Math::ivec2>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::UVEC2:
		return getStrSortedTempl<Math::uvec2>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::VEC4:
		return getStrSortedTempl<Math::vec4>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::IVEC4:
		return getStrSortedTempl<Math::ivec4>(clusterOffsets, clusterData, clusters, width, height);
		break;
	case Cluster::UVEC4:
		return getStrSortedTempl<Math::uvec4>(clusterOffsets, clusterData, clusters, width, height);
		break;
	default:
		break;
	}

	return "";
}

