#pragma once

#include "ClusterBase.h"

#include "../RenderObject/Shader.h"

#include <string>

namespace Rendering
{
	class StorageBuffer;

	class ClusterCoalesced : public ClusterBase
	{
	private:
		ClusterCoalesced(const ClusterCoalesced &cluster);
		ClusterCoalesced operator = (const ClusterCoalesced &cluster);

	private:
		// We assume the counts are being calculated someplace else.
		Shader zeroClusterShader;
		Shader prefixClusterShader;
		Shader minClusterShader;

		unsigned int prefixSumSize;

		StorageBuffer *clusterOffsets;
		StorageBuffer *clusterCounts;
		StorageBuffer *clusterMinCounts;
		StorageBuffer *clusterPos;
		StorageBuffer *clusterDir;
		StorageBuffer *clusterData;

	public:
		ClusterCoalesced(const std::string &name = "cluster", unsigned int clusters = 32, bool storeDataFlag = false, Cluster::DataType dataType = Cluster::UINT, bool overrideDefault = true, int maxFrags = 64, int useAtomics = 0);
		virtual ~ClusterCoalesced();

		virtual void setBmaUniforms(Shader *shader) override;
		virtual void setUniforms(Shader *shader, bool fragAlloc = true) override;
		virtual void setCountUniforms(Shader *shader) override;

		virtual void resize(int width, int height) override;

		void zeroBuffers();
		void prefixSums();
		void calcMinCounts();

		// Requires a storage buffer called Offsets.
		virtual void beginCountFrags() override;
		virtual void endCountFrags() override;

		virtual void beginCapture() override;
		virtual bool endCapture() override;

		virtual void beginComposite() override;
		virtual void endComposite() override;

		virtual std::string getStr() override;
		virtual std::string getStrSorted() override;

		StorageBuffer *getClusterOffsets() { return clusterOffsets; }
		StorageBuffer *getClusterCounts() { return clusterCounts; }
		StorageBuffer *getClusterMinCounts() { return clusterMinCounts; }
		StorageBuffer *getClusterData() { return clusterData; }
	};
}

