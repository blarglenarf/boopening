#include "LFBLinear.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Shader.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"
#include "../Renderer.h"

#include "../../../Math/src/MathCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <iostream>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <cassert>

using namespace Rendering;


template <typename T>
static std::string getStrTempl(StorageBuffer *clusterOffsets, StorageBuffer *clusterData, unsigned int width, unsigned int height, bool ignoreEmpty)
{
	std::stringstream s;

	unsigned int *offsetData = (unsigned int*) clusterOffsets->read();
	T *fragData = (T*) clusterData->read();

	//s << "\n";

	//for (unsigned int i = 0; i < width * height + 1; i++)
	//	s << offsetData[i] << ", ";
	s << "\n";

	unsigned int maxFrags = 0;
	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			unsigned int pixel = y * width + x;
			unsigned int node = pixel > 0 ? offsetData[pixel - 1] : 0;
			unsigned int count = offsetData[pixel] - (pixel > 0 ? offsetData[pixel - 1] : 0);
			if (ignoreEmpty && count == 0)
				continue;

			s << count << ": " << x << "," << y << ": ";

			while (node < offsetData[pixel])
			{
				s << fragData[node] << "; ";
				node++;
			}
			maxFrags = std::max(maxFrags, count);

			s << "\n";
		}
	}
	s << "\n\n\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + s.str();
}

template <typename T>
static std::string getStrSortedTempl(StorageBuffer *clusterOffsets, StorageBuffer *clusterData, unsigned int width, unsigned int height, bool ignoreEmpty)
{
	std::stringstream s;

	size_t maxFrags = 0;

	unsigned int *offsetData = (unsigned int*) clusterOffsets->read();
	T *fragData = (T*) clusterData->read();

	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			unsigned int pixel = y * width + x;
			unsigned int node = pixel > 0 ? offsetData[pixel - 1] : 0;
			unsigned int count = offsetData[pixel] - (pixel > 0 ? offsetData[pixel - 1] : 0);
			if (ignoreEmpty && count == 0)
				continue;

			s << count << ": " << x << "," << y << ": ";

			// Once upon a time I needed to print light ids...
			std::vector<T> frags;

			while (node < offsetData[pixel])
			{
				//float depth = fragData2[node].y;
				frags.push_back(fragData[node]);
				node++;
			}

			std::sort(frags.begin(), frags.end(), [](const T &v, const T &u){ return u < v; });
			for (auto &f : frags)
				s << f << "; ";

			if (frags.size() > maxFrags)
				maxFrags = frags.size();

			if (count != frags.size())
				s << " NOOOOOOOOOOOOOOOOO!";

			s << "\n";
		}
	}
	s << "\n\n\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + s.str();
}


#if LINEAR_STORAGE_BUFFERS
	static std::string getZeroShaderSrc()
	{
		return "\
		#version 450\n\
		\
		buffer Offsets\
		{\
			uint offsets[];\
		};\
		\
		void main()\
		{\
			offsets[gl_VertexID] = 0;\
		}\
		";
	}
#else
	static std::string getZeroImgShaderSrc()
	{
		return "\
		#version 450\n\
		\
		uniform layout(r32ui) uimageBuffer Offsets;\
		\
		void main()\
		{\
			imageStore(Offsets, gl_VertexID, uvec4(0));\
		}\
		";
	}
#endif

static void createLfbSrc(const std::string &name)
{
	auto &src = ShaderSourceCache::getShader(name).loadFromFile("shaders/lfb/lfbL.glsl");
	src.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
}



LFBLinear::LFBLinear(const std::string &name, LFB::DataType dataType, bool overrideDefault, int maxFrags) : LFBBase(name, dataType, maxFrags), offsets(nullptr), data(nullptr), prefixSumSize(0)
{
#if LINEAR_STORAGE_BUFFERS
	offsets = new StorageBuffer();
	data = new StorageBuffer();
#else
	offsets = new TextureBuffer(GL_R32UI);
	data = new TextureBuffer(GL_RG32F);
#endif

	std::string lName = "lfb_L";

	if (!ShaderSourceCache::exists(lName))
		createLfbSrc(lName);

	//if (!ShaderSource::exists("lfb"))

	if (overrideDefault)
		createLfbSrc("lfb");
}

LFBLinear::~LFBLinear()
{
	delete offsets;
	delete data;

	offsets = nullptr;
	data = nullptr;

	zeroShader.release();
	countShader.release();
	prefixShader.release();
}

void LFBLinear::overrideDefault()
{
	createLfbSrc("lfb");
}

void LFBLinear::setBmaUniforms(Shader *shader)
{
	setUniforms(shader);
}

void LFBLinear::setUniforms(Shader *shader, bool fragAlloc, std::string nameOverride)
{
	(void) (fragAlloc);
	if (nameOverride == "")
		nameOverride = name;

	shader->setUniform("Data" + nameOverride, data);
	shader->setUniform("Offsets" + nameOverride, offsets);
	shader->setUniform("size" + nameOverride, Math::ivec2(width, height));
}

void LFBLinear::setCountUniforms(Shader *shader)
{
	shader->setUniform("Offsets", offsets);
	shader->setUniform("size", Math::ivec2(width, height));
}

void LFBLinear::resize(int width, int height)
{
	if ((int) this->width == width && (int) this->height == height)
		return;

	this->width = width;
	this->height = height;

	// Parallel prefix algorithm requires powers of 2, as well as an extra int.
	prefixSumSize = Math::nextPower2(width * height + 1);

	// We also need one more int to store the final count.
	offsets->create(nullptr, (prefixSumSize + 1) * sizeof(unsigned int));

	// If we're using a profiler, set count buffer size.
	if (profiler)
		profiler->setBufferSize(name + "Counts", (prefixSumSize + 1) * sizeof(unsigned int));
}

void LFBLinear::zeroBuffers()
{
	if (!zeroShader.isGenerated())
	{
#if LINEAR_STORAGE_BUFFERS
		auto zeroSrc = ShaderSource("zeroLFB_L", getZeroShaderSrc());
#else
		auto zeroSrc = ShaderSource("zeroLFB_L", getZeroImgShaderSrc());
#endif
		zeroSrc.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
		zeroShader.create(&zeroSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("Offsets", offsets);
	glDrawArrays(GL_POINTS, 0, prefixSumSize + 1);
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
}

void LFBLinear::prefixSums()
{
	if (!prefixShader.isGenerated())
	{
		auto prefixSrc = ShaderSourceCache::getShader("prefixLFB_L").loadFromFile("shaders/lfb/prefix.vert");
		prefixSrc.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
		prefixShader.create(&prefixSrc);
	}

	unsigned int sumSize = prefixSumSize;
	assert(sumSize > 0);

	unsigned int positionOfTotal = sumSize - 1;

	glEnable(GL_RASTERIZER_DISCARD);

	prefixShader.bind();
	prefixShader.setUniform("Offsets", offsets);

	prefixShader.setUniform("pass", 0);
	for (unsigned int step = 2; step <= sumSize; step <<= 1)
	{
		prefixShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	}

#if !LINEAR_STORAGE_BUFFERS
	offsets->unbind();
#endif

	// For saving the total sum of frags.
	TextureBuffer justOneInt(GL_R32UI);
	justOneInt.create(nullptr, sizeof(unsigned int));
	offsets->copy(&justOneInt, positionOfTotal * sizeof(unsigned int), 0, sizeof(unsigned int));

	// Save total sum of frags at the last location (past all the prefix sums).
	offsets->copy(offsets, positionOfTotal * sizeof(unsigned int), (positionOfTotal + 1) * sizeof(unsigned int), sizeof(unsigned int));

	// Value at positionOfTotal must be zero for next pass to correctly calculate prefix sums.
	unsigned int zero = 0;
	offsets->bufferSubData(&zero, positionOfTotal * sizeof(unsigned int), sizeof(unsigned int));

#if !LINEAR_STORAGE_BUFFERS
	offsets->bind();
#endif

	prefixShader.setUniform("pass", 1);
	for (unsigned int step = sumSize; step >= 2; step >>= 1)
	{
		prefixShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	}

#if !LINEAR_STORAGE_BUFFERS
	offsets->unbind();
#endif

	prefixShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	auto prefixSumsTotal = *((unsigned int*) justOneInt.read());

	// Only realloc if the size is different.
	if (prefixSumsTotal != fragAlloc)
	{
		fragAlloc = prefixSumsTotal;

		size_t dataSize = fragAlloc * sizeof(float) * getFragSize();
		data->create(nullptr, dataSize);

		if (profiler)
		{
			size_t countSize = (prefixSumSize + 1) * sizeof(unsigned int);
			profiler->setBufferSize(name + "Data", dataSize);
			profiler->setBufferSize(name + "Total", dataSize + countSize);
		}
	}
}

void LFBLinear::prefixSums(StorageBuffer *counts)
{
	// Probably easiest to just copy the counts to the offsets, then calculate the prefix sums.

	// This step is usually quite fast, not much need to profile it.
	//if (profiler)
	//	profiler->start("Count Copy");
	zeroBuffers();
	counts->copy(offsets);
	//if (profiler)
	//	profiler->time("Count Copy");

	if (profiler)
		profiler->start("Prefix Sums");
	prefixSums();
	if (profiler)
		profiler->time("Prefix Sums");
}

void LFBLinear::incrementOffsets()
{
	if (!offsetIncShader.isGenerated())
	{
		auto offsetIncSrc = ShaderSourceCache::getShader("offsetIncLFB_L").loadFromFile("shaders/lfb/offsetInc.vert");
		//prefixSrc.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
		offsetIncShader.create(&offsetIncSrc);
	}

	// Offsets are incremented during capture, but that won't have happened if you captured a linked list and prefixed.
	if (profiler)
		profiler->start("Offset inc");

	glEnable(GL_RASTERIZER_DISCARD);

	offsetIncShader.bind();

	offsetIncShader.setUniform("Offsets", offsets);

	glDrawArrays(GL_POINTS, 0, width * height);
#if 0
	prefixShader.setUniform("pass", 0);
	for (unsigned int step = 2; step <= sumSize; step <<= 1)
	{
		prefixShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	}
#endif
	offsetIncShader.unbind();

	glDisable(GL_RASTERIZER_DISCARD);

	if (profiler)
		profiler->time("Offset inc");
}

Shader &LFBLinear::beginCountFrags(int indexByTile)
{
	if (!countShader.isGenerated())
	{
		auto countVertSrc = ShaderSourceCache::getShader("countLFBVert_L").loadFromFile("shaders/lfb/counts.vert");
		auto countFragSrc = ShaderSourceCache::getShader("countLFBFrag_L").loadFromFile("shaders/lfb/counts.frag");
		countFragSrc.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
		countFragSrc.setDefine("INDEX_BY_TILE", Utils::toString(indexByTile));
		countShader.create(&countVertSrc, &countFragSrc);
	}

#if !LINEAR_STORAGE_BUFFERS
	offsets->bind();
#endif

	if (profiler)
		profiler->start("Counts");

	zeroBuffers();

	return countShader;
}

void LFBLinear::endCountFrags()
{
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	if (profiler)
		profiler->time("Counts");

	if (profiler)
		profiler->start("Prefix Sums");

	prefixSums();

	if (profiler)
		profiler->time("Prefix Sums");
}

void LFBLinear::beginCapture()
{
#if !LINEAR_STORAGE_BUFFERS
	offsets->bind();
	data->bind();
#endif
}

bool LFBLinear::endCapture()
{
#if !LINEAR_STORAGE_BUFFERS
	offsets->unbind();
	data->unbind();
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	return false;
}

void LFBLinear::beginComposite()
{
	// Here in case we wrote to global memory during a previous sort.
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

#if !LINEAR_STORAGE_BUFFERS
	offsets->bind();
	data->bind();
#endif
}

void LFBLinear::endComposite()
{
#if !LINEAR_STORAGE_BUFFERS
	offsets->unbind();
	data->unbind();
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
}

std::string LFBLinear::getStr(bool ignoreEmpty)
{
	switch (dataType)
	{
	case LFB::UINT:
		return getStrTempl<unsigned int>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::INT:
		return getStrTempl<int>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::FLOAT:
		return getStrTempl<float>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC2:
		return getStrTempl<Math::vec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC2:
		return getStrTempl<Math::ivec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC2:
		return getStrTempl<Math::uvec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC4:
		return getStrTempl<Math::vec4>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC4:
		return getStrTempl<Math::ivec4>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC4:
		return getStrTempl<Math::uvec4>(offsets, data, width, height, ignoreEmpty);
		break;
	default:
		break;
	}

	return "";
}

std::string LFBLinear::getStrSorted(bool ignoreEmpty)
{
	switch (dataType)
	{
	case LFB::UINT:
		return getStrSortedTempl<unsigned int>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::INT:
		return getStrSortedTempl<int>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::FLOAT:
		return getStrSortedTempl<float>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC2:
		return getStrSortedTempl<Math::vec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC2:
		return getStrSortedTempl<Math::ivec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC2:
		return getStrSortedTempl<Math::uvec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC4:
		return getStrSortedTempl<Math::vec4>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC4:
		return getStrSortedTempl<Math::ivec4>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC4:
		return getStrSortedTempl<Math::uvec4>(offsets, data, width, height, ignoreEmpty);
		break;
	default:
		break;
	}

	return "";
}

std::string LFBLinear::getBinary()
{
	return "";
}

std::string LFBLinear::getSVG()
{
	// TODO: Implement me!
	return "";
}

