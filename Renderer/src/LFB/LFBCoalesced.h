#pragma once

#include "LFBBase.h"
#include "../RenderObject/Shader.h"

#include <string>

#define LINEAR_STORAGE_BUFFERS 1

namespace Rendering
{
	class StorageBuffer;
	class TextureBuffer;
	class AtomicBuffer;

	class LFBCoalesced : public LFBBase
	{
	private:
		LFBCoalesced(const LFBCoalesced &list);
		LFBCoalesced operator = (const LFBCoalesced &list);

	private:
		Shader zeroShader;
		Shader countShader;
		Shader prefixShader;
		Shader minShader;

	#if LINEAR_STORAGE_BUFFERS
		StorageBuffer *offsets;
		StorageBuffer *counts;
		StorageBuffer *minCounts;
		StorageBuffer *data;
	#else
		TextureBuffer *offsets;
		TextureBuffer *counts;
		TextureBuffer *minCounts;
		TextureBuffer *data;
	#endif

		unsigned int prefixSumSize;

	public:
		LFBCoalesced(const std::string &name = "lfb", LFB::DataType dataType = LFB::VEC2, bool overrideDefault = true, int maxFrags = 64);
		virtual ~LFBCoalesced();

		virtual void overrideDefault() override;

		virtual void setBmaUniforms(Shader *shader) override;
		virtual void setUniforms(Shader *shader, bool fragAlloc = true, std::string nameOverride = "") override;
		virtual void setCountUniforms(Shader *shader) override;

		virtual void resize(int width, int height) override;

		void zeroBuffers();
		void prefixSums();
		void calcMinCounts();

		virtual Shader &beginCountFrags(int indexByTile = 0) override;
		virtual void endCountFrags() override;

		virtual void beginCapture() override;
		virtual bool endCapture() override;
		
		virtual void beginComposite() override;
		virtual void endComposite() override;

		virtual std::string getStr(bool ignoreEmpty = false) override;
		virtual std::string getStrSorted(bool ignoreEmpty = false) override;

		virtual std::string getBinary() override;
		virtual std::string getSVG() override;

	#if LINEAR_STORAGE_BUFFERS
		StorageBuffer *getOffsets() { return offsets; }
		StorageBuffer *getMinCounts() { return minCounts; }
		StorageBuffer *getData() { return data; }
	#else
		TextureBuffer *getOffsets() { return offsets; }
		TextureBuffer *getMinCounts() { return minCounts; }
		TextureBuffer *getData() { return data; }
	#endif
	};
}

