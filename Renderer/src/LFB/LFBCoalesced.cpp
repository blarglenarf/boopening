#include "LFBCoalesced.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Shader.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"

#include "../../../Math/src/MathCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <iostream>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <cassert>

using namespace Rendering;

#define GROUP_SIZE 32
#define USE_BLOCKS 1
#define BLOCK_SIZE 4


// TODO: Basic Idea:
// Pass 1: Get per-pixel counts as normal.
// Pass 2: Figure out the min count for each warp (threads are nPixels / 32 I guess?).
// Pass 3: Calculate per-pixel offsets as normal and allocate the data as normal.
// Pass 4: Proceed with capturing the lfb as normal.
// TODO: Fix bugs.
// TODO: Figure out a way of avoiding the need for complete per-pixel offsets.
// TODO: Don't hard-code the group size.


template <typename T>
static std::string getStrTempl(StorageBuffer *clusterOffsets, StorageBuffer *clusterData, unsigned int width, unsigned int height, bool ignoreEmpty)
{
	(void) (ignoreEmpty);
	std::stringstream s;

	unsigned int *offsetData = (unsigned int*) clusterOffsets->read();
	T *fragData = (T*) clusterData->read();

	s << "\n";

	for (unsigned int i = 0; i < width * height + 1; i++)
		s << offsetData[i] << ", ";
	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			unsigned int pixel = y * width + x;
			unsigned int node = pixel > 0 ? offsetData[pixel - 1] : 0;

			s << x << "," << y << ": ";

			while (node < offsetData[pixel])
			{
				s << fragData[node] << "; ";
				node++;
			}

			s << "\n";
		}
	}
	s << "\n\n\n";

	return s.str();
}

template <typename T>
std::string getStrSortedTempl(StorageBuffer *clusterOffsets, StorageBuffer *clusterData, unsigned int width, unsigned int height, bool ignoreEmpty)
{
	(void) (ignoreEmpty);
	std::stringstream s;

	size_t maxFrags = 0;

	unsigned int *offsetData = (unsigned int*) clusterOffsets->read();
	T *fragData = (T*) clusterData->read();

	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			unsigned int pixel = y * width + x;
			unsigned int node = pixel > 0 ? offsetData[pixel - 1] : 0;
			unsigned int count = offsetData[pixel] - (pixel > 0 ? offsetData[pixel - 1] : 0);

			s << count << ": " << x << "," << y << ": ";

			// Once upon a time I needed to print light ids...
			std::vector<T> frags;

			while (node < offsetData[pixel])
			{
				//float depth = fragData2[node].y;
				frags.push_back(fragData[node]);
				node++;
			}

			std::sort(frags.begin(), frags.end(), [](const T &v, const T &u){ return u < v; });
			for (auto &f : frags)
				s << f << "; ";

			if (frags.size() > maxFrags)
				maxFrags = frags.size();

			if (count != frags.size())
				s << " NOOOOOOOOOOOOOOOOO!";

			s << "\n";
		}
	}
	s << "\n\n\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + s.str();
}


#if LINEAR_STORAGE_BUFFERS
	static std::string getZeroShaderSrc()
	{
		return "\
		#version 450\n\
		\
		buffer Offsets\
		{\
			uint offsets[];\
		};\
		\
		void main()\
		{\
			offsets[gl_VertexID] = 0;\
		}\
		";
	}

	#if USE_BLOCKS
		static std::string getMinShaderSrc()
		{
			return "\
			#version 450\n\
			#define BLOCK_SIZE " + Utils::toString(BLOCK_SIZE) + "\n\
			\
			buffer MinCounts\
			{\
				uint minCounts[];\
			};\
			\
			buffer Counts\
			{\
				uint counts[];\
			};\
			\
			uniform int groupSize;\
			\
			void main()\
			{\
				uint minCount = 1024;\
				for (int i = 0; i < groupSize; i++)\
				{\
					uint count = counts[gl_VertexID * groupSize + i];\
					uint rounded = count - (count % BLOCK_SIZE);\
					if (rounded < minCount)\
						minCount = rounded;\
				}\
				minCounts[gl_VertexID] = minCount;\
			}\
			";
		}
	#else
		static std::string getMinShaderSrc()
		{
			return "\
			#version 450\n\
			\
			buffer MinCounts\
			{\
				uint minCounts[];\
			};\
			\
			buffer Counts\
			{\
				uint counts[];\
			};\
			\
			uniform int groupSize;\
			\
			void main()\
			{\
				uint minCount = 1024;\
				for (int i = 0; i < groupSize; i++)\
				{\
					uint count = counts[gl_VertexID * groupSize + i];\
					if (count < minCount)\
						minCount = count;\
				}\
				minCounts[gl_VertexID] = minCount;\
			}\
			";
		}
	#endif
#else
	static std::string getZeroImgShaderSrc()
	{
		return "\
		#version 450\n\
		\
		uniform layout(r32ui) uimageBuffer Offsets;\
		\
		void main()\
		{\
			imageStore(Offsets, gl_VertexID, uvec4(0));\
		}\
		";
	}

	static std::string getMinImgShaderSrc()
	{
		return "\
		#version 450\n\
		\
		uniform layout(r32ui) uimageBuffer MinCounts;\
		uniform layout(r32ui) uimageBuffer Counts;\
		\
		uniform int groupSize;\
		\
		void main()\
		{\
			uint minCount = 1024;\
			for (int i = 0; i < groupSize; i++)\
			{\
				uint count = imageLoad(Counts, gl_VertexID * groupSize + i).x;\
				if (count < minCount)\
					minCount = count;\
			}\
			imageStore(MinCounts, gl_VertexID, uvec4(minCount, 0, 0, 0));\
		}\
		";
	}
#endif

static void createLfbSrc(const std::string &name)
{
	auto &src = ShaderSourceCache::getShader(name).loadFromFile("shaders/lfb/lfbCL.glsl");
	src.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
	src.setDefine("USE_BLOCKS", Utils::toString(USE_BLOCKS));
	src.setDefine("BLOCK_SIZE", Utils::toString(BLOCK_SIZE));
}



LFBCoalesced::LFBCoalesced(const std::string &name, LFB::DataType dataType, bool overrideDefault, int maxFrags) : LFBBase(name, dataType, maxFrags), offsets(nullptr), counts(nullptr), minCounts(nullptr), data(nullptr), prefixSumSize(0)
{
#if LINEAR_STORAGE_BUFFERS
	offsets = new StorageBuffer();
	counts = new StorageBuffer();
	minCounts = new StorageBuffer();
	data = new StorageBuffer();
#else
	offsets = new TextureBuffer(GL_R32UI);
	counts = new TextureBuffer(GL_R32UI);
	minCounts = new TextureBuffer(GL_R32UI);
	data = new TextureBuffer(GL_RG32F);
#endif

	std::string lName = "lfb_CL";

	if (!ShaderSourceCache::exists(lName))
		createLfbSrc(lName);

	//if (!ShaderSource::exists("lfb"))
	if (overrideDefault)
		createLfbSrc("lfb");
}

LFBCoalesced::~LFBCoalesced()
{
	delete offsets;
	delete counts;
	delete minCounts;
	delete data;

	offsets = nullptr;
	counts = nullptr;
	minCounts = nullptr;
	data = nullptr;

	zeroShader.release();
	countShader.release();
	prefixShader.release();
	minShader.release();
}

void LFBCoalesced::overrideDefault()
{
	createLfbSrc("lfb");
}

void LFBCoalesced::setBmaUniforms(Shader *shader)
{
	setUniforms(shader);
}

void LFBCoalesced::setUniforms(Shader *shader, bool fragAlloc, std::string nameOverride)
{
	(void) (fragAlloc);
	if (nameOverride == "")
		nameOverride = name;

	shader->setUniform("Data" + nameOverride, data);
	shader->setUniform("Offsets" + nameOverride, offsets);
	shader->setUniform("Counts" + nameOverride, counts);
	shader->setUniform("MinCounts" + nameOverride, minCounts);
	shader->setUniform("size" + nameOverride, Math::ivec2(width, height));
}

void LFBCoalesced::setCountUniforms(Shader *shader)
{
	shader->setUniform("Offsets", offsets);
	shader->setUniform("size", Math::ivec2(width, height));
}

void LFBCoalesced::resize(int width, int height)
{
	if ((int) this->width == width && (int) this->height == height)
		return;

	this->width = width;
	this->height = height;

	// Parallel prefix algorithm requires powers of 2, as well as an extra int.
	prefixSumSize = Math::nextPower2(width * height + 1);

	// We also need one more int to store the final count.
	size_t offsetSize = (prefixSumSize + 1) * sizeof(unsigned int);
	offsets->create(nullptr, offsetSize);

	size_t countSize = width * height * sizeof(unsigned int);
	counts->create(nullptr, countSize);

	// Allocate data for the minCounts.
	int groupSize = GROUP_SIZE;
	size_t minCountSize = ((width * height) / groupSize) * sizeof(unsigned int);
	minCounts->create(nullptr, minCountSize);

	// If we're using a profiler, set count buffer size.
	if (profiler)
	{
		profiler->setBufferSize(name + "Offsets", offsetSize);
		profiler->setBufferSize(name + "Counts", countSize);
		profiler->setBufferSize(name + "MinCounts", minCountSize);
		//profiler->setBufferSize(name + "Total", offsetSize + countSize + minCountSize);
	}
}

void LFBCoalesced::zeroBuffers()
{
	if (!zeroShader.isGenerated())
	{
#if LINEAR_STORAGE_BUFFERS
		auto zeroSrc = ShaderSource("zeroLFB_CL", getZeroShaderSrc());
#else
		auto zeroSrc = ShaderSource("zeroLFB_CL", getZeroImgShaderSrc());
#endif
		zeroSrc.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
		zeroShader.create(&zeroSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("Offsets", offsets);
	glDrawArrays(GL_POINTS, 0, prefixSumSize + 1);
	zeroShader.setUniform("Offsets", counts);
	glDrawArrays(GL_POINTS, 0, width * height);
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
}

void LFBCoalesced::prefixSums()
{
	if (!prefixShader.isGenerated())
	{
		auto prefixSrc = ShaderSourceCache::getShader("prefixLFB_CL").loadFromFile("shaders/lfb/prefix.vert");
		prefixSrc.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
		prefixShader.create(&prefixSrc);
	}

	unsigned int sumSize = prefixSumSize;
	assert(sumSize > 0);

	unsigned int positionOfTotal = sumSize - 1;

	glEnable(GL_RASTERIZER_DISCARD);

	prefixShader.bind();
	prefixShader.setUniform("Offsets", offsets);

	prefixShader.setUniform("pass", 0);
	for (unsigned int step = 2; step <= sumSize; step <<= 1)
	{
		prefixShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	}

#if !LINEAR_STORAGE_BUFFERS
	offsets->unbind();
#endif

	// For saving the total sum of frags.
	TextureBuffer justOneInt(GL_R32UI);
	justOneInt.create(nullptr, sizeof(unsigned int));
	offsets->copy(&justOneInt, positionOfTotal * sizeof(unsigned int), 0, sizeof(unsigned int));

	// Save total sum of frags at the last location (past all the prefix sums).
	offsets->copy(offsets, positionOfTotal * sizeof(unsigned int), (positionOfTotal + 1) * sizeof(unsigned int), sizeof(unsigned int));

	// Value at positionOfTotal must be zero for next pass to correctly calculate prefix sums.
	unsigned int zero = 0;
	offsets->bufferSubData(&zero, positionOfTotal * sizeof(unsigned int), sizeof(unsigned int));

#if !LINEAR_STORAGE_BUFFERS
	offsets->bind();
#endif

	prefixShader.setUniform("pass", 1);
	for (unsigned int step = sumSize; step >= 2; step >>= 1)
	{
		prefixShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	}

#if !LINEAR_STORAGE_BUFFERS
	offsets->unbind();
#endif

	prefixShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	auto prefixSumsTotal = *((unsigned int*) justOneInt.read());

	// Only realloc if the size is different.
	if (prefixSumsTotal != fragAlloc)
	{
		fragAlloc = prefixSumsTotal;
		size_t dataSize = fragAlloc * sizeof(float) * getFragSize();
		data->create(nullptr, dataSize);

		if (profiler)
		{
			size_t offsetSize = (prefixSumSize + 1) * sizeof(unsigned int);
			size_t countSize = width * height * sizeof(unsigned int);
			size_t minCountSize = ((width * height) / GROUP_SIZE) * sizeof(unsigned int);
			profiler->setBufferSize(name + "Data", dataSize);
			profiler->setBufferSize(name + "Total", offsetSize + countSize + minCountSize + dataSize);
		}
	}
}

void LFBCoalesced::calcMinCounts()
{
	if (!minShader.isGenerated())
	{
#if LINEAR_STORAGE_BUFFERS
		auto minSrc = ShaderSource("minLFB_CL", getMinShaderSrc());
#else
		auto minSrc = ShaderSource("minLFB_CL", getMinImgShaderSrc());
#endif
		minSrc.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
		minShader.create(&minSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	glEnable(GL_RASTERIZER_DISCARD);
	minShader.bind();

#if !LINEAR_STORAGE_BUFFERS
	minCounts->bind();
	offsets->bind();
#endif

	minShader.setUniform("MinCounts", minCounts);
	minShader.setUniform("Counts", offsets);
	minShader.setUniform("groupSize", GROUP_SIZE);

	glDrawArrays(GL_POINTS, 0, (width * height) / GROUP_SIZE);

#if !LINEAR_STORAGE_BUFFERS
	minCounts->unbind();
	offsets->unbind();
#endif

	minShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

#if 0
	auto *minData = (unsigned int*) minCounts->read();
	for (size_t i = 0; i < width * height / GROUP_SIZE; i++)
		std::cout << minData[i] << " ";
	std::cout << "\n";
	exit(0);
#endif
}

Shader &LFBCoalesced::beginCountFrags(int indexByTile)
{
	if (!countShader.isGenerated())
	{
		auto countVertSrc = ShaderSourceCache::getShader("countLFBVert_CL").loadFromFile("shaders/lfb/counts.vert");
		auto countFragSrc = ShaderSourceCache::getShader("countLFBFrag_CL").loadFromFile("shaders/lfb/counts.frag");
		countFragSrc.setDefine("STORAGE_BUFFERS", Utils::toString(LINEAR_STORAGE_BUFFERS));
		countFragSrc.setDefine("INDEX_BY_TILE", Utils::toString(indexByTile));
		countShader.create(&countVertSrc, &countFragSrc);
	}

#if !LINEAR_STORAGE_BUFFERS
	offsets->bind();
#endif

	if (profiler)
		profiler->start("Counts");

	zeroBuffers();

	return countShader;
}

void LFBCoalesced::endCountFrags()
{
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	if (profiler)
		profiler->time("Counts");

	if (profiler)
		profiler->start("Min Counts");
	calcMinCounts();
	if (profiler)
		profiler->time("Min Counts");

	if (profiler)
		profiler->start("Prefix Sums");
	prefixSums();
	if (profiler)
		profiler->time("Prefix Sums");
}

void LFBCoalesced::beginCapture()
{
#if !LINEAR_STORAGE_BUFFERS
	offsets->bind();
	data->bind();
#endif
}

bool LFBCoalesced::endCapture()
{
#if !LINEAR_STORAGE_BUFFERS
	offsets->unbind();
	data->unbind();
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	return false;
}

void LFBCoalesced::beginComposite()
{
	// Here in case we wrote to global memory during a previous sort.
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

#if !LINEAR_STORAGE_BUFFERS
	offsets->bind();
	data->bind();
#endif
}

void LFBCoalesced::endComposite()
{
#if !LINEAR_STORAGE_BUFFERS
	offsets->unbind();
	data->unbind();
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
}

std::string LFBCoalesced::getStr(bool ignoreEmpty)
{
	switch (dataType)
	{
	case LFB::UINT:
		return getStrTempl<unsigned int>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::INT:
		return getStrTempl<int>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::FLOAT:
		return getStrTempl<float>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC2:
		return getStrTempl<Math::vec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC2:
		return getStrTempl<Math::ivec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC2:
		return getStrTempl<Math::uvec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC4:
		return getStrTempl<Math::vec4>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC4:
		return getStrTempl<Math::ivec4>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC4:
		return getStrTempl<Math::uvec4>(offsets, data, width, height, ignoreEmpty);
		break;
	default:
		break;
	}

	return "";
}

std::string LFBCoalesced::getStrSorted(bool ignoreEmpty)
{
	switch (dataType)
	{
	case LFB::UINT:
		return getStrSortedTempl<unsigned int>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::INT:
		return getStrSortedTempl<int>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::FLOAT:
		return getStrSortedTempl<float>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC2:
		return getStrSortedTempl<Math::vec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC2:
		return getStrSortedTempl<Math::ivec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC2:
		return getStrSortedTempl<Math::uvec2>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC4:
		return getStrSortedTempl<Math::vec4>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC4:
		return getStrSortedTempl<Math::ivec4>(offsets, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC4:
		return getStrSortedTempl<Math::uvec4>(offsets, data, width, height, ignoreEmpty);
		break;
	default:
		break;
	}

	return "";
}

std::string LFBCoalesced::getBinary()
{
	return "";
}

std::string LFBCoalesced::getSVG()
{
	// TODO: Implement me!
	return "";
}

