#pragma once

#include "Cluster.h"

#include <string>

namespace Rendering
{
	class Shader;
	class Profiler;

	class ClusterBase
	{
	protected:
		std::string name;

		Cluster::DataType dataType;
		int maxFrags;

		unsigned int clusters;

		unsigned int width;
		unsigned int height;

		unsigned int clusterFragAlloc;

		bool storeDataFlag;

		Profiler *profiler;

	public:
		ClusterBase(const std::string &name, unsigned int clusters, bool storeDataFlag = false, Cluster::DataType dataType = Cluster::UINT, int maxFrags = 64) : name(name), dataType(dataType), maxFrags(maxFrags), clusters(clusters), width(0), height(0), clusterFragAlloc(0), storeDataFlag(storeDataFlag), profiler(nullptr) {}
		virtual ~ClusterBase() {}

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }

		virtual void setBmaUniforms(Shader *shader) = 0;
		virtual void setUniforms(Shader *shader, bool fragAlloc = true) = 0;
		virtual void setCountUniforms(Shader *shader) = 0;

		virtual void resize(int width, int height) = 0;

		virtual void beginCountFrags() = 0;
		virtual void endCountFrags() = 0;

		virtual void beginCapture() = 0;
		virtual bool endCapture() = 0;

		virtual void beginComposite() = 0;
		virtual void endComposite() = 0;

		virtual std::string getStr() = 0;
		virtual std::string getStrSorted() = 0;

		virtual size_t getFragSize() const { return Cluster::getFragSize(dataType); }
		virtual Cluster::DataType getDatatype() const { return dataType; }

		virtual void setMaxFrags(int maxFrags) { this->maxFrags = maxFrags; }
		virtual int getMaxFrags() const { return maxFrags; }

		const std::string &getName() const { return name; }
		unsigned int getClusters() const { return clusters; }
		unsigned int getWidth() const { return width; }
		unsigned int getHeight() const { return height; }
		unsigned int getClusterFragAlloc() const { return clusterFragAlloc; }
	};
}

