#include "Cluster.h"

#include "ClusterBase.h"
#include "ClusterList.h"
#include "ClusterLinear.h"
#include "ClusterCoalesced.h"
#include "../Renderer.h"

#include <cassert>

using namespace Rendering;

Cluster::Cluster() : type(NONE), cluster(nullptr)
{
}

Cluster::~Cluster()
{
	release();
}

void Cluster::setProfiler(Profiler *profiler)
{
	cluster->setProfiler(profiler);
}

void Cluster::release()
{
	delete cluster;
	cluster = nullptr;
}

void Cluster::setBmaUniforms(Shader *shader)
{
	cluster->setBmaUniforms(shader);
}

void Cluster::setUniforms(Shader *shader, bool fragAlloc)
{
	cluster->setUniforms(shader, fragAlloc);
}

void Cluster::setCountUniforms(Shader *shader)
{
	cluster->setCountUniforms(shader);
}

void Cluster::resize(int width, int height)
{
	cluster->resize(width, height);
}

void Cluster::beginCountFrags()
{
	cluster->beginCountFrags();
}

void Cluster::endCountFrags()
{
	cluster->endCountFrags();
}

void Cluster::beginCapture()
{
	cluster->beginCapture();
}

bool Cluster::endCapture()
{
	return cluster->endCapture();
}

void Cluster::beginComposite()
{
	cluster->beginComposite();
}

void Cluster::endComposite()
{
	cluster->endComposite();
}

std::string Cluster::getStr()
{
	return cluster->getStr();
}

std::string Cluster::getStrSorted()
{
	return cluster->getStrSorted();
}

size_t Cluster::getFragSize() const
{
	return cluster->getFragSize();
}

void Cluster::setMaxFrags(int maxFrags)
{
	cluster->setMaxFrags(maxFrags);
}

int Cluster::getMaxFrags() const
{
	return cluster->getMaxFrags();
}

void Cluster::setType(ClusterType type, const std::string &name, unsigned int clusters, bool storeDataFlag, Cluster::DataType dataType, bool overrideDefault, int maxFrags, int useAtomics)
{
	// TODO: may need some more checks here.
	if (cluster != nullptr && type == this->type && cluster->getName() == name && cluster->getClusters() == clusters)
		return;

	delete cluster;
	cluster = nullptr;
	this->type = type;

	switch (type)
	{
	case LINK_LIST:
		cluster = new ClusterList(name, clusters, storeDataFlag, dataType, overrideDefault, maxFrags);
		break;

	case LINEARIZED:
		cluster = new ClusterLinear(name, clusters, storeDataFlag, dataType, overrideDefault, maxFrags, useAtomics);
		break;

	case COALESCED:
		cluster = new ClusterCoalesced(name, clusters, storeDataFlag, dataType, overrideDefault, maxFrags, useAtomics);
		break;

	default:
		assert(false);
		break;
	}
}

const std::string &Cluster::getName() const
{
	return cluster->getName();
}

unsigned int Cluster::getClusters()
{
	return cluster->getClusters();
}

unsigned int Cluster::getWidth()
{
	return cluster->getWidth();
}

unsigned int Cluster::getHeight()
{
	return cluster->getHeight();
}

unsigned int Cluster::getClusterFragAlloc()
{
	return cluster->getClusterFragAlloc();
}

size_t Cluster::getFragSize(Cluster::DataType type)
{
	switch (type)
	{
	case UINT:
	case INT:
	case FLOAT:
		return 1;
		break;
	case VEC2:
	case IVEC2:
	case UVEC2:
		return 2;
		break;
	case VEC4:
	case IVEC4:
	case UVEC4:
		return 4;
		break;
	default:
		break;
	}
	return 1;
}

