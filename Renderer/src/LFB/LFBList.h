#pragma once

#include "LFBBase.h"
#include "../RenderObject/Shader.h"

#define LIST_STORAGE_BUFFERS 1

namespace Rendering
{
	class StorageBuffer;
	class TextureBuffer;
	class AtomicBuffer;

	class LFBList : public LFBBase
	{
	private:
		LFBList(const LFBList &list);
		LFBList operator = (const LFBList &list);

	protected:
		Shader zeroShader;

		AtomicBuffer *fragCount;

	#if LIST_STORAGE_BUFFERS
		StorageBuffer *counts;
		StorageBuffer *headPtrs;
		StorageBuffer *nextPtrs;
		StorageBuffer *data;
	#else
		TextureBuffer *counts;
		TextureBuffer *headPtrs;
		TextureBuffer *nextPtrs;
		TextureBuffer *data;
	#endif

	public:
		LFBList(const std::string &name = "lfb", LFB::DataType dataType = LFB::VEC2, bool overrideDefault = true, int maxFrags = 64);
		virtual ~LFBList();

		virtual void overrideDefault() override;

		virtual void setBmaUniforms(Shader *shader) override;
		virtual void setUniforms(Shader *shader, bool fragAlloc = true, std::string nameOverride = "") override;
		virtual void setCountUniforms(Shader *shader) override;

		virtual void resize(int width, int height) override;
		void zeroBuffers();

		virtual Shader &beginCountFrags(int indexByTile = 0) override;
		virtual void endCountFrags() override;

		virtual void beginCapture() override;
		virtual bool endCapture() override;
		
		virtual void beginComposite() override;
		virtual void endComposite() override;

		virtual std::string getStr(bool ignoreEmpty = false) override;
		virtual std::string getStrSorted(bool ignoreEmpty = false) override;

		virtual std::string getBinary() override;
		virtual std::string getSVG() override;

	#if LIST_STORAGE_BUFFERS
		StorageBuffer *getCounts() { return counts; }
		StorageBuffer *getHeadPtrs() { return headPtrs; }
		StorageBuffer *getNextPtrs() { return nextPtrs; }
		StorageBuffer *getData() { return data; }
	#else
		TextureBuffer *getCounts() { return counts; }
		TextureBuffer *getHeadPtrs() { return headPtrs; }
		TextureBuffer *getNextPtrs() { return nextPtrs; }
		TextureBuffer *getData() { return data; }
	#endif
	};
}

