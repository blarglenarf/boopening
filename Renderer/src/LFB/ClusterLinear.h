#pragma once

#include "ClusterBase.h"

#include "../RenderObject/Shader.h"

#include <string>

namespace Rendering
{
	class StorageBuffer;

	class ClusterLinear : public ClusterBase
	{
	private:
		ClusterLinear(const ClusterLinear &cluster);
		ClusterLinear operator = (const ClusterLinear &cluster);

	private:
		Shader zeroClusterShader;
		Shader prefixClusterShader;

		unsigned int prefixSumSize;

		StorageBuffer *clusterOffsets;
		StorageBuffer *clusterPos;
		StorageBuffer *clusterDir;
		StorageBuffer *clusterData;

	public:
		ClusterLinear(const std::string &name = "cluster", unsigned int clusters = 32, bool storeDataFlag = false, Cluster::DataType dataType = Cluster::VEC2, bool overrideDefault = true, int maxFrags = 64, int useAtomics = 0);
		virtual ~ClusterLinear();

		virtual void setBmaUniforms(Shader *shader) override;
		virtual void setUniforms(Shader *shader, bool fragAlloc = true) override;
		virtual void setCountUniforms(Shader *shader) override;

		virtual void resize(int width, int height) override;

		void zeroBuffers();
		void prefixSums();

		// Requires a storage buffer called Offsets.
		virtual void beginCountFrags() override;
		virtual void endCountFrags() override;

		virtual void beginCapture() override;
		virtual bool endCapture() override;

		virtual void beginComposite() override;
		virtual void endComposite() override;

		virtual std::string getStr() override;
		virtual std::string getStrSorted() override;

		StorageBuffer *getClusterOffsets() { return clusterOffsets; }
		StorageBuffer *getClusterData() { return clusterData; }
	};
}
