#pragma once

#include "LFBBase.h"
#include "../RenderObject/Shader.h"

#include <string>

#define LINEAR_STORAGE_BUFFERS 1

namespace Rendering
{
	class StorageBuffer;
	class TextureBuffer;
	class AtomicBuffer;

	class LFBLinear : public LFBBase
	{
	private:
		LFBLinear(const LFBLinear &list);
		LFBLinear operator = (const LFBLinear &list);

	private:
		Shader zeroShader;
		Shader countShader;
		Shader prefixShader;

		Shader offsetIncShader;

	#if LINEAR_STORAGE_BUFFERS
		StorageBuffer *offsets;
		StorageBuffer *data;
	#else
		TextureBuffer *offsets;
		TextureBuffer *data;
	#endif

		unsigned int prefixSumSize;

	public:
		LFBLinear(const std::string &name = "lfb", LFB::DataType dataType = LFB::VEC2, bool overrideDefault = true, int maxFrags = 64);
		virtual ~LFBLinear();

		virtual void overrideDefault() override;

		virtual void setBmaUniforms(Shader *shader) override;
		virtual void setUniforms(Shader *shader, bool fragAlloc = true, std::string nameOverride = "") override;
		virtual void setCountUniforms(Shader *shader) override;

		virtual void resize(int width, int height) override;

		void zeroBuffers();
		void prefixSums();

		// In case you want to calculate prefix sums from a linked list.
		void prefixSums(StorageBuffer *counts);
		void incrementOffsets();

		virtual Shader &beginCountFrags(int indexByTile = 0) override;
		virtual void endCountFrags() override;

		virtual void beginCapture() override;
		virtual bool endCapture() override;
		
		virtual void beginComposite() override;
		virtual void endComposite() override;

		virtual std::string getStr(bool ignoreEmpty = false) override;
		virtual std::string getStrSorted(bool ignoreEmpty = false) override;

		virtual std::string getBinary() override;
		virtual std::string getSVG() override;

	#if LINEAR_STORAGE_BUFFERS
		StorageBuffer *getOffsets() { return offsets; }
		StorageBuffer *getData() { return data; }
	#else
		TextureBuffer *getOffsets() { return offsets; }
		TextureBuffer *getData() { return data; }
	#endif
	};
}

