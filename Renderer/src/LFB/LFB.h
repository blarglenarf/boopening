#pragma once

#include "../../../Math/src/Matrix/MatrixCommon.h"

#include <string>

namespace Rendering
{
	class LFBBase;
	class LFBList;
	class LFBLinear;
	class Shader;
	class Profiler;

	class LFB
	{
	public:
		enum LFBType
		{
			NONE,
			LINEARIZED,
			LINK_LIST,
			COALESCED
		};

		enum DataType
		{
			UINT,
			INT,
			FLOAT,
			VEC2,
			VEC4,
			IVEC2,
			IVEC4,
			UVEC2,
			UVEC4
		};

	private:
		LFBType type;
		LFBBase *lfb;

		Shader *visualiseShader;

	public:
		LFB();
		~LFB();

		void overrideDefault();

		void setProfiler(Profiler *profiler);

		void release();

		void setBmaUniforms(Shader *shader);
		void setUniforms(Shader *shader, bool fragAlloc = true, std::string nameOverride = "");
		void setCountUniforms(Shader *shader);

		void resize(int width, int height);

		// These are only really used by the linearised lfb.
		Shader &beginCountFrags(int indexByTile = 0);
		void endCountFrags();

		void beginCapture();
		bool endCapture();

		void beginComposite();
		void endComposite();

		void composite(Shader *shader);

		void drawData(const Math::mat4 &origInvMvMatrix, const Math::mat4 &origInvPMatrix, const Math::ivec4 &origViewport, const Math::mat4 &mvMatrix, const std::string &lfbName = "lfb");

		std::string getStr(bool ignoreEmpty = false);
		std::string getStrSorted(bool ignoreEmpty = false);

		std::string getBinary();
		std::string getSVG();

		size_t getFragSize() const;

		void setMaxFrags(int maxFrags);
		int getMaxFrags() const;

		void setType(LFBType type, const std::string &name = "lfb", LFB::DataType dataType = LFB::VEC2, bool overrideDefault = true);

		void setDataType(DataType type);
		LFBType getType() const { return type; }

		void setName(const std::string &name);
		const std::string &getName() const;

		unsigned int getFragAlloc() const;
		unsigned int getWidth() const;
		unsigned int getHeight() const;

		LFBBase *getBase() { return lfb; }

		static size_t getFragSize(LFB::DataType type);
	};
}

