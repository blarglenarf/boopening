#pragma once

#include <string>

namespace Rendering
{
	class ClusterBase;
	class ClusterList;
	class ClusterLinear;
	class Shader;
	class Profiler;

	class Cluster
	{
	public:
		enum ClusterType
		{
			NONE,
			LINEARIZED,
			LINK_LIST,
			COALESCED
		};

		enum DataType
		{
			UINT,
			INT,
			FLOAT,
			VEC2,
			VEC4,
			IVEC2,
			IVEC4,
			UVEC2,
			UVEC4
		};

	private:
		ClusterType type;
		ClusterBase *cluster;

	public:
		Cluster();
		~Cluster();

		void setProfiler(Profiler *profiler);

		void release();

		void setBmaUniforms(Shader *shader);
		void setUniforms(Shader *shader, bool fragAlloc = true);
		void setCountUniforms(Shader *shader);
		void resize(int width, int height);

		// These are only really used by linearised clustering (requires a storage buffer named Offsets).
		void beginCountFrags();
		void endCountFrags();

		void beginCapture();
		bool endCapture();

		void beginComposite();
		void endComposite();

		std::string getStr();
		std::string getStrSorted();

		size_t getFragSize() const;

		void setMaxFrags(int maxFrags);
		int getMaxFrags() const;

		void setType(ClusterType type, const std::string &name = "cluster", unsigned int clusters = 32, bool storeDataFlag = false, Cluster::DataType dataType = Cluster::UINT, bool overrideDefault = true, int maxFrags = 64, int useAtomics = 0);
		ClusterType getType() const { return type; }

		const std::string &getName() const;

		unsigned int getClusters();
		unsigned int getWidth();
		unsigned int getHeight();
		unsigned int getClusterFragAlloc();

		ClusterBase *getBase() { return cluster; }

		static size_t getFragSize(Cluster::DataType type);
	};
}

