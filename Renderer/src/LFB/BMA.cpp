#include "BMA.h"

#include "LFB.h"
#include "../RenderObject/Shader.h"
#include "../RenderObject/GPUBuffer.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"
#include "../Renderer.h"

#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"

#define DEBUG_INTERVALS 0

using namespace Rendering;


BMA::Interval::~Interval()
{
	delete shader;
	shader = nullptr;
}


BMA::IntervalCount::IntervalCount() : count(0)
{
	counter = new AtomicBuffer();
	pixels = new StorageBuffer();
}

BMA::IntervalCount::~IntervalCount()
{
	delete counter;
	delete pixels;
	counter = nullptr;
	pixels = nullptr;
	count = 0;
}


BMA::~BMA()
{
	intervals.clear();
	intervalCounts.clear();

	delete maskShader;
	delete countIntervalsShader;
}

void BMA::release()
{
	intervals.clear();
	intervalCounts.clear();

	if (maskShader)
		maskShader->release();
	if (countIntervalsShader)
		countIntervalsShader->release();
}

void BMA::createEmptyMaskShader()
{
	if (maskShader)
		maskShader->release();
	else
		maskShader = new Shader();
}

void BMA::createMaskShader(const std::string &name, const std::string &vert, const std::string &frag, const std::vector<BMA::TagPair> &fragDefines)
{
	createEmptyMaskShader();

	auto bmaVert = ShaderSourceCache::getShader("bmaMaskVert" + name).loadFromFile(vert);
	auto bmaFrag = ShaderSourceCache::getShader("bmaMaskFrag" + name).loadFromFile(frag);
	bmaFrag.replace("LFB_NAME", name);
	bmaFrag.setDefine("DEBUG", Utils::toString(DEBUG_INTERVALS));
	for (auto &fragDefine : fragDefines)
		bmaFrag.setDefine(fragDefine.tag, fragDefine.value);
	maskShader->create(&bmaVert, &bmaFrag);
}

Shader *BMA::getMaskShader()
{
	return maskShader;
}

void BMA::createShaders(LFB *lfb, const std::string &vert, const std::string &frag, const std::vector<BMA::TagPair> &fragDefines)
{
	int maxBmaInterval = 1 << Utils::ceilLog2(lfb->getMaxFrags());
	int prev = 0;

	intervals.clear();

	std::vector<int> splits;
	for (int interval = 8; interval <= maxBmaInterval; interval *= 2)
		splits.push_back(interval);
	std::sort(splits.begin(), splits.end());

	intervals.resize(splits.size());

	for (size_t i = 0; i < splits.size(); i++)
	{
		int intervalEnd = splits[i];
		intervals[i].start = prev;
		intervals[i].end = intervalEnd;
		intervals[i].shader = new Shader();
		auto &bmaVert = ShaderSourceCache::getShader("bmaVert" + Utils::toString(intervalEnd) + lfb->getName()).loadFromFile(vert);
		auto &bmaFrag = ShaderSourceCache::getShader("bmaFrag" + Utils::toString(intervalEnd) + lfb->getName()).loadFromFile(frag);
		bmaFrag.setDefine("MAX_FRAGS", Utils::toString(lfb->getMaxFrags()));
		bmaFrag.setDefine("MAX_FRAGS_OVERRIDE", Utils::toString(intervalEnd));
		for (auto &fragDefine : fragDefines)
			bmaFrag.setDefine(fragDefine.tag, fragDefine.value);
		bmaFrag.replace("LFB_NAME", lfb->getName());
		intervals[i].shader->create(&bmaVert, &bmaFrag);
		prev = intervalEnd;
	}

	// TODO: may be worth re-using bma shader sources that are already stored in the shader source map.
}

void BMA::setShader(size_t idx, LFB *lfb, const std::string &vert, const std::string &frag, const std::vector<BMA::TagPair> &fragDefines)
{
	if (idx >= intervals.size())
		return;

	auto &interval = intervals[idx];
	interval.shader->release();

	auto &bmaVert = ShaderSourceCache::getShader("bmaVert" + Utils::toString(interval.end) + lfb->getName()).loadFromFile(vert);
	auto &bmaFrag = ShaderSourceCache::getShader("bmaFrag" + Utils::toString(interval.end) + lfb->getName()).loadFromFile(frag);
	bmaFrag.setDefine("MAX_FRAGS", Utils::toString(lfb->getMaxFrags()));
	bmaFrag.setDefine("MAX_FRAGS_OVERRIDE", Utils::toString(interval.end));
	for (auto &fragDefine : fragDefines)
		bmaFrag.setDefine(fragDefine.tag, fragDefine.value);
	bmaFrag.replace("LFB_NAME", lfb->getName());
	interval.shader->create(&bmaVert, &bmaFrag);
}

void BMA::setShader(size_t idx, LFB *lfb, const std::string &comp, const std::vector<BMA::TagPair> &compDefines, bool vertexFlag)
{
	if (idx >= intervals.size())
		return;

	auto &interval = intervals[idx];
	interval.shader->release();

	auto &bmaComp = ShaderSourceCache::getShader("bmaComp" + Utils::toString(interval.end) + lfb->getName()).loadFromFile(comp);
	bmaComp.setDefine("MAX_FRAGS", Utils::toString(lfb->getMaxFrags()));
	bmaComp.setDefine("MAX_FRAGS_OVERRIDE", Utils::toString(interval.end));
	for (auto &compDefine : compDefines)
		bmaComp.setDefine(compDefine.tag, compDefine.value);
	bmaComp.replace("LFB_NAME", lfb->getName());
	if (vertexFlag)
		interval.shader->create(&bmaComp);
	else
		interval.shader->createCompute(&bmaComp);
}

void BMA::createCounts(LFB *lfb, const std::string &vert, const std::string &frag, const std::vector<BMA::TagPair> &fragDefines)
{
	if (!countIntervalsShader)
		countIntervalsShader = new Shader();

	auto &countVert = ShaderSourceCache::getShader("bmaCountVert" + lfb->getName()).loadFromFile(vert);
	auto &countFrag = ShaderSourceCache::getShader("bmaCountFrag" + lfb->getName()).loadFromFile(frag);
	for (auto &fragDefine : fragDefines)
		countFrag.setDefine(fragDefine.tag, fragDefine.value);
	countFrag.replace("LFB_NAME", lfb->getName());
	countIntervalsShader->release();
	countIntervalsShader->create(&countVert, &countFrag);

	unsigned int zero = 0;
	intervalCounts.resize(intervals.size());
	for (auto &interval : intervalCounts)
		interval.counter->create(&zero, sizeof(zero));
}

void BMA::countIntervals(LFB *lfb)
{
	if (intervalCounts.empty())
		return;

	if (profiler)
		profiler->start("bmaCount");

	countIntervalsShader->bind();

	for (size_t i = 0; i < intervalCounts.size(); i++)
	{
		auto &interval = intervalCounts[i];
		interval.counter->setUniform((int) i + 1);
	}

	lfb->setBmaUniforms(countIntervalsShader);

	Rendering::Renderer::instance().drawQuad();

	countIntervalsShader->unbind();

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	unsigned int zero = 0;
	for (auto &interval : intervalCounts)
	{
		interval.count = *((unsigned int*) interval.counter->read());
		//std::cout << interval.count << "\n";
		interval.pixels->create(nullptr, interval.count * sizeof(unsigned int));
		interval.counter->bufferData(&zero, sizeof(zero));
	}
	//std::cout << "\n";

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	if (profiler)
		profiler->time("bmaCount");
}

void BMA::createMask(LFB *lfb)
{
#if !DEBUG_INTERVALS
	glClear(GL_STENCIL_BUFFER_BIT);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);
#endif

	maskShader->bind();
	lfb->setBmaUniforms(maskShader);
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		if ((int) intervalCounts.size() > i)
		{
			maskShader->setUniform("IntervalPixels", intervalCounts[i].pixels);
			intervalCounts[i].counter->setUniform(1);
		}
		maskShader->setUniform("interval", intervals[i].start);
		glStencilFunc(GL_GREATER, 1<<i, 0xFF);
		Rendering::Renderer::instance().drawQuad();
	}
	maskShader->unbind();

#if !DEBUG_INTERVALS
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDisable(GL_STENCIL_TEST);
#endif

	unsigned int zero = 0;
	for (auto &interval : intervalCounts)
		interval.counter->bufferData(&zero, sizeof(zero));
#if 0
	// What's in the pixel data?
	for (auto &interval : intervalCounts)
	{
		if (interval.count <= 0)
			continue;
		std::cout << interval.count << "\n";
		auto *pixels = (unsigned int*) interval.pixels->read();
		for (size_t i = 0; i < interval.count; i++)
			std::cout << pixels[i] << ", ";
		std::cout << "\n";
	}
	std::cout << "\n";
	exit(0);
#endif
}

void BMA::sort(LFB *lfb)
{
#if DEBUG_INTERVALS
	(void) (lfb);
#else
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		if (profiler)
			profiler->start("bmaInterval" + Utils::toString(i));
		glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
		intervals[i].shader->bind();
		lfb->setBmaUniforms(intervals[i].shader);
		Rendering::Renderer::instance().drawQuad();
		intervals[i].shader->unbind();
		if (profiler)
			profiler->time("bmaInterval" + Utils::toString(i));
	}
	glDisable(GL_STENCIL_TEST);
#endif
}

void BMA::sort(LFB *from, LFB *to)
{
#if DEBUG_INTERVALS
	(void) (from);
	(void) (to);
#else
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		if (profiler)
			profiler->start("bmaInterval" + Utils::toString(i));
		glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
		intervals[i].shader->bind();
		from->setBmaUniforms(intervals[i].shader);
		to->setBmaUniforms(intervals[i].shader);
		Rendering::Renderer::instance().drawQuad();
		intervals[i].shader->unbind();
		if (profiler)
			profiler->time("bmaInterval" + Utils::toString(i));
	}
	glDisable(GL_STENCIL_TEST);
#endif
}

