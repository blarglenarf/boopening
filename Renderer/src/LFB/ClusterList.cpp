#include "ClusterList.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Shader.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"

#include "../../../Math/src/MathCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <cassert>
#include <iostream>

#define LIGHT_OVERALLOCATE 1.2
#define LIGHT_UNDERALLOCATE 0.5

using namespace Rendering;


template <typename T>
static std::string getStrTempl(StorageBuffer *clusterHeadPtrs, StorageBuffer *clusterNextPtrs, StorageBuffer *clusterData, unsigned int clusters, unsigned int width, unsigned int height)
{
	// TODO: Take into account fragSize and maxFrags...
	std::stringstream s;

	// TODO: test me!
	unsigned int *headData = (unsigned int*) clusterHeadPtrs->read();
	unsigned int *nextData = (unsigned int*) clusterNextPtrs->read();
	T *fragData = (T*) clusterData->read();

	int maxFrags = 0;

	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			int currMaxFrags = 0;

			s << x << "," << y << ": ";

			std::vector<std::pair<unsigned int, T>> clusterVector;

			for (unsigned int cluster = 0; cluster < clusters; cluster++)
			{
				// Convert the pixel into cluster space.
				int fragIndex = x * height * clusters + y * clusters + cluster;
				unsigned int node = headData[fragIndex];

				while (node != 0)
				{
					T t = fragData[node];
					clusterVector.push_back({cluster, t});
					//s << cluster << ", " << id << "; ";

					node = nextData[node];
					currMaxFrags++;
				}
			}

			if (currMaxFrags > maxFrags)
				maxFrags = currMaxFrags;

			for (auto &f : clusterVector)
				s << f.first << "," << f.second << "; ";

			s << "\n";
		}
	}
	s << "\n\n\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + s.str();
}

template <typename T>
static std::string getStrSortedTempl(StorageBuffer *clusterHeadPtrs, StorageBuffer *clusterNextPtrs, StorageBuffer *clusterData, unsigned int clusters, unsigned int width, unsigned int height)
{
	// TODO: Take into account fragSize and maxFrags...
	std::stringstream s;

	// TODO: test me!
	unsigned int *headData = (unsigned int*) clusterHeadPtrs->read();
	unsigned int *nextData = (unsigned int*) clusterNextPtrs->read();
	T *fragData = (T*) clusterData->read();

	int maxFrags = 0;

	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			int currMaxFrags = 0;

			s << x << "," << y << ": ";

			std::vector<std::pair<unsigned int, T>> clusterVector;

			for (unsigned int cluster = 0; cluster < clusters; cluster++)
			{
				// Convert the pixel into cluster space.
				int fragIndex = x * height * clusters + y * clusters + cluster;
				unsigned int node = headData[fragIndex];

				while (node != 0)
				{
					T t = fragData[node];
					clusterVector.push_back({cluster, t});
					//s << cluster << ", " << id << "; ";

					node = nextData[node];
					currMaxFrags++;
				}
			}

			if (currMaxFrags > maxFrags)
				maxFrags = currMaxFrags;

			std::sort(clusterVector.begin(), clusterVector.end(), [](const std::pair<unsigned int, T> &v, const std::pair<unsigned int, T> &u)
			{
				if (u.second == v.second)
					return u.first < v.first;
				return u.second < v.second;
			});
			for (auto &f : clusterVector)
				s << f.first << "," << f.second << "; ";

			s << "\n";
		}
	}
	s << "\n\n\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + s.str();
}


static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer HeadPtrs\
	{\
		uint headPtrs[];\
	};\
	\
	void main()\
	{\
		headPtrs[gl_VertexID] = 0;\
	}\
	";
}

static void createLfbSrc(const std::string &name, unsigned int clusters)
{
	auto &src = ShaderSourceCache::getShader(name).loadFromFile("shaders/lfb/clusterLL.glsl");
	src.setDefine("CLUSTERS", Utils::toString(clusters));
}



ClusterList::ClusterList(const std::string &name, unsigned int clusters, bool storeDataFlag, Cluster::DataType dataType, bool overrideDefault, int maxFrags) : ClusterBase(name, clusters, storeDataFlag, dataType, maxFrags)
{
	clusterFragCount = new AtomicBuffer();
	clusterHeadPtrs = new StorageBuffer();
	clusterNextPtrs = new StorageBuffer();
	clusterPos = new StorageBuffer();
	clusterDir = new StorageBuffer();
	clusterData = new StorageBuffer();

	std::string lName = "cluster_LL";

	if (!ShaderSourceCache::exists(lName))
		createLfbSrc(lName, clusters);

	//if (!ShaderSource::exists("lfb"))
	if (overrideDefault)
		createLfbSrc("cluster", clusters);
}

ClusterList::~ClusterList()
{
	delete clusterFragCount;
	delete clusterHeadPtrs;
	delete clusterNextPtrs;
	delete clusterPos;
	delete clusterDir;
	delete clusterData;

	clusterFragCount = nullptr;
	clusterHeadPtrs = nullptr;
	clusterNextPtrs = nullptr;
	clusterPos = nullptr;
	clusterDir = nullptr;
	clusterData = nullptr;

	zeroClusterShader.release();
}

void ClusterList::setBmaUniforms(Shader *shader)
{
	//shader->setUniform("ClusterPixelCounts" + name, clusterPixelCounts);
	//shader->setUniform("clusterSize" + name, Math::ivec2(width, height));
	setUniforms(shader, false);
}

void ClusterList::setUniforms(Shader *shader, bool fragAlloc)
{
	if (fragAlloc)
		shader->setUniform("clusterFragCount", clusterFragCount);
	shader->setUniform("ClusterHeadPtrs" + name, clusterHeadPtrs);
	shader->setUniform("ClusterNextPtrs" + name, clusterNextPtrs);
	if (storeDataFlag)
	{
		shader->setUniform("ClusterPos" + name, clusterPos);
		shader->setUniform("ClusterDir" + name, clusterDir);
	}
	shader->setUniform("ClusterData" + name, clusterData);
	shader->setUniform("clusterSize" + name, Math::ivec2(width, height));
	if (fragAlloc)
		shader->setUniform("clusterFragAlloc" + name, clusterFragAlloc);
}

void ClusterList::setCountUniforms(Shader *shader)
{
	shader->setUniform("size", Math::ivec2(width, height));
}

void ClusterList::resize(int width, int height)
{
	if ((int) this->width == width && (int) this->height == height)
		return;

	this->width = width;
	this->height = height;

	// No longer just one per pixel, but now one per cluster (eg 256 per pixel).
	clusterHeadPtrs->create(nullptr, width * height * clusters * sizeof(unsigned int));

	if (!clusterFragCount->isGenerated())
	{
		// Have to start at one since zero is being used as null.
		unsigned int one = 1;
		clusterFragCount->create(&one, sizeof(one));
	}

	// For now just allocate 5 frags per pixel for the lfb, this will dynamically resize later.
	if (clusterFragAlloc == 0)
		clusterFragAlloc = width * height * 5;

	// No need to buffer the nextPtrs or data if they've already been created, if more are needed we handle it after capture.
	if (!clusterNextPtrs->isGenerated())
		clusterNextPtrs->create(nullptr, clusterFragAlloc * sizeof(unsigned int));

	size_t totalSize = 0;
	if (storeDataFlag)
	{
		if (!clusterPos->isGenerated())
			clusterPos->create(nullptr, clusterFragAlloc * sizeof(float) * 4);
		if (!clusterDir->isGenerated())
			clusterDir->create(nullptr, clusterFragAlloc * sizeof(float) * 4);
		totalSize += clusterFragAlloc * sizeof(float) * 8;
	}

	// Light fragments require id only.
	if (!clusterData->isGenerated())
		clusterData->create(nullptr, clusterFragAlloc * sizeof(float) * getFragSize());
	totalSize += clusterFragAlloc * sizeof(float) * getFragSize();

	// If we're using a profiler, set memory buffer sizes.
	if (profiler)
	{
		profiler->setBufferSize("clusterHeadPtrs", width * height * clusters * sizeof(unsigned int));
		profiler->setBufferSize("clusterNextPtrs", clusterFragAlloc * sizeof(unsigned int));
		profiler->setBufferSize("clusterData", totalSize);
		profiler->setBufferSize("clusterTotalData", totalSize + (width * height * clusters * sizeof(unsigned int)) + clusterFragAlloc * sizeof(unsigned int));
	}
}

void ClusterList::zeroBuffers()
{
	if (!zeroClusterShader.isGenerated())
	{
		auto zeroSrc = ShaderSource("zeroCluster_LL", getZeroShaderSrc());
		zeroClusterShader.create(&zeroSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Now one thread per pixel per cluster (eg 256 per pixel).
	glEnable(GL_RASTERIZER_DISCARD);
	zeroClusterShader.bind();
	zeroClusterShader.setUniform("HeadPtrs", clusterHeadPtrs);
	glDrawArrays(GL_POINTS, 0, width * height * clusters);
	zeroClusterShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void ClusterList::beginCountFrags()
{
}

void ClusterList::endCountFrags()
{
}

void ClusterList::beginCapture()
{
	// TODO: linearize this somehow...
	// First count the number of active clusters per pixel (this can be done during the original count).
	// Then count the number of fragments in each active cluster (again, can be done during the original count).
	// Prefix sum all of that so you have correct memory offsets.
	// Now save light ids to their clusters.
	// Alternatively, keep an offset per cluster.
	//
	// For this version, instead of linearizing:
	// Just step through the light image, and save active light ids to each cluster.

	// Zero head pointers (one pointer per cluster) and create light clusters using the light image.
	zeroBuffers();
}

bool ClusterList::endCapture()
{
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// It's possible that not enough (or far too many) frags were allocated, if so, reallocate.
	unsigned int totalFrags = *((unsigned int*) clusterFragCount->read());

	bool resizeFlag = false;
	if (totalFrags > clusterFragAlloc || totalFrags < clusterFragAlloc * LIGHT_UNDERALLOCATE)
	{
		clusterFragAlloc = (unsigned int) (totalFrags * LIGHT_OVERALLOCATE);
		resizeFlag = true;
	}

	if (resizeFlag)
	{
		clusterNextPtrs->bufferData(nullptr, clusterFragAlloc * sizeof(unsigned int));

		size_t totalSize = 0;
		if (storeDataFlag)
		{
			clusterPos->bufferData(nullptr, clusterFragAlloc * sizeof(float) * 4);
			clusterDir->bufferData(nullptr, clusterFragAlloc * sizeof(float) * 4);
			totalSize += clusterFragAlloc * sizeof(float) * 8;
		}

		clusterData->bufferData(nullptr, clusterFragAlloc * sizeof(float) * getFragSize());
		totalSize += clusterFragAlloc * sizeof(float) * getFragSize();

		if (profiler)
		{
			profiler->setBufferSize("clusterNextPtrs", clusterFragAlloc * sizeof(unsigned int));
			profiler->setBufferSize("clusterData", totalSize);
			profiler->setBufferSize("clusterTotalData", totalSize + (width * height * clusters * sizeof(unsigned int)) + clusterFragAlloc * sizeof(unsigned int));
		}
	}

	totalFrags = 1;
	clusterFragCount->bufferData(&totalFrags, sizeof(totalFrags));

	return resizeFlag;
}

void ClusterList::beginComposite()
{
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void ClusterList::endComposite()
{
	// Possibly not necessary, depending on what's done during composite.
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

std::string ClusterList::getStr()
{
	switch (dataType)
	{
	case Cluster::UINT:
		return getStrTempl<unsigned int>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::INT:
		return getStrTempl<int>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::FLOAT:
		return getStrTempl<float>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::VEC2:
		return getStrTempl<Math::vec2>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::IVEC2:
		return getStrTempl<Math::ivec2>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::UVEC2:
		return getStrTempl<Math::uvec2>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::VEC4:
		return getStrTempl<Math::vec4>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::IVEC4:
		return getStrTempl<Math::ivec4>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::UVEC4:
		return getStrTempl<Math::uvec4>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	default:
		break;
	}

	return "";
}

std::string ClusterList::getStrSorted()
{
	switch (dataType)
	{
	case Cluster::UINT:
		return getStrSortedTempl<unsigned int>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::INT:
		return getStrSortedTempl<int>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::FLOAT:
		return getStrSortedTempl<float>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::VEC2:
		return getStrSortedTempl<Math::vec2>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::IVEC2:
		return getStrSortedTempl<Math::ivec2>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::UVEC2:
		return getStrSortedTempl<Math::uvec2>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::VEC4:
		return getStrSortedTempl<Math::vec4>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::IVEC4:
		return getStrSortedTempl<Math::ivec4>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	case Cluster::UVEC4:
		return getStrSortedTempl<Math::uvec4>(clusterHeadPtrs, clusterNextPtrs, clusterData, clusters, width, height);
		break;
	default:
		break;
	}

	return "";
}

