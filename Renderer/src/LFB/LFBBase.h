#pragma once

#include "LFB.h"

#include <string>

namespace Rendering
{
	class Shader;
	class Profiler;

	class LFBBase
	{
	protected:
		std::string name;

		LFB::DataType dataType;
		int maxFrags;

		unsigned int fragAlloc;
		unsigned int width;
		unsigned int height;

		Profiler *profiler;

	public:
		LFBBase(const std::string &name = "lfb", LFB::DataType dataType = LFB::VEC2, int maxFrags = 64) : name(name), dataType(dataType), maxFrags(maxFrags), fragAlloc(0), width(0), height(0), profiler(nullptr) {}
		virtual ~LFBBase() {}

		virtual void overrideDefault() = 0;

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }

		virtual void resize(int width, int height) = 0;

		virtual void setBmaUniforms(Shader *shader) = 0;
		virtual void setUniforms(Shader *shader, bool fragAlloc = true, std::string nameOverride = "") = 0;
		virtual void setCountUniforms(Shader *shader) = 0;

		// These are only really used by the linearised lfb.
		virtual Shader &beginCountFrags(int indexByTile = 0) = 0;
		virtual void endCountFrags() = 0;

		virtual void beginCapture() = 0;
		virtual bool endCapture() = 0;

		virtual void beginComposite() = 0;
		virtual void endComposite() = 0;

		virtual std::string getStr(bool ignoreEmpty = false) = 0;
		virtual std::string getStrSorted(bool ignoreEmpty = false) = 0;

		virtual std::string getBinary() = 0;
		virtual std::string getSVG() = 0;

		virtual size_t getFragSize() const { return LFB::getFragSize(dataType); }

		virtual void setDataType(LFB::DataType dataType) { this->dataType = dataType; }
		virtual LFB::DataType getDataType() const { return dataType; }

		virtual void setMaxFrags(int maxFrags) { this->maxFrags = maxFrags; }
		virtual int getMaxFrags() const { return maxFrags; }

		virtual void setName(const std::string &name) { this->name = name; }
		virtual const std::string &getName() const { return name; }

		unsigned int getFragAlloc() const { return fragAlloc; }
		unsigned int getWidth() const { return width; }
		unsigned int getHeight() const { return height; }
	};
}

