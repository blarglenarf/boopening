#pragma once

#include <string>
#include <vector>

namespace Rendering
{
	class Shader;
	class Profiler;
	class LFB;
	class AtomicBuffer;
	class StorageBuffer;

	class BMA
	{
	public:
		struct Interval
		{
			int start;
			int end;
			Shader *shader;
			Interval(int start = 0, int end = 0, Shader *shader = nullptr) : start(start), end(end), shader(shader) {}
			~Interval();
		};

		struct IntervalCount
		{
			size_t count;
			AtomicBuffer *counter;
			StorageBuffer *pixels;
			IntervalCount();
			~IntervalCount();
		};

		struct TagPair
		{
			std::string tag;
			std::string value;
		};

	private:
		std::vector<Interval> intervals;
		Shader *maskShader;

		std::vector<IntervalCount> intervalCounts;
		Shader *countIntervalsShader;

		Profiler *profiler;

	public:
		BMA() : maskShader(nullptr), countIntervalsShader(nullptr), profiler(nullptr) {}
		~BMA();

		void release();

		void createEmptyMaskShader();
		void createMaskShader(const std::string &name, const std::string &vert, const std::string &frag, const std::vector<BMA::TagPair> &fragDefines = std::vector<BMA::TagPair>());
		Shader *getMaskShader();

		void createShaders(LFB *lfb, const std::string &vert, const std::string &frag, const std::vector<BMA::TagPair> &fragDefines = std::vector<BMA::TagPair>());
		void setShader(size_t idx, LFB *lfb, const std::string &vert, const std::string &frag, const std::vector<BMA::TagPair> &fragDefines = std::vector<BMA::TagPair>());
		void setShader(size_t idx, LFB *lfb, const std::string &comp, const std::vector<BMA::TagPair> &compDefines = std::vector<BMA::TagPair>(), bool vertexFlag = false);

		void createCounts(LFB *lfb, const std::string &vert, const std::string &frag, const std::vector<BMA::TagPair> &fragDefines = std::vector<BMA::TagPair>());
		void countIntervals(LFB *lfb);

		void createMask(LFB *lfb);
		void sort(LFB *lfb);
		void sort(LFB *from, LFB *to);

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }

		std::vector<Interval> &getIntervals() { return intervals; }
		std::vector<IntervalCount> &getIntervalCounts() { return intervalCounts; }
	};
}

