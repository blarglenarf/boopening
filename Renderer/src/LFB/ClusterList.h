#pragma once

#include "ClusterBase.h"

#include "../RenderObject/Shader.h"

#include <string>

namespace Rendering
{
	class AtomicBuffer;
	class StorageBuffer;

	class ClusterList : public ClusterBase
	{
	private:
		ClusterList(const ClusterList &cluster);
		ClusterList operator = (const ClusterList &cluster);

	public:
		Shader zeroClusterShader;

		AtomicBuffer *clusterFragCount;
		StorageBuffer *clusterHeadPtrs;
		StorageBuffer *clusterNextPtrs;
		StorageBuffer *clusterPos;
		StorageBuffer *clusterDir;
		StorageBuffer *clusterData;

	public:
		ClusterList(const std::string &name = "cluster", unsigned int clusters = 32, bool storeDataFlag = false, Cluster::DataType dataType = Cluster::UINT, bool overrideDefault = true, int maxFrags = 64);
		virtual ~ClusterList();

		virtual void setBmaUniforms(Shader *shader) override;
		virtual void setUniforms(Shader *shader, bool fragAlloc = true) override;
		virtual void setCountUniforms(Shader *shader) override;

		virtual void resize(int width, int height) override;

		void zeroBuffers();

		virtual void beginCountFrags() override;
		virtual void endCountFrags() override;

		virtual void beginCapture() override;
		virtual bool endCapture() override;

		virtual void beginComposite() override;
		virtual void endComposite() override;

		virtual std::string getStr() override;
		virtual std::string getStrSorted() override;

		StorageBuffer *getClusterData() { return clusterData; }
	};
}
