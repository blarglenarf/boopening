#include "LFBList.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Shader.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"

#include "../../../Math/src/Vector/VectorCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <iostream>
#include <cstring>
#include <sstream>
#include <algorithm>

#define LFB_ALLOC_EXACT 0
#define LFB_OVERALLOCATE 1.2
#define LFB_UNDERALLOCATE 0.5

using namespace Rendering;

static Shader emptyShader;


template <typename T>
static std::string getStrTempl(StorageBuffer *headPtrs, StorageBuffer *nextPtrs, StorageBuffer *counts, StorageBuffer *data, unsigned int width, unsigned int height, bool ignoreEmpty)
{
	(void) (ignoreEmpty);
	std::stringstream s;
	std::stringstream distStr;
	std::map<unsigned int, unsigned int> distribution;

	size_t maxFrags = 0;

	unsigned int *headData = (unsigned int*) headPtrs->read();
	unsigned int *nextData = (unsigned int*) nextPtrs->read();
	unsigned int *countData = (unsigned int*) counts->read();
	T *fragData = (T*) data->read();

	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			unsigned int pixel = y * width + x;
			unsigned int node = headData[pixel];
			unsigned int count = countData[pixel];

			if (count > maxFrags)
				maxFrags = count;

			s << count << ": " << x << "," << y << ": ";
			if (distribution.find(count) == distribution.end())
				distribution[count] = 1;
			else
				distribution[count]++;

			while (node != 0)
			{
				s << fragData[node] << "; ";
				node = nextData[node];
			}


			s << "\n";
		}
	}
	s << "\n\n\n";

	distStr << "\nDistribution: ";
	for (auto d : distribution)
		distStr << d.first << ": " << d.second << "; ";
	distStr << "\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + distStr.str() + s.str();
}

template <typename T>
static std::string getStrSortedTempl(StorageBuffer *headPtrs, StorageBuffer *nextPtrs, StorageBuffer *counts, StorageBuffer *data, unsigned int width, unsigned int height, bool ignoreEmpty)
{
	(void) (ignoreEmpty);
	std::stringstream s;
	std::stringstream distStr;
	std::map<unsigned int, unsigned int> distribution;

	size_t maxFrags = 0;

	unsigned int *headData = (unsigned int*) headPtrs->read();
	unsigned int *nextData = (unsigned int*) nextPtrs->read();
	unsigned int *countData = (unsigned int*) counts->read();
	T *fragData = (T*) data->read();

	s << "\n";

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			unsigned int pixel = y * width + x;
			unsigned int node = headData[pixel];
			unsigned int count = countData[pixel];

			s << count << ": " << x << "," << y << ": ";
			if (distribution.find(count) == distribution.end())
				distribution[count] = 1;
			else
				distribution[count]++;

			std::vector<T> frags;

			while (node != 0)
			{
				frags.push_back(fragData[node]);
				node = nextData[node];
			}

			std::sort(frags.begin(), frags.end(), [](const T &v, const T &u){ return u < v; });
			for (auto &f : frags)
				s << f << "; ";

			if (frags.size() > maxFrags)
				maxFrags = frags.size();

			if (count != frags.size())
				s << " NOOOOOOOOOOOOOOOOO!";

			s << "\n";
		}
	}
	s << "\n\n\n";

	distStr << "\nDistribution: ";
	for (auto d : distribution)
		distStr << d.first << ": " << d.second << "; ";
	distStr << "\n";

	return std::string("Max frags: ") + Utils::toString(maxFrags) + distStr.str() + s.str();
}

template <typename T>
static std::string getBinaryTempl(StorageBuffer *headPtrs, StorageBuffer *nextPtrs, StorageBuffer *counts, StorageBuffer *data, unsigned int width, unsigned int height)
{
	std::string s;
	s.resize(4096);

	unsigned int *headData = (unsigned int*) headPtrs->read();
	unsigned int *nextData = (unsigned int*) nextPtrs->read();
	unsigned int *countData = (unsigned int*) counts->read();
	T *fragData = (T*) data->read();

	size_t bytesWritten = 0;
	memcpy(&s[0], &width, sizeof(width));
	bytesWritten += sizeof(width);
	memcpy(&s[bytesWritten], &height, sizeof(height));
	bytesWritten += sizeof(height);

	for (unsigned int y = 0; y < height; y++)
	{
		for (unsigned int x = 0; x < width; x++)
		{
			unsigned int pixel = y * width + x;
			unsigned int node = headData[pixel];
			unsigned int count = countData[pixel];

			if (s.size() < bytesWritten + sizeof(count) + sizeof(x) + sizeof(y))
				s.resize(s.size() * 2);

			memcpy(&s[bytesWritten], &count, sizeof(count));
			bytesWritten += sizeof(count);
			memcpy(&s[bytesWritten], &x, sizeof(x));
			bytesWritten += sizeof(x);
			memcpy(&s[bytesWritten], &y, sizeof(y));
			bytesWritten += sizeof(y);

			while (node != 0)
			{
				if (s.size() < bytesWritten + sizeof(fragData[node]))
					s.resize(s.size() * 2);
				memcpy(&s[bytesWritten], &fragData[node], sizeof(fragData[node]));
				bytesWritten += sizeof(fragData[node]);
				node = nextData[node];
			}
		}
	}
	s.resize(bytesWritten);

	return s;
}


#if LIST_STORAGE_BUFFERS
static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer Counts\
	{\
		uint counts[];\
	};\
	\
	buffer HeadPtrs\
	{\
		uint headPtrs[];\
	};\
	\
	void main()\
	{\
		counts[gl_VertexID] = 0;\
		headPtrs[gl_VertexID] = 0;\
	}\
	";
}
#else
static std::string getZeroImgShaderSrc()
{
	return "\
	#version 450\n\
	\
	uniform layout(r32ui) uimageBuffer Counts;\
	uniform layout(r32ui) uimageBuffer HeadPtrs;\
	\
	void main()\
	{\
		imageStore(Counts, gl_VertexID, uvec4(0));\
		imageStore(HeadPtrs, gl_VertexID, uvec4(0));\
	}\
	";
}
#endif

static void createLfbSrc(const std::string &name)
{
	auto &src = ShaderSourceCache::getShader(name).loadFromFile("shaders/lfb/lfbLL.glsl");
	src.setDefine("STORAGE_BUFFERS", Utils::toString(LIST_STORAGE_BUFFERS));
	src.setDefine("REQUIRE_COUNTS", Utils::toString(1));
}



LFBList::LFBList(const std::string &name, LFB::DataType dataType, bool overrideDefault, int maxFrags) : LFBBase(name, dataType, maxFrags), fragCount(nullptr), counts(nullptr), headPtrs(nullptr), nextPtrs(nullptr), data(nullptr)
{
	fragCount = new AtomicBuffer();
#if LIST_STORAGE_BUFFERS
	counts = new StorageBuffer();
	data = new StorageBuffer();
	headPtrs = new StorageBuffer();
	nextPtrs = new StorageBuffer();
#else
	counts = new TextureBuffer(GL_R32UI);
	data = new TextureBuffer(GL_RG32F);
	headPtrs = new TextureBuffer(GL_R32UI);
	nextPtrs = new TextureBuffer(GL_R32UI);
#endif

	std::string lName = "lfb_LL";

	if (!ShaderSourceCache::exists(lName))
		createLfbSrc(lName);

	//if (!ShaderSource::exists("lfb"))
	if (overrideDefault)
		createLfbSrc("lfb");
}

LFBList::~LFBList()
{
	delete fragCount;
	delete counts;
	delete headPtrs;
	delete nextPtrs;
	delete data;

	fragCount = nullptr;
	counts = nullptr;
	headPtrs = nullptr;
	nextPtrs = nullptr;
	data = nullptr;

	zeroShader.release();
}

void LFBList::overrideDefault()
{
	createLfbSrc("lfb");
}

void LFBList::setBmaUniforms(Shader *shader)
{
	//shader->setUniform("Counts" + name, counts);
	//shader->setUniform("size" + name, Math::ivec2(width, height));
	setUniforms(shader, false);
}

void LFBList::setUniforms(Shader *shader, bool fragAlloc, std::string nameOverride)
{
	if (nameOverride == "")
		nameOverride = name;

	if (fragAlloc)
		shader->setUniform("fragCount", fragCount);
	shader->setUniform("Counts" + nameOverride, counts);
	shader->setUniform("HeadPtrs" + nameOverride, headPtrs);
	shader->setUniform("NextPtrs" + nameOverride, nextPtrs);
	shader->setUniform("Data" + nameOverride, data);
	shader->setUniform("size" + nameOverride, Math::ivec2(width, height));
	if (fragAlloc)
		shader->setUniform("fragAlloc" + nameOverride, this->fragAlloc);
}

void LFBList::setCountUniforms(Shader *shader)
{
	shader->setUniform("Counts", counts);
	shader->setUniform("size", Math::ivec2(width, height));
}

void LFBList::resize(int width, int height)
{
	if ((int) this->width == width && (int) this->height == height)
		return;

	this->width = width;
	this->height = height;

	if (!fragCount->isGenerated())
	{
		// Have to start at one since zero is being used as null.
		unsigned int one = 1;
		fragCount->create(&one, sizeof(one));
	}

	// For now just allocate 5 frags per pixel for the lfb, this will dynamically resize later.
	if (fragAlloc == 0)
		fragAlloc = width * height * 5;

	// Always one count per pixel.
	size_t countSize = width * height * sizeof(unsigned int);
	counts->create(nullptr, countSize);

	// Always buffer the head ptrs, since you always need one per pixel.
	size_t headPtrSize = width * height * sizeof(unsigned int);
	headPtrs->create(nullptr, headPtrSize);

	// No need to buffer the nextPtrs or data if they've already been created, if more are needed endCapture() will handle it.
	size_t nextPtrSize = fragAlloc * sizeof(unsigned int);
	if (!nextPtrs->isGenerated())
		nextPtrs->create(nullptr, nextPtrSize);

	size_t dataSize = fragAlloc * sizeof(float) * getFragSize();
	if (!data->isGenerated())
		data->create(nullptr, dataSize);

	// If we're using a profiler, set memory buffer sizes.
	if (profiler)
	{
		profiler->setBufferSize(name + "Counts", countSize);
		profiler->setBufferSize(name + "HeadPtrs", headPtrSize);
		profiler->setBufferSize(name + "NextPtrs", nextPtrSize);
		profiler->setBufferSize(name + "Data", dataSize);
		profiler->setBufferSize(name + "Total", countSize + headPtrSize + nextPtrSize + dataSize);
	}
}

void LFBList::zeroBuffers()
{
	if (!zeroShader.isGenerated())
	{
	#if LIST_STORAGE_BUFFERS
		auto zeroSrc = ShaderSource("zeroLFB_LL", getZeroShaderSrc());
	#else
		auto zeroSrc = ShaderSource("zeroLFB_LL", getZeroImgShaderSrc());
	#endif
		zeroShader.create(&zeroSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("Counts", counts);
	zeroShader.setUniform("HeadPtrs", headPtrs);
	glDrawArrays(GL_POINTS, 0, width * height);
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);
	
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
}

Shader &LFBList::beginCountFrags(int indexByTile)
{
	// This is only used by the linearised lfb, don't bother counting frags for link lists.
	(void) (indexByTile);
	return emptyShader;
}

void LFBList::endCountFrags()
{
	// Again, only used by the linearised lfb.
}

void LFBList::beginCapture()
{
#if !LIST_STORAGE_BUFFERS
	counts->bind();
	data->bind();
	headPtrs->bind();
	nextPtrs->bind();
#endif

	zeroBuffers();
}

bool LFBList::endCapture()
{
	// FIXME: this may lead to too many re-draws...
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

#if !LIST_STORAGE_BUFFERS
	counts->unbind();
	data->unbind();
	headPtrs->unbind();
	nextPtrs->unbind();
#endif

	// It's possible that not enough (or far too many) frags were allocated, if so, reallocate.
	unsigned int totalFrags = *((unsigned int*) fragCount->read());

	bool resizeFlag = false;
#if LFB_EXACT_ALLOC
	if (totalFrags != fragAlloc)
	{
		fragAlloc = (unsigned int) totalFrags;
		resizeFlag = true;
	}
#else
	if (totalFrags > fragAlloc || totalFrags < fragAlloc * LFB_UNDERALLOCATE)
	{
		fragAlloc = (unsigned int) (totalFrags * LFB_OVERALLOCATE);
		resizeFlag = true;
	}
#endif

	if (resizeFlag)
	{
		size_t nextPtrSize = fragAlloc * sizeof(unsigned int);
		size_t dataSize = fragAlloc * sizeof(float) * getFragSize();

		nextPtrs->bufferData(nullptr, nextPtrSize);
		data->bufferData(nullptr, dataSize);

		if (profiler)
		{
			size_t countSize = width * height * sizeof(unsigned int);
			size_t headPtrSize = width * height * sizeof(unsigned int);
			profiler->setBufferSize(name + "NextPtrs", nextPtrSize);
			profiler->setBufferSize(name + "Data", dataSize);
			profiler->setBufferSize(name + "Total", countSize + headPtrSize + nextPtrSize + dataSize);
		}
	}

	totalFrags = 1;
	fragCount->bufferData(&totalFrags, sizeof(totalFrags));

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	return resizeFlag;
}

void LFBList::beginComposite()
{
	// Here in case we wrote to global memory during a previous sort.
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

#if !LIST_STORAGE_BUFFERS
	counts->bind();
	data->bind();
	headPtrs->bind();
	nextPtrs->bind();
#endif
}

void LFBList::endComposite()
{
#if !LIST_STORAGE_BUFFERS
	counts->unbind();
	data->unbind();
	headPtrs->unbind();
	nextPtrs->unbind();
#endif

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
}

std::string LFBList::getStr(bool ignoreEmpty)
{
	switch (dataType)
	{
	case LFB::UINT:
		return getStrTempl<unsigned int>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::INT:
		return getStrTempl<int>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::FLOAT:
		return getStrTempl<float>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC2:
		return getStrTempl<Math::vec2>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC2:
		return getStrTempl<Math::ivec2>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC2:
		return getStrTempl<Math::uvec2>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC4:
		return getStrTempl<Math::vec4>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC4:
		return getStrTempl<Math::ivec4>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC4:
		return getStrTempl<Math::uvec4>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	default:
		break;
	}

	return "";
}

std::string LFBList::getStrSorted(bool ignoreEmpty)
{
	switch (dataType)
	{
	case LFB::UINT:
		return getStrSortedTempl<unsigned int>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::INT:
		return getStrSortedTempl<int>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::FLOAT:
		return getStrSortedTempl<float>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC2:
		return getStrSortedTempl<Math::vec2>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC2:
		return getStrSortedTempl<Math::ivec2>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC2:
		return getStrSortedTempl<Math::uvec2>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::VEC4:
		return getStrSortedTempl<Math::vec4>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::IVEC4:
		return getStrSortedTempl<Math::ivec4>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	case LFB::UVEC4:
		return getStrSortedTempl<Math::uvec4>(headPtrs, nextPtrs, counts, data, width, height, ignoreEmpty);
		break;
	default:
		break;
	}

	return "";
}

std::string LFBList::getBinary()
{
	switch (dataType)
	{
	case LFB::UINT:
		return getBinaryTempl<unsigned int>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	case LFB::INT:
		return getBinaryTempl<int>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	case LFB::FLOAT:
		return getBinaryTempl<float>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	case LFB::VEC2:
		return getBinaryTempl<Math::vec2>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	case LFB::IVEC2:
		return getBinaryTempl<Math::ivec2>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	case LFB::UVEC2:
		return getBinaryTempl<Math::uvec2>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	case LFB::VEC4:
		return getBinaryTempl<Math::vec4>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	case LFB::IVEC4:
		return getBinaryTempl<Math::ivec4>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	case LFB::UVEC4:
		return getBinaryTempl<Math::uvec4>(headPtrs, nextPtrs, counts, data, width, height);
		break;
	default:
		break;
	}

	return "";
}

std::string LFBList::getSVG()
{
	// TODO: Implement me!
	return "";
}

