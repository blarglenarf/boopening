#include "ClusterMask.h"

#include "../Renderer.h"
#include "../Camera.h"
#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Texture.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"

#include "../../../Math/src/MathCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <sstream>
#include <bitset>
#include <cassert>

using namespace Rendering;


static std::string getZeroSrc()
{
	return "\
	#version 450\n\
	\
	buffer ClusterMasks\
	{\
		uint clusterMasks[];\
	};\
	\
	void main()\
	{\
		clusterMasks[gl_VertexID] = 0;\
	}\
	";
}

static std::string getZeroDepthSrc()
{
	return "\
	#version 450\n\
	\
	buffer ClusterMasks\
	{\
		uvec2 clusterMasks[];\
	};\
	\
	void main()\
	{\
		clusterMasks[gl_VertexID].x = 1024;\
		clusterMasks[gl_VertexID].y = 0;\
	}\
	";
}



ClusterMask::ClusterMask(int clusters, float maxDepth, int gridCellSize) : width(0), height(0), prePassWidth(0), prePassHeight(0), clusters(clusters), gridCellSize(gridCellSize), maxDepth(maxDepth), prePassFlag(false), saveIndexFlag(false), prefixSumSize(0), compactIndexSize(0), profiler(nullptr)
{
	maskSize = clusters / 32;
	masks = new StorageBuffer();
	maskDepths = new StorageBuffer();

	clusterBlockSize = 32;

	depthTexture = new Texture2D();
	depthBuffer = new RenderBuffer();
	depthFbo = new FrameBuffer();

	indexBuffer = new StorageBuffer();
	offsetsBuffer = new StorageBuffer();
	compactIndexBuffer = new StorageBuffer();
}

ClusterMask::~ClusterMask()
{
	zeroMaskShader.release();
	createMaskShader.release();

	delete masks;
	masks = nullptr;

	delete maskDepths;
	maskDepths = nullptr;

	delete depthTexture;
	delete depthBuffer;
	delete depthFbo;
	depthTexture = nullptr;
	depthBuffer = nullptr;
	depthFbo = nullptr;

	delete indexBuffer;
	delete offsetsBuffer;
	delete compactIndexBuffer;
	indexBuffer = nullptr;
	offsetsBuffer = nullptr;
	compactIndexBuffer = nullptr;
}

void ClusterMask::regen(int clusters, float maxDepth, int gridCellSize, bool regenShader)
{
	this->clusters = clusters;
	this->maxDepth = maxDepth;
	this->gridCellSize = gridCellSize;

	maskSize = clusters / 32;

	if (regenShader)
	{
		createMaskShader.release();

		auto vertMask = ShaderSourceCache::getShader("clusterMaskVert").loadFromFile("shaders/lfb/createMask.vert");
		auto fragMask = ShaderSourceCache::getShader("clusterMaskFrag").loadFromFile("shaders/lfb/createMask.frag");
		fragMask.setDefine("GRID_CELL_SIZE", Utils::toString(gridCellSize));
		fragMask.setDefine("CLUSTERS", Utils::toString(clusters));
		fragMask.setDefine("MASK_SIZE", Utils::toString(maskSize));
		fragMask.setDefine("MAX_EYE_Z", Utils::toString(maxDepth));
		fragMask.setDefine("USE_DEPTH_TEXTURE", "0");
		vertMask.setDefine("USE_DEPTH_TEXTURE", "0");
		if (saveIndexFlag)
			fragMask.setDefine("SAVE_INDICES", "1");

		createMaskShader.create(&vertMask, &fragMask);

		depthPrePassMaskShader.release();

		auto vertPreMask = ShaderSourceCache::getShader("clusterPreMaskVert").loadFromFile("shaders/lfb/createMask.vert");
		auto fragPreMask = ShaderSourceCache::getShader("clusterPreMaskFrag").loadFromFile("shaders/lfb/createMask.frag");
		fragPreMask.setDefine("GRID_CELL_SIZE", Utils::toString(gridCellSize));
		fragPreMask.setDefine("CLUSTERS", Utils::toString(clusters));
		fragPreMask.setDefine("MASK_SIZE", Utils::toString(maskSize));
		fragPreMask.setDefine("MAX_EYE_Z", Utils::toString(maxDepth));
		fragPreMask.setDefine("USE_DEPTH_TEXTURE", "1");
		vertPreMask.setDefine("USE_DEPTH_TEXTURE", "1");
		if (saveIndexFlag)
			fragPreMask.setDefine("SAVE_INDICES", "1");

		depthPrePassMaskShader.create(&vertPreMask, &fragPreMask);
	}
}

void ClusterMask::resize(int width, int height)
{
	if (this->width == width && this->height == height)
		return;

	this->width = width;
	this->height = height;

	masks->create(nullptr, width * height * sizeof(unsigned int) * maskSize);
	maskDepths->create(nullptr, width * height * clusters * sizeof(unsigned int) * 2);

	if (saveIndexFlag)
	{
		indexBuffer->create(nullptr, width * height * clusters * sizeof(unsigned int));
		prefixSumSize = Math::nextPower2(((width * height * clusters) / clusterBlockSize) + 1);
		offsetsBuffer->create(nullptr, (prefixSumSize + 1) * sizeof(unsigned int));
	}

	if (profiler)
	{
		profiler->setBufferSize("clusterMasks", width * height * sizeof(unsigned int) * maskSize);
		profiler->setBufferSize("clusterMaskDepths", width * height * clusters * sizeof(unsigned int) * 2);

		if (saveIndexFlag)
		{
			profiler->setBufferSize("clusterIndices", width * height * clusters * sizeof(unsigned int));
			profiler->setBufferSize("clusterOffsets", (prefixSumSize + 1) * sizeof(unsigned int));
		}
	}
}

void ClusterMask::setUniforms(Shader *shader, const std::string &name)
{
	shader->setUniform(name, masks);
	shader->setUniform(name + "Depths", maskDepths);
}

void ClusterMask::zero()
{
	// Zero the cluster masks.
	if (!zeroMaskShader.isGenerated())
	{
		auto zeroSrc = ShaderSource("zeroMask", getZeroSrc());
		zeroMaskShader.create(&zeroSrc);
	}
	if (!zeroMaskDepthShader.isGenerated())
	{
		auto zeroSrc = ShaderSource("zeroMaskDepth", getZeroDepthSrc());
		zeroMaskDepthShader.create(&zeroSrc);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// One thread per mask.
	glEnable(GL_RASTERIZER_DISCARD);
	zeroMaskShader.bind();
	zeroMaskShader.setUniform("ClusterMasks", masks);
	glDrawArrays(GL_POINTS, 0, width * height * maskSize);

	if (saveIndexFlag)
	{
		zeroMaskShader.setUniform("ClusterMasks", indexBuffer);
		glDrawArrays(GL_POINTS, 0, width * height * clusters);
		zeroMaskShader.setUniform("ClusterMasks", offsetsBuffer);
		glDrawArrays(GL_POINTS, 0, prefixSumSize + 1);
	}

	zeroMaskShader.unbind();

	zeroMaskDepthShader.bind();
	zeroMaskDepthShader.setUniform("ClusterMasks", maskDepths);
	glDrawArrays(GL_POINTS, 0, width * height * clusters);
	zeroMaskDepthShader.unbind();

	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void ClusterMask::compactClusterIndices()
{
	// TODO: Cluster compaction actually isn't that slow, but could potentially be made faster.
	//if (profiler)
	//	profiler->start("Cluster compact");

	if (!countIndicesShader.isGenerated())
	{
		auto countSrc = ShaderSourceCache::getShader("countIndices").loadFromFile("shaders/lfb/countMaskIndices.vert");
		countSrc.setDefine("CLUSTER_BLOCK_SIZE", Utils::toString(clusterBlockSize));
		countIndicesShader.create(&countSrc);
	}

	if (!compactIndicesShader.isGenerated())
	{
		auto compactSrc = ShaderSourceCache::getShader("compactIndices").loadFromFile("shaders/lfb/compactMaskIndices.vert");
		compactIndicesShader.create(&compactSrc);
	}

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Divide the cluster indices into blocks, one thread for each block, and write total occupied cluster count for each block into cluster offsets.
	countIndicesShader.bind();
	countIndicesShader.setUniform("Indices", indexBuffer);
	countIndicesShader.setUniform("Offsets", offsetsBuffer);
	glDrawArrays(GL_POINTS, 0, (width * height * clusters) / clusterBlockSize);
	countIndicesShader.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	//if (profiler)
	//	profiler->start("Cluster prefix");

	// Prefix sums on that buffer to get offsets to write data to.
	prefixClusterIndices();

	//if (profiler)
	//	profiler->time("Cluster prefix");

	// Write data in blocks, one thread for each block, based on offsets. We now have a buffer of occupied cluster indices.
	compactIndicesShader.bind();
	compactIndicesShader.setUniform("Indices", indexBuffer);
	compactIndicesShader.setUniform("Offsets", offsetsBuffer);
	compactIndicesShader.setUniform("CompactIndices", compactIndexBuffer);
	glDrawArrays(GL_POINTS, 0, (width * height * clusters) / clusterBlockSize);
	compactIndicesShader.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// We now have an in-order buffer containing the indices of occupied clusters only.

	glPopAttrib();

	//if (profiler)
	//	profiler->time("Cluster compact");
}

void ClusterMask::prefixClusterIndices()
{
	if (!prefixIndicesShader.isGenerated())
	{
		auto prefixSrc = ShaderSourceCache::getShader("prefixIndices").loadFromFile("shaders/lfb/prefix.vert");
		prefixSrc.setDefine("STORAGE_BUFFERS", Utils::toString(1));
		prefixIndicesShader.create(&prefixSrc);
	}

	unsigned int sumSize = prefixSumSize;
	assert(sumSize > 0);

	unsigned int positionOfTotal = sumSize - 1;

	prefixIndicesShader.bind();
	prefixIndicesShader.setUniform("Offsets", offsetsBuffer);

	prefixIndicesShader.setUniform("pass", 0);
	for (unsigned int step = 2; step <= sumSize; step <<= 1)
	{
		prefixIndicesShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	// For saving the total sum of clusters.
	TextureBuffer justOneInt(GL_R32UI);
	justOneInt.create(nullptr, sizeof(unsigned int));
	offsetsBuffer->copy(&justOneInt, positionOfTotal * sizeof(unsigned int), 0, sizeof(unsigned int));

	// Save total sum of clusters at the last location (past all the prefix sums).
	offsetsBuffer->copy(offsetsBuffer, positionOfTotal * sizeof(unsigned int), (positionOfTotal + 1) * sizeof(unsigned int), sizeof(unsigned int));

	// Value at positionOfTotal must be zero for next pass to correctly calculate prefix sums.
	unsigned int zero = 0;
	offsetsBuffer->bufferSubData(&zero, positionOfTotal * sizeof(unsigned int), sizeof(unsigned int));

	prefixIndicesShader.setUniform("pass", 1);
	for (unsigned int step = sumSize; step >= 2; step >>= 1)
	{
		prefixIndicesShader.setUniform("stepSize", step);
		int kernelSize = sumSize / step;
		glDrawArrays(GL_POINTS, 0, kernelSize);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	prefixIndicesShader.unbind();

	auto prefixSumsTotal = *((unsigned int*) justOneInt.read());

	// Only realloc if the size is different.
	if (prefixSumsTotal != compactIndexSize)
	{
		compactIndexSize = prefixSumsTotal;
		compactIndexBuffer->create(nullptr, compactIndexSize * sizeof(unsigned int));

		//if (profiler)
		//	profiler->setBufferSize("lfbData", fragAlloc * sizeof(unsigned int));
	}
}

void ClusterMask::beginDepthPrePass()
{
	if (!prePassFlag)
		return;

	auto &camera = Renderer::instance().getActiveCamera();
	if (camera.getWidth() != prePassWidth || camera.getHeight() != prePassHeight)
	{
		prePassWidth = camera.getWidth();
		prePassHeight = camera.getHeight();

		depthTexture->release();
		depthBuffer->release();
		depthFbo->release();

		depthFbo->setSize(prePassWidth, prePassHeight);
		depthFbo->create(GL_R32F, depthTexture, depthBuffer);

		depthFbo->bind();
		depthFbo->setDrawBuffers();
		depthFbo->unbind();

		size_t depthSize = prePassWidth * prePassHeight * 4 * 1; // 4 bytes per channel, 1 channel per pixel.

		if (profiler)
			profiler->setBufferSize("depthTexture", depthSize);
	}

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	depthFbo->bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	depthPrePassShader.bind();
	camera.setUniforms(&depthPrePassShader);
}

void ClusterMask::endDepthPrePass()
{
	if (!prePassFlag)
		return;

	depthPrePassShader.unbind();

	depthFbo->unbind();

	glPopAttrib();
}

void ClusterMask::beginCreateMask()
{
	// Load create shader from shaders/lfb/createMask.vert/.frag if necessary.
	if (!createMaskShader.isGenerated())
	{
		auto vertMask = ShaderSourceCache::getShader("clusterMaskVert").loadFromFile("shaders/lfb/createMask.vert");
		auto fragMask = ShaderSourceCache::getShader("clusterMaskFrag").loadFromFile("shaders/lfb/createMask.frag");
		fragMask.setDefine("GRID_CELL_SIZE", Utils::toString(gridCellSize));
		fragMask.setDefine("CLUSTERS", Utils::toString(clusters));
		fragMask.setDefine("MASK_SIZE", Utils::toString(maskSize));
		fragMask.setDefine("MAX_EYE_Z", Utils::toString(maxDepth));
		fragMask.setDefine("USE_DEPTH_TEXTURE", "0");
		vertMask.setDefine("USE_DEPTH_TEXTURE", "0");
		if (saveIndexFlag)
			fragMask.setDefine("SAVE_INDICES", "1");

		createMaskShader.create(&vertMask, &fragMask);
	}

	if (!depthPrePassShader.isGenerated())
	{
		auto vertDepth = ShaderSourceCache::getShader("clusterMaskDepthVert").loadFromFile("shaders/lfb/depthPrePass.vert");
		auto fragDepth = ShaderSourceCache::getShader("clusterMaskDepthFrag").loadFromFile("shaders/lfb/depthPrePass.frag");
		depthPrePassShader.create(&vertDepth, &fragDepth);
	}

	if (!depthPrePassMaskShader.isGenerated())
	{
		auto vertMask = ShaderSourceCache::getShader("clusterPreMaskVert").loadFromFile("shaders/lfb/createMask.vert");
		auto fragMask = ShaderSourceCache::getShader("clusterPreMaskFrag").loadFromFile("shaders/lfb/createMask.frag");
		fragMask.setDefine("GRID_CELL_SIZE", Utils::toString(gridCellSize));
		fragMask.setDefine("CLUSTERS", Utils::toString(clusters));
		fragMask.setDefine("MASK_SIZE", Utils::toString(maskSize));
		fragMask.setDefine("MAX_EYE_Z", Utils::toString(maxDepth));
		fragMask.setDefine("USE_DEPTH_TEXTURE", "1");
		vertMask.setDefine("USE_DEPTH_TEXTURE", "1");
		if (saveIndexFlag)
			fragMask.setDefine("SAVE_INDICES", "1");

		depthPrePassMaskShader.create(&vertMask, &fragMask);
	}

	// Have to render high-res geometry I guess :(
	// TODO: Not necessarily, can render at low-res using conservative rasterization and conservative depth.

	// If we're performing a depth pre-pass, then do that, otherwise render geometry and populate the cluster masks.
	if (prePassFlag)
		beginDepthPrePass();
	else
	{
		auto &camera = Renderer::instance().getActiveCamera();
		zero();

		createMaskShader.bind();

		createMaskShader.setUniform("mvMatrix", camera.getInverse());
		createMaskShader.setUniform("pMatrix", camera.getProjection());
		createMaskShader.setUniform("size", Math::ivec2(width, height));
		createMaskShader.setUniform("ClusterMasks", masks);
		//createMaskShader.setUniform("ClusterMasksDepths", maskDepths);

		if (saveIndexFlag)
			createMaskShader.setUniform("ClusterIndices", indexBuffer);
	}
}

void ClusterMask::endCreateMask()
{
	// If we're performing a depth pre-pass, then create cluster masks using the depth texture, otherwise, not much to do.
	if (prePassFlag)
	{
		endDepthPrePass();
		createMaskFromDepths(depthTexture);
	}
	else
	{
		createMaskShader.unbind();
		if (saveIndexFlag)
			compactClusterIndices();
	}
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void ClusterMask::createMaskFromDepths(Texture2D *depths)
{
	if (!depthPrePassMaskShader.isGenerated())
	{
		auto vertMask = ShaderSourceCache::getShader("clusterPreMaskVert").loadFromFile("shaders/lfb/createMask.vert");
		auto fragMask = ShaderSourceCache::getShader("clusterPreMaskFrag").loadFromFile("shaders/lfb/createMask.frag");
		fragMask.setDefine("GRID_CELL_SIZE", Utils::toString(gridCellSize));
		fragMask.setDefine("CLUSTERS", Utils::toString(clusters));
		fragMask.setDefine("MASK_SIZE", Utils::toString(maskSize));
		fragMask.setDefine("MAX_EYE_Z", Utils::toString(maxDepth));
		fragMask.setDefine("USE_DEPTH_TEXTURE", "1");
		vertMask.setDefine("USE_DEPTH_TEXTURE", "1");
		if (saveIndexFlag)
			fragMask.setDefine("SAVE_INDICES", "1");

		depthPrePassMaskShader.create(&vertMask, &fragMask);
	}

	zero();

	depthPrePassMaskShader.bind();
	depths->bind();

	depthPrePassMaskShader.setUniform("size", Math::ivec2(width, height));
	depthPrePassMaskShader.setUniform("ClusterMasks", masks);
	//depthPrePassMaskShader.setUniform("ClusterMasksDepths", maskDepths);
	depthPrePassMaskShader.setUniform("depthTexture", depths);

	if (saveIndexFlag)
		depthPrePassMaskShader.setUniform("ClusterIndices", indexBuffer);

	Renderer::instance().drawQuad();

	depths->unbind();
	depthPrePassMaskShader.unbind();

	if (saveIndexFlag)
		compactClusterIndices();
}

std::string ClusterMask::getStr()
{
	unsigned int *maskData = (unsigned int*) masks->read();
	Math::uvec2 *maskDepthData = (Math::uvec2*) maskDepths->read();
	std::stringstream maskStr;

	(void) (maskData);
	(void) (maskDepthData);

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			maskStr << x << ", " << y << "; ";
		#if 1
			for (int c = 0; c < clusters; c++)
			{
				size_t index = y * width * clusters + x * clusters + c;
				if (maskDepthData[index].x == 1024 && maskDepthData[index].y == 0)
					continue;
				maskStr << c << ": (" << maskDepthData[index].x << "," << maskDepthData[index].y << "), ";
			}
		#else
			size_t index = y * width + x;

			for (int i = 0; i < maskSize; i++)
				maskStr << std::bitset<32>(maskData[index + i]) << " ";
		#endif
			maskStr << "\n";
		}
	}

	return maskStr.str();
}

