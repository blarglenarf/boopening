#include "GL.h"
#include "../../Utils/src/StringUtils.h"

#include <string>
#include <iostream>
#include <map>

using namespace Rendering;

static Utils::StringHash glErrorCache;

void Rendering::logGLError(std::string msg)
{
	if (!glErrorCache.existsStr(msg))
	{
		glErrorCache.addStr(msg);
		std::cout << msg << "\n";
	}
}

bool Rendering::checkErrors(const char* filename, int line, bool silent)
{
	static std::map<GLenum, std::string> glErrors;
	static bool initFlag = false;

	if (!initFlag)
	{
		glErrors[GL_INVALID_ENUM] = "GL_INVALID_ENUM";
		glErrors[GL_INVALID_VALUE] = "GL_INVALID_VALUE";
		glErrors[GL_INVALID_OPERATION] = "GL_INVALID_OPERATION";
		glErrors[GL_NO_ERROR] = "GL_NO_ERROR";
		glErrors[GL_STACK_OVERFLOW] = "GL_STACK_OVERFLOW";
		glErrors[GL_STACK_UNDERFLOW] = "GL_STACK_UNDERFLOW";
		glErrors[GL_OUT_OF_MEMORY] = "GL_OUT_OF_MEMORY";
		initFlag = true;
	}

	bool ok = true;
    GLenum error, last;

	//get all errors
	error = glGetError();
    while (error != GL_NO_ERROR)
    {
		const char* p = filename;
		while (*p != '\\' && *p != '\0')
			++p;
		if (*p == '\\')
			filename = p+1; //print only filename past the last "\"

		//print error
    	ok = false;
    	if (!silent)
			logGLError("glError " + Utils::toString((unsigned int) error) + " caught in " + std::string(filename) + " at " + Utils::toString(line) + ": " + glErrors[error] + "\n");

		//to stop an infinite loop
		last = error;
		error = glGetError();
		if (error == last)
		{
			std::cout << "GL error in error function. Have you initialized opengl?\n";
			return false;
		}
    }
    return !ok;
}

#if _WIN32
	void *getGLFuncAddr(const char *name)
	{
		void *p = (void *) wglGetProcAddress(name);
		if (p == 0 || (p == (void*) 0x1) || (p == (void*) 0x2) || (p == (void*) 0x3) || (p == (void*) -1))
		{
			HMODULE module = LoadLibraryA("opengl32.dll");
			p = (void*) GetProcAddress(module, name);
		}

		return p;
	}
#endif

void printExtensionList()
{
	auto *str = glGetString(GL_EXTENSIONS);
	size_t loc = 0;
	while (str[loc] != '\0')
	{
		if (str[loc] == ' ')
			std::cout << "\n";
		else
			std::cout << str[loc];
		loc++;
	}
}

