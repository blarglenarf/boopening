#pragma once

#include "../../Math/src/MathCommon.h"

#include <iostream>

// Fuck windows annoys me sometimes.
#undef NEAR
#undef FAR
#undef OUT
#undef IN

namespace Rendering
{
	class Shader;

	class Camera
	{
	private:
		struct Frustum
		{
			friend class Camera;

			enum Plane
			{
				NEAR,
				FAR,
				TOP,
				BOTTOM,
				LEFT,
				RIGHT
			};

			Math::vec4 planes[6];

			void calcFrustum(Camera *camera);
		};

	public:
		enum class Type
		{
			NORMAL,
			ORTHO,
			PERSP
		};

		enum FrustumCollision
		{
			OUT,
			IN,
			INTERSECT
		};

	private:
		Frustum frustum;

		float fov;
		float nearPlane;
		float farPlane;

		Math::ivec4 viewport;
		Math::vec4 orthoCoords;

		Math::mat4 proj;
		Math::mat4 invProj;

		Math::mat4 transform;
		Math::mat4 inverse;

		Math::vec3 pos;
		float zoom;

		Math::vec3 eulerRot;
		Math::Quat quatRot;

		float sensitivity;

		Math::vec3 forward;
		Math::vec3 up;
		Math::vec3 right;

		bool calcProjFlag;
		bool calcTransFlag;
		bool orthoCoordFlag;

		bool gimbalLockFlag;

		Type type;

	public:
		Camera(Type type = Type::NORMAL, int x = 0, int y = 0, int width = 512, int height = 512, float near = 0.01f, float far = 30.0f, float sensitivity = 0.5f);
		//Camera(Resources::XmlLibrary::XmlData &data);
		~Camera();

		void calcProjection();
		void calcTransform();

		void reset();

		void update(float dt);

		void upload() const;
		void upload(const Math::mat4 &proj, const Math::mat4 &mv) const;
		void uploadViewport() const;

		void setUniforms(Shader *shader);

		void setType(Type type) { this->type = type; calcProjFlag = true; }
		Type getType() const { return type; }

		void setFov(float fov) { this->fov = fov; calcProjFlag = true; }
		void setDistance(float nearPlane, float farPlane) { this->nearPlane = nearPlane; this->farPlane = farPlane; calcProjFlag = true; }
		void setViewport(int x, int y, int width, int height);
		void setOrtho(float left, float right, float bottom, float top);

		void setPos(const Math::vec3 &pos) { this->pos = pos; calcTransFlag = true; }
		void setQuatRot(const Math::Quat &rot);
		void setEulerRot(const Math::vec3 &rot);
		void setViewDir(const Math::vec3 &forward);
		void setZoom(float zoom) { this->zoom = zoom; calcTransFlag = true; }

		void setGimbalLock(bool flag) { gimbalLockFlag = flag; }
		void setDirty() { calcProjFlag = true; calcTransFlag = true; }

		Math::ivec4 getViewport() const { return viewport; }
		Math::mat4 getProjection() const { return proj; }
		Math::mat4 getInverseProj() const { return invProj; }

		Math::mat4 getTransform() const { return transform; }
		Math::mat4 getInverse() const { return inverse; }

		int getWidth() const { return viewport.z; }
		int getHeight() const { return viewport.w; }
		float getFov() const { return fov; }
		float getNear() const { return nearPlane; }
		float getFar() const { return farPlane; }

		const Math::vec3 &getPos() const { return pos; }
		const Math::vec3 &getEulerRot() const { return eulerRot; }
		const Math::Quat &getQuatRot() const { return quatRot; }
		float getZoom() const { return zoom; }
		float &getZoom() { return zoom; } // Needed for camera interpolation.

		const Math::vec3 &getForward() const { return forward; }
		const Math::vec3 &getUp() const { return up; }
		const Math::vec3 &getRight() const { return right; }

		bool isActive() const { return true; }

		void updateRotation(float dx, float dy);
		void updateZoom(float dy);
		void updatePan(float dx, float dy);

		void moveRight(float dt);
		void moveLeft(float dt);
		void moveForward(float dt);
		void moveBackward(float dt);
		void moveUp(float dt);
		void moveDown(float dt);
		// TODO: Maybe something to rotate the view?

		FrustumCollision isInFrustum(const Math::vec3 &pos, float radius);
	};
}

std::ostream &operator << (std::ostream &out, const Rendering::Camera &camera);

