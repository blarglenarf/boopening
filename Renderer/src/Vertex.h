#pragma once

#include "../../Math/src/Vector/VectorCommon.h"
#include "GL.h"

#include <vector>

namespace Rendering
{
	struct Vertex
	{
		Math::vec3 pos;
		Math::vec3 norm;
		Math::vec2 tex;
		Math::vec3 tan;
	};

	struct VertexAttrib
	{
		enum Type
		{
			POSITION,
			NORMAL,
			TEXCOORD,
			TANGENT,
			BINORMAL,
			COLOR,
			OTHER
		};

		Type type;
		unsigned int bindPoint;
		unsigned int nVals;
		GLenum dataType;
	};

	struct VertexFormat
	{
		std::vector<VertexAttrib> attribs;

		VertexFormat(const std::vector<VertexAttrib> &attribs) : attribs(attribs) {}
	};
}

std::ostream &operator << (std::ostream &ostr, const Rendering::Vertex &v);

