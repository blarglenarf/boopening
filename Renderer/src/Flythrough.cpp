#include "Flythrough.h"
#include "Profiler.h"
#include "RenderResource/Image.h"
#include "../../Resources/src/Image/ImageLoader.h"
#include "../../Utils/src/StringUtils.h"
#include "../../Utils/src/File.h"

using namespace Rendering;

// TODO: Tie all this into the profiler/benchmark stuff so we can get benchmarks for an animation.

static void saveScreenshot(Image &img)
{
	img.flip();
	Resources::ImageLoader::save(&img, img.getName());
}



void Interpolator::addKeyFrame(float time, Math::vec3 val)
{
	keyFrames.push_back(KeyFrame(time, val));
}

void Interpolator::reset()
{
	currTime = 0;
	*val = initialVal;
}

bool Interpolator::update(float dt)
{
	if (currTime > maxTime)
		return true;

	int currKeyFrame = 0;
	currTime += dt;

	for (auto &keyFrame : keyFrames)
	{
		if (keyFrame.time >= currTime)
			break;
		currKeyFrame++;
	}

	KeyFrame &kf0 = keyFrames[currKeyFrame - 1];
	KeyFrame &kf1 = keyFrames[currKeyFrame];
	//std::cout << "currKeyframe: " << currKeyFrame << "; currTime: " << currTime << "; time: " << kf1.time << "; maxTime: " << maxTime << "\n";
	Math::vec3 currVal = kf0.value + ((kf1.value - kf0.value) * ((currTime - kf0.time) / (kf1.time - kf0.time))); // lerp.

	*val = currVal;

	return false;
}


size_t Flythrough::addInterpolator(Math::vec3 *val, float maxTime, Math::vec3 initialVal)
{
	interpolators.push_back(Interpolator(val, maxTime, initialVal));
	return interpolators.size() - 1;
}

float Flythrough::update(float dt)
{
	if (!animFlag)
		return dt;

	if (animFilename != "")
	{
		if (currExtraFrame < extraFrames)
		{
			currExtraFrame++;
			return 0;
		}

		currExtraFrame = 0;

		// Save the frame to a file.
		std::string ext = Utils::getExtension(animFilename);
		std::string path = Utils::getFilePath(animFilename);
		std::string basename = Utils::getBaseFilename(animFilename);
		std::string newName = path + basename + Utils::toString(frameNo, 5) + "." + ext;

		Image img(newName);
		img.screenshot();
		saveScreenshot(img);

		// Advance frametime by an amount that lets us keep a smooth 60 fps.
		dt = 16.0f / 1000.0f;

		frameNo++;
	}

	// Advance frametime by an amount that lets us keep a smooth 60 fps.
	if (profiler)
		dt = 16.0f / 1000.0f;

	bool finished = true;
	for (auto &interpolator : interpolators)
	{
		if (!interpolator.update(dt))
			finished = false;
	}

	if (finished)
	{
		animFlag = false;

		for (auto &interpolator : interpolators)
			interpolator.reset();

		if (csvFilename != "")
			writeProfilerCSV();
	}

	if (!profiler)
		return dt;

	// If we're profiling stuff, then we need to record the appropriate times for each individual frame.
	for (auto &it : profileTimes)
	{
		auto &name = it.first;
		float time = profiler->getTime(name);
		it.second.push_back(time);
	}

	return dt;
}

void Flythrough::writeProfilerCSV()
{
	std::stringstream csv;

	// Firstly, what platform are we using?
	csv << (const char*) glGetString(GL_VENDOR) << " " << (const char*) glGetString(GL_RENDERER) << " " << (const char*) glGetString(GL_VERSION) << "\n";

	// Titles for each column.
	csv << "Frame,";
	for (auto &it : profileTimes)
		csv << it.first << " (ms)" << ",";
	csv << "\n";

	// Now just throw out the data, if it needs reformatting to be more readable, that's what python is for.
	size_t curr = 0;
	bool fin = false;
	while (!fin)
	{
		csv << curr << ",";
		for (auto &it : profileTimes)
		{
			if (curr >= it.second.size())
			{
				fin = true;
				break;
			}
			csv << it.second[curr] << ",";
		}
		csv << "\n";
		curr++;
	}

	Utils::File file(csvFilename);
	file.write(csv.str());
}

