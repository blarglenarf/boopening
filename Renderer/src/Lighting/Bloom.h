#pragma once

#include "../RenderObject/Shader.h"

namespace Rendering
{
	class FrameBuffer;
	class RenderBuffer;
	class Texture2D;
	class Profiler;

	class Bloom
	{
	private:
		Shader drawShader;
		Shader blurShader;
		Shader compositeShader;

		FrameBuffer *drawBuffer;
		RenderBuffer *depthBuffer;
		Texture2D *color;
		Texture2D *bright;

		FrameBuffer *blurBuffers[4];
		Texture2D *blurTexes[4];

		int width;
		int height;

		Profiler *profiler;

	public:
		Bloom();
		~Bloom();

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }

		void resize(int width, int height);
		void reloadShaders();

		void beginCapture();
		void endCapture();

		void applyBlur();
		void composite();

		void debugRenderColor();
		void debugRenderBright();
		void debugRenderBlur(int lod = 0);

		Texture2D *getColorTexture() { return color; }
		Texture2D *getBrightTexture() { return bright; }

		int getWidth() const { return width; }
		int getHeight() const { return height; }
	};
}

