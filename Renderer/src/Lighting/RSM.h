#pragma once

#include "../RenderObject/Shader.h"

namespace Rendering
{
	class FrameBuffer;
	class RenderBuffer;
	class Texture2D;
	class Profiler;

	class RSM
	{
	private:
		FrameBuffer *fbo;
		RenderBuffer *depthBuffer;

		Texture2D *dirs;
		Texture2D *colors;
		Texture2D *positions;
		Texture2D *lods;
		Texture2D *normals;

		Shader captureShader;
		Shader drawShader;

		int width;
		int height;

		bool mipmapFlag;

		Profiler *profiler;

	public:
		RSM();
		~RSM();

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }

		void resize(int width, int height);
		void release();

		void beginCapture();
		void endCapture();

		void bindTextures();
		void unbindTextures();
		void setUniforms(Shader *shader);

		void debugRenderDirs();
		void debugRenderColors();
		void debugRenderPositions();
		void debugRenderLods();
		void debugRenderNormals();

		Texture2D *getDirTexture() { return dirs; }
		Texture2D *getColorTexture() { return colors; }
		Texture2D *getPosTexture() { return positions; }
		Texture2D *getLodTexture() { return lods; }
		Texture2D *getNormalTexture() { return normals; }

		Shader &getCaptureShader() { return captureShader; }

		int getWidth() const { return width; }
		int getHeight() const { return height; }
	};
}

