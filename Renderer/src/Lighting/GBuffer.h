#pragma once

#include "../RenderObject/Shader.h"

namespace Rendering
{
	class FrameBuffer;
	class RenderBuffer;
	class Texture2D;
	class Profiler;

	class GBuffer
	{
	private:
		Shader captureShader;
		Shader ambientShader;

		Shader drawDepthShader;
		Shader drawNormShader;

		FrameBuffer *gBuffer;
		RenderBuffer *depthBuffer;
		Texture2D *depth;
		Texture2D *normal;
		Texture2D *material;
		Texture2D *polyID;
		Texture2D *position;

		int width;
		int height;

		bool mipmapFlag;

		Profiler *profiler;

	public:
		GBuffer();
		~GBuffer();

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }

		void resize(int width, int height);
		void release();

		void beginCapture();
		void endCapture();

		void bindTextures();
		void unbindTextures();
		void setUniforms(Shader *shader);

		void applyAmbient();

		void debugRenderDepth();
		void debugRenderNormal();
		void debugRenderMaterial();

		Texture2D *getDepthTexture() { return depth; }
		Texture2D *getNormalTexture() { return normal; }
		Texture2D *getMaterialTexture() { return material; }
		Texture2D *getPolyIDTexture() { return polyID; }
		Texture2D *getPositionTexture() { return position; }

		Shader &getCaptureShader() { return captureShader; }
		Shader &getAmbientShader() { return ambientShader; }

		int getWidth() const { return width; }
		int getHeight() const { return height; }
	};
}

