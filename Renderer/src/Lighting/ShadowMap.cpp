#include "ShadowMap.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Texture.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"
#include "../Renderer.h"

using namespace Rendering;

ShadowMap::ShadowMap() : width(0), height(0), profiler(nullptr)
{
	fbo = new FrameBuffer();
	depthBuffer = new RenderBuffer();
	depthTexture = new Texture2D();
}

ShadowMap::~ShadowMap()
{
	delete fbo;
	delete depthBuffer;
	delete depthTexture;

	fbo = nullptr;
	depthBuffer = nullptr;
	depthTexture = nullptr;
}

void ShadowMap::resize(int width, int height)
{
	if (this->width == width && this->height == height)
		return;

	if (!captureShader.isGenerated())
	{
		auto shadowCapVert = ShaderSourceCache::getShader("captureShadowVert").loadFromFile("shaders/shadow/captureShadow.vert");
		auto shadowCapFrag = ShaderSourceCache::getShader("captureShadowFrag").loadFromFile("shaders/shadow/captureShadow.frag");
		captureShader.release();
		captureShader.create(&shadowCapVert, &shadowCapFrag);
	}
	if (!drawShader.isGenerated())
	{
		auto shadowDrawVert = ShaderSourceCache::getShader("drawShadowVert").loadFromFile("shaders/shadow/drawShadow.vert");
		auto shadowDrawFrag = ShaderSourceCache::getShader("drawShadowFrag").loadFromFile("shaders/shadow/drawShadow.frag");
		drawShader.release();
		drawShader.create(&shadowDrawVert, &shadowDrawFrag);
	}

	this->width = width;
	this->height = height;

	depthTexture->release();
	depthBuffer->release();
	fbo->release();

	// I'm on the fence about wether a sampler2D or sampler2DShadow is better.

	fbo->setSize(width, height);
	//depthTexture->create(nullptr, GL_R32F, width, height);
	depthTexture->create(nullptr, GL_DEPTH_COMPONENT32, width, height); // DEPTH_COMPONENT32 maybe? Does it exist?
	depthTexture->setFilters(GL_CLAMP_TO_EDGE);
	//depthBuffer->create(GL_DEPTH24_STENCIL8, width, height);

	fbo->generate();
	fbo->bind();
	//fbo->attachColorBuffer(depthTexture);
	//fbo->attachDepthBuffer(depthBuffer);
	fbo->attachDepthBuffer(depthTexture);
	fbo->unbind();

	depthTexture->bind();
	depthTexture->useAsShadowMap();
	depthTexture->unbind();

	size_t depthSize = width * height * 2 * 1; // 2 bytes per channel, 1 channel per pixel.

	if (profiler)
		profiler->setBufferSize("shadow-buffer", depthSize);
}

void ShadowMap::release()
{
	fbo->release();
	depthTexture->release();

	captureShader.release();
	drawShader.release();

	width = 0;
	height = 0;
}

void ShadowMap::beginCapture()
{
	if (profiler)
		profiler->start("Capture shadows");

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	fbo->bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void ShadowMap::endCapture()
{
	fbo->unbind();
	glPopAttrib();

	// If you must mipmap them, here's where you do it.

	if (profiler)
		profiler->time("Capture shadows");
}

void ShadowMap::bindTextures()
{
	depthTexture->bind();
}

void ShadowMap::unbindTextures()
{
	depthTexture->unbind();
}

void ShadowMap::setUniforms(Shader *shader)
{
	shader->setUniform("shadowTex", depthTexture);
}

void ShadowMap::debugRender()
{
	depthTexture->bind();

	drawShader.bind();
	drawShader.setUniform("tex", depthTexture);
	Renderer::instance().drawQuad();
	drawShader.unbind();

	depthTexture->unbind();
}

