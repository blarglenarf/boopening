#include "RSM.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Texture.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"
#include "../Renderer.h"

using namespace Rendering;

static void debugRender(Shader *shader, Texture2D *tex)
{
	tex->bind();

	shader->bind();
	shader->setUniform("tex", tex);
	Renderer::instance().drawQuad();
	shader->unbind();

	tex->unbind();
}


RSM::RSM() : width(0), height(0), mipmapFlag(false), profiler(nullptr)
{
	fbo = new FrameBuffer();
	depthBuffer = new RenderBuffer();

	dirs = new Texture2D();
	colors = new Texture2D();
	positions = new Texture2D();
	lods = new Texture2D();
	normals = new Texture2D();
}

RSM::~RSM()
{
	delete fbo;
	delete depthBuffer;
	fbo = nullptr;
	depthBuffer = nullptr;

	delete dirs;
	delete colors;
	delete positions;
	delete lods;
	delete normals;
	dirs = nullptr;
	colors = nullptr;
	positions = nullptr;
	lods = nullptr;
	normals = nullptr;
}

void RSM::resize(int width, int height)
{
	if (this->width == width && this->height == height)
		return;

	if (!captureShader.isGenerated())
	{
		auto rsmCapVert = ShaderSourceCache::getShader("captureRsmVert").loadFromFile("shaders/rsm/captureRSM.vert");
		auto rsmCapFrag = ShaderSourceCache::getShader("captureRsmFrag").loadFromFile("shaders/rsm/captureRSM.frag");
		captureShader.release();
		captureShader.create(&rsmCapVert, &rsmCapFrag);
	}
	if (!drawShader.isGenerated())
	{
		auto rsmDrawVert = ShaderSourceCache::getShader("drawRsmVert").loadFromFile("shaders/rsm/drawRSM.vert");
		auto rsmDrawFrag = ShaderSourceCache::getShader("drawRsmFrag").loadFromFile("shaders/rsm/drawRSM.frag");
		drawShader.release();
		drawShader.create(&rsmDrawVert, &rsmDrawFrag);
	}

	this->width = width;
	this->height = height;

	dirs->release();
	colors->release();
	positions->release();
	lods->release();
	normals->release();

	depthBuffer->release();
	fbo->release();

	fbo->setSize(width, height);
	fbo->create(depthBuffer, true);

	// Create g-buffer textures.
	if (mipmapFlag)
	{
		dirs->create(nullptr, GL_RGB32F, width, height, false);
		colors->create(nullptr, GL_RGB32F, width, height, false);
		positions->create(nullptr, GL_RGB32F, width, height, false);
		lods->create(nullptr, GL_R32F, width, height, false);
		normals->create(nullptr, GL_RGB32F, width, height, false);
	}
	else
	{
		dirs->create(nullptr, GL_RGB32F, width, height);
		colors->create(nullptr, GL_RGB32F, width, height);
		positions->create(nullptr, GL_RGB32F, width, height);
		lods->create(nullptr, GL_R32F, width, height);
		normals->create(nullptr, GL_RGB32F, width, height);
	}

	if (mipmapFlag)
	{
		// TODO: May not need any of this. Not 100% sure.
		dirs->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
		colors->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
		positions->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
		lods->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
		normals->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	}

	fbo->bind();
	fbo->attachColorBuffer(dirs);
	fbo->attachColorBuffer(colors);
	fbo->attachColorBuffer(positions);
	fbo->attachColorBuffer(lods);
	fbo->attachColorBuffer(normals);
	fbo->setDrawBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	fbo->unbind();

	size_t dirSize = width * height * 4 * 3; // 4 bytes per channel, 3 channels per pixel.
	size_t colorSize = width * height * 4 * 3; // 4 bytes per channel, 3 channels per pixel.
	size_t positionSize = width * height * 4 * 3; // 4 bytes per channel, 3 channels per pixel.
	size_t lodSize = width * height * 4 * 1; // 4 bytes per channel, 1 channel per pixel.
	size_t normalSize = width * height * 4 * 3; // 4 bytes per channel, 3 channels per pixel.

	if (profiler)
		profiler->setBufferSize("rsm-buffer", dirSize + colorSize + positionSize + lodSize + normalSize);
}

void RSM::release()
{
	fbo->release();
	depthBuffer->release();

	dirs->release();
	colors->release();
	positions->release();
	lods->release();
	normals->release();

	captureShader.release();
	drawShader.release();

	width = 0;
	height = 0;
}

void RSM::beginCapture()
{
	if (profiler)
		profiler->start("Capture rsm");

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	fbo->bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RSM::endCapture()
{
	fbo->unbind();
	glPopAttrib();

	if (mipmapFlag)
	{
		dirs->genMipmap();
		colors->genMipmap();
		positions->genMipmap();
		lods->genMipmap();
		normals->genMipmap();
	}

	if (profiler)
		profiler->time("Capture rsm");
}

void RSM::bindTextures()
{
	dirs->bind();
	colors->bind();
	positions->bind();
	lods->bind();
	normals->bind();
}

void RSM::unbindTextures()
{
	dirs->unbind();
	colors->unbind();
	positions->unbind();
	lods->unbind();
	normals->unbind();
}

void RSM::setUniforms(Shader *shader)
{
	shader->setUniform("dirTex", dirs);
	shader->setUniform("colorTex", colors);
	shader->setUniform("posTex", positions);
	shader->setUniform("lodTex", lods);
	shader->setUniform("normalTex", normals);
}

void RSM::debugRenderDirs()
{
	debugRender(&drawShader, dirs);
}

void RSM::debugRenderColors()
{
	debugRender(&drawShader, colors);
}

void RSM::debugRenderPositions()
{
	debugRender(&drawShader, positions);
}

void RSM::debugRenderLods()
{
	debugRender(&drawShader, lods);
}

void RSM::debugRenderNormals()
{
	debugRender(&drawShader, normals);
}

