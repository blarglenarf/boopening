#include "GBuffer.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Texture.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"
#include "../Renderer.h"

#define DEPTH_ONLY 1

using namespace Rendering;

GBuffer::GBuffer() : width(0), height(0), mipmapFlag(false), profiler(nullptr)
{
	gBuffer = new FrameBuffer();
	depthBuffer = new RenderBuffer();
	depth = new Texture2D();
	normal = new Texture2D();
	material = new Texture2D();
	polyID = new Texture2D();
	position = new Texture2D();
}

GBuffer::~GBuffer()
{
	delete gBuffer;
	delete depthBuffer;
	delete depth;
	delete normal;
	delete material;
	delete polyID;
	delete position;
	gBuffer = nullptr;
	depthBuffer = nullptr;
	depth = nullptr;
	normal = nullptr;
	material = nullptr;
	polyID = nullptr;
	position = nullptr;
}

void GBuffer::resize(int width, int height)
{
	if (this->width == width && this->height == height)
		return;

	if (!captureShader.isGenerated())
	{
		auto capOpaqueVert = ShaderSourceCache::getShader("capGBufferVert").loadFromFile("shaders/gBuffer/capture.vert");
		auto capOpaqueFrag = ShaderSourceCache::getShader("capGBufferFrag").loadFromFile("shaders/gBuffer/capture.frag");
		capOpaqueFrag.setDefine("DEPTH_ONLY", Utils::toString(DEPTH_ONLY));
		captureShader.create(&capOpaqueVert, &capOpaqueFrag);
	}

	if (!ambientShader.isGenerated())
	{
		auto lightAmbientVert = ShaderSourceCache::getShader("ambientGBufferVert").loadFromFile("shaders/gBuffer/ambient.vert");
		auto lightAmbientFrag = ShaderSourceCache::getShader("ambientGBufferFrag").loadFromFile("shaders/gBuffer/ambient.frag");
		ambientShader.create(&lightAmbientVert, &lightAmbientFrag);
	}

	this->width = width;
	this->height = height;

	depth->release();
	normal->release();
	material->release();
	polyID->release();
	position->release();
	depthBuffer->release();
	gBuffer->release();

	gBuffer->setSize(width, height);
	gBuffer->create(depthBuffer, true);

	// Create g-buffer textures.
	if (mipmapFlag)
	{
		depth->create(nullptr, GL_R32F, width, height, false);
		normal->create(nullptr, GL_RGB32F, width, height, false);
		material->create(nullptr, GL_RGBA, width, height, false);
	#if !DEPTH_ONLY
		position->create(nullptr, GL_RGB32F, width, height, false);
	#endif
	}
	else
	{
		depth->create(nullptr, GL_R32F, width, height);
		normal->create(nullptr, GL_RGB32F, width, height);
		material->create(nullptr, GL_RGBA, width, height);
	#if !DEPTH_ONLY
		position->create(nullptr, GL_RGB32F, width, height);
	#endif
	}
	polyID->create(nullptr, GL_R32UI, width, height);

	if (mipmapFlag)
	{
		depth->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
		normal->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
		material->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	#if !DEPTH_ONLY
		position->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	#endif
	}

	gBuffer->bind();
	gBuffer->attachColorBuffer(depth);
	gBuffer->attachColorBuffer(normal);
	gBuffer->attachColorBuffer(material);
	gBuffer->attachColorBuffer(polyID);
#if !DEPTH_ONLY
	gBuffer->attachColorBuffer(position);
#endif
	gBuffer->setDrawBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	gBuffer->unbind();

	size_t depthSize = width * height * 4 * 1; // 4 bytes per channel, 1 channel per pixel.
	size_t normalSize = width * height * 4 * 3; // 4 bytes per channel, 3 channels per pixel.
	size_t materialSize = width * height * 1 * 1; // 1 byte per channel, 1 channel per pixel.
	size_t polySize = width * height * 4 * 1; // 4 bytes per channel, 1 channel per pixel.
#if !DEPTH_ONLY
	size_t posSize = width * height * 4 * 3; // 4 bytes per channel, 3 channels per pixel.
#else
	size_t posSize = 0;
#endif

	if (profiler)
		profiler->setBufferSize("g-buffer", depthSize + normalSize + materialSize + polySize + posSize);
}

void GBuffer::release()
{
	gBuffer->release();
	depthBuffer->release();
	depth->release();
	normal->release();
	material->release();
	polyID->release();
	position->release();

	captureShader.release();
	ambientShader.release();
	drawDepthShader.release();
	drawNormShader.release();

	width = 0;
	height = 0;
}

void GBuffer::beginCapture()
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	gBuffer->bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void GBuffer::endCapture()
{
	gBuffer->unbind();
	glPopAttrib();

	if (mipmapFlag)
	{
		depth->genMipmap();
		normal->genMipmap();
		material->genMipmap();
	#if !DEPTH_ONLY
		position->genMipmap();
	#endif
	}
}

void GBuffer::bindTextures()
{
	depth->bind();
	normal->bind();
	material->bind();
	polyID->bind();
#if !DEPTH_ONLY
	position->bind();
#endif
}

void GBuffer::unbindTextures()
{
	depth->unbind();
	normal->unbind();
	material->unbind();
	polyID->unbind();
#if !DEPTH_ONLY
	position->unbind();
#endif
}

void GBuffer::setUniforms(Shader *shader)
{
	shader->setUniform("depthTex", depth);
	shader->setUniform("normalTex", normal);
	shader->setUniform("materialTex", material);
	shader->setUniform("polyIDTex", polyID);
#if !DEPTH_ONLY
	shader->setUniform("positionTex", position);
#endif
}

void GBuffer::applyAmbient()
{
	ambientShader.bind();
	material->bind();
	ambientShader.setUniform("materialTex", material);
	Renderer::instance().drawQuad();
	material->unbind();
	ambientShader.unbind();
}

void GBuffer::debugRenderDepth()
{
	if (!drawDepthShader.isGenerated())
	{
		auto capVert = ShaderSourceCache::getShader("drawDepthGBufferVert").loadFromFile("shaders/gBuffer/drawDepth.vert");
		auto capFrag = ShaderSourceCache::getShader("drawDepthGBufferFrag").loadFromFile("shaders/gBuffer/drawDepth.frag");
		capFrag.setDefine("DEPTH_ONLY", Utils::toString(DEPTH_ONLY));
		drawDepthShader.create(&capVert, &capFrag);
	}

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	drawDepthShader.bind();

	depth->bind();
	drawDepthShader.setUniform("depthTex", depth);
	Renderer::instance().drawQuad();
	depth->unbind();

	drawDepthShader.unbind();

	glPopAttrib();
}

void GBuffer::debugRenderNormal()
{
	if (!drawDepthShader.isGenerated())
	{
		auto capVert = ShaderSourceCache::getShader("drawNormGBufferVert").loadFromFile("shaders/gBuffer/drawNorm.vert");
		auto capFrag = ShaderSourceCache::getShader("drawNormGBufferFrag").loadFromFile("shaders/gBuffer/drawNorm.frag");
		capFrag.setDefine("DEPTH_ONLY", Utils::toString(DEPTH_ONLY));
		drawNormShader.create(&capVert, &capFrag);
	}

	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	drawNormShader.bind();

	normal->bind();
	drawNormShader.setUniform("normTex", normal);
	Renderer::instance().drawQuad();
	normal->unbind();

	drawNormShader.unbind();

	glPopAttrib();
}

void GBuffer::debugRenderMaterial()
{
	std::cout << "Implement me: GBuffer::debugRenderMaterial()\n";
}

