#include "Bloom.h"

#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Texture.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"
#include "../Profiler.h"
#include "../Renderer.h"

using namespace Rendering;

Bloom::Bloom() : width(0), height(0), profiler(nullptr)
{
	drawBuffer = new FrameBuffer();
	depthBuffer = new RenderBuffer();
	color = new Texture2D();
	bright = new Texture2D();

	for (int i = 0; i < 4; i++)
	{
		blurBuffers[i] = new FrameBuffer();
		blurTexes[i] = new Texture2D();
	}
}

Bloom::~Bloom()
{
	delete color;
	delete bright;
	delete depthBuffer;
	delete drawBuffer;
	color = nullptr;
	bright = nullptr;
	depthBuffer = nullptr;
	drawBuffer = nullptr;

	for (int i = 0; i < 4; i++)
	{
		delete blurBuffers[i];
		delete blurTexes[i];
		blurBuffers[i] = nullptr;
		blurTexes[i] = nullptr;
	}
}

void Bloom::resize(int width, int height)
{
	if (this->width == width && this->height == height)
		return;

	this->width = width;
	this->height = height;

	color->release();
	bright->release();
	depthBuffer->release();
	drawBuffer->release();

	for (int i = 0; i < 4; i++)
	{
		blurTexes[i]->release();
		blurBuffers[i]->release();
	}

	drawBuffer->setSize(width, height);
	drawBuffer->create(depthBuffer, true); // could be false if you don't need stencil buffer.

	// Create textures.
	color->create(nullptr, GL_RGB32F, width, height, false);
	bright->create(nullptr, GL_RGB32F, width, height, false);
	color->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);
	bright->setFilters(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_LINEAR);

	drawBuffer->bind();
	drawBuffer->attachColorBuffer(color);
	drawBuffer->attachColorBuffer(bright);
	drawBuffer->setDrawBuffers();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	drawBuffer->unbind();

	int amt = 2;
	for (int i = 0; i < 4; i++)
	{
		blurBuffers[i]->setSize(width / amt, height / amt);
		blurBuffers[i]->generate();
		blurTexes[i]->create(nullptr, GL_RGB32F, width / amt, height / amt, false);
		blurTexes[i]->setFilters(GL_CLAMP_TO_EDGE);

		blurBuffers[i]->bind();
		blurBuffers[i]->attachColorBuffer(blurTexes[i]);
		blurBuffers[i]->setDrawBuffers();
		glClear(GL_COLOR_BUFFER_BIT);
		blurBuffers[i]->unbind();

		amt *= 2;
	}

	size_t colorSize = width * height * 4 * 3; // 4 bytes per channel, 3 channels per pixel.
	size_t brightSize = width * height * 4 * 3; // 4 byte per channel, 3 channel per pixel.

	if (profiler)
		profiler->setBufferSize("bloomBuffer", colorSize + brightSize);
}

void Bloom::reloadShaders()
{
	auto blurVert = ShaderSourceCache::getShader("blurBloomVert").loadFromFile("shaders/bloom/blur.vert");
	auto blurFrag = ShaderSourceCache::getShader("blurBloomFrag").loadFromFile("shaders/bloom/blur.frag");
	blurShader.release();
	blurShader.create(&blurVert, &blurFrag);

	auto compVert = ShaderSourceCache::getShader("compositeBloomVert").loadFromFile("shaders/bloom/composite.vert");
	auto compFrag = ShaderSourceCache::getShader("compositeBloomFrag").loadFromFile("shaders/bloom/composite.frag");
	compositeShader.release();
	compositeShader.create(&compVert, &compFrag);

	auto capVert = ShaderSourceCache::getShader("drawBloomBufferVert").loadFromFile("shaders/bloom/draw.vert");
	auto capFrag = ShaderSourceCache::getShader("drawBloomBufferFrag").loadFromFile("shaders/bloom/draw.frag");
	drawShader.release();
	drawShader.create(&capVert, &capFrag);
}

void Bloom::beginCapture()
{
	// TODO: What if you use bloom with deferred rendering?
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	drawBuffer->bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Bloom::endCapture()
{
	drawBuffer->unbind();
	glPopAttrib();

	color->genMipmap();
	bright->genMipmap();
}

void Bloom::applyBlur()
{
	if (!blurShader.isGenerated())
		reloadShaders();

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	blurShader.bind();

	glClear(GL_COLOR_BUFFER_BIT);

	bright->bind();
	blurShader.setUniform("brightTex", bright);
	blurShader.setUniform("prevFlag", false);

	// TODO: Blurring steps:
	// 1. Start at 1/16th res mipmap level.
	// 2. Read pixels from mipmap, blur horizontally, write to texture.
	// 3. Read pixels from that texture, blur vertically, write to texture.
	// 4. Combine blurred result with next mipmap level.
	// 5. Repeat from step 2.
	int amt = 16;
	for (int i = 3; i >= 0; i--)
	{
		// Combine downscaled bright pass with previous blurred output.
		if (i < 3)
		{
			blurTexes[i + 1]->bind();
			blurShader.setUniform("prevTex", blurTexes[i + 1]);
		}

		blurShader.setUniform("lod", i);

		// Blur current.
		blurBuffers[i]->bind();
		camera.setViewport(0, 0, width / amt, height / amt);
		camera.upload();

		blurShader.setUniform("horizontal", true);
		Renderer::instance().drawQuad();

		//blurShader.setUniform("prevFlag", false);
		//blurShader.setUniform("horizontal", false);
		//Renderer::instance().drawQuad();

		blurBuffers[i]->unbind();

		amt /= 2;

		if (i < 3)
			blurTexes[i + 1]->unbind();
		blurShader.setUniform("prevFlag", true);
	}

	bright->unbind();

	blurShader.unbind();

	camera.setViewport(0, 0, width, height);
	camera.upload();

	glPopAttrib();
}

void Bloom::composite()
{
	// Just composite from color and blur textures. Do tone mapping here.
	if (!compositeShader.isGenerated())
		reloadShaders();

	// TODO: First calculate the avg luminance of the whole scene.
	// TODO: Adjust tone mapping based on this value.

	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	compositeShader.bind();

	color->bind();
	bright->bind();
	blurTexes[0]->bind();

	compositeShader.setUniform("colorTex", color);
	compositeShader.setUniform("brightTex", bright);
	compositeShader.setUniform("blurTex", blurTexes[0]);

	Renderer::instance().drawQuad();

	color->unbind();
	bright->unbind();
	blurTexes[0]->unbind();

	compositeShader.unbind();

	glPopAttrib();
}

void Bloom::debugRenderColor()
{
	if (!drawShader.isGenerated())
		reloadShaders();

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	drawShader.bind();

	color->bind();
	drawShader.setUniform("tex", color);
	Renderer::instance().drawQuad();
	color->unbind();

	drawShader.unbind();

	glPopAttrib();
}

void Bloom::debugRenderBright()
{
	if (!drawShader.isGenerated())
		reloadShaders();

	//bright->setFilters();

	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	drawShader.bind();

	bright->bind();
	drawShader.setUniform("tex", bright);
	Renderer::instance().drawQuad();
	bright->unbind();

	drawShader.unbind();

	glPopAttrib();
}

void Bloom::debugRenderBlur(int lod)
{
	if (lod < 0)
		lod = 0;
	if (lod > 3)
		lod = 3;

	if (!drawShader.isGenerated())
		reloadShaders();

	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	drawShader.bind();

	blurTexes[lod]->bind();
	drawShader.setUniform("tex", blurTexes[lod]);
	Renderer::instance().drawQuad();
	blurTexes[lod]->unbind();

	drawShader.unbind();

	glPopAttrib();
}

