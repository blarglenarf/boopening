#pragma once

#include "../RenderObject/Shader.h"

namespace Rendering
{
	class FrameBuffer;
	class RenderBuffer;
	class Texture2D;
	class Profiler;

	// TODO: Add support for multiple directions (eg, a point light casts a cubemap of shadows).
	class ShadowMap
	{
	private:
		FrameBuffer *fbo;
		RenderBuffer *depthBuffer;
		Texture2D *depthTexture;

		Shader captureShader;
		Shader drawShader;

		int width;
		int height;

		Profiler *profiler;

	public:
		ShadowMap();
		~ShadowMap();

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }

		void resize(int width, int height);
		void release();

		void beginCapture();
		void endCapture();

		void bindTextures();
		void unbindTextures();
		void setUniforms(Shader *shader);

		void debugRender();

		Texture2D *getDepthTexture() { return depthTexture; }

		Shader &getCaptureShader() { return captureShader; }

		int getWidth() const { return width; }
		int getHeight() const { return height; }
	};
}

