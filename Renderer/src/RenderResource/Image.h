#pragma once

#include "../GL.h"

#include "../../../Math/src/Vector/VectorCommon.h"

#include <string>

namespace Rendering
{
	// TODO: Get rid of dimensions, just a width/height will do.
	class Image
	{
	private:
		std::string name;

		unsigned char *data;
		size_t size;
		int channels;
		GLenum format;
		Math::ivec2 dimensions;

	public:
		Image(const std::string &name = "") : name(name), data(nullptr), size(0), channels(0), format(0) {}
		virtual ~Image() { clear(); }

		void setName(const std::string &name) { this->name = name; }
		const std::string &getName() const { return name; }

		void clear();

		Image &load(unsigned char *data, size_t size, int channels, Math::ivec2 dimensions);

		void flip();
		void addBorder(int borderSize);
		void screenshot();

		unsigned char *getData() { return data; }
		int getChannels() const { return channels; }
		GLenum getFormat() const { return format; }
		Math::ivec2 getDimensions() const { return dimensions; }
		int getPixelIndex(int x, int y, int width = -1);

		void blit();

		// If you explicitly need a deep copy of this.
		Image deepCopy();
	};
}

