#pragma once

#include "../Vertex.h"

#include <string>
#include <vector>

namespace Rendering
{
	class Material;

	class Mesh
	{
	public:
		struct Faceset
		{
			unsigned int start;
			unsigned int end;
			Material *mat;
			Faceset(Material *mat = nullptr, unsigned int start = 0, unsigned int end = 0) : start(start), end(end), mat(mat) {}
		};

	private:
		std::string name;

		std::vector<Vertex> vertices;
		std::vector<unsigned int> indices;

		std::vector<Material*> materials;

		std::vector<Faceset> facesets;

		Math::vec3 min;
		Math::vec3 max;

	public:
		Mesh() {}

		void clear() { min = Math::vec3(0.0f, 0.0f, 0.0f); max = Math::vec3(0.0f, 0.0f, 0.0f); vertices.clear(); indices.clear(); materials.clear(); facesets.clear(); }

		Mesh &load(std::vector<Vertex> &&vertices, std::vector<unsigned int> &&indices, const Math::vec3 &min, const Math::vec3 &max);

		void addMaterial(Material *mat);
		void useMaterial(Material *mat, size_t start, size_t end);

		Mesh extractFromMaterials(const std::vector<std::string> &materialNames, bool removeFlag = false);

		Vertex *getVertices() { if (vertices.empty()) return nullptr; return &vertices[0]; }
		Vertex &getVertex(size_t pos) { return vertices[pos]; }
		size_t getNumVertices() { return vertices.size(); }
		unsigned int *getIndices() { if (indices.empty()) return nullptr; return &indices[0]; }
		unsigned int &getIndex(size_t pos) { return indices[pos]; }
		size_t getNumIndices() { return indices.size(); }

		std::vector<Material*> &getMaterials() { return materials; }
		std::vector<Faceset> &getFacesets() { return facesets; }

		void resizeVertices(size_t nVertices) { vertices.resize(nVertices); }
		void resizeIndices(size_t nIndices) { indices.resize(nIndices); }

		void generateNormals();
		void generateTangents();

		void renderNormals(float scale = 0.1f);
		void renderTangents(float scale = 0.1f);

		void scale(const Math::vec3 &size);
		void scale(const Math::vec3 &min, const Math::vec3 &max);
		void translate(const Math::vec3 &pos);

		static void updateMinMax(const Math::vec3 &v, Math::vec3 &min, Math::vec3 &max);

		const Math::vec3 &getMin() const { return min; }
		const Math::vec3 &getMax() const { return max; }
		void setMin(const Math::vec3 &min) { this->min = min; }
		void setMax(const Math::vec3 &max) { this->max = max; }

		const std::string &getName() const { return name; }
		void setName(const std::string &name) { this->name = name; }

		void renderImmediate();
		void renderVertexArray();

		static Mesh getSquare(float z = 0);
		static Mesh getCube();
		static Mesh getCircle();
		static Mesh getSphere();
		static Mesh getCylinder();
		static Mesh getEllipsoid(float a = 1, float b = 1, float c = 1);
		static Mesh getGrid(float width = 100.0f, float height = 100.0f, int slices = 11, int stacks = 11);
	};
}

