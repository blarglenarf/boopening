#include "RenderResourceCache.h"
#include "Material.h"
#include "ShaderSource.h"
#include "Image.h"
#include "Mesh.h"
#include "Font.h"

#include <iostream>

using namespace Rendering;

std::map<std::string, Material*> MaterialCache::cache;
std::map<std::string, ShaderSource*> ShaderSourceCache::cache;
std::map<std::string, Image*> ImageCache::cache;
std::map<std::string, Mesh*> MeshCache::cache;
std::map<std::string, Font*> FontCache::cache;


template <typename T, typename U>
static bool get(const std::string &name, T *&obj, U &cache)
{
	auto it = cache.find(name);
	if (it == cache.end())
	{
		auto *t = new T();
		t->setName(name);
		cache[name] = t;
		obj = t;
		return false;
	}
	else
	{
		obj = it->second;
		return true;
	}
}

template <typename T>
static bool exists(const std::string &name, T &cache)
{
	return cache.find(name) != cache.end();
}

template <typename T, typename U>
static T &get(const std::string &name, U &cache)
{
	auto it = cache.find(name);
	if (it == cache.end())
	{
		auto *t = new T();
		t->setName(name);
		cache[name] = t;
		return *t;
	}
	return *it->second;
}

template <typename T, typename U>
static T &add(T *obj, U &cache, const std::string &type)
{
	static T empty;
	if (obj->getName().empty())
	{
		std::cout << "Cannot add " << type << " with no name to cache\n";
		return empty;
	}

	auto it = cache.find(obj->getName());
	if (it != cache.end())
		std::cout << "Warning: " << type << " " << obj->getName() << " already exists in " << type << " cache\n";

	cache[obj->getName()] = obj;
	return *obj;
}

template <typename T>
static void clear(T &cache)
{
	for (auto it : cache)
		delete it.second;
	cache.clear();
}

template <typename T, typename U>
static T *removeByName(const std::string &name, U &cache, const std::string &type)
{
	auto it = cache.find(name);
	if (it == cache.end())
	{
		std::cout << "Cannot remove " << name << " from " << type << " cache, item does not exist\n";
		return nullptr;
	}
	T *obj = it->second;
	cache.erase(it);
	return obj;
}

template <typename T, typename U>
static T *removeByObj(T *obj, U &cache, const std::string &type)
{
	if (!obj)
	{
		std::cout << "Cannot remove empty item from " << type << " cache\n";
		return nullptr;
	}
	auto it = cache.find(obj->getName());
	if (it == cache.end())
	{
		std::cout << "Cannot remove " << obj->getName() << " from " << type << " cache, item does not exist\n";
		return nullptr;
	}
	cache.erase(it);
	return obj;
}



void MaterialCache::clear()
{
	::clear(cache);
}

bool MaterialCache::getMaterial(const std::string &name, Material *&mat)
{
	return get(name, mat, cache);
}

bool MaterialCache::exists(const std::string &name)
{
	return ::exists(name, cache);
}

Material &MaterialCache::getMaterial(const std::string &name)
{
	return get<Material>(name, cache);
}

Material &MaterialCache::addMaterial(Material *mat)
{
	return add(mat, cache, "material");
}

Material *MaterialCache::removeMaterial(const std::string &name)
{
	return removeByName<Material>(name, cache, "material");
}

Material *MaterialCache::removeMaterial(Material *mat)
{
	return removeByObj(mat, cache, "material");
}



void ShaderSourceCache::clear()
{
	::clear(cache);
}

bool ShaderSourceCache::getShader(const std::string &name, ShaderSource *&src)
{
	ShaderSource::initDefaultShaders(cache);
	return get(name, src, cache);
}

bool ShaderSourceCache::exists(const std::string &name)
{
	ShaderSource::initDefaultShaders(cache);
	return ::exists(name, cache);
}

ShaderSource &ShaderSourceCache::getShader(const std::string &name)
{
	ShaderSource::initDefaultShaders(cache);
	return get<ShaderSource>(name, cache);
}

ShaderSource &ShaderSourceCache::addShader(ShaderSource *src)
{
	ShaderSource::initDefaultShaders(cache);
	return add(src, cache, "shader");
}

ShaderSource *ShaderSourceCache::removeShaderSource(const std::string &name)
{
	return removeByName<ShaderSource>(name, cache, "shader");
}

ShaderSource *ShaderSourceCache::removeShaderSource(ShaderSource *src)
{
	return removeByObj(src, cache, "shader");
}



void ImageCache::clear()
{
	::clear(cache);
}

bool ImageCache::getImage(const std::string &name, Image *&img)
{
	return get(name, img, cache);
}

bool ImageCache::exists(const std::string &name)
{
	return ::exists(name, cache);
}

Image &ImageCache::getImage(const std::string &name)
{
	return get<Image>(name, cache);
}

Image &ImageCache::addImage(Image *img)
{
	return add(img, cache, "image");
}

Image *ImageCache::removeImage(const std::string &name)
{
	return removeByName<Image>(name, cache, "image");
}

Image *ImageCache::removeImage(Image *img)
{
	return removeByObj(img, cache, "image");
}



void MeshCache::clear()
{
	::clear(cache);
}

bool MeshCache::getMesh(const std::string &name, Mesh *&mesh)
{
	return get(name, mesh, cache);
}

bool MeshCache::exists(const std::string &name)
{
	return ::exists(name, cache);
}

Mesh &MeshCache::getMesh(const std::string &name)
{
	return get<Mesh>(name, cache);
}

Mesh &MeshCache::addMesh(Mesh *mesh)
{
	return add(mesh, cache, "mesh");
}

Mesh *MeshCache::removeMesh(const std::string &name)
{
	return removeByName<Mesh>(name, cache, "mesh");
}

Mesh *MeshCache::removeMesh(Mesh *mesh)
{
	return removeByObj(mesh, cache, "mesh");
}



void FontCache::clear()
{
	::clear(cache);
}

bool FontCache::getFont(const std::string &name, Font *&font)
{
	return get(name, font, cache);
}

bool FontCache::exists(const std::string &name)
{
	return ::exists(name, cache);
}

Font &FontCache::getFont(const std::string &name)
{
	return get<Font>(name, cache);
}

Font &FontCache::addFont(Font *font)
{
	return add(font, cache, "font");
}

Font *FontCache::removeFont(const std::string &name)
{
	return removeByName<Font>(name, cache, "font");
}

Font *FontCache::removeFont(Font *font)
{
	return removeByObj(font, cache, "font");
}

