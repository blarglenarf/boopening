#pragma once

#include <string>
#include <vector>
#include <map>

namespace Rendering
{
	class ShaderSource
	{
	public:
		// TODO: Probably needs to be a tree structure.
		struct InnerSource
		{
		public:
			enum Type
			{
				INCLUDE,
				IMPORT,
				NONE
			};
			Type type;
			std::string name;
			size_t lineNo;
			size_t lineCount;

			InnerSource(Type type = NONE, const std::string &name = "", size_t lineNo = 0, size_t lineCount = 0) : type(type), name(name), lineNo(lineNo), lineCount(lineCount) {}
		};

	private:
		// TODO: Probably need a tree of sources.
		std::string name;
		std::string src;

		//std::vector<std::pair<std::string, std::string>> srcIncludes;
		std::vector<InnerSource> srcIncludes;

		static bool initFlag;

	private:
		void preprocessSrc(std::string &newSrc, const std::string &name, const std::string &filename, int totalLines = 1);
		void getSrcFromFile(const std::string &filename);

	public:
		ShaderSource(const std::string &name = "", const std::string &src = "") : name(name), src(src) {}

		static void initDefaultShaders(std::map<std::string, ShaderSource*> &cache);

		ShaderSource &load(const std::string &src, bool printSrc = false);
		ShaderSource &loadFromFile(const std::string &filename, bool printSrc = false);

		const std::string &getName() const { return name; }
		const std::string &getSrc() const { return src; }

		void setName(const std::string &name) { this->name = name; }
		void setSrc(const std::string &src) { this->src = src; srcIncludes.clear(); }

		void setDefine(const std::string &def, const std::string &val);
		void replace(const std::string &tag, const std::string &val);

		const std::vector<InnerSource> &getSrcIncludes() const { return srcIncludes; }
	};
}
