#include "Scene.h"

#include "RenderResourceCache.h"

#include "../../../Resources/src/Mesh/MeshLoader.h"

#include <iostream>

using namespace Rendering;


Scene &Scene::load(const std::string &name, std::vector<SceneObject> &&sceneObjects)
{
	clear();

	this->name = name;
	this->sceneObjects = std::move(sceneObjects);

	return *this;
}

void Scene::loadMeshesToCache()
{
	// The sceneObjects won't have loaded the actual meshes themselves. That now needs to be done.
	for (auto &obj : sceneObjects)
	{
		sceneMap[obj.meshName] = &obj;

		if (!MeshCache::exists(obj.meshName))
		{
			Mesh *mesh = new Mesh();
			if (!Resources::MeshLoader::load(mesh, obj.file))
			{
				delete mesh;
				std::cout << "Failed to load mesh " << obj.meshName << " from file " << obj.file << "\n";
				continue;
			}
			mesh->setName(obj.meshName);
			MeshCache::addMesh(mesh);
			obj.mesh = mesh;

			if (obj.bakeFlag)
			{
				if (obj.scaleMin != Math::vec3(0, 0, 0) && obj.scaleMax != Math::vec3(0, 0, 0))
					mesh->scale(obj.scaleMin, obj.scaleMax);
				else if (obj.scale != Math::vec3(1, 1, 1))
					mesh->scale(obj.scale);
			}
		}
		else
			obj.mesh = &MeshCache::getMesh(obj.meshName);
	}
}

void Scene::loadGpuMeshes(bool useMap)
{
	// In case we encounter any meshes that aren't cached.
	std::map<std::string, Mesh*> tmpMeshes;

	for (auto &obj : sceneObjects)
	{
		// If the gpuMesh is already mapped, then we don't need to create another one.
		if (useMap && gpuMeshMap.find(obj.meshName) != gpuMeshMap.end())
		{
			obj.gpuMesh = gpuMeshMap[obj.meshName];
			continue;
		}

		// Otherwise we do.
		Mesh *mesh = nullptr;
		if (!MeshCache::exists(obj.meshName))
		{
			if (tmpMeshes.find(obj.meshName) == tmpMeshes.end())
			{
				mesh = new Mesh();
				if (!Resources::MeshLoader::load(mesh, obj.file))
				{
					delete mesh;
					std::cout << "Failed to load mesh " << obj.meshName << " from file " << obj.file << "\n";
					continue;
				}
				mesh->setName(obj.meshName);
				tmpMeshes[obj.meshName] = mesh;
				if (obj.bakeFlag)
				{
					if (obj.scaleMin != Math::vec3(0, 0, 0) && obj.scaleMax != Math::vec3(0, 0, 0))
					{
						mesh->scale(obj.scaleMin, obj.scaleMax);
						std::cout << "boo!\n";
					}
					else if (obj.scale != Math::vec3(1, 1, 1))
					{
						mesh->scale(obj.scale);
						std::cout << "ya!\n";
					}
				}
			}
			else
				mesh = tmpMeshes[obj.meshName];
		}
		else
		{
			mesh = &MeshCache::getMesh(obj.meshName);
		}

		std::cout << "Mesh: " << obj.meshName << ", No. triangles: " << mesh->getNumIndices() / 3 << ", no. vertices: " << mesh->getNumVertices() << "\n";

		gpuMeshes.push_back(new GPUMesh());
		auto *gpuMesh = gpuMeshes[gpuMeshes.size() - 1];

		obj.gpuMesh = gpuMesh;
		if (useMap)
			gpuMeshMap[obj.meshName] = gpuMesh;
		gpuMesh->create(mesh);
	}

	// Clean them up after we're done.
	for (auto it : tmpMeshes)
		delete it.second;
	tmpMeshes.clear();
}

Scene::SceneObject &Scene::operator [] (const std::string &name)
{
	static SceneObject nope;
	auto it = sceneMap.find(name);
	if (it == sceneMap.end())
		return nope;
	return *it->second;
}

GPUMesh *Scene::getGpuMesh(const std::string &name)
{
	auto it = gpuMeshMap.find(name);
	if (it == gpuMeshMap.end())
		return nullptr;
	return it->second;
}

void Scene::clearGpuMeshes()
{
	for (auto *mesh : gpuMeshes)
		delete mesh;
	gpuMeshes.clear();
	gpuMeshMap.clear();
}

void Scene::clearCachedMeshes()
{
	for (auto &obj : sceneObjects)
	{
		if (MeshCache::exists(obj.meshName))
			delete MeshCache::removeMesh(obj.meshName);
		obj.mesh = nullptr;
	}
}

void Scene::clear()
{
	clearGpuMeshes();

	name = "";
	sceneObjects.clear();
	sceneMap.clear();
}

