#include "Image.h"
#include "../Renderer.h"

#include <cstring>
#include <iostream>

using namespace Rendering;

static GLenum createFormat(int channels)
{
	switch (channels)
	{
	case 1:
		return GL_LUMINANCE;
	case 2:
		return GL_LUMINANCE_ALPHA;
	case 3:
		return GL_RGB;
	default:
		return GL_RGBA;
	}
}



void Image::clear()
{
	delete [] data;
	data = nullptr;

	size = 0;
	channels = 0;
	format = 0;
	dimensions = Math::ivec2(0, 0);
}

Image &Image::load(unsigned char *data, size_t size, int channels, Math::ivec2 dimensions)
{
	this->data = data;
	this->size = size;
	this->channels = channels;
	this->format = createFormat(channels);
	this->dimensions = dimensions;

	return *this;
}

void Image::flip()
{
	int width = dimensions.width * channels;
	int height = dimensions.height;

	unsigned char *row1 = data;
	unsigned char *row2 = data + (height - 1) * width;

	for (int y = 0; y < height / 2; y++)
	{
		for (int x = 0; x < width; x++)
		{
			char tmp = row1[x];
			row1[x] = row2[x];
			row2[x] = tmp;
		}
		row1 += width;
		row2 -= width;
	}
}

void Image::addBorder(int borderSize)
{
	int newWidth = dimensions.width + (borderSize * 2);
	int newHeight = dimensions.height + (borderSize * 2);
	size_t newSize = newWidth * newHeight * channels;

	unsigned char *newData = new unsigned char[newSize];
	memset(newData, 0, newSize);
	//unsigned char *newData = (unsigned char *) calloc(newSize, sizeof(unsigned char));
	//if (!newData)
	//	return;

	int pos = 0;
	for (int y = borderSize; y < newHeight - borderSize; y++)
		for (int x = borderSize; x < newWidth - borderSize; x++)
			for (int i = 0; i < channels; i++)
				newData[getPixelIndex(x, y, newWidth) + i] = data[pos++];

	dimensions.width = newWidth;
	dimensions.height = newHeight;

	delete [] data;

	size = newSize;
	data = newData;
}

void Image::screenshot()
{
	if (data)
		delete [] data;

	auto &camera = Renderer::instance().getActiveCamera();
	dimensions = Math::ivec2(camera.getWidth(), camera.getHeight());
	channels = 4;
	format = GL_RGBA;
	size = dimensions.x * dimensions.y * channels;
	data = new unsigned char[size];

	// TODO: If there's a currently bound fbo, use that, otherwise glReadPixels.
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_BLEND);
	glReadPixels(0, 0, dimensions.x, dimensions.y, format, GL_UNSIGNED_BYTE, &data[0]);
	glPopAttrib();
}

int Image::getPixelIndex(int x, int y, int width)
{
	if (width == -1)
		width = dimensions.width;
	return x * channels + y * width * channels;
}

void Image::blit()
{
	glDrawPixels(dimensions.width, dimensions.height, format, GL_UNSIGNED_BYTE, data);
}

Image Image::deepCopy()
{
	Image img;
	img.name = name;
	img.dimensions = dimensions;
	img.channels = channels;
	img.format = format;
	img.size = size;
	img.data = new unsigned char[size];
	for (size_t i = 0; i < size; i++)
		img.data[i] = data[i];
	return img;
}

