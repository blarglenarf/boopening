#pragma once

#include "../../../Math/src/Vector/VectorCommon.h"

#include <string>
#include <vector>

namespace Rendering
{
	class Image;

	class Material
	{
	private:
		std::string name;

		Math::vec4 ambientColor;
		Math::vec4 diffuseColor;
		Math::vec4 specularColor;
		float shininess;

		Image *diffuseImg;
		Image *normalImg;

	public:
		Material(const std::string &name = "", const Math::vec4 &ambientColor = Math::vec4(), const Math::vec4 &diffuseColor = Math::vec4(), const Math::vec4 &specularColor = Math::vec4(), float shininess = 0.0f, Image *diffuseImg = nullptr, Image *normalImg = nullptr);

		void setName(const std::string &name) { this->name = name; }
		const std::string &getName() const { return name; }

		void setAmbientColor(const Math::vec4 &ambientColor) { this->ambientColor = ambientColor; }
		const Math::vec4 &getAmbientColor() const { return ambientColor; }
		Math::vec4 &getAmbientColor() { return ambientColor; }

		void setDiffuseColor(const Math::vec4 &diffuseColor) { this->diffuseColor = diffuseColor; }
		const Math::vec4 &getDiffuseColor() const { return diffuseColor; }
		Math::vec4 &getDiffuseColor() { return diffuseColor; }

		void setSpecularColor(const Math::vec4 &specularColor) { this->specularColor = specularColor; }
		const Math::vec4 &getSpecularColor() const { return specularColor; }
		Math::vec4 &getSpecularColor() { return specularColor; }

		void setShininess(float shininess) { this->shininess = shininess; }
		float getShininess() const { return shininess; }

		void setDiffuseImg(Image *diffuseImg) { this->diffuseImg = diffuseImg; }
		Image *getDiffuseImg() { return diffuseImg; }

		void setNormalImg(Image *normalImg) { this->normalImg = normalImg; }
		Image *getNormalImg() { return normalImg; }
	};
}

