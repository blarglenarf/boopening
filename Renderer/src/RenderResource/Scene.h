#pragma once

#include "../../../Math/src/MathCommon.h"

#include "Mesh.h"
#include "../RenderObject/GPUBuffer.h"

#include <string>
#include <vector>
#include <map>


namespace Rendering
{
	class Scene
	{
	public:
		struct SceneObject
		{
			Math::vec3 scale;
			Math::vec3 pos;
			Math::vec3 rot;

			Math::vec3 scaleMin;
			Math::vec3 scaleMax;

			std::string meshName;
			std::string file;

			bool bakeFlag;

			Mesh *mesh;
			GPUMesh *gpuMesh;

			SceneObject() : scale(Math::vec3(1, 1, 1)), bakeFlag(false), mesh(nullptr), gpuMesh(nullptr) {}
		};
	private:
		std::string name;

		std::vector<SceneObject> sceneObjects;
		std::vector<GPUMesh*> gpuMeshes;

		std::map<std::string, SceneObject*> sceneMap;
		std::map<std::string, GPUMesh*> gpuMeshMap;

	public:
		Scene(const std::string &name = "") : name(name) {}
		~Scene() { clearGpuMeshes(); }

		// This will load the scene object data, but no mesh data.
		Scene &load(const std::string &name, std::vector<SceneObject> &&sceneObjects);

		// This will load all meshes from scene object data.
		void loadMeshesToCache();

		// Load gpu meshes from cache if they're cached, otherwise load them from files.
		void loadGpuMeshes(bool useMap = true);

		std::vector<SceneObject> &getSceneObjects() { return sceneObjects; }
		std::vector<GPUMesh*> &getGpuMeshes() { return gpuMeshes; }

		GPUMesh *getGpuMesh(const std::string &name);

		SceneObject &operator [] (const std::string &name);

		const std::string &getName() const { return name; }

		void clearCachedMeshes();
		void clearGpuMeshes();
		void clear();
	};
}

