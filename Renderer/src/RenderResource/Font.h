#pragma once

#include "../GL.h"
#include "../../../Math/src/Vector/Vec2.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/Atlas.h"

#include <string>
#include <vector>
#include <map>

namespace Resources
{
	class FreetypeFont;
}

namespace Rendering
{
	class Shader;
	class VertexArrayObject;

	class Font
	{
		// TODO: Handle multiple font sizes perhaps?
		// TODO: Have atlas rendering handle kerning.
	private:
		struct GlyphData
		{
			size_t id;
			int offset;
			int left;
			int advance;
			GlyphData(size_t id = 0, int offset = 0, int left = 0, int advance = 0) : id(id), offset(offset), left(left), advance(advance) {}
		};

	private:
		std::string name;

		Resources::FreetypeFont *ftFont;

		Utils::StringHash hashes;
		std::map<std::string, std::pair<GLuint, Math::ivec2>> cache;

		Utils::Atlas atlas;
		std::map<char, GlyphData> regions;
		GLuint atlasTexture;

		static Shader *defaultCacheShader;
		static Shader *defaultAtlasShader;
		static VertexArrayObject *atlasVao;

	private:
		static void init();

	public:
		Font(const std::string &name = "") : name(name), ftFont(nullptr), atlasTexture(0) {}
		~Font();

		void setName(const std::string &name) { this->name = name; }
		const std::string &getName() const { return name; }

		void setFreetypeFont(Resources::FreetypeFont *ftFont) { this->ftFont = ftFont; }

		void setSize(int size);
		int getSize() const;

		void clearCache();
		void clearAtlas();

		void debugRender();
		void debugRender(const std::string &str, int x = 0, int y = 0, int linePadding = 0);

		// Stores/renders strings using cached vbo/textures.
		void storeInCache(const std::string &str, int width = 0, int height = 0, int linePadding = 0);
		void renderFromCache(const std::string &str, int x = 0, int y = 0, float z = 0, float r = 1, float g = 1, float b = 1, int width = 0, int height = 0, int linePadding = 0, int vertScroll = 0, int sideScroll = 0, bool wrapFlag = false);

		// Stores/renders a bunch of chars using an atlas.
		void storeInAtlas(const std::string &str);
		void renderFromAtlas(const std::string &str, int x = 0, int y = 0, float z = 0, float r = 1, float g = 1, float b = 1, int width = 0, int height = 0, int linePadding = 0, int vertScroll = 0, int sideScroll = 0);

		Math::ivec2 getTextSize(const std::string &str, int width = 0, int height = 0, int linePadding = 0);
		Math::ivec3 getCursorPos(const std::string &str, int xOffset, int yOffset, int charPos = -1, int width = 0, int linePadding = 0, int vertScroll = 0, int sideScroll = 0);

		void blit();
	};
}

