#pragma once

#include "../RenderResource/Image.h"

#include "../../../Utils/src/Atlas.h"

#include <map>

namespace Rendering
{
	class ImageAtlas : public Image
	{
	private:
		bool ownsImages;
		Utils::Atlas atlas;

		std::map<Image*, size_t> regions;

	public:
		ImageAtlas() : Image(), ownsImages(false) {}
		ImageAtlas(int size, int channels, Math::ivec2 dimensions, bool ownsImages = false);
		virtual ~ImageAtlas();

		void init(int size, int channels, Math::ivec2 dimensions, bool ownsImages = false);

		bool add(Image *image);
		bool addBlank(int width, int height);
		bool hasImage(Image *image);

		Math::RectF getTexCoords(Image *image);
		Math::RectI getRealCoords(Image *image);

		void blit(bool lineFlag = true);
	};
}
