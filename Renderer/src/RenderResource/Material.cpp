#include "Material.h"

using namespace Rendering;

Material::Material(const std::string &name, const Math::vec4 &ambientColor, const Math::vec4 &diffuseColor, const Math::vec4 &specularColor, float shininess, Image *diffuseImg, Image *normalImg) : name(name), ambientColor(ambientColor), diffuseColor(diffuseColor), specularColor(specularColor), shininess(shininess), diffuseImg(diffuseImg), normalImg(normalImg)
{
}

