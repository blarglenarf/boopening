#include "Mesh.h"
#include "Material.h"
#include "../GL.h"

#include "../../../Math/src/MathGeneral.h"

using namespace Rendering;
using namespace Math;

static void makeGridIndices(int stacks, int slices, std::vector<unsigned int> &indices)
{
	int i = 0;
	for (int stack = 0; stack < stacks - 1; stack++)
	{
		for (int slice = 0; slice < slices - 1; slice++)
		{
			indices[i++] = slice * stacks + stack;
			indices[i++] = (slice + 1) * stacks + stack;
			indices[i++] = slice * stacks + stack + 1;
			indices[i++] = slice * stacks + stack + 1;
			indices[i++] = (slice + 1) * stacks + stack;
			indices[i++] = (slice + 1) * stacks + stack + 1;
		}
	}
}

#if 0
static void renderAxis()
{
	glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINES);

	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0); glVertex3f(5, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0); glVertex3f(0, 5, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0); glVertex3f(0, 0, 5);

	glEnd();

	glPopAttrib();
}
#endif


Mesh &Mesh::load(std::vector<Vertex> &&vertices, std::vector<unsigned int> &&indices, const Math::vec3 &min, const Math::vec3 &max)
{
	this->min = min;
	this->max = max;
	this->vertices = std::move(vertices);
	this->indices = std::move(indices);

	return *this;
}

void Mesh::addMaterial(Material *mat)
{
	for (auto *m : materials)
		if (m == mat)
			return;
	materials.push_back(mat);
}

void Mesh::useMaterial(Material *mat, size_t start, size_t end)
{
	facesets.push_back(Faceset(mat, (unsigned int) start, (unsigned int) end));
}

Mesh Mesh::extractFromMaterials(const std::vector<std::string> &materialNames, bool removeFlag)
{
	// FIXME: This will linearise verts/indices in the newMesh so that vertices are not re-used when possible.
	Mesh newMesh;

	std::vector<Vertex> newVertices(indices.size());
	std::vector<unsigned int> newIndices(indices.size());

	size_t offset = 0;

	// Don't forget to set the min/max of the newMesh as we go.
	vec3 newMin(99999, 99999, 99999);
	vec3 newMax(-99999, -99999, -99999);

	for (auto &faceset : facesets)
	{
		if (!faceset.mat)
			continue;

		// Check if this faceset has a material in materialNames, if not, continue.
		bool useMat = false;
		for (auto &matName : materialNames)
			if (matName == faceset.mat->getName())
			{
				useMat = true;
				break;
			}
		if (removeFlag && useMat)
			continue;
		if (!removeFlag && !useMat)
			continue;
		newMesh.addMaterial(faceset.mat);

		// Add vertices/indices from this faceset to newMesh.
		// TODO: It's possible that the same vertices will be indexed multiple times, may need to account for this.
		size_t count = faceset.end - faceset.start;
		for (size_t i = 0; i < count; i++)
		{
			auto index = indices[i + faceset.start];
			auto &vertex = vertices[index];
			if (vertex.pos.x < newMin.x)
				newMin.x = vertex.pos.x;
			if (vertex.pos.x > newMax.x)
				newMax.x = vertex.pos.x;
			if (vertex.pos.y < newMin.y)
				newMin.y = vertex.pos.y;
			if (vertex.pos.y > newMax.y)
				newMax.y = vertex.pos.y;
			if (vertex.pos.z < newMin.z)
				newMin.z = vertex.pos.z;
			if (vertex.pos.z > newMax.z)
				newMax.z = vertex.pos.z;

			newIndices[offset + i] = (unsigned int) (offset + i);
			newVertices[offset + i] = vertex;
		}

		// Use the material in newMesh, using the new indices/vertices.
		newMesh.useMaterial(faceset.mat, offset, offset + count);
		offset += count;
	}

	// Resize the newVertices and newIndices and load them into newMesh.
	newVertices.resize(offset);
	newIndices.resize(offset);
	newMesh.load(std::move(newVertices), std::move(newIndices), newMin, newMax);

	return newMesh;
}

void Mesh::generateNormals()
{
	// Note: approximation only.
	for (size_t i = 0; i < indices.size() / 3; i++)
	{
		size_t t0 = indices[i * 3 + 0];
		size_t t1 = indices[i * 3 + 1];
		size_t t2 = indices[i * 3 + 2];

		vec3 u = vertices[t1].pos - vertices[t0].pos;
		vec3 v = vertices[t2].pos - vertices[t0].pos;
		vec3 n = cross(u, v);

		vertices[t0].norm += n;
		vertices[t1].norm += n;
		vertices[t2].norm += n;
	}

	for (size_t i = 0; i < vertices.size(); i++)
		vertices[i].norm.normalize();

	// Alternate approximation (untested):
	//generateTangents();
	//for (size_t i = 0; i < mesh->nVerts; i++)
	//	mesh->vertices[i].n = normaliseVec3f(cross(mesh->vertices[i].bin, mesh->vertices[i].tan));
}

void Mesh::generateTangents()
{
	// Generates both tangents and binormals.
	// Note: approximation only.
	for (size_t i = 0; i < indices.size() / 3; i++)
	{
		size_t t0 = indices[i * 3 + 0];
		size_t t1 = indices[i * 3 + 1];
		size_t t2 = indices[i * 3 + 2];

		vec3 p0 = vertices[t0].pos;
		vec3 p1 = vertices[t1].pos;
		vec3 p2 = vertices[t2].pos;

		vec2 uv0 = vertices[t0].tex;
		vec2 uv1 = vertices[t1].tex;
		vec2 uv2 = vertices[t2].tex;

		vec3 e1 = p1 - p0;
		vec3 e2 = p2 - p0;
		vec2 dUV1 = uv1 - uv0;
		vec2 dUV2 = uv2 - uv0;

		float div = dUV1.x * dUV2.y - dUV2.x * dUV1.y;
		if (div == 0)
			continue; // FIXME: Can't generate these if there are no textures.
		float f = 1.0f / div;

		// Binormal is cross product of normal and tangent.
		vec3 tan;
		tan.x = f * (dUV2.y * e1.x - dUV1.y * e2.x);
		tan.y = f * (dUV2.y * e1.y - dUV1.y * e2.y);
		tan.z = f * (dUV2.y * e1.z - dUV1.y * e2.z);
		//bin.x = f * (-dUV2.x * e1.x + dUV1.x * e2.x);
		//bin.y = f * (-dUV2.x * e1.y + dUV1.x * e2.y);
		//bin.z = f * (-dUV2.x * e1.z + dUV1.x * e2.z);

		vertices[t0].tan += tan;
		//vertices[t0].bin += bin;
		vertices[t1].tan += tan;
		//vertices[t1].bin += bin;
		vertices[t2].tan += tan;
		//vertices[t2].bin += bin;
	}

	for (size_t i = 0; i < vertices.size(); i++)
	{
		vertices[i].tan.normalize();
		//vertices[i].bin.normalize();
	}
}

void Mesh::renderNormals(float scale)
{
	glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINES);
	glColor3f(0, 1, 0);
	for (size_t i = 0; i < vertices.size(); i++)
	{
		vec3 s = vertices[i].pos;
		vec3 e = vertices[i].pos + (vertices[i].norm * scale);
		glVertex3fv(&s.x);
		glVertex3fv(&e.x);
	}
	glEnd();

	glPopAttrib();
}

void Mesh::renderTangents(float scale)
{
	glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINES);
	for (size_t i = 0; i < vertices.size(); i++)
	{
		vec3 s = vertices[i].pos;
		vec3 t = vertices[i].pos + (vertices[i].tan * scale);
		glColor3f(1, 0, 0);
		glVertex3fv(&s.x);
		glVertex3fv(&t.x);
	}
	glEnd();

	glPopAttrib();
}

void Mesh::scale(const Math::vec3 &size)
{
	for (size_t i = 0; i < vertices.size(); i++)
		vertices[i].pos *= size;
	generateNormals();
}

void Mesh::scale(const Math::vec3 &min, const Math::vec3 &max)
{
	vec3 scaleDiff = max - min;
	vec3 meshDiff = this->max - this->min;

	float x = Math::max(scaleDiff.x, meshDiff.x) / Math::min(scaleDiff.x, meshDiff.x);
	float y = Math::max(scaleDiff.y, meshDiff.y) / Math::min(scaleDiff.y, meshDiff.y);
	float z = Math::max(scaleDiff.z, meshDiff.z) / Math::min(scaleDiff.z, meshDiff.z);
	vec3 scale(x, y, z);

	vec3 newMin, newMax;
	for (size_t i = 0; i < vertices.size(); i++)
	{
		vertices[i].pos *= scale;
		updateMinMax(vertices[i].pos, newMin, newMax);
	}

	vec3 trans = min - newMin;
	for (size_t i = 0; i < vertices.size(); i++)
		vertices[i].pos += trans;

	generateNormals();
}

void Mesh::translate(const Math::vec3 &pos)
{
	for (size_t i = 0; i < vertices.size(); i++)
		vertices[i].pos += pos;
}

void Mesh::updateMinMax(const Math::vec3 &v, Math::vec3 &min, Math::vec3 &max)
{
	if (v.x < min.x)
		min.x = v.x;
	if (v.x > max.x)
		max.x = v.x;
	if (v.y < min.y)
		min.y = v.y;
	if (v.y > max.y)
		max.y = v.y;
	if (v.z < min.z)
		min.z = v.z;
	if (v.z > max.z)
		max.z = v.z;
}

void Mesh::renderImmediate()
{
	glBegin(GL_TRIANGLES);

	for (size_t i = 0; i < indices.size(); i++)
	{
		unsigned int index = indices[i];
		const Vertex &v = vertices[index];

		glNormal3f(v.norm.x, v.norm.y, v.norm.z);
		glTexCoord2f(v.tex.x, v.tex.y);
		glVertex3f(v.pos.x, v.pos.y, v.pos.z);
	}

	glEnd();
}

void Mesh::renderVertexArray()
{
	// FIXME: This isn't working for some reason...
	glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, GL_FLOAT, sizeof(Vertex), &vertices[0].pos);
	glNormalPointer(GL_FLOAT, sizeof(Vertex), &vertices[0].norm);
	glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &vertices[0].tex);

	glDrawElements(GL_TRIANGLES, (GLsizei) indices.size(), GL_UNSIGNED_INT, &indices[0]);

	glPopClientAttrib();
}

Mesh Mesh::getSquare(float z)
{
	Mesh square;

	square.vertices.resize(4);
	square.indices.resize(6);

	square.vertices[0].pos = Math::vec3(-1, -1, z); square.vertices[0].tex = Math::vec2(0, 0); square.vertices[0].norm = Math::vec3(0, 0, 1);
	square.vertices[1].pos = Math::vec3( 1, -1, z); square.vertices[1].tex = Math::vec2(1, 0); square.vertices[1].norm = Math::vec3(0, 0, 1);
	square.vertices[2].pos = Math::vec3( 1,  1, z); square.vertices[2].tex = Math::vec2(1, 1); square.vertices[2].norm = Math::vec3(0, 0, 1);
	square.vertices[3].pos = Math::vec3(-1,  1, z); square.vertices[3].tex = Math::vec2(0, 1); square.vertices[3].norm = Math::vec3(0, 0, 1);

	square.indices[0] = 0;
	square.indices[1] = 1;
	square.indices[2] = 2;
	square.indices[3] = 0;
	square.indices[4] = 2;
	square.indices[5] = 3;

	square.min = Math::vec3(0, 0, 0);
	square.max = Math::vec3(1, 1, 0);

	return square;
}

Mesh Mesh::getCube()
{
	Mesh cube;

	cube.vertices.resize(24);
	cube.indices.resize(36);

	for (int i = 0; i < 6; i++)
	{
		cube.indices[i * 6] = i * 4;
		cube.indices[i * 6 + 1] = i * 4 + 1;
		cube.indices[i * 6 + 2] = i * 4 + 2;
		cube.indices[i * 6 + 3] = i * 4;
		cube.indices[i * 6 + 4] = i * 4 + 2;
		cube.indices[i * 6 + 5] = i * 4 + 3;
	}

	cube.min = Math::vec3(-1, -1, -1);
	cube.max = Math::vec3( 1,  1,  1);

	// Front
	cube.vertices[0].pos  = Math::vec3( 1, -1,  1); cube.vertices[3].tex  = Math::vec2(1, 0); cube.vertices[3].norm  = Math::vec3( 0,  0,  1);
	cube.vertices[1].pos  = Math::vec3( 1,  1,  1); cube.vertices[2].tex  = Math::vec2(1, 1); cube.vertices[2].norm  = Math::vec3( 0,  0,  1);
	cube.vertices[2].pos  = Math::vec3(-1,  1,  1); cube.vertices[1].tex  = Math::vec2(0, 1); cube.vertices[1].norm  = Math::vec3( 0,  0,  1);
	cube.vertices[3].pos  = Math::vec3(-1, -1,  1); cube.vertices[0].tex  = Math::vec2(0, 0); cube.vertices[0].norm  = Math::vec3( 0,  0,  1);

	// Back
	cube.vertices[4].pos  = Math::vec3( 1, -1, -1); cube.vertices[4].tex  = Math::vec2(0, 0); cube.vertices[4].norm  = Math::vec3( 0,  0, -1);
	cube.vertices[5].pos  = Math::vec3(-1, -1, -1); cube.vertices[5].tex  = Math::vec2(0, 1); cube.vertices[5].norm  = Math::vec3( 0,  0, -1);
	cube.vertices[6].pos  = Math::vec3(-1,  1, -1); cube.vertices[6].tex  = Math::vec2(1, 1); cube.vertices[6].norm  = Math::vec3( 0,  0, -1);
	cube.vertices[7].pos  = Math::vec3( 1,  1, -1); cube.vertices[7].tex  = Math::vec2(1, 0); cube.vertices[7].norm  = Math::vec3( 0,  0, -1);

	// Left
	cube.vertices[8].pos  = Math::vec3(-1, -1, -1); cube.vertices[8].tex  = Math::vec2(0, 0); cube.vertices[8].norm  = Math::vec3(-1,  0,  0);
	cube.vertices[9].pos  = Math::vec3(-1, -1,  1); cube.vertices[9].tex  = Math::vec2(0, 1); cube.vertices[9].norm  = Math::vec3(-1,  0,  0);
	cube.vertices[10].pos = Math::vec3(-1,  1,  1); cube.vertices[10].tex = Math::vec2(1, 1); cube.vertices[10].norm = Math::vec3(-1,  0,  0);
	cube.vertices[11].pos = Math::vec3(-1,  1, -1); cube.vertices[11].tex = Math::vec2(1, 0); cube.vertices[11].norm = Math::vec3(-1,  0,  0);

	// Right
	cube.vertices[12].pos = Math::vec3( 1, -1, -1); cube.vertices[15].tex = Math::vec2(1, 0); cube.vertices[15].norm = Math::vec3( 1,  0,  0);
	cube.vertices[13].pos = Math::vec3( 1,  1, -1); cube.vertices[14].tex = Math::vec2(1, 1); cube.vertices[14].norm = Math::vec3( 1,  0,  0);
	cube.vertices[14].pos = Math::vec3( 1,  1,  1); cube.vertices[13].tex = Math::vec2(0, 1); cube.vertices[13].norm = Math::vec3( 1,  0,  0);
	cube.vertices[15].pos = Math::vec3( 1, -1,  1); cube.vertices[12].tex = Math::vec2(0, 0); cube.vertices[12].norm = Math::vec3( 1,  0,  0);

	// Top
	cube.vertices[16].pos = Math::vec3(-1,  1, -1); cube.vertices[17].tex = Math::vec2(0, 1); cube.vertices[17].norm = Math::vec3( 0,  1,  0);
	cube.vertices[17].pos = Math::vec3(-1,  1,  1); cube.vertices[16].tex = Math::vec2(0, 0); cube.vertices[16].norm = Math::vec3( 0,  1,  0);
	cube.vertices[18].pos = Math::vec3( 1,  1,  1); cube.vertices[19].tex = Math::vec2(1, 0); cube.vertices[19].norm = Math::vec3( 0,  1,  0);
	cube.vertices[19].pos = Math::vec3( 1,  1, -1); cube.vertices[18].tex = Math::vec2(1, 1); cube.vertices[18].norm = Math::vec3( 0,  1,  0);

	// Bottom
	cube.vertices[20].pos = Math::vec3(-1, -1, -1); cube.vertices[20].tex = Math::vec2(0, 0); cube.vertices[20].norm = Math::vec3( 0, -1,  0);
	cube.vertices[21].pos = Math::vec3( 1, -1, -1); cube.vertices[21].tex = Math::vec2(0, 1); cube.vertices[21].norm = Math::vec3( 0, -1,  0);
	cube.vertices[22].pos = Math::vec3( 1, -1,  1); cube.vertices[22].tex = Math::vec2(1, 1); cube.vertices[22].norm = Math::vec3( 0, -1,  0);
	cube.vertices[23].pos = Math::vec3(-1, -1,  1); cube.vertices[23].tex = Math::vec2(1, 0); cube.vertices[23].norm = Math::vec3( 0, -1,  0);

	return cube;
}

Mesh Mesh::getCircle()
{
	Mesh circle;

	int slices = 11;
	float scaleU = 2.0f * (float) M_PI;

	circle.vertices.resize(slices);
	circle.indices.resize((slices - 2) * 3);

	for (int slice = 0; slice < slices; slice++)
	{
		float u = (slice / (float) (slices - 1)) * scaleU;
		float x = cosf(u);
		float z = sinf(u);
		float y = 0.0f;
		int pos = slice;
		circle.vertices[pos].pos = Math::vec3(x, y, z);
		circle.vertices[pos].tex = Math::vec2(x, z);
		circle.vertices[pos].norm = Math::vec3(0, 1, 0);
	}

	int i = 0;
	for (int slice = 1; slice < slices - 1; slice++)
	{
		circle.indices[i++] = 0;
		circle.indices[i++] = slice + 1;
		circle.indices[i++] = slice;
	}

	return circle;
}

Mesh Mesh::getSphere()
{
	Mesh sphere;

	int stacks = 11, slices = 11;
	float scaleU = (float) M_PI, scaleV = 2.0f * (float) M_PI;

	sphere.vertices.resize(stacks * slices);
	sphere.indices.resize((stacks - 1) * (slices - 1) * 6);

	for (int stack = 0; stack < stacks; stack++)
	{
		float u = (stack / (float) (stacks - 1)) * scaleU;

		for (int slice = 0; slice < slices; slice++)
		{
			int pos = stack * stacks + slice;
			float v = (slice / (float) (slices - 1)) * scaleV;
			Math::vec3 p(sinf(u) * cosf(v), sinf(u) * sinf(v), cosf(u));
			sphere.vertices[pos].pos = p;
			sphere.vertices[pos].tex = Math::vec2(1.0f - (float) slice / (float) slices, 1.0f - (float) stack / (float) stacks);
			sphere.vertices[pos].norm = p.unit();
		}
	}

	makeGridIndices(stacks, slices, sphere.indices);

	return sphere;
}

Mesh Mesh::getCylinder()
{
	Mesh cylinder;

	int slices = 11, stacks = 2;
	float scaleU = 2.0f * (float) M_PI;
	float offsetU = 0, offsetV = -0.5f;
	float r = 1.0f;

	cylinder.vertices.resize(slices * stacks + (slices * 2));
	cylinder.indices.resize((slices - 1) * (stacks - 1) * 6 + (slices - 2) * 6);

	for (int stack = 0; stack < stacks; stack++)
	{
		for (int slice = 0; slice < slices; slice++)
		{
			float u = (slice / (float) (slices - 1)) * scaleU + offsetU;
			Math::vec3 p(r * cosf(u), r * sinf(u), stack / (float) (stacks - 1) + offsetV);
			int pos = slice * stacks + stack;
			cylinder.vertices[pos].pos = p;
			cylinder.vertices[pos].tex = Math::vec2(1.0f - (float) slice / (float) slices, 1.0f - (float) stack / (float) stacks);
			cylinder.vertices[pos].norm = p.unit();
		}
	}

	for (int slice = 0; slice < slices; slice++)
	{
		float u = (slice / (float) (slices - 1)) * scaleU;
		float x = cosf(u);
		float y = sinf(u);
		int pos = slices * stacks + slice;
		cylinder.vertices[pos].pos = Math::vec3(x, y, -0.5f);
		cylinder.vertices[pos].tex = Math::vec2(x, y);
		cylinder.vertices[pos].norm = Math::vec3(0, 0, -1);
		pos = slices * stacks + slices + slice;
		cylinder.vertices[pos].pos = Math::vec3(x, y, 0.5f);
		cylinder.vertices[pos].tex = Math::vec2(x, y);
		cylinder.vertices[pos].norm = Math::vec3(0, 0, 1);
	}
	
	int endCircleVerts = slices * stacks;
	int endCircleIndices = (slices - 1) * (stacks - 1) * 6;
	int i = endCircleIndices;
	for (int slice = endCircleVerts + 1; slice < endCircleVerts + slices - 1; slice++)
	{
		cylinder.indices[i++] = endCircleVerts;
		cylinder.indices[i++] = slice + 1;
		cylinder.indices[i++] = slice;
	}
	for (int slice = endCircleVerts + slices + 1; slice < endCircleVerts + slices * 2 - 1; slice++)
	{
		cylinder.indices[i++] = slice;
		cylinder.indices[i++] = slice + 1;
		cylinder.indices[i++] = endCircleVerts + slices;
	}

	makeGridIndices(stacks, slices, cylinder.indices);
	
	return cylinder;
}

Mesh Mesh::getEllipsoid(float a, float b, float c)
{
	Mesh ellipsoid;

	int slices = 11, stacks = 11;
	float scaleU = (float) M_PI, scaleV = 2.0f * (float) M_PI;
	float offsetU = (float) -M_PI * 0.5f, offsetV = (float) -M_PI;

	ellipsoid.vertices.resize(slices * stacks);
	ellipsoid.indices.resize((slices - 1) * (stacks - 1) * 6);

	for (int stack = 0; stack < stacks; stack++)
	{
		float u = (stack / (float) (stacks - 1)) * scaleU + offsetU;

		for (int slice = 0; slice < slices; slice++)
		{
			float v = (slice / (float) (slices - 1)) * scaleV + offsetV;
			Math::vec3 p(a * cosf(u) * cosf(v), b * cosf(u) * sinf(v), c * sinf(u));
			int pos = slice * slices + stack;
			ellipsoid.vertices[pos].pos = p;
			ellipsoid.vertices[pos].tex = Math::vec2(1.0f - (float) slice / (float) slices, 1.0f - (float) stack / (float) stacks);
			ellipsoid.vertices[pos].norm = Math::vec3(2.0f * p.x / (a * a), 2.0f * p.y / (b * b), 2.0f * p.z / (c * c)).normalize();
		}
	}

	makeGridIndices(stacks, slices, ellipsoid.indices);
	
	return ellipsoid;
}

Mesh Mesh::getGrid(float width, float height, int slices, int stacks)
{
	Mesh grid;

	grid.vertices.resize(slices * stacks);
	grid.indices.resize((slices - 1) * (stacks - 1) * 6);

	float startX = width / 2.0f;
	float startY = height / 2.0f;

	for (int stack = 0; stack < stacks; stack++)
	{
		float x = (float) stack / (float) stacks * width - startX;
		for (int slice = 0; slice < slices; slice++)
		{
			float y = (float) slice / (float) slices * height - startY;
			int pos = slice * slices + stack;
			grid.vertices[pos].tex = Math::vec2(x / width, y / height);
			grid.vertices[pos].pos = Math::vec3(x, 0, y);
			grid.vertices[pos].norm = Math::vec3(0, 1, 0);
		}
	}

	makeGridIndices(stacks, slices, grid.indices);

	return grid;
}

