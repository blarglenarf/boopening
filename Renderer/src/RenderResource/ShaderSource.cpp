#include "ShaderSource.h"
#include "RenderResourceCache.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/File.h"
#include "../../../Resources/src/Index.h"

#include <string>
#include <cassert>
#include <iostream>
#include <algorithm>

using namespace Rendering;

bool ShaderSource::initFlag = false;


static ShaderSource *createBasicVertShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	layout(location = 0) in vec3 vertex;\
	layout(location = 1) in vec3 normal;\
	layout(location = 2) in vec2 texCoord;\
	\
	uniform mat4 mvMatrix;\
	uniform mat4 pMatrix;\
	\
	void main()\
	{\
		vec4 osVert = vec4(vertex, 1.0);\
		vec4 esVert = mvMatrix * osVert;\
		vec4 csVert = pMatrix * esVert;\
		\
		gl_Position = csVert;\
	}\
	");
}

static ShaderSource *createBasicFragShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	out vec4 fragColor;\
	\
	void main()\
	{\
		fragColor = vec4(1.0, 1.0, 1.0, 1.0);\
	}\
	");
}

static ShaderSource *createTexVertShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	layout(location = 0) in vec3 vertex;\
	layout(location = 1) in vec3 normal;\
	layout(location = 2) in vec2 texCoord;\
	\
	uniform mat4 mvMatrix;\
	uniform mat4 pMatrix;\
	\
	out vec2 vTexCoord;\
	\
	void main()\
	{\
		vec4 osVert = vec4(vertex, 1.0);\
		vec4 esVert = mvMatrix * osVert;\
		vec4 csVert = pMatrix * esVert;\
		\
		vTexCoord = texCoord;\
		gl_Position = csVert;\
	}\
	");
}

static ShaderSource *createTexFragShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	in vec2 vTexCoord;\
	\
	uniform sampler2D texture;\
	\
	out vec4 fragColor;\
	\
	void main()\
	{\
		fragColor = texture2D(texture, vTexCoord);\
	}\
	");
}

static ShaderSource *createPhongVertShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	layout(location = 0) in vec3 vertex;\
	layout(location = 1) in vec3 normal;\
	layout(location = 2) in vec2 texCoord;\
	\
	uniform mat4 mvMatrix;\
	uniform mat4 pMatrix;\
	uniform mat3 nMatrix;\
	\
	uniform vec3 lightPos;\
	\
	out vec3 vNormal;\
	out vec3 vLightDir;\
	out vec3 vViewDir;\
	\
	void main()\
	{\
		vec4 osVert = vec4(vertex, 1.0);\
		vec4 esVert = mvMatrix * osVert;\
		vec4 csVert = pMatrix * esVert;\
		\
		vLightDir = (mvMatrix * vec4(lightPos, 0)).xyz;\
		vViewDir = normalize(-esVert.xyz);\
		vNormal = nMatrix * normalize(normal);\
		\
		gl_Position = csVert;\
	}\
	");
}

static ShaderSource *createPhongFragShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	uniform vec3 ambientMat;\
	uniform vec3 diffuseMat;\
	uniform vec3 specularMat;\
	\
	in vec3 vNormal;\
	in vec3 vLightDir;\
	in vec3 vViewDir;\
	\
	out vec4 fragColor;\
	\
	void main()\
	{\
		vec3 ambient = ambientMat;\
		vec3 diffuse = vec3(0.0);\
		vec3 specular = vec3(0.0);\
		\
		float dp = max(dot(vNormal, vLightDir), 0.0);\
		if (dp > 0.0)\
		{\
			diffuse = diffuseMat * dp;\
			vec3 reflection = reflect(-vLightDir, vNormal);\
			float nDotH = max(dot(normalize(vViewDir), normalize(reflection)), 0.0);\
			float shininess = 128.0;\
			float intensity = pow(nDotH, shininess);\
			specular = specularMat * intensity;\
		}\
		vec4 color = vec4(ambient + diffuse + specular, 1.0);\
		fragColor = color;\
	}\
	");
}

static ShaderSource *createMultiSampleVertShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	layout(location = 0) in vec3 vertex;\
	layout(location = 1) in vec3 normal;\
	layout(location = 2) in vec2 texCoord;\
	\
	uniform mat4 mvMatrix;\
	uniform mat4 pMatrix;\
	\
	out vec2 vTexCoord;\
	\
	void main()\
	{\
		vec4 osVert = vec4(vertex, 1.0);\
		vec4 esVert = mvMatrix * osVert;\
		vec4 csVert = pMatrix * esVert;\
		\
		vTexCoord = texCoord;\
		gl_Position = csVert;\
	}\
	");
}

static ShaderSource *createMultiSampleFragShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	vec4 textureFetch(in sampler2DMS tex, in vec2 texCoord, in int nSamples)\
	{\
		ivec2 iTexCoord = ivec2(texCoord * textureSize(tex));\
		vec4 color = vec4(0.0);\
		for (int i = 0; i < nSamples; i++)\
		{\
			color += texelFetch(tex, iTexCoord, i);\
		}\
	\
		return color / nSamples;\
	}\
	\
	in vec2 vTexCoord;\
	\
	uniform sampler2DMS texture;\
	uniform int nSamples;\
	\
	out vec4 fragColor;\
	\
	void main()\
	{\
		fragColor = textureFetch(texture, vTexCoord, nSamples);\
	}\
	");
}

static ShaderSource *createDepthVertShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	layout(location = 0) in vec3 vertex;\
	layout(location = 1) in vec3 normal;\
	layout(location = 2) in vec2 texCoord;\
	\
	uniform mat4 mvMatrix;\
	uniform mat4 pMatrix;\
	\
	void main()\
	{\
		vec4 osVert = vec4(vertex, 1.0);\
		vec4 esVert = mvMatrix * osVert;\
		vec4 csVert = pMatrix * esVert;\
		\
		gl_Position = csVert;\
	}\
	");
}

static ShaderSource *createDepthFragShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	out float fragDepth;\
	\
	void main()\
	{\
		fragDepth = gl_FragCoord.z;\
	}\
	");
}

static ShaderSource *createShadowVertShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	layout(location = 0) in vec3 vertex;\
	layout(location = 1) in vec3 normal;\
	layout(location = 2) in vec2 texCoord;\
	\
	uniform mat4 mvMatrix;\
	uniform mat4 pMatrix;\
	uniform mat4 tMatrix;\
	uniform mat3 nMatrix;\
	\
	uniform vec3 lightPos;\
	\
	out vec4 vShadowCoord;\
	out vec3 vNormal;\
	out vec3 vLightDir;\
	out vec3 vViewDir;\
	\
	void main()\
	{\
		vec4 osVert = vec4(vertex, 1.0);\
		vec4 esVert = mvMatrix * osVert;\
		vec4 csVert = pMatrix * esVert;\
		\
		vShadowCoord = tMatrix * osVert;\
		vLightDir = (mvMatrix * vec4(lightPos, 0)).xyz;\
		vViewDir = normalize(-esVert.xyz);\
		vNormal = nMatrix * normalize(normal);\
		\
		gl_Position = csVert;\
	}\
	");
}

static ShaderSource *createShadowFragShader(const std::string &name)
{
	return new ShaderSource(name, "\
	#version 450\n\
	\
	in vec4 vShadowCoord;\
	\
	in vec3 vNormal;\
	in vec3 vLightDir;\
	in vec3 vViewDir;\
	\
	uniform sampler2DShadow shadowMap;\
	\
	uniform vec3 ambientMat;\
	uniform vec3 diffuseMat;\
	uniform vec3 specularMat;\
	\
	out vec4 fragColor;\
	\
	vec2 poissonDisk[16] = vec2[16](\
		vec2(-0.94201624,  -0.39906216),\
		vec2( 0.94558609,  -0.76890725),\
		vec2(-0.094184101, -0.92938870),\
		vec2( 0.34495938,   0.29387760),\
		vec2(-0.91588581,   0.45771432),\
		vec2(-0.81544232,  -0.87912464),\
		vec2(-0.38277543,   0.27676845),\
		vec2( 0.97484398,   0.75648379),\
		vec2( 0.44323325,  -0.97511554),\
		vec2( 0.53742981,  -0.47373420),\
		vec2(-0.26496911,  -0.41893023),\
		vec2( 0.79197514,   0.19090188),\
		vec2(-0.24188840,   0.99706507),\
		vec2(-0.81409955,   0.91437590),\
		vec2( 0.19984126,   0.78641367),\
		vec2( 0.14383161,  -0.14100790)\
	);\
	\
	float random(vec3 seed, int i)\
	{\
		vec4 seed4 = vec4(seed, i);\
		float dp = dot(seed4, vec4(12.9898, 78.233, 45.164, 94.673));\
		return fract(sin(dp) * 43758.5453);\
	}\
	\
	void main()\
	{\
		float cosTheta = clamp(dot(vNormal, vLightDir), 0.0, 1.0);\
		\
		float bias = 0.005;\
		bias = 0.005 * tan(acos(cosTheta));\
		bias = clamp(bias, 0.0, 0.01);\
		\
		float visibility = 1.0;\
		for (int i = 0; i < 16; i++)\
		{\
			//int index = int(16.0 * random(gl_FragCoord.xyy, i)) % 16;\
			int index = i;\
			visibility -= 0.05 * (1.0 - texture(shadowMap, vec3(vShadowCoord.xy + poissonDisk[index] / 700.0, (vShadowCoord.z - bias) / vShadowCoord.w)));\
		}\
		\
		vec3 ambient = ambientMat;\
		vec3 diffuse = vec3(0.0);\
		vec3 specular = vec3(0.0);\
		\
		float dp = max(cosTheta, 0.0);\
		if (dp > 0.0)\
		{\
			diffuse = diffuseMat * dp;\
			vec3 reflection = reflect(-vLightDir, vNormal);\
			float nDotH = max(dot(normalize(vViewDir), normalize(reflection)), 0.0);\
			float shininess = 128.0;\
			float intensity = pow(nDotH, shininess);\
			specular = specularMat * intensity;\
		}\
		\
		vec3 shadowColor = visibility * diffuse + visibility * specular;\
		vec4 color = vec4(ambient + shadowColor, 1.0);\
		\
		fragColor = color;\
	}\
	");
}

static ShaderSource::InnerSource::Type getHashType(const std::string &src, size_t loc)
{
	const static std::string incStr = "#include";
	const static std::string impStr = "#import";

	if (loc + 2 >= src.length())
		return ShaderSource::InnerSource::NONE;

	// Make sure it's either a #include or #import.
	std::string tag;
	ShaderSource::InnerSource::Type type = ShaderSource::InnerSource::NONE;
	if (src[loc + 2] == 'n')
	{
		tag = incStr;
		type = ShaderSource::InnerSource::INCLUDE;
	}
	else if (src[loc + 2] == 'm')
	{
		tag = impStr;
		type = ShaderSource::InnerSource::IMPORT;
	}
	else
		return ShaderSource::InnerSource::NONE;

	if (loc + tag.length() >= src.length())
		return ShaderSource::InnerSource::NONE;

	size_t i = 0;
	while (loc < src.length() && i < tag.length())
	{
		if (src[loc++] != tag[i++])
			return ShaderSource::InnerSource::NONE;
	}

	return type;
}

static std::pair<std::string, size_t> getHashTag(const std::string &src, size_t loc)
{
	// Skip whitespace.
	// TODO: Make sure extra chars are actually whitespace.
	while (loc < src.length() && src[loc] != '"')
		loc++;
	size_t endLoc = loc + 1;
	while (endLoc < src.length() && src[endLoc] != '"')
		endLoc++;
	return {src.substr(loc + 1, endLoc - loc - 1), endLoc};
}

static std::pair<std::string, std::pair<size_t, size_t>> getDefTag(const std::string &src, size_t loc)
{
	while (loc < src.length() && !isspace(src[loc]))
		loc++;
	size_t midLoc = loc + 1;
	while (midLoc < src.length() && !isspace(src[midLoc]))
		midLoc++;
	size_t endLoc = midLoc + 1;
	while (endLoc < src.length() && !isspace(src[endLoc]))
		endLoc++;
	return {src.substr(loc + 1, midLoc - loc - 1), {midLoc + 1, endLoc - midLoc - 1}};
}



void ShaderSource::preprocessSrc(std::string &newSrc, const std::string &name, const std::string &filename, int totalLines)
{
	// Need to do a little pre-processing to handle #import statements.
	// TODO: A better way of ignoring comments rather than just removing them (I need to keep them for line numbers).
	//Utils::removeComments(newSrc);

	// Need to find imports and includes in the order they appear.

	// Total lines is cumulative, it helps in finding errors 
	size_t lineNo = totalLines;
	size_t loc = newSrc.find_first_of("#\n");
	while (loc != std::string::npos)
	{
		if (newSrc[loc] == '\n')
		{
			lineNo++;
			loc = newSrc.find_first_of("#\n", loc + 1);
			continue;
		}

		// #import or #include (or neither)?
		auto hashType = getHashType(newSrc, loc);
		if (hashType == ShaderSource::InnerSource::NONE)
		{
			loc = newSrc.find_first_of("#\n", loc + 1);
			continue;
		}
		auto tagPair = getHashTag(newSrc, loc);

		newSrc.erase(loc, tagPair.second - loc + 1);

		// Add this stuff to the includes so we can track compile errors later.
		std::string incSrc;
		if (hashType == ShaderSource::InnerSource::INCLUDE)
		{
			// TODO: Is it really necessary to get the path twice?
			auto filePath = Resources::Index::getFilePath(Utils::getFilePath(filename) + tagPair.first);
			auto path = Resources::Index::getFilePath(filePath);
			Utils::File file(path);
			incSrc = file.getStr();
			file.close();

			size_t lineCount = std::count(incSrc.begin(), incSrc.end(), '\n');
			srcIncludes.push_back(InnerSource(InnerSource::INCLUDE, path, lineNo, lineCount));
		}
		else
		{
			incSrc = ShaderSourceCache::getShader(tagPair.first).getSrc();
			size_t lineCount = std::count(incSrc.begin(), incSrc.end(), '\n');
			srcIncludes.push_back(InnerSource(InnerSource::IMPORT, tagPair.first, lineNo, lineCount));
		}
		preprocessSrc(incSrc, name, filename, (int) lineNo);
		newSrc.insert(loc, incSrc); // TODO: Do we actually want to insert here or not?

		loc = newSrc.find_first_of("#\n", loc + 1);
	}
}

void ShaderSource::getSrcFromFile(const std::string &filename)
{
	srcIncludes.clear();

	// Is it worth having the whole file also be a srcInclude? Might allow easier error reporting...
	auto path = Resources::Index::getFilePath(filename);
	Utils::File file(path);
	src = file.getStr();
	file.close();

	preprocessSrc(src, name, filename);
}



void ShaderSource::initDefaultShaders(std::map<std::string, ShaderSource*> &cache)
{
	if (initFlag)
		return;
	cache["defaultVert"] = createBasicVertShader("defaultVert");
	cache["defaultFrag"] = createBasicFragShader("defaultFrag");
	cache["texVert"] = createTexVertShader("texVert");
	cache["texFrag"] = createTexFragShader("texFrag");
	cache["phongVert"] = createPhongVertShader("phongVert");
	cache["phongFrag"] = createPhongFragShader("phongFrag");
	cache["msVert"] = createMultiSampleVertShader("msVert");
	cache["msFrag"] = createMultiSampleFragShader("msFrag");
	cache["depthVert"] = createDepthVertShader("depthVert");
	cache["depthFrag"] = createDepthFragShader("depthFrag");
	cache["shadowVert"] = createShadowVertShader("shadowVert");
	cache["shadowFrag"] = createShadowFragShader("shadowFrag");
	initFlag = true;
}

ShaderSource &ShaderSource::load(const std::string &src, bool printSrc)
{
	this->src = src;
	srcIncludes.clear();

	preprocessSrc(this->src, name, "");

	if (printSrc)
		std::cout << this->src << "\n";
	return *this;
}

ShaderSource &ShaderSource::loadFromFile(const std::string &filename, bool printSrc)
{
	srcIncludes.clear();
	getSrcFromFile(filename);

	if (printSrc)
		std::cout << this->src << "\n";
	return *this;
}

void ShaderSource::setDefine(const std::string &def, const std::string &val)
{
	size_t loc = src.find("#define");
	while (loc != std::string::npos)
	{
		auto tagPair = getDefTag(src, loc);
		if (tagPair.first != def)
		{
			loc = src.find("#define", loc + 1);
			continue;
		}

		auto midLoc = tagPair.second.first;
		auto len = tagPair.second.second;

		src.erase(midLoc, len);
		src.insert(midLoc, val);
		loc = src.find("#define", loc + 1);
	}
}

void ShaderSource::replace(const std::string &tag, const std::string &val)
{
	size_t loc = src.find(tag);
	while (loc != std::string::npos)
	{
		src.replace(loc, tag.length(), val);
		loc = src.find(tag, loc + 1);
	}
}

#if 0
ShaderSource &ShaderSource::create(const std::string &name, const std::string &src, bool printSrc)
{
	std::cout << "Create1 is being called from somewhere\n";
	ShaderSource source;
	source.load(name, src, printSrc);
	return srcMap[name];
}

ShaderSource &ShaderSource::createFromFile(const std::string &name, const std::string &filename, bool printSrc)
{
	std::cout << "Create2 is being called from somewhere\n";
	ShaderSource src;
	src.loadFromFile(name, filename, printSrc);
	return srcMap[name];
}

bool ShaderSource::exists(const std::string &name)
{
	return srcMap.find(name) != srcMap.end();
}

ShaderSource &ShaderSource::getShader(const std::string &name)
{
	static bool initFlag = false;

	if (!initFlag)
	{
		srcMap["defaultVert"] = createBasicVertShader("defaultVert");
		srcMap["defaultFrag"] = createBasicFragShader("defaultFrag");

		srcMap["texVert"] = createTexVertShader("texVert");
		srcMap["texFrag"] = createTexFragShader("texFrag");

		srcMap["phongVert"] = createPhongVertShader("phongVert");
		srcMap["phongFrag"] = createPhongFragShader("phongFrag");

		srcMap["msVert"] = createMultiSampleVertShader("msVert");
		srcMap["msFrag"] = createMultiSampleFragShader("msFrag");

		srcMap["depthVert"] = createDepthVertShader("depthVert");
		srcMap["depthFrag"] = createDepthFragShader("depthFrag");

		srcMap["shadowVert"] = createShadowVertShader("shadowVert");
		srcMap["shadowFrag"] = createShadowFragShader("shadowFrag");

		initFlag = true;
	}

	auto it = srcMap.find(name);
	if (it == srcMap.end())
		std::cout << "Failed to find shader with name " << name << "\n";
	assert(it != srcMap.end());

	return it->second;
}
#endif

