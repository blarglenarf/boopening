#include "Font.h"
#include "ShaderSource.h"
#include "ImageAtlas.h"
#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Shader.h"
#include "../../../Resources/src/Font/FreetypeFont.h"
#include "../../../Platform/src/Window.h"
#include "../../../Math/src/Matrix/Matrix4.h"

#include <iostream>

// TODO: Take width/height into account, update cached text if its width/height changes.

using namespace Rendering;

GLuint debugFontTex = 0;

Shader *Font::defaultCacheShader = nullptr;
Shader *Font::defaultAtlasShader = nullptr;
VertexArrayObject *Font::atlasVao = nullptr;


static std::string getCacheFontVert()
{
	return "\
	#version 430\n\
	\
	layout(location = 0) in vec2 vertex;\
	\
	uniform ivec2 pos;\
	\
	out vec2 vert;\
	\
	void main()\
	{\
		vert = pos;\
	}";
}

static std::string getCacheFontGeom()
{
	return "\
	#version 430\n\
	\
	layout(points) in;\n\
	layout(triangle_strip, max_vertices = 4) out;\n\
	\
	in vec2 vert[1];\
	\
	uniform mat4 pMatrix;\
	uniform ivec2 size;\
	uniform ivec2 scroll;\
	uniform float z;\
	\
	out vec2 texCoord;\
	\
	void main()\
	{\
		vec4 esVert;\
		\
		esVert = vec4(vert[0].x - scroll.x, vert[0].y - scroll.y, z, 1);\
		texCoord = vec2(0, 0);\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vert[0].x - scroll.x, vert[0].y + size.y - scroll.y, z, 1);\
		texCoord = vec2(0, 1);\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vert[0].x + size.x - scroll.x, vert[0].y - scroll.y, z, 1);\
		texCoord = vec2(1, 0);\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vert[0].x + size.x - scroll.x, vert[0].y + size.y - scroll.y, z, 1);\
		texCoord = vec2(1, 1);\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		EndPrimitive();\
	}";
}

static std::string getCacheFontFrag()
{
	return "\
	#version 430\n\
	\
	in vec2 texCoord;\
	\
	uniform sampler2D texture;\
	uniform vec3 color;\
	uniform ivec2 size;\
	uniform ivec4 box;\
	\
	out vec4 fragColor;\
	\
	void main()\
	{\
		if (gl_FragCoord.x < box.x || gl_FragCoord.x > box.y || gl_FragCoord.y > box.z || gl_FragCoord.y < box.w)\
		{\
			discard;\
			return;\
		}\
		float a = texture2D(texture, texCoord).a;\
		fragColor = vec4(color.x, color.y, color.z, a);\
	}";
}

static std::string getAtlasFontVert()
{
	return "\
	#version 430\n\
	\
	layout(location = 0) in vec4 pos;\
	layout(location = 1) in vec2 tex;\
	\
	uniform ivec2 scroll;\
	\
	out ivec4 vertPos;\
	out ivec2 texPos;\
	\
	void main()\
	{\
		vertPos = ivec4(pos.x, pos.y, pos.z, pos.w);\
		vertPos.x -= scroll.x;\
		vertPos.y += scroll.y;\
		texPos = ivec2(tex.x, tex.y);\
	}\
	";
}

static std::string getAtlasFontGeom()
{
	return "\
	#version 430\n\
	\
	layout(points) in;\n\
	layout(triangle_strip, max_vertices = 4) out;\n\
	\
	in ivec4 vertPos[1];\
	in ivec2 texPos[1];\
	\
	uniform mat4 pMatrix;\
	uniform float z;\
	\
	flat out ivec2 vertOffset;\
	flat out ivec2 texOffset;\
	flat out int height;\
	\
	void main()\
	{\
		vec4 esVert;\
		\
		esVert = vec4(vertPos[0].x, vertPos[0].y + vertPos[0].w, z, 1);\
		vertOffset = vertPos[0].xy;\
		texOffset = texPos[0];\
		height = vertPos[0].w;\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vertPos[0].x, vertPos[0].y, z, 1);\
		vertOffset = vertPos[0].xy;\
		texOffset = texPos[0];\
		height = vertPos[0].w;\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vertPos[0].x + vertPos[0].z, vertPos[0].y + vertPos[0].w, z, 1);\
		vertOffset = vertPos[0].xy;\
		texOffset = texPos[0];\
		height = vertPos[0].w;\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vertPos[0].x + vertPos[0].z, vertPos[0].y, z, 1);\
		vertOffset = vertPos[0].xy;\
		texOffset = texPos[0];\
		height = vertPos[0].w;\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		EndPrimitive();\
	}\
	";
}

static std::string getAtlasFontFrag()
{
	return "\
	#version 430\n\
	\
	flat in ivec2 vertOffset;\
	flat in ivec2 texOffset;\
	flat in int height;\
	\
	uniform sampler2D texture;\
	uniform vec3 color;\
	uniform ivec4 box;\
	\
	out vec4 fragColor;\
	\
	void main()\
	{\
		ivec2 pixelPos = ivec2(gl_FragCoord.xy - vertOffset);\
		if (gl_FragCoord.x < box.x || gl_FragCoord.x > box.y || gl_FragCoord.y > box.z || gl_FragCoord.y < box.w)\
		{\
			discard;\
			return;\
		}\
		float a = texelFetch(texture, ivec2(texOffset.x + pixelPos.x, texOffset.y + (height - pixelPos.y)), 0).a;\
		\
		fragColor = vec4(color.x, color.y, color.z, a);\
	}";
}


void Font::init()
{
	if (!defaultCacheShader)
	{
		defaultCacheShader = new Shader();
		auto vertSrc = ShaderSource("cacheFontVert", getCacheFontVert());
		auto fragSrc = ShaderSource("cacheFontFrag", getCacheFontFrag());
		auto geomSrc = ShaderSource("cacheFontGeom", getCacheFontGeom());
		defaultCacheShader->create(&vertSrc, &fragSrc, &geomSrc);
	}

	if (!defaultAtlasShader)
	{
		defaultAtlasShader = new Shader();
		auto vertSrc = ShaderSource("atlasFontVert", getAtlasFontVert());
		auto fragSrc = ShaderSource("atlasFontFrag", getAtlasFontFrag());
		auto geomSrc = ShaderSource("atlasFontGeom", getAtlasFontGeom());
		defaultAtlasShader->create(&vertSrc, &fragSrc, &geomSrc);
	}

	if (!atlasVao)
		atlasVao = new VertexArrayObject(GL_POINTS);
}


Font::~Font()
{
	delete ftFont;

	clearCache();
	clearAtlas();
}

void Font::setSize(int size)
{
	if (ftFont)
		ftFont->setSize(size);
}

int Font::getSize() const
{
	if (!ftFont)
		return 0;
	return ftFont->getSize();
}

void Font::clearCache()
{
	for (auto it : cache)
	{
		GLuint tex = it.second.first;
		glDeleteTextures(1, &tex);
	}
}

void Font::clearAtlas()
{
	glDeleteTextures(1, &atlasTexture);
}

void Font::debugRender()
{
	// Basically just here to make sure freetype is giving useful font data.
	setSize(48);
	debugRender("Font testing\nMore font testing");
}

void Font::debugRender(const std::string &str, int xPos, int yPos, int linePadding)
{
	if (str.empty())
		return;

	// This is much slower than the other font rendering approaches. Only use it for debugging.
	if (!ftFont)
		return;

	glPushAttrib(GL_TRANSFORM_BIT | GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT | GL_TEXTURE_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, Platform::Window::getWidth(), 0, Platform::Window::getHeight(), -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef((GLfloat) xPos, (GLfloat) yPos, 0);
	glLoadIdentity();

	//glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0);

	if (debugFontTex == 0)
		glGenTextures(1, &debugFontTex);

	glBindTexture(GL_TEXTURE_2D, debugFontTex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	Math::ivec2 size;
	std::vector<Resources::FreetypeFont::Glyph> glyphs;
	ftFont->getGlyphs(str, 0, size, glyphs, linePadding);
	auto fontSize = ftFont->getSize();

	for (auto &glyph : glyphs)
	{
		auto bitmap = glyph.convertToBitmap();

		// Documentation says GL_ALPHA isn't a valid format, but it seems to work.
		glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, bitmap->bitmap.width, bitmap->bitmap.rows, 0, GL_ALPHA, GL_UNSIGNED_BYTE, bitmap->bitmap.buffer);

		int w = bitmap->bitmap.width;
		int h = bitmap->bitmap.rows;
		int x = glyph.x + bitmap->left;
		int y = size.y - fontSize - glyph.y + bitmap->top - h;

		glBegin(GL_QUADS);
		glTexCoord2f(0, 1); glVertex3i(x, y, 0);
		glTexCoord2f(1, 1); glVertex3i(x + w, y, 0);
		glTexCoord2f(1, 0); glVertex3i(x + w, y + h, 0);
		glTexCoord2f(0, 0); glVertex3i(x, y + h, 0);
		glEnd();
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glPopAttrib();
}

void Font::storeInCache(const std::string &str, int width, int height, int linePadding)
{
	(void) (height);

	if (str.empty())
		return;

	if (!ftFont)
	{
		std::cout << "Cannot store text in cache for non-existing font\n";
		return;
	}

	GLuint tex = 0;
	auto hash = hashes.getHash(str);

	glPushAttrib(GL_ENABLE_BIT | GL_TEXTURE_BIT | GL_COLOR_BUFFER_BIT);

	glEnable(GL_TEXTURE_2D);

	// Check cache to see if it's already there, if so, overwrite.
	if (hashes.existsHash(hash))
		tex = cache[hash].first;
	else
	{
		glGenTextures(1, &tex);
		cache[hash] = {tex, Math::ivec2(0, 0)};
		hashes.addHash(hash);
	}

	Math::ivec2 size;
	std::vector<Resources::FreetypeFont::Glyph> glyphs;
	ftFont->getGlyphs(str, width, size, glyphs, linePadding);
	auto fontSize = ftFont->getSize();

	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// Need to fill the texture with 0s.
	unsigned char *empty = (unsigned char *) calloc(size.x * size.y, sizeof(unsigned char));
	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, size.x, size.y, 0, GL_ALPHA, GL_UNSIGNED_BYTE, empty);
	free(empty);

	for (auto &glyph : glyphs)
	{
		auto bitmap = glyph.convertToBitmap();

		int w = bitmap->bitmap.width;
		int h = bitmap->bitmap.rows;
		int x = glyph.x + bitmap->left;
		int y = glyph.y + fontSize - bitmap->top;
		//int y = size.y - fontSize - glyph.y + bitmap->top - h;

		// TODO: Wrapping based on width/height.
		//if ((width == 0 || x + w <= width) && (height == 0 || y + h <= height))
		glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, GL_ALPHA, GL_UNSIGNED_BYTE, bitmap->bitmap.buffer);
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopAttrib();

	cache[hash].second = size;
}

void Font::renderFromCache(const std::string &str, int x, int y, float z, float r, float g, float b, int width, int height, int linePadding, int vertScroll, int sideScroll, bool wrapFlag)
{
	if (str.empty())
		return;

	init();

	// If it's not cached, then cache it.
	auto hash = hashes.getHash(str);
	if (!hashes.existsHash(hash))
		storeInCache(str, wrapFlag ? width : 0, height, linePadding);

	auto cacheData = cache[hash];
	auto tex = cacheData.first;
	auto size = cacheData.second;

	// TODO: Get the uniform locations before rendering so they can be re-used.

	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT | GL_TEXTURE_BIT);

	//glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0);

	defaultCacheShader->bind();

	// Currently the projection is flipped (ie 0,0 is top left).
	Math::mat4 pMatrix = Math::getOrtho<float>(0, Platform::Window::getWidth(), Platform::Window::getHeight(), 0, -1000, 1000);

	glBindTexture(GL_TEXTURE_2D, tex);

	defaultCacheShader->setUniform("pMatrix", pMatrix);
	defaultCacheShader->setUniform("pos", Math::ivec2(x, y));
	defaultCacheShader->setUniform("size", size);
	defaultCacheShader->setUniform("color", Math::vec3(r, g, b));
	defaultCacheShader->setUniform("scroll", Math::ivec2(sideScroll, vertScroll));

	if (width == 0)
		width = 100000;
	if (height == 0)
		height = 100000;
	int left = x, right = x + width;
	int top = Platform::Window::getHeight() - y;
	int bottom = Platform::Window::getHeight() - (y + height);
	defaultCacheShader->setUniform("box", Math::ivec4(left, right, top, bottom));
	defaultCacheShader->setUniform("z", z);

	auto loc = defaultCacheShader->getUniform("texture");
	glUniform1i(loc, 0);

	glDrawArrays(GL_POINTS, 0, 1);

	glBindTexture(GL_TEXTURE_2D, 0);

	defaultCacheShader->unbind();

	glPopAttrib();
}

void Font::storeInAtlas(const std::string &str)
{
	if (str.empty())
		return;

	// TODO: Allow atlas to resize based on str data.
	if (!regions.empty())
	{
		atlas.clear();
		regions.clear();
	}

	// TODO: Better texture size for fonts maybe?
	int width = 512;
	int height = 512;

	glPushAttrib(GL_ENABLE_BIT | GL_TEXTURE_BIT | GL_COLOR_BUFFER_BIT);

	glEnable(GL_TEXTURE_2D);

	atlas.init(width, height);

	if (atlasTexture == 0)
		glGenTextures(1, &atlasTexture);

	glBindTexture(GL_TEXTURE_2D, atlasTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// Need to fill the texture with 0s.
	unsigned char *empty = (unsigned char *) calloc(width * height, sizeof(unsigned char));
	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, width, height, 0, GL_ALPHA, GL_UNSIGNED_BYTE, empty);
	free(empty);

	for (char c : str)
	{
		// Handle whitespace characters as a special case.
		if (c == ' ' || c == '\n' || c == '\t' || c == '\r')
			continue;

		Math::ivec2 size;
		Resources::FreetypeFont::Glyph glyph;
		ftFont->getGlyph(c, size, glyph);
		//auto fontSize = ftFont->getSize();
		auto bitmap = glyph.convertToBitmap();

		auto id = atlas.add(bitmap->bitmap.width, bitmap->bitmap.rows);
		regions[c] = {id, (int) bitmap->bitmap.rows - (int) bitmap->top, bitmap->left, size.x};

		auto rect = atlas.getRealCoords(id);

		int x = rect.left();
		int y = rect.bottom();
		int w = rect.right() - rect.left();
		int h = rect.top() - rect.bottom();

		glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, w, h, GL_ALPHA, GL_UNSIGNED_BYTE, bitmap->bitmap.buffer);
	}

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopAttrib();

	// Special case for ' ' char which stores width, then left.
	Math::ivec2 size;
	Resources::FreetypeFont::Glyph glyph;
	ftFont->getGlyph(' ', size, glyph);
	auto bitmap = glyph.convertToBitmap();
	regions[' '] = {0, size.x, bitmap->left};

	// TODO: Special case for '\t' char.
}

void Font::renderFromAtlas(const std::string &str, int x, int y, float z, float r, float g, float b, int width, int height, int linePadding, int vertScroll, int sideScroll)
{
	if (str.empty())
		return;

	// To render a character, you need bottom left of quad, width/height of quad and bottom left of texture.
	struct AtlasVert
	{
		Math::vec4 rect;
		Math::vec2 tex;
	};
	static std::vector<AtlasVert> quads;
	static VertexFormat format({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT}, {VertexAttrib::TEXCOORD, 1, 2, GL_FLOAT} });

	if (quads.size() < str.size())
		quads.resize(str.size());

	init();

	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT);
	//glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0);

	defaultAtlasShader->bind();

	// In this case projection is not flipped (ie 0,0 is bottom left).
	Math::mat4 pMatrix = Math::getOrtho<float>(0, Platform::Window::getWidth(), 0, Platform::Window::getHeight(), -1000, 1000);

	glBindTexture(GL_TEXTURE_2D, atlasTexture);

	defaultAtlasShader->setUniform("pMatrix", pMatrix);
	defaultAtlasShader->setUniform("color", Math::vec3(r, g, b));
	defaultAtlasShader->setUniform("scroll", Math::ivec2(sideScroll, vertScroll));

	int tmpWidth = width, tmpHeight = height;
	if (tmpWidth == 0)
		tmpWidth = 100000;
	if (tmpHeight == 0)
		tmpHeight = 100000;
	int left = x, right = x + tmpWidth;
	int top = Platform::Window::getHeight() - y;
	int bottom = Platform::Window::getHeight() - (y + tmpHeight);
	defaultAtlasShader->setUniform("box", Math::ivec4(left, right, top, bottom));
	defaultAtlasShader->setUniform("z", z);

	auto loc = defaultAtlasShader->getUniform("texture");
	glUniform1i(loc, 0);

	auto fontSize = ftFont->getSize();

	// TODO: Handle whitespace and newline.
	int xPos = x, yPos = y;
	size_t nWhitespace = 0;
	size_t nChars = 0;
	for (size_t i = 0; i < str.size(); i++)
	{
		char c = str[i];

		if (c == '\n')
		{
			yPos += fontSize;
			yPos += linePadding;
			xPos = x;
		}

		bool tooLow = height != 0 && yPos - y - vertScroll > height;
		bool tooHigh = height != 0 && yPos - vertScroll + fontSize < y;
		if (tooLow)
			break;
		if (tooHigh)
			continue;

		if (c == '\n' || c == '\r' || c == '\t')
		{
			nWhitespace++;
			continue;
		}

		auto id = regions[c].id;
		auto rect = atlas.getRealCoords(id);

		int offset = regions[c].offset;
		int left = regions[c].left;

		if (c == ' ')
		{
			// Special case for ' ' char which stores width, then left.
			xPos += offset + left;
			nWhitespace++;
			continue;
		}

		bool tooLeft = width != 0 && xPos + left + rect.width() - sideScroll < x;
		bool tooRight = width != 0 && xPos + left - x - sideScroll - rect.width() > width;
		if (tooLeft || tooRight)
		{
			xPos += regions[c].advance;
			continue;
		}

		// FIXME: Why do I need to subtract 1 from here?
		quads[nChars++] = { Math::vec4(xPos + left, Platform::Window::getHeight() - yPos - fontSize - offset, rect.width(), rect.height()), Math::vec2(rect.left(), rect.bottom() - 1) };

		xPos += regions[c].advance;
	}

	// FIXME: Not thread safe, not even a little...
	atlasVao->create(format, &quads[0].rect.x, sizeof(AtlasVert) * nChars, sizeof(AtlasVert));
	atlasVao->render();

	glBindTexture(GL_TEXTURE_2D, 0);

	defaultAtlasShader->unbind();

	glPopAttrib();
}

Math::ivec2 Font::getTextSize(const std::string &str, int width, int height, int linePadding)
{
	(void) (height);
	return ftFont->getDimensions(str, width, linePadding);
}

Math::ivec3 Font::getCursorPos(const std::string &str, int xOffset, int yOffset, int charPos, int width, int linePadding, int vertScroll, int sideScroll)
{
	// TODO: Implement me!
	// TODO: Use vertScroll and sideScroll.
	(void) (vertScroll);
	(void) (sideScroll);
	return ftFont->getCursorPos(str, xOffset, yOffset, charPos, width, linePadding);
}

void Font::blit()
{
	// FIXME: This is currently flipped, as all the characters are flipped when they are inserted into the atlas.
	int width = Platform::Window::getWidth();
	int height = Platform::Window::getHeight();

	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT | GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT);

	glClearColor(0, 0, 0, 0);
	glClear(GL_COLOR_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, width, 0, height, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, atlasTexture);

	glColor4f(1, 1, 1, 1);

	glBegin(GL_QUADS);
	glTexCoord2f(0, 1); glVertex2f(0, 0);
	glTexCoord2f(0, 0); glVertex2f(0, (GLfloat) atlas.getHeight());
	glTexCoord2f(1, 0); glVertex2f((GLfloat) atlas.getWidth(), (GLfloat) atlas.getHeight());
	glTexCoord2f(1, 1); glVertex2f((GLfloat) atlas.getWidth(), 0);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
}

