#include "ImageAtlas.h"
#include "../../../Platform/src/Window.h"

#include <iostream>
#include <queue>
#include <cstring>

using namespace Rendering;

ImageAtlas::ImageAtlas(int size, int channels, Math::ivec2 dimensions, bool ownsImages) : Image(), ownsImages(ownsImages)
{
	init(size, channels, dimensions, ownsImages);
}

void ImageAtlas::init(int size, int channels, Math::ivec2 dimensions, bool ownsImages)
{
	this->ownsImages = ownsImages;

	unsigned char *data = new unsigned char[size];
	memset(data, 0, size);

	load(data, size, channels, dimensions);

	atlas.clear();
	atlas.init(dimensions.width, dimensions.height);
}

ImageAtlas::~ImageAtlas()
{
	if (!ownsImages)
		return;
	for (auto it : regions)
		delete it.first;
}

bool ImageAtlas::add(Image *image)
{
	if (image->getChannels() != getChannels() || image->getFormat() != getFormat())
	{
		std::cout << "Unable to insert image into atlas\n";
		return false;
	}

	size_t id = atlas.add(image->getDimensions().width, image->getDimensions().height);
	regions[image] = id;

	auto rect = atlas.getRealCoords(id);

	unsigned int imgPos = 0;

	for (int y = rect.bottom(); y < rect.top(); y++)
		for (int x = rect.left(); x < rect.right(); x++)
			for (int i = 0; i < getChannels(); i++)
				getData()[getPixelIndex(x, y) + i] = image->getData()[imgPos++];

	return true;
}

bool ImageAtlas::addBlank(int width, int height)
{
	size_t id = atlas.add(width, height);
	//regions[image] = id; // FIXME: Region isn't added.
	//auto rect = atlas.getRealCoords(id);

	(void) (id);

	return true;
}

bool ImageAtlas::hasImage(Image *image)
{
	return regions.find(image) != regions.end();
}

Math::RectF ImageAtlas::getTexCoords(Image *image)
{
	auto it = regions.find(image);
	if (it == regions.end())
	{
		std::cout << "Image doesn't exist in atlas\n";
		return Math::RectF();
	}
	auto id = it->second;
	return atlas.getTexCoords(id);
}

Math::RectI ImageAtlas::getRealCoords(Image *image)
{
	auto it = regions.find(image);
	if (it == regions.end())
	{
		std::cout << "Image doesn't exist in atlas\n";
		return Math::RectI();
	}
	auto id = it->second;
	return atlas.getRealCoords(id);
}

void ImageAtlas::blit(bool lineFlag)
{
	Image::blit();

	if (!lineFlag)
		return;

	auto width = Platform::Window::getWidth();
	auto height = Platform::Window::getHeight();

	glPushAttrib(GL_POLYGON_BIT | GL_ENABLE_BIT | GL_TRANSFORM_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_TEXTURE_2D);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, width, 0, height, -1, 1);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	std::queue<Utils::Atlas::AtlasRegion*> regionQueue;
	regionQueue.push(atlas.getHead());

	while (!regionQueue.empty())
	{
		auto *curr = regionQueue.front();
		regionQueue.pop();

		if (curr->getLeft() || curr->getRight())
		{
			regionQueue.push(curr->getLeft());
			regionQueue.push(curr->getRight());
		}

		float x = (float) curr->getRect().left();
		float y = (float) curr->getRect().bottom();
		float right = (float) curr->getRect().right();
		float top = (float) curr->getRect().top();

		glBegin(GL_QUADS);
		glVertex2f(x, y);
		glVertex2f(x, top);
		glVertex2f(right, top);
		glVertex2f(right, y);
		glEnd();
	}

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
}

