#pragma once

#include <string>
#include <map>

namespace Rendering
{
	class Material;
	class ShaderSource;
	class Image;
	class Mesh;
	class Font;

	class MaterialCache
	{
	private:
		static std::map<std::string, Material*> cache;

	public:
		static void clear();
		static bool getMaterial(const std::string &name, Material *&mat);
		static bool exists(const std::string &name);
		static Material &getMaterial(const std::string &name);
		static Material &addMaterial(Material *mat);
		static Material *removeMaterial(const std::string &name);
		static Material *removeMaterial(Material *mat);
	};

	class ShaderSourceCache
	{
	private:
		static std::map<std::string, ShaderSource*> cache;

	public:
		static void clear();
		static bool getShader(const std::string &name, ShaderSource *&src);
		static bool exists(const std::string &name);
		static ShaderSource &getShader(const std::string &name);
		static ShaderSource &addShader(ShaderSource *src);
		static ShaderSource *removeShaderSource(const std::string &name);
		static ShaderSource *removeShaderSource(ShaderSource *src);
	};

	class ImageCache
	{
	private:
		static std::map<std::string, Image*> cache;

	public:
		static void clear();
		static bool getImage(const std::string &name, Image *&img);
		static bool exists(const std::string &name);
		static Image &getImage(const std::string &name);
		static Image &addImage(Image *img);
		static Image *removeImage(const std::string &name);
		static Image *removeImage(Image *img);
	};

	class MeshCache
	{
	private:
		static std::map<std::string, Mesh*> cache;

	public:
		static void clear();
		static bool getMesh(const std::string &name, Mesh *&mesh);
		static bool exists(const std::string &name);
		static Mesh &getMesh(const std::string &name);
		static Mesh &addMesh(Mesh *mesh);
		static Mesh *removeMesh(const std::string &name);
		static Mesh *removeMesh(Mesh *mesh);
	};

	class FontCache
	{
	private:
		static std::map<std::string, Font*> cache;

	public:
		static void clear();
		static bool getFont(const std::string &name, Font *&font);
		static bool exists(const std::string &name);
		static Font &getFont(const std::string &name);
		static Font &addFont(Font *font);
		static Font *removeFont(const std::string &name);
		static Font *removeFont(Font *font);
	};
}

