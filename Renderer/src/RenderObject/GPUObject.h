#pragma once

#include "../GL.h"

#include <string>
#include <vector>

namespace Rendering
{
	class Shader;

	class GPUObject
	{
	protected:
		std::string name;

		GLuint object;
		GLenum type;

		bool boundFlag;

	public:
		GPUObject(GLenum type) : object(0), type(type), boundFlag(false) {}
		virtual ~GPUObject() {}

		operator GLuint () const { return object; }

		GLuint getObject() const { return object; }
		GLenum getType() const { return type; }

		virtual void generate() = 0;
		virtual void release() = 0;

		virtual void bind() = 0;
		virtual void unbind() = 0;

		virtual bool setUniformInShader(const Shader *shader, const std::string &name);
		virtual void setUniformInShader(const Shader *shader, GLint loc);

		bool isGenerated() const { return object != 0; }
		bool isBound() const { return boundFlag; }

		void setName(const std::string &name) { this->name = name; }
		const std::string &getName() const { return name; }
	};

	class GPUObjectGroup
	{
	private:
		std::vector<GPUObject*> uniques;

	public:
		int add(GPUObject *obj);
		void remove(GPUObject *obj);
		int getIndex(GPUObject *obj);
		bool exists(GPUObject *obj);

		std::vector<GPUObject*>::iterator begin() { return uniques.begin(); }
		std::vector<GPUObject*>::iterator end() { return uniques.end(); }

		void clear() { uniques.clear(); }
	};
}
