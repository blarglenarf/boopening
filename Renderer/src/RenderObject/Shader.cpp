#include "Shader.h"
#include "../GL.h"
#include "../RenderResource/ShaderSource.h"

//#include <stdexcept>
#include <iostream>

using namespace Rendering;

Shader::ShaderStack Shader::shaderStack;
Shader *Shader::currBound = nullptr;
Utils::StringHash Shader::errorCache;

static void shaderError(GLuint shader, GLuint errorCheck, const std::string &msgType, std::vector<ShaderSource::InnerSource> &srcIncludes, bool programFlag = false)
{
	GLint param;
	if (!programFlag)
		glGetShaderiv(shader, errorCheck, &param);
	else
		glGetProgramiv(shader, errorCheck, &param);
	if (param == GL_FALSE)
	{
		int infoLogLength = 0;
		int maxLength = 0;
		char *infoLog;

		if (!programFlag)
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
		else
			glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &maxLength);
		infoLog = new char[(size_t) maxLength + 1];
		if (!programFlag)
			glGetShaderInfoLog(shader, maxLength, &infoLogLength, infoLog);
		else
			glGetProgramInfoLog(shader, maxLength, &infoLogLength, infoLog);
		infoLog[maxLength] = 0;

		std::string error = infoLog;
		delete [] infoLog;
		glDeleteShader(shader);

		// If extra files have been included/imported then the line number is wrong. Need to account for this.
		int lineNo = 0;
		if (!srcIncludes.empty())
		{
			// Find first of '(', get substring up to ')', this gives line number.
			auto start = error.find_first_of("(");
			auto end = error.find_first_of(")", start);
			if (start == std::string::npos || end == std::string::npos)
			{
				std::cout << "Failed to compile " + msgType + "shader, reason: " + error << "\n";
				return;
			}
			auto lineStr = error.substr(start + 1, end - 2);
			lineNo = std::stoi(lineStr);
		}

		// Adjust line number based on includes/imports and get the actual filename where the error occured.
		std::string incName;
		if (!srcIncludes.empty())
		{
			int incLines = 0;
			bool found = false;
			for (size_t i = 0; i < srcIncludes.size(); i++)
			{
				// Line number is before this include, ignore it and everything after.
				if (lineNo < (int) srcIncludes[i].lineNo)
					break;
				// Line number is inside this include.
				if (lineNo > (int) srcIncludes[i].lineNo && lineNo < (int) srcIncludes[i].lineNo + (int) srcIncludes[i].lineCount)
				{
					incLines = (int) srcIncludes[i].lineNo - 1;
					incName = srcIncludes[i].name;
					found = true;
				}
				if (!found)
					incLines += (int) srcIncludes[i].lineCount;
			}
			lineNo -= incLines;
		}

		if (!incName.empty())
			incName = " in " + incName;
		std::string msg = "Failed to compile " + msgType + " shader at line " + Utils::toString(lineNo) + incName + ", reason: " + error;
		std::cout << msg << "\n";
	}
}

static GLuint getShader(const char *src, int len, GLuint type, const std::string &msgType, std::vector<ShaderSource::InnerSource> &srcIncludes)
{
	GLuint shader = glCreateShader(type);

	glShaderSource(shader, 1, &src, &len);
	glCompileShader(shader);

	shaderError(shader, GL_COMPILE_STATUS, msgType, srcIncludes);

	return shader;
}

static void attachShader(const std::string &shaderSrc, const std::string &name, GLuint object, GLuint type, std::vector<ShaderSource::InnerSource> &srcIncludes)
{
	GLuint shader = getShader(shaderSrc.c_str(), (int) shaderSrc.length(), type, name, srcIncludes);

	glAttachShader(object, shader);
	glDeleteShader(shader);
}



void Shader::ShaderStack::pop()
{
	if (stack.empty())
		return;

	stack.erase(stack.begin());
	bindTopShader();
}

void Shader::ShaderStack::bind(Shader *shader)
{
	stack.push_front(shader);
	shader->glBind();
}

void Shader::ShaderStack::unbind(Shader *shader)
{
	if (stack.empty())
		return;

	for (auto it = stack.begin(); it != stack.end(); it++)
	{
		if (*it != shader)
			continue;
		stack.erase(it);
		break;
	}

	shader->glUnbind();
	bindTopShader();
}

void Shader::ShaderStack::remove(Shader *shader)
{
	if (stack.empty())
		return;

	for (auto it = stack.begin(); it != stack.end(); )
	{
		if (*it == shader)
			it = stack.erase(it);
		else
			it++;
	}

	if (!shader->isBound())
		return;

	shader->glUnbind();
	bindTopShader();
}

void Shader::ShaderStack::bindTopShader()
{
	if (!stack.empty())
		stack.front()->glBind();
}



void Shader::glBind()
{
	glUseProgram(object);
	boundFlag = true;
	currBound = this;
}

void Shader::glUnbind()
{
	glUseProgram(0);
	boundFlag = false;
	if (currBound == this)
		currBound = nullptr;
}

void Shader::logError(const std::string &msg)
{
	if (!errorCache.existsStr(msg))
	{
		errorCache.addStr(msg);
		std::cout << msg << "\n";
	}
}



Shader::Shader() : GPUObject(0)
{
}

Shader::~Shader()
{
	release();
}

void Shader::generate()
{
	if (object == 0)
		object = glCreateProgram();
}

void Shader::release()
{
	shaderStack.remove(this);

	if (object == 0)
		return;
	glDeleteProgram(object);
	object = 0;
}

void Shader::bind()
{
	shaderStack.bind(this);
}

void Shader::unbind()
{
	shaderStack.unbind(this);
}

void Shader::bufferData(ShaderSource *vertSrc, ShaderSource *fragSrc, ShaderSource *geomSrc, ShaderSource *tcSrc, ShaderSource *teSrc)
{
	if (vertSrc)
	{
		vertSrcIncludes = vertSrc->getSrcIncludes();
		vertName = vertSrc->getName();
		attachVert(vertSrc->getSrc());
	}
	if (fragSrc)
	{
		fragSrcIncludes = fragSrc->getSrcIncludes();
		fragName = fragSrc->getName();
		attachFrag(fragSrc->getSrc());
	}
	if (geomSrc)
	{
		geomSrcIncludes = geomSrc->getSrcIncludes();
		geomName = geomSrc->getName();
		attachGeom(geomSrc->getSrc());
	}
	if (tcSrc)
	{
		tcSrcIncludes = tcSrc->getSrcIncludes();
		tcName = tcSrc->getName();
		attachTessControl(tcSrc->getSrc());
	}
	if (teSrc)
	{
		teSrcIncludes = teSrc->getSrcIncludes();
		teName = teSrc->getName();
		attachTessEval(teSrc->getSrc());
	}
	link();
}

void Shader::bufferCompute(ShaderSource *compSrc)
{
	if (compSrc)
	{
		compSrcIncludes = compSrc->getSrcIncludes();
		attachCompute(compSrc->getSrc());
		link();
	}
}

void Shader::bufferData(const std::string &vertSrc, const std::string &fragSrc, const std::string &geomSrc, const std::string &tcSrc, const std::string &teSrc)
{
	if (vertSrc != "")
		attachVert(vertSrc);
	if (fragSrc != "")
		attachFrag(fragSrc);
	if (geomSrc != "")
		attachGeom(geomSrc);
	if (tcSrc != "")
		attachTessControl(tcSrc);
	if (teSrc != "")
		attachTessEval(teSrc);
	link();
}

void Shader::bufferCompute(const std::string &compSrc)
{
	if (compSrc != "")
	{
		attachCompute(compSrc);
		link();
	}
}

void Shader::attachVert(const std::string &vertSrc)
{
	attachShader(vertSrc, vertName, object, GL_VERTEX_SHADER, vertSrcIncludes);
}

void Shader::attachFrag(const std::string &fragSrc)
{
	attachShader(fragSrc, fragName, object, GL_FRAGMENT_SHADER, fragSrcIncludes);
}

void Shader::attachGeom(const std::string &geomSrc)
{
	attachShader(geomSrc, geomName, object, GL_GEOMETRY_SHADER, geomSrcIncludes);
}

void Shader::attachTessControl(const std::string &tcSrc)
{
	attachShader(tcSrc, tcName, object, GL_TESS_CONTROL_SHADER, tcSrcIncludes);
}

void Shader::attachTessEval(const std::string &teSrc)
{
	attachShader(teSrc, teName, object, GL_TESS_EVALUATION_SHADER, teSrcIncludes);
}

void Shader::attachCompute(const std::string &compSrc)
{
	attachShader(compSrc, compName, object, GL_COMPUTE_SHADER, compSrcIncludes);
}

void Shader::link()
{
	static std::vector<ShaderSource::InnerSource> srcIncludes;

	std::string name = "unknown";
	if (vertName != "")
		name = vertName;
	else if (fragName != "")
		name = fragName;
	else if (geomName != "")
		name = geomName;
	else if (tcName != "")
		name = tcName;
	else if (teName != "")
		name = teName;

	glLinkProgram(object);
	shaderError(object, GL_LINK_STATUS, name, srcIncludes, true);
}

void Shader::create(const std::string &vertName, const std::string &vertSrc, const std::string &fragName, const std::string &fragSrc, const std::string &geomName, const std::string &geomSrc,
	 const std::string &tcName, const std::string &tcSrc, const std::string &teName, const std::string &teSrc)
{
	if (object != 0)
		return;

	this->vertName = vertName;
	this->fragName = fragName;
	this->geomName = geomName;
	this->tcName = tcName;
	this->teName = teName;

	generate();
	bufferData(vertSrc, fragSrc, geomSrc, tcSrc, teSrc);
}

void Shader::create(ShaderSource *vertSrc, ShaderSource *fragSrc, ShaderSource *geomSrc, ShaderSource *tcSrc, ShaderSource *teSrc)
{
	if (object != 0)
		return;
	
	generate();

	if (vertSrc)
	{
		vertSrcIncludes = vertSrc->getSrcIncludes();
		vertName = vertSrc->getName();
		attachVert(vertSrc->getSrc());
	}
	if (fragSrc)
	{
		fragSrcIncludes = fragSrc->getSrcIncludes();
		fragName = fragSrc->getName();
		attachFrag(fragSrc->getSrc());
	}
	if (geomSrc)
	{
		geomSrcIncludes = geomSrc->getSrcIncludes();
		geomName = geomSrc->getName();
		attachGeom(geomSrc->getSrc());
	}
	if (tcSrc)
	{
		tcSrcIncludes = tcSrc->getSrcIncludes();
		tcName = tcSrc->getName();
		attachTessControl(tcSrc->getSrc());
	}
	if (teSrc)
	{
		teSrcIncludes = teSrc->getSrcIncludes();
		teName = teSrc->getName();
		attachTessEval(teSrc->getSrc());
	}

	link();
}

void Shader::createCompute(const std::string &compName, const std::string &compSrc)
{
	if (object != 0)
		return;

	this->compName = compName;

	generate();
	bufferCompute(compSrc);
}

void Shader::createCompute(ShaderSource *compSrc)
{
	if (object != 0)
		return;

	generate();

	if (compSrc)
	{
		compSrcIncludes = compSrc->getSrcIncludes();
		compName = compSrc->getName();
		attachCompute(compSrc->getSrc());
	}

	link();
}

GLint Shader::getUniform(const std::string &name) const
{
	GLint loc = glGetUniformLocation(object, name.c_str());
	if (loc == -1)
		logError("Unable to find uniform variable " + name + " in shader, vertex shader: " + vertName + ", fragment shader: " + fragName + ", geometry shader: " + geomName);
	return loc;
}

GLint Shader::getAttribute(const std::string &name) const
{
	GLint loc = glGetAttribLocation(object, name.c_str());
	if (loc == -1)
		logError("Unable to find attribute " + name + " in shader, vertex shader: " + vertName + ", fragment shader: " + fragName + ", geometry shader: " + geomName);
	return loc;
}

GLint Shader::getUniformBlock(const std::string &name) const
{
	GLint loc = glGetUniformBlockIndex(object, name.c_str());
	if (loc == -1)
		logError("Unable to find uniform block " + name + " in shader, vertex shader: " + vertName + ", fragment shader: " + fragName + ", geometry shader: " + geomName);
	return loc;
}

GLint Shader::getStorageBlock(const std::string &name) const
{
	GLint loc = glGetProgramResourceIndex(object, GL_SHADER_STORAGE_BLOCK, name.c_str());
	if (loc == -1)
		logError("Unable to find storage block " + name + " in shader, vertex shader: " + vertName + ", fragment shader: " + fragName + ", geometry shader: " + geomName);
	return loc;
}

bool Shader::setUniform(const std::string &name, int i) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform1i(loc, i);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, unsigned int i) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform1ui(loc, i);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, float f) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform1f(loc, f);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::vec2 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform2f(loc, v.x, v.y);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::ivec2 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform2i(loc, v.x, v.y);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::uvec2 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform2ui(loc, v.x, v.y);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::vec3 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform3f(loc, v.x, v.y, v.z);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::ivec3 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform3i(loc, v.x, v.y, v.z);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::uvec3 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform3ui(loc, v.x, v.y, v.z);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::vec4 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform4f(loc, v.x, v.y, v.z, v.w);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::ivec4 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform4i(loc, v.x, v.y, v.z, v.w);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::uvec4 &v) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniform4ui(loc, v.x, v.y, v.z, v.w);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::mat3 &m) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniformMatrix3fv(loc, 1, 0, m);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, const Math::mat4 &m) const
{
	auto loc = getUniform(name);
	if (loc != -1)
	{
		glUniformMatrix4fv(loc, 1, 0, m);
		return true;
	}
	return false;
}

bool Shader::setUniform(const std::string &name, GPUObject *obj) const
{
	return obj->setUniformInShader(this, name);
}

bool Shader::setUniform(const std::string &name, GPUObject &obj) const
{
	return obj.setUniformInShader(this, name);
}

void Shader::setUniform(GLint loc, int i) const
{
	glUniform1i(loc, i);
}

void Shader::setUniform(GLint loc, unsigned int i) const
{
	glUniform1ui(loc, i);
}

void Shader::setUniform(GLint loc, float f) const
{
	glUniform1f(loc, f);
}

void Shader::setUniform(GLint loc, const Math::vec2 &v) const
{
	glUniform2f(loc, v.x, v.y);
}

void Shader::setUniform(GLint loc, const Math::ivec2 &v) const
{
	glUniform2i(loc, v.x, v.y);
}

void Shader::setUniform(GLint loc, const Math::uvec2 &v) const
{
	glUniform2ui(loc, v.x, v.y);
}

void Shader::setUniform(GLint loc, const Math::vec3 &v) const
{
	glUniform3f(loc, v.x, v.y, v.z);
}

void Shader::setUniform(GLint loc, const Math::ivec3 &v) const
{
	glUniform3i(loc, v.x, v.y, v.z);
}

void Shader::setUniform(GLint loc, const Math::uvec3 &v) const
{
	glUniform3ui(loc, v.x, v.y, v.z);
}

void Shader::setUniform(GLint loc, const Math::vec4 &v) const
{
	glUniform4f(loc, v.x, v.y, v.z, v.w);
}

void Shader::setUniform(GLint loc, const Math::ivec4 &v) const
{
	glUniform4i(loc, v.x, v.y, v.z, v.w);
}

void Shader::setUniform(GLint loc, const Math::uvec4 &v) const
{
	glUniform4ui(loc, v.x, v.y, v.z, v.w);
}

void Shader::setUniform(GLint loc, const Math::mat3 &m) const
{
	glUniformMatrix3fv(loc, 1, 0, m);
}

void Shader::setUniform(GLint loc, const Math::mat4 &m) const
{
	glUniformMatrix4fv(loc, 1, 0, m);
}

void Shader::setUniform(GLint loc, GPUObject *obj) const
{
	obj->setUniformInShader(this, loc);
}

void Shader::setUniform(GLint loc, GPUObject &obj) const
{
	obj.setUniformInShader(this, loc);
}

std::string Shader::getBinary() const
{
	if (!isGenerated())
		return "";

	int len = 0;
	glGetProgramiv(object, GL_PROGRAM_BINARY_LENGTH, &len);

	if (len == 0)
		return "";

	std::string binary;
	binary.resize(len);

	GLenum fmt;
	glGetProgramBinary(object, (GLsizei) binary.size(), &len, &fmt, &binary[0]);

	CHECKERRORS;
	if ((int) binary.size() != len)
		return "";
	return binary;
}

