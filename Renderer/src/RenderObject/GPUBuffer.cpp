#include "GPUBuffer.h"
#include "Shader.h"
#include "Texture.h"
#include "GPUObjectCache.h"
#include "../RenderResource/Mesh.h"
#include "../RenderResource/Material.h"
#include "../RenderResource/Image.h"

#include <cassert>
#include <iostream>

using namespace Rendering;

GPUObjectGroup UniformBuffer::uniformBuffers;
GPUObjectGroup StorageBuffer::storageBuffers;
GPUObjectGroup AtomicBuffer::atomicBuffers;
TextureBuffer AtomicBuffer::atomicReadBuffer(GL_R32UI);
GPUObjectGroup TransformFeedbackBuffer::transformFeedbackBuffers;

static unsigned int getGLReadWrite(bool read, bool write)
{
	assert(read || write);
	if (read)
	{
		if (write)
			return GL_READ_WRITE;
		else
			return GL_READ_ONLY;
	}
	return GL_WRITE_ONLY;
}


GPUBuffer::GPUBuffer(GLenum type, GLenum access) : GPUObject(type), size(0), access(access)
{
}

GPUBuffer::~GPUBuffer()
{
	release();
}

void GPUBuffer::generate()
{
	if (object == 0)
		glGenBuffers(1, &object);
}

void GPUBuffer::release()
{
	unbind();

	if (object == 0)
		return;
	glDeleteBuffers(1, &object);
	object = 0;
	size = 0;
}

void GPUBuffer::bind()
{
	if (boundFlag)
		return;

	glBindBuffer(type, object);
	boundFlag = true;
}

void GPUBuffer::unbind()
{
	if (!boundFlag)
		return;

	glBindBuffer(type, 0);
	boundFlag = false;
}

void GPUBuffer::bufferData(const void *data, size_t bytes)
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glBufferData(type, bytes, data, access);
	if (doBind)
		unbind();
	size = bytes;
}

void GPUBuffer::bufferSubData(const void *data, size_t offset, size_t bytes)
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glBufferSubData(type, offset, bytes, data);
	if (doBind)
		unbind();
}

void GPUBuffer::create(const void *data, size_t bytes)
{
	generate();
	bufferData(data, bytes);
}

void *GPUBuffer::map(bool read, bool write)
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	void *ptr = glMapBuffer(type, getGLReadWrite(read, write));
	if (doBind)
		unbind();
	return ptr;
}

void *GPUBuffer::map(size_t offset, size_t size, bool read, bool write)
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	void *ptr = glMapBufferRange(type, offset, size, getGLReadWrite(read, write));
	if (doBind)
		unbind();
	return ptr;
}

void GPUBuffer::unmap()
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	bool ok = (glUnmapBuffer(type) == GL_TRUE);
	assert(ok);
	if (doBind)
		unbind();
}

void GPUBuffer::copy(GPUBuffer *buffer, size_t readOffset, size_t writeOffset, size_t len)
{
	if (len == 0)
		len = size - readOffset;
	assert(size >= readOffset + len);
	assert(buffer->size >= writeOffset + len);
	glBindBuffer(GL_COPY_READ_BUFFER, object);
	glBindBuffer(GL_COPY_WRITE_BUFFER, buffer->object);
	glCopyBufferSubData(GL_COPY_READ_BUFFER, GL_COPY_WRITE_BUFFER, readOffset, writeOffset, len);
	glBindBuffer(GL_COPY_READ_BUFFER, 0);
	glBindBuffer(GL_COPY_WRITE_BUFFER, 0);
}

void *GPUBuffer::read()
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	void *ptr = map(true, false);
	unmap();
	if (doBind)
		unbind();

	return ptr;
}



VertexBuffer::VertexBuffer(GLenum mode, GLenum access) : GPUBuffer(GL_ARRAY_BUFFER, access), mode(mode), nVerts(0)
{
}

void VertexBuffer::render()
{
	if (nVerts <= 0)
		return;

	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glDrawArrays(mode, 0, (GLsizei) nVerts);
	if (doBind)
		unbind();
}

void VertexBuffer::renderRange(unsigned int start, unsigned int count)
{
	if (count <= 0)
		return;

	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glDrawArrays(mode, start, (GLsizei) count);
	if (doBind)
		unbind();
}

void VertexBuffer::bufferData(const VertexFormat &format, const void *data, size_t bytes, size_t stride, bool legacyFlag)
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glBufferData(type, bytes, data, access);
	setPointers(format, stride, legacyFlag);
	if (doBind)
		unbind();
	size = bytes;
	nVerts = bytes / stride;
}

#define BUFFER_OFFSET(i) (reinterpret_cast<void*>(i))

void VertexBuffer::setPointers(const VertexFormat &format, size_t stride, bool legacyFlag)
{
	if (!legacyFlag)
	{
		size_t offset = 0;
		for (size_t i = 0; i < format.attribs.size(); i++)
		{
			auto &attrib = format.attribs[i];
			glVertexAttribPointer(attrib.bindPoint, attrib.nVals, attrib.dataType, 0, (GLsizei) stride, BUFFER_OFFSET(offset));
			offset += sizeof(attrib.dataType) * attrib.nVals;
		}
	}
	else
	{
		size_t offset = 0;
		for (size_t i = 0; i < format.attribs.size(); i++)
		{
			auto &attrib = format.attribs[i];
			switch (attrib.type)
			{
			case VertexAttrib::POSITION:
				glVertexPointer(attrib.nVals, attrib.dataType, (GLsizei) stride, BUFFER_OFFSET(offset));
				break;
			case VertexAttrib::NORMAL:
				glNormalPointer(attrib.dataType, (GLsizei) stride, BUFFER_OFFSET(offset));
				break;
			case VertexAttrib::TEXCOORD:
				glTexCoordPointer(attrib.nVals, attrib.dataType, (GLsizei) stride, BUFFER_OFFSET(offset));
				break;
			case VertexAttrib::COLOR:
				glColorPointer(attrib.nVals, attrib.dataType, (GLsizei) stride, BUFFER_OFFSET(offset));
			default:
				break;
			}
			offset += sizeof(attrib.dataType) * attrib.nVals;
		}
	}
}

void VertexBuffer::create(const VertexFormat &format, const void *data, size_t bytes, size_t stride, bool legacyFlag)
{
	generate();
	bufferData(format, data, bytes, stride, legacyFlag);
}

void VertexBuffer::enableAttribs(const VertexFormat &format, bool legacyFlag)
{
	if (!legacyFlag)
	{
		for (size_t i = 0; i < format.attribs.size(); i++)
			glEnableVertexAttribArray(format.attribs[i].bindPoint);
	}
	else
	{
		for (size_t i = 0; i < format.attribs.size(); i++)
		{
			auto &attrib = format.attribs[i];
			switch (attrib.type)
			{
			case VertexAttrib::POSITION:
				glEnableClientState(GL_VERTEX_ARRAY);
				break;
			case VertexAttrib::NORMAL:
				glEnableClientState(GL_NORMAL_ARRAY);
				break;
			case VertexAttrib::TEXCOORD:
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				break;
			case VertexAttrib::COLOR:
				glEnableClientState(GL_COLOR_ARRAY);
				break;
			default:
				break;
			}
		}
	}
}

void VertexBuffer::disableAttribs(const VertexFormat &format, bool legacyFlag)
{
	if (!legacyFlag)
	{
		for (size_t i = 0; i < format.attribs.size(); i++)
			glDisableVertexAttribArray(format.attribs[i].bindPoint);
	}
	else
	{
		for (size_t i = 0; i < format.attribs.size(); i++)
		{
			auto &attrib = format.attribs[i];
			switch (attrib.type)
			{
			case VertexAttrib::POSITION:
				glDisableClientState(GL_VERTEX_ARRAY);
				break;
			case VertexAttrib::NORMAL:
				glDisableClientState(GL_NORMAL_ARRAY);
				break;
			case VertexAttrib::TEXCOORD:
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);
				break;
			case VertexAttrib::COLOR:
				glDisableClientState(GL_COLOR_ARRAY);
				break;
			default:
				break;
			}
		}
	}
}



IndexBuffer::IndexBuffer(GLenum mode) : GPUBuffer(GL_ELEMENT_ARRAY_BUFFER), mode(mode), nIndices(0)
{
}

void IndexBuffer::render()
{
	if (nIndices <= 0)
		return;

	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glDrawElements(mode, (GLsizei) nIndices, GL_UNSIGNED_INT, 0);
	if (doBind)
		unbind();
}

#define BUFFER_OFFSET(i) (reinterpret_cast<void*>(i))

void IndexBuffer::renderRange(unsigned int start, unsigned int count)
{
	if (count <= 0)
		return;

	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glDrawRangeElements(mode, (GLuint) 0, (GLuint) count, (GLsizei) count, GL_UNSIGNED_INT, BUFFER_OFFSET(start * sizeof(unsigned int)));
	//glDrawRangeElements(mode, (GLuint) start, (GLuint) (start + count), (GLsizei) count, GL_UNSIGNED_INT, 0); // FIXME: Figure out why this doesn't work...
	if (doBind)
		unbind();
}

void IndexBuffer::bufferData(unsigned int *indices, size_t nIndices)
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glBufferData(type, nIndices * sizeof(unsigned int), indices, GL_STATIC_DRAW);
	if (doBind)
		unbind();
	size = nIndices * sizeof(unsigned int);
	this->nIndices = nIndices;
}

void IndexBuffer::create(unsigned int *indices, size_t nIndices)
{
	generate();
	bufferData(indices, nIndices);
}



TextureBuffer::TextureBuffer(GLenum format) : GPUBuffer(GL_TEXTURE_BUFFER), texture(0), format(format), activeNumber(-1)
{
}

void TextureBuffer::generate()
{
	if (object == 0)
	{
		glGenBuffers(1, &object);
		glGenTextures(1, &texture);
	}
}

void TextureBuffer::release()
{
	unbind();

	if (texture != 0)
		glDeleteTextures(1, &texture);
	texture = 0;

	if (object != 0)
		glDeleteBuffers(1, &object);
	object = 0;
	size = 0;

	activeNumber = -1;
}

void TextureBuffer::bind()
{
	if (boundFlag)
		return;

	auto &textureGroup = Texture::getTextureGroup();

	activeNumber = textureGroup.add(this);
	if (activeNumber == -1)
		activeNumber = textureGroup.getIndex(this);
	boundFlag = true;
	
	glBindBuffer(type, object);
	glActiveTexture(GL_TEXTURE0 + activeNumber);
	glBindTexture(type, texture);
}

void TextureBuffer::unbind()
{
	if (!boundFlag)
		return;

	auto &textureGroup = Texture::getTextureGroup();

	textureGroup.remove(this);
	boundFlag = false;
	
	glBindBuffer(type, 0);
	glActiveTexture(GL_TEXTURE0 + activeNumber);
	glBindTexture(type, 0);
	activeNumber = 0;
}

void TextureBuffer::bufferData(const void *data, size_t bytes)
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glBufferData(type, bytes, data, access);
	glTexBuffer(type, format, object);
	if (doBind)
		unbind();

	size = bytes;
}

bool TextureBuffer::setUniformInShader(const Shader *shader, const std::string &name)
{
	GLint loc = shader->getUniform(name);
	if (loc == -1)
		return false;

	setUniformInShader(shader, loc);
	return true;
}

void TextureBuffer::setUniformInShader(const Shader *shader, GLint loc)
{
	(void) (shader);
	if (loc == -1)
		return;
	
	GLuint index = (GLuint) activeNumber;
	glUniform1i(loc, index);
	glBindImageTexture(index, texture, 0, GL_FALSE, 0, GL_READ_WRITE, format);
}



UniformBuffer::UniformBuffer() : GPUBuffer(GL_UNIFORM_BUFFER), activeNumber(-1)
{
}

void UniformBuffer::release()
{
	GPUBuffer::release();

	if (activeNumber != -1)
		uniformBuffers.remove(this);

	activeNumber = -1;
}

bool UniformBuffer::setUniformInShader(const Shader *shader, const std::string &name)
{
	GLint loc = shader->getUniformBlock(name);
	if (loc == -1)
		return false;

	setUniformInShader(shader, loc);
	return true;
}

void UniformBuffer::setUniformInShader(const Shader *shader, GLint loc)
{
	// FIXME: I get a bunch of errors here when rendering the obj powerplant. Fix them!
	if (loc == -1)
		return;

	if (activeNumber == -1)
		activeNumber = uniformBuffers.add(this);

	// Theoretically this shouldn't already be in uniformBuffers if activeNumber is -1, but just in case.
	if (activeNumber == -1)
		activeNumber = uniformBuffers.getIndex(this);
	
	GLuint index = (GLuint) activeNumber;
	glUniformBlockBinding(shader->getObject(), loc, index);
	//glBindBufferRange(type, index, object, 0, size); // <-- Can use this instead, but probably don't need to.
	glBindBufferBase(type, index, object);
}

void UniformBuffer::clearBufferGroup()
{
	for (auto *buffer : uniformBuffers)
		if (buffer)
			((UniformBuffer*) buffer)->activeNumber = -1;
	uniformBuffers.clear();
}



StorageBuffer::StorageBuffer() : GPUBuffer(GL_SHADER_STORAGE_BUFFER), activeNumber(-1)
{
}

void StorageBuffer::release()
{
	GPUBuffer::release();

	if (activeNumber != -1)
		storageBuffers.remove(this);

	activeNumber = -1;
}

bool StorageBuffer::setUniformInShader(const Shader *shader, const std::string &name)
{
	GLint loc = shader->getStorageBlock(name);
	if (loc == -1)
		return false;

	setUniformInShader(shader, loc);
	return true;
}

void StorageBuffer::setUniformInShader(const Shader *shader, GLint loc)
{
	if (loc == -1)
		return;

	if (activeNumber == -1)
		activeNumber = storageBuffers.add(this);

	// Theoretically this shouldn't already be in storageBuffers if activeNumber is -1, but just in case.
	if (activeNumber == -1)
		activeNumber = storageBuffers.getIndex(this);
	
	GLuint index = (GLuint) activeNumber;
	glShaderStorageBlockBinding(shader->getObject(), loc, index);
	glBindBufferBase(type, index, object);
}

void StorageBuffer::clearBufferGroup()
{
	for (auto *buffer : storageBuffers)
		if (buffer)
			((StorageBuffer*) buffer)->activeNumber = -1;
	storageBuffers.clear();
}



AtomicBuffer::AtomicBuffer() : GPUBuffer(GL_ATOMIC_COUNTER_BUFFER), activeNumber(0) // FIXME: Can't remember why this is 0 and not -1 :(
{
}

void AtomicBuffer::generate()
{
	GPUBuffer::generate();

	if (atomicReadBuffer.getObject() == 0)
	{
		unsigned int tmp = 0;
		atomicReadBuffer.create(&tmp, sizeof(tmp));
	}
}

void AtomicBuffer::release()
{
	GPUBuffer::release();

	if (activeNumber != -1)
		atomicBuffers.remove(this);

	activeNumber = -1;
}

void *AtomicBuffer::read()
{
	// Need to copy this into a texture buffer and read from that instead... not sure why.
	copy(&atomicReadBuffer);
	return atomicReadBuffer.read();
}

bool AtomicBuffer::setUniformInShader(const Shader *shader, const std::string &name)
{
	// No need to get the uniform location for an atomic counter.
	(void) (name);

	setUniformInShader(shader, 0);
	return true;
}

void AtomicBuffer::setUniformInShader(const Shader *shader, GLint loc)
{
	(void) (shader);

	// Again, no need for a uniform location for an atomic counter.
	(void) (loc);

	if (activeNumber == -1)
		activeNumber = atomicBuffers.add(this);

	// Theoretically this shouldn't already be in atomicBuffers if activeNumber is -1, but just in case.
	if (activeNumber == -1)
		activeNumber = atomicBuffers.getIndex(this);
	
	GLuint index = (GLuint) activeNumber;
	glBindBufferBase(type, index, object);
}

void AtomicBuffer::setUniform(GLuint index)
{
	glBindBufferBase(type, index, object);
}

void AtomicBuffer::clearUniform(GLuint index)
{
	glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, index, 0);
}

void AtomicBuffer::clearBufferGroup()
{
	// FIXME: Should these really be -1, I'm inclined to think not given that they're initialised to 0...
	for (auto *buffer : atomicBuffers)
		if (buffer)
			((AtomicBuffer*) buffer)->activeNumber = -1;
	atomicBuffers.clear();
}



TransformFeedbackBuffer::TransformFeedbackBuffer() : GPUBuffer(GL_TRANSFORM_FEEDBACK_BUFFER), activeNumber(-1)
{
}

void TransformFeedbackBuffer::release()
{
	GPUBuffer::release();

	if (activeNumber != -1)
		transformFeedbackBuffers.remove(this);

	activeNumber = -1;
}

bool TransformFeedbackBuffer::setUniformInShader(const Shader *shader, const std::string &name)
{
	// I guess you don't actually set any uniforms for a transform feedback buffer.
	(void) (shader);
	(void) (name);
	std::cout << "Warning: you don't actually set any uniforms for a transform feedback buffer dumbass\n";
	return false;
}

void TransformFeedbackBuffer::setUniformInShader(const Shader *shader, GLint loc)
{
	// I guess you don't actually set any uniforms for a transform feedback buffer.
	(void) (shader);
	(void) (loc);
	std::cout << "Warning: you don't actually set any uniforms for a transform feedback buffer dumbass\n";
}

void TransformFeedbackBuffer::clearBufferGroup()
{
	for (auto *buffer : transformFeedbackBuffers)
		if (buffer)
			((TransformFeedbackBuffer*) buffer)->activeNumber = -1;
	transformFeedbackBuffers.clear();
}

void TransformFeedbackBuffer::bindBufferBase(GLuint index)
{
	glBindBufferBase(type, index, object);
}



VertexArrayObject::VertexArrayObject(GLenum mode, GLenum access) : GPUObject(0), mode(mode)
{
	vbo = new VertexBuffer(mode, access);
	ibo = new IndexBuffer(mode);
}

VertexArrayObject::~VertexArrayObject()
{
	release();

	delete vbo;
	delete ibo;
}

void VertexArrayObject::setMode(GLenum mode)
{
	this->mode = mode;
	vbo->setMode(mode);
	ibo->setMode(mode);
}

void VertexArrayObject::generate()
{
	if (object == 0)
		glGenVertexArrays(1, &object);
}

void VertexArrayObject::release()
{
	unbind();

	if (object == 0)
		return;
	glDeleteVertexArrays(1, &object);
	object = 0;
	vbo->release();
	ibo->release();
}

void VertexArrayObject::bind()
{
	if (boundFlag)
		return;
	glBindVertexArray(object);
	boundFlag = true;
}

void VertexArrayObject::unbind()
{
	if (!boundFlag)
		return;
	glBindVertexArray(0);
	boundFlag = false;
}

void VertexArrayObject::render()
{
	if (ibo->isGenerated())
	{
		if (boundFlag)
			ibo->render();
		else
		{
			bind();
			ibo->render();
			unbind();
		}
	}
	else
	{
		if (boundFlag)
			vbo->render();
		else
		{
			bind();
			vbo->render();
			unbind();
		}
	}
}

void VertexArrayObject::renderRange(unsigned int start, unsigned int count)
{
	if (ibo->isGenerated())
	{
		if (boundFlag)
			ibo->renderRange(start, count);
		else
		{
			bind();
			ibo->renderRange(start, count);
			unbind();
		}
	}
	else
	{
		if (boundFlag)
			vbo->renderRange(start, count);
		else
		{
			bind();
			vbo->renderRange(start, count);
			unbind();
		}
	}
}

void VertexArrayObject::bufferData(const VertexFormat &format, const void *data, size_t bytes, size_t stride, bool legacyFlag)
{
	bind();

	if (!legacyFlag)
	{
		for (size_t i = 0; i < format.attribs.size(); i++)
			glEnableVertexAttribArray(format.attribs[i].bindPoint);
	}
	else
	{
		for (size_t i = 0; i < format.attribs.size(); i++)
		{
			auto &attrib = format.attribs[i];
			switch (attrib.type)
			{
			case VertexAttrib::POSITION:
				glEnableClientState(GL_VERTEX_ARRAY);
				break;
			case VertexAttrib::NORMAL:
				glEnableClientState(GL_NORMAL_ARRAY);
				break;
			case VertexAttrib::TEXCOORD:
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				break;
			case VertexAttrib::COLOR:
				glEnableClientState(GL_COLOR_ARRAY);
				break;
			default:
				break;
			}
		}
	}

	vbo->generate();

	vbo->setSize(bytes);
	vbo->setNVerts(bytes / stride);
	vbo->bind();
	glBufferData(GL_ARRAY_BUFFER, bytes, data, GL_STATIC_DRAW);
	vbo->setPointers(format, stride, legacyFlag);

	// FIXME: is unbind() supposed to come before or after?
	vbo->unbind();
	unbind();
}

void VertexArrayObject::create(const VertexFormat &format, const void *data, size_t bytes, size_t stride, bool legacyFlag)
{
	generate();
	bufferData(format, data, bytes, stride, legacyFlag);
}

void VertexArrayObject::bufferData(const VertexFormat &format, const void *data, size_t bytes, size_t stride, unsigned int *indices, size_t nIndices, bool legacyFlag)
{
	bind();

	if (!legacyFlag)
	{
		for (size_t i = 0; i < format.attribs.size(); i++)
			glEnableVertexAttribArray(format.attribs[i].bindPoint);
	}
	else
	{
		for (size_t i = 0; i < format.attribs.size(); i++)
		{
			auto &attrib = format.attribs[i];
			switch (attrib.type)
			{
			case VertexAttrib::POSITION:
				glEnableClientState(GL_VERTEX_ARRAY);
				break;
			case VertexAttrib::NORMAL:
				glEnableClientState(GL_NORMAL_ARRAY);
				break;
			case VertexAttrib::TEXCOORD:
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);
				break;
			case VertexAttrib::COLOR:
				glEnableClientState(GL_COLOR_ARRAY);
				break;
			default:
				break;
			}
		}
	}

	vbo->generate();
	ibo->generate();

	vbo->setSize(bytes);
	vbo->setNVerts(bytes / stride);
	vbo->bind();
	glBufferData(GL_ARRAY_BUFFER, bytes, data, GL_STATIC_DRAW);
	vbo->setPointers(format, stride, legacyFlag);

	ibo->setSize(nIndices * sizeof(unsigned int));
	ibo->setNIndices(nIndices);
	ibo->bind();
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, nIndices * sizeof(unsigned int), indices, GL_STATIC_DRAW);

	// FIXME: is unbind() supposed to come before or after?
	vbo->unbind();
	ibo->unbind();
	unbind();
}

void VertexArrayObject::bufferData(Mesh *mesh, bool legacyFlag)
{
	//static VertexFormat meshFormat({ {VertexAttrib::POSITION, 0, 3, GL_FLOAT}, {VertexAttrib::NORMAL, 1, 3, GL_FLOAT}, {VertexAttrib::TEXCOORD, 2, 2, GL_FLOAT} });
	static VertexFormat meshFormat({ {VertexAttrib::POSITION, 0, 3, GL_FLOAT}, {VertexAttrib::NORMAL, 1, 3, GL_FLOAT}, {VertexAttrib::TEXCOORD, 2, 2, GL_FLOAT}, {VertexAttrib::TANGENT, 3, 3, GL_FLOAT} });
	bufferData(meshFormat, mesh->getVertices(), mesh->getNumVertices() * sizeof(Vertex), sizeof(Vertex), mesh->getIndices(), mesh->getNumIndices(), legacyFlag);
}

void VertexArrayObject::create(const VertexFormat &format, const void *data, size_t bytes, size_t stride, unsigned int *indices, size_t nIndices, bool legacyFlag)
{
	generate();
	bufferData(format, data, bytes, stride, indices, nIndices, legacyFlag);
}

void VertexArrayObject::create(Mesh *mesh, bool legacyFlag)
{
	//static VertexFormat meshFormat({ {VertexAttrib::POSITION, 0, 3, GL_FLOAT}, {VertexAttrib::NORMAL, 1, 3, GL_FLOAT}, {VertexAttrib::TEXCOORD, 2, 2, GL_FLOAT} });
	static VertexFormat meshFormat({ {VertexAttrib::POSITION, 0, 3, GL_FLOAT}, {VertexAttrib::NORMAL, 1, 3, GL_FLOAT}, {VertexAttrib::TEXCOORD, 2, 2, GL_FLOAT}, {VertexAttrib::TANGENT, 3, 3, GL_FLOAT} });
	create(meshFormat, mesh->getVertices(), mesh->getNumVertices() * sizeof(Vertex), sizeof(Vertex), mesh->getIndices(), mesh->getNumIndices(), legacyFlag);
}



void GPUMesh::release()
{
	subMeshes.clear();
	vao.release();
}

void GPUMesh::createSubMeshes(Mesh *mesh)
{
	// Currently expects diffuse texture to be bound to 1, and normal texture to be bound to 2.
	subMeshes.clear();

	for (auto &faceset : mesh->getFacesets())
	{
		MeshMaterial mat(faceset.mat->getAmbientColor(), faceset.mat->getDiffuseColor(), faceset.mat->getSpecularColor(), faceset.mat->getShininess());
		subMeshes.push_back(SubMesh(faceset.start, faceset.end - faceset.start, mat));

		auto &subMesh = subMeshes[subMeshes.size() - 1];

		auto *diffuseImg = faceset.mat->getDiffuseImg();
		auto *normalImg = faceset.mat->getNormalImg();

		if (diffuseImg)
		{
			if (!Texture2DCache::getTexture(diffuseImg->getName(), subMesh.diffuseTex))
			{
				subMesh.diffuseTex->create(diffuseImg->getData(), diffuseImg->getFormat(), diffuseImg->getDimensions().x, diffuseImg->getDimensions().y, false);
				subMesh.diffuseTex->setFilters(GL_REPEAT, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR, 8);
				subMesh.diffuseTex->genMipmap();
			}
		}
		if (normalImg)
		{
			if (!Texture2DCache::getTexture(normalImg->getName(), subMesh.normalTex))
				subMesh.normalTex->create(normalImg->getData(), normalImg->getFormat(), normalImg->getDimensions().x, normalImg->getDimensions().y);
		}

		// Create the uniform buffer and store it in the cache.
		if (!UniformBufferCache::getUniformBuffer(faceset.mat->getName(), subMesh.uniforms))
			subMesh.uniforms->create(&subMesh.mat, sizeof(subMesh.mat));
	}

	// Create the default material.
	defaultMat = MeshMaterial(Math::vec4(0.4, 0.4, 0.4, 0.2), Math::vec4(0.8, 0.8, 0.8, 0.2), Math::vec4(1, 1, 1, 1), 128);
	defaultMatBuffer.create(&defaultMat, sizeof(defaultMat));
}

void GPUMesh::bufferData(Mesh *mesh, bool legacyFlag)
{
	vao.bufferData(mesh, legacyFlag);
	createSubMeshes(mesh);
}

void GPUMesh::create(Mesh *mesh, bool legacyFlag)
{
	vao.create(mesh, legacyFlag);
	createSubMeshes(mesh);
}

void GPUMesh::render(bool useMaterials)
{
	auto *shader = Shader::getBound();

	if (!shader && useMaterials)
		glPushAttrib(GL_LIGHTING_BIT);

	// Currently expects diffuse texture to be bound to 1, and normal texture to be bound to 2.
	vao.bind();

	// If there are no subMeshes, just draw the whole thing.
	if (subMeshes.empty())
	{
		if (shader)
		{
			// TODO: Allow single material for whole meshes.
			shader->setUniform("startIndex", (unsigned int) 0);
			shader->setUniform("MaterialBlock", &defaultMatBuffer);
			shader->setUniform("texFlag", false);
			shader->setUniform("normFlag", false);
		}
		vao.render();
	}
	else
	{
		size_t lastIndex = 0;
		for (auto &m : subMeshes)
		{
			// Submeshes with no material.
			if (lastIndex < m.start)
			{
				// Pass in lastIndex in case it's needed to calculate correct polygon offsets.
				if (shader)
				{
					shader->setUniform("startIndex", (unsigned int) lastIndex);
					if (useMaterials)
					{
						shader->setUniform("MaterialBlock", &defaultMatBuffer);
						shader->setUniform("texFlag", false);
						shader->setUniform("normFlag", false);
					}
				}
				else if (useMaterials)
				{
					glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (const GLfloat*) &defaultMat.ambient.x);
					glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (const GLfloat*) &defaultMat.diffuse.x);
					glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (const GLfloat*) &defaultMat.specular.x);
					glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, defaultMat.shininess);
				}

				vao.renderRange((unsigned int) lastIndex, (unsigned int) (m.start - lastIndex));
			}

			// Submeshes with a material.
			else
			{
				// Render using the material data.
				if (useMaterials)
				{
					if (m.diffuseTex)
						m.diffuseTex->bind();
					if (m.normalTex)
						m.normalTex->bind();

					// Bind the appropriate uniform buffer to the currently bound shader, as well as any textures.
					if (shader)
					{
						if (m.uniforms)
							shader->setUniform("MaterialBlock", m.uniforms);
						if (m.diffuseTex)
							shader->setUniform("diffuseTex", m.diffuseTex);
						if (m.normalTex)
							shader->setUniform("normalTex", m.normalTex);
						if (!m.diffuseTex)
							shader->setUniform("texFlag", false);
						else
							shader->setUniform("texFlag", true);
						if (!m.normalTex)
							shader->setUniform("normFlag", false);
						else
							shader->setUniform("normFlag", true);
					}
					else
					{
						glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (const GLfloat*) &m.mat.ambient.x);
						glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (const GLfloat*) &m.mat.diffuse.x);
						glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (const GLfloat*) &m.mat.specular.x);
						glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, m.mat.shininess);
					}
				}

				// Pass in startIndex in case it's needed to calculate correct polygon offsets.
				if (shader)
					shader->setUniform("startIndex", (unsigned int) m.start);

				vao.renderRange((unsigned int) m.start, (unsigned int) m.count);

				if (useMaterials)
				{
					if (m.diffuseTex)
						m.diffuseTex->unbind();
					if (m.normalTex)
						m.normalTex->unbind();
				}
			}
			lastIndex = m.start + m.count;
		}

		// Render the rest of the mesh if necessary.
		if (lastIndex < vao.getIndexBuffer()->getNIndices())
		{
			// Pass in lastIndex in case it's needed to calculate correct polygon offsets.
			if (shader)
			{
				shader->setUniform("startIndex", (unsigned int) lastIndex);
				if (useMaterials)
				{
					shader->setUniform("MaterialBlock", &defaultMatBuffer);
					shader->setUniform("texFlag", false);
					shader->setUniform("normFlag", false);
				}
			}
			else if (useMaterials)
			{
				glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, (const GLfloat*) &defaultMat.ambient.x);
				glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, (const GLfloat*) &defaultMat.diffuse.x);
				glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, (const GLfloat*) &defaultMat.specular.x);
				glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, defaultMat.shininess);
			}
			vao.renderRange((unsigned int) lastIndex, (unsigned int) (vao.getIndexBuffer()->getNIndices() - lastIndex));
		}
	}

	if (!shader && useMaterials)
		glPopAttrib();

	vao.unbind();
}

