#pragma once

#include "../GL.h"
#include "GPUObject.h"
#include "../Vertex.h"

#include <list>
#include <vector>

namespace Rendering
{
	class Mesh;
	class Texture2D;

	class GPUBuffer : public GPUObject
	{
	private:
		GPUBuffer(const GPUBuffer &gbo);
		GPUBuffer &operator = (const GPUBuffer &gbo);

	protected:
		size_t size;

		GLenum access;

	public:
		GPUBuffer(GLenum type, GLenum access = GL_STATIC_DRAW);
		virtual ~GPUBuffer();

		virtual void generate() override;
		virtual void release() override;
		virtual void bind() override;
		virtual void unbind() override;
		virtual void render() {}
		virtual void renderRange(unsigned int start, unsigned int count) { (void) (start); (void) (count); }

		virtual void bufferData(const void *data, size_t bytes);
		virtual void bufferSubData(const void *data, size_t offset, size_t bytes);
		virtual void create(const void *data, size_t bytes);
		virtual void *map(bool read = true, bool write = true);
		virtual void *map(size_t offset, size_t size, bool read = true, bool write = true);
		virtual void unmap();
		virtual void copy(GPUBuffer *buffer, size_t readOffset = 0, size_t writeOffset = 0, size_t len = 0);
		virtual void *read();

		void setSize(size_t size) { this->size = size; }
		void setAccess(GLenum access) { this->access = access; }

		size_t getSize() const { return size; }
		GLenum getAccess() const { return access; }
	};

	class VertexBuffer : public GPUBuffer
	{
	private:
		VertexBuffer(const VertexBuffer &vbo);
		VertexBuffer &operator = (const VertexBuffer &vbo);

	protected:
		GLenum mode;

		size_t nVerts;

	public:
		VertexBuffer(GLenum mode = GL_TRIANGLES, GLenum access = GL_STATIC_DRAW);
		virtual ~VertexBuffer() { release(); }

		void setMode(GLenum mode) { this->mode = mode; }
		void setNVerts(size_t nVerts) { this->nVerts = nVerts; }

		size_t getNVerts() const { return nVerts; }

		virtual void render() override;
		virtual void renderRange(unsigned int start, unsigned int count) override;

		void bufferData(const VertexFormat &format, const void *data, size_t bytes, size_t stride, bool legacyFlag = false);
		void setPointers(const VertexFormat &format, size_t stride, bool legacyFlag = false);
		void create(const VertexFormat &format, const void *data, size_t bytes, size_t stride, bool legacyFlag = false);

		void enableAttribs(const VertexFormat &format, bool legacyFlag = false);
		void disableAttribs(const VertexFormat &format, bool legacyFlag = false);
	};

	class IndexBuffer : public GPUBuffer
	{
	private:
		IndexBuffer(const IndexBuffer &ibo);
		IndexBuffer &operator = (const IndexBuffer &ibo);

	protected:
		GLenum mode;

		size_t nIndices;

	public:
		IndexBuffer(GLenum mode = GL_TRIANGLES);
		virtual ~IndexBuffer() { release(); }

		void setMode(GLenum mode) { this->mode = mode; }
		void setNIndices(size_t nIndices) { this->nIndices = nIndices; }

		size_t getNIndices() const { return nIndices; }

		virtual void render() override;
		virtual void renderRange(unsigned int start, unsigned int count) override;

		//void renderRange(size_t start, size_t count);
		void bufferData(unsigned int *indices, size_t nIndices);
		void create(unsigned int *indices, size_t nIndices);
	};

	class TextureBuffer : public GPUBuffer
	{
	private:
		TextureBuffer(const TextureBuffer &tbo);
		TextureBuffer &operator = (const TextureBuffer &tbo);

	private:
		GLuint texture;

		GLenum format;
		int activeNumber;

	public:
		TextureBuffer(GLenum format = GL_R32I);
		virtual ~TextureBuffer() { release(); }

		virtual void generate() override;
		virtual void release() override;
		virtual void bind() override;
		virtual void unbind() override;

		virtual void bufferData(const void *data, size_t bytes) override;

		void setFormat(GLenum format) { this->format = format; }

		virtual bool setUniformInShader(const Shader *shader, const std::string &name) override;
		virtual void setUniformInShader(const Shader *shader, GLint loc) override;
		int getActiveNumber() const { return activeNumber; }
	};

	class UniformBuffer : public GPUBuffer
	{
	private:
		UniformBuffer(const UniformBuffer &ubo);
		UniformBuffer &operator = (const UniformBuffer &ubo);

	private:
		static GPUObjectGroup uniformBuffers;

	private:
		int activeNumber;

	public:
		UniformBuffer();
		virtual ~UniformBuffer() { release(); }

		virtual void release() override;

		virtual bool setUniformInShader(const Shader *shader, const std::string &name) override;
		virtual void setUniformInShader(const Shader *shader, GLint loc) override;
		int getActiveNumber() const { return activeNumber; }

		static void clearBufferGroup();
	};

	class StorageBuffer : public GPUBuffer
	{
	private:
		StorageBuffer(const StorageBuffer &sbo);
		StorageBuffer &operator = (const StorageBuffer &sbo);

	private:
		static GPUObjectGroup storageBuffers;

	private:
		int activeNumber;

	public:
		StorageBuffer();
		virtual ~StorageBuffer() { release(); }

		virtual void release() override;

		virtual bool setUniformInShader(const Shader *shader, const std::string &name) override;
		virtual void setUniformInShader(const Shader *shader, GLint loc) override;
		int getActiveNumber() const { return activeNumber; }

		static void clearBufferGroup();
	};

	class AtomicBuffer : public GPUBuffer
	{
	private:
		AtomicBuffer(const AtomicBuffer &abo);
		AtomicBuffer &operator = (const AtomicBuffer &abo);

	private:
		static GPUObjectGroup atomicBuffers;
		static TextureBuffer atomicReadBuffer;

	private:
		int activeNumber;

	public:
		AtomicBuffer();
		virtual ~AtomicBuffer() { release(); }

		virtual void generate() override;
		virtual void release() override;

		virtual void *read();

		virtual bool setUniformInShader(const Shader *shader, const std::string &name = "") override;
		virtual void setUniformInShader(const Shader *shader, GLint loc = 0) override;
		int getActiveNumber() const { return activeNumber; }

		// Atomic counters have their own special setUniform method.
		void setUniform(GLuint index);
		static void clearUniform(GLuint index);

		static void clearBufferGroup();
	};

	class TransformFeedbackBuffer : public GPUBuffer
	{
	private:
		TransformFeedbackBuffer(const TransformFeedbackBuffer &tbo);
		TransformFeedbackBuffer &operator = (const TransformFeedbackBuffer &tbo);

	private:
		static GPUObjectGroup transformFeedbackBuffers;

	private:
		int activeNumber;

	public:
		TransformFeedbackBuffer();
		virtual ~TransformFeedbackBuffer() { release(); }

		virtual void release() override;

		virtual bool setUniformInShader(const Shader *shader, const std::string &name) override;
		virtual void setUniformInShader(const Shader *shader, GLint loc) override;
		int getActiveNumber() const { return activeNumber; }

		virtual void bindBufferBase(GLuint index);

		static void clearBufferGroup();
	};

	class VertexArrayObject : public GPUObject
	{
	private:
		VertexArrayObject(const VertexArrayObject &vao);
		VertexArrayObject &operator = (const VertexArrayObject &vao);

	private:
		VertexBuffer *vbo;
		IndexBuffer *ibo;

		GLenum mode;

	public:
		VertexArrayObject(GLenum mode = GL_TRIANGLES, GLenum access = GL_STATIC_DRAW);
		virtual ~VertexArrayObject();

		void setMode(GLenum mode);
		
		virtual void generate() override;
		virtual void release() override;
		virtual void bind() override;
		virtual void unbind() override;

		void render();
		void renderRange(unsigned int start, unsigned int count);

		void bufferData(const VertexFormat &format, const void *data, size_t bytes, size_t stride, bool legacyFlag = false);
		void create(const VertexFormat &format, const void *data, size_t bytes, size_t stride, bool legacyFlag = false);

		void bufferData(const VertexFormat &format, const void *data, size_t bytes, size_t stride, unsigned int *indices, size_t nIndices, bool legacyFlag = false);
		void bufferData(Mesh *mesh, bool legacyFlag = false);

		void create(const VertexFormat &format, const void *data, size_t bytes, size_t stride, unsigned int *indices, size_t nIndices, bool legacyFlag = false);
		void create(Mesh *mesh, bool legacyFlag = false);

		bool isGenerated() const { return (vbo != nullptr && vbo->isGenerated()) || (ibo != nullptr && ibo->isGenerated()); }

		VertexBuffer *getVertexBuffer() { return vbo; }
		IndexBuffer *getIndexBuffer() { return ibo; }
	};

	// TODO: Decide if this really should be here or not.
	class GPUMesh
	{
	public:
		struct MeshMaterial
		{
			Math::vec4 ambient;
			Math::vec4 diffuse;
			Math::vec4 specular;
			float shininess;

			MeshMaterial(const Math::vec4 &ambient = Math::vec4(), const Math::vec4 &diffuse = Math::vec4(), const Math::vec4 &specular = Math::vec4(), float shininess = 64) : ambient(ambient), diffuse(diffuse), specular(specular), shininess(shininess) {}
		};

		struct SubMesh
		{
			size_t start;
			size_t count;

			Texture2D *diffuseTex;
			Texture2D *normalTex;
			UniformBuffer *uniforms;

			MeshMaterial mat;

			SubMesh(size_t start = 0, size_t count = 0, const MeshMaterial &mat = MeshMaterial()) : start(start), count(count), diffuseTex(nullptr), normalTex(nullptr), uniforms(nullptr), mat(mat) {}
		};

	private:
		GPUMesh(const GPUMesh &gbo);
		GPUMesh &operator = (const GPUMesh &gbo);

	private:
		std::vector<SubMesh> subMeshes;

		VertexArrayObject vao;

		MeshMaterial defaultMat;
		UniformBuffer defaultMatBuffer;

	public:
		GPUMesh(GLenum mode = GL_TRIANGLES, GLenum access = GL_STATIC_DRAW) : vao(mode, access) {}

		void release();

		void createSubMeshes(Mesh *mesh);

		void bufferData(Mesh *mesh, bool legacyFlag = false);
		void create(Mesh *mesh, bool legacyFlag = false);

		void render(bool useMaterials = true);

		bool isGenerated() const { return vao.isGenerated(); }

		std::vector<SubMesh> &getSubMeshes() { return subMeshes; }
		VertexArrayObject &getVao() { return vao; }
	};
}

