#include "Texture.h"
#include "Shader.h"

#include <iostream>

using namespace Rendering;

GPUObjectGroup Texture::textureGroup;

FrameBuffer::FrameBufferStack FrameBuffer::frameBufferStack;


// TODO: look up the table on https://www.khronos.org/opengles/sdk/docs/man3/html/glTexStorage2D.xhtml and make sure formats/internal formats and storage types all match properly...
static int channelsPerPixel(GLenum format)
{
	switch (format)
	{
	case GL_RED:
	case GL_R16F:
	case GL_R16I:
	case GL_R16UI:
	case GL_R32F:
	case GL_R32I:
	case GL_R32UI:
	case GL_LUMINANCE:
	case GL_DEPTH_COMPONENT:
	case GL_DEPTH_COMPONENT16:
	case GL_DEPTH_COMPONENT24:
	case GL_DEPTH_COMPONENT32:
		return 1;
	case GL_LUMINANCE_ALPHA:
	case GL_RG:
	case GL_RG8:
	case GL_RG8I:
	case GL_RG8UI:
	case GL_RG16F:
	case GL_RG16I:
	case GL_RG16UI:
	case GL_RG32F:
	case GL_RG32I:
	case GL_RG32UI:
		return 2;
	case GL_BGR:
	case GL_RGB:
	case GL_RGB16F:
	case GL_RGB16I:
	case GL_RGB16UI:
	case GL_RGB32F:
	case GL_RGB32I:
	case GL_RGB32UI:
		return 3;
	case GL_BGRA:
	case GL_RGBA:
	case GL_RGBA8:
	case GL_RGBA16F:
	case GL_RGBA16I:
	case GL_RGBA16UI:
	case GL_RGBA32F:
	case GL_RGBA32I:
	case GL_RGBA32UI:
		return 4;
	case GL_DEPTH24_STENCIL8: //???
		return 4;
	default:
		std::cout << "Unknown channels per pixel in Texture.cpp\n";
		return 4;
	}
}


static GLenum defaultFormat(GLenum format)
{
#if 0
	int cpp = channelsPerPixel(format);
	GLenum informat;

	switch (cpp)
	{
	case 1:
		informat = GL_RED;
		break;
	case 2:
		informat = GL_RG;
		break;
	case 3:
		informat = GL_RGB;
		break;
	case 4:
		informat = GL_RGBA;
		break;
	default:
		informat = GL_RGBA;
		break;
	}
#endif

	switch (format)
	{
	case GL_ALPHA:
	case GL_RED:
	case GL_R8:
	case GL_R16F:
	case GL_R32F:
	case GL_LUMINANCE:
		return GL_RED;

	case GL_R8I:
	case GL_R8UI:
	case GL_R16I:
	case GL_R16UI:
	case GL_R32I:
	case GL_R32UI:
		return GL_RED_INTEGER;

	case GL_LUMINANCE_ALPHA:
	case GL_RG:
	case GL_RG8:
	case GL_RG16F:
	case GL_RG32F:
		return GL_RG;

	case GL_RG8I:
	case GL_RG8UI:
	case GL_RG16I:
	case GL_RG16UI:
	case GL_RG32I:
	case GL_RG32UI:
		return GL_RG_INTEGER;

	case GL_BGR:
	case GL_RGB:
	case GL_RGB16F:
	case GL_RGB32F:
		return GL_RGB;

	case GL_RGB16I:
	case GL_RGB16UI:
	case GL_RGB32I:
	case GL_RGB32UI:
		return GL_RGB_INTEGER;

	case GL_BGRA:
	case GL_RGBA:
	case GL_RGBA8:
	case GL_RGBA16F:
	case GL_RGBA32F:
		return GL_RGBA;

	case GL_RGBA16I:
	case GL_RGBA16UI:
	case GL_RGBA32I:
	case GL_RGBA32UI:
		return GL_RGBA_INTEGER;

	case GL_DEPTH24_STENCIL8:
		return GL_DEPTH_COMPONENT; // Not sure if there's such a thing as depth and stencil component.

	case GL_DEPTH_COMPONENT:
	case GL_DEPTH_COMPONENT16:
	case GL_DEPTH_COMPONENT24:
	case GL_DEPTH_COMPONENT32:
		return GL_DEPTH_COMPONENT;

	default:
		std::cout << "Unknown texture format in Texture.cpp\n";
		return GL_RGBA;
	}
}

static GLenum defaultType(GLenum format)
{
#if 0
	int cpp = channelsPerPixel(format);
	GLenum informat;

	switch (cpp)
	{
	case 1:
		informat = GL_RED;
		break;
	case 2:
		informat = GL_RG;
		break;
	case 3:
		informat = GL_RGB;
		break;
	case 4:
		informat = GL_RGBA;
		break;
	default:
		informat = GL_RGBA;
		break;
	}
#endif

	switch (format)
	{
	case GL_ALPHA:
	case GL_RED:
	case GL_R8:
	case GL_LUMINANCE:
	case GL_LUMINANCE_ALPHA:
	case GL_RG:
	case GL_RG8:
	case GL_BGR:
	case GL_RGB:
	case GL_BGRA:
	case GL_RGBA:
	case GL_RGBA8:
	case GL_DEPTH24_STENCIL8:
	case GL_DEPTH_COMPONENT:
	case GL_DEPTH_COMPONENT16:
	case GL_DEPTH_COMPONENT24:
	case GL_DEPTH_COMPONENT32:
		return GL_UNSIGNED_BYTE;

	case GL_R8UI:
	case GL_R16UI:
	case GL_R32UI:
	case GL_RG8UI:
	case GL_RG16UI:
	case GL_RG32UI:
	case GL_RGB16UI:
	case GL_RGB32UI:
	case GL_RGBA16UI:
	case GL_RGBA32UI:
		return GL_UNSIGNED_INT;

	case GL_R8I:
	case GL_R16I:
	case GL_R32I:
	case GL_RG8I:
	case GL_RG16I:
	case GL_RG32I:
	case GL_RGB16I:
	case GL_RGB32I:
	case GL_RGBA16I:
	case GL_RGBA32I:
		return GL_INT;

	case GL_R16F:
	case GL_R32F:
	case GL_RG16F:
	case GL_RG32F:
	case GL_RGB16F:
	case GL_RGB32F:
	case GL_RGBA16F:
	case GL_RGBA32F:
		return GL_FLOAT;

	default:
		std::cout << "Unknown texture type in Texture.cpp\n";
		return GL_RGBA;
	}
}



Texture::Texture(GLenum type) : GPUObject(type), multisampleFlag(false), activeNumber(-1), nSamples(4), width(0), height(0), format(0)
{
}

Texture::Texture(GLenum type, GLenum format, size_t width, size_t height) : GPUObject(type), multisampleFlag(false), activeNumber(-1), nSamples(4), width(width), height(height), format(format)
{
}

void Texture::generate()
{
	if (object == 0)
		glGenTextures(1, &object);
}

void Texture::release()
{
	unbind();

	if (object == 0)
		return;
	glDeleteTextures(1, &object);
	object = 0;
	activeNumber = -1;
	nSamples = 0;
	width = 0;
	height = 0;
}

void Texture::bind()
{
	//if (boundFlag)
	//	return;

	activeNumber = textureGroup.add(this);
	if (activeNumber == -1)
		activeNumber = textureGroup.getIndex(this);
	boundFlag = true;

	if (activeNumber != -1)
		glActiveTexture(GL_TEXTURE0 + activeNumber);
	glBindTexture(type, object);
}

void Texture::unbind()
{
	//if (!boundFlag)
	//	return;

	textureGroup.remove(this);
	boundFlag = false;

	if (activeNumber != -1)
		glActiveTexture(GL_TEXTURE0 + activeNumber);
	glBindTexture(type, 0);
	activeNumber = -1;
}

void Texture::bindAsTexBuffer()
{
	activeNumber = textureGroup.add(this);
	if (activeNumber == -1)
		activeNumber = textureGroup.getIndex(this);
	boundFlag = true;

	glBindBuffer(GL_TEXTURE_BUFFER, object);
	if (activeNumber != -1)
		glActiveTexture(GL_TEXTURE0 + activeNumber);
	glBindTexture(type, object);
}

void Texture::unbindAsTexBuffer()
{
	textureGroup.remove(this);
	boundFlag = false;

	glBindBuffer(GL_TEXTURE_BUFFER, 0);
	if (activeNumber != -1)
		glActiveTexture(GL_TEXTURE0 + activeNumber);
	glBindTexture(type, 0);
	activeNumber = -1;
}

bool Texture::setUniformInShader(const Shader *shader, const std::string &name)
{
	GLint loc = shader->getUniform(name);
	if (loc != -1)
	{
		glUniform1i(loc, getActiveNumber());
		return true;
	}
	return false;
}

void Texture::setUniformInShader(const Shader *shader, GLint loc)
{
	(void) (shader);
	if (loc != -1)
		glUniform1i(loc, getActiveNumber());
}

bool Texture::setUniformAsTexBuffer(const Shader *shader, const std::string &name)
{
	GLint loc = shader->getUniform(name);
	if (loc != -1)
	{
		GLuint index = (GLuint) getActiveNumber();
		glUniform1i(loc, index);
		glBindImageTexture(index, object, 0, GL_FALSE, 0, GL_READ_WRITE, format);
		return true;
	}
	return false;
}

void Texture::setUniformAsTexBuffer(const Shader *shader, GLint loc)
{
	(void) (shader);
	if (loc != -1)
	{
		GLuint index = (GLuint) getActiveNumber();
		glUniform1i(loc, index);
		glBindImageTexture(index, object, 0, GL_FALSE, 0, GL_READ_WRITE, format);
	}
}

void Texture::setFilters(GLenum wrap, GLenum minFilter, GLenum maxFilter, int aniso)
{
	bool doBind = !boundFlag;
	if (doBind)
		bind();

	glTexParameteri(type, GL_TEXTURE_MIN_FILTER, minFilter);
	glTexParameteri(type, GL_TEXTURE_MAG_FILTER, maxFilter);

	if (aniso > 0)
		glTexParameterf(type, GL_TEXTURE_MAX_ANISOTROPY_EXT, (GLfloat) aniso);

	glTexParameteri(type, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(type, GL_TEXTURE_WRAP_T, wrap);
	glTexParameteri(type, GL_TEXTURE_WRAP_R, wrap); // Not sure if this should be here or elsewhere.

	if (doBind)
		unbind();
}

void Texture::genMipmap()
{
	bool doBind = !boundFlag;
	if (doBind)
		bind();

	glGenerateMipmap(type);

	if (doBind)
		unbind();
}

std::vector<unsigned char> Texture::read()
{
	std::vector<unsigned char> texData(width * height * channelsPerPixel(format));

	bool doBind = !boundFlag;

	if (doBind)
		bind();

	glGetTexImage(type, 0, format, GL_UNSIGNED_BYTE, &texData[0]);

	if (doBind)
		unbind();

	return texData;
}

void Texture::read(void *texData)
{
	bool doBind = !boundFlag;
	auto dataType = defaultType(format);

	if (doBind)
		bind();
	glGetTexImage(type, 0, format, dataType, texData);
	if (doBind)
		unbind();
}

bool Texture::isDepthTexture() const
{
	return format == GL_DEPTH_COMPONENT || format == GL_DEPTH_COMPONENT16 || format == GL_DEPTH_COMPONENT24 || format == GL_DEPTH_COMPONENT32;
}

void Texture::clearTextureGroup()
{
	for (auto *texture : textureGroup)
		if (texture)
			((Texture*) texture)->activeNumber = -1;
	textureGroup.clear();
}



Texture2D::Texture2D() : Texture(GL_TEXTURE_2D)
{
}

Texture2D::Texture2D(GLenum format, size_t width, size_t height) : Texture(GL_TEXTURE_2D, format, width, height)
{
}

void Texture2D::toggleMultisampling(int nSamples)
{
	this->nSamples = nSamples;
	multisampleFlag = !multisampleFlag;
	if (!multisampleFlag)
		type = GL_TEXTURE_2D;
	else
		type = GL_TEXTURE_2D_MULTISAMPLE;
}

void Texture2D::bufferData(bool filterFlag)
{
	bufferData(nullptr, filterFlag);
}

void Texture2D::bufferData(GLenum format, size_t width, size_t height, size_t layers, bool filterFlag)
{
	(void) (layers);

	bufferData(nullptr, format, width, height, filterFlag);
}

void Texture2D::bufferData(unsigned char *data, bool filterFlag)
{
	bool doBind = !boundFlag;

	auto informat = defaultFormat(format);
	auto dataType = defaultType(format);

	if (doBind)
		bind();

	if (!multisampleFlag)
		glTexImage2D(type, 0, format, (GLsizei) width, (GLsizei) height, 0, informat, dataType, data);
	else
		glTexImage2DMultisample(type, nSamples, format, (GLsizei) width, (GLsizei) height, GL_TRUE);

	// Now handled by filter/mipmap functions.
	if (filterFlag)
		setFilters();

	if (doBind)
		unbind();
}

Texture2D &Texture2D::create(unsigned char *data, bool filterFlag)
{
	generate();
	bufferData(data, format, width, height, filterFlag);

	return *this;
}

void Texture2D::bufferData(unsigned char *data, GLenum format, size_t width, size_t height, bool filterFlag)
{
	this->format = format;
	this->width = width;
	this->height = height;

	bufferData(data, filterFlag);
}

Texture2D &Texture2D::create(unsigned char *data, GLenum format, size_t width, size_t height, bool filterFlag)
{
	this->format = format;
	this->width = width;
	this->height = height;

	return create(data, filterFlag);
}

void Texture2D::useAsShadowMap()
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glTexParameteri(type, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
	if (doBind)
		unbind();
}

void Texture2D::blit(float z)
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();

	glPushAttrib(GL_TRANSFORM_BIT | GL_ENABLE_BIT);

	glDisable(GL_DEPTH_TEST);
	glEnable(type);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glBegin(GL_QUADS);

	glTexCoord2f(0, 0); glVertex3f(-1, -1, z);
	glTexCoord2f(1, 0); glVertex3f( 1, -1, z);
	glTexCoord2f(1, 1); glVertex3f( 1,  1, z);
	glTexCoord2f(0, 1); glVertex3f(-1,  1, z);

	glEnd();

	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();

	if (doBind)
		unbind();
}



Texture2DArray::Texture2DArray() : Texture(GL_TEXTURE_2D_ARRAY), layers(0)
{
}

Texture2DArray::Texture2DArray(GLenum format, size_t width, size_t height, size_t layers) : Texture(GL_TEXTURE_2D_ARRAY, format, width, height), layers(layers)
{
}

void Texture2DArray::toggleMultisampling(int nSamples)
{
	this->nSamples = nSamples;
	multisampleFlag = !multisampleFlag;
	if (!multisampleFlag)
		type = GL_TEXTURE_2D_ARRAY;
	else
		type = GL_TEXTURE_2D_MULTISAMPLE_ARRAY;
}
		
void Texture2DArray::bufferData(bool filterFlag)
{
	bool doBind = !boundFlag;

	auto informat = defaultFormat(format);
	auto dataType = defaultType(format);

	if (doBind)
		bind();

	// Not needed.
	//glTexStorage3D(type, 1, format, (GLsizei) width, (GLsizei) height, (GLsizei) layers);

	if (!multisampleFlag)
		glTexImage3D(type, 0, format, (GLsizei) width, (GLsizei) height, (GLsizei) layers, 0, informat, dataType, nullptr);
	else
		glTexImage3DMultisample(type, nSamples, format, (GLsizei) width, (GLsizei) height, (GLsizei) layers, GL_TRUE);

	if (filterFlag)
		setFilters();

	if (doBind)
		unbind();
}

void Texture2DArray::bufferData(GLenum format, size_t width, size_t height, size_t layers, bool filterFlag)
{
	this->format = format;
	this->width = width;
	this->height = height;
	this->layers = layers;

	bufferData(filterFlag);
}

void Texture2DArray::bufferData(unsigned char *data, size_t layer, bool filterFlag)
{
	bool doBind = !boundFlag;

	auto informat = defaultFormat(format);
	auto dataType = defaultType(format);

	if (doBind)
		bind();

	glTexSubImage3D(type, 0, 0, 0, (GLint) layer, (GLsizei) width, (GLsizei) height, (GLsizei) 1, informat, dataType, data);

	if (filterFlag)
		setFilters();

	if (doBind)
		unbind();
}

Texture2DArray &Texture2DArray::create(bool filterFlag)
{
	generate();
	bufferData(format, width, height, layers, filterFlag);

	return *this;
}

Texture2DArray &Texture2DArray::create(GLenum format, size_t width, size_t height, size_t layers, bool filterFlag)
{
	this->format = format;
	this->width = width;
	this->height = height;
	this->layers = layers;

	return create(filterFlag);
}

void Texture2DArray::useAsShadowMap()
{
	bool doBind = !boundFlag;

	if (doBind)
		bind();
	glTexParameteri(type, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
	glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY);
	if (doBind)
		unbind();
}



Texture3D::Texture3D() : Texture(GL_TEXTURE_3D), layers(0)
{
}

Texture3D::Texture3D(GLenum format, size_t width, size_t height, size_t layers) : Texture(GL_TEXTURE_3D, format, width, height), layers(layers)
{
}

void Texture3D::toggleMultisampling(int nSamples)
{
	this->nSamples = nSamples;
	multisampleFlag = !multisampleFlag;
	if (!multisampleFlag)
		type = GL_TEXTURE_3D;
	else
		type = GL_TEXTURE_2D_MULTISAMPLE_ARRAY;
}

void Texture3D::bufferData(bool filterFlag)
{
	bool doBind = !boundFlag;

	auto informat = defaultFormat(format);
	auto dataType = defaultType(format);

	if (doBind)
		bind();

	// Not needed.
	//glTexStorage3D(type, 1, format, (GLsizei) width, (GLsizei) height, (GLsizei) layers);

	if (!multisampleFlag)
		glTexImage3D(type, 0, format, (GLsizei) width, (GLsizei) height, (GLsizei) layers, 0, informat, dataType, nullptr);
	else
		glTexImage3DMultisample(type, nSamples, format, (GLsizei) width, (GLsizei) height, (GLsizei) layers, GL_TRUE);

	if (filterFlag)
		setFilters();

	if (doBind)
		unbind();
}

void Texture3D::bufferData(GLenum format, size_t width, size_t height, size_t layers, bool filterFlag)
{
	this->format = format;
	this->width = width;
	this->height = height;
	this->layers = layers;

	bufferData(filterFlag);
}

Texture3D &Texture3D::create(bool filterFlag)
{
	generate();
	bufferData(format, width, height, layers, filterFlag);

	return *this;
}

Texture3D &Texture3D::create(GLenum format, size_t width, size_t height, size_t layers, bool filterFlag)
{
	this->format = format;
	this->width = width;
	this->height = height;
	this->layers = layers;

	return create(filterFlag);
}

bool Texture3D::setUniformInShader(const Shader *shader, const std::string &name)
{
	GLint loc = shader->getUniform(name);
	if (loc != -1)
	{
		GLint index = (GLint) getActiveNumber();
		glUniform1i(loc, index);
		glBindImageTexture(index, object, 0, GL_TRUE, 0, GL_READ_WRITE, format);
		return true;
	}
	return false;
}

void Texture3D::setUniformInShader(const Shader *shader, GLint loc)
{
	(void) (shader);
	if (loc != -1)
	{
		GLint index = (GLint) getActiveNumber();
		glUniform1i(loc, index);
		glBindImageTexture(index, object, 0, GL_TRUE, 0, GL_READ_WRITE, format);
	}
}

bool Texture3D::setUniformAsTexBuffer(const Shader *shader, const std::string &name)
{
	GLint loc = shader->getUniform(name);
	if (loc != -1)
	{
		GLint index = (GLint) getActiveNumber();
		glUniform1i(loc, index);
		glBindImageTexture(index, object, 0, GL_TRUE, 0, GL_READ_WRITE, format);
		return true;
	}
	return false;
}

void Texture3D::setUniformAsTexBuffer(const Shader *shader, GLint loc)
{
	(void) (shader);
	if (loc != -1)
	{
		GLint index = (GLint) getActiveNumber();
		glUniform1i(loc, index);
		glBindImageTexture(index, object, 0, GL_TRUE, 0, GL_READ_WRITE, format);
	}
}



TextureCubeMap::TextureCubeMap() : Texture(GL_TEXTURE_CUBE_MAP)
{
}

TextureCubeMap::TextureCubeMap(GLenum format, size_t width, size_t height) : Texture(GL_TEXTURE_CUBE_MAP, format, width, height)
{
}

void TextureCubeMap::toggleMultisampling(int nSamples)
{
	this->nSamples = nSamples;
	multisampleFlag = !multisampleFlag;
}

void TextureCubeMap::bufferData(bool filterFlag)
{
	bool doBind = !boundFlag;

	auto informat = defaultFormat(format);
	auto dataType = defaultType(format);

	if (doBind)
		bind();

	for (int i = 0; i < 6; i++)
		if (!multisampleFlag)
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, (GLsizei) width, (GLsizei) height, 0, informat, dataType, nullptr);
		else
			glTexImage2DMultisample(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, nSamples, format, (GLsizei) width, (GLsizei) height, GL_TRUE);

	if (filterFlag)
		setFilters();

	if (doBind)
		unbind();
}

void TextureCubeMap::bufferData(GLenum format, size_t width, size_t height, size_t layers, bool filterFlag)
{
	(void) (layers);

	this->format = format;
	this->width = width;
	this->height = height;

	bufferData(filterFlag);
}

void TextureCubeMap::bufferData(GLenum format, int index, unsigned char *data, size_t width, size_t height, bool filterFlag)
{
	this->format = format;
	this->width = width;
	this->height = height;

	bool doBind = !boundFlag;

	auto informat = defaultFormat(format);
	auto dataType = defaultType(format);

	if (doBind)
		bind();

	if (!multisampleFlag)
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + index, 0, format, (GLsizei) width, (GLsizei) height, 0, informat, dataType, data);
	else
		glTexImage2DMultisample(GL_TEXTURE_CUBE_MAP_POSITIVE_X + index, nSamples, format, (GLsizei) width, (GLsizei) height, GL_TRUE);

	if (filterFlag)
		setFilters();

	if (doBind)
		unbind();
}

TextureCubeMap &TextureCubeMap::create(bool filterFlag)
{
	generate();
	bufferData(format, width, height, filterFlag);

	return *this;
}

TextureCubeMap &TextureCubeMap::create(GLenum format, size_t width, size_t height, bool filterFlag)
{
	this->format = format;
	this->width = width;
	this->height = height;

	return create(filterFlag);
}



RenderBuffer::RenderBuffer() : GPUObject(GL_RENDERBUFFER), multisampleFlag(false), nSamples(4), width(0), height(0), format(0)
{
}

RenderBuffer::RenderBuffer(GLenum format, size_t width, size_t height) : GPUObject(GL_RENDERBUFFER), multisampleFlag(false), nSamples(4), width(width), height(height), format(format)
{
}

void RenderBuffer::toggleMultisampling(int nSamples)
{
	this->nSamples = nSamples;
	multisampleFlag = !multisampleFlag;
}

void RenderBuffer::generate()
{
	if (object == 0)
		glGenRenderbuffers(1, &object);
}

void RenderBuffer::release()
{
	unbind();

	if (object == 0)
		return;
	glDeleteRenderbuffers(1, &object);
	object = 0;
}

void RenderBuffer::bind()
{
	if (boundFlag)
		return;

	glBindRenderbuffer(type, object);
	boundFlag = true;
}

void RenderBuffer::unbind()
{
	if (!boundFlag)
		return;

	glBindRenderbuffer(type, 0);
	boundFlag = false;
}

void RenderBuffer::bufferData()
{
	bool doBind = !boundFlag;

	// FIXME: I'm pretty sure I should be using informat here rather than format, test this!!!
	//auto informat = defaultFormat(format);

	if (doBind)
		bind();

	if (!multisampleFlag)
		glRenderbufferStorage(type, format, (GLsizei) width, (GLsizei) height);
	else
		glRenderbufferStorageMultisample(type, nSamples, format, (GLsizei) width, (GLsizei) height);

	if (doBind)
		unbind();
}

RenderBuffer &RenderBuffer::create()
{
	generate();
	bufferData(format, width, height);

	return *this;
}

void RenderBuffer::bufferData(GLenum format, size_t width, size_t height)
{
	this->format = format;
	this->width = width;
	this->height = height;

	bufferData();
}

RenderBuffer &RenderBuffer::create(GLenum format, size_t width, size_t height)
{
	this->format = format;
	this->width = width;
	this->height = height;

	return create();
}



static bool checkStatus(GLenum type)
{
	auto status = glCheckFramebufferStatus(type);
	switch (status)
	{
	case GL_FRAMEBUFFER_COMPLETE: /*std::cout << "GL_FRAMEBUFFER_COMPLETE\n";*/ break;
	case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT: std::cout << "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n"; break;
	case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: std::cout << "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n"; break;
	case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT: std::cout << "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS\n"; break;
	case GL_FRAMEBUFFER_INCOMPLETE_FORMATS_EXT: std::cout << "GL_FRAMEBUFFER_INCOMPLETE_FORMATS\n"; break;
	case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER: std::cout << "GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER\n"; break;
	case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER: std::cout << "GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER\n"; break;
	case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS: std::cout << "GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS\n"; break;
	case GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE: std::cout << "GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE\n"; break;
	case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE: std::cout << "GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE\n"; break;
	case GL_FRAMEBUFFER_UNSUPPORTED: std::cout << "GL_FRAMEBUFFER_UNSUPPORTED\n"; break;
	default:  std::cout << "Unknown framebuffer status error: " << status << "\n"; break;
	}
	return status == GL_FRAMEBUFFER_COMPLETE;
}



void FrameBuffer::FrameBufferStack::pop()
{
	if (stack.empty())
		return;

	stack.erase(stack.begin());
	bindTopFrameBuffer();
}

void FrameBuffer::FrameBufferStack::bind(FrameBuffer *frameBuffer)
{
	stack.push_front(frameBuffer);
	frameBuffer->glBind();
}

void FrameBuffer::FrameBufferStack::unbind(FrameBuffer *frameBuffer)
{
	if (stack.empty())
		return;

	for (auto it = stack.begin(); it != stack.end(); it++)
	{
		if (*it != frameBuffer)
			continue;
		stack.erase(it);
		break;
	}

	frameBuffer->glUnbind();
	bindTopFrameBuffer();
}

void FrameBuffer::FrameBufferStack::remove(FrameBuffer *frameBuffer)
{
	if (stack.empty())
		return;

	for (auto it = stack.begin(); it != stack.end(); )
	{
		if (*it == frameBuffer)
			it = stack.erase(it);
		else
			it++;
	}

	if (!frameBuffer->isBound())
		return;

	frameBuffer->glUnbind();
	bindTopFrameBuffer();
}

void FrameBuffer::FrameBufferStack::bindTopFrameBuffer()
{
	if (!stack.empty())
		stack.front()->glBind();
}



void FrameBuffer::glBind()
{
	glBindFramebuffer(type, object);
	boundFlag = true;
}

void FrameBuffer::glUnbind()
{
	glBindFramebuffer(type, 0);
	boundFlag = false;
}



FrameBuffer::FrameBuffer(size_t width, size_t height, size_t layers) : GPUObject(GL_FRAMEBUFFER), width(width), height(height), layers(layers), currAttachment(0), depthRenderBuffer(nullptr), depthTexture(nullptr), multisampleFlag(false)
{
	for (size_t i = 0; i < 8; i++)
		colorBuffers[i] = nullptr;
}

FrameBuffer::~FrameBuffer()
{
	release();
}

void FrameBuffer::generate()
{
	if (object == 0)
		glGenFramebuffers(1, &object);
}

void FrameBuffer::release()
{
	if (object == 0)
		return;
	glDeleteFramebuffers(1, &object);
	object = 0;

	//colorBuffer = nullptr;
	for (size_t i = 0; i < 8; i++)
		colorBuffers[i] = nullptr;

	drawBuffers.clear();

	depthRenderBuffer = nullptr;
	depthTexture = nullptr;

	currAttachment = 0;
}

void FrameBuffer::bind()
{
	frameBufferStack.bind(this);
	//glBind();
}

void FrameBuffer::unbind()
{
	frameBufferStack.unbind(this);
	//glUnbind();
}

void FrameBuffer::attachColorBuffer(Texture *colorBuffer)
{
	if (!colorBuffer || currAttachment >= 8)
	{
		std::cout << "Failed to attach color buffer, either no buffer given or we're full\n";
		return;
	}

	bool doBind = false;
	if (!boundFlag)
	{
		bind();
		doBind = true;
	}

	//this->colorBuffer = colorBuffer;
	colorBuffers[currAttachment] = colorBuffer;

	// TODO: Not sure if this still applies or not...
	if (colorBuffer->isDepthTexture())
	{
		glFramebufferTexture2D(type, GL_DEPTH_ATTACHMENT, colorBuffer->getType(), colorBuffer->getObject(), 0);
		glDrawBuffer(GL_NONE); // FIXME: This probably won't work if there are multiple render targets...
		// drawBuffers.push_back(GL_NONE); // FIXME: Possible alternative?
	}
	else
	{
		// 2D textures that aren't multisampled get their own special treatment.
		multisampleFlag = colorBuffer->isMultisampling();
		Texture2D *tex2d = dynamic_cast<Texture2D*>(colorBuffer);

		if (!multisampleFlag && tex2d)
			glFramebufferTexture2D(type, GL_COLOR_ATTACHMENT0 + currAttachment, colorBuffer->getType(), colorBuffer->getObject(), 0);
		else
			glFramebufferTexture(type, GL_COLOR_ATTACHMENT0 + currAttachment, colorBuffer->getObject(), 0);
		drawBuffers.push_back(GL_COLOR_ATTACHMENT0 + currAttachment);
		currAttachment++;
	}

	if (!checkStatus(type))
		std::cout << "Failed to attach color buffer to framebuffer object\n";

	if (doBind)
		unbind();
}

void FrameBuffer::attachDepthBuffer(RenderBuffer *depthRenderBuffer)
{
	if (!depthRenderBuffer)
	{
		std::cout << "Failed to attach depth buffer, no buffer given\n";
		return;
	}

	bool doBind = false;
	if (!boundFlag)
	{
		bind();
		doBind = true;
	}

	this->depthRenderBuffer = depthRenderBuffer;

	glFramebufferRenderbuffer(type, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer->getObject());

	if (!checkStatus(type))
		std::cout << "Failed to attach depth buffer to framebuffer object\n";

	if (doBind)
		unbind();
}

void FrameBuffer::attachDepthBuffer(Texture *depthTexture)
{
	if (!depthTexture)
	{
		std::cout << "Failed to attach depth texture, no texture given\n";
		return;
	}

	bool doBind = false;
	if (!boundFlag)
	{
		bind();
		doBind = true;
	}

	this->depthTexture = depthTexture;

	// Should this be glFramebufferTexture2D?
	//glFramebufferTexture2D(type, GL_DEPTH_ATTACHMENT, depthTexture->getType(), depthTexture->getObject(), 0);
	glFramebufferTexture(type, GL_DEPTH_ATTACHMENT, depthTexture->getObject(), 0);
	//glDrawBuffer(GL_NONE);

	if (!checkStatus(type))
		std::cout << "Failed to attach depth texture to framebuffer object\n";

	if (doBind)
		unbind();
}

void FrameBuffer::attachDepthStencilBuffer(RenderBuffer *depthRenderBuffer)
{
	if (!depthRenderBuffer)
	{
		std::cout << "Failed to attach depth/stencil buffer, no buffer given\n";
		return;
	}

	bool doBind = false;
	if (!boundFlag)
	{
		bind();
		doBind = true;
	}

	this->depthRenderBuffer = depthRenderBuffer;

	glFramebufferRenderbuffer(type, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer->getObject());

	if (!checkStatus(type))
		std::cout << "Failed to attach depth/stencil buffer to framebuffer object\n";

	if (doBind)
		unbind();
}

void FrameBuffer::attachDepthStencilBuffer(Texture *depthTexture)
{
	if (!depthTexture)
	{
		std::cout << "Failed to attach depth/stencil texture, no texture given\n";
		return;
	}

	bool doBind = false;
	if (!boundFlag)
	{
		bind();
		doBind = true;
	}

	this->depthTexture = depthTexture;

	// Should this be glFramebufferTexture2D?
	//glFramebufferTexture2D(type, GL_DEPTH_STENCIL_ATTACHMENT, depthTexture->getType(), depthTexture->getObject(), 0);
	glFramebufferTexture(type, GL_DEPTH_STENCIL_ATTACHMENT, depthTexture->getObject(), 0);
	//glDrawBuffer(GL_NONE);

	if (!checkStatus(type))
		std::cout << "Failed to attach depth/stencil texture to framebuffer object\n";

	if (doBind)
		unbind();
}

void FrameBuffer::create(GLenum format, Texture *texture, RenderBuffer *depthRenderBuffer, bool includeStencil)
{
	texture->generate();
	texture->bufferData(format, width, height, layers);

	generate();
	bind();

	attachColorBuffer(texture);

	if (depthRenderBuffer)
	{
		if (!includeStencil)
		{
			depthRenderBuffer->create(GL_DEPTH_COMPONENT24, width, height);
			attachDepthBuffer(depthRenderBuffer);
		}
		else
		{
			depthRenderBuffer->create(GL_DEPTH24_STENCIL8, width, height);
			attachDepthStencilBuffer(depthRenderBuffer);
		}
	}

	unbind();
}

void FrameBuffer::create(RenderBuffer *depthRenderBuffer, bool includeStencil)
{
	generate();
	bind();

	if (depthRenderBuffer)
	{
		if (!includeStencil)
		{
			depthRenderBuffer->create(GL_DEPTH_COMPONENT24, width, height);
			attachDepthBuffer(depthRenderBuffer);
		}
		else
		{
			depthRenderBuffer->create(GL_DEPTH24_STENCIL8, width, height);
			attachDepthStencilBuffer(depthRenderBuffer);
		}
	}

	unbind();
}

void FrameBuffer::setDrawBuffer(int buffer)
{
	glDrawBuffer(GL_COLOR_ATTACHMENT0 + buffer);
}

void FrameBuffer::setDrawBuffers()
{
	glDrawBuffers((GLsizei) drawBuffers.size(), &drawBuffers[0]);
}

void FrameBuffer::blit()
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, object);
	glDrawBuffer(GL_BACK);
	glBlitFramebuffer(0, 0, (GLint) width, (GLint) height, 0, 0, (GLint) width, (GLint) height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
	glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
}

