#pragma once

#include <string>
#include <map>

namespace Rendering
{
	class Texture2D;
	class UniformBuffer;

	// Only cache things that *really* need to be used by other stuff, eg 2d textures and uniform buffers are used by materials.
	class Texture2DCache
	{
	private:
		static std::map<std::string, Texture2D*> cache;

	public:
		static void clear();
		static bool getTexture(const std::string &name, Texture2D *&tex);
		static bool exists(const std::string &name);
		static Texture2D &getTexture(const std::string &name);
		static Texture2D &addTexture(Texture2D *tex);
	};

	class UniformBufferCache
	{
	private:
		static std::map<std::string, UniformBuffer*> cache;

	public:
		static void clear();
		static bool getUniformBuffer(const std::string &name, UniformBuffer *&buf);
		static bool exists(const std::string &name);
		static UniformBuffer &getUniformBuffer(const std::string &name);
		static UniformBuffer &addUniformBuffer(UniformBuffer *buf);
	};
}
