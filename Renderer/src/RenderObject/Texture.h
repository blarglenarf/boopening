#pragma once

#include "GPUObject.h"

#include "../../../Math/src/Vector/VectorCommon.h"

#include <string>
#include <list>
#include <vector>

namespace Rendering
{
	class Texture : public GPUObject
	{
	private:
		static GPUObjectGroup textureGroup;

	protected:
		bool multisampleFlag;

		int activeNumber;
		int nSamples;

		size_t width;
		size_t height;

		GLenum format;

	public:
		Texture(GLenum type);
		Texture(GLenum type, GLenum format, size_t width = 0, size_t height = 0);
		virtual ~Texture() { release(); }

		virtual void toggleMultisampling(int nSamples = 4) = 0;
		bool isMultisampling() const { return multisampleFlag; }

		virtual void generate() override;
		virtual void release() override;

		virtual void bind() override;
		virtual void unbind() override;

		virtual void bindAsTexBuffer();
		virtual void unbindAsTexBuffer();

		virtual bool setUniformInShader(const Shader *shader, const std::string &name) override;
		virtual void setUniformInShader(const Shader *shader, GLint loc) override;

		virtual bool setUniformAsTexBuffer(const Shader *shader, const std::string &name);
		virtual void setUniformAsTexBuffer(const Shader *shader, GLint loc);

		virtual void bufferData(bool filterFlag = true) = 0;
		virtual void bufferData(GLenum format, size_t width, size_t height, size_t layers = 1, bool filterFlag = true) = 0;

		virtual void setFilters(GLenum wrap = GL_REPEAT, GLenum minFilter = GL_NEAREST, GLenum maxFilter = GL_LINEAR, int aniso = 0);
		virtual void genMipmap();

		virtual std::vector<unsigned char> read();
		virtual void read(void *texData);

		int getActiveNumber() const { return activeNumber; }
		GLenum getFormat() const { return format; }

		void setFormat(GLenum format) { this->format = format; }

		bool isDepthTexture() const;

		static GPUObjectGroup &getTextureGroup() { return textureGroup; }
		static void clearTextureGroup();

		size_t getWidth() const { return width; }
		size_t getHeight() const { return height; }
	};

	class Texture2D : public Texture
	{
	private:
		Texture2D(const Texture2D &t);
		Texture2D &operator = (const Texture2D &t);

	public:
		Texture2D();
		Texture2D(GLenum format, size_t width = 0, size_t height = 0);
		virtual ~Texture2D() { release(); }

		virtual void toggleMultisampling(int nSamples = 4) override;

		virtual void bufferData(bool filterFlag = true) override;
		virtual void bufferData(GLenum format, size_t width, size_t height, size_t layers = 1, bool filterFlag = true) override;
		
		void bufferData(unsigned char *data, bool filterFlag = true);
		Texture2D &create(unsigned char *data, bool filterFlag = true);
		void bufferData(unsigned char *data, GLenum format, size_t width, size_t height, bool filterFlag = true);
		Texture2D &create(unsigned char *data, GLenum format, size_t width, size_t height, bool filterFlag = true);
		
		void useAsShadowMap();
		void blit(float z = 0);
	};

	// FIXME: This used to be different, but is now essentially the same as a 3D texture, so don't duplicate them!
	// Although it might be worth keeping them separate in case I want to change one and not the other in the future.
	class Texture2DArray : public Texture
	{
	private:
		Texture2DArray(const Texture2DArray &t);
		Texture2DArray &operator = (const Texture2DArray &t);

	private:
		size_t layers;

	public:
		Texture2DArray();
		Texture2DArray(GLenum format, size_t width = 0, size_t height = 0, size_t layers = 0);
		virtual ~Texture2DArray() { release(); }

		virtual void toggleMultisampling(int nSamples = 4) override;
		
		virtual void bufferData(bool filterFlag = true) override;
		virtual void bufferData(GLenum format, size_t width, size_t height, size_t layers = 1, bool filterFlag = true) override;

		void bufferData(unsigned char *data, size_t layer, bool filterFlag = true);

		Texture2DArray &create(bool filterFlag = true);
		Texture2DArray &create(GLenum format, size_t width, size_t height, size_t layers, bool filterFlag = true);

		void useAsShadowMap();
	};

	class Texture3D : public Texture
	{
	private:
		Texture3D(const Texture3D &t);
		Texture3D &operator = (const Texture3D &t);

	private:
		size_t layers;

	public:
		Texture3D();
		Texture3D(GLenum format, size_t width = 0, size_t height = 0, size_t layers = 0);
		virtual ~Texture3D() { release(); }

		virtual void toggleMultisampling(int nSamples = 4) override;
		
		virtual void bufferData(bool filterFlag = true) override;
		virtual void bufferData(GLenum format, size_t width, size_t height, size_t layers = 1, bool filterFlag = true) override;

		Texture3D &create(bool filterFlag = true);
		Texture3D &create(GLenum format, size_t width, size_t height, size_t layers, bool filterFlag = true);

		virtual bool setUniformInShader(const Shader *shader, const std::string &name) override;
		virtual void setUniformInShader(const Shader *shader, GLint loc) override;

		virtual bool setUniformAsTexBuffer(const Shader *shader, const std::string &name) override;
		virtual void setUniformAsTexBuffer(const Shader *shader, GLint loc) override;
	};

	class TextureCubeMap : public Texture
	{
	private:
		TextureCubeMap(const TextureCubeMap &t);
		TextureCubeMap &operator = (const TextureCubeMap &t);

	public:
		TextureCubeMap();
		TextureCubeMap(GLenum format, size_t width = 0, size_t height = 0);
		virtual ~TextureCubeMap() { release(); }

		virtual void toggleMultisampling(int nSamples = 4) override;

		virtual void bufferData(bool filterFlag = true) override;
		virtual void bufferData(GLenum format, size_t width, size_t height, size_t layers = 1, bool filterFlag = true) override;

		void bufferData(GLenum format, int index, unsigned char *data, size_t width, size_t height, bool filterFlag = true); // May also want to buffer all the textures at once.

		TextureCubeMap &create(bool filterFlag = true);
		TextureCubeMap &create(GLenum format, size_t width, size_t height, bool filterFlag = true);
	};

	class RenderBuffer : public GPUObject
	{
	private:
		RenderBuffer(const RenderBuffer &t);
		RenderBuffer operator = (const RenderBuffer &t);

	private:
		bool multisampleFlag;
		int nSamples;

		size_t width;
		size_t height;

		GLenum format;

	public:
		RenderBuffer();
		RenderBuffer(GLenum format, size_t width = 0, size_t height = 0);
		virtual ~RenderBuffer() { release(); }

		void toggleMultisampling(int nSamples = 4);

		virtual void generate() override;
		virtual void release() override;

		virtual void bind() override;
		virtual void unbind() override;

		void bufferData();
		RenderBuffer &create();
		void bufferData(GLenum format, size_t width, size_t height);
		RenderBuffer &create(GLenum format, size_t width, size_t height);
	};

	class FrameBuffer : public GPUObject
	{
	private:
		FrameBuffer(const FrameBuffer &fbo);
		FrameBuffer &operator = (const FrameBuffer &fbo);

	private:
		class FrameBufferStack
		{
		private:
			std::list<FrameBuffer*> stack;

		public:
			void pop();

			void bind(FrameBuffer *frameBuffer);
			void unbind(FrameBuffer *frameBuffer);

			void remove(FrameBuffer *frameBuffer);
			void bindTopFrameBuffer();
		};

		static FrameBufferStack frameBufferStack;

	private:
		std::vector<GLenum> drawBuffers;

		size_t width;
		size_t height;
		size_t layers;

		int currAttachment;

		// Do you really need more than 8 of these... *really*??? If so then use a 2d texture array instead.
		Texture *colorBuffers[8];

		// Can either use a render buffer or texture for the depth/stencil attachment.
		RenderBuffer *depthRenderBuffer;
		Texture *depthTexture;

		bool multisampleFlag;

	private:
		void glBind();
		void glUnbind();

	public:
		FrameBuffer(size_t width = 512, size_t height = 512, size_t layers = 1);
		virtual ~FrameBuffer();

		virtual void generate() override;
		virtual void release() override;

		virtual void bind() override;
		virtual void unbind() override;

		void attachColorBuffer(Texture *colorBuffer);

		void attachDepthBuffer(RenderBuffer *depthRenderBuffer);
		void attachDepthBuffer(Texture *depthTexture);

		void attachDepthStencilBuffer(RenderBuffer *depthRenderBuffer);
		void attachDepthStencilBuffer(Texture *depthTexture);

		void create(GLenum format, Texture *texture, RenderBuffer *depthRenderBuffer = nullptr, bool includeStencil = false);
		void create(RenderBuffer *depthRenderBuffer = nullptr, bool includeStencil = false);

		void setDrawBuffer(int buffer);
		void setDrawBuffers();

		void setSize(size_t width, size_t height, size_t layers = 1) { this->width = width; this->height = height; this->layers = layers; }

		void blit();
	};
}

