#pragma once

#include "GPUObject.h"

#include "../RenderResource/ShaderSource.h"
#include "../../../Math/src/Vector/VectorCommon.h"
#include "../../../Math/src/Matrix/MatrixCommon.h"
#include "../../../Utils/src/StringUtils.h"

#include <string>
#include <list>

namespace Rendering
{
	class Texture;
	//class ShaderSource;

	class Shader : public GPUObject
	{
	private:
		class ShaderStack
		{
		private:
			std::list<Shader*> stack;

		public:
			void pop();

			void bind(Shader *shader);
			void unbind(Shader *shader);

			void remove(Shader *shader);
			void bindTopShader();
		};

		static ShaderStack shaderStack;

	private:
		std::string vertName; // Vertex.
		std::string fragName; // Fragment.
		std::string geomName; // Geometry.
		std::string tcName; // Tessellation control.
		std::string teName; // Tessellation evaluation.
		std::string compName; // Compute;

		// For correct line no. error reporting.
		std::vector<ShaderSource::InnerSource> vertSrcIncludes;
		std::vector<ShaderSource::InnerSource> fragSrcIncludes;
		std::vector<ShaderSource::InnerSource> geomSrcIncludes;
		std::vector<ShaderSource::InnerSource> tcSrcIncludes;
		std::vector<ShaderSource::InnerSource> teSrcIncludes;
		std::vector<ShaderSource::InnerSource> compSrcIncludes;

		static Shader *currBound;
		static Utils::StringHash errorCache;

	private:
		void glBind();
		void glUnbind();

		static void logError(const std::string &msg);

		Shader(const Shader &shader);
		Shader &operator = (const Shader &shader);

	public:
		Shader();

		virtual ~Shader();

		virtual void generate() override;
		virtual void release() override;

		virtual void bind() override;
		virtual void unbind() override;

		static void pop() { shaderStack.pop(); }
		static Shader *getBound() { return currBound; }

		//void bufferData(const ShaderSource &vertSrc = ShaderSource(), const ShaderSource &fragSrc = ShaderSource(), const ShaderSource &geomSrc = ShaderSource(), const ShaderSource &tcSrc = ShaderSource(), const ShaderSource &teSrc = ShaderSource());
		//void bufferCompute(const ShaderSource &compSrc = ShaderSource());
		void bufferData(ShaderSource *vertSrc = nullptr, ShaderSource *fragSrc = nullptr, ShaderSource *geomSrc = nullptr, ShaderSource *tcSrc = nullptr, ShaderSource *teSrc = nullptr);
		void bufferCompute(ShaderSource *compSrc = nullptr);

		void bufferData(const std::string &vertSrc = "", const std::string &fragSrc = "", const std::string &geomSrc = "", const std::string &tcSrc = "", const std::string &teSrc = "");
		void bufferCompute(const std::string &compSrc = "");

		void attachVert(const std::string &vertSrc);
		void attachFrag(const std::string &fragSrc);
		void attachGeom(const std::string &geomSrc);
		void attachTessControl(const std::string &tcSrc);
		void attachTessEval(const std::string &teSrc);
		void attachCompute(const std::string &compSrc);
		void link();

		void create(const std::string &vertName = "", const std::string &vertSrc = "", const std::string &fragName = "", const std::string &fragSrc = "",
			const std::string &geomName = "", const std::string &geomSrc = "", const std::string &tcName = "", const std::string &tcSrc = "", const std::string &teName = "", const std::string &teSrc = "");
		void create(ShaderSource *vertSrc = nullptr, ShaderSource *fragSrc = nullptr, ShaderSource *geomSrc = nullptr, ShaderSource *tcSrc = nullptr, ShaderSource *teSrc = nullptr);

		void createCompute(const std::string &compName = "", const std::string &compSrc = "");
		void createCompute(ShaderSource *compSrc = nullptr);

		GLint getUniform(const std::string &name) const;
		GLint getAttribute(const std::string &name) const;
		GLint getUniformBlock(const std::string &name) const;
		GLint getStorageBlock(const std::string &name) const;

		bool setUniform(const std::string &name, int i) const;
		bool setUniform(const std::string &name, unsigned int i) const;
		bool setUniform(const std::string &name, float f) const;

		bool setUniform(const std::string &name, const Math::vec2 &v) const;
		bool setUniform(const std::string &name, const Math::ivec2 &v) const;
		bool setUniform(const std::string &name, const Math::uvec2 &v) const;

		bool setUniform(const std::string &name, const Math::vec3 &v) const;
		bool setUniform(const std::string &name, const Math::ivec3 &v) const;
		bool setUniform(const std::string &name, const Math::uvec3 &v) const;

		bool setUniform(const std::string &name, const Math::vec4 &v) const;
		bool setUniform(const std::string &name, const Math::ivec4 &v) const;
		bool setUniform(const std::string &name, const Math::uvec4 &v) const;

		bool setUniform(const std::string &name, const Math::mat3 &m) const;
		bool setUniform(const std::string &name, const Math::mat4 &m) const;

		bool setUniform(const std::string &name, GPUObject *obj) const;
		bool setUniform(const std::string &name, GPUObject &obj) const;

		void setUniform(GLint loc, int i) const;
		void setUniform(GLint loc, unsigned int i) const;
		void setUniform(GLint loc, float f) const;

		void setUniform(GLint loc, const Math::vec2 &v) const;
		void setUniform(GLint loc, const Math::ivec2 &v) const;
		void setUniform(GLint loc, const Math::uvec2 &v) const;

		void setUniform(GLint loc, const Math::vec3 &v) const;
		void setUniform(GLint loc, const Math::ivec3 &v) const;
		void setUniform(GLint loc, const Math::uvec3 &v) const;

		void setUniform(GLint loc, const Math::vec4 &v) const;
		void setUniform(GLint loc, const Math::ivec4 &v) const;
		void setUniform(GLint loc, const Math::uvec4 &v) const;

		void setUniform(GLint loc, const Math::mat3 &m) const;
		void setUniform(GLint loc, const Math::mat4 &m) const;
		void setUniform(GLint loc, GPUObject *obj) const;
		void setUniform(GLint loc, GPUObject &obj) const;

		bool isBound() const { return boundFlag; }

		std::string getBinary() const;
	};
}

