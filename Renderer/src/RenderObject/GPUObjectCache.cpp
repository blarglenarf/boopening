#include "GPUObjectCache.h"
#include "Texture.h"
#include "GPUBuffer.h"

#include <iostream>

using namespace Rendering;

std::map<std::string, Texture2D*> Texture2DCache::cache;
std::map<std::string, UniformBuffer*> UniformBufferCache::cache;


template <typename T, typename U>
static bool get(const std::string &name, T *&obj, U &cache)
{
	auto it = cache.find(name);
	if (it == cache.end())
	{
		auto *t = new T();
		t->setName(name);
		cache[name] = t;
		obj = t;
		return false;
	}
	else
	{
		obj = it->second;
		return true;
	}
}

template <typename T>
static bool exists(const std::string &name, T &cache)
{
	return cache.find(name) != cache.end();
}

template <typename T, typename U>
static T &get(const std::string &name, U &cache)
{
	auto it = cache.find(name);
	if (it == cache.end())
	{
		auto *t = new T();
		t->setName(name);
		cache[name] = t;
		return *t;
	}
	return *it->second;
}

template <typename T, typename U>
static T &add(T *obj, U &cache, const std::string &type)
{
	static T empty;
	if (obj->getName().empty())
	{
		std::cout << "Cannot add " << type << " with no name to cache\n";
		return empty;
	}

	auto it = cache.find(obj->getName());
	if (it != cache.end())
		std::cout << "Warning: " << type << " " << obj->getName() << " already exists in " << type << " cache\n";

	cache[obj->getName()] = obj;
	return *obj;
}

template <typename T>
static void clear(T &cache)
{
	for (auto it : cache)
		delete it.second;
	cache.clear();
}



void Texture2DCache::clear()
{
	::clear(cache);
}

bool Texture2DCache::getTexture(const std::string &name, Texture2D *&tex)
{
	return get(name, tex, cache);
}

bool Texture2DCache::exists(const std::string &name)
{
	return ::exists(name, cache);
}

Texture2D &Texture2DCache::getTexture(const std::string &name)
{
	return get<Texture2D>(name, cache);
}

Texture2D &Texture2DCache::addTexture(Texture2D *tex)
{
	return add(tex, cache, "texture");
}



void UniformBufferCache::clear()
{
	::clear(cache);
}

bool UniformBufferCache::getUniformBuffer(const std::string &name, UniformBuffer *&buf)
{
	return get(name, buf, cache);
}

bool UniformBufferCache::exists(const std::string &name)
{
	return ::exists(name, cache);
}

UniformBuffer &UniformBufferCache::getUniformBuffer(const std::string &name)
{
	return get<UniformBuffer>(name, cache);
}

UniformBuffer &UniformBufferCache::addUniformBuffer(UniformBuffer *buf)
{
	return add(buf, cache, "uniform buffer");
}
