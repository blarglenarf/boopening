#include "GPUObject.h"

#include <iostream>

using namespace Rendering;

bool GPUObject::setUniformInShader(const Shader *shader, const std::string &name)
{
	(void) (shader);
	(void) (name);
	std::cout << "setUniformInShader not implemented for this GPUObject type\n";
	return false;
}

void GPUObject::setUniformInShader(const Shader *shader, GLint loc)
{
	(void) (shader);
	(void) (loc);
	std::cout << "setUniformInShader not implemented for this GPUObject type\n";
}



int GPUObjectGroup::add(GPUObject *obj)
{
	int loc = -1;
	for (int i = 0; i < (int) uniques.size(); i++)
	{
		if (uniques[i] == nullptr)
		{
			loc = i;
			break;
		}
		if (uniques[i] == obj)
		{
			std::cout << "GPUObject has already been bound in this object group\n";
			return -1;
		}
	}

	if (loc == -1)
	{
		uniques.push_back(obj);
		return (int) uniques.size() - 1;
	}
	else
	{
		uniques[loc] = obj;
		return loc;
	}
}

void GPUObjectGroup::remove(GPUObject *obj)
{
	for (size_t i = 0; i < uniques.size(); i++)
		if (uniques[i] == obj)
			uniques[i] = nullptr;
}

int GPUObjectGroup::getIndex(GPUObject *obj)
{
	for (int i = 0; i < (int) uniques.size(); i++)
		if (uniques[i] == obj)
			return i;
	return -1;
}

bool GPUObjectGroup::exists(GPUObject *obj)
{
	return getIndex(obj) != -1;
}

