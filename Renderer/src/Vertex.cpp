#include "Vertex.h"

#include <iostream>

std::ostream &operator << (std::ostream &ostr, const Rendering::Vertex &v)
{
	ostr << "pos: " << v.pos << "; norm: " << v.norm << "; tex: " << v.tex << ";";
	return ostr;
}

