#include "Profiler.h"

#include <iostream>

using namespace Rendering;

GLuint Profiler::getNextQueryObject()
{
	GLuint query = 0;
	if (queryObjects.size() <= currQueryObj)
	{
		glGenQueries(1, &query);
		queryObjects.push_back(query);
	}
	else
		query = queryObjects[currQueryObj];
	currQueryObj++;
	return query;
}



void Profiler::clear()
{
	for (auto o : queryObjects)
		glDeleteQueries(1, &o);
	queryObjects.clear();
	timeStamps.clear();
	queries.clear();
	bufferSizes.clear();
}

void Profiler::begin()
{
	if (ready)
		ready = false;

	if (queryObjects.empty() || queries.empty())
	{
		ready = true;
		return;
	}

	// If the last query is done, they are all done, get their results.
	GLint available = 0;
	glGetQueryObjectiv(queryObjects[queryObjects.size() - 1], GL_QUERY_RESULT_AVAILABLE, &available);
	if (available == 1)
	{
		// Get results.
		timeStamps.resize(queryObjects.size());
		for (size_t i = 0; i < queryObjects.size(); i++)
		{
			GLuint64 result;
			glGetQueryObjectui64v(queryObjects[i], GL_QUERY_RESULT, &result);
			timeStamps[i] = result;
		}

		ready = true;

		// TODO: restart the queries.
		// FIXME: Figure out if this goes inside or outside the if statement.
		currQueryObj = 0;
	}
}

void Profiler::start(const std::string &name)
{
	// Only start a new query if we're not already processing queries.
	if (!ready)
		return;

	// TODO: valgrind doesn't like the way I'm using glQueryCounter(), figure out why.
	auto it = queries.find(name);
	if (it == queries.end())
	{
		// Brand new query.
		auto &query = queries[name];
		query.timeFrom = (int) currQueryObj;
		GLuint queryObj = getNextQueryObject();
		glQueryCounter(queryObj, GL_TIMESTAMP);
	}
	else
	{
		// Pre-existing query, start it again.
		auto &query = it->second;
		query.timeFrom = (int) currQueryObj;
		GLuint queryObj = getNextQueryObject();
		glQueryCounter(queryObj, GL_TIMESTAMP);
	}
}

void Profiler::time(const std::string &name)
{
	// Only get the time for a query if we're not already processing queries.
	if (!ready)
		return;

	// TODO: valgrind doesn't like the way I'm using glQueryCounter(), figure out why.
	auto it = queries.find(name);
	if (it == queries.end())
	{
		// Unknown start, just time from the last time point.
		auto &query = queries[name];
		query.timeFrom = (int) currQueryObj - 1;
		query.timeTo = (int) currQueryObj;
		GLuint queryObj = getNextQueryObject();
		glQueryCounter(queryObj, GL_TIMESTAMP);
	}
	else
	{
		auto &query = it->second;
		query.timeTo = (int) currQueryObj;
		GLuint queryObj = getNextQueryObject();
		glQueryCounter(queryObj, GL_TIMESTAMP);
	}
}

float Profiler::getTime(const std::string &name)
{
	if (timeStamps.empty())
		return -1;

	auto it = queries.find(name);
	if (it == queries.end())
		return -1;

	auto &query = it->second;

	if (query.timeTo < 0 || query.timeTo >= (int) timeStamps.size() || query.timeFrom < 0 || query.timeFrom >= (int) timeStamps.size())
		return -1;

	GLuint64 result = timeStamps[query.timeTo] - timeStamps[query.timeFrom];
	return (float) ((double) result / 1000000.0);
}

std::vector<std::pair<std::string, float>> Profiler::getTimes()
{
	// FIXME: This vector is constantly re-alloc'd, maybe do this a little differently.
	std::vector<std::pair<std::string, float>> times;

	if (queries.empty())
		return times;

	times.reserve(queries.size());
	for (auto it : queries)
		times.push_back({it.first, getTime(it.first)});

	return times;
}

void Profiler::setBufferSize(const std::string &buffer, size_t bytes)
{
	bufferSizes[buffer] = bytes;
}

size_t Profiler::getBufferSize(const std::string &buffer)
{
	if (buffer == "total")
	{
		size_t totalSize = 0;
		for (auto &it : bufferSizes)
			totalSize += it.second;
		return totalSize;
	}

	return bufferSizes[buffer];
}

void Profiler::print()
{
	for (auto it : queries)
		std::cout << it.first << ": " << getTime(it.first) << "ms\n";
	for (auto it : bufferSizes)
		std::cout << it.first << ": " << (it.second / 1000000.0) << "mb\n";
	std::cout << "\n";
}

