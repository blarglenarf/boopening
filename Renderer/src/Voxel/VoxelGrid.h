#pragma once

#include "../GL.h"
#include "../Camera.h"
#include "../RenderObject/GPUBuffer.h"
#include "../RenderObject/Shader.h"

namespace Rendering
{
	class Profiler;

	class VoxelGrid
	{
		// TODO: May want to specify what information is stored in each voxel.

	private:
		StorageBuffer voxelGrid;
		StorageBuffer voxelList;
		AtomicBuffer voxelCount;

		// Voxel list only stores voxels at the highest grid level (maximum resolution).
		Shader countVoxelListShader;
		Shader buildVoxelListShader;

		Shader drawVoxelGridShader;
		Shader drawVoxelListShader;

		Shader fillVoxelLevelShader;

		int size; // Grid dimensions are all the same (size * size * size).
		int nLevels; // Total number of grid levels based on size.

		unsigned int nGridVoxels; // Total number of voxels in all levels of the grid.
		unsigned int nListVoxels; // Number of voxels in the voxel list.

		Profiler *profiler;

	private:
		void zeroGrid();

	public:
		VoxelGrid() : size(0), nLevels(0), nGridVoxels(0), nListVoxels(0), profiler(nullptr) {}
		~VoxelGrid() {}

		void release();
		void releaseShaders();

		void resize(int size);

		void beginCapture();
		void endCapture(bool fillLevelsFlag = true);

		void setUniforms(Shader *shader);

		void fillLevels();
		void buildVoxelList();

		void debugRender(Camera &camera, int level = -1, bool voxelListFlag = false);

		Rendering::StorageBuffer &getVoxelGrid() { return voxelGrid; }
		Rendering::StorageBuffer &getVoxelList() { return voxelList; }

		unsigned int getNGridVoxels() const { return nGridVoxels; }
		unsigned int getNListVoxels() const { return nListVoxels; }

		int getNLevels() const { return nLevels; }
		unsigned int getStartIndex(int level) const;

		int getSize() const { return size; }

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }
	};
}

