#include "VoxelGrid.h"
#include "../Profiler.h"
#include "../RenderResource/ShaderSource.h"
#include "../RenderResource/RenderResourceCache.h"

#include <cmath>

using namespace Rendering;


void VoxelGrid::zeroGrid()
{
	static Shader zeroVoxelShader;
	if (!zeroVoxelShader.isGenerated())
	{
		auto zeroVoxelVert = ShaderSourceCache::getShader("zeroVoxels").loadFromFile("shaders/zero.vert");
		zeroVoxelVert.replace("DATA_TYPE", "uvec4"); // TODO: May want to store different types of data in the grid.
		zeroVoxelShader.create(&zeroVoxelVert);
	}

	if (profiler)
		profiler->start("zero voxels");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glEnable(GL_RASTERIZER_DISCARD);
	zeroVoxelShader.bind();
	zeroVoxelShader.setUniform("Data", &voxelGrid);
	glDrawArrays(GL_POINTS, 0, nGridVoxels); // FIXME: Not working correctly...
	zeroVoxelShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	if (profiler)
		profiler->time("zero voxels");
}


void VoxelGrid::release()
{
	voxelGrid.release();
	voxelList.release();
	voxelCount.release();

	countVoxelListShader.release();
	buildVoxelListShader.release();
	drawVoxelGridShader.release();
	drawVoxelListShader.release();
}

void VoxelGrid::releaseShaders()
{
	countVoxelListShader.release();
	buildVoxelListShader.release();
	drawVoxelGridShader.release();
	drawVoxelListShader.release();
}

void VoxelGrid::resize(int size)
{
	this->size = size;
	this->nLevels = (int) log2(size);

	// Total data required for the grid would be width * height * depth for the bottom level.
#if 1
	// Just start by looping through the appropriate levels I guess.
	nGridVoxels = 0;
	int levelDim = 2;
	for (int i = 0; i < nLevels; i++)
	{
		nGridVoxels += (levelDim * levelDim * levelDim);
		levelDim *= 2;
	}
#else
	// level 0: dim 2,   size 8,        starts at 0
	// level 1: dim 4,   size 64,       starts at 8
	// level 2: dim 8,   size 512,      starts at 72
	// level 3: dim 16,  size 4096,     starts at 584
	// level 4: dim 32,  size 32768,    starts at 4680
	// level 5: dim 64,  size 262144,   starts at 37448
	// level 6: dim 128, size 2097152,  starts at 299592
	// level 7: dim 256, size 16777216, starts at 2396744
	// total size is: 19173960
	//
	// level dimensions are 2 ** (level + 1)
	// level size is dimension ** 3
	// prev size is level size / (2 ** (level + 1))
	// start index is...
#endif

	// 4 uints per-cell (uvec4).
	// TODO: Need to take the different levels into account.
	voxelGrid.release();
	voxelGrid.create(nullptr, nGridVoxels * 4 * sizeof(unsigned int));

	voxelList.release();
	voxelCount.release();

	nListVoxels = 0;
	voxelCount.create(&nListVoxels, sizeof(nListVoxels));

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
}

void VoxelGrid::beginCapture()
{
	// First zero the voxel grid.
	zeroGrid();

	// Then build the uniform voxel grid.
	if (profiler)
		profiler->start("Capture voxel grid");

	// TODO: May not want to do this stuff here, not sure.
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void VoxelGrid::endCapture(bool fillLevelsFlag)
{
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);

	glPopAttrib();

	if (profiler)
		profiler->time("Capture voxel grid");

	// May not always want to do this. Not sure.
	if (fillLevelsFlag)
		fillLevels();
}

void VoxelGrid::setUniforms(Shader *shader)
{
	shader->setUniform("VoxelGrid", &voxelGrid);
	//shader.setUniform("mvMatrix", deepCamZ.getInverse());
	//shader.setUniform("pMatrix", deepCamZ.getProjection());
}

void VoxelGrid::fillLevels()
{
	if (!fillVoxelLevelShader.isGenerated())
	{
		auto fillVoxelVert = ShaderSourceCache::getShader("fillVoxels").loadFromFile("shaders/voxel/fillVoxelLevels.vert");
		//fillVoxelVert.replace("DATA_TYPE", "uvec4");
		fillVoxelLevelShader.create(&fillVoxelVert);
	}

	// TODO: Alternative is to build the voxel list first, then fill upper levels from that.
	auto &shader = fillVoxelLevelShader;

	if (profiler)
		profiler->start("Fill voxel levels");

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_RASTERIZER_DISCARD);

	shader.bind();

	shader.setUniform("VoxelGrid", &voxelGrid);

	for (int level = nLevels - 2; level >= 0; level--)
	{
		// Get startIndex and dimensions for level.
		int startIndex = 0;
		int lvlDim = 2;
		for (int i = 0; i < level; i++)
		{
			startIndex += (lvlDim * lvlDim * lvlDim);
			lvlDim *= 2;
		}

		//shader.setUniform("level", level);
		shader.setUniform("startIndex", startIndex);
		shader.setUniform("dim", lvlDim);

		// Get number of voxels for level.
		int lvlVoxels = lvlDim * lvlDim * lvlDim;

		// Each thread reads colour data for 8 child voxels, this becomes the voxel's colour.
		glDrawArrays(GL_POINTS, 0, lvlVoxels);

		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	shader.unbind();

	glDisable(GL_RASTERIZER_DISCARD);
	glPopAttrib();

	if (profiler)
		profiler->time("Fill voxel levels");
}

void VoxelGrid::buildVoxelList()
{
	// TODO: Voxel index can just be an integer assuming we only go up to 1024 (easily done really).
	// TODO: This means each unique voxel can be a vec2.
	// TODO: The remaining 2 ints of a vec4 can store the normal vector or some other information maybe?
	//uint uPos = voxelListData[id];
	//uvec3 pos = uvec3((uPos >> 22U) & 1023, (uPos >> 12U) & 1023, (uPos >> 2U) & 1023);

	if (!countVoxelListShader.isGenerated())
	{
		auto countVoxelListVert = ShaderSourceCache::getShader("countVoxelListVert").loadFromFile("shaders/voxel/voxelList.vert");
		countVoxelListVert.setDefine("COUNT", "1");
		//countVoxelListVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
		//countVoxelListShader.release();
		countVoxelListShader.create(&countVoxelListVert);
	}
	if (!buildVoxelListShader.isGenerated())
	{
		auto buildVoxelListVert = ShaderSourceCache::getShader("buildVoxelListVert").loadFromFile("shaders/voxel/voxelList.vert");
		buildVoxelListVert.setDefine("COUNT", "0");
		//buildVoxelListVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
		buildVoxelListShader.release();
		buildVoxelListShader.create(&buildVoxelListVert);
	}

	int startIndex = 0;
	int lvlDim = 2;
	for (int i = 0; i < nLevels - 1; i++)
	{
		startIndex += (lvlDim * lvlDim * lvlDim);
		lvlDim *= 2;
	}

	// Now build the global voxel list.
	if (profiler)
		profiler->start("Voxel list count");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	// Reset the counter.
	nListVoxels = 0;
	voxelCount.bufferData(&nListVoxels, sizeof(nListVoxels));

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	// Count the alive voxels first, then resize the list.
	glEnable(GL_RASTERIZER_DISCARD);
	countVoxelListShader.bind();
	countVoxelListShader.setUniform("voxelCount", &voxelCount);
	countVoxelListShader.setUniform("VoxelGrid", &voxelGrid);
	countVoxelListShader.setUniform("dim", size);
	countVoxelListShader.setUniform("startIndex", startIndex);
	glDrawArrays(GL_POINTS, 0, size * size * size);
	countVoxelListShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	nListVoxels = *((unsigned int*) voxelCount.read());

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	if (profiler)
		profiler->time("Voxel list count");

	if (profiler)
		profiler->start("Voxel list build");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	// Create the list, give it a reasonably large size (one vec4 per-voxel storing position and diffuse).
	unsigned int zero = 0;
	voxelCount.bufferData(&zero, sizeof(zero));
	voxelList.create(nullptr, nListVoxels * 4 * sizeof(float)); // TODO: Allow different data types.

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	glEnable(GL_RASTERIZER_DISCARD);
	buildVoxelListShader.bind();
	buildVoxelListShader.setUniform("voxelCount", &voxelCount);
	buildVoxelListShader.setUniform("VoxelGrid", &voxelGrid);
	buildVoxelListShader.setUniform("VoxelList", &voxelList);
	buildVoxelListShader.setUniform("dim", size);
	buildVoxelListShader.setUniform("startIndex", startIndex);
	glDrawArrays(GL_POINTS, 0, size * size * size);
	buildVoxelListShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	if (profiler)
		profiler->time("Voxel list build");
}

void VoxelGrid::debugRender(Camera &camera, int level, bool voxelListFlag)
{
	// If voxelListFlag is true, then we always draw the highest res.
	if (level < 0)
		level = nLevels - 1;

	Shader &shader = voxelListFlag ? drawVoxelListShader : drawVoxelGridShader;

	if (!shader.isGenerated())
	{
		std::string listStr = voxelListFlag ? "1" : "0";
		auto drawVoxelVert = ShaderSourceCache::getShader("drawVoxelVert").loadFromFile("shaders/voxel/drawVoxels.vert");
		auto drawVoxelGeom = ShaderSourceCache::getShader("drawVoxelGeom").loadFromFile("shaders/voxel/drawVoxels.geom");
		auto drawVoxelFrag = ShaderSourceCache::getShader("drawVoxelFrag").loadFromFile("shaders/voxel/drawVoxels.frag");
		drawVoxelVert.setDefine("VOXEL_LIST", listStr);
		drawVoxelGeom.setDefine("VOXEL_LIST", listStr);
		shader.create(&drawVoxelVert, &drawVoxelFrag, &drawVoxelGeom);
	}

	int startIndex = 0;
	int dim = 2;
	for (int i = 0; i < level; i++)
	{
		startIndex += (dim * dim * dim);
		dim *= 2;
	}

	shader.bind();

	if (voxelListFlag)
		shader.setUniform("VoxelList", &voxelList);
	else
		shader.setUniform("VoxelGrid", &voxelGrid);

	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("dim", dim);
	shader.setUniform("startIndex", startIndex);

	if (voxelListFlag)
		glDrawArrays(GL_POINTS, 0, nListVoxels);
	else
		glDrawArrays(GL_POINTS, 0, dim * dim * dim);

	shader.unbind();
}

unsigned int VoxelGrid::getStartIndex(int level) const
{
	unsigned int startIndex = 0;
	int dim = 2;
	for (int i = 0; i < level; i++)
	{
		startIndex += (dim * dim * dim);
		dim *= 2;
	}

	return startIndex;
}

