#pragma once

#include "GL.h"

#include <string>
#include <vector>
#include <map>

namespace Rendering
{
	class Profiler
	{
	private:
		struct Query
		{
			int timeFrom;
			int timeTo;
			Query(int timeFrom = -1, int timeTo = -1) : timeFrom(timeFrom), timeTo(timeTo) {}
		};

	private:
		std::vector<GLuint> queryObjects;
		std::vector<GLuint64> timeStamps;
		std::map<std::string, Query> queries;

		std::map<std::string, size_t> bufferSizes;

		bool ready;

		size_t currQueryObj;

	private:
		GLuint getNextQueryObject();

	public:
		Profiler() : ready(false), currQueryObj(0) {}
		~Profiler() { clear(); }

		void clear();

		void begin();

		void start(const std::string &name);
		void time(const std::string &name); // TODO: Don't like the way it's start/time, but end isn't quite right either.

		float getTime(const std::string &name);
		std::vector<std::pair<std::string, float>> getTimes();

		void setBufferSize(const std::string &buffer, size_t bytes);
		size_t getBufferSize(const std::string &buffer);

		void print();
	};
}

