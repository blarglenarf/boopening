#pragma once

#include "../../Math/src/MathCommon.h"

#include <vector>
#include <map>

namespace Rendering
{
	class Profiler;

	struct KeyFrame
	{
		float time;
		Math::vec3 value;

		KeyFrame(float time = 0, Math::vec3 val = Math::vec3()) : time(time), value(val) {}
	};

	class Interpolator
	{
		// TODO: Different kinds of interpolation.
	private:
		float maxTime;
		float currTime;

		Math::vec3 initialVal;
		Math::vec3 *val;

		std::vector<KeyFrame> keyFrames;

	public:
		Interpolator(Math::vec3 *val = nullptr, float maxTime = 0, Math::vec3 initialVal = Math::vec3()) : maxTime(maxTime), currTime(0), initialVal(initialVal), val(val) {}

		void addKeyFrame(float time, Math::vec3 val);
		void reset();

		bool update(float dt);
	};

	class Flythrough
	{
	private:
		bool animFlag;

		std::vector<Interpolator> interpolators;

		std::string animFilename;
		std::string csvFilename;

		unsigned int frameNo;
		int extraFrames;
		int currExtraFrame;

		std::map<std::string, std::vector<float>> profileTimes;

		Profiler *profiler;

	public:
		Flythrough() : animFlag(false), frameNo(0), extraFrames(20), currExtraFrame(0), profiler(nullptr) { interpolators.reserve(20); }

		size_t addInterpolator(Math::vec3 *val, float maxTime, Math::vec3 initialVal);
		Interpolator &getInterpolator(size_t index) { return interpolators[index]; }

		float update(float dt);

		bool isAnimating() const { return animFlag; }
		void play() { animFlag = true; frameNo = 0; currExtraFrame = 0; }

		void setAnimFilename(const std::string &animFilename) { this->animFilename = animFilename; }
		void setCSVFilename(const std::string &csvFilename) { this->csvFilename = csvFilename; }
		void setExtraFrames(int extraFrames) { this->extraFrames = extraFrames; }

		void setProfiler(Profiler *profiler) { this->profiler = profiler; }
		void addProfilerTime(const std::string &str) { profileTimes[str] = std::vector<float>(); }

		void writeProfilerCSV();
	};
}

