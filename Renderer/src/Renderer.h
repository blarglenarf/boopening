#pragma once

#include "Camera.h"
#include "RenderObject/GPUBuffer.h"
#include "RenderObject/Shader.h"

#include <string>
#include <map>

namespace Rendering
{
	class Camera;

	class Renderer
	{
	private:
		Camera *activeCamera;
		std::map<std::string, Camera> cameras;

		VertexArrayObject quad;

		// TODO: Shader for rendering a fullscreen quad.
		Shader blankShader;
		Shader basicShader;

		static Renderer renderer;

	private:
		Renderer() : activeCamera(nullptr) {}

	public:
		~Renderer();

		static Renderer &instance();

		void init();
		void clear();

		Camera &addCamera(const std::string &name, Camera camera);
		Camera &getCamera(const std::string &name);

		Camera &setActiveCamera(const std::string &name);
		Camera &getActiveCamera();

		Shader &getBlankShader() { return blankShader; }
		Shader &getBasicShader() { return basicShader; }
		
		void drawAxes(Math::vec3 pos = Math::vec3(0, 0, 0), float len = 5);
		void drawQuad();
		void drawBounds(Math::vec3 min = Math::vec3(-1, -1, -1), Math::vec3 max = Math::vec3(1, 1, 1));
	};
}

