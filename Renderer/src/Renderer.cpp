#include "Renderer.h"

#include "Camera.h"
#include "GL.h"
#include "RenderResource/Mesh.h"
#include "RenderResource/Image.h"
#include "RenderResource/Material.h"
#include "RenderResource/ShaderSource.h"
#include "RenderResource/RenderResourceCache.h"
#include "RenderObject/GPUObjectCache.h"

using namespace Rendering;

Renderer Renderer::renderer;

Renderer::~Renderer()
{
}

Renderer &Renderer::instance()
{
	return renderer;
}

void Renderer::init()
{
	glewInit();

	//printExtensionList(); // Don't really want this to print every single time.

	glDisable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	glClearColor(0, 0, 0, 0);

	glEnable(GL_CULL_FACE);

	auto quadMesh = Mesh::getSquare();
	quad.create(&quadMesh);

	auto &blankVert = ShaderSourceCache::getShader("defaultVert");
	auto &blankFrag = ShaderSourceCache::getShader("defaultFrag");
	blankShader.create(&blankVert, &blankFrag);

	auto &basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto &basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	auto &basicGeom = ShaderSourceCache::getShader("basicGeom").loadFromFile("shaders/basic.geom");
	basicShader.create(&basicVert, &basicFrag, &basicGeom);
}

void Renderer::clear()
{
	UniformBufferCache::clear();
	Texture2DCache::clear();
	MaterialCache::clear();
	ShaderSourceCache::clear();
	ImageCache::clear();
}

Camera &Renderer::addCamera(const std::string &name, Camera camera)
{
	cameras[name] = camera;
	if (!activeCamera)
		activeCamera = &cameras[name];
	return cameras[name];
}

Camera &Renderer::getCamera(const std::string &name)
{
	if (cameras.find(name) == cameras.end())
	{
		Camera cam;
		addCamera(name, cam);
	}
	return cameras[name];
}

Camera &Renderer::setActiveCamera(const std::string &name)
{
	activeCamera = &cameras[name];
	return *activeCamera;
}

Camera &Renderer::getActiveCamera()
{
	return *activeCamera;
}

void Renderer::drawAxes(Math::vec3 pos, float len)
{
	glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINES);

	glColor3f(1, 0, 0);
	glVertex3f(pos.x, pos.y, pos.z); glVertex3f(pos.x + len, pos.y, pos.z);

	glColor3f(0, 1, 0);
	glVertex3f(pos.x, pos.y, pos.z); glVertex3f(pos.x, pos.y + len, pos.z);

	glColor3f(0, 0, 1);
	glVertex3f(pos.x, pos.y, pos.z); glVertex3f(pos.x, pos.y, pos.z + len);

	glEnd();

	glPopAttrib();
}

void Renderer::drawQuad()
{
#if 0
	glPushAttrib(GL_TRANSFORM_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glBegin(GL_QUADS);
	glVertex3f(-1, -1, 0);
	glVertex3f(-1, 1, 0);
	glVertex3f(1, 1, 0);
	glVertex3f(1, -1, 0);
	glEnd();

	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
#else
	quad.render();
#endif
}

void Renderer::drawBounds(Math::vec3 min, Math::vec3 max)
{
	glBegin(GL_LINES);

	glVertex3f(min.x, min.y, min.z); glVertex3f(max.x, min.y, min.z);
	glVertex3f(min.x, max.y, min.z); glVertex3f(max.x, max.y, min.z);

	glVertex3f(min.x, min.y, max.z); glVertex3f(max.x, min.y, max.z);
	glVertex3f(min.x, max.y, max.z); glVertex3f(max.x, max.y, max.z);

	glVertex3f(min.x, max.y, min.z); glVertex3f(min.x, min.y, min.z);
	glVertex3f(max.x, max.y, min.z); glVertex3f(max.x, min.y, min.z);

	glVertex3f(min.x, max.y, max.z); glVertex3f(min.x, min.y, max.z);
	glVertex3f(max.x, max.y, max.z); glVertex3f(max.x, min.y, max.z);

	glVertex3f(min.x, min.y, min.z); glVertex3f(min.x, min.y, max.z);
	glVertex3f(max.x, min.y, min.z); glVertex3f(max.x, min.y, max.z);

	glVertex3f(min.x, max.y, min.z); glVertex3f(min.x, max.y, max.z);
	glVertex3f(max.x, max.y, min.z); glVertex3f(max.x, max.y, max.z);

	glEnd();
}

