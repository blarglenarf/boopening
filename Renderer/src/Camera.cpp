#include "Camera.h"
#include "GL.h"
#include "RenderObject/Shader.h"

#include "../../Math/src/MathGeneral.h"

#include <string>
#include <iostream>
#include <vector>

// Dammit windows stop doing this to me.
#undef near
#undef far
#undef NEAR
#undef FAR
#undef IN
#undef OUT

using namespace Rendering;

void Camera::Frustum::calcFrustum(Camera *camera)
{
	// View frustum code based on http://ruh.li/CameraViewFrustum.html and http://www.lighthouse3d.com/tutorials/view-frustum-culling/clip-space-approach-implementation-details/ with alterations.
	auto mat = camera->inverse * camera->proj;

	// For each plane, xyz are normal components, w is the d component.
	auto &left = planes[LEFT];
	left.x = mat.t[0][0] + mat.t[0][3];
	left.y = mat.t[1][0] + mat.t[1][3];
	left.z = mat.t[2][0] + mat.t[2][3];
	left.w = mat.t[3][0] + mat.t[3][3];

	auto &right = planes[RIGHT];
	right.x = -mat.t[0][0] + mat.t[0][3];
	right.y = -mat.t[1][0] + mat.t[1][3];
	right.z = -mat.t[2][0] + mat.t[2][3];
	right.w = -mat.t[3][0] + mat.t[3][3];

	auto &bottom = planes[BOTTOM];
	bottom.x = mat.t[0][1] + mat.t[0][3];
	bottom.y = mat.t[1][1] + mat.t[1][3];
	bottom.z = mat.t[2][1] + mat.t[2][3];
	bottom.w = mat.t[3][1] + mat.t[3][3];

	auto &top = planes[TOP];
	top.x = -mat.t[0][1] + mat.t[0][3];
	top.y = -mat.t[1][1] + mat.t[1][3];
	top.z = -mat.t[2][1] + mat.t[2][3];
	top.w = -mat.t[3][1] + mat.t[3][3];

	auto &near = planes[NEAR];
	near.x = mat.t[0][2] + mat.t[0][3];
	near.y = mat.t[1][2] + mat.t[1][3];
	near.z = mat.t[2][2] + mat.t[2][3];
	near.w = mat.t[3][2] + mat.t[3][3];

	auto &far = planes[FAR];
	far.x = -mat.t[0][2] + mat.t[0][3];
	far.y = -mat.t[1][2] + mat.t[1][3];
	far.z = -mat.t[2][2] + mat.t[2][3];
	far.w = -mat.t[3][2] + mat.t[3][3];

	// Normalize based on normal component only.
	float l = 1.0f;
	for (int i = 0; i < 6; i++)
	{
		l = Math::vec3(planes[i].xyz).len();
		planes[i] /= l;
	}
}



Camera::Camera(Type type, int x, int y, int width, int height, float near, float far, float sensitivity) : fov(75.0f * (float) M_PI / 180.0f), nearPlane(near), farPlane(far), zoom(0.0f), sensitivity(sensitivity), forward(0.0f, 0.0f, -1.0f), up(0.0f, 1.0f, 0.0f), right(1.0f, 0.0f, 0.0f), calcProjFlag(true), calcTransFlag(true), orthoCoordFlag(false), gimbalLockFlag(true), type(type)
{
	proj.identity();
	transform.identity();
	inverse.identity();

	setViewport(x, y, width, height);
}
#if 0
Camera::Camera(Resources::XmlLibrary::XmlData &data) : fov(90), nearPlane(0.1f), farPlane(100), zoom(1), calcProjFlag(true), calcTransFlag(true), type(Type::NORMAL)
{
	handler = new WindowSizeHandler(this);
	Platform::EventHandler::addHandler(Platform::Event::Type::WINDOW_RESIZED, handler);

	proj.identity();
	transform.identity();
	inverse.identity();

	if (data["Near"] != "")
		nearPlane = std::stof(data["Near"]);
	if (data["Far"] != "")
		farPlane = std::stof(data["Far"]);
	if (data["Fov"] != "")
		fov = std::stof(data["Fov"]);
	if (data["Zoom"] != "")
		zoom = std::stof(data["Zoom"]);

	int x = data["X"] != "" ? std::stoi(data["X"]) : 0;
	int y = data["Y"] != "" ? std::stoi(data["Y"]) : 0;
	int width = data["Width"] != "" ? std::stoi(data["Width"]) : 800;
	int height = data["Height"] != "" ? std::stoi(data["Height"]) : 600;

	setViewport(x, y, width, height);

	if (data["Pos"] != "")
	{
		// TODO: this, but better
		Math::vec3f p;
		auto tags = Utils::split(data["Pos"], Resources::XmlLibrary::sep());
		if (tags.size() >= 1)
			p.x() = std::stof(tags[0]);
		if (tags.size() >= 2)
			p.y() = std::stof(tags[1]);
		if (tags.size() >= 3)
			p.z() = std::stof(tags[2]);
		setPos(p);
	}

	if (data["Rot"] != "")
	{
		// TODO: this, but better
		Math::Quat r;
		auto tags = Utils::split(data["Rot"], Resources::XmlLibrary::sep());
		if (tags.size() >= 1)
			r.x = std::stof(tags[0]);
		if (tags.size() >= 2)
			r.y = std::stof(tags[1]);
		if (tags.size() >= 3)
			r.z = std::stof(tags[2]);
		if (tags.size() >= 4)
			r.w = std::stof(tags[3]);
		setRot(r);
	}

	if (data["Type"] == "ortho")
		type = Type::ORTHO;
	else if (data["Type"] == "persp")
		type = Type::PERSP;
}
#endif
Camera::~Camera()
{
}

void Camera::calcProjection()
{
	switch (type)
	{
	case Type::ORTHO:
		if (!orthoCoordFlag)
			proj = Math::getOrtho<float>(0, (float) viewport.z, 0, (float) viewport.w, 0, farPlane);
		else
			proj = Math::getOrtho<float>(orthoCoords.x, orthoCoords.y, orthoCoords.z, orthoCoords.w, nearPlane, farPlane);
		invProj = proj.getInverse();
		break;
	case Type::PERSP:
		proj = Math::getPerspective<float>(fov, (float) viewport.z / (float) viewport.w, nearPlane, farPlane);
		invProj = proj.getInverse();
		break;
	default:
		proj.identity();
		invProj.identity();
		break;
	}

	calcProjFlag = false;
}

void Camera::calcTransform()
{
	auto totalRot = quatRot;

	transform.identity();

	if (zoom > 0)
	{
		if (type == Type::PERSP)
			transform.translate(0, 0, zoom);
		else
		{
			// TODO: Test this against the raytracing stuff, make sure it all still works.
		#if 0
			if (orthoCoordFlag)
			{
				float width = orthoCoords.y - orthoCoords.x;
				float height = orthoCoords.w - orthoCoords.z;
				float depth = farPlane - nearPlane;
				transform.scale(zoom / width, zoom / height, zoom / depth);
			}
			else
		#endif
				transform.scale(1.0f / zoom, 1.0f / zoom, 1.0f / zoom);
		}
	}

	if (type == Type::ORTHO && !orthoCoordFlag)
		transform.translate(pos);

	if (gimbalLockFlag)
	{
		transform.rotateX(eulerRot.x);
		transform.rotateY(eulerRot.y);
	}
	else
		transform *= totalRot.getMatrix();

	// TODO: Test this with the raytracing stuff, make sure it all still works.
	if (type != Type::ORTHO || orthoCoordFlag)
		transform.translate(pos);

	// TODO: if attached to something, translate here as well

	inverse = transform.getInverse();

	forward = (transform * Math::vec4(0.0f, 0.0f, -1.0f, 0.0f)).xyz;
	up = (transform * Math::vec4(0.0f, 1.0f, 0.0f, 0.0f)).xyz;
	right = (transform * Math::vec4(1.0f, 0.0f, 0.0f, 0.0f)).xyz;
	forward.normalize();
	up.normalize();
	right.normalize();
	frustum.calcFrustum(this);

	calcTransFlag = false;
}

void Camera::reset()
{
	proj.identity();
	invProj.identity();

	transform.identity();
	inverse.identity();

	pos = Math::vec3(0.0f, 0.0f, 0.0f);
	zoom = 0.0f;

	eulerRot = Math::vec3(0.0f, 0.0f, 0.0f);
	quatRot = Math::Quat(0.0f, 0.0f, 0.0f, 1.0f);

	forward = Math::vec3(0.0f, 0.0f, -1.0f);
	up = Math::vec3(0.0f, 1.0f, 0.0f);
	right = Math::vec3(1.0f, 0.0f, 0.0f);
}

void Camera::update(float dt)
{
	(void) (dt);

	if (calcProjFlag)
		calcProjection();

	if (calcTransFlag)
		calcTransform();
}

void Camera::upload() const
{
	glViewport(viewport.x, viewport.y, viewport.z, viewport.w);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(proj);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(inverse);
}

void Camera::upload(const Math::mat4 &proj, const Math::mat4 &mv) const
{
	glViewport(viewport.x, viewport.y, viewport.z, viewport.w);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(proj);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(mv);
}

void Camera::uploadViewport() const
{
	glViewport(viewport.x, viewport.y, viewport.z, viewport.w);
}

void Camera::setUniforms(Shader *shader)
{
	if (!shader)
		return;
	shader->setUniform("mvMatrix", inverse);
	shader->setUniform("pMatrix", proj);
	//shader->setUniform("viewport", viewport); // FIXME: May or may not be necessary...
}

void Camera::setViewport(int x, int y, int width, int height)
{
	if (viewport.x == x && viewport.y == y && viewport.z == width && viewport.w == height)
		return;
	viewport.x = x;
	viewport.y = y;
	viewport.z = width;
	viewport.w = height;
	calcProjFlag = true;
}

void Camera::setOrtho(float left, float right, float bottom, float top)
{
	if (orthoCoords.x == left && orthoCoords.y == right && orthoCoords.z == bottom && orthoCoords.w == top)
		return;
	orthoCoordFlag = true;
	orthoCoords.x = left;
	orthoCoords.y = right;
	orthoCoords.z = bottom;
	orthoCoords.w = top;
	calcProjFlag = true;
}

void Camera::setQuatRot(const Math::Quat &rot)
{
	quatRot = rot;
	// TODO: Something with eulerRot?
	calcTransFlag = true;
}

void Camera::setEulerRot(const Math::vec3 &rot)
{
	eulerRot.x = fmod(rot.x, 2.0f * (float) M_PI);
	eulerRot.y = fmod(rot.y, 2.0f * (float) M_PI);
	eulerRot.z = fmod(rot.z, 2.0f * (float) M_PI);
	if (eulerRot.x >= (float) M_PI * 0.5f)
		eulerRot.x = (float) M_PI * 0.5f - 0.001f;
	if (eulerRot.x <= (float) -M_PI * 0.5f)
		eulerRot.x = (float) -M_PI * 0.5f + 0.001f;
	if (eulerRot.y > (float) M_PI)
		eulerRot.y -= (float) M_PI * 2.0f;
	if (eulerRot.y <= (float) -M_PI)
		eulerRot.y += (float) M_PI * 2.0f;
	if (eulerRot.z > (float) M_PI)
		eulerRot.z -= (float) M_PI * 2.0f;
	if (eulerRot.z <= (float) -M_PI)
		eulerRot.z += (float) M_PI * 2.0f;
	quatRot = Math::Quat::fromEuler(eulerRot);
	calcTransFlag = true;
}

void Camera::setViewDir(const Math::vec3 &forward)
{
	// FIXME: Can't actually get a proper orientation with just a forward vector, need an up vector as well.
	//setQuatRot(Math::Quat::fromTo(Math::vec3(0, 0, -1), forward));
	//if (type == Type::ORTHO)
	//	setEulerRot(Math::Quat::fromTo(Math::vec3(0, 0, 1), forward).euler());
	//else
		setEulerRot(Math::Quat::fromTo(Math::vec3(0, 0, -1), forward).euler());
	//setQuatRot(Math::Quat::toRotation(forward, up, right));
}

void Camera::updateRotation(float dx, float dy)
{
	static const float pi = 3.14159265f;
	static const float toRad = pi / 180.0f;

	float x = -dx * toRad * sensitivity;
	float y = -dy * toRad * sensitivity;

	eulerRot.x -= y;
	eulerRot.y += x;

	if (gimbalLockFlag)
		setEulerRot(eulerRot);
	else
	{
		auto quat = Math::Quat::fromEuler(Math::vec3(x, y, 0));
		setQuatRot(quatRot * quat);
	}
}

void Camera::updateZoom(float dy)
{
	float z = zoom + dy * (zoom + 0.01f) * sensitivity * 0.1f;
	//z = Math::clamp(z, nearPlane, farPlane / 2.0f);
	setZoom(z);
}

void Camera::updatePan(float dx, float dy)
{
	auto p = pos;
	p += right * dx * sensitivity * 0.1f;
	p += up * dy * sensitivity * 0.1f;
	//p.x += dx * sensitivity * 0.1f;
	//p.y += dy * sensitivity * 0.1f;
	setPos(p);
}

void Camera::moveRight(float dt)
{
	auto p = pos;
	p += right * dt;
	setPos(p);
}

void Camera::moveLeft(float dt)
{
	auto p = pos;
	p -= right * dt;
	setPos(p);
}

void Camera::moveForward(float dt)
{
	auto p = pos;
	p += forward * dt;
	setPos(p);
}

void Camera::moveBackward(float dt)
{
	auto p = pos;
	p -= forward * dt;
	setPos(p);
}

void Camera::moveUp(float dt)
{
	auto p = pos;
	p += up * dt;
	setPos(p);
}

void Camera::moveDown(float dt)
{
	auto p = pos;
	p -= up * dt;
	setPos(p);
}

Camera::FrustumCollision Camera::isInFrustum(const Math::vec3 &pos, float radius)
{
	// Frustum culling based on http://www.flipcode.com/archives/Frustum_Culling.shtml.
	float dist = 0.0f;

	for (int i = 0; i < 6; i++)
	{
		dist = Math::dot(Math::vec3(frustum.planes[i].xyz), pos) + frustum.planes[i].w;

		// If this distance is < -sphere.radius, we are outside.
		if (dist < -radius)
			return Camera::OUT;

		// Else if the distance is between +- radius, then we intersect.
		if (fabsf(dist) < radius)
			return Camera::INTERSECT;
	}

	// Otherwise we are fully in view.
	return Camera::IN;
}

std::ostream &operator << (std::ostream &out, const Rendering::Camera &camera)
{
	static std::vector<std::string> types = {"NORMAL", "ORTHO", "PERSPECTIVE"};

	out << "type: " << types[(int) camera.getType()] << "\n";
	out << "pos: " << camera.getPos() << "\n";
	out << "euler rot: " << camera.getEulerRot() << "\n";
	out << "quat rot: " << camera.getQuatRot() << "\n";
	out << "zoom: " << camera.getZoom() << "\n";
	out << "fov/near/far: " << camera.getFov() << ", " << camera.getNear() << ", " << camera.getFar() << "\n";
	out << "viewport: " << camera.getViewport() << "\n";
	out << "forward: " << camera.getForward() << "\n";
	out << "up: " << camera.getUp() << "\n";
	out << "right: " << camera.getRight() << "\n";
	out << "mvMat: " << camera.getInverse() << "\n";
	out << "proj: " << camera.getProjection() << "\n";

	return out;
}

