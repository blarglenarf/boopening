// Gui stuff:
// - Widget rendering using 9-boxes.
// - BoxPool for faster rendering of multiple 9-boxes.
// - Handle different z values in the BoxPool.
// - Different image/colour backgrounds.
// - Transparency.
// - Background image.
// - Color.
// - Hide/unhide 9-boxes.
// - Reuse images from the imageAtlas, and allow boxes to be removed/re-added.
// - Remove widget rendering, any rendering that needs to be done can be handled by the canvas, and a separate font thingy.
// - Text z values should match their widget z values.
// TODO - Combo box entry selecting.
// TODO - Picking cursor position on last line isn't quite correct, also we need to handle home/end keys.
// TODO - Figure out why reported text width is wrong.
// TODO - Mouse wheel scrolling for hovered sliders.
// TODO - Delete cached text when it's no longer needed (eg, text constantly changing in a text box).
// TODO - Maybe just have the font cache check how often text is used, not often enough and it gets deleted.
// TODO - After unhiding list, selected entry doesn't highlight until after scrolling.
// TODO - We don't actually have to update all the widgets, may want to have a vector of widgets that specifically need updating.
// TODO - Test behaviour of mouse down events on windows, do they repeat?
// TODO - Mouse pressed events should really trigger on release, not on button down.
// TODO - Actually use the TextPool as intended, to batch the rendering of text.
// TODO - Text input prompts.
// TODO - Auto text cursoring, especially on drop down combo.
// TODO - Text output: Label (DONE), Tooltip (TODO), StatusBar (TODO), InfoBar (TODO).
// - Button: Button (DONE), CycleButton (DONE), SpinnerButton (DONE), RadioButton (DONE), RadioGroup (DONE), Checkbox (DONE), CheckGroup (DONE), Slider (DONE), Scrollbar (DONE).
// - List: ListBox (DONE), DropDownList (DONE).
// - Text input: ComboBox (DONE), DropDownCombo (DONE), TextBox (DONE), Console (DONE).
// TODO - Container: Panel (DONE), DropPanel (DONE), TabPanel (DONE), DialogBox (TODO), Accordian (TODO), Toolbar (TODO).
// TODO - View: TreeView (TODO), GridView (TODO), FolderView (TODO)
// TODO - Menu: Menu (TODO), MenuBar (TODO), ContextMenu (TODO), PieMenu (TODO).
// TODO - Layout: GridLayout based on bootstrap (TODO), FlowLayout based on captains log (TODO).
// TODO - Default colour scheme, so I don't have to constantly call setDefaultColors all the time.
// TODO - Layouts need to handle different resolutions.
// TODO - Layouts and test needs to handle different dpi.
// TODO - Console commands - not sure if this should be part of the gui.
// TODO - Text rendering in correct order according to widgets (means text rendering has to happen at the same time as widget rendering).
// TODO - Is it a massive problem to use OIT for this? If I already know the position of everything then I should also know the blend function.
// TODO - Defaults: default settings for size/colour etc for widget stuff, so you don't have to create and customise everything.
// TODO - Pools for different groups, so group hiding can be done more easily.
// TODO - Tests: Really need a bunch of test code to make sure all this works as intended (maybe, or I can just assume it all works).
// TODO - Interaction: Click, move, drag, hover, scroll, key press, key hold, widget drag, widget resize, interaction modes.
// TODO - Ability to interact with objects not part of the gui (eg a game object), including different controls for it, and text info.
// TODO - Formatting for display of text (centre/left/right, padding).
// TODO - Some sort of style system, possibly based on css3.
// TODO - Some kind of gui file format thingy.
// TODO - Templates.
// TODO - Gui interaction with a database of some kind, and different files.
// TODO - Markdown support where appropriate.
// TODO - Widget images.
// TODO - A bunch of text interaction (highlight, copy/paste, find, etc).
// TODO - Corner rounding (not really sure that I want this tbh).
// TODO - Gradients (possibly with shapes).
// TODO - Shadow effects.
// TODO - 2d and 3d transforms.
// TODO - Animation/transitions.
// TODO - Some kind of support for different resolution sizes (eg, different layouts maybe?).
// TODO - Text sizes, need a way of making things bigger on larger monitors (eg 4K).
// TODO - Lots of magic numbers in the list class. Get rid of them.
// TODO - Make a WidgetPool class to handle widget memory.

#include <cstdlib>

#include "../../Renderer/src/RendererCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"
#include "../../Gui/src/GuiCommon.h"
#include "../../Gui/src/NineBox/BoxPool.h"


#define UNUSED(x) (void)(x)

using namespace Platform;
using namespace Rendering;
using namespace Resources;
using namespace Gui;

static void update(float dt)
{
	Renderer::instance().getActiveCamera().update(dt);

	Canvas::instance().update(dt, Platform::Mouse::x, Platform::Window::getHeight() - Platform::Mouse::y, Platform::Mouse::dx, -Platform::Mouse::dy);
}

static void render()
{
	Renderer::instance().getActiveCamera().setViewport(0, 0, Window::getWidth(), Window::getHeight());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glLoadIdentity();
	Renderer::instance().getActiveCamera().upload();

	Canvas::instance().render();

	Renderer::instance().drawAxes();
}

static bool mouseDrag(Event &ev)
{
	Canvas::instance().handleMouseDragEvent(ev);

	if (Canvas::instance().isDragging() || Canvas::instance().isClicking())
		return false;

	if (Mouse::isDown(Mouse::LEFT))
		Renderer::instance().getActiveCamera().updateRotation((float) ev.dx, (float) ev.dy);
	if (Mouse::isDown(Mouse::RIGHT))
		Renderer::instance().getActiveCamera().updateZoom((float) ev.dy);
	if (Mouse::isDown(Mouse::MIDDLE))
		Renderer::instance().getActiveCamera().updatePan((float) ev.dx, (float) ev.dy);

	return false;
}

static bool mousePressed(Event &ev)
{
	Canvas::instance().handleMouseClickEvent(ev);
	return false;
}

static bool mouseDown(Event &ev)
{
	Canvas::instance().handleMouseDownEvent(ev);
	return false;
}

static bool mouseUp(Event &ev)
{
	Canvas::instance().handleMouseUpEvent(ev);
	return false;
}

static bool keyPress(Event &ev)
{
	if (ev.key == Keyboard::ESCAPE)
		exit(EXIT_SUCCESS);

	if (Canvas::instance().handleKeyPressEvent(ev))
		return true;

	switch (ev.key)
	{
	case Keyboard::q:
		exit(EXIT_SUCCESS);
		break;

	case Keyboard::p:
		//toggleWireframe();
		break;

	case Keyboard::LEFT:
		//app.usePrevLFB();
		break;

	case Keyboard::RIGHT:
		//app.useNextLFB();
		break;

	case Keyboard::UP:
		//app.doubleLights();
		//app.loadNextMesh();
		break;

	case Keyboard::DOWN:
		//app.halveLights();
		//app.loadPrevMesh();
		break;

	case Keyboard::t:
		//app.toggleTransparency();
		break;

	case Keyboard::l:
		//app.cycleLighting();
		break;

	case Keyboard::b:
		//app.runBenchmarks();
		break;

	case Keyboard::c:
		std::cout << Renderer::instance().getActiveCamera() << "\n";
		break;

	case Keyboard::o:
		//app.saveLFBData();
		break;

	case Keyboard::m:
		//app.loadNextMesh();
		break;

	case Keyboard::a:
		//app.playAnim();
		break;

	case Keyboard::GRAVE:
		Canvas::instance().toggleConsole();
		break;

	default:
		break;
	}

	return false;
}

static bool keyDown(Event &ev)
{
	return Canvas::instance().handleKeyDownEvent(ev);
}

static bool keyUp(Event &ev)
{
	return Canvas::instance().handleKeyUpEvent(ev);
}

static void setDefaultColors(Widget *widget)
{
	auto &box = widget->getNineBox();
	auto col = Math::vec4(0.5, 0.0, 0.5, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.8, 0.0, 0.8, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 2);
}

static void setDefaultToggleColors(Widget *widget)
{
	auto &box = widget->getNineBox();
	auto col = Math::vec4(0.0, 0.5, 0.5, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 3);
	col = Math::vec4(0.0, 0.8, 0.8, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 4);
	col = Math::vec4(0, 1, 1, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 5);
}

void testLabel(WidgetGroup &group)
{
	// Example 9-box for a label.
	//Button label;
	Label *label = new Label();
	Canvas::instance().addWidgetMemory(label);

	NineBox &testBox = label->getNineBox();
	Math::vec4 col(0.8, 0.8, 0.8, 0.8);
	testBox.setColors(col, col, col, col, col, col, col, col, col, 0);

	//float h1 = 50, h2 = 100, h3 = 400, h4 = 450;
	//float v1 = 200, v2 = 250, v3 = 350, v4 = 400;
	//testBox->setVerts(h1, h2, h3, h4, v1, v2, v3, v4);

	// BL, L, TL, BR, R, TR, B, M, T.
	//testBox.useImagesInPool(img, img, img, img, img, img, img, img, img, 1);
	//testBox.useImagesLocal(img, img, img, img, img, img, img, img, img, 1);

	// Just to make sure this works.
	testBox.useBackground(0);
	testBox.hide();
	testBox.unhide();

	// Example text output widgets.
	label->setStr("gui font testing");

	//label->setBorder(5, 5);
	label->setPos(50, 200);
	//label->setSize((float)  label->getTextWidth() + 10, (float) label->getTextHeight() + 10);

	group.addWidget(label);

	//Text &text = label.getText();
	//text.setStr("gui font testing");
	//text.setColor({0, 0, 1});
	//text.setFont(canvas.getFont("vera"));

	// Just to make sure this works.
	//label.getText().hide();
	//label.getText().unhide();

	// Note: coordinate system assumes (0,0) is top left.

	//label.setPos(20, 100);
	//label.setSize(label.getTextWidth() + 20, label.getTextHeight() + 20);
	//label.setBorder(10, 10);

	//label.onClicked([](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); std::cout << "label clicked!\n"; });
}

void testButton(WidgetGroup &group)
{
	Button *button = new Button();
	Canvas::instance().addWidgetMemory(button);
	group.addWidget(button);

	Image *img = new Image();
	ImageLoader::load(img, "images/lenna-small.png");

	NineBox &buttonBox = button->getNineBox();
	auto col = Math::vec4(0.5, 0.0, 0.5, 0.8);
	buttonBox.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.8, 0.0, 0.8, 0.8);
	buttonBox.setColors(col, col, col, col, col, col, col, col, col, 1);
	buttonBox.useImagesInPool(img, img, img, img, img, img, img, img, img, 2);

	button->setStr("i'm a test button");
	//Text &text2 = button.getText();
	//text2.setStr("i'm a test button");
	//text2.setColor({0, 0, 1});
	//text2.setFont(canvas.getFont("vera"));

	button->setPos(50, 150);
	//button->setBorder(5, 5);
	//button->setSize((float) button->getTextWidth() + 10, (float) button->getTextHeight() + 10);

	button->onClicked([](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); std::cout << "button clicked!\n"; });

	button->hide();
	button->unhide();
}

void testCycleButton(WidgetGroup &group)
{
	CycleButton *cycle = new CycleButton();
	Canvas::instance().addWidgetMemory(cycle);
	group.addWidget(cycle);
	setDefaultColors(cycle);

	cycle->setOptions({"Option 1", "Option 2", "Option 3", "Option fgfdgfd 4"});
	cycle->setStr(cycle->getCurrOption());
	//Text &text3 = cycle.getText();
	//text3.setStr(cycle.getCurrOption());
	//text3.setFont(canvas.getFont("vera"));
	//text3.setColor({0, 0, 1});
	auto cycleSize = cycle->getMaxOptionSize();

	cycle->setPos(50, 250);
	//cycle->setBorder(5, 5);
	//cycle->setSize((float) cycleSize.width + 10, (float) cycleSize.height + 10);

	cycle->onClicked([](Widget *w, Platform::Event &ev) -> void { (void) (ev); auto opt = ((CycleButton*) (w))->getCurrOption(); std::cout << "cycle clicked: " << opt << "\n"; });
}

void testSliderButton(WidgetGroup &group)
{
	// Randomly floating sliderButton, for testing.
	SliderButton *sliderButton = new SliderButton(50, 300);
	Canvas::instance().addWidgetMemory(sliderButton);
	group.addWidget(sliderButton);
	setDefaultColors(sliderButton);

	// Should be able to set size or pos first, hopefully dosn't matter.
	sliderButton->setRealPos(50, 50);
	//sliderButton->setBorder(5, 5);
	sliderButton->setSize(20, 20);
}

void testSlider(WidgetGroup &group)
{
	// TODO: Make a general slider widget to simplify this.
	// Testing a slider with a draggable sliderButton.
	Slider *slider = new Slider(125, 350, 200);
	Canvas::instance().addWidgetMemory(slider);
	slider->onValUpdate([](Widget *w, float val) -> void { (void) (w); std::cout << "slider val: " << val << "\n"; });
	slider->setVertical(true);
	group.addWidget(slider);
	setDefaultColors(slider);

	NineBox &sliderBoxButton = slider->getButton().getNineBox();
	auto col = Math::vec4(0.7, 0.0, 0.7, 0.8);
	sliderBoxButton.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.9, 0.0, 0.9, 0.8);
	sliderBoxButton.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	sliderBoxButton.setColors(col, col, col, col, col, col, col, col, col, 2);

	// Should be able to set size or pos first, hopefully dosn't matter.
	slider->setSize(20, 300);
	slider->setPos(300, 50);
	//slider->setBorder(5, 5);

	// Try changing the size and position of the slider now.
	slider->setPos(250, 100);
	slider->setSize(20, 450);
}

void testScrollbar(WidgetGroup &group)
{
	// TODO: Make a general scrollbar widget thingy to simplify all this.
	// Now throw in a scrollbar.
	Scrollbar *scrollbar = new Scrollbar(125, 350, 200);
	Canvas::instance().addWidgetMemory(scrollbar);
	scrollbar->onValUpdate([](Widget *w, float val) -> void { (void) (w); std::cout << "scrollbar val: " << val << "\n"; });
	scrollbar->setVertical(true);
	group.addWidget(scrollbar);
	setDefaultColors(scrollbar);

	NineBox &scrollBoxButton = scrollbar->getButton().getNineBox();
	auto col = Math::vec4(0.7, 0.0, 0.7, 0.8);
	scrollBoxButton.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.9, 0.0, 0.9, 0.8);
	scrollBoxButton.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	scrollBoxButton.setColors(col, col, col, col, col, col, col, col, col, 2);

	NineBox &scrollLeftBox = scrollbar->getLeftButton().getNineBox();
	col = Math::vec4(0.7, 0.0, 0.7, 0.8);
	scrollLeftBox.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.9, 0.0, 0.9, 0.8);
	scrollLeftBox.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	scrollLeftBox.setColors(col, col, col, col, col, col, col, col, col, 2);

	NineBox &scrollRightBox = scrollbar->getRightButton().getNineBox();
	col = Math::vec4(0.7, 0.0, 0.7, 0.8);
	scrollRightBox.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.9, 0.0, 0.9, 0.8);
	scrollRightBox.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	scrollRightBox.setColors(col, col, col, col, col, col, col, col, col, 2);

	// Should be able to set size or pos first, hopefully dosn't matter.
	scrollbar->setSize(20, 300);
	scrollbar->setPos(300, 50);
	//scrollbar->setBorder(5, 5);

	// Try changing the size and position of the scrollbar now.
	scrollbar->setPos(400, 100);
	scrollbar->setSize(20, 450);

	//scrollbar.setRange(0, 10);
	//scrollbar.updateButtonPos();
}

void testListBox(WidgetGroup &group)
{
	// Testing a basic list.
	ListBox *list = new ListBox();
	Canvas::instance().addWidgetMemory(list);
	group.addWidget(list);
	setDefaultColors(list);

	//button.setStr("i'm a test button");
	//Text &text2 = button.getText();
	//text2.setStr("i'm a test button");
	//text2.setColor({0, 0, 1});
	//text2.setFont(canvas.getFont("vera"));

	list->setPos(550, 50);
	//list->setBorder(5, 5);
	list->setSize(400, 200);

	for (int i = 0; i < 20; i++)
		list->addEntry("Entry number (extra text here for some reason) " + Utils::toString(i));
	//list.addEntry("Entry number one");
	//list.addEntry("Entry number two");

	// But what specific entry was clicked?
	list->onClicked([](Widget *w, Platform::Event &ev) -> void {(void) (ev); std::cout << "list clicked: " << ((ListBox*) w)->getSelectedEntry() << "\n"; });

	list->hide();
	list->unhide();
}

void testDropDownList(WidgetGroup &group)
{
	// Now testing a drop down list.
	DropDownList *dropList = new DropDownList();
	Canvas::instance().addWidgetMemory(dropList);
	group.addWidget(dropList);
	setDefaultColors(dropList);

	dropList->setStr("i'm a test drop list");

	NineBox &dropListBox = dropList->getList().getNineBox();
	auto col = Math::vec4(0.5, 0.0, 0.5, 0.8);
	dropListBox.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.8, 0.0, 0.8, 0.8);
	dropListBox.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	dropListBox.setColors(col, col, col, col, col, col, col, col, col, 2);

	dropList->setPos(550, 300);
	//dropList->setBorder(5, 5);
	//dropList->setSize((float) dropList->getTextWidth() + 10, (float) dropList->getTextHeight() + 10);

	for (int i = 0; i < 20; i++)
		dropList->addEntry("Entry number (extra text here for some reason) " + Utils::toString(i));

	// Worth adding some code to test the listbox selection of the dropdown?

	dropList->hide();
	dropList->unhide();
}

void testCheckboxes(WidgetGroup &group)
{
	// TODO: Need to decide what to do with this, may want an easier way of positioning the checkboxes, or just make it invisible.
	CheckboxGroup *checkGroup = new CheckboxGroup();
	Canvas::instance().addWidgetMemory(checkGroup);
	group.addWidget(checkGroup);
	setDefaultColors(checkGroup);

	// Group of checkboxes.
	float maxWidth = 0;
	for (int i = 0; i < 3; i++)
	{
		ToggleButton *toggle = new ToggleButton();
		Canvas::instance().addWidgetMemory(toggle);
		group.addWidget(toggle);
		setDefaultColors(toggle);
		setDefaultToggleColors(toggle);

		toggle->setStr("test checkbox " + Utils::toString(i));

		toggle->setPos(50, 350 + ((float) i * 40));
		//toggle->setBorder(5, 5);
		//toggle->setSize((float) toggle->getTextWidth() + 10, (float) toggle->getTextHeight() + 10);

		maxWidth = std::max(maxWidth, (float) toggle->getTextWidth() + 10);

		toggle->onClicked([i](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); std::cout << "checkbox " << i << " clicked!\n"; });

		checkGroup->addButton(toggle);
	}

	checkGroup->setPos(40, 340);
	//checkGroup->setBorder(5, 5);
	// TODO: This should be automatic!
	checkGroup->setSize(maxWidth + 20, 135);
}

void testRadioButtons(WidgetGroup &group)
{
	// TODO: Need to decide what to do with this, may want an easier way of positioning the checkboxes, or just make it invisible.
	RadioGroup *radioGroup = new RadioGroup();
	Canvas::instance().addWidgetMemory(radioGroup);
	group.addWidget(radioGroup);
	setDefaultColors(radioGroup);

	// Group of checkboxes.
	float maxWidth = 0;
	for (int i = 0; i < 3; i++)
	{
		ToggleButton *toggle = new ToggleButton();
		Canvas::instance().addWidgetMemory(toggle);
		group.addWidget(toggle);
		setDefaultColors(toggle);
		setDefaultToggleColors(toggle);

		toggle->setStr("test radio button " + Utils::toString(i));

		// TODO: These positions should really just be offsets from the top/left.
		toggle->setPos(50, 550 + ((float) i * 40));
		//toggle->setBorder(5, 5);
		//toggle->setSize((float) toggle->getTextWidth() + 10, (float) toggle->getTextHeight() + 10);

		maxWidth = std::max(maxWidth, (float) toggle->getTextWidth() + 10);

		toggle->onClicked([i](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); std::cout << "radio button " << i << " clicked!\n"; });

		radioGroup->addButton(toggle);
	}

	// TODO: Shouldn't this also update the position of all the radio buttons?
	radioGroup->setPos(40, 540);
	//radioGroup->setBorder(5, 5);
	radioGroup->setSize(maxWidth + 20, 135);
}

void testTextBox(WidgetGroup &group)
{
	// Now a text box.
	TextBox *textBox = new TextBox();
	Canvas::instance().addWidgetMemory(textBox);
	group.addWidget(textBox);
	setDefaultColors(textBox);

	textBox->setStr("Enter some text");

	// TODO: Needs to not resize itself when text changes.
	textBox->setPos(450, 400);
	//textBox->setBorder(5, 5);
	textBox->setSize(400, 400);
}

void testConsole(WidgetGroup &group)
{
	Console *console = new Console();
	Canvas::instance().addWidgetMemory(console);
	group.addWidget(console);
	setDefaultColors(console);
	setDefaultColors(&console->getInput());

	console->setPos(0, 0);
	console->setBorder(5, 5);
	console->setHeight(400);

	Canvas::instance().setConsole(console);

	// TODO: Need this to be on top of everything.
	// TODO: If we're visible, need to update size when the window size changes.
	// TODO: Apparently the tilde key for activating/deactivating the console also adds the character to the string.
}

void testComboBox(WidgetGroup &group)
{
	ComboBox *combo = new ComboBox();
	Canvas::instance().addWidgetMemory(combo);
	group.addWidget(combo);
	setDefaultColors(combo);
	setDefaultColors(&combo->getInput());

	combo->setPos(1000, 50);
	//combo->setBorder(5, 5);
	combo->setSize(400, 200);

	for (int i = 0; i < 10; i++)
		combo->addEntry("Combo number " + Utils::toString(i));
	for (int i = 0; i < 10; i++)
		combo->addEntry("More combo number " + Utils::toString(i));

	combo->onClicked([](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); std::cout << "combo clicked!\n"; });

	combo->hide();
	combo->unhide();
}

void testDropDownCombo(WidgetGroup &group)
{
	DropDownCombo *combo = new DropDownCombo();
	Canvas::instance().addWidgetMemory(combo);
	group.addWidget(combo);
	setDefaultColors(combo);
	setDefaultColors(&combo->getInput());
	setDefaultColors(&combo->getList());

	combo->setStr("i'm a test drop combo");

	combo->setPos(1500, 50);
	combo->setBorder(5, 5);
	combo->setSize(400, (float) combo->getTextHeight() + 10);

	for (int i = 0; i < 10; i++)
		combo->addEntry("Combo number " + Utils::toString(i));
	for (int i = 0; i < 10; i++)
		combo->addEntry("More combo number " + Utils::toString(i));

	combo->hide();
	combo->unhide();
}

void testButtonSpinner(WidgetGroup &group)
{
	SpinnerButton *spinner = new SpinnerButton();
	Canvas::instance().addWidgetMemory(spinner);
	group.addWidget(spinner);
	setDefaultColors(spinner);
	setDefaultColors(&spinner->getForward());
	setDefaultColors(&spinner->getBack());

	spinner->setOptions({ "Option 1", "Option 2", "Option 3", "Option fgfdgfd 4" });
	spinner->setStr(spinner->getCurrOption());
	auto spinnerSize = spinner->getMaxOptionSize();

	spinner->setPos(50, 250);
	spinner->setBorder(5, 5);
	spinner->setSize((float)spinnerSize.width + 60, (float)spinnerSize.height);
	spinner->setAlign(SpinnerButton::RIGHT_LEFT);

	spinner->hide();
	spinner->unhide();
}

void testPanel(WidgetGroup &group)
{
	DropPanel *panel = new DropPanel();
	Canvas::instance().addWidgetMemory(panel);
	group.addWidget(panel);
	setDefaultColors(panel);
	setDefaultColors(&panel->getPanel());

	panel->setStr("i'm a test panel");

	//panel->setPos();
	//panel->setBorder();
	//panel->setSize();

	// TODO: Try a whole bunch of different widget types to see how well they work.
	std::vector<Label*> tmpLabels;
	for (int i = 0; i < 10; i++)
	{
		Label *label = new Label();
		Canvas::instance().addWidgetMemory(label);
		setDefaultColors(label);
		label->setStr("test label " + Utils::toString(i));
		tmpLabels.push_back(label);

		label->setPos(0, 0);
		label->setBorder(5, 5);
		label->setSize((float) label->getTextWidth() + 10, (float) label->getTextHeight() + 10);

		group.addWidget(label);
	}

	// TODO: Needs to be some kind of min size for all this.
	// Layout: 3, 4, 2, 1.
	//GridLayout &layout = (GridLayout&) *panel->getLayout().getStrat();
	GridLayout &layout = panel->getLayout().gridLayout();
	layout.addRow().addColumns({4, 4, 4});
	layout.addRow().addColumns({3, 3, 3, 3});
	layout.addRow().addColumns({6, 6});
	layout.addRow().addColumns({12});

	// First row.
	layout.addWidget(tmpLabels[0], 0, 0, Column::CENTER);
	layout.addWidget(tmpLabels[1], 0, 1, Column::CENTER);
	layout.addWidget(tmpLabels[2], 0, 2, Column::CENTER);

	// Second row.
	layout.addWidget(tmpLabels[3], 1, 0, Column::CENTER);
	layout.addWidget(tmpLabels[4], 1, 1, Column::CENTER);
	layout.addWidget(tmpLabels[5], 1, 2, Column::CENTER);
	layout.addWidget(tmpLabels[6], 1, 3, Column::CENTER);

	// Third row.
	layout.addWidget(tmpLabels[7], 2, 0, Column::CENTER);
	layout.addWidget(tmpLabels[8], 2, 1, Column::CENTER);

	// Fourth row. Try a button now to make sure that works.
	Button *b = new Button;
	Canvas::instance().addWidgetMemory(b);
	setDefaultColors(b);
	b->setStr("i'm a test button");
	b->onClicked([](Widget* w, Platform::Event& ev) -> void { (void) (w); (void) (ev); std::cout << "button clicked!\n"; });

	// Needed for setting initial size I guess...
	b->setPos(0, 0);
	b->setBorder(5, 5);
	b->setSize((float) b->getTextWidth() + 10, (float) b->getTextHeight() + 10);

	group.addWidget(b); // Needed for event handling.
	//layout.addWidget(tmpLabels[9], 3, 0, Column::CENTER);
	layout.addWidget(b, 3, 0, Column::CENTER);

	panel->setPos(100, 100);
	panel->setSize((float) panel->getTextWidth() + 10, (float) panel->getTextHeight() + 10);
	panel->setPanelSize(1000, 1000);
	panel->setBorder(5, 5);
	panel->setPadding(5, 5);

	panel->hide();
	panel->unhide();
}

void testTabs(WidgetGroup &group)
{
	TabPanel *panel = new TabPanel();
	Canvas::instance().addWidgetMemory(panel);
	group.addWidget(panel);
	setDefaultColors(panel);
	//setDefaultColors(&panel->getPanel());

	//panel->setStr("i'm a test panel");

	for (int i = 0; i < 10; i++)
	{
		DropPanel &p = panel->addTab("test tab " + Utils::toString(i));
		setDefaultColors(&p);
		group.addWidget(&p);

		// Add some widgets to each tab.
		GridLayout &layout = p.getLayout().gridLayout();
		layout.addRow().addColumns({4, 4, 4});
		layout.addRow().addColumns({3, 3, 3, 3});
		layout.addRow().addColumns({6, 6});
		layout.addRow().addColumns({12});

		// TODO: Simplify this.
		std::vector<Label*> tmpLabels;
		for (int j = 0; j < 10; j++)
		{
			Label *label = new Label();
			Canvas::instance().addWidgetMemory(label);
			setDefaultColors(label);
			label->setStr(Utils::toString(i) + " : " + Utils::toString(j));
			tmpLabels.push_back(label);

			label->setPos(0, 0);
			label->setBorder(5, 5);
			label->setSize((float) label->getTextWidth() + 10, (float) label->getTextHeight() + 10);

			group.addWidget(label);
		}

		// First row.
		layout.addWidget(tmpLabels[0], 0, 0, Column::CENTER);
		layout.addWidget(tmpLabels[1], 0, 1, Column::CENTER);
		layout.addWidget(tmpLabels[2], 0, 2, Column::CENTER);

		// Second row.
		layout.addWidget(tmpLabels[3], 1, 0, Column::CENTER);
		layout.addWidget(tmpLabels[4], 1, 1, Column::CENTER);
		layout.addWidget(tmpLabels[5], 1, 2, Column::CENTER);
		layout.addWidget(tmpLabels[6], 1, 3, Column::CENTER);

		// Third row.
		layout.addWidget(tmpLabels[7], 2, 0, Column::CENTER);
		layout.addWidget(tmpLabels[8], 2, 1, Column::CENTER);

		// Fourth row.
		layout.addWidget(tmpLabels[9], 3, 0, Column::CENTER);
	}

	panel->setPos(100, 100);
	panel->setSize(800, 800);
	panel->setBorder(5, 5);
	panel->setPadding(5, 5);

	panel->hide();
	panel->unhide();
}

void testDialogBox(WidgetGroup &group)
{
	DialogBox *dialog = new DialogBox();
	Canvas::instance().addWidgetMemory(dialog);
	group.addWidget(dialog);
	setDefaultColors(dialog);

	dialog->setTitleStr("title here");
	dialog->setInfoStr("info here");

	setDefaultColors(&dialog->getTitle());
	setDefaultColors(&dialog->getInfo());
#if 0
	float maxWidth = 0;
	Button* ok = new Button();
	Canvas::instance().addWidgetMemory(ok);
	group.addWidget(ok);
	setDefaultColors(ok);

	ok->setStr("ok");

	ok->setPos(50, 550 + ((float) i * 40));
	//toggle->setBorder(5, 5);
	//toggle->setSize((float) toggle->getTextWidth() + 10, (float) toggle->getTextHeight() + 10);

	maxWidth = std::max(maxWidth, (float)toggle->getTextWidth() + 10);

	toggle->onClicked([i](Widget* w, Platform::Event& ev) -> void { (void)(w); (void)(ev); std::cout << "radio button " << i << " clicked!\n"; });

	radioGroup->addButton(toggle);

	radioGroup->setPos(40, 540);
	//radioGroup->setBorder(5, 5);
	radioGroup->setSize(maxWidth + 20, 135);
#endif
#if 0
	// TODO: Dialog should ideally resize itself to fit all the buttons.
	// TODO: Probably need a way to specifically set the ok/no/cancel buttons with default behaviours.
	// TODO: Dialog needs to resize to accommodate the buttons.
	auto &ok = dialog->addButton("ok?");
	setDefaultColors(&ok); // Not a fan of constantly needing to do this.
	ok.onClicked([](Widget* w, Platform::Event& ev) -> void { (void) (w); (void) (ev); std::cout << "ok\n"; });
	//dialog->addButton("no!");
	//dialog->addButton("fin");
#endif
	dialog->setPos(100, 100);
	dialog->setBorder(5, 5);
	//dialog->setPadding(5, 5);

	dialog->hide();
	dialog->unhide();
}



int main(int argc, char **argv)
{
	UNUSED(argc);
	UNUSED(argv);

	std::atexit([]()-> void {
		Rendering::Renderer::instance().clear();
		Window::destroyWindow();
	});

	Window::createWindow(0, 0, 512, 512);
	Renderer::instance().init();
	Canvas::instance().init();

	Renderer::instance().addCamera("main", Camera(Camera::Type::PERSP));

	Profiler profiler;
	float fpsUpdate = 0;

	// The gui elements can be created directly in code...

	// Gui needs a font for text rendering.
	auto &canvas = Canvas::instance();
	canvas.addFont("vera", "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf", new Font("vera"));
	canvas.addFont("default", "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf", new Font("default")); // Need to have a default font.

	// Example of using the canvas and widget groups instead (necessary for event handling and hovering, not much difference otherwise).
	auto &group = canvas.getWidgetGroup("test");

	//testConsole(group);
#if 0
	testLabel(group);
	testButton(group);
	testCycleButton(group);
	testSliderButton(group);
	testSlider(group);
	testScrollbar(group);
	testListBox(group);
	testDropDownList(group);
	testCheckboxes(group);
	testRadioButtons(group);
	testTextBox(group);
	testComboBox(group);
	testDropDownCombo(group);
	testButtonSpinner(group);
	testPanel(group);
#endif
	testDialogBox(group);
	//testTabs(group);

	// TODO: Gui config/style can be done in code.
	// TODO: Or it can be loaded from files.
	// TODO: Alternatively the entire gui can be loaded from files.
	// TODO: Need a way of displaying stuff stored elsewhere or in a database.

	bool finished = false;
	while (!finished)
	{
		float dt = Window::getDeltaTime();
		Window::processEvents();

		for (auto &ev : EventHandler::events)
		{
			switch (ev.type)
			{
			case Event::Type::WINDOW_RESIZED:
				Platform::Window::setWindowSize(ev);
				break;

			case Event::Type::WINDOW_CLOSED:
				finished = true;
				break;

			case Event::Type::KEY_PRESSED:
				keyPress(ev);
				break;

			case Event::Type::KEY_DOWN:
				keyDown(ev);
				break;

			case Event::Type::KEY_UP:
				keyUp(ev);
				break;

			case Event::Type::MOUSE_DRAG:
				mouseDrag(ev);
				break;

			case Event::Type::MOUSE_PRESSED:
				mousePressed(ev);
				break;

			case Event::Type::MOUSE_DOWN:
				mouseDown(ev);
				break;

			case Event::Type::MOUSE_UP:
				mouseUp(ev);
				break;

			default:
				break;
			}
		}

		profiler.begin();

		profiler.start("update");
		update(dt);
		profiler.time("update");

		profiler.start("render");
		render();
		profiler.time("render");

		fpsUpdate += dt;
		if (fpsUpdate > 10)
		{
			profiler.print();
			fpsUpdate = 0;
		}

		Window::swapBuffers();
	}

	return EXIT_SUCCESS;
}

