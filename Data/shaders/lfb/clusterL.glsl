#ifndef CLUSTER_L_GLSL
#define CLUSTER_L_GLSL

#ifndef ATOMIC_ADD
	#define ATOMIC_ADD 0
#endif

#ifndef STORE_LIGHT_DATA
	#define STORE_LIGHT_DATA 0
#endif

#ifndef CLUSTERS
	#define CLUSTERS 1
#endif

struct ClusterIteratorL
{
	uint start;
	uint node;
	uint end;
};

#if STORE_LIGHT_DATA
	#define CLUSTER_DEC_L(name, dataType) \
		coherent buffer ClusterOffsets##name \
		{ \
			uint clusterOffsets##name[]; \
		}; \
		\
		coherent buffer ClusterPos##name \
		{ \
			vec4 clusterPos##name[]; \
		}; \
		\
		coherent buffer ClusterDir##name \
		{ \
			vec4 clusterDir##name[]; \
		}; \
		\
		coherent buffer ClusterData##name \
		{ \
			dataType clusterData##name[]; \
		}; \
		\
		ClusterIteratorL clusterIter##name; \
		uniform ivec2 clusterSize##name;
#else
	#define CLUSTER_DEC_L(name, dataType) \
		coherent buffer ClusterOffsets##name \
		{ \
			uint clusterOffsets##name[]; \
		}; \
		\
		coherent buffer ClusterData##name \
		{ \
			dataType clusterData##name[]; \
		}; \
		\
		ClusterIteratorL clusterIter##name; \
		uniform ivec2 clusterSize##name;
#endif

#define CLUSTER_ITER_BEGIN_L(name, cluster) clusterIter##name.start = cluster > 0 ? clusterOffsets##name[cluster - 1] : 0, clusterIter##name.end = clusterOffsets##name[cluster], clusterIter##name.node = 0
#define CLUSTER_ITER_INC_L(name) clusterIter##name.node++
#define CLUSTER_ITER_CHECK_L(name) clusterIter##name.node + clusterIter##name.start < clusterIter##name.end
#define CLUSTER_COUNT_AT_L(name, cluster) clusterOffsets##name[cluster] - ((cluster > 0) ? clusterOffsets##name[cluster - 1] : 0)

// May or may not need to be atomic, depending on what you're doing.
#if ATOMIC_ADD
	#define CLUSTER_INC_COUNT_L(name, fragIndex) \
		atomicAdd(clusterOffsets##name[fragIndex], 1);

	#if STORE_LIGHT_DATA
		#define CLUSTER_ADD_DATA_L(name, cluster, frag, pos, dir) \
			uint index = atomicAdd(clusterOffsets##name[cluster], 1); \
			clusterPos##name[index] = pos; \
			clusterDir##name[index] = dir; \
			clusterData##name[index] = frag;
	#else
		#define CLUSTER_ADD_DATA_L(name, cluster, frag) \
			uint index = atomicAdd(clusterOffsets##name[cluster], 1); \
			clusterData##name[index] = frag;
	#endif
#else
	#define CLUSTER_INC_COUNT_L(name, fragIndex) \
		clusterOffsets##name[fragIndex]++;

	#if STORE_LIGHT_DATA
		#define CLUSTER_ADD_DATA_L(name, cluster, frag, pos, dir) \
			uint index = clusterOffsets##name[cluster]++; \
			clusterPos##name[index] = pos; \
			clusterDir##name[index] = dir; \
			clusterData##name[index] = frag;
	#else
		#define CLUSTER_ADD_DATA_L(name, cluster, frag) \
			uint index = clusterOffsets##name[cluster]++; \
			clusterData##name[index] = frag;
	#endif
#endif

#define CLUSTER_GET_DATA_L(name) clusterData##name[clusterIter##name.node + clusterIter##name.start]

#if STORE_LIGHT_DATA
	#define CLUSTER_GET_POS_L(name) clusterPos##name[clusterIter##name.node + clusterIter##name.start]
	#define CLUSTER_GET_DIR_L(name) clusterDir##name[clusterIter##name.node + clusterIter##name.start]
#endif

#define CLUSTER_WRITE_DATA_L(name, frag) clusterData##name[clusterIter##name.node + clusterIter##name.start] = frag
#define CLUSTER_GET_SIZE_L(name) clusterSize##name

// Returns cluster for given depth and range (range based on maxEyeZ).
#define CLUSTER_GET_L(depth, maxEyeZ) max(0, min(CLUSTERS - 1, int(depth / (maxEyeZ / float(CLUSTERS - 1.0)))))

// Returns frag index for given cluster and given x/y grid coord.
// Depends if you want them arranged by columns or rows.
//#define CLUSTER_GET_INDEX_L(name, gridCoord, cluster) int(gridCoord.x) * CLUSTER_GET_SIZE(name).y * CLUSTERS + int(gridCoord.y) * CLUSTERS + cluster
#define CLUSTER_GET_INDEX_L(name, cluster, gridCoord) int(gridCoord.y) * CLUSTER_GET_SIZE(name).x * CLUSTERS + int(gridCoord.x) * CLUSTERS + cluster

#ifndef CLUSTER_DEC
	#define CLUSTER_DEC(name, dataType) CLUSTER_DEC_L(name, dataType)
	#define CLUSTER_ITER_BEGIN(name, cluster) CLUSTER_ITER_BEGIN_L(name, cluster)
	#define CLUSTER_ITER_INC(name) CLUSTER_ITER_INC_L(name)
	#define CLUSTER_ITER_CHECK(name) CLUSTER_ITER_CHECK_L(name)
	#define CLUSTER_COUNT_AT(name, cluster) CLUSTER_COUNT_AT_L(name, cluster)
	#define CLUSTER_INC_COUNT(name, cluster) CLUSTER_INC_COUNT_L(name, cluster)
	#if STORE_LIGHT_DATA
		#define CLUSTER_ADD_DATA(name, cluster, frag, pos, dir) CLUSTER_ADD_DATA_L(name, cluster, frag, pos, dir)
		#define CLUSTER_GET_POS(name) CLUSTER_GET_POS_L(name)
		#define CLUSTER_GET_DIR(name) CLUSTER_GET_DIR_L(name)
	#else
		#define CLUSTER_ADD_DATA(name, cluster, frag) CLUSTER_ADD_DATA_L(name, cluster, frag)
	#endif
	#define CLUSTER_GET_DATA(name) CLUSTER_GET_DATA_L(name)
	#define CLUSTER_WRITE_DATA(name, frag) CLUSTER_WRITE_DATA_L(name, frag)
	#define CLUSTER_GET_SIZE(name) CLUSTER_GET_SIZE_L(name)
	#define CLUSTER_GET(depth, maxEyeZ) CLUSTER_GET_L(depth, maxEyeZ)
	#define CLUSTER_GET_INDEX(name, cluster, gridCoord) CLUSTER_GET_INDEX_L(name, cluster, gridCoord)
#endif

#endif

