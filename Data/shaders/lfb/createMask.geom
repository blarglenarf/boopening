// This is from when I tried doing depth masking with conservative rasterization, it was sloooooooooow.

#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in EsVertexData
{
	flat vec3 esVert;
} VertexIn[3];

out VertexData
{
	flat vec3 v1;
	flat vec3 v2;
	flat vec3 v3;
} VertexOut;


void createVert(int i)
{
	VertexOut.v1 = VertexIn[0].esVert;
	VertexOut.v2 = VertexIn[1].esVert;
	VertexOut.v3 = VertexIn[2].esVert;
	gl_Position = gl_in[i].gl_Position;
	EmitVertex();
}


void main()
{
	createVert(0);
	createVert(1);
	createVert(2);
	EndPrimitive();
}

