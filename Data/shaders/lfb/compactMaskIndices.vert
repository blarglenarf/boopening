#version 430

#define CLUSTER_BLOCK_SIZE 32

readonly buffer Indices
{
	uint indices[];
};

buffer Offsets
{
	uint offsets[];
};

buffer CompactIndices
{
	uint compactIndices[];
};

void main()
{
	uint offset = offsets[gl_VertexID];;

	for (int i = gl_VertexID * CLUSTER_BLOCK_SIZE; i < (gl_VertexID + 1) * CLUSTER_BLOCK_SIZE; i++)
	{
		if (indices[i] == 0)
		{
			continue;
		}
		else
		{
			compactIndices[offset++] = indices[i];
		}
	}
}

