#ifndef LFB_PLL_GLSL
#define LFB_PLL_GLSL

// TODO: need an LFB_COUNT_AT macro for bma to work...
// TODO: make this work with different fragment sizes...
// TODO: make this work in general...

// NOTE: This doesn't work with shader storage buffer objects for some odd reason...
// TODO: Find out how to get this working with ssbo's.
#define STORAGE_BUFFERS 0
#define PAGE_SIZE 4

struct LFBIterator
{
	uint node;
	int offset;
	int i;
};

// Only supports 1, 2 or 4 floats per fragment.
#define FRAG_SIZE 2

#if FRAG_SIZE == 1
	#define FRAG_TYPE float
	#define FRAG_REMAIN ,0,0,0
#elif FRAG_SIZE == 2
	#define FRAG_TYPE vec2
	#define FRAG_REMAIN ,0,0
#elif FRAG_SIZE == 3
	#error SCREW YOU, YOU CAN'T HAVE A SIZE 3!!!!
#else
	#define FRAG_TYPE vec4
	#define FRAG_REMAIN 
#endif


#if STORAGE_BUFFERS
	uniform layout(binding = 0, offset = 0) atomic_uint pageCount;

	coherent buffer HeadPtrs
	{
		uint headPtrs[];
	};

	buffer NextPtrs
	{
		uint nextPtrs[];
	};

	coherent buffer Semaphores
	{
		uint semaphores[];
	};

	coherent buffer Counts
	{
		uint counts[];
	};

	buffer Data
	{
		FRAG_TYPE data[];
	};

	uniform ivec2 size;
	uniform uint fragAlloc;
	LFBIterator iter;

	void addFragToLFB(in FRAG_TYPE frag)
	{
		int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
		uint pageIndex = 0, currPage = 0, pageOffset = 0, canary = 0;

		uint locked = 1;
		bool done = false;
		while (!done && canary < 1000)
		{
			// Acquire semaphore.
			locked = atomicExchange(semaphores[pixel], 1);
			if (locked == 0)
			{
				memoryBarrier();

				currPage = headPtrs[pixel];
				pageIndex = atomicAdd(counts[pixel], 1);
				//pageIndex = counts[pixel];
				pageOffset = pageIndex % PAGE_SIZE;

				// Need to create a new page.
				if (pageOffset == 0)
				{
					uint nodeAlloc = atomicCounterIncrement(pageCount);
					if (nodeAlloc * PAGE_SIZE < fragAlloc)
					{
						nextPtrs[nodeAlloc] = currPage;
						headPtrs[pixel] = nodeAlloc;
						currPage = nodeAlloc;
					}
					else
					{
						currPage = 0;
					}
				}

				if (currPage > 0)
				{
					data[currPage * PAGE_SIZE + pageOffset] = frag;
					//counts[pixel] = pageIndex + 1;
				}

				memoryBarrier();

				semaphores[pixel] = 0;
				done = true;
			}

			canary++;
		}
	}

	void beginIterFragments(in uint pixel)
	{
		iter.node = headPtrs[pixel];
		iter.i = int(counts[pixel]) - 1;
		iter.offset = iter.i % PAGE_SIZE;
	}

	void iterFragments()
	{
		if (iter.offset == 0)
			iter.node = nextPtrs[iter.node];
		iter.i--;
		iter.offset = iter.i % PAGE_SIZE;
	}

	#define LFB_ITER_BEGIN(pixel) beginIterFragments(pixel)
	#define LFB_ITER_INC() iterFragments()
	#define LFB_ITER_CHECK() iter.i >= 0
	#define LFB_GET_DATA() data[iter.node * PAGE_SIZE + iter.offset]
#else
	uniform layout(binding = 0, offset = 0) atomic_uint pageCount;

	#define HEAD_TYPE layout(r32ui) coherent uimageBuffer
	#define NEXT_TYPE layout(r32ui) uimageBuffer
	#define SEM_TYPE layout(r32ui) coherent uimageBuffer
	#define COUNT_TYPE layout(r32ui) coherent uimageBuffer

	#if FRAG_SIZE == 1
	#define DATA_TYPE layout(r32f) imageBuffer
	#elif FRAG_SIZE == 2
	#define DATA_TYPE layout(rg32f) imageBuffer
	#else
	#define DATA_TYPE layout(rgba32f) imageBuffer
	#endif

	#define LFB_DEC(name) \
	\
	uniform HEAD_TYPE HeadPtrs##name; \
	uniform NEXT_TYPE NextPtrs##name; \
	uniform SEM_TYPE Semaphores##name; \
	uniform COUNT_TYPE Counts##name; \
	uniform DATA_TYPE Data##name; \
	\
	uniform ivec2 size##name; \
	uniform uint fragAlloc##name; \
	LFBIterator iter##name;

	void addFragToLFB(in ivec2 size, in FRAG_TYPE frag, in uint fragAlloc, in HEAD_TYPE HeadPtrs, in NEXT_TYPE NextPtrs, in SEM_TYPE Semaphores, in COUNT_TYPE Counts, in DATA_TYPE Data)
	{
		int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
		uint pageIndex = 0, currPage = 0, pageOffset = 0, canary = 0;

		uint locked = 1;
		bool done = false;
		while (!done && canary < 1000)
		{
			// Acquire semaphore.
			locked = imageAtomicExchange(Semaphores, pixel, 1U);
			if (locked == 0)
			{
				memoryBarrier();

				currPage = imageLoad(HeadPtrs, pixel).r;
				pageIndex = imageAtomicAdd(Counts, pixel, 1U);
				//pageIndex = imageLoad(Counts, pixel).r;
				pageOffset = pageIndex % PAGE_SIZE;

				// Need to create a new page.
				if (pageOffset == 0)
				{
					uint nodeAlloc = atomicCounterIncrement(pageCount);
					if (nodeAlloc * PAGE_SIZE < fragAlloc)
					{
						imageStore(NextPtrs, int(nodeAlloc), uvec4(currPage));
						imageStore(HeadPtrs, pixel, uvec4(nodeAlloc));
						currPage = nodeAlloc;
					}
					else
					{
						currPage = 0;
					}
				}

				if (currPage > 0)
				{
					imageStore(Data, int(currPage) * PAGE_SIZE + int(pageOffset), vec4(frag FRAG_REMAIN));
					//imageStore(Counts, pixel, uvec4(pageIndex + 1));
				}

				memoryBarrier();

				imageStore(Semaphores, pixel, uvec4(0U));
				done = true;
			}

			canary++;
		}
	}

	void beginIterFragments(in uint pixel, inout LFBIterator iter, in HEAD_TYPE HeadPtrs, in COUNT_TYPE Counts)
	{
		iter.node = imageLoad(HeadPtrs, int(pixel)).r;
		iter.i = int(imageLoad(Counts, int(pixel)).r) - 1;
		iter.offset = iter.i % PAGE_SIZE;
	}

	void iterFragments(inout LFBIterator iter, in NEXT_TYPE NextPtrs)
	{
		if (iter.offset == 0)
			iter.node = imageLoad(NextPtrs, int(iter.node)).r;
		iter.i--;
		iter.offset = iter.i % PAGE_SIZE;
	}

	#define LFB_ITER_BEGIN(name, pixel) beginIterFragments(pixel, iter##name, HeadPtrs##name, Counts##name)
	#define LFB_ITER_INC(name) iterFragments(iter##name, NextPtrs##name)
	#define LFB_ITER_CHECK(name) iter##name.i >= 0 && iter##name.node != 0
	#define LFB_GET_DATA(name) imageLoad(Data##name, int(iter##name.node) * PAGE_SIZE + iter##name.offset).rg
	#define LFB_ADD_DATA(name, frag) addFragToLFB(size##name, frag, fragAlloc##name, HeadPtrs##name, NextPtrs##name, Semaphores##name, Counts##name, Data##name)
	#define LFB_GET_SIZE(name) size##name
#endif
#endif

