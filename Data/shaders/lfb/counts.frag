#version 450

// I don't think this extension is supported on my card :(
//#extension GL_ARB_fragment_shader_interlock: enable

#define STORAGE_BUFFERS 0
#define INDEX_BY_TILE 0

#include "tiles.glsl"

#if STORAGE_BUFFERS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	uniform layout(r32ui) coherent uimageBuffer Offsets;
#endif

uniform ivec2 size;

void main()
{
#if INDEX_BY_TILE
	int pixel = tilesIndex(size, ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

#if STORAGE_BUFFERS
	atomicAdd(offsets[pixel], 1);
#else
	imageAtomicAdd(Offsets, pixel, 1U);
#endif
}

