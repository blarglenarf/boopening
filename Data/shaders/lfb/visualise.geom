#version 430

layout(points) in;

// Can output points, or quads (as two triangles).
layout(points, max_vertices = 128) out;


// There's not actually any data coming in.
#import "lfb"

#include "../utils.glsl"

LFB_DEC(LFB_NAME, vec2);

uniform mat4 origInvMvMatrix;
uniform mat4 origInvPMatrix;
uniform ivec4 origViewport;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;


in PixelData
{
	int pixelIdx;
} PixelIn[1];

out ColorData
{
	vec4 color;
} ColorOut;


void main()
{
	// TODO: Note that fragIndex is not actually the pixel index, that's different. Need a way of accessing this...

	//ivec2 lfbSize = LFB_GET_SIZE(lfb);
	ivec2 lfbSize = origViewport.zw;

	// TODO: If we've indexed by tile then this changes (for now just assume that we haven't).
	vec2 ssPos = vec2(PixelIn[0].pixelIdx % lfbSize.x, PixelIn[0].pixelIdx / lfbSize.x);

	// TODO: Have a version that also works with an interleaved array and linked list.
	// Access the fragment at the location in the array that matches fragIndex (this only works with a linearized array).
	int fragCount = 0;
	for (LFB_ITER_BEGIN(LFB_NAME, PixelIn[0].pixelIdx); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		vec2 frag = LFB_GET_DATA(LFB_NAME);
		vec4 color = floatToRGBA8(frag.x);
		float depth = -frag.y;

		// Get frag position back into its own object space.
		// FIXME: I'm pretty sure this calculation is wrong. Need to double check it.
		vec4 esPos = getEyeFromWindow(vec3(ssPos, depth), origViewport, origInvPMatrix);

		// FIXME: I'm pretty sure everything below is correct, but not 100% sure...
		vec4 osVert = origInvMvMatrix * esPos;

		// Then put it into clip space.
		vec4 esVert = mvMatrix * osVert;
		vec4 csVert = pMatrix * esVert;

		ColorOut.color = color;
		gl_Position = csVert;
		EmitVertex();
		EndPrimitive();

		fragCount++;
		if (fragCount >= 128)
			break;
	}
}

