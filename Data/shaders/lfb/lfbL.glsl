#ifndef LFB_L_GLSL
#define LFB_L_GLSL

// I'm really sorry to anyone who has to look at this :(

#define STORAGE_BUFFERS 1
#define REQUIRE_COUNTS 1

// So that data will be in the same order as the link list lfb.
#define LFB_REVERSE 0

struct LFBIteratorL
{
	uint start;
	uint node;
	uint end;
};

// Only supports 1, 2 or 4 values per fragment.


// FIXME: I'd really prefer not to define different macros for different fragment sizes...

// Note: These can't be passed as args to functions...
#if STORAGE_BUFFERS

	#define FRAG_TYPE_L_1 float
	#define FRAG_TYPE_L_2 vec2
	#define FRAG_TYPE_L_4 vec4

	#define LFB_DEC_L(name, fragType) \
		coherent buffer Offsets##name \
		{ \
			uint offsets##name[]; \
		}; \
		\
		coherent buffer Data##name \
		{ \
			fragType data##name[]; \
		}; \
		\
		LFBIteratorL iter##name; \
		uniform ivec2 size##name;

	#define LFB_DEC_L_1(name) LFB_DEC_L(name, FRAG_TYPE_L_1)
	#define LFB_DEC_L_2(name) LFB_DEC_L(name, FRAG_TYPE_L_2)
	#define LFB_DEC_L_4(name) LFB_DEC_L(name, FRAG_TYPE_L_4)

	#define LFB_ITER_BEGIN_L(name, pixel) iter##name.start = pixel > 0 ? offsets##name[pixel - 1] : 0, iter##name.end = offsets##name[pixel], iter##name.node = 0
	#define LFB_ITER_SET_L(name, pixel, loc) iter##name.start = pixel > 0 ? offsets##name[pixel - 1] : 0, iter##name.end = offsets##name[pixel], iter##name.node = loc
	#define LFB_ITER_INC_L(name) iter##name.node++
	#define LFB_ITER_CHECK_L(name) iter##name.node + iter##name.start < iter##name.end
	#define LFB_ITER_CHECK_MAX_L(name, loc) iter##name.node + iter##name.start < iter##name.end && iter##name.node < loc
	#define LFB_COUNT_AT_L(name, pixel) offsets##name[pixel] - ((pixel > 0) ? offsets##name[pixel - 1] : 0)
	#define LFB_OFFSET_AT_L(name, pixel) pixel > 0 ? offsets##name[pixel - 1] : 0

	#if LFB_REVERSE
		#define LFB_GET_DATA_L(name) data##name[iter##name.end - 1 - iter##name.node]
		#define LFB_GET_DATA_AT_L(name, offset) data##name[iter##name.end - 1 - offset]
		#define LFB_WRITE_DATA_L(name, frag) data##name[iter##name.end - 1 - iter##name.node] = frag
	#else
		#define LFB_GET_DATA_L(name) data##name[iter##name.node + iter##name.start]
		#define LFB_GET_DATA_AT_L(name, offset) data##name[iter##name.start + offset]
		#define LFB_WRITE_DATA_L(name, frag) data##name[iter##name.node + iter##name.start] = frag

		#define LFB_GET_DATA_REV_L(name) data##name[iter##name.end - 1 - iter##name.node]
	#endif

	#define LFB_GET_SIZE_L(name) size##name
	#define LFB_ADD_DATA_L(name, pixel, frag) \
		{ \
			uint index = atomicAdd(offsets##name[pixel], 1); \
			data##name[index] = frag; \
		}

#else

	// FIXME: This is all really outdated now, don't use it.
	#define OFFSETS_TYPE_L layout(r32ui) coherent uimageBuffer
	
	// float size.
	#define FRAG_TYPE_L_1 float
	#define DATA_TYPE_L_1 layout(r32f) imageBuffer

	#define LFB_DEC_L_1(name) \
		uniform OFFSETS_TYPE_L Offsets##name; \
		uniform DATA_TYPE_L_1 Data##name; \
		\
		LFBIteratorL iter##name; \
		uniform ivec2 size##name;

	void addFragToLFB_L_1(in ivec2 size, in int pixel, in FRAG_TYPE_L_1 frag, in OFFSETS_TYPE_L offsets, in DATA_TYPE_L_1 data)
	{
		uint index = imageAtomicAdd(offsets, pixel, 1U);
		imageStore(data, int(index), vec4(frag FRAG_REMAIN));
	}

	#define LFB_ITER_BEGIN_L_1(name, pixel) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = 0
	#define LFB_ITER_SET_L_1(name, pixel, loc) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = loc
	#define LFB_ITER_INC_L_1(name) iter##name.node++
	#define LFB_ITER_CHECK_L_1(name) iter##name.node + iter##name.start < iter##name.end
	#define LFB_ITER_CHECK_MAX_L_1(name, loc) iter##name.node + iter##name.start < iter##name.end && iter##name.node < loc
	#define LFB_COUNT_AT_L_1(name, pixel) imageLoad(Offsets##name, int(pixel)).r - ((pixel > 0) ? imageLoad(Offsets##name, int(pixel - 1)).r : 0)
	#define LFB_OFFSET_AT_L_1(name, pixel) imageLoad(Offsets##name, int(pixel).r)

	#if LFB_REVERSE
		#define LFB_GET_DATA_L_1(name) imageLoad(Data##name, int(iter##name.end - 1 - iter##name.node)).rg
		#define LFB_WRITE_DATA_L_1(name, frag) imageStore(Data##name, int(iter##name.end - 1 - iter##name.node), vec4(frag, 0, 0, 0))
	#else
		#define LFB_GET_DATA_L_1(name) imageLoad(Data##name, int(iter##name.node + iter##name.start)).rg
		#define LFB_WRITE_DATA_L_1(name, frag) imageStore(Data##name, int(iter##name.node + iter##name.start), vec4(frag, 0, 0, 0))
	#endif

	#define LFB_ADD_DATA_L_1(name, pixel, frag) addFragToLFB_L_1(size##name, pixel, frag, Offsets##name, Data##name)
	#define LFB_GET_SIZE_L_1(name) size##name

	// vec2 size.
	#define FRAG_TYPE_L_2 vec2
	#define DATA_TYPE_L_2 layout(rg32f) imageBuffer

	#define LFB_DEC_L_2(name) \
		uniform OFFSETS_TYPE_L Offsets##name; \
		uniform DATA_TYPE_L_2 Data##name; \
		\
		LFBIteratorL iter##name; \
		uniform ivec2 size##name;

	void addFragToLFB_L_2(in ivec2 size, in int pixel, in FRAG_TYPE_L_2 frag, in OFFSETS_TYPE_L offsets, in DATA_TYPE_L_2 data)
	{
		uint index = imageAtomicAdd(offsets, pixel, 1U);
		imageStore(data, int(index), vec4(frag FRAG_REMAIN));
	}

	#define LFB_ITER_BEGIN_L_2(name, pixel) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = 0
	#define LFB_ITER_SET_L_2(name, pixel, loc) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = loc
	#define LFB_ITER_INC_L_2(name) iter##name.node++
	#define LFB_ITER_CHECK_L_2(name) iter##name.node + iter##name.start < iter##name.end
	#define LFB_ITER_CHECK_MAX_L_2(name, loc) iter##name.node + iter##name.start < iter##name.end && iter##name.node < loc
	#define LFB_COUNT_AT_L_2(name, pixel) imageLoad(Offsets##name, int(pixel)).r - ((pixel > 0) ? imageLoad(Offsets##name, int(pixel - 1)).r : 0)
	#define LFB_OFFSET_AT_L_2(name, pixel) imageLoad(Offsets##name, int(pixel).r)

	#if LFB_REVERSE
		#define LFB_GET_DATA_L_2(name) imageLoad(Data##name, int(iter##name.end - 1 - iter##name.node)).rg
		#define LFB_WRITE_DATA_L_2(name, frag) imageStore(Data##name, int(iter##name.end - 1 - iter##name.node), vec4(frag, 0, 0))
	#else
		#define LFB_GET_DATA_L_2(name) imageLoad(Data##name, int(iter##name.node + iter##name.start)).rg
		#define LFB_WRITE_DATA_L_2(name, frag) imageStore(Data##name, int(iter##name.node + iter##name.start), vec4(frag, 0, 0))
	#endif

	#define LFB_ADD_DATA_L_2(name, pixel, frag) addFragToLFB_L_2(size##name, pixel, frag, Offsets##name, Data##name)
	#define LFB_GET_SIZE_L_2(name) size##name

	// vec4 size.
	#define FRAG_TYPE_L_4 vec4
	#define DATA_TYPE_L_4 layout(rgba32f) imageBuffer

	#define LFB_DEC_L_4(name) \
		uniform OFFSETS_TYPE_L Offsets##name; \
		uniform DATA_TYPE_L_4 Data##name; \
		\
		LFBIteratorL iter##name; \
		uniform ivec2 size##name;

	void addFragToLFB_L_4(in ivec2 size, in int pixel, in FRAG_TYPE_L_4 frag, in OFFSETS_TYPE_L offsets, in DATA_TYPE_L_4 data)
	{
		uint index = imageAtomicAdd(offsets, pixel, 1U);
		imageStore(data, int(index), vec4(frag FRAG_REMAIN));
	}

	#define LFB_ITER_BEGIN_L_4(name, pixel) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = 0
	#define LFB_ITER_SET_L_4(name, pixel, loc) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = loc
	#define LFB_ITER_INC_L_4(name) iter##name.node++
	#define LFB_ITER_CHECK_L_4(name) iter##name.node + iter##name.start < iter##name.end
	#define LFB_ITER_CHECK_MAX_L_4(name, loc) iter##name.node + iter##name.start < iter##name.end && iter##name.node < loc
	#define LFB_COUNT_AT_L_4(name, pixel) imageLoad(Offsets##name, int(pixel)).r - ((pixel > 0) ? imageLoad(Offsets##name, int(pixel - 1)).r : 0)
	#define LFB_OFFSET_AT_L_4(name, pixel) imageLoad(Offsets##name, int(pixel).r)

	#if LFB_REVERSE
		#define LFB_GET_DATA_L_4(name) imageLoad(Data##name, int(iter##name.end - 1 - iter##name.node)).rg
		#define LFB_WRITE_DATA_L_4(name, frag) imageStore(Data##name, int(iter##name.end - 1 - iter##name.node), vec4(frag))
	#else
		#define LFB_GET_DATA_L_4(name) imageLoad(Data##name, int(iter##name.node + iter##name.start)).rg
		#define LFB_WRITE_DATA_L_4(name, frag) imageStore(Data##name, int(iter##name.node + iter##name.start), vec4(frag))
	#endif

	#define LFB_ADD_DATA_L_4(name, pixel, frag) addFragToLFB_L_4(size##name, pixel, frag, Offsets##name, Data##name)
	#define LFB_GET_SIZE_L_4(name) size##name
#endif

// Defaults for different sizes.
#ifndef FRAG_TYPE_1
	#define FRAG_TYPE_1 FRAG_TYPE_L_1
	#define LFB_DEC_1(name) LFB_DEC_L_1(name)
#endif

#ifndef FRAG_TYPE_2
	#define FRAG_TYPE_2 FRAG_TYPE_L_2
	#define LFB_DEC_2(name) LFB_DEC_L_2(name)
#endif

#ifndef FRAG_TYPE_4
	#define FRAG_TYPE_4 FRAG_TYPE_L_4
	#define LFB_DEC_4(name) LFB_DEC_L_4(name)
#endif

// Use default macro names if none are explicitly specified.
#ifndef LFB_DEC
	#define LFB_DEC(name, fragType) LFB_DEC_L(name, fragType)
	#define LFB_ITER_BEGIN(name, pixel) LFB_ITER_BEGIN_L(name, pixel)
	#define LFB_ITER_SET(name, pixel, loc) LFB_ITER_SET_L(name, pixel, loc)
	#define LFB_ITER_INC(name) LFB_ITER_INC_L(name)
	#define LFB_ITER_CHECK(name) LFB_ITER_CHECK_L(name)
	#define LFB_ITER_CHECK_MAX(name, loc) LFB_ITER_CHECK_MAX_L(name, loc)
	#define LFB_COUNT_AT(name, pixel) LFB_COUNT_AT_L(name, pixel)
	#define LFB_OFFSET_AT(name, pixel) LFB_OFFSET_AT_L(name, pixel)
	#define LFB_GET_DATA(name) LFB_GET_DATA_L(name)
	#define LFB_GET_DATA_AT(name, offset) LFB_GET_DATA_AT_L(name, offset)
	#define LFB_GET_DATA_REV(name) LFB_GET_DATA_REV_L(name)
	#define LFB_WRITE_DATA(name, frag) LFB_WRITE_DATA_L(name, frag)
	#define LFB_ADD_DATA(name, pixel, frag) LFB_ADD_DATA_L(name, pixel, frag)
	#define LFB_GET_SIZE(name) LFB_GET_SIZE_L(name)
#endif

#endif

