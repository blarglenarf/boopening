#version 430

in ColorData
{
	vec4 color;
} ColorIn;

out vec4 fragColor;

void main()
{
	fragColor = ColorIn.color;
}

