#version 430

#define USE_DEPTH_TEXTURE 0

layout(location = 0) in vec4 vertex;

#if USE_DEPTH_TEXTURE
	void main()
	{
		gl_Position = vec4(vertex.xyz, 1.0);
	}
#else
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;

	out VertexData
	{
		vec3 esFrag;
	} VertexOut;

	void main()
	{
		vec4 osVert = vec4(vertex.xyz, 1.0);
		vec4 esVert = mvMatrix * osVert;
		vec4 csVert = pMatrix * esVert;

		VertexOut.esFrag = esVert.xyz;

		gl_Position = csVert;
	}
#endif

