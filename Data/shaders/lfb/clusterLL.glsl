#ifndef CLUSTER_LL_GLSL
#define CLUSTER_LL_GLSL

#ifndef CLUSTERS
	#define CLUSTERS 1
#endif

#ifndef USE_ATOMIC_COUNTER
	#define USE_ATOMIC_COUNTER 1
#endif

#ifndef STORE_LIGHT_DATA
	#define STORE_LIGHT_DATA 0
#endif

struct ClusterIteratorLL
{
	uint node;
};

// No extra atomic counters, since binding must be hard coded...
// Just in case the link list lfb is currently being used.
#if USE_ATOMIC_COUNTER
	uniform layout(binding = 0, offset = 0) atomic_uint clusterFragCount;
#endif


// Could potentially merge light ids and next ptrs into a single 32 bit unsigned int.
#if STORE_LIGHT_DATA
	#define CLUSTER_DEC_LL(name, dataType) \
		coherent buffer ClusterHeadPtrs##name \
		{ \
			uint clusterHeadPtrs##name[]; \
		}; \
		\
		coherent buffer ClusterNextPtrs##name \
		{ \
			uint clusterNextPtrs##name[]; \
		}; \
		\
		coherent buffer ClusterPos##name \
		{ \
			vec4 clusterPos##name[]; \
		}; \
		\
		coherent buffer ClusterDir##name \
		{ \
			vec4 clusterDir##name[]; \
		}; \
		\
		coherent buffer ClusterData##name \
		{ \
			dataType clusterData##name[]; \
		}; \
		\
		ClusterIteratorLL clusterIter##name; \
		uniform ivec2 clusterSize##name; \
		uniform uint clusterFragAlloc##name;
#else
	#define CLUSTER_DEC_LL(name, dataType) \
		coherent buffer ClusterHeadPtrs##name \
		{ \
			uint clusterHeadPtrs##name[]; \
		}; \
		\
		coherent buffer ClusterNextPtrs##name \
		{ \
			uint clusterNextPtrs##name[]; \
		}; \
		\
		coherent buffer ClusterData##name \
		{ \
			dataType clusterData##name[]; \
		}; \
		\
		ClusterIteratorLL clusterIter##name; \
		uniform ivec2 clusterSize##name; \
		uniform uint clusterFragAlloc##name;
#endif

#define CLUSTER_ITER_BEGIN_LL(name, cluster) clusterIter##name.node = clusterHeadPtrs##name[cluster]
#define CLUSTER_ITER_INC_LL(name) clusterIter##name.node = clusterNextPtrs##name[clusterIter##name.node]
#define CLUSTER_ITER_CHECK_LL(name) clusterIter##name.node != 0
//#define CLUSTER_COUNT_AT_L(name, cluster) clusterOffsets##name[cluster] - ((cluster > 0) ? clusterOffsets##name[cluster - 1] : 0)

#if USE_ATOMIC_COUNTER
	#if STORE_LIGHT_DATA
		#define CLUSTER_ADD_DATA_LL(name, cluster, frag, pos, dir) \
			uint currFrag = atomicCounterIncrement(clusterFragCount); \
			if (currFrag < clusterFragAlloc##name) \
			{ \
				uint currHead = atomicExchange(clusterHeadPtrs##name[cluster], currFrag); \
				clusterNextPtrs##name[currFrag] = currHead; \
				clusterPos##name[currFrag] = pos; \
				clusterDir##name[currFrag] = dir; \
				clusterData##name[currFrag] = frag; \
			}
	#else
		#define CLUSTER_ADD_DATA_LL(name, cluster, frag) \
			uint currFrag = atomicCounterIncrement(clusterFragCount); \
			if (currFrag < clusterFragAlloc##name) \
			{ \
				uint currHead = atomicExchange(clusterHeadPtrs##name[cluster], currFrag); \
				clusterNextPtrs##name[currFrag] = currHead; \
				clusterData##name[currFrag] = frag; \
			}
	#endif
#endif

#define CLUSTER_GET_DATA_LL(name) clusterData##name[clusterIter##name.node]

#if STORE_LIGHT_DATA
	#define CLUSTER_GET_POS_LL(name) clusterPos##name[clusterIter##name.node]
	#define CLUSTER_GET_DIR_LL(name) clusterDir##name[clusterIter##name.node]
#endif

#define CLUSTER_WRITE_DATA_LL(name, frag) clusterData##name[clusterIter##name.node] = frag
#define CLUSTER_GET_SIZE_LL(name) clusterSize##name

// Returns cluster for given depth and range (range based on maxEyeZ).
#define CLUSTER_GET_LL(depth, maxEyeZ) max(0, min(CLUSTERS - 1, int(depth / (maxEyeZ / float(CLUSTERS - 1.0)))))

// Returns frag index for given cluster and given x/y grid coord.
// Depends if you want them arranged by columns or rows.
//#define CLUSTER_GET_INDEX_LL(name, gridCoord, cluster) int(gridCoord.x) * CLUSTER_GET_SIZE(name).y * CLUSTERS + int(gridCoord.y) * CLUSTERS + cluster
#define CLUSTER_GET_INDEX_LL(name, cluster, gridCoord) int(gridCoord.y) * CLUSTER_GET_SIZE(name).x * CLUSTERS + int(gridCoord.x) * CLUSTERS + cluster

#ifndef CLUSTER_DEC
	#define CLUSTER_DEC(name, dataType) CLUSTER_DEC_LL(name, dataType)
	#define CLUSTER_ITER_BEGIN(name, cluster) CLUSTER_ITER_BEGIN_LL(name, cluster)
	#define CLUSTER_ITER_INC(name) CLUSTER_ITER_INC_LL(name)
	#define CLUSTER_ITER_CHECK(name) CLUSTER_ITER_CHECK_LL(name)
	#define CLUSTER_COUNT_AT(name, cluster) CLUSTER_COUNT_AT_LL(name, cluster)
	#define CLUSTER_INC_COUNT(name, cluster) CLUSTER_INC_COUNT_LL(name, cluster)
	#if STORE_LIGHT_DATA
		#define CLUSTER_ADD_DATA(name, cluster, frag, pos, dir) CLUSTER_ADD_DATA_LL(name, cluster, frag, pos, dir)
		#define CLUSTER_GET_POS(name) CLUSTER_GET_POS_LL(name)
		#define CLUSTER_GET_DIR(name) CLUSTER_GET_DIR_LL(name)
	#else
		#define CLUSTER_ADD_DATA(name, cluster, frag) CLUSTER_ADD_DATA_LL(name, cluster, frag)
	#endif
	#define CLUSTER_GET_DATA(name) CLUSTER_GET_DATA_LL(name)
	#define CLUSTER_WRITE_DATA(name, frag) CLUSTER_WRITE_DATA_LL(name, frag)
	#define CLUSTER_GET_SIZE(name) CLUSTER_GET_SIZE_LL(name)
	#define CLUSTER_GET(depth, maxEyeZ) CLUSTER_GET_LL(depth, maxEyeZ)
	#define CLUSTER_GET_INDEX(name, cluster, gridCoord) CLUSTER_GET_INDEX_LL(name, cluster, gridCoord)
#endif

#endif

