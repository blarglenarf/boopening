#version 450

coherent buffer Offsets
{
	uint offsets[];
};

void main()
{
	uint index = uint(gl_VertexID);
	uint next = index + 1;

	// Does this need to be done atomically? Is it even correct?
	uint prevOffset = ((index > 0) ? offsets[index - 1] : 0);
	uint currOffset = offsets[index];
	uint req = currOffset - prevOffset;
	offsets[index] = currOffset + req;

#if 0
	uint index = uint(gl_VertexID);
	index = (index + 1) * stepSize - 1;
	uint otherIndex = index - (stepSize >> 1);

	#if STORAGE_BUFFERS
		uint a = offsets[index];
		uint b = offsets[otherIndex];
		offsets[index] = a + b;

		if (pass == 1)
			offsets[otherIndex] = a;
	#else
		uint a = imageLoad(Offsets, int(index)).r;
		uint b = imageLoad(Offsets, int(otherIndex)).r;
		imageStore(Offsets, int(index), uvec4(a + b));

		if (pass == 1)
			imageStore(Offsets, int(otherIndex), uvec4(a));
	#endif
#endif
}

