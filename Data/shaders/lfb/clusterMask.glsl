#ifndef CLUSTER_MASK_GLSL
#define CLUSTER_MASK_GLSL

#define PI 3.1415926

// Following code is from "Clustered Shading" by Kevin Ortegren: http://www.kevinortegren.com/portfolio/clustered-shading-assigning-arbitrarily-shaped-convex-light-volumes-using-conservative-rasterization/
bool linesegmentVsPlane(vec3 p0, vec3 p1, vec3 pn, out float lerpVal)
{
	vec3 u = p1 - p0;
	float D = dot(pn, u);
	float N = -dot(pn, p0);

	lerpVal = N / D;

	// Avoid NaN or INF.
	return !(lerpVal != clamp(lerpVal, 0.0, 1.0));
}

bool isInXslice(vec3 top, vec3 bottom, vec3 vert)
{
	return (top.y * vert.y + top.z * vert.z >= 0 && bottom.y * vert.y + bottom.z * vert.z >= 0);
}

bool isInYslice(vec3 left, vec3 right, vec3 vert)
{
	return (left.x * vert.x + left.z * vert.z >= 0 && right.x * vert.x + right.z * vert.z >= 0);
}

bool rayVsTriangle(vec3 ray, vec3 v0, vec3 v1, vec3 v2, out float z)
{
	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;
	vec3 q = cross(ray, e2);
	float a = dot(e1, q);

	if(a > -0.000001f && a < 0.000001f)
		return false;

	float f = 1.0f / a;
	float u = f * dot(-v0, q);

	if(u != clamp(u, 0.0, 1.0))
		return false;

	vec3 r = cross(-v0, e1);
	float v = f * dot(ray, r);

	if(v < 0.0f || (u + v) > 1.0f)
		return false;

	z = f * dot(e2, r) * ray.z;

	return true;
}

vec2 minMax(vec2 depthMinMax, in float depth)
{
	depthMinMax.x = min(depthMinMax.x, depth);
	depthMinMax.y = max(depthMinMax.y, depth);

	return depthMinMax;
}

vec2 cornerDepth(vec3 top, vec3 bottom, vec3 left, vec3 right, vec3 v0, vec3 v1, vec3 v2)
{
	// Case where the min/max depth is one of the corners of the tile.
	// Cross product of plane normals return the ray direction through the corner. All planes and rays go through (0,0,0).
	float depth = 0.0;
	vec2 depthMinMax = vec2(10000.0, -10000.0);
	depthMinMax = rayVsTriangle(cross(top, left), v0, v1, v2, depth) ? minMax(depthMinMax, depth) : depthMinMax;
	depthMinMax = rayVsTriangle(cross(top, right), v0, v1, v2, depth) ? minMax(depthMinMax, depth) : depthMinMax;
	depthMinMax = rayVsTriangle(cross(right, bottom), v0, v1, v2, depth) ? minMax(depthMinMax, depth) : depthMinMax;
	depthMinMax = rayVsTriangle(cross(bottom, left), v0, v1, v2, depth) ? minMax(depthMinMax, depth) : depthMinMax;
	return depthMinMax;
}

vec2 vertexDepth(vec3 top, vec3 bottom, vec3 left, vec3 right, vec3 v0, vec3 v1, vec3 v2)
{
	// Case where a vertex is the min/max depth of the tile //Check if vertex is inside all four planes.
	vec2 depthMinMax = vec2(10000.0, -10000.0);
	depthMinMax = (isInXslice(top, bottom, v0) && isInYslice(left, right, v0)) ? minMax(depthMinMax, v0.z) : depthMinMax;
	depthMinMax = (isInXslice(top, bottom, v1) && isInYslice(left, right, v1)) ? minMax(depthMinMax, v1.z) : depthMinMax;
	depthMinMax = (isInXslice(top, bottom, v2) && isInYslice(left, right, v2)) ? minMax(depthMinMax, v2.z) : depthMinMax;
	return depthMinMax;
}

vec2 edgeDepth(vec3 top, vec3 bottom, vec3 left, vec3 right, vec3 v0, vec3 v1, vec3 v2)
{
	// Case where a vertex edge intersects a tile side is the min/max depth.
	float lerpVal = 0.0;
	vec2 depthMinMax = vec2(10000.0, -10000.0);

	// Left side.
	depthMinMax = isInXslice(top, bottom, linesegmentVsPlane(v0, v1, left, lerpVal) ? mix(v0, v1, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v0.z, v1.z, lerpVal)) : depthMinMax;
	depthMinMax = isInXslice(top, bottom, linesegmentVsPlane(v1, v2, left, lerpVal) ? mix(v1, v2, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v1.z, v2.z, lerpVal)) : depthMinMax;
	depthMinMax = isInXslice(top, bottom, linesegmentVsPlane(v2, v0, left, lerpVal) ? mix(v2, v0, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v2.z, v0.z, lerpVal)) : depthMinMax;

	// Right side.
	depthMinMax = isInXslice(top, bottom, linesegmentVsPlane(v0, v1, right, lerpVal) ? mix(v0, v1, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v0.z, v1.z, lerpVal)) : depthMinMax;
	depthMinMax = isInXslice(top, bottom, linesegmentVsPlane(v1, v2, right, lerpVal) ? mix(v1, v2, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v1.z, v2.z, lerpVal)) : depthMinMax;
	depthMinMax = isInXslice(top, bottom, linesegmentVsPlane(v2, v0, right, lerpVal) ? mix(v2, v0, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v2.z, v0.z, lerpVal)) : depthMinMax;

	// Bottom side.
	depthMinMax = isInYslice(left, right, linesegmentVsPlane(v0, v1, bottom, lerpVal) ? mix(v0, v1, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v0.z, v1.z, lerpVal)) : depthMinMax;
	depthMinMax = isInYslice(left, right, linesegmentVsPlane(v1, v2, bottom, lerpVal) ? mix(v1, v2, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v1.z, v2.z, lerpVal)) : depthMinMax;
	depthMinMax = isInYslice(left, right, linesegmentVsPlane(v2, v0, bottom, lerpVal) ? mix(v2, v0, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v2.z, v0.z, lerpVal)) : depthMinMax;

	// Top side.
	depthMinMax = isInYslice(left, right, linesegmentVsPlane(v0, v1, top, lerpVal) ? mix(v0, v1, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v0.z, v1.z, lerpVal)) : depthMinMax;
	depthMinMax = isInYslice(left, right, linesegmentVsPlane(v1, v2, top, lerpVal) ? mix(v1, v2, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v1.z, v2.z, lerpVal)) : depthMinMax;
	depthMinMax = isInYslice(left, right, linesegmentVsPlane(v2, v0, top, lerpVal) ? mix(v2, v0, lerpVal) : vec3(0, 0, -1)) ? minMax(depthMinMax, mix(v2.z, v0.z, lerpVal)) : depthMinMax;

	return depthMinMax;
}

vec4 computePlane(vec3 p0, vec3 p1, vec3 p2)
{
#if 1
	vec3 v0 = p1 - p0;
	vec3 v2 = p2 - p0;
#else
	vec3 v0 = p0 - p1;
	vec3 v2 = p0 - p2;
#endif
	vec3 n = cross(v0, v2);
	float l = length(n);

	vec4 plane = vec4(n.x, n.y, n.z, 0);
	plane /= l;

	// Compute the distance to the origin using p0.
	plane.w = 0;

	return plane;
}

vec2 triangleClusterIntersection(vec2 tileSize, ivec4 viewport, mat4 invPMatrix, vec2 fragCoord, vec3 v0, vec3 v1, vec3 v2)
{
	// Create frustum planes for full res eye space.
	float x = fragCoord.x;
	float y = fragCoord.y;

	vec4 ssPos[4];
	ssPos[0] = vec4(x * tileSize.x, y * tileSize.y, -1, 1);
	ssPos[1] = vec4((x + 1) * tileSize.x, y * tileSize.y, -1, 1);
	ssPos[2] = vec4(x * tileSize.x, (y + 1) * tileSize.y, -1, 1);
	ssPos[3] = vec4((x + 1) * tileSize.x, (y + 1) * tileSize.y, -1, 1);

	vec3 esPos[4];
	for (int i = 0; i < 4; i++)
		esPos[i] = getEyeFromWindow(ssPos[i].xyz, viewport, invPMatrix).xyz;

	// Frustum planes are from eye space points, planes are left, right, top bottom.
	vec3 eyePos = vec3(0.0);
	vec3 left = computePlane(eyePos, esPos[2], esPos[0]).xyz;
	vec3 right = computePlane(eyePos, esPos[1], esPos[3]).xyz;
	vec3 top = computePlane(eyePos, esPos[0], esPos[1]).xyz;
	vec3 bottom = computePlane(eyePos, esPos[3], esPos[2]).xyz;

	vec2 depth = vec2(0.0);
	vec2 d0 = cornerDepth(top, bottom, left, right, v0, v1, v2);
	vec2 d1 = vertexDepth(top, bottom, left, right, v0, v1, v2);
	vec2 d2 = edgeDepth(top, bottom, left, right, v0, v1, v2);

	// Depth values are negative.
	depth.x = -max(max(d0.y, d1.y), d2.y);
	depth.y = -min(min(d0.x, d1.x), d2.x);

	return depth;
}
// End Clustered Shading code.


#endif

