#version 430

#define CLUSTER_BLOCK_SIZE 32

buffer Indices
{
	uint indices[];
};

buffer Offsets
{
	uint offsets[];
};

void main()
{
	uint count = 0;
	for (int i = gl_VertexID * CLUSTER_BLOCK_SIZE; i < (gl_VertexID + 1) * CLUSTER_BLOCK_SIZE; i++)
		if (indices[i] != 0)
			count++;
	offsets[gl_VertexID] = count;
}

