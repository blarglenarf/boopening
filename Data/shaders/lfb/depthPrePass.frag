#version 430

in VertexData
{
	float depth;
} VertexIn;

out vec4 fragColor;


void main()
{
	fragColor = vec4(VertexIn.depth, 0.0, 0.0, 0.0);
}

