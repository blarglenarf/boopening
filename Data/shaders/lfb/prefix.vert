#version 450

#define STORAGE_BUFFERS 0

uniform uint stepSize;
uniform int pass;

#if STORAGE_BUFFERS
buffer Offsets
{
	uint offsets[];
};
#else
uniform layout(r32ui) coherent uimageBuffer Offsets;
#endif

void main()
{
	uint index = uint(gl_VertexID);
	index = (index + 1) * stepSize - 1;
	uint otherIndex = index - (stepSize >> 1);

#if STORAGE_BUFFERS
	uint a = offsets[index];
	uint b = offsets[otherIndex];
	offsets[index] = a + b;

	if (pass == 1)
		offsets[otherIndex] = a;
#else
	uint a = imageLoad(Offsets, int(index)).r;
	uint b = imageLoad(Offsets, int(otherIndex)).r;
	imageStore(Offsets, int(index), uvec4(a + b));

	if (pass == 1)
		imageStore(Offsets, int(otherIndex), uvec4(a));
#endif
}

