#ifndef CLUSTER_CL_GLSL
#define CLUSTER_CL_GLSL

#define REQUIRE_COUNTS 1

#define GROUP_SIZE 32

#define USE_BLOCKS 0
#define BLOCK_SIZE 4

#ifndef ATOMIC_ADD
	#define ATOMIC_ADD 0
#endif

#ifndef STORE_LIGHT_DATA
	#define STORE_LIGHT_DATA 0
#endif

#ifndef CLUSTERS
	#define CLUSTERS 1
#endif

#if USE_BLOCKS
	struct ClusterIteratorCL
	{
		//uint start;
		uint run;
		uint groupOffset;
		uint extraOffset;
		uint minCount;
		uint node;
		uint end;
	};
#else
	struct ClusterIteratorCL
	{
		//uint start;
		uint interOffset;
		uint extraOffset;
		uint minCount;
		uint node;
		uint end;
	};
#endif


#if STORE_LIGHT_DATA
	#define CLUSTER_DEC_CL(name, dataType) \
		coherent buffer ClusterOffsets##name \
		{ \
			uint clusterOffsets##name[]; \
		}; \
		\
		coherent buffer ClusterCounts##name \
		{ \
			uint clusterCounts##name[]; \
		}; \
		\
		coherent buffer ClusterMinCounts##name \
		{ \
			uint clusterMinCounts##name[]; \
		}; \
		\
		coherent buffer ClusterPos##name \
		{ \
			vec4 clusterPos##name[]; \
		}; \
		\
		coherent buffer ClusterDir##name \
		{ \
			vec4 clusterDir##name[]; \
		}; \
		\
		coherent buffer ClusterData##name \
		{ \
			dataType clusterData##name[]; \
		}; \
		\
		ClusterIteratorCL clusterIter##name; \
		uniform ivec2 clusterSize##name;
#else
	#define CLUSTER_DEC_CL(name, dataType) \
		coherent buffer ClusterOffsets##name \
		{ \
			uint clusterOffsets##name[]; \
		}; \
		\
		coherent buffer ClusterCounts##name \
		{ \
			uint clusterCounts##name[]; \
		}; \
		\
		coherent buffer ClusterMinCounts##name \
		{ \
			uint clusterMinCounts##name[]; \
		}; \
		\
		coherent buffer ClusterData##name \
		{ \
			dataType clusterData##name[]; \
		}; \
		\
		ClusterIteratorCL clusterIter##name; \
		uniform ivec2 clusterSize##name;
#endif

#if 0
#define CLUSTER_ITER_BEGIN_L(name, cluster) clusterIter##name.start = cluster > 0 ? clusterOffsets##name[cluster - 1] : 0, clusterIter##name.end = clusterOffsets##name[cluster], clusterIter##name.node = 0
#define CLUSTER_ITER_INC_L(name) clusterIter##name.node++
#define CLUSTER_ITER_CHECK_L(name) clusterIter##name.node + clusterIter##name.start < clusterIter##name.end
#endif


#if USE_BLOCKS
	#define CLUSTER_ITER_BEGIN_CL(name, cluster) clusterIter##name.run = (cluster % GROUP_SIZE), clusterIter##name.groupOffset = clusterOffsets##name[(cluster / GROUP_SIZE) * GROUP_SIZE], clusterIter##name.extraOffset = clusterOffsets##name[cluster] - clusterMinCounts##name[cluster / GROUP_SIZE] * (cluster % GROUP_SIZE), clusterIter##name.minCount = clusterMinCounts##name[cluster / GROUP_SIZE], clusterIter##name.end = clusterCounts##name[cluster], clusterIter##name.node = 0
	#define CLUSTER_ITER_SET_CL(name, cluster, loc) clusterIter##name.run = (cluster % GROUP_SIZE), clusterIter##name.groupOffset = clusterOffsets##name[(cluster / GROUP_SIZE) * GROUP_SIZE], clusterIter##name.extraOffset = clusterOffsets##name[cluster] - clusterMinCounts##name[cluster / GROUP_SIZE] * (cluster % GROUP_SIZE), clusterIter##name.minCount = clusterMinCounts##name[cluster / GROUP_SIZE], clusterIter##name.end = clusterCounts##name[cluster], clusterIter##name.node = loc
#else
	#define CLUSTER_ITER_BEGIN_CL(name, cluster) clusterIter##name.interOffset = (cluster % GROUP_SIZE) + clusterOffsets##name[(cluster / GROUP_SIZE) * GROUP_SIZE], clusterIter##name.extraOffset = clusterOffsets##name[cluster] - clusterMinCounts##name[cluster / GROUP_SIZE] * (cluster % GROUP_SIZE), clusterIter##name.minCount = clusterMinCounts##name[cluster / GROUP_SIZE], clusterIter##name.end = clusterCounts##name[cluster], clusterIter##name.node = 0
	#define CLUSTER_ITER_SET_CL(name, cluster, loc) clusterIter##name.interOffset = (cluster % GROUP_SIZE) + clusterOffsets##name[(cluster / GROUP_SIZE) * GROUP_SIZE], clusterIter##name.extraOffset = clusterOffsets##name[cluster] - clusterMinCounts##name[cluster / GROUP_SIZE] * (cluster % GROUP_SIZE), clusterIter##name.minCount = clusterMinCounts##name[cluster / GROUP_SIZE], clusterIter##name.end = clusterCounts##name[cluster], clusterIter##name.node = loc
#endif
#define CLUSTER_ITER_INC_CL(name) clusterIter##name.node++
#define CLUSTER_ITER_CHECK_CL(name) clusterIter##name.node < clusterIter##name.end
#define CLUSTER_ITER_CHECK_MAX_CL(name, loc) clusterIter##name.node < clusterIter##name.end && clusterIter##name.node < loc
#define CLUSTER_COUNT_AT_CL(name, cluster) clusterCounts##name[cluster]


#if STORE_LIGHT_DATA
	#if USE_BLOCKS
		#define CLUSTER_GET_DATA_CL(name) clusterData##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.groupOffset + (clusterIter##name.node / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + clusterIter##name.run * BLOCK_SIZE + clusterIter##name.node % BLOCK_SIZE) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)]
		#define CLUSTER_GET_POS_CL(name) clusterPos##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.groupOffset + (clusterIter##name.node / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + clusterIter##name.run * BLOCK_SIZE + clusterIter##name.node % BLOCK_SIZE) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)]
		#define CLUSTER_GET_DIR_CL(name) clusterDir##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.groupOffset + (clusterIter##name.node / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + clusterIter##name.run * BLOCK_SIZE + clusterIter##name.node % BLOCK_SIZE) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)]
		#define CLUSTER_WRITE_DATA_CL(name, frag) clusterData##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.groupOffset + (clusterIter##name.node / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + clusterIter##name.run * BLOCK_SIZE + clusterIter##name.node % BLOCK_SIZE) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)] = frag
	#else
		#define CLUSTER_GET_DATA_CL(name) clusterData##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.interOffset + GROUP_SIZE * clusterIter##name.node) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)]
		#define CLUSTER_GET_POS_CL(name) clusterPos##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.interOffset + GROUP_SIZE * clusterIter##name.node) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)]
		#define CLUSTER_GET_DIR_CL(name) clusterDir##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.interOffset + GROUP_SIZE * clusterIter##name.node) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)]
		#define CLUSTER_WRITE_DATA_CL(name, frag) clusterData##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.interOffset + GROUP_SIZE * clusterIter##name.node) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)] = frag
	#endif
#else
	#if USE_BLOCKS
		#define CLUSTER_GET_DATA_CL(name) clusterData##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.groupOffset + (clusterIter##name.node / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + clusterIter##name.run * BLOCK_SIZE + clusterIter##name.node % BLOCK_SIZE) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)]
		#define CLUSTER_WRITE_DATA_CL(name, frag) clusterData##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.groupOffset + (clusterIter##name.node / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + clusterIter##name.run * BLOCK_SIZE + clusterIter##name.node % BLOCK_SIZE) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)] = frag
	#else
		#define CLUSTER_GET_DATA_CL(name) clusterData##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.interOffset + GROUP_SIZE * clusterIter##name.node) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)]
		#define CLUSTER_WRITE_DATA_CL(name, frag) clusterData##name[clusterIter##name.node < clusterIter##name.minCount ? (clusterIter##name.interOffset + GROUP_SIZE * clusterIter##name.node) : (clusterIter##name.minCount * GROUP_SIZE + clusterIter##name.extraOffset + clusterIter##name.node - clusterIter##name.minCount)] = frag
	#endif
#endif

#define CLUSTER_GET_SIZE_CL(name) clusterSize##name


// May or may not need to be atomic, depending on what you're doing.
#if USE_BLOCKS
	#if STORE_LIGHT_DATA
		#define CLUSTER_ADD_DATA_CL(name, cluster, frag, pos, dir) \
			{ \
				uint run = uint(cluster) % GROUP_SIZE; \
				uint group = uint(cluster) / GROUP_SIZE; \
				uint groupOffset = clusterOffsets##name[group * GROUP_SIZE]; \
				uint minCount = clusterMinCounts##name[group]; \
				uint currCount = atomicAdd(clusterCounts##name[cluster], 1); \
				if (currCount < minCount) \
				{ \
					uint loc = groupOffset + (currCount / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + run * BLOCK_SIZE + currCount % BLOCK_SIZE; \
					clusterPos##name[loc] = pos; \
					clusterDir##name[loc] = dir; \
					clusterData##name[loc] = frag; \
				} \
				else \
				{ \
					uint adjustedOffset = clusterOffsets##name[cluster] - minCount * run; \
					uint loc = minCount * GROUP_SIZE + adjustedOffset + (currCount - minCount); \
					clusterPos##name[loc] = pos; \
					clusterDir##name[loc] = dir; \
					clusterData##name[loc] = frag; \
				} \
			}
	#else
		#define CLUSTER_ADD_DATA_CL(name, cluster, frag) \
			{ \
				uint run = uint(cluster) % GROUP_SIZE; \
				uint group = uint(cluster) / GROUP_SIZE; \
				uint groupOffset = clusterOffsets##name[group * GROUP_SIZE]; \
				uint minCount = clusterMinCounts##name[group]; \
				uint currCount = atomicAdd(clusterCounts##name[cluster], 1); \
				if (currCount < minCount) \
					clusterData##name[groupOffset + (currCount / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + run * BLOCK_SIZE + currCount % BLOCK_SIZE] = frag; \
				else \
				{ \
					uint adjustedOffset = clusterOffsets##name[cluster] - minCount * run; \
					clusterData##name[minCount * GROUP_SIZE + adjustedOffset + (currCount - minCount)] = frag; \
				} \
			}
	#endif
#else
	#if STORE_LIGHT_DATA
		#define CLUSTER_ADD_DATA_CL(name, cluster, frag, pos, dir) \
			{ \
				uint run = uint(cluster) % GROUP_SIZE; \
				uint group = uint(cluster) / GROUP_SIZE; \
				uint groupOffset = clusterOffsets##name[group * GROUP_SIZE]; \
				uint minCount = clusterMinCounts##name[group]; \
				uint currCount = atomicAdd(clusterCounts##name[cluster], 1); \
				if (currCount < minCount) \
					clusterData##name[groupOffset + run + GROUP_SIZE * currCount] = frag; \
				else \
				{ \
					uint adjustedOffset = clusterOffsets##name[cluster] - minCount * run; \
					clusterData##name[minCount * GROUP_SIZE + adjustedOffset + (currCount - minCount)] = frag; \
				} \
			}
	#else
		#define CLUSTER_ADD_DATA_CL(name, cluster, frag) \
			{ \
				uint run = uint(cluster) % GROUP_SIZE; \
				uint group = uint(cluster) / GROUP_SIZE; \
				uint groupOffset = clusterOffsets##name[group * GROUP_SIZE]; \
				uint minCount = clusterMinCounts##name[group]; \
				uint currCount = atomicAdd(clusterCounts##name[cluster], 1); \
				if (currCount < minCount) \
					clusterData##name[groupOffset + run + GROUP_SIZE * currCount] = frag; \
				else \
				{ \
					uint adjustedOffset = clusterOffsets##name[cluster] - minCount * run; \
					clusterData##name[minCount * GROUP_SIZE + adjustedOffset + (currCount - minCount)] = frag; \
				} \
			}
	#endif
#endif

// Returns cluster for given depth and range (range based on maxEyeZ).
#define CLUSTER_GET_CL(depth, maxEyeZ) max(0, min(CLUSTERS - 1, int(depth / (maxEyeZ / float(CLUSTERS - 1.0)))))

// Returns frag index for given cluster and given x/y grid coord.
// Depends if you want them arranged by columns or rows.
//#define CLUSTER_GET_INDEX_L(name, gridCoord, cluster) int(gridCoord.x) * CLUSTER_GET_SIZE(name).y * CLUSTERS + int(gridCoord.y) * CLUSTERS + cluster
#define CLUSTER_GET_INDEX_CL(name, cluster, gridCoord) int(gridCoord.y) * CLUSTER_GET_SIZE(name).x * CLUSTERS + int(gridCoord.x) * CLUSTERS + cluster

#ifndef CLUSTER_DEC
	#define CLUSTER_DEC(name, dataType) CLUSTER_DEC_CL(name, dataType)
	#define CLUSTER_ITER_BEGIN(name, cluster) CLUSTER_ITER_BEGIN_CL(name, cluster)
	#define CLUSTER_ITER_SET(name, cluster, loc) CLUSTER_ITER_SET_CL(name, cluster, loc)
	#define CLUSTER_ITER_INC(name) CLUSTER_ITER_INC_CL(name)
	#define CLUSTER_ITER_CHECK(name) CLUSTER_ITER_CHECK_CL(name)
	#define CLUSTER_ITER_CHECK_MAX(name) CLUSTER_ITER_CHECK_MAX_CL(name)
	#define CLUSTER_COUNT_AT(name, cluster) CLUSTER_COUNT_AT_CL(name, cluster)
	#define CLUSTER_INC_COUNT(name, cluster) CLUSTER_INC_COUNT_CL(name, cluster)
	#define CLUSTER_GET_DATA(name) CLUSTER_GET_DATA_CL(name)
	#define CLUSTER_WRITE_DATA(name, frag) CLUSTER_WRITE_DATA_CL(name, frag)
	#if STORE_LIGHT_DATA
		#define CLUSTER_ADD_DATA(name, cluster, frag, pos, dir) CLUSTER_ADD_DATA_CL(name, cluster, frag, pos, dir)
		#define CLUSTER_GET_POS(name) CLUSTER_GET_POS_CL(name)
		#define CLUSTER_GET_DIR(name) CLUSTER_GET_DIR_CL(name)
	#else
		#define CLUSTER_ADD_DATA(name, cluster, frag) CLUSTER_ADD_DATA_CL(name, cluster, frag)
	#endif
	#define CLUSTER_GET_SIZE(name) CLUSTER_GET_SIZE_CL(name)
	#define CLUSTER_GET(depth, maxEyeZ) CLUSTER_GET_CL(depth, maxEyeZ)
	#define CLUSTER_GET_INDEX(name, cluster, gridCoord) CLUSTER_GET_INDEX_CL(name, cluster, gridCoord)
#endif

#endif

