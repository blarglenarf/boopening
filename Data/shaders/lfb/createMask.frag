#version 430

#define CLUSTERS 1
#define GRID_CELL_SIZE 1
#define MASK_SIZE 1
#define USE_DEPTH_TEXTURE 0
#define SAVE_INDICES 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

buffer ClusterMasks
{
	uint clusterMasks[];
};

#if 0
buffer ClusterMasksDepths
{
	uvec2 clusterMaskDepths[];
};
#endif

#if SAVE_INDICES
	buffer ClusterIndices
	{
		uint clusterIndices[];
	};
#endif

#if USE_DEPTH_TEXTURE
	uniform sampler2D depthTexture;
#else
	in VertexData
	{
		vec3 esFrag;
	} VertexIn;
#endif

uniform ivec2 size;

out vec4 fragColor;

void main()
{
	// If we're rendering at full-res, then divide by GRID_CELL_SIZE.
	vec2 gridCoord = gl_FragCoord.xy / vec2(GRID_CELL_SIZE);

	// Convert fragment depth into cluster space.
#if USE_DEPTH_TEXTURE
	float depth = texelFetch(depthTexture, ivec2(gl_FragCoord.xy), 0).x;
#else
	float depth = VertexIn.esFrag.z;
#endif
	int cluster = min(CLUSTERS - 1, int(-depth / (MAX_EYE_Z / float(CLUSTERS - 1.0))));

	int fragIndex = int(gridCoord.y) * size.x * MASK_SIZE + int(gridCoord.x) * MASK_SIZE;

	// Set mask for cluster to 1 (has to be done atomically), if it's already 1 don't bother.
	int maskIndex = cluster / 32;
	int clusterIndex = cluster % 32;

	uint clusterMask = 1 << clusterIndex;

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);

	if (clusterMask == 0)
	{
		discard;
		return;
	}

#if 0
	// Quantize the depth to between a min/max range (1024 in this case).
	int idx = int(gridCoord.y) * size.x * CLUSTERS + int(gridCoord.x) * CLUSTERS + cluster;

	float range = MAX_EYE_Z / float(CLUSTERS);
	float minClusterDepth = range * cluster;
	float maxClusterDepth = range * (cluster + 1);

	float d = ((-depth - minClusterDepth) * 1024.0) / range;
	uint quantizedDepth = uint(d);

	if (quantizedDepth < clusterMaskDepths[idx].x)
		atomicMin(clusterMaskDepths[idx].x, quantizedDepth);
	if (quantizedDepth > clusterMaskDepths[idx].y)
		atomicMax(clusterMaskDepths[idx].y, quantizedDepth);
#endif

	// Finally set the bitmask for the cluster. Possibly not necessary if min/max z is available.
	if ((clusterMasks[fragIndex + maskIndex] & clusterMask) == 0)
	{
		atomicOr(clusterMasks[fragIndex + maskIndex], clusterMask);

	#if SAVE_INDICES
		int c = int(gridCoord.y) * size.x * CLUSTERS + int(gridCoord.x) * CLUSTERS + cluster;
		clusterIndices[c] = c;
	#endif
	}

	discard;
}

