#ifndef LFB_LL_GLSL
#define LFB_LL_GLSL

#define STORAGE_BUFFERS 1
#define REQUIRE_COUNTS 1

struct LFBIteratorLL
{
	uint node;
};

// Only supports 1, 2 or 4 floats per fragment.

// FIXME: I'd really prefer not to define different macros for different fragment sizes...


#if STORAGE_BUFFERS

	// No extra atomic counters, since binding must be hard coded...
	uniform layout(binding = 0, offset = 0) atomic_uint fragCount;

	#define FRAG_TYPE_LL_1 float
	#define FRAG_TYPE_LL_2 vec2
	#define FRAG_TYPE_LL_4 vec4

	#if !REQUIRE_COUNTS
		#define LFB_DEC_LL(name, fragType) \
			coherent buffer HeadPtrs##name \
			{ \
				uint headPtrs##name[]; \
			}; \
			\
			coherent buffer NextPtrs##name \
			{ \
				uint nextPtrs##name[]; \
			}; \
			\
			coherent buffer Data##name \
			{ \
				fragType data##name[]; \
			}; \
			\
			LFBIteratorLL iter##name; \
			uniform ivec2 size##name; \
			uniform uint fragAlloc##name;

		#if 0
		uint getLfbDataLoc(uint offset)
		{
			uint startNode = iter##name.node;
			for (uint i = 0; i < offset; i++)
				iter##name.node = nextPtrs##name[iter##name.node];
			uint loc = iter##name.node;
			iter##name.node = startNode;
			return loc;
		}
		#endif
	#else
		#define LFB_DEC_LL(name, fragType) \
			coherent buffer Counts##name \
			{ \
				uint counts##name[]; \
			}; \
			\
			coherent buffer HeadPtrs##name \
			{ \
				uint headPtrs##name[]; \
			}; \
			\
			coherent buffer NextPtrs##name \
			{ \
				uint nextPtrs##name[]; \
			}; \
			\
			coherent buffer Data##name \
			{ \
				fragType data##name[]; \
			}; \
			\
			LFBIteratorLL iter##name; \
			uniform ivec2 size##name; \
			uniform uint fragAlloc##name;

		#if 0
		uint getLfbDataLoc(uint offset) \
		{ \
			uint startNode = iter##name.node; \
			for (uint i = 0; i < offset; i++) \
				iter##name.node = nextPtrs##name[iter##name.node]; \
			uint loc = iter##name.node; \
			iter##name.node = startNode; \
			return loc; \
		}
		#endif
	#endif

	#define LFB_DEC_LL_1(name) LFB_DEC_LL(name, FRAG_TYPE_LL_1)
	#define LFB_DEC_LL_2(name) LFB_DEC_LL(name, FRAG_TYPE_LL_2)
	#define LFB_DEC_LL_4(name) LFB_DEC_LL(name, FRAG_TYPE_LL_4)

	#define LFB_ITER_BEGIN_LL(name, pixel) iter##name.node = headPtrs##name[pixel]
	#define LFB_ITER_INC_LL(name) iter##name.node = nextPtrs##name[iter##name.node]
	#define LFB_ITER_SET_LL(name, pixel, loc) { LFB_ITER_BEGIN_LL(name, pixel); for (int i = 0; i < loc; i++) LFB_ITER_INC_LL(name); }
	#define LFB_ITER_CHECK_LL(name) iter##name.node != 0
	#define LFB_GET_DATA_LL(name) data##name[iter##name.node]
	//#define LFB_GET_DATA_AT_LL(name, offset) data##name[getDataLoc(offset)]
	#define LFB_GET_SIZE_LL(name) size##name
	#define LFB_WRITE_DATA_LL(name, frag) data##name[iter##name.node] = frag

	#if !REQUIRE_COUNTS
		#define LFB_ADD_DATA_LL(name, pixel, frag) \
			{ \
				uint currFrag = atomicCounterIncrement(fragCount); \
				if (currFrag < fragAlloc##name) \
				{ \
					uint currHead = atomicExchange(headPtrs##name[pixel], currFrag); \
					nextPtrs##name[currFrag] = currHead; \
					data##name[currFrag] = frag; \
				} \
			}
	#else
		#define LFB_ADD_DATA_LL(name, pixel, frag) \
			{ \
				uint currFrag = atomicCounterIncrement(fragCount); \
				if (currFrag < fragAlloc##name) \
				{ \
					uint currHead = atomicExchange(headPtrs##name[pixel], currFrag); \
					nextPtrs##name[currFrag] = currHead; \
					data##name[currFrag] = frag; \
					atomicAdd(counts##name[pixel], 1); \
				} \
			}
		#define LFB_COUNT_AT_LL(name, pixel) counts##name[pixel]
	#endif

#else

	// FIXME: This is all really outdated now, don't use it.
	// No extra atomic counters, since binding must be hard coded...
	uniform layout(binding = 0, offset = 0) atomic_uint fragCount;

	#define HEAD_TYPE_LL layout(r32ui) coherent uimageBuffer
	#define NEXT_TYPE_LL layout(r32ui) uimageBuffer

	#if REQUIRE_COUNTS
		#define COUNT_TYPE_LL layout(r32ui) coherent uimageBuffer
	#endif

	// float size.
	#define FRAG_TYPE_LL_1 float
	#define DATA_TYPE_LL_1 layout(r32f) imageBuffer
	#if !REQUIRE_COUNTS
		#define LFB_DEC_LL_1(name) \
			uniform HEAD_TYPE_LL HeadPtrs##name; \
			uniform NEXT_TYPE_LL NextPtrs##name; \
			uniform DATA_TYPE_LL_1 Data##name; \
			\
			LFBIteratorLL iter##name; \
			uniform ivec2 size##name; \
			uniform uint fragAlloc##name;
	#else
		#define LFB_DEC_LL_1(name) \
			uniform HEAD_TYPE_LL HeadPtrs##name; \
			uniform NEXT_TYPE_LL NextPtrs##name; \
			uniform DATA_TYPE_LL_1 Data##name; \
			uniform COUNT_TYPE_LL Counts##name; \
			\
			LFBIteratorLL iter##name; \
			uniform ivec2 size##name; \
			uniform uint fragAlloc##name;
	#endif

	#if !REQUIRE_COUNTS
		void addFragToLFB_LL_1(in ivec2 size, in int pixel, in FRAG_TYPE_LL_1 frag, in uint alloc, in HEAD_TYPE_LL HeadPtrs, in NEXT_TYPE_LL NextPtrs, in DATA_TYPE_LL Data)
		{
			uint currFrag = atomicCounterIncrement(fragCount);
			if (currFrag < alloc)
			{
				uint currHead = imageAtomicExchange(HeadPtrs, pixel, currFrag).r;

				imageStore(NextPtrs, int(currFrag), uvec4(currHead));
				imageStore(Data, int(currFrag), vec4(frag, 0, 0, 0));
			}
		}
	#else
		void addFragToLFB_LL_1(in ivec2 size, in int pixel, in FRAG_TYPE_LL_1 frag, in uint alloc, in HEAD_TYPE_LL HeadPtrs, in NEXT_TYPE_LL NextPtrs, in DATA_TYPE_LL Data, in COUNT_TYPE_LL Counts)
		{
			uint currFrag = atomicCounterIncrement(fragCount);
			if (currFrag < alloc)
			{
				uint currHead = imageAtomicExchange(HeadPtrs, pixel, currFrag).r;

				imageStore(NextPtrs, int(currFrag), uvec4(currHead));
				imageStore(Data, int(currFrag), vec4(frag, 0, 0, 0));
				imageAtomicAdd(Counts, pixel, 1U);
			}
		}
	#endif

	#define LFB_ITER_BEGIN_LL_1(name, pixel) iter##name.node = imageLoad(HeadPtrs##name, int(pixel)).r
	#define LFB_ITER_INC_LL_1(name) iter##name.node = imageLoad(NextPtrs##name, int(iter##name.node)).r
	#define LFB_ITER_SET_LL_1(name, pixel, loc) { LFB_ITER_BEGIN_LL_1(name, pixel); for (int i = 0; i < loc; i++) LFB_ITER_INC_LL_1(name); }
	#define LFB_ITER_CHECK_LL_1(name) iter##name.node != 0
	#define LFB_GET_DATA_LL_1(name) imageLoad(Data##name, int(iter##name.node)).rg
	#define LFB_ADD_DATA_LL_1(name, pixel, frag) addFragToLFB_LL_1(size##name, pixel, frag, fragAlloc##name, HeadPtrs##name, NextPtrs##name, Data##name)
	#define LFB_GET_SIZE_LL_1(name) size##name
	#define LFB_WRITE_DATA_LL_1(name, frag) imageStore(Data##name, int(iter##name.node), vec4(frag, 0, 0, 0))

	#if REQUIRE_COUNTS
		#define LFB_COUNT_AT_LL_1(name, pixel) imageLoad(Counts##name, int(pixel)).r
	#endif

	// vec2 size.
	#define FRAG_TYPE_LL_2 vec2
	#define DATA_TYPE_LL_2 layout(rg32f) imageBuffer
	#if !REQUIRE_COUNTS
		#define LFB_DEC_LL_2(name) \
			uniform HEAD_TYPE_LL HeadPtrs##name; \
			uniform NEXT_TYPE_LL NextPtrs##name; \
			uniform DATA_TYPE_LL_2 Data##name; \
			\
			LFBIteratorLL iter##name; \
			uniform ivec2 size##name; \
			uniform uint fragAlloc##name;
	#else
		#define LFB_DEC_LL_2(name) \
			uniform HEAD_TYPE_LL HeadPtrs##name; \
			uniform NEXT_TYPE_LL NextPtrs##name; \
			uniform DATA_TYPE_LL_2 Data##name; \
			uniform COUNT_TYPE_LL Counts##name; \
			\
			LFBIteratorLL iter##name; \
			uniform ivec2 size##name; \
			uniform uint fragAlloc##name;
	#endif

	#if !REQUIRE_COUNTS
		void addFragToLFB_LL_2(in ivec2 size, in int pixel, in FRAG_TYPE_LL_2 frag, in uint alloc, in HEAD_TYPE_LL HeadPtrs, in NEXT_TYPE_LL NextPtrs, in DATA_TYPE_LL Data)
		{
			uint currFrag = atomicCounterIncrement(fragCount);
			if (currFrag < alloc)
			{
				uint currHead = imageAtomicExchange(HeadPtrs, pixel, currFrag).r;

				imageStore(NextPtrs, int(currFrag), uvec4(currHead));
				imageStore(Data, int(currFrag), vec4(frag, 0, 0));
			}
		}
	#else
		void addFragToLFB_LL_2(in ivec2 size, in int pixel, in FRAG_TYPE_LL_2 frag, in uint alloc, in HEAD_TYPE_LL HeadPtrs, in NEXT_TYPE_LL NextPtrs, in DATA_TYPE_LL Data, in COUNT_TYPE_LL Counts)
		{
			uint currFrag = atomicCounterIncrement(fragCount);
			if (currFrag < alloc)
			{
				uint currHead = imageAtomicExchange(HeadPtrs, pixel, currFrag).r;

				imageStore(NextPtrs, int(currFrag), uvec4(currHead));
				imageStore(Data, int(currFrag), vec4(frag, 0, 0));
				imageAtomicAdd(Counts, pixel, 1U);
			}
		}
	#endif

	#define LFB_ITER_BEGIN_LL_2(name, pixel) iter##name.node = imageLoad(HeadPtrs##name, int(pixel)).r
	#define LFB_ITER_INC_LL_2(name) iter##name.node = imageLoad(NextPtrs##name, int(iter##name.node)).r
	#define LFB_ITER_SET_LL_2(name, pixel, loc) { LFB_ITER_BEGIN_LL_2(name, pixel); for (int i = 0; i < loc; i++) LFB_ITER_INC_LL_2(name); }
	#define LFB_ITER_CHECK_LL_2(name) iter##name.node != 0
	#define LFB_GET_DATA_LL_2(name) imageLoad(Data##name, int(iter##name.node)).rg
	#define LFB_ADD_DATA_LL_2(name, pixel, frag) addFragToLFB_LL_2(size##name, pixel, frag, fragAlloc##name, HeadPtrs##name, NextPtrs##name, Data##name)
	#define LFB_GET_SIZE_LL_2(name) size##name
	#define LFB_WRITE_DATA_LL_2(name, frag) imageStore(Data##name, int(iter##name.node), vec4(frag, 0, 0))

	#if REQUIRE_COUNTS
		#define LFB_COUNT_AT_LL_2(name, pixel) imageLoad(Counts##name, int(pixel)).r
	#endif

	// vec4 size.
	#define FRAG_TYPE_LL_4 vec4
	#define DATA_TYPE_LL_4 layout(rgba32f) imageBuffer
	#if !REQUIRE_COUNTS
		#define LFB_DEC_LL_4(name) \
			uniform HEAD_TYPE_LL HeadPtrs##name; \
			uniform NEXT_TYPE_LL NextPtrs##name; \
			uniform DATA_TYPE_LL_4 Data##name; \
			\
			LFBIteratorLL iter##name; \
			uniform ivec2 size##name; \
			uniform uint fragAlloc##name;
	#else
		#define LFB_DEC_LL_4(name) \
			uniform HEAD_TYPE_LL HeadPtrs##name; \
			uniform NEXT_TYPE_LL NextPtrs##name; \
			uniform DATA_TYPE_LL_4 Data##name; \
			uniform COUNT_TYPE_LL Counts##name; \
			\
			LFBIteratorLL iter##name; \
			uniform ivec2 size##name; \
			uniform uint fragAlloc##name;
	#endif

	#if !REQUIRE_COUNTS
		void addFragToLFB_LL_2(in ivec2 size, in int pixel, in FRAG_TYPE_LL_4 frag, in uint alloc, in HEAD_TYPE_LL HeadPtrs, in NEXT_TYPE_LL NextPtrs, in DATA_TYPE_LL Data)
		{
			uint currFrag = atomicCounterIncrement(fragCount);
			if (currFrag < alloc)
			{
				uint currHead = imageAtomicExchange(HeadPtrs, pixel, currFrag).r;

				imageStore(NextPtrs, int(currFrag), uvec4(currHead));
				imageStore(Data, int(currFrag), vec4(frag));
			}
		}
	#else
		void addFragToLFB_LL_2(in ivec2 size, in int pixel, in FRAG_TYPE_LL_4 frag, in uint alloc, in HEAD_TYPE_LL HeadPtrs, in NEXT_TYPE_LL NextPtrs, in DATA_TYPE_LL Data, in COUNT_TYPE_LL Counts)
		{
			uint currFrag = atomicCounterIncrement(fragCount);
			if (currFrag < alloc)
			{
				uint currHead = imageAtomicExchange(HeadPtrs, pixel, currFrag).r;

				imageStore(NextPtrs, int(currFrag), uvec4(currHead));
				imageStore(Data, int(currFrag), vec4(frag));
				imageAtomicAdd(Counts, pixel, 1U);
			}
		}
	#endif

	#define LFB_ITER_BEGIN_LL_4(name, pixel) iter##name.node = imageLoad(HeadPtrs##name, int(pixel)).r
	#define LFB_ITER_INC_LL_4(name) iter##name.node = imageLoad(NextPtrs##name, int(iter##name.node)).r
	#define LFB_ITER_SET_LL_4(name, pixel, loc) { LFB_ITER_BEGIN_LL_4(name, pixel); for (int i = 0; i < loc; i++) LFB_ITER_INC_LL_4(name); }
	#define LFB_ITER_CHECK_LL_4(name) iter##name.node != 0
	#define LFB_GET_DATA_LL_4(name) imageLoad(Data##name, int(iter##name.node)).rg
	#define LFB_ADD_DATA_LL_4(name, pixel, frag) addFragToLFB_LL_4(size##name, pixel, frag, fragAlloc##name, HeadPtrs##name, NextPtrs##name, Data##name)
	#define LFB_GET_SIZE_LL_4(name) size##name
	#define LFB_WRITE_DATA_LL_4(name, frag) imageStore(Data##name, int(iter##name.node), vec4(frag))

	#if REQUIRE_COUNTS
		#define LFB_COUNT_AT_LL_4(name, pixel) imageLoad(Counts##name, int(pixel)).r
	#endif
#endif

// Defaults for different sizes.
#ifndef FRAG_TYPE_1
	#define FRAG_TYPE_1 FRAG_TYPE_LL_1
	#define LFB_DEC_1(name) LFB_DEC_LL_1(name)
#endif

#ifndef FRAG_TYPE_2
	#define FRAG_TYPE_2 FRAG_TYPE_LL_2
	#define LFB_DEC_2(name) LFB_DEC_LL_2(name)
#endif

#ifndef FRAG_TYPE_4
	#define FRAG_TYPE_4 FRAG_TYPE_LL_4
	#define LFB_DEC_4(name) LFB_DEC_LL_4(name)
#endif

// Use default macro names if none are explicitly specified.
#ifndef LFB_DEC
	#define LFB_DEC(name, fragType) LFB_DEC_LL(name, fragType)
	#define LFB_ITER_BEGIN(name, pixel) LFB_ITER_BEGIN_LL(name, pixel)
	#define LFB_ITER_SET(name, pixel, loc) LFB_ITER_SET_LL(name, pixel, loc)
	#define LFB_ITER_INC(name) LFB_ITER_INC_LL(name)
	#define LFB_ITER_CHECK(name) LFB_ITER_CHECK_LL(name)
	//#define LFB_ITER_CHECK_MAX_LL(name, loc) LFB_ITER_CHECK_MAX_LL_2(name, loc)
	#define LFB_COUNT_AT(name, pixel) LFB_COUNT_AT_LL(name, pixel)
	#define LFB_GET_DATA(name) LFB_GET_DATA_LL(name)
	#define LFB_GET_DATA_AT(name, offset) LFB_GET_DATA_AT_LL(name, offset)
	#define LFB_WRITE_DATA(name, frag) LFB_WRITE_DATA_LL(name, frag)
	#define LFB_ADD_DATA(name, pixel, frag) LFB_ADD_DATA_LL(name, pixel, frag)
	#define LFB_GET_SIZE(name) LFB_GET_SIZE_LL(name)
#endif

#endif


