#ifndef LFB_CL_GLSL
#define LFB_CL_GLSL

// I'm really sorry to anyone who has to look at this :(

#define STORAGE_BUFFERS 1
#define REQUIRE_COUNTS 1

#define GROUP_SIZE 32

#define USE_BLOCKS 0
#define BLOCK_SIZE 4

// So that data will be in the same order as the link list lfb.
#define LFB_REVERSE 0

#if USE_BLOCKS
	struct LFBIteratorCL
	{
		uint run;
		uint groupOffset;
		uint extraOffset;
		uint minCount;
		uint node;
		uint end;
	};
#else
	struct LFBIteratorCL
	{
		uint interOffset;
		uint extraOffset;
		uint minCount;
		uint node;
		uint end;
	};
#endif

// Only supports 1, 2 or 4 floats per fragment.


// FIXME: I'd really prefer not to define different macros for different fragment sizes...

// Note: These can't be passed as args to functions...
#if STORAGE_BUFFERS

	#define FRAG_TYPE_CL_1 float
	#define FRAG_TYPE_CL_2 vec2
	#define FRAG_TYPE_CL_4 vec4

	#define LFB_DEC_CL(name, fragType) \
		coherent buffer Offsets##name \
		{ \
			uint offsets##name[]; \
		}; \
		\
		coherent buffer Counts##name \
		{\
			uint counts##name[];\
		};\
		\
		coherent buffer MinCounts##name \
		{ \
			uint minCounts##name[]; \
		}; \
		\
		coherent buffer Data##name \
		{ \
			fragType data##name[]; \
		}; \
		\
		LFBIteratorCL iter##name; \
		uniform ivec2 size##name;

	#define LFB_DEC_CL_1(name) LFB_DEC_CL(name, FRAG_TYPE_CL_1)
	#define LFB_DEC_CL_2(name) LFB_DEC_CL(name, FRAG_TYPE_CL_2)
	#define LFB_DEC_CL_4(name) LFB_DEC_CL(name, FRAG_TYPE_CL_4)

	#if USE_BLOCKS
		#define LFB_ITER_BEGIN_CL(name, pixel) iter##name.run = (pixel % GROUP_SIZE), iter##name.groupOffset = offsets##name[(pixel / GROUP_SIZE) * GROUP_SIZE], iter##name.extraOffset = offsets##name[pixel] - minCounts##name[pixel / GROUP_SIZE] * (pixel % GROUP_SIZE), iter##name.minCount = minCounts##name[pixel / GROUP_SIZE], iter##name.end = counts##name[pixel], iter##name.node = 0
		#define LFB_ITER_SET_CL(name, pixel, loc) iter##name.run = (pixel % GROUP_SIZE), iter##name.groupOffset = offsets##name[(pixel / GROUP_SIZE) * GROUP_SIZE], iter##name.extraOffset = offsets##name[pixel] - minCounts##name[pixel / GROUP_SIZE] * (pixel % GROUP_SIZE), iter##name.minCount = minCounts##name[pixel / GROUP_SIZE], iter##name.end = counts##name[pixel], iter##name.node = loc
	#else
		#define LFB_ITER_BEGIN_CL(name, pixel) iter##name.interOffset = (pixel % GROUP_SIZE) + offsets##name[(pixel / GROUP_SIZE) * GROUP_SIZE], iter##name.extraOffset = offsets##name[pixel] - minCounts##name[pixel / GROUP_SIZE] * (pixel % GROUP_SIZE), iter##name.minCount = minCounts##name[pixel / GROUP_SIZE], iter##name.end = counts##name[pixel], iter##name.node = 0
		#define LFB_ITER_SET_CL(name, pixel, loc) iter##name.interOffset = (pixel % GROUP_SIZE) + offsets##name[(pixel / GROUP_SIZE) * GROUP_SIZE], iter##name.extraOffset = offsets##name[pixel] - minCounts##name[pixel / GROUP_SIZE] * (pixel % GROUP_SIZE), iter##name.minCount = minCounts##name[pixel / GROUP_SIZE], iter##name.end = counts##name[pixel], iter##name.node = loc
	#endif
	#define LFB_ITER_INC_CL(name) iter##name.node++
	#define LFB_ITER_CHECK_CL(name) iter##name.node < iter##name.end
	#define LFB_ITER_CHECK_MAX_CL(name, loc) iter##name.node < iter##name.end && iter##name.node < loc
	#define LFB_COUNT_AT_CL(name, pixel) counts##name[pixel]

	//blockCount = currCount / blockSize
	//index = groupOffset + blockCount * groupSize * blockSize + run * blockSize + currCount % blockSize

	//index = groupOffset + run + groupSize * currCount

	#if USE_BLOCKS
		#if LFB_REVERSE
			#define LFB_GET_DATA_CL(name) data##name[(iter##name.end - 1 - iter##name.node) < iter##name.minCount ? (iter##name.groupOffset + ((iter##name.end - 1 - iter##name.node) / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + iter##name.run * BLOCK_SIZE + (iter##name.end - 1 - iter##name.node) % BLOCK_SIZE) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + (iter##name.end - 1 - iter##name.node) - iter##name.minCount)]
			#define LFB_WRITE_DATA_CL(name, frag) data##name[(iter##name.end - 1 - iter##name.node) < iter##name.minCount ? (iter##name.groupOffset + ((iter##name.end - 1 - iter##name.node) / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + iter##name.run * BLOCK_SIZE + (iter##name.end - 1 - iter##name.node) % BLOCK_SIZE) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + (iter##name.end - 1 - iter##name.node) - iter##name.minCount)] = frag
			#define LFB_GET_DATA_AT_CL(name, offset) data##name[(iter##name.end - 1 - offset) < iter##name.minCount ? (iter##name.groupOffset + ((iter##name.end - 1 - offset) / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + iter##name.run * BLOCK_SIZE + (iter##name.end - 1 - offset) % BLOCK_SIZE) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + (iter##name.end - 1 - offset) - iter##name.minCount)]
		#else
			#define LFB_GET_DATA_CL(name) data##name[iter##name.node < iter##name.minCount ? (iter##name.groupOffset + (iter##name.node / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + iter##name.run * BLOCK_SIZE + iter##name.node % BLOCK_SIZE) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + iter##name.node - iter##name.minCount)]
			#define LFB_WRITE_DATA_CL(name, frag) data##name[iter##name.node < iter##name.minCount ? (iter##name.groupOffset + (iter##name.node / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + iter##name.run * BLOCK_SIZE + iter##name.node % BLOCK_SIZE) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + iter##name.node - iter##name.minCount)] = frag
			#define LFB_GET_DATA_AT_CL(name, offset) data##name[offset < iter##name.minCount ? (iter##name.groupOffset + (offset / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + iter##name.run * BLOCK_SIZE + offset % BLOCK_SIZE) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + offset - iter##name.minCount)]
		#endif
	#else
		#if LFB_REVERSE
			#define LFB_GET_DATA_CL(name) data##name[(iter##name.end - 1 - iter##name.node) < iter##name.minCount ? (iter##name.interOffset + GROUP_SIZE * (iter##name.end - 1 - iter##name.node)) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + (iter##name.end - 1 - iter##name.node) - iter##name.minCount)]
			#define LFB_WRITE_DATA_CL(name, frag) data##name[(iter##name.end - 1 - iter##name.node) < iter##name.minCount ? (iter##name.interOffset + GROUP_SIZE * (iter##name.end - 1 - iter##name.node)) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + (iter##name.end - 1 - iter##name.node) - iter##name.minCount)] = frag
			#define LFB_GET_DATA_AT_CL(name, offset) data##name[(iter##name.end - 1 - offset) < iter##name.minCount ? (iter##name.interOffset + GROUP_SIZE * (iter##name.end - 1 - offset)) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + (iter##name.end - 1 - offset) - iter##name.minCount)]
		#else
			#define LFB_GET_DATA_CL(name) data##name[iter##name.node < iter##name.minCount ? (iter##name.interOffset + GROUP_SIZE * iter##name.node) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + iter##name.node - iter##name.minCount)]
			#define LFB_WRITE_DATA_CL(name, frag) data##name[iter##name.node < iter##name.minCount ? (iter##name.interOffset + GROUP_SIZE * iter##name.node) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + iter##name.node - iter##name.minCount)] = frag
			#define LFB_GET_DATA_AT_CL(name, offset) data##name[offset < iter##name.minCount ? (iter##name.interOffset + GROUP_SIZE * offset) : (iter##name.minCount * GROUP_SIZE + iter##name.extraOffset + offset - iter##name.minCount)]
		#endif
	#endif

	#define LFB_GET_SIZE_CL(name) size##name
	#if USE_BLOCKS
		#define LFB_ADD_DATA_CL(name, pixel, frag) \
			{ \
				uint run = uint(pixel) % GROUP_SIZE; \
				uint group = uint(pixel) / GROUP_SIZE; \
				uint groupOffset = offsets##name[group * GROUP_SIZE]; \
				uint minCount = minCounts##name[group]; \
				uint currCount = atomicAdd(counts##name[pixel], 1); \
				if (currCount < minCount) \
					data##name[groupOffset + (currCount / BLOCK_SIZE) * GROUP_SIZE * BLOCK_SIZE + run * BLOCK_SIZE + currCount % BLOCK_SIZE] = frag; \
				else \
				{ \
					uint adjustedOffset = offsets##name[pixel] - minCount * run; \
					data##name[minCount * GROUP_SIZE + adjustedOffset + (currCount - minCount)] = frag; \
				} \
			}
	#else
		#define LFB_ADD_DATA_CL(name, pixel, frag) \
			{ \
				uint run = uint(pixel) % GROUP_SIZE; \
				uint group = uint(pixel) / GROUP_SIZE; \
				uint groupOffset = offsets##name[group * GROUP_SIZE]; \
				uint minCount = minCounts##name[group]; \
				uint currCount = atomicAdd(counts##name[pixel], 1); \
				if (currCount < minCount) \
					data##name[groupOffset + run + GROUP_SIZE * currCount] = frag; \
				else \
				{ \
					uint adjustedOffset = offsets##name[pixel] - minCount * run; \
					data##name[minCount * GROUP_SIZE + adjustedOffset + (currCount - minCount)] = frag; \
				} \
			}
	#endif

#else

	// FIXME: This is all really outdated now, don't use it.
	#define OFFSETS_TYPE_CL layout(r32ui) coherent uimageBuffer
	
	// float size.
	#define FRAG_TYPE_CL_1 float
	#define DATA_TYPE_CL_1 layout(r32f) imageBuffer

	#define LFB_DEC_CL_1(name) \
		uniform OFFSETS_TYPE_CL Offsets##name; \
		uniform DATA_TYPE_CL_1 Data##name; \
		\
		LFBIteratorCL iter##name; \
		uniform ivec2 size##name;

	void addFragToLFB_CL_1(in ivec2 size, in int pixel, in FRAG_TYPE_L_1 frag, in OFFSETS_TYPE_L offsets, in DATA_TYPE_L_1 data)
	{
		uint index = imageAtomicAdd(offsets, pixel, 1U);
		imageStore(data, int(index), vec4(frag FRAG_REMAIN));
	}

	#define LFB_ITER_BEGIN_CL_1(name, pixel) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = 0
	#define LFB_ITER_SET_CL_1(name, pixel, loc) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = loc
	#define LFB_ITER_INC_CL_1(name) iter##name.node++
	#define LFB_ITER_CHECK_CL_1(name) iter##name.node + iter##name.start < iter##name.end
	#define LFB_ITER_CHECK_MAX_CL_1(name, loc) iter##name.node + iter##name.start < iter##name.end && iter##name.node < loc
	#define LFB_COUNT_AT_CL_1(name, pixel) imageLoad(Offsets##name, int(pixel)).r - ((pixel > 0) ? imageLoad(Offsets##name, int(pixel - 1)).r : 0)

	#if LFB_REVERSE
		#define LFB_GET_DATA_CL_1(name) imageLoad(Data##name, int(iter##name.end - 1 - iter##name.node)).rg
		#define LFB_WRITE_DATA_CL_1(name, frag) imageStore(Data##name, int(iter##name.end - 1 - iter##name.node), vec4(frag, 0, 0, 0))
	#else
		#define LFB_GET_DATA_CL_1(name) imageLoad(Data##name, int(iter##name.node + iter##name.start)).rg
		#define LFB_WRITE_DATA_CL_1(name, frag) imageStore(Data##name, int(iter##name.node + iter##name.start), vec4(frag, 0, 0, 0))
	#endif

	#define LFB_ADD_DATA_CL_1(name, pixel, frag) addFragToLFB_CL_1(size##name, pixel, frag, Offsets##name, Data##name)
	#define LFB_GET_SIZE_CL_1(name) size##name

	// vec2 size.
	#define FRAG_TYPE_CL_2 vec2
	#define DATA_TYPE_CL_2 layout(rg32f) imageBuffer

	#define LFB_DEC_CL_2(name) \
		uniform OFFSETS_TYPE_CL Offsets##name; \
		uniform DATA_TYPE_CL_2 Data##name; \
		\
		LFBIteratorCL iter##name; \
		uniform ivec2 size##name;

	void addFragToLFB_CL_2(in ivec2 size, in int pixel, in FRAG_TYPE_CL_2 frag, in OFFSETS_TYPE_CL offsets, in DATA_TYPE_CL_2 data)
	{
		uint index = imageAtomicAdd(offsets, pixel, 1U);
		imageStore(data, int(index), vec4(frag FRAG_REMAIN));
	}

	#define LFB_ITER_BEGIN_CL_2(name, pixel) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = 0
	#define LFB_ITER_SET_CL_2(name, pixel, loc) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = loc
	#define LFB_ITER_INC_CL_2(name) iter##name.node++
	#define LFB_ITER_CHECK_CL_2(name) iter##name.node + iter##name.start < iter##name.end
	#define LFB_ITER_CHECK_MAX_CL_2(name, loc) iter##name.node + iter##name.start < iter##name.end && iter##name.node < loc
	#define LFB_COUNT_AT_CL_2(name, pixel) imageLoad(Offsets##name, int(pixel)).r - ((pixel > 0) ? imageLoad(Offsets##name, int(pixel - 1)).r : 0)

	#if LFB_REVERSE
		#define LFB_GET_DATA_CL_2(name) imageLoad(Data##name, int(iter##name.end - 1 - iter##name.node)).rg
		#define LFB_WRITE_DATA_CL_2(name, frag) imageStore(Data##name, int(iter##name.end - 1 - iter##name.node), vec4(frag, 0, 0))
	#else
		#define LFB_GET_DATA_CL_2(name) imageLoad(Data##name, int(iter##name.node + iter##name.start)).rg
		#define LFB_WRITE_DATA_CL_2(name, frag) imageStore(Data##name, int(iter##name.node + iter##name.start), vec4(frag, 0, 0))
	#endif

	#define LFB_ADD_DATA_CL_2(name, pixel, frag) addFragToLFB_CL_2(size##name, pixel, frag, Offsets##name, Data##name)
	#define LFB_GET_SIZE_CL_2(name) size##name

	// vec4 size.
	#define FRAG_TYPE_CL_4 vec4
	#define DATA_TYPE_CL_4 layout(rgba32f) imageBuffer

	#define LFB_DEC_CL_4(name) \
		uniform OFFSETS_TYPE_CL Offsets##name; \
		uniform DATA_TYPE_CL_4 Data##name; \
		\
		LFBIteratorCL iter##name; \
		uniform ivec2 size##name;

	void addFragToLFB_CL_4(in ivec2 size, in int pixel, in FRAG_TYPE_CL_4 frag, in OFFSETS_TYPE_CL offsets, in DATA_TYPE_CL_4 data)
	{
		uint index = imageAtomicAdd(offsets, pixel, 1U);
		imageStore(data, int(index), vec4(frag FRAG_REMAIN));
	}

	#define LFB_ITER_BEGIN_CL_4(name, pixel) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = 0
	#define LFB_ITER_SET_CL_4(name, pixel, loc) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = loc
	#define LFB_ITER_INC_CL_4(name) iter##name.node++
	#define LFB_ITER_CHECK_CL_4(name) iter##name.node + iter##name.start < iter##name.end
	#define LFB_ITER_CHECK_MAX_CL_4(name, loc) iter##name.node + iter##name.start < iter##name.end && iter##name.node < loc
	#define LFB_COUNT_AT_CL_4(name, pixel) imageLoad(Offsets##name, int(pixel)).r - ((pixel > 0) ? imageLoad(Offsets##name, int(pixel - 1)).r : 0)

	#if LFB_REVERSE
		#define LFB_GET_DATA_CL_4(name) imageLoad(Data##name, int(iter##name.end - 1 - iter##name.node)).rg
		#define LFB_WRITE_DATA_CL_4(name, frag) imageStore(Data##name, int(iter##name.end - 1 - iter##name.node), vec4(frag))
	#else
		#define LFB_GET_DATA_CL_4(name) imageLoad(Data##name, int(iter##name.node + iter##name.start)).rg
		#define LFB_WRITE_DATA_CL_4(name, frag) imageStore(Data##name, int(iter##name.node + iter##name.start), vec4(frag))
	#endif

	#define LFB_ADD_DATA_CL_4(name, pixel, frag) addFragToLFB_CL_4(size##name, pixel, frag, Offsets##name, Data##name)
	#define LFB_GET_SIZE_CL_4(name) size##name
#endif

// Defaults for different sizes.
#ifndef FRAG_TYPE_1
	#define FRAG_TYPE_1 FRAG_TYPE_CL_1
	#define LFB_DEC_1(name) LFB_DEC_CL_1(name)
#endif

#ifndef FRAG_TYPE_2
	#define FRAG_TYPE_2 FRAG_TYPE_CL_2
	#define LFB_DEC_2(name) LFB_DEC_CL_2(name)
#endif

#ifndef FRAG_TYPE_4
	#define FRAG_TYPE_4 FRAG_TYPE_CL_4
	#define LFB_DEC_4(name) LFB_DEC_CL_4(name)
#endif

// Use default macro names if none are explicitly specified.
#ifndef LFB_DEC
	#define LFB_DEC(name, fragType) LFB_DEC_CL(name, fragType)
	#define LFB_ITER_BEGIN(name, pixel) LFB_ITER_BEGIN_CL(name, pixel)
	#define LFB_ITER_SET(name, pixel, loc) LFB_ITER_SET_CL(name, pixel, loc)
	#define LFB_ITER_INC(name) LFB_ITER_INC_CL(name)
	#define LFB_ITER_CHECK(name) LFB_ITER_CHECK_CL(name)
	#define LFB_ITER_CHECK_MAX(name, loc) LFB_ITER_CHECK_MAX_CL(name, loc)
	#define LFB_COUNT_AT(name, pixel) LFB_COUNT_AT_CL(name, pixel)
	#define LFB_GET_DATA(name) LFB_GET_DATA_CL(name)
	#define LFB_GET_DATA_AT(name, offset) LFB_GET_DATA_AT_CL(name, offset)
	#define LFB_WRITE_DATA(name, frag) LFB_WRITE_DATA_CL(name, frag)
	#define LFB_ADD_DATA(name, pixel, frag) LFB_ADD_DATA_CL(name, pixel, frag)
	#define LFB_GET_SIZE(name) LFB_GET_SIZE_CL(name)
#endif

#endif

