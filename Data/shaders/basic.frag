#version 430

uniform vec3 lightDir;

in FragData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} FragIn;

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

out vec4 fragColor;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;


void main()
{
	vec3 normal = normalize(FragIn.normal);
	vec3 viewDir = normalize(-FragIn.esFrag);
	vec3 lightDirN = normalize(lightDir);

	float cosTheta = clamp(dot(normal, lightDirN), 0.0, 1.0);

	vec3 texColor = vec3(0.0);
	if (texFlag)
		texColor = texture2D(diffuseTex, FragIn.texCoord).xyz;

	vec3 ambient = Material.ambient.xyz * 0.2;
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float dp = max(cosTheta, 0.0);

	if (texFlag)
		ambient = texColor * 0.2;

	if (dp > 0.0)
	{
		if (!texFlag)
			diffuse = Material.diffuse.xyz * dp;
		else
			diffuse = texColor * dp;
		vec3 reflection = normalize(reflect(-lightDirN, normal));
		float nDotH = max(dot(viewDir, reflection), 0.0);
		float intensity = pow(nDotH, Material.shininess);
		specular = Material.specular.xyz * intensity;
	}

	vec4 color = vec4(ambient + diffuse + specular, 0.1);
	fragColor = color;
}

