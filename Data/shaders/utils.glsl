#ifndef UTILS_GLSL
#define UTILS_GLSL

#define PI 3.1415926

// TODO: test performance of these functions, they could possibly be made faster.

// Conversion adapted from http://stackoverflow.com/questions/1659440/32-bit-to-16-bit-floating-point-conversion
// Can also see UtilsGeneral.cpp for C++ version.
uint f32ToF16(float f)
{
	int v = int(floatBitsToUint(f));
	uint uSign = uint(v & 0x80000000);
	v ^= int(uSign);
	uSign >>= 16;
	int s = 0x52000000;
	float sf = uintBitsToFloat(uint(s));
	float vf = uintBitsToFloat(uint(v));
	s = int(floatBitsToUint(sf * vf));
	v ^= (s ^ v) & -int(0x38800000 > v);
	v ^= (0x7F800000 ^ v) & -(int(0x7F800000 > v) & int(v > 0x477FE000));
	v ^= ((((0x7F800000 >> 13) + 1) << 13) ^ v) & -(int((((0x7F800000 >> 13) + 1) << 13) > v) & int(v > 0x7F800000));
	uint vu = (uint(v)) >> 13;
	v = int(vu);
	v ^= ((v - ((0x7F800000 >> 13) - (0x477FE000 >> 13) - 1)) ^ v) & -int(v > (0x477FE000 >> 13));
	v ^= ((v - ((0x38800000 >> 13) - 0x003FF - 1)) ^ v) & -int(v > 0x003FF);
	return (uint(v)) | uSign;
}

float f16ToF32(uint i)
{
	int v = int(i);
	int iSign = int(v & (0x80000000 >> 16));
	v ^= iSign;
	iSign <<= 16;
	v ^= ((v + ((0x38800000 >> 13) - 0x003FF - 1)) ^ v) & -int(v > 0x003FF);
	v ^= ((v + ((0x7F800000 >> 13) - (0x477FE000 >> 13) - 1)) ^ v) & -int(v > (0x477FE000 >> 13));
	int s = 0x33800000;
	float sf = uintBitsToFloat(uint(s));
	sf *= v;
	s = int(floatBitsToUint(sf));
	int mask = -int(0x00400 > v);
	v <<= 13;
	v ^= (s ^ v) & mask;
	v |= iSign;
	return uintBitsToFloat(uint(v));
}

vec2 decodeLight(float f)
{
	uint u = floatBitsToUint(f);
	uint id = u >> 16;
	float depth = f16ToF32(u & 0xFFFFU);
	return vec2(uintBitsToFloat(id), depth);
}

float encodeLight(uint id, float depth)
{
	uint d = f32ToF16(depth);
	uint u = (id << 16) | d;
	return uintBitsToFloat(u);
}

uint rgba8ToUint(vec4 v)
{
	uvec4 i = clamp(uvec4(v * 255.0), 0U, 255U);
	return (i.x << 24U) | (i.y << 16U) | (i.z << 8U) | i.w;
}

float rgba8ToFloat(vec4 v)
{
	uint i = rgba8ToUint(v);
	return uintBitsToFloat(i);
}

vec4 uintToRGBA8(uint i)
{
	return vec4((float(i >> 24U)) / 255.0f,
		(float((i >> 16U) & 0xFFU)) / 255.0f,
		(float((i >> 8U) & 0xFFU)) / 255.0f,
		(float(i & 0xFFU)) / 255.0f
	);
}

vec4 floatToRGBA8(float f)
{
	uint i = floatBitsToUint(f);
	return uintToRGBA8(i);
}

float avg(vec3 c)
{
	return (c.r + c.g + c.b) / 3.0;
}

vec3 hueToRGB(float h)
{
	h = fract(h) * 6.0;
	vec3 rgb;
	rgb.r = clamp(abs(3.0 - h)-1.0, 0.0, 1.0);
	rgb.g = clamp(2.0 - abs(2.0 - h), 0.0, 1.0);
	rgb.b = clamp(2.0 - abs(4.0 - h), 0.0, 1.0);
	return rgb;
}

vec3 heat(float x)
{
	return hueToRGB(2.0/3.0-(2.0/3.0)*x);
}

vec3 debugColLog(int i)
{
	if (i == 0) return vec3(1.0);
	if (i == 1) return vec3(0.0);
	if (i == 2) return vec3(1,0.5,0);
	if (i <= 4) return vec3(0.5,0,1);
	if (i <= 8) return vec3(0,0,1);
	if (i <= 16) return vec3(0,1,1);
	if (i <= 32) return vec3(0,1,0.5);
	if (i <= 64) return vec3(1,1,0);
	if (i <= 128) return vec3(0,0.5,1);
	if (i <= 256) return vec3(0,1,0);
	if (i <= 512) return vec3(1,0,0);
	return vec3(0.5, 0.5, 0.5);
}

#if 0
vec3 getDirFromRot(float zRot, float xRot, vec3 dir)
{
	float sinZ = sin(zRot);
	float cosZ = cos(zRot);
	float sinX = sin(xRot);
	float cosX = cos(xRot);

	// Currently column major, may need to be changed to row major.
	mat4 rotMat = mat4(	vec4(cosZ, sinZ, 0, 0),
						vec4(-sinZ * cosX, cosZ * cosX, sinZ, 0)
						vec4(-sinZ * sinX, -cosZ * sinX, cosX, 0),
						vec4(0, 0, 0, 1));

	// TODO: not sure if the order here should be changed or not.
	return rotMat * dir;
}
#endif

bool intersectsAABB(vec4 vMin, vec4 vMax, vec4 aabbMin, vec4 aabbMax)
{
	// Ignore w values.
	if (vMax.x < aabbMin.x || vMin.x > aabbMax.x)
		return false;
	if (vMax.y < aabbMin.y || vMin.y > aabbMax.y)
		return false;
	if (vMax.z < aabbMin.z || vMin.z > aabbMax.z)
		return false;
	return true;
}

vec4 getEyeFromWindow(vec3 p, ivec4 viewport, mat4 invPMatrix)
{
	vec3 ndcPos;
	ndcPos.xy = ((p.xy - vec2(viewport.xy)) / vec2(viewport.zw)) * 2.0 - 1.0;
	ndcPos.z = 1.0;
	//ndcPos.z = p.z;

	vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0);
	eyeDir /= eyeDir.w;
	vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0);
	return eyePos;
}

vec2 getScreenFromClip(vec4 csPos, vec2 size)
{
	return vec2(((vec2(csPos.xy / csPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(size.x, size.y));
}

// Returns distance along esRay for intersection with plane (or 0.0 for parallel).
bool planeIntersection(vec3 planePos, vec3 normal, vec3 esRay, vec3 origin, out float depth)
{
	vec3 l = normalize(esRay);
	vec3 n = normalize(normal);

	float d = dot(l, n);
	if (d < 0.0001 && d > -0.0001)
		return false;
	depth = dot(planePos - origin, n) / d;
	return true;
}

// Returns components (a, b, c, discr) based on a sphere position/radius, and a ray intersection.
vec4 sphereEquation(vec3 spherePos, float radius, vec3 esRay, vec3 origin)
{
	vec3 l = normalize(esRay);
	vec3 p = spherePos;
	float r2 = radius * radius;

	float a = dot(l, l);
	float b = 2.0 * dot(l, (origin - p));
	float c = dot((origin - p), (origin - p)) - r2;
	float discr = (b * b) - (4.0 * a * c);

	return vec4(a, b, c, discr);
}

vec4 coneEquation(vec3 conePos, float range, vec3 esRay, vec3 origin, vec3 coneDir, float aperture)
{
	// Cone, where:
	//	- l is direction of line
	//	- n is direction of cone
	//	- o is origin of line
	//	- theta is aperture
	//	- d is distance to intersect along line
	//	- p is position of cone
	//
	// d^2 * (l.dot_product(n)^2 - l.dot_product(l) * n.dot_product(n) * cos(theta)^2) +
	// 2 * d * (n.dot_product(l) * n.dot_product(o) - n.dot_product(c) * n.dot_product(l) + (n.dot_product(n) * c.dot_product(l) - n.dot_product(n) * l.dot_product(o)) * cos(theta)^2) +
	// c.dot_product(n)^2 - (c.dot_product(c) * n.dot_product(n) + n.dot_product(n) * o.dot_product(o) - 2 * n.dot_product(n) * c.dot_product(o)) * cos(theta)^2 - 
	// 2 * n.dot_product(c) * n.dot_product(o) + n.dot_product(o)^2
	//
	// Or for a cone at the origin (no c):
	//
	// d^2 * -(l.dot_product(l) * n.dot_product(n) * cos(theta)^2 - l.dot_product(n)^2) - 2 * d * (l.dot_product(o) * n.dot_product(n) * cos(theta)^2 - l.dot_product(n) * n.dot_product(o)) -
	// n.dot_product(n) * o.dot_product(o) * cos(theta)^2 + n.dot_product(o)^2
	float cosA = cos(aperture);
	float cos2 = cosA * cosA;

	vec3 l = normalize(esRay);
	vec3 p = conePos;
	vec3 n = normalize(coneDir);
	//vec3 o = origin;

	float nn = dot(n, n);
	float ln = dot(l, n);
	//float on = dot(o, n);
	float pn = dot(p, n);

	// This is what it would normally be, but since we know the esRay origin is (0,0,0), we can simplify.
	// float a = (ln * ln) - (dot(l, l) * nn * cos2);
	// float b = 2.0 * ((ln * on) - (pn * ln) + ((nn * dot(p, l)) - (nn * dot(l, o))) * cos2);
	// float c = (pn * pn) - ((dot(p, p) * nn) + (dot(o, o) * nn) - (2.0 * dot(p, o) * nn)) * cos2 - (2.0 * pn * on) + (on * on);

	float a = ln * ln - dot(l, l) * nn * cos2;
	float b = 2.0 * (-pn * ln + nn * dot(p, l) * cos2);
	float c = pn * pn - dot(p, p) * nn * cos2;

	float discr = (b * b) - (4.0 * a * c);

	return vec4(a, b, c, discr);
}

vec4 cylinderEquation(vec3 cylPos, float range, vec3 esRay, vec3 origin, vec3 cylDir, float radius)
{
	// Cylinder, where:
	//	- l is direction of line
	//	- n is direction of cylinder
	//	- o is origin of line
	//	- r is radius of cylinder
	//	- d is distance to intersect along line
	//	- p is position of cylinder

	vec3 l = normalize(esRay);
	vec3 p = cylPos;
	vec3 n = normalize(cylDir);
	vec3 o = origin;
	float r = radius;

	float ln = dot(l, n);
	float pn = dot(p, n);
	float on = dot(o, n);

	// This is what it would normally be, but since we know the esRay origin is (0,0,0), we can simplify.
	// float a = dot(l, l) - (ln * ln);
	// float b = 2.0 * (pn * ln - on * ln - dot(l, p) + dot(l, o));
	// float c = 2.0 * (pn * on - dot(p, o)) + dot(p, p) + dot(o, o) - (pn * pn) - (on * on) - r * r;

	float a = dot(l, l) - (ln * ln);
	float b = 2.0 * (pn * ln - on * ln - dot(l, p) + dot(l, o));
	float c = 2.0 * (pn * on - dot(p, o)) + dot(p, p) + dot(o, o) - (pn * pn) - (on * on) - r * r;

	float discr = (b * b) - (4.0 * a * c);
	return vec4(a, b, c, discr);
}

// Numerically stable.
vec2 getDepthDistances(vec4 eq)
{
	float discr = sqrt(eq.w);
	float d1, d2, z1, z2;

	if (eq.y >= 0)
	{
		d1 = 2.0 * eq.x;
		d2 = -eq.y - discr;
		if (d1 == 0.0)
			z1 = 0.0;
		else
			z1 = (-eq.y - discr) / d1;
		if (d2 == 0.0)
			z2 = 0.0;
		else
			z2 = (2.0 * eq.z) / d2;
	}
	else
	{
		d1 = -eq.y + discr;
		d2 = 2.0 * eq.x;
		if (d1 == 0.0)
			z1 = 0.0;
		else
			z1 = (2.0 * eq.z) / d1;
		if (d2 == 0.0)
			z2 = 0.0;
		else
			z2 = (-eq.y + discr) / d2;
	}

	return vec2(z1, z2);
}

vec2 getDepths(vec4 eq, vec3 dir)
{
	vec2 z = getDepthDistances(eq);
	return vec2((dir * z.x).z, (dir * z.y).z);
}

bool lineIntersection(vec2 p1, vec2 p2, vec2 p3, vec2 p4, out vec2 p)
{
	// l1 -> p1 - p2.
	// l2 -> p3 - p4.
	// Parallel?
	p = vec2(0);
	float c = ((p1.x - p2.x) * (p3.y - p4.y)) - ((p1.y - p2.y) * (p3.x - p4.x));
	if (c == 0)
		return false;

	float a = (p1.x * p2.y) - (p1.y * p2.x);
	float b = (p3.x * p4.y) - (p3.y * p4.x);

	p.x = ((a * (p3.x - p4.x)) - (b * (p1.x - p2.x))) / c;
	p.y = ((a * (p3.y - p4.y)) - (b * (p1.y - p2.y))) / c;

	// Test if intersection is outside line segments.
	// TODO: Possibly a faster way than this?
	float minX = min(p1.x, p2.x);
	float maxX = max(p1.x, p2.x);
	float minY = min(p1.y, p2.y);
	float maxY = max(p1.y, p2.y);

	if (p.x < minX || p.x > maxX || p.y < minY || p.y > maxY)
		return false;
	return true;
}

bool sphereIntersection(vec3 spherePos, float radius, vec3 esRay, out float frontDepth, out float backDepth)
{
	vec4 eq = sphereEquation(spherePos, radius * 1.1, esRay, vec3(0.0));

	// No intersections.
	if (eq.w < 0)
		return false;

	vec2 depths = getDepths(eq, esRay);

	frontDepth = min(-depths.x, -depths.y);
	backDepth = max(-depths.x, -depths.y);

	return true;
}

bool coneIntersection(vec3 conePos, float radius, vec3 esRay, vec3 coneDir, float aperture, out float frontDepth, out float backDepth)
{
	vec3 planePos = conePos + coneDir * (radius * 1.1);
	vec4 eq = coneEquation(conePos, radius * 1.1, esRay, vec3(0.0), coneDir, aperture * 1.1);

	// No intersections.
	if (eq.w < 0)
		return false;

	bool b1 = false, b2 = false, b3 = false;

	// Intersection with an infinite conical surface.
	vec2 distances = getDepthDistances(eq);

	float d1 = distances.x;
	float d2 = distances.y;
	vec3 p1 = esRay * d1;
	vec3 p2 = esRay * d2;

	float z1 = 0, z2 = 0;

	// Check if the intersections are between conePos and its end plane.

	// Behind the cone or not?
	if (dot(p1 - conePos, coneDir) > 0)
		b1 = true;
	if (dot(p2 - conePos, coneDir) > 0)
		b2 = true;

	if (!b1 && !b2)
		return false;

	// Too far away?
	//if (b1 && dot(p1 - planePos, coneDir) < 0)
	if (b1 && distance(p1, conePos) > radius * 1.1)
		b1 = false;
	//if (b2 && dot(p2 - planePos, coneDir) < 0)
	if (b2 && distance(p2, conePos) > radius * 1.1)
		b2 = false;

	// Neither are valid, no intersection.
	if (!b1 && !b2)
		return false;

	// Both are valid, use them.
	if (b1 && b2)
	{
		z1 = p1.z;
		z2 = p2.z;
	}
	else
	{
		// TODO: Use a spherical cap rather than a plane.
	#if 1
		vec4 eq = sphereEquation(conePos, radius * 1.1, esRay, vec3(0.0));

		// No intersections.
		if (eq.w < 0)
			return false;

		vec2 depths = getDepths(eq, esRay);

		//frontDepth = min(-depths.x, -depths.y);
		//backDepth = max(-depths.x, -depths.y);
		z1 = depths.x;
		z2 = depths.y;
	#else
		//return false;
		// Intersection with a plane at the edge of the cone (ignoring results outside the cone's bounds).
		float d3 = 0;
		planeIntersection(planePos, coneDir, esRay, vec3(0.0), d3);
		vec3 p3 = esRay * d3;
		#if 0
			// Determine if d3 is inside the circle.
			if (distance(p3, VertexIn.conePos.xyz) < VertexIn.radius * 1.1)
				b3 = true;

			// FIXME: not sure if this step is necessary or not...
			// If it's invalid, then probably no intersection.
			if (!b3)
			{
				return false;
			}
		#endif
		z2 = p3.z;

		// Choose the two valid intersections.
		if (!b1)
			z1 = p2.z;
		else
			z1 = p1.z;
	#endif
	}

	frontDepth = min(-z1, -z2);
	backDepth = max(-z1, -z2);

	return true;
}

bool cylinderIntersection(vec3 cylPos, float range, vec3 esRay, vec3 cylDir, float radius, out float frontDepth, out float backDepth)
{
	// Bottom plane will be at the cylinder's position.
	vec3 planePos = cylPos + cylDir * (range * 1.2);
	vec4 eq = cylinderEquation(cylPos, range * 1.2, esRay, vec3(0.0), cylDir, radius * 1.1);

	// No intersections.
	if (eq.w < 0)
		return false;

	bool b1 = false, b2 = false, b3 = false;

	// Intersection with an infinite cylindrical surface.
	vec2 distances = getDepthDistances(eq);

	float d1 = distances.x;
	float d2 = distances.y;
	vec3 p1 = esRay * d1;
	vec3 p2 = esRay * d2;

	float z1 = 0, z2 = 0;

	// Check if the intersections are between cylPos and its end planes.

	// Above the cylinder's position?
	if (dot(p1 - cylPos, cylDir) > 0)
		b1 = true;
	if (dot(p2 - cylPos, cylDir) > 0)
		b2 = true;

	// Above the end plane?
	if (b1 && dot(p1 - planePos, cylDir) > 0)
		b1 = false;
	if (b2 && dot(p2 - planePos, cylDir) > 0)
		b2 = false;

	// Neither are valid, no intersection.
	if (!b1 && !b2)
		return false;

	// Both are valid, use them.
	if (b1 && b2)
	{
		z1 = p1.z;
		z2 = p2.z;
	}
	else
	{
		// Intersection with a plane at either end of the cylinder (ignoring results outside the cylinder's radius).
		float d3 = 0, d4 = 0;
		planeIntersection(cylPos, cylDir, esRay, vec3(0.0), d3);
		planeIntersection(cylPos, cylDir, esRay, vec3(0.0), d4);
		vec3 p3 = esRay * d3;
		vec3 p4 = esRay * d4;

		if (!b1 && !b2)
		{
			// Have to use both intersections.
			z1 = p3.z;
			z2 = p4.z;
		}
		else
		{
			// Pick the intersection inside the cylinder's radius.
			vec3 p5 = p4;
			if (distance(p3, cylPos) < radius * 1.1)
				p5 = p3;

			z2 = p5.z;

			// Choose the two valid intersections.
			if (!b1)
				z1 = p2.z;
			else
				z1 = p1.z;
		}
	}

	frontDepth = min(-z1, -z2);
	backDepth = max(-z1, -z2);

	return true;
}


float rand(vec2 seed)
{
    return fract(sin(dot(seed.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float random(vec3 seed, int i)
{
    vec4 seed4 = vec4(seed, i);
    float dp = dot(seed4, vec4(12.9898, 78.233, 45.164, 94.673));
    return fract(sin(dp) * 43758.5453);
}



float noise2D(int x, int y)
{
	int n = x + y * 57;
	n = (n << 13) ^ n;
	return float(1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

float noise3D(int x, int y, int z)
{
	int n = x + y * 57 + z * 211;
	n = (n << 13) ^ n;
	return float(1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

float interpLinear(float a, float b, float x)
{
	return a + (b - a) * x;
}

float smoothNoise2D(float x, float y)
{
	int i = int(floor(x));
	int j = int(floor(y));
	float a = interpLinear(noise2D(i, j), noise2D(i + 1, j), x - i);
	float b = interpLinear(noise2D(i, j + 1), noise2D(i + 1, j + 1), x - i);
	return interpLinear(a, b, y - j);
}

float smoothNoise3D(float x, float y, float z)
{
	int i = int(floor(x));
	int j = int(floor(y));
	int k = int(floor(z));
	float a = interpLinear(noise3D(i, j, k), noise3D(i + 1, j, k), x - i);
	float b = interpLinear(noise3D(i, j + 1, k), noise3D(i + 1, j + 1, k), x - i);
	float c = interpLinear(noise3D(i, j, k + 1), noise3D(i + 1, j, k + 1), x - i);
	float d = interpLinear(noise3D(i, j + 1, k + 1), noise3D(i + 1, j + 1, k + 1), x - i);
	a = interpLinear(a, b, y - j);
	b = interpLinear(c, d, y - j);
	return interpLinear(a, b, z - k);
}



float perlin2D(float x, float y)
{
	float freq, amp = 1, val = 0.0;
	for (int oct = 0; oct < 12; oct++)
	{
		freq = float(1 << oct);
		//val += smoothNoise2D(1000.0 + x * freq, 1000.0 + y * freq) * amp;
		val += smoothNoise2D(x * freq, y * freq) * amp;
		amp *= 0.6;
	}
	return val;
}

float perlin3D(float x, float y, float z)
{
	float freq, amp = 1.0, val = 0.0;
	for (int oct = 0; oct < 12; oct++)
	{
		freq = float(1 << oct);
		val += smoothNoise3D(1000.0 + x * freq, 1000.0 + y * freq, 1000.0 + z * freq) * amp;
		amp *= 0.6;
	}
	return val;
}



#endif

