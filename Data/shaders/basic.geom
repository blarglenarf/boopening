#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn[3];

out FragData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} FragOut;


void main()
{
	for (int i = 0; i < gl_in.length(); i++)
	{
		gl_Position = gl_in[i].gl_Position;
		FragOut.normal = VertexIn[i].normal;
		FragOut.esFrag = VertexIn[i].esFrag;
		FragOut.texCoord = VertexIn[i].texCoord;
		EmitVertex();
	}
	EndPrimitive();
}

