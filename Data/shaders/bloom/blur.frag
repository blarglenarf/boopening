#version 430

in vec2 texCoord;

uniform sampler2D brightTex;
uniform sampler2D prevTex;

uniform bool horizontal;
uniform int lod;
uniform float weight[5] = float[] (0.227027, 0.1945946, 0.1216216, 0.054054, 0.016216);

uniform bool prevFlag;

out vec4 fragColor;


vec4 gaussianBlur()
{
	vec2 texOffset = 1.0 / textureSize(brightTex, lod); // gets size of single texel
	vec3 result = texture(brightTex, texCoord).rgb * weight[0]; // current fragment's contribution

	if (horizontal)
	{
		for (int i = 1; i < 5; ++i)
		{
			result += texture(brightTex, texCoord + vec2(texOffset.x * i, 0)).rgb * weight[i];
			result += texture(brightTex, texCoord - vec2(texOffset.x * i, 0)).rgb * weight[i];
		}
	}
	else
	{
		for (int i = 1; i < 5; ++i)
		{
			result += texture(brightTex, texCoord + vec2(0, texOffset.y * i)).rgb * weight[i];
			result += texture(brightTex, texCoord - vec2(0, texOffset.y * i)).rgb * weight[i];
		}
	}

	return vec4(result, 1);
}


void main()
{
	// Blur along with the previous color or not?
	vec4 prevColor = vec4(0);

	vec4 color = gaussianBlur();

	fragColor = vec4(color.xyz, 1);
}

