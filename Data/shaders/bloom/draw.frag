#version 430

in vec2 texCoord;

uniform sampler2D tex;

out vec4 fragColor;


void main()
{
	//vec4 color = texelFetch(tex, ivec2(gl_FragCoord.xy), 0);
	vec4 color = texture2D(tex, texCoord);

	fragColor = vec4(color.rgb, 1.0);
}

