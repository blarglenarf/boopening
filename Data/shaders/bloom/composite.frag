#version 430

in vec2 texCoord;

uniform sampler2D colorTex;
uniform sampler2D brightTex;
uniform sampler2D blurTex;

out vec4 fragColor;


// https://software.intel.com/en-us/articles/compute-shader-hdr-and-bloom for better tone mapping
// L = Linput * (middleGrey / avgLogLuminance)
// Loutput = L / (1 + L)
// or
// https://learnopengl.com/Advanced-Lighting/HDR
vec3 toneMapColor(vec3 hdrColor, float exposure, float gamma)
{
	// How do I get good looking tone mapping?
#if 0
	// If you're much brighter than the avg, go down.
	// If you're much dimmer than the avg, go up.

	// Average luminance from highest level mipmap.
	vec3 avgBlur = textureLod(blurTex, texCoord, 1000).rgb;
	vec3 avgColor = textureLod(colorTex, texCoord, 1000).rgb;
#if 0
float lum = dot(rgb, vec3(0.2126f, 0.7152f, 0.0722f));
float L = (scale / averageLum) * lum;
float Ld = (L * (1.0 + L / lumwhite2)) / (1.0 + L);
//first
vec3 xyY = RGBtoxyY(rgb);
xyY.z *= Ld;
rgb = xyYtoRGB(xyY);
//second
rgb = (rgb / lum) * Ld;
#endif
	vec3 avg = (avgBlur + avgColor) / 2.0;
	vec3 upper = vec3(0.299, 0.587, 0.114);
	float avgLum = dot(avg, upper);
	float lum = dot(hdrColor, upper);

	float L = (0.5 / avgLum) * lum;
	float Ld = (L * (1.0 + L)) / (1.0 + L);
	vec3 mapped = (hdrColor / lum) * Ld;
	mapped = mapped * (vec3(1.0) - exp(-hdrColor.xyz * exposure));

	// Exposure tone mapping.
	//vec3 mapped = hdrColor.xyz / (hdrColor.xyz + vec3(1));
	//vec3 mapped = vec3(1.0) - exp(-hdrColor.xyz * exposure);

	// Gamma correction.
	mapped = pow(mapped, vec3(1.0 / gamma));

	return mapped;
#else
	vec3 result = vec3(1.0) - exp(-hdrColor * exposure);
	result = pow(result, vec3(1.0 / gamma));

	return result;
#endif
}


void main()
{
	vec4 color = texture2D(colorTex, texCoord);
	vec4 blur = texture2D(blurTex, texCoord);

	color.rgb += blur.rgb;

	//color.rgb = toneMapColor(color.rgb, 1.0, 1.0);

	fragColor = vec4(color.rgb, 1.0);
}

