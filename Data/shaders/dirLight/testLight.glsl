#ifndef LIGHT_GLSL
#define LIGHT_GLSL

vec4 dirLight(in vec4 lightColor, in vec3 lightDir, in vec3 esFrag, in vec3 normal, in vec3 viewDir)
{
	//float cosTheta = clamp(dot(VertexIn.normal, lightDir), 0.0, 1.0);
	float cosTheta = dot(normal, lightDir);

	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	//float dp = max(cosTheta, 0.0);
	float dp = abs(cosTheta);

	//if (dp > 0.0)
	//{
		//vec4 lightColor = floatToRGBA8(lightColors[id]);
		diffuse = Material.diffuse.xyz * dp * lightColor.xyz;
		vec3 reflection = reflect(-lightDir, normal);
		//float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
		float nDotH = abs(dot(viewDir, normalize(reflection)));
		float intensity = pow(nDotH, Material.shininess);
		specular = Material.specular.xyz * intensity * lightColor.xyz;

		//float att = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
		//att = 1.0f;
		//att *= att;
		//return vec4(att * (diffuse + specular), 0);
		return vec4(diffuse + specular, 0);
		//color.w = 0.8;
	//}
}


#endif

