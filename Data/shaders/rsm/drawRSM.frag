#version 430

in vec2 texCoord;

uniform sampler2D tex;

#if 0
uniform sampler2D dirTex;
uniform sampler2D colorTex;
uniform sampler2D posTex;
uniform sampler2D lodTex;
uniform sampler2D normalTex;
#endif

out vec4 fragColor;


void main()
{
	//vec4 color = texture2D(dirTex, texCoord);
	//vec4 color = texture2D(colorTex, texCoord);
	//vec4 color = texture2D(posTex, texCoord);
	//vec4 color = texture2D(lodTex, texCoord) / 30.0;
	//vec4 color = texture2D(normalTex, texCoord);
	vec4 color = texture2D(tex, texCoord);

	fragColor = vec4(color.rgb, 1.0);
}

