#version 430

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

//#define FRAG_SIZE 2
#define LFB_BLOCK_SIZE 32
#define INDEX_BY_TILE 0

#define LFB_FRAG_TYPE vec2
#define USE_FLOAT 1

out vec4 fragColor;

#import "lfb_LL"

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

// To use different deep images, call replace(LFB_NAME, ...).
#if FRAG_SIZE == 1
	LFB_DEC_LL_1(LFB_NAME);
#elif FRAG_SIZE == 4
	LFB_DEC_LL_4(LFB_NAME);
#else
	LFB_DEC_LL_2(LFB_NAME);
#endif

#define COMPOSITE 0
#define COMPOSITE_LOCAL 0

#define MAX_FRAGS 0
#define MAX_FRAGS_OVERRIDE 0

#if MAX_FRAGS_OVERRIDE != 0
	#define _MAX_FRAGS MAX_FRAGS_OVERRIDE
#else
	#define _MAX_FRAGS MAX_FRAGS
#endif

#define LFB_FRAG_TYPE vec2
#define LFB_FRAG_DEPTH(frag) frag.y

LFB_FRAG_TYPE frags[_MAX_FRAGS];

#if FRAG_SIZE == 1
	#define LFB_GET_FRAG(LFB_NAME) decodeLight(LFB_GET_DATA(LFB_NAME))
	#define LFB_PUT_FRAG(LFB_NAME, frag) LFB_WRITE_DATA(LFB_NAME, encodeLight(floatBitsToUint(frag.x), frag.y))
#elif FRAG_SIZE == 4
	#error I have no idea what I'm doing...
#else
	#define LFB_GET_FRAG(LFB_NAME) LFB_GET_DATA(LFB_NAME)
	#define LFB_PUT_FRAG(LFB_NAME, frag) LFB_WRITE_DATA(LFB_NAME, frag)
#endif


#include "sort.glsl"


void main()
{
	fragColor = vec4(1.0);

#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	// Load into local memory and sort. This will also write to global memory.
	int fragCount = sort(pixel);

#if COMPOSITE && 0
	// Read from global memory and composite.
	for (LFB_ITER_BEGIN(LFB_NAME, pixel); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		LFB_FRAG_TYPE f = LFB_GET_FRAG(LFB_NAME);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif

#if COMPOSITE_LOCAL && 0
	#if _MAX_FRAGS <= LFB_BLOCK_SIZE // Dirty hack to account for rbs not sorting in place.
	for (int i = fragCount - 1; i >= 0; i--)
	{
		LFB_FRAG_TYPE f = frags[i];
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
	#endif
#endif
}

