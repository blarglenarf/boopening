#version 430

#define FRAG_SIZE 2
#define INDEX_BY_TILE 0

#import "lfb_LL"

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#if FRAG_SIZE == 1
	LFB_DEC_LL_1(LFB_NAME);
#elif FRAG_SIZE == 4
	LFB_DEC_LL_4(LFB_NAME);
#else
	LFB_DEC_LL_2(LFB_NAME);
#endif

uniform int interval;

#define DEBUG 0

#if DEBUG
	out vec4 fragColor;
#endif

void main()
{
#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif
	int fragCount = int(LFB_COUNT_AT_LL(LFB_NAME, pixel));

#if DEBUG
	if (fragCount == 0)
		fragColor = vec4(0.5, 0.0, 0.0, 1.0);
	else if (fragCount <= interval)
		fragColor = vec4(debugColLog(fragCount), 1.0);
	else
		fragColor = vec4(debugColLog(fragCount), 1.0);
#else
	if (fragCount <= interval)
		discard;
#endif
}

