#version 430

#define VOXEL_LIST 0

#if VOXEL_LIST
	readonly buffer VoxelList
	{
		vec4 voxelList[];
	};
#else
	readonly buffer VoxelGrid
	{
		uint voxelGrid[];
	};
#endif

uniform int dim;
uniform int startIndex;

flat out vec3 voxelPos;
flat out vec3 voxelCol;

#if !VOXEL_LIST
	flat out uint exists;
#endif

#include "voxelIndex.glsl"

#include "../utils.glsl"

void main()
{
	vec4 voxel = vec4(0);
	vec4 col = vec4(0);

#if VOXEL_LIST
	uint index = gl_VertexID;

	voxel = voxelList[gl_VertexID];
	col = floatToRGBA8(voxel.w);
#else
	int index = gl_VertexID;
	int dataIndex = (startIndex + index) * 4;

	uint r = voxelGrid[dataIndex + 0];
	uint g = voxelGrid[dataIndex + 1];
	uint b = voxelGrid[dataIndex + 2];
	uint c = voxelGrid[dataIndex + 3];

	if (c != 0)
	{
		r /= c;
		g /= c;
		b /= c;
		col = vec4(float(r) / 255.0, float(g) / 255.0, float(b) / 255.0, 1);

		ivec3 coord = getCoord(index, dim);
		voxel.xyz = vec3(coord.x, coord.y, coord.z);

		exists = 1;
	}
	else
		exists = 0;
#endif

	voxelCol = col.xyz;
	voxelPos = voxel.xyz;
}

