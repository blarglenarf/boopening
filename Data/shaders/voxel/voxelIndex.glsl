#ifndef VOXEL_INDEX
#define VOXEL_INDEX

#if 0

int getIndex(int x, int y, int z, int width, int depth)
{
	int index = (y * width * depth + x * depth + z);
	return index;
}

ivec3 getCoord(int index, int width, int depth)
{
	int x = (index / depth) % width;
	int y = index / (width * depth);
	int z = index % depth;

	return ivec3(x, y, z);
}

#else

int getLevel(int dim)
{
	// Not sure if log or if statements are faster.
#if 0
	// dim 2   is level 0
	// dim 4   is level 1
	// dim 8   is level 2
	// dim 16  is level 3
	// dim 32  is level 4
	// dim 64  is level 5
	// dim 128 is level 6
	// dim 256 is level 7
	if (dim == 2)   return 0;
	if (dim == 4)   return 1;
	if (dim == 8)   return 2;
	if (dim == 16)  return 2;
	if (dim == 32)  return 2;
	if (dim == 64)  return 2;
	if (dim == 128) return 2;
	if (dim == 256) return 2;
#else
	return int(log2(dim)) - 1;
#endif
}

int getDim(int level)
{
	return int(pow(2, (level + 1)));
}

int getStartIndex(int level)
{	
	int startIndex = 0;
	int lvlDim = 2;
	for (int i = 0; i < level; i++)
	{
		startIndex += (lvlDim * lvlDim * lvlDim);
		lvlDim *= 2;
	}
	return startIndex;
}

int getIndex(int x, int y, int z, int dim)
{
#if 0
	//int dim = getDim(level);
	//int startIndex = getStartIndex(level);
	int index = (y * dim * dim + x * dim + z);
	//return startIndex + index;
	return index;
#endif
	int index = (y * dim * dim + x * dim + z);
	return index;
}

// index is from the start of this level, no startIndex needed.
ivec3 getCoord(int index, int dim)
{
	//int dim = getDim(level);
	int x = (index / dim) % dim;
	int y = index / (dim * dim);
	int z = index % dim;
	return ivec3(x, y, z);
}

#endif


#endif

