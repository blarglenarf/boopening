// In case I want to try conservative rasterization... why I do.

#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix; // Don't use this, we need the normal in world space.

in VertexData
{
	vec3 wsFrag;
	vec3 wsNorm;
} VertexIn[3];

out VoxelData
{
	vec3 wsFrag;
	vec3 wsNorm;
	flat vec3 aabbMin;
	flat vec3 aabbMax;
} VertexOut;


void updateAABB(vec3 val, inout vec3 aabbMin, inout vec3 aabbMax)
{
	aabbMin.x = min(aabbMin.x, val.x);
	aabbMin.y = min(aabbMin.y, val.y);
	aabbMin.z = min(aabbMin.z, val.z);
	aabbMax.x = max(aabbMax.x, val.x);
	aabbMax.y = max(aabbMax.y, val.y);
	aabbMax.z = max(aabbMax.z, val.z);
}


void main()
{
	// Can't rely on the incoming vertex normal, have to calculate a proper polygonal normal.
	vec3 osNorm = normalize(cross(VertexIn[0].wsFrag - VertexIn[1].wsFrag, VertexIn[2].wsFrag - VertexIn[1].wsFrag));

	// Which axis does the primitive face?
	vec3 xAxis = vec3(1, 0, 0);
	vec3 yAxis = vec3(0, 1, 0);
	vec3 zAxis = vec3(0, 0, 1);

	// Swizzle the components based on which dot product is the largest.
	float dpX = abs(dot(xAxis, osNorm));
	float dpY = abs(dot(yAxis, osNorm));
	float dpZ = abs(dot(zAxis, osNorm));

	// X-axis major, swap x and z.
	bool xFlag = dpX > dpY && dpX > dpZ;

	// Y-axis major, swap y and z.
	bool yFlag = dpY > dpX && dpY > dpZ;

	vec3 aabbMin = VertexIn[0].wsFrag;
	vec3 aabbMax = VertexIn[0].wsFrag;
	for (int i = 0; i < gl_in.length(); i++)
		updateAABB(VertexIn[i].wsFrag, aabbMin, aabbMax);

	for (int i = 0; i < gl_in.length(); i++)
	{
		vec4 osVert = vec4(VertexIn[i].wsFrag, 1.0);
		VertexOut.wsFrag = osVert.xyz;
		VertexOut.wsNorm = osNorm;
		VertexOut.aabbMin = aabbMin;
		VertexOut.aabbMax = aabbMax;

		// Note: This assumes the voxel geometry is being rendered along the z-axis.
		if (xFlag)
			osVert.xyzw = osVert.zyxw;
		else if (yFlag)
			osVert.xyzw = osVert.xzyw;

		vec4 esVert = mvMatrix * osVert;
		vec4 csVert = pMatrix * esVert;
		gl_Position = csVert;
		EmitVertex();
	}
	EndPrimitive();
}

