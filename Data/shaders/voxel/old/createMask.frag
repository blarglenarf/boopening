#version 430

#include "voxelMask.glsl"

VOXEL_DEC(gridMask, uint);

in VoxelData
{
	vec3 wsFrag;
	vec3 wsNorm;
	flat vec3 aabbMin;
	flat vec3 aabbMax;
} VertexIn;

out vec4 fragColor;

void main()
{
	// Conservative rasterization may trigger more hits than necessary, check the voxel aabb against triangle aabb.
	vec3 voxelSize = VOXEL_SIZE(gridMask);
	vec3 voxelMin = VertexIn.wsFrag - voxelSize;
	vec3 voxelMax = VertexIn.wsFrag + voxelSize;

	if ((VertexIn.aabbMax.x - voxelMin.x) * (VertexIn.aabbMin.x - voxelMax.x) >= 0.0 ||
		(VertexIn.aabbMax.y - voxelMin.y) * (VertexIn.aabbMin.y - voxelMax.y) >= 0.0 ||
		(VertexIn.aabbMax.z - voxelMin.z) * (VertexIn.aabbMin.z - voxelMax.z) >= 0.0)
	{
		discard;
		return;
	}

	//ADD_VOXEL(gridMask, VertexIn.wsFrag);
	vec3 wsFrag = VertexIn.wsFrag;
	vec3 pos = ((wsFrag - wsMin) * vec3(size) / (wsMax - wsMin));

	// Find start node.
	uvec3 maxPos = uvec3(size, size, size);
	uvec3 midPos = maxPos / 2;
	uvec3 minPos = uvec3(0, 0, 0);
	uint currIndex = 0;

	// Fill all affected nodes in all levels.
	for (int level = 0; level < nLevels; level++)
	{
		if (pos.y >= midPos.y)
		{
			currIndex += 4;
			minPos.y = midPos.y;
		}
		else
			maxPos.y = midPos.y;
		if (pos.x >= midPos.x)
		{
			currIndex += 2;
			minPos.x = midPos.x;
		}
		else
			maxPos.x = midPos.x;
		if (pos.z >= midPos.z)
		{
			currIndex += 1;
			minPos.z = midPos.z;
		}
		else
			maxPos.z = midPos.z;
		midPos = minPos + ((maxPos - minPos) / 2);
	#if 1
		VOXEL_SET(gridMask, currIndex);
	#else
		gridMasks[currIndex] = 1;
	#endif
		currIndex = (currIndex + 1) * 8;
	}

	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
	discard;
}

