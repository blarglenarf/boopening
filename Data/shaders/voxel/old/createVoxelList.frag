#version 430

#define USE_MASK 0

uniform layout(binding = 0, offset = 0) atomic_uint VoxelListCount;

buffer VoxelListData
{
	uint voxelListData[];
};

#if USE_MASK
	buffer VoxelGridMask
	{
		uint gridMasks[];
	};
#endif

uniform uint voxelListAlloc;


in VoxelData
{
	vec3 wsFrag;
	vec3 wsNorm;
	flat vec3 aabbMin;
	flat vec3 aabbMax;
} VertexIn;

uniform int size;

uniform vec3 wsMin;
uniform vec3 wsMax;

out vec4 fragColor;

void main()
{
	// Conservative rasterization may trigger more hits than necessary, check the voxel aabb against triangle aabb.
	vec3 voxelSize = (wsMax - wsMin) / (float(size) / 2.0);
	vec3 voxelMin = VertexIn.wsFrag - voxelSize;
	vec3 voxelMax = VertexIn.wsFrag + voxelSize;

	if ((VertexIn.aabbMax.x - voxelMin.x) * (VertexIn.aabbMin.x - voxelMax.x) >= 0.0 ||
		(VertexIn.aabbMax.y - voxelMin.y) * (VertexIn.aabbMin.y - voxelMax.y) >= 0.0 ||
		(VertexIn.aabbMax.z - voxelMin.z) * (VertexIn.aabbMin.z - voxelMax.z) >= 0.0)
	{
		discard;
		return;
	}

	// Need the voxel space coord (between min/max world space).
	vec3 voxelCoord = (VertexIn.wsFrag - wsMin) * vec3(size) / (wsMax - wsMin);

#if USE_MASK
	// TODO: May want to try multiple resolution levels.
	int maskSize = size / 32;
	int voxelIndex = int(voxelCoord.y) * size * maskSize + int(voxelCoord.x) * maskSize;

	// Set mask for cluster to 1 (has to be done atomically), if it's already 1 don't bother.
	int maskIndex = int(voxelCoord.z) / 32;
	int clusterIndex = int(voxelCoord.z) % 32;

	uint gridMask = 1 << clusterIndex;
	if (gridMask == 0)
		return;

	uint currMask = gridMasks[voxelIndex + maskIndex];
	if ((currMask & gridMask) == 0)
	{
		uint prevMask = atomicOr(gridMasks[voxelIndex + maskIndex], gridMask);

		// If prevMask already had the bit set, then no need to do anything.
		if ((prevMask & gridMask) != 0)
			return;
	}
	else
		return;
#endif

	// Just add the position to the list.
	uint currVoxel = atomicCounterIncrement(VoxelListCount);
	if (currVoxel < voxelListAlloc)
	{
		// From here, can either store a voxel index, or the actual voxel position.
		//int voxelIndex = int(voxelCoord.y) * size * size + int(voxelCoord.x) * size;
		uint voxelPos = 0;
		uvec3 uCoord = uvec3(uint(voxelCoord.x), uint(voxelCoord.y), uint(voxelCoord.z));
		voxelPos = ((uCoord.x << 22U) | (uCoord.y << 12U) | (uCoord.z << 2U));

		voxelListData[currVoxel] = voxelPos;
	}
#if 0
	// Need to add to all levels of the tree.
	int startIndex = 0;

	// Root starts at size / 32.
	for (int level = 32; level >= 1; level /= 2)
	{
		int levelSize = size / level;
		int maskSize = levelSize / 32;

		// Need the voxel space coord (between min/max world space).
		vec3 voxelCoord = (VertexIn.wsFrag - wsMin) * vec3(levelSize) / (wsMax - wsMin);
		//vec3 voxelCoord = coord / vec3(level);

		// Get voxel index.
		int voxelIndex = int(voxelCoord.y) * levelSize * maskSize + int(voxelCoord.x) * maskSize;

		// Set mask for cluster to 1 (has to be done atomically), if it's already 1 don't bother.
		int maskIndex = int(voxelCoord.z) / 32;
		int clusterIndex = int(voxelCoord.z) % 32;

		uint gridMask = 1 << clusterIndex;

		if (gridMask == 0)
		{
			startIndex += (levelSize * levelSize * maskSize);
			continue;
		}
		else if ((gridMasks[startIndex + voxelIndex + maskIndex] & gridMask) == 0)
		{
			atomicOr(gridMasks[startIndex + voxelIndex + maskIndex], gridMask);
		}

		startIndex += (levelSize * levelSize * maskSize);
	}
#endif
	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
	discard;
}

