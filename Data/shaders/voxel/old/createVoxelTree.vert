#version 430

uniform layout(binding = 0, offset = 0) atomic_uint VoxelNodeCount;

readonly buffer VoxelListData
{
	uint voxelListData[];
};

coherent buffer VoxelNodeData
{
	uint voxelNodeData[];
};

uniform uint voxelNodeAlloc;

uniform int size;

uniform bool flagging;

uniform int nLevels;

uniform int prevAllocated;

uniform vec3 wsMin;
uniform vec3 wsMax;



int traverseNodes(uvec3 pos, in int level)
{
	// Find start node.
	uvec3 maxPos = uvec3(size, size, size);
	uvec3 midPos = maxPos / 2;
	uvec3 minPos = uvec3(0, 0, 0);

	int currIndex = 0;
	int currLevel = 1;

	// What side of the split on x/y/z are we on? Adjust index accordingly.
	if (pos.y >= midPos.y)
	{
		currIndex += 4;
		minPos.y = midPos.y;
	}
	else
		maxPos.y = midPos.y;

	if (pos.x >= midPos.x)
	{
		currIndex += 2;
		minPos.x = midPos.x;
	}
	else
		maxPos.x = midPos.x;

	if (pos.z >= midPos.z)
	{
		currIndex += 1;
		minPos.z = midPos.z;
	}
	else
		maxPos.z = midPos.z;
	midPos = minPos + ((maxPos - minPos) / 2);

	// If currIndex >= voxelNodeAlloc then we need to allocate more data.
	if (currIndex >= voxelNodeAlloc)
		return -1;

	// Follow nodes until we hit null or marked for creation (or we hit the end).
	while (voxelNodeData[currIndex] != 0 && voxelNodeData[currIndex] != 1 && currLevel < nLevels)
	{
		currIndex = int(voxelNodeData[currIndex]);
		if (currIndex >= voxelNodeAlloc)
			return -1;

		if (pos.y >= midPos.y)
		{
			currIndex += 4;
			minPos.y = midPos.y;
		}
		else
			maxPos.y = midPos.y;

		if (pos.x >= midPos.x)
		{
			currIndex += 2;
			minPos.x = midPos.x;
		}
		else
			maxPos.x = midPos.x;

		if (pos.z >= midPos.z)
		{
			currIndex += 1;
			minPos.z = midPos.z;
		}
		else
			maxPos.z = midPos.z;
		midPos = minPos + ((maxPos - minPos) / 2);
		currLevel++;

		// If currIndex >= voxelNodeAlloc, then we need to allocate more data.
		if (currIndex >= voxelNodeAlloc)
			return -1;
	}

	// Note: If currLevel != nLevels, then we didn't reach the end of the tree.
	level = currLevel;

	// Final position is aabb between min and max.
	// TODO: Not sure what to do with this info yet.
	uvec3 aabbMin = minPos;
	uvec3 aabbMax = maxPos;

	return currIndex;
}


void main()
{
	// First value is NULL.
	int id = gl_VertexID + 1;

	// Just flag the appropriate voxel nodes as active.
	if (flagging)
	{
		// Get voxel coord.
		uint uPos = voxelListData[id];
		uvec3 pos = uvec3((uPos >> 22U) & 1023, (uPos >> 12U) & 1023, (uPos >> 2U) & 1023);

		// Traverse the tree until we find an empty node surrounding the given position.
		int level = 0;
		int currIndex = traverseNodes(pos, level);

		// If there wasn't enough room, ignore.
		if (currIndex == -1)
			return;

		// Otherwise, mark the node as needing to be created.
		voxelNodeData[currIndex] = 1;
	}

	// Create new voxel nodes based on nodes marked as active, and link their parents to them.
	else
	{
		int nodePos = prevAllocated + gl_VertexID;

		// If node isn't marked, or doesn't exist, ignore it.
		if (nodePos >= voxelNodeAlloc || voxelNodeData[nodePos] != 1)
			return;

		// Node is marked, make a new node.
		uint newNode = atomicCounterIncrement(VoxelNodeCount);
		if (newNode * 8 >= voxelNodeAlloc)
			return;
		voxelNodeData[nodePos] = newNode * 8;
	}
}

