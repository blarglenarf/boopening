#ifndef VOXEL_MASK_GLSL
#define VOXEL_MASK_GLSL

#define VOXEL_DEC(name, type) \
	coherent buffer GridMasks \
	{ \
		type gridMasks[]; \
	}; \
	\
	uniform vec3 wsMin; \
	uniform vec3 wsMax; \
	\
	uniform int size; \
	uniform int nLevels;

// FIXME: The / 2.0 may not be necessary.
#define VOXEL_SIZE(name) ((wsMax - wsMin) / (float(size) / 2.0))


// Convert world space to voxel space.
#define LEVEL_SIZE(level) int(pow(2, level + 1))
#define VOXEL_COORD_SIZE(name, wsFrag, levelSize) ((wsFrag - wsMin) * vec3(levelSize) / (wsMax - wsMin))

#define VOXEL_COORD(name, wsFrag) VOXEL_COORD_SIZE(name, wsFrag, size)

// Convert index to voxel bit, voxel int, and voxel mask.
#define VOXEL_INT(name, index) ((index) / 32)
#define VOXEL_BIT(name, index) (31 - ((index) % 32))
#define VOXEL_MASK(name, index) (1 << VOXEL_BIT(name, (index)))

#if 0

	#define VOXEL_GET(name, index) gridMasks[(index)]
	#define VOXEL_SET(name, index) (gridMasks[(index)] = 1)

#else

	// Getting and setting voxels.
	#define VOXEL_GET(name, index) (gridMasks[VOXEL_INT(name, index)] & VOXEL_MASK(name, (index)))

	#define VOXEL_SET(name, index) \
		{ \
			uint mask = VOXEL_MASK(name, (index)); \
			if (mask != 0 && (VOXEL_GET(name, (index)) == 0)) \
				atomicOr(gridMasks[VOXEL_INT(name, (index))], mask); \
		}

#endif

// TODO: Simple voxel checking.


#if 0

#define LEVEL_SIZE(level) int(pow(2, level + 1))
#define MASK_SIZE(levelSize) (levelSize / 32)
#define MASK_INDEX(voxelCoord) (int(voxelCoord.z) / 32)
#define CLUSTER_INDEX(voxelCoord) (int(voxelCoord.z) % 32)
#define GRID_MASK(clusterIndex) (1 << clusterIndex)


// FIXME: THIS IS ALL WRONG!!!!!!!
#define VOXEL_COORD(name, levelSize, wsFrag) ((wsFrag - wsMin) * vec3(levelSize) / (wsMax - wsMin))
#define VOXEL_INDEX(levelSize, maskSize, voxelCoord) (int(voxelCoord.y) * levelSize * maskSize + int(voxelCoord.x) * maskSize)

#define ADD_VOXEL(name, wsFrag) \
	{ \
		int startIndex = 0; \
		for (int level = 4; level < nLevels; level++) \
		{ \
			int levelSize = LEVEL_SIZE(level); \
			int maskSize = MASK_SIZE(levelSize); \
			\
			vec3 voxelCoord = VOXEL_COORD(name, levelSize, wsFrag); \
			int voxelIndex = VOXEL_INDEX(levelSize, maskSize, voxelCoord); \
			\
			int maskIndex = MASK_INDEX(voxelCoord); \
			int clusterIndex = CLUSTER_INDEX(voxelCoord); \
			uint gridMask = GRID_MASK(clusterIndex); \
			\
			if (gridMask == 0) \
			{ \
				startIndex += (levelSize * levelSize * maskSize); \
				continue; \
			} \
			else if ((gridMasks[startIndex + voxelIndex + maskIndex] & gridMask) == 0) \
			{ \
				atomicOr(gridMasks[startIndex + voxelIndex + maskIndex], gridMask); \
			} \
			\
			startIndex += (levelSize * levelSize * maskSize); \
		} \
	}


#if 0
void addVoxel(vec3 wsPos)
{
	// Need to add to all levels of the tree.
	int startIndex = 0;

#if 0
	// Root starts at size / 32.
	for (int level = 32; level >= 1; level /= 2)
	{
		int levelSize = size / level;
		int maskSize = levelSize / 32;

		// Need the voxel space coord (between min/max world space).
		vec3 voxelCoord = (VertexIn.wsFrag - wsMin) * vec3(levelSize) / (wsMax - wsMin);
		//vec3 voxelCoord = coord / vec3(level);

		// Get voxel index.
		int voxelIndex = int(voxelCoord.y) * levelSize * maskSize + int(voxelCoord.x) * maskSize;

		// Set mask for cluster to 1 (has to be done atomically), if it's already 1 don't bother.
		int maskIndex = int(voxelCoord.z) / 32;
		int clusterIndex = int(voxelCoord.z) % 32;

		uint gridMask = 1 << clusterIndex;

		if (gridMask == 0)
		{
			startIndex += (levelSize * levelSize * maskSize);
			continue;
		}
		else if ((gridMasks[startIndex + voxelIndex + maskIndex] & gridMask) == 0)
		{
			atomicOr(gridMasks[startIndex + voxelIndex + maskIndex], gridMask);
		}

		startIndex += (levelSize * levelSize * maskSize);
	}
#else
	// Alternative way of doing the same thing as above.
	// TODO: At each level, populate list of unique positions. For faster zero'ing and for tree building.
	for (int level = 4; level < nLevels; level++)
	{
		int levelSize = int(pow(2, level + 1));
		int maskSize = levelSize / 32;

		// Need the voxel space coord (between min/max world space).
		vec3 voxelCoord = (VertexIn.wsFrag - wsMin) * vec3(levelSize) / (wsMax - wsMin);

		// Get voxel index.
		int voxelIndex = int(voxelCoord.y) * levelSize * maskSize + int(voxelCoord.x) * maskSize;

		// Set mask for cluster to 1 (has to be done atomically), if it's already 1 don't bother.
		int maskIndex = int(voxelCoord.z) / 32;
		int clusterIndex = int(voxelCoord.z) % 32;

		uint gridMask = 1 << clusterIndex;

		if (gridMask == 0)
		{
			startIndex += (levelSize * levelSize * maskSize);
			continue;
		}
		else if ((gridMasks[startIndex + voxelIndex + maskIndex] & gridMask) == 0)
		{
			atomicOr(gridMasks[startIndex + voxelIndex + maskIndex], gridMask);
		}

		startIndex += (levelSize * levelSize * maskSize);
	}
#endif
}
#endif

#endif

#endif

