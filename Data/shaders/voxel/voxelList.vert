#version 430

// Do we count or create?
#define COUNT 0

uniform layout(binding = 0, offset = 0) atomic_uint voxelCount;

readonly buffer VoxelGrid
{
	uint voxelGrid[];
};

#if !COUNT
	coherent buffer VoxelList
	{
		vec4 voxelList[];
	};
#endif


uniform int dim;
uniform int startIndex;

#include "voxelIndex.glsl"

#include "../utils.glsl"


void main()
{
	uint index = (startIndex + gl_VertexID) * 4;

	uint r = voxelGrid[index + 0];
	uint g = voxelGrid[index + 1];
	uint b = voxelGrid[index + 2];
	uint c = voxelGrid[index + 3];

	if (c == 0)
		return;

#if COUNT
	atomicCounterIncrement(voxelCount);
#else
	r /= c;
	g /= c;
	b /= c;

	vec4 voxelCol = vec4(float(r) / 255.0, float(g) / 255.0, float(b) / 255.0, 1);
	float col = rgba8ToFloat(voxelCol);

	// Pretty sure this is the right order.
	//ivec3 coord = getCoord(gl_VertexID, size.x, size.z);
	ivec3 coord = getCoord(gl_VertexID, dim);

	// TODO: Do we want to convert to object space here or not?
	vec3 voxelPos = vec3(coord.x, coord.y, coord.z);

	// Store voxel in the list.
	uint listIndex = atomicCounterIncrement(voxelCount);
	voxelList[listIndex] = vec4(voxelPos, col);
#endif
}

