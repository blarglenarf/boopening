#version 430

coherent buffer VoxelGrid
{
	uint voxelGrid[];
};

uniform int startIndex;
uniform int dim;

#include "voxelIndex.glsl"

#include "../utils.glsl"

void main()
{
	int index = gl_VertexID;
	ivec3 coord = getCoord(index, dim);

	int childDim = dim * 2;

	uint totalCols = 0;
	uint totalR = 0;
	uint totalG = 0;
	uint totalB = 0;

	// What are the child's coordinates?
	coord.x *= 2;
	coord.y *= 2;
	coord.z *= 2;

	// Access the 8 child voxels of this voxel.
	for (int x = coord.x; x <= coord.x + 1; x++)
	{
		for (int y = coord.y; y <= coord.y + 1; y++)
		{
			for (int z = coord.z; z <= coord.z + 1; z++)
			{
				//ivec3 childCoord = ivec3(x, y, z);
				int childIndex = getIndex(x, y, z, childDim);
				int startChildIndex = startIndex + (dim * dim * dim);

				int totalIndex = (childIndex + startChildIndex) * 4;
				uint c = voxelGrid[totalIndex + 3]; // Don't assume the colours have been divided.

				if (c == 0)
					continue;
				else
				{
					uint r = voxelGrid[totalIndex + 0];
					uint g = voxelGrid[totalIndex + 1];
					uint b = voxelGrid[totalIndex + 2];

					// Get the average colour of the 8 children.
					totalCols += 1;
					totalR += (r / c);
					totalG += (g / c);
					totalB += (b / c);
				}
			}
		}
	}

	// Store that colour in this voxel.
	voxelGrid[(startIndex + index) * 4 + 0] = totalR;
	voxelGrid[(startIndex + index) * 4 + 1] = totalG;
	voxelGrid[(startIndex + index) * 4 + 2] = totalB;
	voxelGrid[(startIndex + index) * 4 + 3] = totalCols;
}

