#version 450

#define VOXEL_LIST 0

layout(points) in;
layout(triangle_strip, max_vertices = 36) out;


flat in vec3 voxelPos[1];
flat in vec3 voxelCol[1];

#if !VOXEL_LIST
	flat in uint exists[1];
#endif

out vec3 color;


uniform mat4 mvMatrix;
uniform mat4 pMatrix;

uniform int dim;


#define EMIT_FACE(bl, br, tl, tr) \
	color = voxelCol[0]; gl_Position = tl; EmitVertex(); \
	color = voxelCol[0]; gl_Position = bl; EmitVertex(); \
	color = voxelCol[0]; gl_Position = br; EmitVertex(); \
	EndPrimitive(); \
	color = voxelCol[0]; gl_Position = tl; EmitVertex(); \
	color = voxelCol[0]; gl_Position = br; EmitVertex(); \
	color = voxelCol[0]; gl_Position = tr; EmitVertex(); \
	EndPrimitive();


void main()
{
	color = voxelCol[0];

#if !VOXEL_LIST
	if (exists[0] == 0)
	{
		for (int i = 0; i < 6; i++)
		{
			EMIT_FACE(vec4(0), vec4(0), vec4(0), vec4(0));
		}
		return;
	}
#endif

	// At the moment I'm just going to assume that the scene is from -15 to 15 on xyz.
	vec3 osPos = vec3(	float(voxelPos[0].x * 30.0) / float(dim) - 15.0,
						float(voxelPos[0].y * 30.0) / float(dim) - 15.0,
						float(voxelPos[0].z * 30.0) / float(dim) - 15.0);

	// I guess each voxel is of size 30 / dim.
	float voxSize = (30.0 / float(dim)) / 2.0;

	// Not sure why this is necessary but whatevs.
	osPos.xyz += vec3(30.0 / vec3(dim)) / 2.0;

	// FIXME: Some of these faces are incorrect...

	// Eye-space corners.
	vec4 fBL = mvMatrix * vec4(osPos + vec3(-voxSize, -voxSize, -voxSize), 1);
	vec4 fBR = mvMatrix * vec4(osPos + vec3( voxSize, -voxSize, -voxSize), 1);
	vec4 fTL = mvMatrix * vec4(osPos + vec3(-voxSize,  voxSize, -voxSize), 1);
	vec4 fTR = mvMatrix * vec4(osPos + vec3( voxSize,  voxSize, -voxSize), 1);

	vec4 bBL = mvMatrix * vec4(osPos + vec3(-voxSize, -voxSize,  voxSize), 1);
	vec4 bBR = mvMatrix * vec4(osPos + vec3( voxSize, -voxSize,  voxSize), 1);
	vec4 bTL = mvMatrix * vec4(osPos + vec3(-voxSize,  voxSize,  voxSize), 1);
	vec4 bTR = mvMatrix * vec4(osPos + vec3( voxSize,  voxSize,  voxSize), 1);

	// Clip-space corners.
	vec4 csFBL = pMatrix * fBL;
	vec4 csFBR = pMatrix * fBR;
	vec4 csFTL = pMatrix * fTL;
	vec4 csFTR = pMatrix * fTR;

	vec4 csBBL = pMatrix * bBL;
	vec4 csBBR = pMatrix * bBR;
	vec4 csBTL = pMatrix * bTL;
	vec4 csBTR = pMatrix * bTR;

	// Now create all 6 faces, with two triangles of 6 vertices per-face.
	EMIT_FACE(csFBR, csFBL, csFTR, csFTL); // Front.
	EMIT_FACE(csBBL, csBBR, csBTL, csBTR); // Back.
	EMIT_FACE(csFTR, csFTL, csBTR, csBTL); // Top.
	EMIT_FACE(csFBL, csFBR, csBBL, csBBR); // Bottom.
	EMIT_FACE(csBBR, csFBR, csBTR, csFTR); // Right.
	EMIT_FACE(csFBL, csBBL, csFTL, csBTL); // left.
}

