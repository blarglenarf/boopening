#version 430

//#define DEPTH_ONLY 0
#define MAX_EYE_Z 30.0

in vec2 texCoord;

uniform sampler2D depthTex;

out vec4 fragColor;


void main()
{
	//vec4 depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0);
	vec4 depth = texture2D(depthTex, texCoord);

//#if DEPTH_ONLY
//	depth = vec4(-depth.x, -depth.x, -depth.x, 1.0);
//#else
	depth.z = -depth.z;
//#endif

	fragColor = vec4((MAX_EYE_Z - depth.xyz) / MAX_EYE_Z, 1.0);
}

