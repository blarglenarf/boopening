#version 430

in vec2 texCoord;

uniform sampler2D normTex;

out vec4 fragColor;


void main()
{
	//vec4 norm = texelFetch(normTex, ivec2(gl_FragCoord.xy), 0);
	vec4 norm = texture2D(normTex, texCoord);

	fragColor = vec4(norm.rgb, 1.0);
}

