#version 430

#define DEPTH_ONLY 1

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec3 osFrag;
	vec2 texCoord;
	mat3 tbn;
} VertexIn;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform bool normFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

uniform uint startIndex;


#if DEPTH_ONLY
	out vec4 fragColor[4];
#else
	out vec4 fragColor[5];
#endif


void main()
{
	vec3 normal = normalize(VertexIn.normal);

	if (normFlag)
	{
		normal = texture2D(normalTex, VertexIn.texCoord).rgb;
		normal = normalize(normal * 2.0 - 1.0);
		normal = normalize(VertexIn.tbn * normal);
	}

	// Write materials.
	// FIXME: This isn't really sufficient for handling diffuse+specular+shininess...
	vec4 ambient = Material.ambient; // FIXME: May need to use Material.diffuse instead...
	if (texFlag)
	{
		vec4 texColor = texture2D(diffuseTex, VertexIn.texCoord);
		ambient = vec4(texColor.xyz, texColor);
	}
	if (ambient.w == 0) // FIXME: Need to do something better than this.
	{
		discard;
		return;
	}

	// Write depths.
	fragColor[0] = vec4(VertexIn.esFrag.z, 0.0, 0.0, 0.0);

	// Write normals.
	fragColor[1] = vec4(normal, 0.0);

	fragColor[2] = ambient;

	// Write polygon ids.
	fragColor[3] = uvec4(startIndex / 3 + uint(gl_PrimitiveID), 0.0, 0.0, 0.0);

#if !DEPTH_ONLY
	// Write positions (do we want eye space or object space for this).
	fragColor[4] = vec4(VertexIn.esFrag.x, VertexIn.esFrag.y, VertexIn.esFrag.z, 0.0);
#endif
}

