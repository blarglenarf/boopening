#version 430

uniform sampler2D materialTex;

out vec4 fragColor;


void main()
{
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);

	fragColor = vec4(material.xyz * 0.4, material.w);
}

