#version 430

#define LIGHT_DRAW 0
#define CLUSTER_CULL 0
#define USE_CLUSTER 0
#define STORE_LIGHT_DATA 0

#if CLUSTER_CULL && !USE_CLUSTER
	#define CLUSTERS 32
#endif

#define MASK_SIZE 1
#define MAX_EYE_Z 30.0

#include "../utils.glsl"


#if USE_CLUSTER
	#import "cluster"

	#if STORE_LIGHT_DATA
		CLUSTER_DEC(cluster, vec2);
	#else
		CLUSTER_DEC(cluster, uint);
	#endif
#else
	#import "lfb_L"

	LFB_DEC(light, vec2);
#endif


#if LIGHT_DRAW
	readonly buffer LightColors
	{
		float lightColors[];
	};
#endif

#if CLUSTER_CULL
	buffer ClusterMasks
	{
		uint clusterMasks[];
	};
#endif

in VertexData
{
	flat vec4 esLightPos;
	flat vec4 csLightPos;
	flat uint id;
	flat float radius;
	flat vec4 dir;
	flat float color;
} VertexIn;


uniform ivec4 viewport;
uniform mat4 invPMatrix;
uniform mat4 pMatrix;

uniform ivec2 size;

out vec4 fragColor;

#if CLUSTER_CULL
	bool checkBit(int fragIndex, int cluster)
	{
		int maskIndex = cluster / (MASK_SIZE * 32);
		int clusterIndex = cluster % 32;
		return (clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
	}
#endif

#if CLUSTER_CULL && !USE_CLUSTER
	int getFragIndex()
	{
		ivec2 gridCoord = ivec2(gl_FragCoord.xy);
		return LFB_GET_SIZE(light).x * gridCoord.y + gridCoord.x;
	}

	int getCluster(float depth)
	{
		return max(0, min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1)))));
	}

	bool checkBits(float minZ, float maxZ)
	{
		int fragIndex = getFragIndex();

		int cluster1 = getCluster(minZ);
		int cluster2 = getCluster(maxZ);

		for (int cluster = cluster1; cluster <= cluster2; cluster++)
			if (checkBit(fragIndex, cluster))
				return true;
		return false;
	}
#endif



// Store two fragments, one for front and one for back.
void main()
{
	fragColor = vec4(1);

	// Light screen space position.
	vec2 ssPos = gl_FragCoord.xy;
	vec2 ssLightPos = vec2(0, 0);

	// If we're a sphere, we need to pick the point in the tile closest to the centre.
	vec3 esLightPos = VertexIn.esLightPos.xyz;
	vec4 csLightPos = VertexIn.csLightPos;
	float radius = VertexIn.radius;

#if 0
	// If we're a cone, shift towards the cap or the apex, depending on orientation.
	if (VertexIn.esLightPos.w != 0)
	{
		// Cone is facing toward or away?
		vec3 coneDir = normalize(VertexIn.dir.xyz);
		if (dot(coneDir, vec3(0, 0, -1)) < 0)
		{
			// Toward.
			vec3 cap = VertexIn.esLightPos.xyz;
			cap += (coneDir * VertexIn.radius);
			csLightPos = pMatrix * vec4(cap, 1.0);
		}
	}
#else
	#if 0
	if (VertexIn.esLightPos.w != 0)
	{
		// XXX: Cheap hack: If we're a cone, use the centre of the cone, and half its radius.
		// in reality radius is determined by circumscribed circle about a triangle for acute cones.
		// for obtuse cones centre point is centre of cone's cap, radius is half cone's cap length.
		radius *= 0.5;
		esLightPos += normalize(VertexIn.dir.xyz) * radius;
		csLightPos = pMatrix * vec4(esLightPos, 1.0);
		radius *= 1.3;
	}
	#endif
#endif

#if LIGHT_DRAW
	vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -30.0), viewport, invPMatrix).xyz);
#else
	ssLightPos = ((vec2(csLightPos.xy / csLightPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(size.x, size.y);

	// If the ssLightPos is inside this pixel, use it, otherwise push closer to it.
	if (ssLightPos.x < gl_FragCoord.x - 0.5)
		ssPos.x -= 0.5;
	else if (ssLightPos.x > gl_FragCoord.x + 0.5)
		ssPos.x += 0.5;
	else
		ssPos.x = ssLightPos.x;
	if (ssLightPos.y < gl_FragCoord.y - 0.5)
		ssPos.y -= 0.5;
	else if (ssLightPos.y > gl_FragCoord.y + 0.5)
		ssPos.y += 0.5;
	else
		ssPos.y = ssLightPos.y;

	// TODO: If we're outside the radius of the light, do nothing.
	vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -30.0), viewport, invPMatrix).xyz);
	//vec3 dir = normalize(VertexIn.esVert.xyz);
#endif

	float frontDepth, backDepth;

#if 0
	if (!sphereIntersection(esLightPos, radius, dir, frontDepth, backDepth))
	{
		discard;
		return;
	}
#else
	// Sphere.
	if (VertexIn.esLightPos.w == 0)
	{
		if (!sphereIntersection(esLightPos, radius, dir, frontDepth, backDepth))
		{
			discard;
			return;
		}
	}

	// Cone.
	else
	{
		vec3 conePos = esLightPos.xyz;
		vec3 coneDir = normalize(VertexIn.dir.xyz);
		float coneRad = VertexIn.esLightPos.w;
		//conePos = esLightPos + (coneDir * coneRad);

		if (!coneIntersection(conePos, VertexIn.radius, dir, coneDir, VertexIn.dir.w, frontDepth, backDepth))
		// Sphere inersections are faster than cones.
		//if (!sphereIntersection(conePos, coneRad, dir, frontDepth, backDepth))
		{
			//fragColor = vec4(1, 0, 0, 1);
			discard;
			return;
		}
	}
#endif

#if CLUSTER_CULL && !USE_CLUSTER
	if (!checkBits(frontDepth, backDepth))
	{
		discard;
		return;
	}
#endif

#if USE_CLUSTER && !LIGHT_DRAW
	int pixel = CLUSTER_GET_SIZE(cluster).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	int frontCluster = CLUSTER_GET(frontDepth, MAX_EYE_Z);
	int backCluster = CLUSTER_GET(backDepth, MAX_EYE_Z);

	for (int cluster = frontCluster; cluster <= backCluster; cluster++)
	{
	#if CLUSTER_CULL
		if (!checkBit(pixel, cluster))
			continue;
	#endif
		int fragIndex = CLUSTER_GET_INDEX(cluster, cluster, gl_FragCoord.xy);
	#if STORE_LIGHT_DATA
		CLUSTER_ADD_DATA(cluster, fragIndex, vec2(VertexIn.radius, VertexIn.color), vec4(VertexIn.esLightPos.xyz, 1), VertexIn.dir);
	#else
		CLUSTER_ADD_DATA(cluster, fragIndex, VertexIn.id);
	#endif
	}
#else
	int pixel = LFB_GET_SIZE(light).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	#if LIGHT_DRAW	
		vec4 color = floatToRGBA8(lightColors[VertexIn.id]);
		color.w = 0.4;
		float c = rgba8ToFloat(color);
		{ LFB_ADD_DATA(light, pixel, vec2(c, frontDepth)); }
		{ LFB_ADD_DATA(light, pixel, vec2(c, backDepth)); }
	#else
		{ LFB_ADD_DATA(light, pixel, vec2(uintBitsToFloat(VertexIn.id), frontDepth)); }
		{ LFB_ADD_DATA(light, pixel, vec2(uintBitsToFloat(VertexIn.id), backDepth)); }
	#endif
#endif
}

