#version 430

#define STORAGE_BUFFERS 1
#define CONSERVATIVE_RASTER 0

#if STORAGE_BUFFERS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	uniform layout(r32ui) coherent uimageBuffer Offsets;
#endif

#if CONSERVATIVE_RASTER
	in VertexData
	{
		vec3 esVert;
		flat vec4 aabb;
	} VertexIn;
#endif

uniform ivec2 size;

void main()
{
#if CONSERVATIVE_RASTER && 0
	if (!(gl_FragCoord.x >= VertexIn.aabb.x && gl_FragCoord.y >= VertexIn.aabb.y && gl_FragCoord.x <= VertexIn.aabb.z && gl_FragCoord.y <= VertexIn.aabb.w))
	{
		discard;
		return;
	}
#endif

	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

#if STORAGE_BUFFERS
	atomicAdd(offsets[pixel], 1);
#else
	imageAtomicAdd(Offsets, pixel, 1U);
#endif
}

