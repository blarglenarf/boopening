#ifndef VOLUME_LIGHT_GLSL
#define VOLUME_LIGHT_GLSL

#ifndef USE_G_BUFFER
	#define USE_G_BUFFER 0
#endif

vec4 toneMapColor(vec4 hdrColor, float exposure, float gamma)
{
	// Exposure tone mapping.
	//vec3 mapped = hdrColor.xyz / (hdrColor.xyz + vec3(1));
	vec3 mapped = vec3(1.0) - exp(-hdrColor.xyz * exposure);

	// Gamma correction.
	mapped = pow(mapped, vec3(1.0 / gamma));

	return vec4(mapped, hdrColor.w);
}


// Diffuse.
vec3 brdfLambert(vec3 material, vec3 lightColor, float dp)
{
	// Could implement cosine weighted sampling maybe: https://github.com/McNopper/OpenGL/blob/master/Example32/shader/brdf.frag.glsl
	return material * dp * lightColor;
}

// Phong specular.
vec3 brdfPhong(vec3 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal, float shininess)
{
	vec3 reflection = reflect(-lightDir, normal);
	float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
	//float nDotH = abs(dot(viewDir, normalize(reflection)));
	float intensity = pow(nDotH, shininess);
	return intensity * lightColor;
}

// see http://en.wikipedia.org/wiki/Schlick%27s_approximation
float fresnelSchlick(float r0, float vDotH)
{
	return r0 + (1.0 - r0) * pow(1.0 - vDotH, 5.0);
}

// see http://graphicrants.blogspot.de/2013/08/specular-brdf-reference.html
// see http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float geometricShadowingSchlickBeckmann(float nDotV, float k)
{
	return nDotV / (nDotV * (1.0 - k) + k);
}

// see http://graphicrants.blogspot.de/2013/08/specular-brdf-reference.html
// see http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float geometricShadowingSmith(float nDotL, float nDotV, float k)
{
	return geometricShadowingSchlickBeckmann(nDotL, k) * geometricShadowingSchlickBeckmann(nDotV, k);
}

vec3 brdfCookTorrence(vec3 material, vec3 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal, float roughness)
{
	// See https://github.com/McNopper/OpenGL/blob/master/Example32/shader/brdf.frag.glsl
	// basis, N, V, K
	// basis: tangent, bitangent, normal matrix, normal: normal vector, eye: eyeDir vector, k: roughness float

#if 0
	vec3 HtangentSpace = microfacetWeightedSampling(randomPoint);

	// Transform H to world space.
	vec3 H = basis * HtangentSpace;

	// Note: reflect takes incident vector.
	vec3 L = reflect(-V, H);

	float NdotL = dot(N, L);
	float NdotV = dot(N, V);
	float NdotH = dot(N, H);
#endif
	vec3 V = normalize(viewDir); // FIXME: I'm pretty sure this should be negative.
	vec3 H = normalize(reflect(normalize(-lightDir), normalize(normal)));
	vec3 L = normalize(lightDir);//reflect(-viewDir, H); // FIXME: Again, pretty sure this should be negative.

	float nDotL = dot(normal, L);
	float nDotV = dot(normal, V);
	float nDotH = dot(normal, H);

	// Lighted and visible?
	if (nDotL > 0 && nDotV > 0)
	{
		float vDotH = max(0, dot(V, H));

		// Fresnel.
		float refCoefficient = 1.0;
		float F = fresnelSchlick(refCoefficient, vDotH);

		// Geometric Shadowing.
		float G = geometricShadowingSmith(nDotL, nDotV, roughness);

		//
		// Lo = BRDF * L * NdotL / PDF
		//
		// BRDF is D * F * G / (4 * NdotL * NdotV).
		// PDF is D * NdotH / (4 * VdotH).
		// D and 4 * NdotL are canceled out, which results in this formula: Lo = color * L * F * G * VdotH / (NdotV * NdotH)		
		float colorFactor = F * G * vDotH / (nDotV * nDotH);

		// Note: Needed for robustness. With specific parameters, a NaN can be the result.
		if (isnan(colorFactor))
			return vec3(0);

		return material * lightColor * max(0, colorFactor);
		//return texture(u_panoramaTexture, panorama(L)).rgb * colorFactor;
	}

	return vec3(0);
}


#if USE_G_BUFFER

	vec4 getAmbient(vec4 material, float alpha)
	{
		return vec4(material.xyz * 0.4, min(material.w, alpha));
	}

	// TODO: args.
	vec4 directionLight(vec3 material, vec4 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal)
	{
		float cosTheta = clamp(dot(normal, lightDir), 0.0, 1.0);

		vec3 diffuse = vec3(0.0);
		vec3 specular = vec3(0.0);

		float dp = clamp(cosTheta, 0.0, 1.0);

		if (dp > 0.0)
		{
			diffuse = brdfLambert(material, lightColor.xyz, dp);
			specular = brdfPhong(lightColor.xyz, lightDir, viewDir, normal, 128);
			//specular = brdfCookTorrence(Material.diffuse.xyz, lightColor.xyz, lightDir, viewDir, normal, 10.0);
			return vec4(diffuse + specular, 0);
		}

		return vec4(0);
	}

	vec4 sphereLight(vec3 material, vec4 lightColor, vec4 lightPos, vec3 esFrag, vec3 normal, vec3 viewDir, float lightRadius, float dist, bool phongFlag, out float att)
	{
		if (material == vec3(0))
			material = vec3(0.2);
		att = 0;

		vec3 lightDir = normalize(lightPos.xyz - esFrag);
		//float cosTheta = clamp(dot(VertexIn.normal, lightDir), 0.0, 1.0);
		float cosTheta = dot(normal, lightDir);

		vec3 diffuse = vec3(0);
		vec3 specular = vec3(0);

		//float dp = max(cosTheta, 0.0);
		//float dp = abs(cosTheta);
		float dp = clamp(cosTheta, 0.0, 1.0);
		//float dp = cosTheta;

		if (dp > 0)
		{
			diffuse = brdfLambert(material, lightColor.xyz, dp);
			if (phongFlag)
				specular = brdfPhong(lightColor.xyz, lightDir, viewDir, normal, 128);
				//specular = brdfCookTorrence(material, lightColor.xyz, lightDir, viewDir, normal, 1.0);
			else
				specular = brdfCookTorrence(material, lightColor.xyz, lightDir, viewDir, normal, 0.3);

			att = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
			//att = 1.0 / (dist * dist);
			//att = pow(2.71828, -dist * 0.1);
			//att = 1.0f;
			att *= att;
			//att = 1.0;
			return vec4(att * (diffuse + specular), 0);
		#if 0
			else
			{
				diffuse = brdfLambert(material, lightColor.xyz, dp);
				specular = brdfPhong(lightColor.xyz, lightDir, viewDir, normal, 128);

				// TODO: Need a smooth cutoff to the edge...
				// FIXME: Trying a bunch of different stuff. Not sure which one I like best.
				float d = max(dist - lightRadius, 0);
				float denom = d / lightRadius + 1;
				att = 1.0 / (denom * denom);
				att = (att - lightRadius) / (1.0 - lightRadius);
				//att = pow(2.71828, -dist * 0.8);
				att = 1.0 / (dist * dist);
				if (att < 1.0)
				{
					att = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
					att *= att;
				}
				att = clamp(att, 0.0, 1.0);
				//if (att < 1)
				//	att = 1;
				att = 1.0;
				return vec4(att * (diffuse + specular), 0);
			}
		#endif
		}
		return vec4(0);
	}

	vec4 coneLight(vec3 material, vec4 lightColor, vec4 lightPos, vec3 esFrag, vec3 normal, vec3 viewDir, vec3 coneDir, float aperture, float lightRadius, float dist, out float att)
	{
		att = 0;

		vec3 lightDir = normalize(lightPos.xyz - esFrag);
		vec3 spotDir = normalize(coneDir); // TODO: may need to negate this.

		// Inside the cone?
		float theta = dot(-lightDir, spotDir);
		if (acos(theta) > aperture)
			return vec4(0.0);

		vec3 diffuse = vec3(0.0);
		vec3 specular = vec3(0.0);

		float cosTheta = dot(normal, lightDir);
		//float dp = abs(cosTheta);
		float dp = clamp(cosTheta, 0.0, 1.0);

	#if 0
		if (dp > 0.0)
		{
			//vec4 lightColor = floatToRGBA8(lightColors[id]);
			//diffuse = Material.diffuseMat.xyz * dp * lightColor.xyz;
			diffuse = material * dp * lightColor.xyz;
			vec3 reflection = reflect(-lightDir, normal);
			float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
			//float nDotH = abs(dot(viewDir, normalize(reflection)));
			float intensity = pow(nDotH, 512);
			specular = intensity * lightColor.xyz;

			float cosAp = cos(aperture);
			float angularAtt = max(0, (theta - cosAp) / (1.0 - cosAp));
			float radialAtt = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
			//float radialAtt = 1.0 / (dist * dist);
			//att = 1.0;
			//att = 1.0 / (dist * dist);
			//radialAtt = 1.0;
			att = radialAtt * angularAtt;
			//att = radialAtt * pow(2.71828, -dist * 0.1); // Physical attenuation?
			return vec4(att * (diffuse + specular), 0);
		}
	#else
		if (dp > 0.0)
		{
			diffuse = brdfLambert(material, lightColor.xyz, dp);
			specular = brdfPhong(lightColor.xyz, lightDir, viewDir, normal, 128);

			float cosAp = cos(aperture);
			float angularAtt = max(0, (theta - cosAp) / (1.0 - cosAp));

		#if 0
			// TODO: Need a smooth cutoff to the edge...
			float d = max(dist - lightRadius, 0);
			float denom = d / lightRadius + 1;
			att = 1.0 / (denom * denom);
			att = (att - lightRadius) / (1.0 - lightRadius);
			//att = 1.0 / (dist * dist);
			att = clamp(att, 0.0, 1.0);
			att *= angularAtt;
			att = 1.0;
		#else
			float radialAtt = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
			//att = 1.0f;
			att = angularAtt * radialAtt;
		#endif
			return vec4(att * (diffuse + specular), 0);
		}
	#endif
		return vec4(0);
	}

#else

	vec4 getAmbient(float alpha)
	{
		vec4 ambient = vec4(Material.ambient.xyz * 0.4, Material.ambient.w);
		if (texFlag)
		{
			vec4 texColor = texture2D(diffuseTex, VertexIn.texCoord);
			ambient = texColor * 0.4;
			ambient.w = min(Material.ambient.w, texColor.w);
		}
		ambient.w = min(ambient.w, alpha);
		return ambient;
	}

	// TODO: args.
	vec4 directionLight(vec4 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal)
	{
		float cosTheta = clamp(dot(normal, lightDir), 0.0, 1.0);

		vec3 diffuse = vec3(0.0);
		vec3 specular = vec3(0.0);

		float dp = clamp(cosTheta, 0.0, 1.0);

		if (dp > 0.0)
		{
			if (texFlag)
				diffuse = brdfLambert(texture2D(diffuseTex, VertexIn.texCoord).xyz, lightColor.xyz, dp);
			else
				diffuse = brdfLambert(Material.diffuse.xyz, lightColor.xyz, dp);
			specular = brdfPhong(lightColor.xyz, lightDir, viewDir, normal, 128);
			//specular = brdfCookTorrence(Material.diffuse.xyz, lightColor.xyz, lightDir, viewDir, normal, 10.0);
			return vec4(diffuse + specular, 0);
		}

		return vec4(0);
	}

	vec4 sphereLight(vec4 lightColor, vec4 lightPos, vec3 esFrag, vec3 normal, vec3 viewDir, float lightRadius, float dist)
	{
		vec3 lightDir = normalize(lightPos.xyz - esFrag);
		//float cosTheta = clamp(dot(VertexIn.normal, lightDir), 0.0, 1.0);
		float cosTheta = dot(normal, lightDir);

		vec3 diffuse = vec3(0.0);
		vec3 specular = vec3(0.0);

		vec3 texColor = vec3(0.0);
		if (texFlag)
			texColor = texture2D(diffuseTex, VertexIn.texCoord).xyz;

		//float dp = max((cosTheta + 1.0) / 2.0, 0.0);
		//float dp = abs(cosTheta);
		float dp = clamp(cosTheta, 0.0, 1.0);

		if (dp > 0.0)
		{
			if (texFlag)
				diffuse = brdfLambert(texture2D(diffuseTex, VertexIn.texCoord).xyz, lightColor.xyz, dp);
			else
				diffuse = brdfLambert(Material.diffuse.xyz, lightColor.xyz, dp);
			specular = brdfPhong(lightColor.xyz, lightDir, viewDir, normal, 128);
			//specular = brdfCookTorrence(Material.diffuse.xyz, lightColor.xyz, lightDir, viewDir, normal, 1.0);

			//float att = 1.0 / (dist * dist);
			float att = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
			att *= att;
			return vec4(att * (diffuse + specular), 0);
		}
		return vec4(0);
	}

	vec4 coneLight(vec4 lightColor, vec4 lightPos, vec3 esFrag, vec3 normal, vec3 viewDir, vec3 coneDir, float aperture, float lightRadius, float dist, out float att)
	{
		vec3 lightDir = normalize(lightPos.xyz - esFrag);
		vec3 spotDir = normalize(coneDir); // TODO: may need to negate this.

		// Inside the cone?
		float theta = dot(-lightDir, spotDir);
		if (acos(theta) > aperture)
			return vec4(0.0);

		vec3 diffuse = vec3(0.0);
		vec3 specular = vec3(0.0);

		vec3 texColor = vec3(0.0);
		if (texFlag)
			texColor = texture2D(diffuseTex, VertexIn.texCoord).xyz;

		float cosTheta = dot(normal, lightDir);
		//float dp = abs(cosTheta);
		float dp = clamp(cosTheta, 0.0, 1.0);

		if (dp > 0.0)
		{
			if (texFlag)
				diffuse = brdfLambert(texture2D(diffuseTex, VertexIn.texCoord).xyz, lightColor.xyz, dp);
			else
				diffuse = brdfLambert(Material.diffuse.xyz, lightColor.xyz, dp);
			specular = brdfPhong(lightColor.xyz, lightDir, viewDir, normal, 128);

			float cosAp = cos(aperture);
			float angularAtt = max(0, (theta - cosAp) / (1.0 - cosAp));
			float radialAtt = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
			//att = 1.0f;
			att = angularAtt * radialAtt;
			return vec4(att * (diffuse + specular), 0);
		}
		return vec4(0);
	}

#endif

#endif

