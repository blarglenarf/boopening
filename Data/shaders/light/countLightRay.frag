#version 430

#define STORAGE_BUFFERS 1
#define CLUSTER_CULL 0
#define USE_CLUSTER 0
#define LIGHT_DRAW 0

#define CLUSTERS 32
#define MASK_SIZE 1
#define MAX_EYE_Z 30.0

#include "../utils.glsl"

#if STORAGE_BUFFERS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	uniform layout(r32ui) coherent uimageBuffer Offsets;
#endif


#if CLUSTER_CULL
	readonly buffer ClusterMasks
	{
		uint clusterMasks[];
	};
#endif

in VertexData
{
	flat vec4 esLightPos;
	flat vec4 csLightPos;
	flat float radius;
	flat vec4 dir;
} VertexIn;

uniform ivec4 viewport;
uniform mat4 invPMatrix;
uniform mat4 pMatrix;

uniform ivec2 size;

out vec4 fragColor;

#if CLUSTER_CULL || USE_CLUSTER
	int getCluster(float depth)
	{
		return max(0, min(CLUSTERS - 1, int(depth / (MAX_EYE_Z / float(CLUSTERS - 1)))));
	}

	int getClusterIndex(vec2 fragCoord, int cluster)
	{
		return int(fragCoord.y) * size.x * CLUSTERS + int(fragCoord.x) * CLUSTERS + cluster;
	}
#endif

#if CLUSTER_CULL
	int getFragIndex()
	{
		ivec2 gridCoord = ivec2(gl_FragCoord.xy);
		return size.x * gridCoord.y + gridCoord.x;
	}

	bool checkBit(int fragIndex, int cluster)
	{
		int maskIndex = cluster / (MASK_SIZE * 32);
		int clusterIndex = cluster % 32;
		return (clusterMasks[fragIndex * MASK_SIZE + maskIndex] & (1 << clusterIndex)) != 0;
	}

	bool checkBits(float minZ, float maxZ)
	{
		int fragIndex = getFragIndex();

		int cluster1 = getCluster(minZ);
		int cluster2 = getCluster(maxZ);

		for (int cluster = cluster1; cluster <= cluster2; cluster++)
			if (checkBit(fragIndex, cluster))
				return true;
		return false;
	}
#endif


// Count two fragments, one for front and one for back.
void main()
{
	fragColor = vec4(1);

	// Light screen space position.
	vec2 ssPos = gl_FragCoord.xy;
	vec2 ssLightPos = vec2(0, 0);

	// If we're a sphere, we need to pick the point in the tile closest to the centre.
	vec3 esLightPos = VertexIn.esLightPos.xyz;
	vec4 csLightPos = VertexIn.csLightPos;
	float radius = VertexIn.radius;

#if 0
	// If we're a cone, shift towards the cap or the apex, depending on orientation.
	if (VertexIn.esLightPos.w != 0)
	{
		// Cone is facing toward or away?
		vec3 coneDir = normalize(VertexIn.dir.xyz);
		if (dot(coneDir, vec3(0, 0, -1)) < 0)
		{
			// Toward.
			vec3 cap = VertexIn.esLightPos.xyz;
			cap += (coneDir * VertexIn.radius);
			csLightPos = pMatrix * vec4(cap, 1.0);
		}
	}
#else
	#if 0
	// If we're a cone, use the centre of the cone, and half its radius.
	if (VertexIn.esLightPos.w != 0)
	{
		radius *= 0.5;
		esLightPos += normalize(VertexIn.dir.xyz) * radius;
		csLightPos = pMatrix * vec4(esLightPos, 1.0);
		radius *= 1.3;
	}
	#endif
#endif

#if LIGHT_DRAW
	vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -30.0), viewport, invPMatrix).xyz);
#else
	ssLightPos = ((vec2(csLightPos.xy / csLightPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(size.x, size.y);

	// If the ssLightPos is inside this pixel, use it, otherwise push closer to it.
	if (ssLightPos.x < gl_FragCoord.x - 0.5)
		ssPos.x -= 0.5;
	else if (ssLightPos.x > gl_FragCoord.x + 0.5)
		ssPos.x += 0.5;
	else
		ssPos.x = ssLightPos.x;
	if (ssLightPos.y < gl_FragCoord.y - 0.5)
		ssPos.y -= 0.5;
	else if (ssLightPos.y > gl_FragCoord.y + 0.5)
		ssPos.y += 0.5;
	else
		ssPos.y = ssLightPos.y;

	// TODO: If we're outside the radius of the light, do nothing.
	vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -30.0), viewport, invPMatrix).xyz);
	//vec3 dir = normalize(VertexIn.esVert.xyz);
#endif

	float frontDepth, backDepth;

#if 0
	if (!sphereIntersection(esLightPos, radius, dir, frontDepth, backDepth))
	{
		discard;
		return;
	}
#else
	// Sphere.
	if (VertexIn.esLightPos.w == 0)
	{
		if (!sphereIntersection(esLightPos, radius, dir, frontDepth, backDepth))
		{
			discard;
			return;
		}
	}

	// Cone.
	else
	{
		vec3 conePos = esLightPos.xyz;
		vec3 coneDir = normalize(VertexIn.dir.xyz);
		float coneRad = VertexIn.esLightPos.w;
		//conePos = esLightPos + (coneDir * coneRad);

		if (!coneIntersection(conePos, VertexIn.radius, dir, coneDir, VertexIn.dir.w, frontDepth, backDepth))
		// Sphere inersections are faster than cones.
		//if (!sphereIntersection(conePos, coneRad, dir, frontDepth, backDepth))
		{
			discard;
			return;
		}
	}
#endif

#if CLUSTER_CULL && !USE_CLUSTER
	// One or two intersections.
	if (!checkBits(frontDepth, backDepth))
	{
		discard;
		return;
	}
#endif

	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

#if USE_CLUSTER && !LIGHT_DRAW
	int frontCluster = getCluster(frontDepth);
	int backCluster = getCluster(backDepth);

	for (int cluster = frontCluster; cluster <= backCluster; cluster++)
	{
	#if CLUSTER_CULL
		if (!checkBit(pixel, cluster))
			continue;
	#endif
		int clusterIndex = getClusterIndex(gl_FragCoord.xy, cluster);
		atomicAdd(offsets[clusterIndex], 1);
	}
#else
	#if STORAGE_BUFFERS
		atomicAdd(offsets[pixel], 1);
		atomicAdd(offsets[pixel], 1);
	#else
		imageAtomicAdd(Offsets, pixel, 1U);
		imageAtomicAdd(Offsets, pixel, 1U);
	#endif
#endif
}

