#version 430

#define LIGHT_DRAW 0
#define CONSERVATIVE_RASTER 0

#import "lfb_L"

#include "../utils.glsl"

LFB_DEC(light);

#if LIGHT_DRAW
	readonly buffer LightColors
	{
		float lightColors[];
	};
#endif

in VertexData
{
	vec3 esVert;
	flat uint id;
#if CONSERVATIVE_RASTER
	flat vec4 aabb;
#endif
} VertexIn;


void main()
{
#if CONSERVATIVE_RASTER && 0
	if (!(gl_FragCoord.x >= VertexIn.aabb.x && gl_FragCoord.y >= VertexIn.aabb.y && gl_FragCoord.x <= VertexIn.aabb.z && gl_FragCoord.y <= VertexIn.aabb.w))
	{
		discard;
		return;
	}
#endif

	int pixel = LFB_GET_SIZE(light).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

#if LIGHT_DRAW
	LFB_ADD_DATA(light, pixel, vec2(lightColors[VertexIn.id], -VertexIn.esVert.z));
#else
	LFB_ADD_DATA(light, pixel, vec2(uintBitsToFloat(VertexIn.id), -VertexIn.esVert.z));
#endif
}

