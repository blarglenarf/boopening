#version 430

// Billboarded quad or sphere?
// Note: Billboarded quads seem to be much faster, even though they result in more pixels covered.
#define USE_QUAD 1

#define MAX_EYE_Z 30.0

layout(location = 0) in vec4 vertex;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform uint indexOffset;

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

#if USE_QUAD
	out LightData
	{
		flat vec4 esLightPos;
		vec4 esVert;
		vec4 csVert;
		flat float radius;
		flat vec4 dir;
	} VertexOut;
#else
	out LightData
	{
		vec4 osVert;
		flat float w;
		flat float radius;
		flat vec4 dir;
	} VertexOut;
#endif

void main()
{
	vec4 osVert = vec4(vertex.xyz, 1.0);
#if USE_QUAD
	vec4 esVert = mvMatrix * osVert;
	VertexOut.esLightPos = esVert;
#else
	VertexOut.osVert = osVert;
	VertexOut.w = vertex.w;
#endif

	// Sphere or cone?
	if (indexOffset == 0)
	{
		VertexOut.radius = sphereLightRadii[gl_VertexID];
		//VertexOut.esVert.w = 0;
		VertexOut.esLightPos.w = 0;
	}
	else
	{
		VertexOut.esLightPos.w = 1;
	#if 1
		vec4 dir = mvMatrix * vec4(coneLightDir[gl_VertexID].xyz, 0.0);
		dir.w = coneLightDir[gl_VertexID].w;

		VertexOut.radius = coneLightDist[gl_VertexID];
		VertexOut.dir = dir;
	#else
		vec4 osDir = vec4(coneLightDir[gl_VertexID]);
		float aperture = osDir.w;
		osDir.w = 0.0;

		// Bounding sphere based on dir.
		// Only need the radius.
		// FIXME: Only works in 3 dimensions.
		float d = coneLightDist[gl_VertexID];
		float bc = d * tan(aperture / 2.0);
		vec3 A = vec3(bc, d, 0);
		vec3 B = vec3(-bc, d, 0);
		vec3 a = A;
		vec3 b = B;

		vec3 na = normalize(a);
		vec3 nb = normalize(b);
		vec3 cab = cross(a, b);
		vec3 ncab = cross(na, nb);

		vec3 centre = cross((dot(na, na) * b) - (dot(nb, nb) * a), cab) / (2.0 * dot(ncab, ncab));
		#if 1
			float sa = distance(a, vec3(0));
			float sb = distance(b, vec3(0));
			float sc = distance(a, b);
			float s = (sa + sb + sc) / 2.0;
			float area = sqrt(s * (s - sa) * (s - sb) * (s - sc));
			float r = (sa * sb * sc) / (4.0 * area);
		#else
			float r = distance(centre, vec3(0));
		#endif

		//vec3 nBC = normalize(B - C);
		//float r = dot(nBC, nBC) / (2.0 * sin(aperture * 2.0));
		r *= 1.1;
		VertexOut.esVert.w = r;

		vec4 dir = mvMatrix * vec4(osDir.xyz, 0);
		dir.w = coneLightDir[gl_VertexID].w;

		VertexOut.radius = coneLightDist[gl_VertexID];
		VertexOut.dir = dir;
	#endif
	}

#if 1
	// Light is outside the frustum range but overlaps the frustum?
	if (-esVert.z < 0 && -esVert.z + VertexOut.radius >= 0)
		esVert.z = -0.1;
	else if (-esVert.z > MAX_EYE_Z && -esVert.z - VertexOut.radius <= MAX_EYE_Z)
		esVert.z = -(MAX_EYE_Z - 0.1);
#endif
	VertexOut.esVert = vec4(esVert.xyz, vertex.w);
	VertexOut.csVert = pMatrix * esVert;
}

