#ifndef CAPTURE_LIGHT
#define CAPTURE_LIGHT

#define CONSERVATIVE_RASTER 0

// Cones only require 18 vertices, but 60 are needed by the icospheres.
layout(points) in;
layout(triangle_strip, max_vertices = 60) out;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

uniform uint indexOffset;

#if CONSERVATIVE_RASTER
	uniform ivec4 viewport;
#endif

in LightData
{
	vec4 osVert;
#if STORE_ID
	flat uint id;
#endif
	flat float radius;
	flat vec2 rot;
} VertexIn[1];

out VertexData
{
	vec3 esVert;
#if STORE_ID
	flat uint id;
#endif
#if CONSERVATIVE_RASTER
	flat vec4 aabb;
#endif
} VertexOut;


// If we need conservative rasterization but don't have the hardware, handle that here, also we need to know the pixel size.
#if CONSERVATIVE_RASTER
	// TODO: Algorithm 1:
	// * Calc normal for each of the three edges.
	// * A semi-diagonal is a direction vector from the vertex to each corner of a bounding quad, which is the size of a pixel.
	// * For each vertex, of the four semi-diagonals, get the two that are in the same quadrants as the two normals.
	// * Normalise the semi-diagonals.
	// * Handle the three separate cases (normals in same quadrant, normals in neighbouring quadrants, normals in opposite quadrants).
	// * Final xy position is hPixel.xy * diag * vtxPos.w (needs to be in clip space).
	// * Conservative depth values need to be handled in the fragment shader.
	//
	// TODO: Algorithm 2:
	// * Calculate AABB of triangle using the original vertices, then increase size by hPixel.
	// * Pass the AABB to the fragment shader, any fragment that falls outside the AABB can be discarded.
	// * Extrude the vertices of the triangle based on semi-diagonals of each vertex.
	//
	// TODO: Conservative depth has to be done in the fragment shader.
	// * For all points of the pixel, project those onto the triangle, pick the min or max depth based on front or back face.
	// * Or get maximum gradient of triangle for the pixel.

	// AABB is in screen space.
	#define AABB(v1, v2, v3) \
		ssVertA = (csVertA.xy / csVertA.w + vec2(1, 1)) * viewport.zw / 2.0; \
		ssVertB = (csVertB.xy / csVertB.w + vec2(1, 1)) * viewport.zw / 2.0; \
		ssVertC = (csVertC.xy / csVertC.w + vec2(1, 1)) * viewport.zw / 2.0; \
		aabb.x = min(min(v1.x, v2.x), v3.x); \
		aabb.y = min(min(v1.y, v2.y), v3.y); \
		aabb.z = max(max(v1.x, v2.x), v3.x); \
		aabb.w = max(max(v1.y, v2.y), v3.y);

	#define CS_VERTS(v1, v2, v3) \
		esVertA = mvMatrix * vec4(verts[v1] + VertexIn[0].osVert.xyz, 1); \
		csVertA = pMatrix * esVertA; \
		esVertB = mvMatrix * vec4(verts[v2] + VertexIn[0].osVert.xyz, 1); \
		csVertB = pMatrix * esVertB; \
		esVertC = mvMatrix * vec4(verts[v3] + VertexIn[0].osVert.xyz, 1); \
		csVertC = pMatrix * esVertC;

	#define EXTRUDE(prevPos, currPos, nextPos) \
		plane[0] = cross(currPos.xyw - prevPos.xyw, prevPos.xyw); \
		plane[1] = cross(nextPos.xyw - currPos.xyw, currPos.xyw); \
		plane[0].z -= dot(hPixel.xy, abs(plane[0].xy)); \
		plane[1].z -= dot(hPixel.xy, abs(plane[1].xy)); \
		finalPos.xyw = cross(plane[0], plane[1]); \
		finalPos.z = currPos.z;

	// FIXME: need to compute new esVert values based on the extruded csVert values.
	#if STORE_ID
		#define TRI(v1, v2, v3) \
			CS_VERTS(v1, v2, v3); \
			AABB(csVertA, csVertB, csVertC); \
			EXTRUDE(csVertC, csVertA, csVertB); \
			VertexOut.esVert = esVertA.xyz; \
			VertexOut.id = VertexIn[0].id; \
			VertexOut.aabb = aabb; \
			gl_Position = finalPos; \
			EmitVertex(); \
			EXTRUDE(csVertA, csVertB, csVertC); \
			VertexOut.esVert = esVertB.xyz; \
			VertexOut.id = VertexIn[0].id; \
			VertexOut.aabb = aabb; \
			gl_Position = finalPos; \
			EmitVertex(); \
			EXTRUDE(csVertB, csVertC, csVertA); \
			VertexOut.esVert = esVertC.xyz; \
			VertexOut.id = VertexIn[0].id; \
			VertexOut.aabb = aabb; \
			gl_Position = finalPos; \
			EmitVertex(); \
			EndPrimitive();
	#else
		#define TRI(v1, v2, v3) \
			CS_VERTS(v1, v2, v3); \
			AABB(csVertA, csVertB, csVertC); \
			EXTRUDE(csVertC, csVertA, csVertB); \
			VertexOut.esVert = esVertA.xyz; \
			VertexOut.aabb = aabb; \
			gl_Position = finalPos; \
			EmitVertex(); \
			EXTRUDE(csVertA, csVertB, csVertC); \
			VertexOut.esVert = esVertB.xyz; \
			VertexOut.aabb = aabb; \
			gl_Position = finalPos; \
			EmitVertex(); \
			EXTRUDE(csVertB, csVertC, csVertA); \
			VertexOut.esVert = esVertC.xyz; \
			VertexOut.aabb = aabb; \
			gl_Position = finalPos; \
			EmitVertex(); \
			EndPrimitive();
	#endif
#else
	#if STORE_ID
		#define TRI(v1, v2, v3) \
			esVert = mvMatrix * vec4(verts[v1] + VertexIn[0].osVert.xyz, 1); \
			VertexOut.esVert = esVert.xyz; \
			VertexOut.id = VertexIn[0].id; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			esVert = mvMatrix * vec4(verts[v2] + VertexIn[0].osVert.xyz, 1); \
			VertexOut.esVert = esVert.xyz; \
			VertexOut.id = VertexIn[0].id; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			esVert = mvMatrix * vec4(verts[v3] + VertexIn[0].osVert.xyz, 1); \
			VertexOut.esVert = esVert.xyz; \
			VertexOut.id = VertexIn[0].id; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			EndPrimitive();
	#else
		#define TRI(v1, v2, v3) \
			esVert = mvMatrix * vec4(verts[v1] + VertexIn[0].osVert.xyz, 1); \
			VertexOut.esVert = esVert.xyz; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			esVert = mvMatrix * vec4(verts[v2] + VertexIn[0].osVert.xyz, 1); \
			VertexOut.esVert = esVert.xyz; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			esVert = mvMatrix * vec4(verts[v3] + VertexIn[0].osVert.xyz, 1); \
			VertexOut.esVert = esVert.xyz; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			EndPrimitive();
	#endif
#endif


void main()
{
#if CONSERVATIVE_RASTER
	vec3 plane[2];

	vec4 esVertA, esVertB, esVertC;
	vec4 csVertA, csVertB, csVertC;
	vec2 ssVertA, ssVertB, ssVertC;
	vec4 finalPos;
	vec4 aabb;

	// Pixel size in clip space.
	vec2 pixel = vec2(1.0 / float(viewport.z), 1.0 / float(viewport.w));
	vec2 hPixel = pixel / 2.0;

	// In case you ever need pixel size in eye space.
	// float pixelWidthRatio = 2.0 / (float(viewport.z) * pMatrix[0][0]);
	// float pixelHeightRatio = 2.0 / (float(viewport.w) * pMatrix[1][1]);
	// float pixelWidth = pos.w * pixelWidthRatio;
	// float pixelHeight = pos.w * pixelHeightRatio;
#else
	vec4 esVert;
#endif

	float size = VertexIn[0].radius * 1.2;

	// First draw spheres.
	if (indexOffset == 0)
	{
		vec3 verts[12];
		verts[0]  = vec3(-size,  size,	 0);
		verts[1]  = vec3( size,  size,	 0);
		verts[2]  = vec3(-size, -size,	 0);
		verts[3]  = vec3( size, -size,	 0);
		verts[4]  = vec3( 0,	-size,	 size);
		verts[5]  = vec3( 0,	 size,	 size);
		verts[6]  = vec3( 0,	-size,	-size);
		verts[7]  = vec3( 0,	 size,	-size);
		verts[8]  = vec3( size,  0,		-size);
		verts[9]  = vec3( size,  0,		 size);
		verts[10] = vec3(-size,  0,		-size);
		verts[11] = vec3(-size,  0,		 size);

		// Create 20 triangles of the icosahedron.

		// 5 faces around point 0.
		TRI(0, 11, 5);
		TRI(0, 5, 1);
		TRI(0, 1, 7);
		TRI(0, 7, 10);
		TRI(0, 10, 11);

		// 5 adjacent faces.
		TRI(1, 5, 9);
		TRI(5, 11, 4);
		TRI(11, 10, 2);
		TRI(10, 7, 6);
		TRI(7, 1, 8);

		// 5 faces around point 3.
		TRI(3, 9, 4);
		TRI(3, 4, 2);
		TRI(3, 2, 6);
		TRI(3, 6, 8);
		TRI(3, 8, 9);

		// 5 adjacent faces.
		TRI(4, 9, 5);
		TRI(2, 4, 11);
		TRI(6, 2, 10);
		TRI(8, 6, 7);
		TRI(9, 8, 1);
	}

	// Then draw cones.
	else
	{
		vec3 verts[5];

		// TODO: positions based on aperture.
		// Just a pyramid.
		verts[0] = vec3(0, 0, 0);
		verts[1] = vec3(-size, size,  size);
		verts[2] = vec3(-size, size, -size);
		verts[3] = vec3( size, size, -size);
		verts[4] = vec3( size, size,  size);

		// Construct rotation matrix using given x and z rotations.
		float cosX = cos(VertexIn[0].rot.x);
		float cosZ = cos(VertexIn[0].rot.y);
		float sinX = sin(VertexIn[0].rot.x);
		float sinZ = sin(VertexIn[0].rot.y);

	#if 0
		// rot(X) * rot(Z).
		#if 1
			// Row major.
			mat4 rot = mat4(vec4(cosZ, -sinZ, 0, 0),
							vec4(cosX * sinZ, cosX * cosZ, -sinX, 0),
							vec4(sinX * sinZ, cosZ * sinX, cosX, 0),
							vec4(0, 0, 0, 1));
		#else
			// Column major.
			mat4 rot = mat4(vec4(cosZ, -sinZ, 0, 0),
							vec4(cosX * sinZ, cosX * cosZ, -sinX, 0),
							vec4(sinX * sinZ, cosZ * sinX, cosX, 0),
							vec4(0, 0, 0, 1));
		#endif
	#else
		// rot(Z) * rot(X).
		#if 1
			// Row major.
			mat4 rot = mat4(vec4(cosZ, -cosX * sinZ, sinX * sinZ, 0),
							vec4(sinZ, cosX * cosZ, -cosZ * sinX, 0),
							vec4(0, sinX, cosX, 0),
							vec4(0, 0, 0, 1));
		#else
			// Column major.
			mat4 rot = mat4(vec4(cosZ, sinZ, 0, 0),
							vec4(-cosX * sinZ, cosX * cosZ, sinX, 0),
							vec4(sinX * sinZ, -cosZ * sinX, cosX, 0),
							vec4(0, 0, 0, 1));
		#endif
	#endif

		// Rotate vertices using rotation matrix.
		verts[1] = (rot * vec4(verts[1], 1.0)).xyz;
		verts[2] = (rot * vec4(verts[2], 1.0)).xyz;
		verts[3] = (rot * vec4(verts[3], 1.0)).xyz;
		verts[4] = (rot * vec4(verts[4], 1.0)).xyz;

		// Join vertices to make a pyramid (currently clockwise).

		// Front.
		TRI(0, 4, 1);

		// Back.
		TRI(0, 2, 3);

		// Left.
		TRI(0, 1, 2);

		// Right.
		TRI(0, 3, 4);

		// Cap.
		TRI(1, 4, 2);
		TRI(2, 4, 3);
	}
}

#endif
