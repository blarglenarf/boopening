#version 430

layout(location = 0) in vec4 vertex;

uniform uint indexOffset;

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightRot
{
	vec2 coneLightRot[];
};

out LightData
{
	vec4 osVert;
	flat float radius;
	flat vec2 rot;
} VertexOut;

void main()
{
	VertexOut.osVert = vec4(vertex.xyz, 1.0);

	// Sphere or cone?
	if (indexOffset == 0)
	{
		VertexOut.radius = sphereLightRadii[gl_VertexID];
	}
	else
	{
		VertexOut.rot = coneLightRot[gl_VertexID];
		VertexOut.radius = coneLightDist[gl_VertexID];
	}
}

