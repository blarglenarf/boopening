#version 430

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;
layout(location = 3) in vec3 tangent;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;

out VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
	mat3 tbn;
} VertexOut;

void main()
{
	vec3 binormal = cross(normal, tangent);

	vec3 T = normalize(nMatrix * tangent);
	vec3 B = normalize(nMatrix * binormal);
	vec3 N = normalize(nMatrix * normal);
	mat3 TBN = mat3(T, B, N);
	VertexOut.tbn = TBN;

	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

    VertexOut.esFrag = esVert.xyz;
	VertexOut.normal = nMatrix * normalize(normal);
	VertexOut.texCoord = texCoord;

	gl_Position = csVert;
}

