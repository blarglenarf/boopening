#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define INDEX_BY_TILE 0

#import "lfb"

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#define OPAQUE 0
#define LFB_NAME lfb


#if !OPAQUE
	LFB_DEC(LFB_NAME, vec2);
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
	mat3 tbn;
} VertexIn;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform bool normFlag
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;


#include "../light/light.glsl"

#if OPAQUE
	out vec4 fragColor;
#endif


void main()
{
	vec4 color = vec4(0);

	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	if (normFlag)
	{
		normal = texture2D(normalTex, VertexIn.texCoord).rgb;
		normal = normalize(normal * 2.0 - 1.0);
		normal = normalize(VertexIn.tbn * normal);
	}

	color = getAmbient(0.3);
#if OPAQUE
	if (color.w == 0)
	{
		discard;
		return;
	}
	color.w = 1.0;
#endif

	color += directionLight(viewDir, normal);

#if !OPAQUE
	#if INDEX_BY_TILE
		int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
	#else
		int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	#endif
	LFB_ADD_DATA(LFB_NAME, pixel, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
	discard;
#else
	fragColor = color;
#endif
}

