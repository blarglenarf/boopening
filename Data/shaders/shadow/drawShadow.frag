#version 430

in vec2 texCoord;

uniform sampler2DShadow tex;

out vec4 fragColor;


void main()
{
	float vis = texture(tex, vec3(texCoord, 1)).x;

	fragColor = vec4(vis, vis, vis, 1.0);
}

