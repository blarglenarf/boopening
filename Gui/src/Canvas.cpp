#include "Canvas.h"

#include "Widgets/Widget.h"
#include "Widgets/TextInput.h"
#include "../../Platform/src/Window.h"
#include "../../Renderer/src/RenderResource/Font.h"
#include "../../Resources/src/Font/FontLoader.h"

using namespace Gui;

Canvas Canvas::canvas;

Canvas::~Canvas()
{
	for (auto &it : fonts)
		delete it.second;
	fonts.clear();

	// Can't delete widgets here, since we don't know how they/their memory was allocated.
	//for (auto *widget : widgets)
	//	delete widget;
#if 0
	// Deleting the widget can potentially affect the global widget memory, have to delete stuff this way.
	while (!widgets.empty())
	{
		auto it = widgets.begin() + widgets.size() - 1;
		widgets.erase(it);
		delete *it;
	}
#endif
	widgets.clear();
}

Canvas &Canvas::instance()
{
	return canvas;
}

void Canvas::init()
{
}

Rendering::Font *Canvas::addFont(const std::string &name, const std::string &filename, Rendering::Font *font)
{
	auto it = fonts.find(name);
	if (it != fonts.end())
	{
		std::cout << "Warning: font " << name << " already loaded, replacing.\n";
		delete it->second;
	}
	fonts[name] = font;

	// TODO: May want to dynamically setup font sizes as requested.
	Resources::FontLoader::load(font, filename);
	font->setSize(18); // FIXME: Do something other than this, probably have a bunch of font sizes.
	font->storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");

	return font;
}

Rendering::Font *Canvas::getFont(const std::string &name)
{
	auto it = fonts.find(name);
	if (it == fonts.end())
		return nullptr;
	return it->second;
}

NineBox *Canvas::addNineBox(NineBox *box)
{
	return boxPool.getBox(boxPool.addBox(box));
}

NineBox *Canvas::removeNineBox(NineBox *box)
{
	boxPool.removeBox(box);
	return box;
}

Text *Canvas::addText(Text *text)
{
	return textPool.getText(textPool.addText(text));
}

Text *Canvas::removeText(Text *text)
{
	textPool.removeText(text);
	return text;
}

Widget *Canvas::addWidgetMemory(Widget *widget)
{
	// TODO: If the widget is already here, don't add it twice.
	//for (auto *w : widgets)
	//	if (w == widget)
	//		return widget;
	widgets.push_back(widget);
	return widget;
}

Widget *Canvas::removeWidgetMemory(Widget *widget)
{
	for (auto it = widgets.begin(); it != widgets.end(); it++)
	{
		if (*it == widget)
		{
			widgets.erase(it);
			return widget;
			break;
		}
	}

	// If it wasn't found then it was probably already deleted, so just return null.
	return nullptr;
}

void Canvas::toggleConsole()
{
	if (consoleWidget)
		((Console*) consoleWidget)->toggle();
}

void Canvas::render()
{
	boxPool.render();
	textPool.render();
}

void Canvas::update(float dt, int x, int y, int dx, int dy)
{
	float currZ = -1000000;
	Widget *newHover = nullptr;

	// If we're dragging something, move it to the correct position.
	if (draggedWidget)
	{
		auto pos = draggedWidget->getPos();
		draggedWidget->setPos(pos.x + dx, pos.y + dy);
	}

	for (auto &it : widgetGroups)
	{
		auto &group = it.second;
		if (group.isHidden())
			continue;

		group.update(dt, x, y);
		auto *widget = group.getHoveredWidget();
		if (!widget)
			continue;
		if (widget->getZ() > currZ)
		{
			newHover = widget;
			currZ = widget->getZ();
		}
	}

	if (!draggedWidget && hoveredWidget != newHover)
	{
		// No longer hovering over that thing we clicked? Get rid of it.
		if (hoveredWidget == clickedWidget)
			clickedWidget = nullptr;

		if (hoveredWidget)
		{
			hoveredWidget->getNineBox().useStandardBackground();
			hoveredWidget->endHover();
		}
		if (newHover)
		{
			newHover->getNineBox().useHoverBackground();
			newHover->beginHover();
		}
		hoveredWidget = newHover;
	}

	//if (!boxPool.checkIntegrity())
	//	std::cout << "Bad BoxPool integrity.\n";
}

bool Canvas::handleMouseClickEvent(Platform::Event &ev)
{
	// Ways of handling clickable stuff:
	// * Can be assigned to any (or multiple) mouse buttons.
	// * Can also be assigned to a key, but will only trigger if it's currently highlighted.
	// * May need to know mouse position when it's clicked?

	if (!hoveredWidget)
	{
		if (selectedWidget)
		{
			selectedWidget->endSelect();
			selectedWidget = nullptr;
		}
		return false;
	}

	// If the draggable flag is true, then we need to begin dragging.
	if (hoveredWidget->isDraggable())
		draggedWidget = hoveredWidget; // TODO: Record initial drag pos/mouse pos so we can keep the relative amount the same.

	// TODO: If it's a draggable widget, send the begin drag event.

	// TODO: Only if the widget can be selected.
	if (selectedWidget != hoveredWidget)
	{
		if (selectedWidget)
			selectedWidget->endSelect();
		selectedWidget = hoveredWidget;
		selectedWidget->beginSelect();
	}

	// This should only happen if the widget is clickable.
	auto *clicked = dynamic_cast<Clickable*>(hoveredWidget);
	if (!clicked)
		return false;

	// Remember that y is flipped.
	int y = ev.y;
	ev.y = Platform::Window::getHeight() - y;

	clickedWidget = hoveredWidget;
	clicked->clickEvent(clickedWidget, ev);
	clickedWidget->getNineBox().useClickBackground();

	ev.y = y;

	return true;
}

bool Canvas::handleMouseDownEvent(Platform::Event &ev)
{
	if (!hoveredWidget)
		return false;

	(void) (ev);
	// TODO: Don't forget that y is flipped.

	return true;
}

bool Canvas::handleMouseUpEvent(Platform::Event &ev)
{
	// Not sure what I want to do with this event yet...
	(void) (ev);

	// If the draggable flag is true, then we need to stop dragging.
	if (draggedWidget)
		draggedWidget = nullptr;

	// TODO: If it's a draggable widget and it's being dragged, send the end drag event.

	if (!clickedWidget)
		return false;

	auto &box = clickedWidget->getNineBox();
	if (clickedWidget == hoveredWidget)
		box.useHoverBackground();
	else
		box.useStandardBackground();

	return true;
}

bool Canvas::handleMouseDragEvent(Platform::Event &ev)
{
	(void) (ev);
	// TODO: Don't forget that y is flipped.

	return false;
}

bool Canvas::handleKeyDownEvent(Platform::Event &ev)
{
	// Not sure what else is needed here, so for now send the event to the selected widget.
	if (!selectedWidget)
		return false;
	auto *down = dynamic_cast<KeyPressable*>(selectedWidget);
	if (!down)
		return false;
	return down->keyDownEvent(selectedWidget, ev);
}

bool Canvas::handleKeyUpEvent(Platform::Event &ev)
{
	// Not sure what else is needed here, so for now send the event to the selected widget.
	if (!selectedWidget)
		return false;
	auto *up = dynamic_cast<KeyPressable*>(selectedWidget);
	if (!up)
		return false;
	return up->keyUpEvent(selectedWidget, ev);
}

bool Canvas::handleKeyPressEvent(Platform::Event &ev)
{
	// Not sure what else is needed here, so for now send the event to the selected widget.
	if (!selectedWidget)
		return false;
	auto *press = dynamic_cast<KeyPressable*>(selectedWidget);
	if (!press)
		return false;
	return press->keyPressEvent(selectedWidget, ev);
}

