#pragma once

#include "NineBox/BoxPool.h"
#include "Text/TextPool.h"
#include "Widgets/WidgetGroup.h"

#include "../../Platform/src/Event.h"

#include <string>
#include <map>
#include <vector>

namespace Rendering
{
	class Font;
}

namespace Gui
{
	class Widget;

	class Canvas
	{
	private:
		std::map<std::string, Rendering::Font*> fonts;

		// TODO: Add multiple pools (vector) if it becomes necessary.
		// TODO: Could have a separate boxPool per widget group, would make hiding/unhiding simpler.
		BoxPool boxPool;
		TextPool textPool;

		Widget *hoveredWidget;
		Widget *clickedWidget;
		Widget *draggedWidget;

		Widget *selectedWidget;

		// For easy access to the in-app console.
		Widget *consoleWidget;

		std::map<std::string, WidgetGroup> widgetGroups;

		// This is here only to own the memory of these widgets. May need to be replaced by something else later.
		// TODO: Replace this with a WidgetPool class.
		std::vector<Widget*> widgets;

		static Canvas canvas;

	private:
		Canvas() : hoveredWidget(nullptr), clickedWidget(nullptr), draggedWidget(nullptr), selectedWidget(nullptr), consoleWidget(nullptr) {}

	public:
		~Canvas();

		static Canvas &instance();

		void init();

		Rendering::Font *addFont(const std::string &name, const std::string &filename, Rendering::Font *font);
		Rendering::Font *getFont(const std::string &name);

		NineBox *addNineBox(NineBox *box);
		NineBox *removeNineBox(NineBox *box);

		Text *addText(Text *text);
		Text *removeText(Text *text);

		Widget *addWidgetMemory(Widget *widget);
		Widget *removeWidgetMemory(Widget *widget);

		Widget *setConsole(Widget *consoleWidget) { this->consoleWidget = consoleWidget; return consoleWidget; }
		Widget *getConsole() { return consoleWidget; }
		void toggleConsole();

		bool isHovering() const { return hoveredWidget != nullptr; }
		bool isClicking() const { return clickedWidget != nullptr; }
		bool isDragging() const { return draggedWidget != nullptr; }

		Widget *getHovered() { return hoveredWidget; }
		Widget *getClicked() { return clickedWidget; }
		Widget *getDragged() { return draggedWidget; }

		void setHovered(Widget *hovered) { hoveredWidget = hovered; }
		void setClicked(Widget *clicked) { clickedWidget = clicked; }
		void setDragged(Widget *dragged) { draggedWidget = dragged; }

		WidgetGroup &getWidgetGroup(const std::string &name) { return widgetGroups[name]; }

		void render();
		void update(float dt, int x, int y, int dx, int dy);

		bool handleMouseClickEvent(Platform::Event &ev);
		bool handleMouseDownEvent(Platform::Event &ev);
		bool handleMouseUpEvent(Platform::Event &ev);
		bool handleMouseDragEvent(Platform::Event &ev);

		bool handleKeyDownEvent(Platform::Event &ev);
		bool handleKeyUpEvent(Platform::Event &ev);
		bool handleKeyPressEvent(Platform::Event &ev);
	};
}

