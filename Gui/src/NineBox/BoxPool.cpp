#include "BoxPool.h"

#include "../../../Renderer/src/RenderResource/ImageAtlas.h"
#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/RenderResource/ShaderSource.h"
#include "../../../Platform/src/Window.h"

using namespace Gui;
using namespace Rendering;


static void useImage(ImageAtlas *atlas, Image *img, Math::vec4 &texCoord)
{
	if (!img)
	{
		texCoord.x = 0;
		texCoord.y = 0;
		texCoord.z = 0;
		texCoord.w = 0;
		return;
	}

	if (!atlas->hasImage(img))
		atlas->add(img);

	auto rect = atlas->getTexCoords(img);
	texCoord.x = rect.left();
	texCoord.y = rect.bottom();
	texCoord.z = rect.right();
	texCoord.w = rect.top();
}


BoxPool::BoxPool() : imgAtlas(nullptr), atlasTexture(0), vao(nullptr), shader(nullptr), dirtyFlag(false)
{
}

BoxPool::~BoxPool()
{
	delete imgAtlas;
	delete vao;
	delete shader;

	if (atlasTexture != 0)
		glDeleteTextures(1, &atlasTexture);
}

size_t BoxPool::addBox(NineBox *box)
{
	// If the box has a parent, then we need to add after the parent.
	size_t parentIndex = 0;
	auto *parent = box->getParent();
	if (parent)
		parentIndex = parent->getPoolIndex();

	// Only add the box if it's not already here.
	for (size_t i = 0; i < boxes.size(); i++)
	{
		if (boxes[i] == box)
		{
			if (parentIndex > 0 && i <= parentIndex)
			{
				// Already here, but at the wrong spot, need to add to the right place.
				removeBox(box);
				return addBox(box);
			}

			boxes[i]->setPool(this, i);
			return i;
		}
	}

	size_t index = 0;

	// First see if there's available space, then add there.
	if (!removedBoxes.empty())
	{
		if (parentIndex > 0)
		{
			// If we have to add after a parent, then we need to look for the right spot.
			bool found = false;
			for (size_t i = 0; i < removedBoxes.size(); i++)
			{
				if (removedBoxes[i] > parentIndex)
				{
					index = removedBoxes[i];
					removedBoxes.erase(removedBoxes.begin() + i);
					boxes[index] = box;
					found = true;
					break;
				}
			}
			if (!found)
			{
				// No available spot, add to the end.
				boxes.push_back(box);
				index = boxes.size() - 1;
				boxVerts.resize(boxVerts.size() + 9);
			}
		}
		else
		{
			// No parent, so just use the first available space.
			index = removedBoxes[0];
			removedBoxes.erase(removedBoxes.begin());
			boxes[index] = box;
		}
	}
	else
	{
		// No available space, add to end.
		boxes.push_back(box);
		index = boxes.size() - 1;
		boxVerts.resize(boxVerts.size() + 9);
	}

	boxes[index]->setPool(this, index);
	boxes[index]->setDirty(true);
	addDirty(index); // Just in case the box had been removed and was already dirty.
	return index;
}

NineBox *BoxPool::getBox(size_t index)
{
	if (index >= boxes.size())
		return nullptr;
	return boxes[index];
}

bool BoxPool::checkIntegrity()
{
	bool okFlag = true;
	for (size_t i = 0; i < boxes.size(); i++)
	{
		if (boxes[i] && boxes[i]->getPoolIndex() != i)
			okFlag = false;
	}
	return okFlag;
}

void BoxPool::removeBox(NineBox *nineBox)
{
	// Only remove the box if it's here.
	size_t index = 0;
	bool found = false;
	for (auto it = boxes.begin(); it != boxes.end(); it++)
	{
		if (*it == nineBox)
		{
			boxes[index] = nullptr;
			removedBoxes.push_back(index);
			found = true;
			break;
		}
		index++;
	}

	if (!found)
		return;

	for (auto it = dirtyBoxes.begin(); it != dirtyBoxes.end(); it++)
	{
		if (*it == index)
		{
			dirtyBoxes.erase(it);
			break;
		}
	}

	for (size_t i = index * 9; i < (index + 1) * 9; i++)
		boxVerts[i].pos = Math::vec4(0, 0, 0, 0);

	dirtyFlag = true;
}

void BoxPool::addDirty(size_t index)
{
	for (auto d : dirtyBoxes)
		if (d == index)
			return;
	dirtyBoxes.push_back(index);
}

void BoxPool::useImages(size_t index, size_t background, Image *bl, Image *l, Image *tl, Image *br, Image *r, Image *tr, Image *b, Image *m, Image *t)
{
	if (index >= boxes.size())
		return;

	// 4096x4096 texture maybe?
	int imgSize = 4096;
	int channels = 4;
	Math::ivec2 dimensions(imgSize, imgSize);

	if (!imgAtlas)
	{
		imgAtlas = new ImageAtlas();
		imgAtlas->init(dimensions.x * dimensions.y * channels, channels, dimensions, true);

		// Need to start by adding a small black section to the bottom to allow for boxes with no texturing.
		imgAtlas->addBlank(2, 2);
	}

	auto *box = boxes[index];
	auto *boxTex = box->getTexes(background);

	useImage(imgAtlas, bl, boxTex[0]);
	useImage(imgAtlas, l, boxTex[1]);
	useImage(imgAtlas, tl, boxTex[2]);
	useImage(imgAtlas, br, boxTex[3]);
	useImage(imgAtlas, r, boxTex[4]);
	useImage(imgAtlas, tr, boxTex[5]);
	useImage(imgAtlas, b, boxTex[6]);
	useImage(imgAtlas, m, boxTex[7]);
	useImage(imgAtlas, t, boxTex[8]);

	if (atlasTexture == 0)
		glGenTextures(1, &atlasTexture);

	glBindTexture(GL_TEXTURE_2D, atlasTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, dimensions.x, dimensions.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgAtlas->getData());

	glBindTexture(GL_TEXTURE_2D, 0);
}

void BoxPool::createVao()
{
	if (!vao)
		vao = new VertexArrayObject(GL_POINTS);
}

void BoxPool::createShader()
{
	if (!shader)
	{
		shader = new Shader();
		auto vertSrc = ShaderSource("boxVert", NineBox::getVertSrc());
		auto fragSrc = ShaderSource("boxFrag", NineBox::getFragSrc());
		auto geomSrc = ShaderSource("boxGeom", NineBox::getGeomSrc());
		shader->create(&vertSrc, &fragSrc, &geomSrc);
	}
}

void BoxPool::updateVao(size_t index)
{
	static VertexFormat format({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT}, {VertexAttrib::TEXCOORD, 1, 4, GL_FLOAT}, {VertexAttrib::COLOR, 2, 4, GL_FLOAT}, {VertexAttrib::OTHER, 3, 1, GL_FLOAT} });

	if (index >= boxes.size())
		return;

	auto *box = boxes[index];

	for (int i = 0; i < 9; i++)
	{
		if (boxes[index]->isHidden())
			boxVerts[index * 9 + i].pos = Math::vec4(0, 0, 0, 0);
		else
			boxVerts[index * 9 + i].pos = box->getVerts()[i];
		if (!box->isEmpty())
		{
			boxVerts[index * 9 + i].tex = box->getTexes()[i];
			boxVerts[index * 9 + i].col = box->getColors()[i];
			boxVerts[index * 9 + i].z = box->getZ();
		}
	}

	box->setDirty(false);
}

void BoxPool::render()
{
	static VertexFormat format({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT}, {VertexAttrib::TEXCOORD, 1, 4, GL_FLOAT}, {VertexAttrib::COLOR, 2, 4, GL_FLOAT}, {VertexAttrib::OTHER, 3, 1, GL_FLOAT} });

	createVao();
	createShader();

	if (!dirtyBoxes.empty())
	{
		for (auto i : dirtyBoxes)
			updateVao(i);
		dirtyBoxes.clear();
		vao->create(format, &boxVerts[0].pos.x, sizeof(BoxVert) * boxVerts.size(), sizeof(BoxVert));
	}
	else if (dirtyFlag)
	{
		vao->create(format, &boxVerts[0].pos.x, sizeof(BoxVert) * boxVerts.size(), sizeof(BoxVert));
		dirtyFlag = false;
	}

	if (!vao->isGenerated())
		return;

	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0);

	shader->bind();

	Math::mat4 pMatrix = Math::getOrtho<float>(0, Platform::Window::getWidth(), Platform::Window::getHeight(), 0, -1000, 1000);
	shader->setUniform("pMatrix", pMatrix);
	shader->setUniform("useTexture", imgAtlas != nullptr);

	if (imgAtlas)
	{
		glBindTexture(GL_TEXTURE_2D, atlasTexture);
		auto loc = shader->getUniform("texture");
		glUniform1i(loc, 0);
	}

	vao->render();

	glBindTexture(GL_TEXTURE_2D, 0);

	shader->unbind();

	glPopAttrib();
}

