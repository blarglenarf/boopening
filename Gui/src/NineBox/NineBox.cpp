#include "NineBox.h"
#include "BoxPool.h"

#include "../../../Renderer/src/RenderResource/ImageAtlas.h"
#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/RenderResource/ShaderSource.h"
#include "../../../Platform/src/Window.h"

using namespace Gui;
using namespace Rendering;


static void useImage(ImageAtlas *atlas, Image *img, Math::vec4 &texCoord)
{
	if (!img)
	{
		texCoord.x = 0;
		texCoord.y = 0;
		texCoord.z = 0;
		texCoord.w = 0;
		return;
	}

	// If image is already in the atlas, use that.
	if (!atlas->hasImage(img))
		atlas->add(img);

	auto rect = atlas->getTexCoords(img);
	texCoord.x = rect.left();
	texCoord.y = rect.bottom();
	texCoord.z = rect.right();
	texCoord.w = rect.top();
}


NineBox::NineBox() : z(-999), currBackground(0), backgroundSet(0), dirtyFlag(false), hiddenFlag(false), parent(nullptr), pool(nullptr), poolIndex(0), imgAtlas(nullptr), atlasTexture(0), vao(nullptr), shader(nullptr)
{
	backgrounds.reserve(3);

	// Needs a default size. If it's size 0 then you can't set the border properly.
	setPos(0, 0);
	setSize(100, 100);
}

NineBox::~NineBox()
{
	delete imgAtlas;
	delete vao;
	delete shader;

	if (atlasTexture != 0)
		glDeleteTextures(1, &atlasTexture);
}

void NineBox::setVerts(Math::vec4 bl, Math::vec4 l, Math::vec4 tl, Math::vec4 br, Math::vec4 r, Math::vec4 tr, Math::vec4 b, Math::vec4 m, Math::vec4 t)
{
	boxVert[0] = bl;
	boxVert[1] = l;
	boxVert[2] = tl;
	boxVert[3] = br;
	boxVert[4] = r;
	boxVert[5] = tr;
	boxVert[6] = b;
	boxVert[7] = m;
	boxVert[8] = t;

	pos = bl.xy;
	size = tr.zw - bl.xy;

	setDirty(true);
}

void NineBox::setVerts(float h1, float h2, float h3, float h4, float v1, float v2, float v3, float v4)
{
	boxVert[0] = Math::vec4(h1, v1, h2, v2);
	boxVert[1] = Math::vec4(h1, v2, h2, v3);
	boxVert[2] = Math::vec4(h1, v3, h2, v4);
	boxVert[3] = Math::vec4(h3, v1, h4, v2);
	boxVert[4] = Math::vec4(h3, v2, h4, v3);
	boxVert[5] = Math::vec4(h3, v3, h4, v4);
	boxVert[6] = Math::vec4(h2, v1, h3, v2);
	boxVert[7] = Math::vec4(h2, v2, h3, v3);
	boxVert[8] = Math::vec4(h2, v3, h3, v4);

	pos = Math::vec2(h1, v1);
	size = Math::vec2(h4 - h1, v4 - v1);

	setDirty(true);
}

void NineBox::setColors(Math::vec4 bl, Math::vec4 l, Math::vec4 tl, Math::vec4 br, Math::vec4 r, Math::vec4 tr, Math::vec4 b, Math::vec4 m, Math::vec4 t, size_t background)
{
	if (background >= backgrounds.size())
		backgrounds.resize(background + 1);

	auto *boxColor = backgrounds[background].boxColor;

	boxColor[0] = bl;
	boxColor[1] = l;
	boxColor[2] = tl;
	boxColor[3] = br;
	boxColor[4] = r;
	boxColor[5] = tr;
	boxColor[6] = b;
	boxColor[7] = m;
	boxColor[8] = t;

	setDirty(true);
}

void NineBox::setBorder(float w, float h)
{
	float h1 = boxVert[0].x;
	float h2 = boxVert[0].z;
	float h3 = boxVert[3].x;
	float h4 = boxVert[3].z;
	float v1 = boxVert[0].y;
	float v2 = boxVert[0].w;
	float v3 = boxVert[2].y;
	float v4 = boxVert[2].w;

	float leftWidth = h2 - h1;
	float rightWidth = h4 - h3;
	float topHeight = v4 - v3;
	float botHeight = v2 - v1;

	if (leftWidth == rightWidth && leftWidth == w && topHeight == botHeight && topHeight == h)
		return;

	if (h1 + w <= h4 - w)
	{
		h2 = h1 + w;
		h3 = h4 - w;
	}

	if (v1 + h <= v4 - h)
	{
		v2 = v1 + h;
		v3 = v4 - h;
	}

	setVerts(h1, h2, h3, h4, v1, v2, v3, v4);
}

void NineBox::setSize(float w, float h)
{
	float diffX = size.x - w;
	float diffY = size.y - h;

	if (diffX == 0 && diffY == 0)
		return;

	float h1 = boxVert[0].x;
	float h2 = boxVert[0].z;
	float h3 = boxVert[3].x;
	float h4 = boxVert[3].z;
	float v1 = boxVert[0].y;
	float v2 = boxVert[0].w;
	float v3 = boxVert[2].y;
	float v4 = boxVert[2].w;

	float leftWidth = h2 - h1;
	float rightWidth = h4 - h3;
	float topHeight = v4 - v3;
	float botHeight = v2 - v1;

	float newWidth = w - (leftWidth + rightWidth);
	float newHeight = h - (topHeight + botHeight);

	if (newWidth < 0 && newHeight < 0)
		return;

	if (newWidth >= 0)
	{
		h3 = h2 + newWidth;
		h4 = h3 + rightWidth;
	}

	if (newHeight >= 0)
	{
		v3 = v2 + newHeight;
		v4 = v3 + topHeight;
	}
	setVerts(h1, h2, h3, h4, v1, v2, v3, v4);
}

void NineBox::setPos(float x, float y)
{
	float diffX = pos.x - x;
	float diffY = pos.y - y;

	if (diffX == 0 && diffY == 0)
		return;

	for (int i = 0; i < 9; i++)
	{
		boxVert[i].x -= diffX;
		boxVert[i].y -= diffY;
		boxVert[i].z -= diffX;
		boxVert[i].w -= diffY;
	}

	pos.x = x;
	pos.y = y;

	setDirty(true);
}

void NineBox::useImagesInPool(Image *bl, Image *l, Image *tl, Image *br, Image *r, Image *tr, Image *b, Image *m, Image *t, size_t background)
{
	if (!pool)
		return;
	if (background >= backgrounds.size())
		backgrounds.resize(background + 1);

	pool->useImages(poolIndex, background, bl, l, tl, br, r, tr, b, m, t);

	setDirty(true);
}

void NineBox::useImagesLocal(Image *bl, Image *l, Image *tl, Image *br, Image *r, Image *tr, Image *b, Image *m, Image *t, size_t background, bool owns)
{
	if (background >= backgrounds.size())
		backgrounds.resize(background + 1);

	// 1024x1024 texture maybe?
	int imgSize = 1024;
	int channels = 4;
	Math::ivec2 dimensions(imgSize, imgSize);

	if (!imgAtlas)
	{
		imgAtlas = new ImageAtlas();
		imgAtlas->init(dimensions.x * dimensions.y * channels, channels, dimensions, owns);

		// Need to start by adding a small black section to the bottom to allow for boxes with no texturing.
		imgAtlas->addBlank(2, 2);
	}

	auto *boxTex = backgrounds[background].boxTex;

	useImage(imgAtlas, bl, boxTex[0]);
	useImage(imgAtlas, l, boxTex[1]);
	useImage(imgAtlas, tl, boxTex[2]);
	useImage(imgAtlas, br, boxTex[3]);
	useImage(imgAtlas, r, boxTex[4]);
	useImage(imgAtlas, tr, boxTex[5]);
	useImage(imgAtlas, b, boxTex[6]);
	useImage(imgAtlas, m, boxTex[7]);
	useImage(imgAtlas, t, boxTex[8]);

	if (atlasTexture == 0)
		glGenTextures(1, &atlasTexture);

	glBindTexture(GL_TEXTURE_2D, atlasTexture);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, dimensions.x, dimensions.y, 0, GL_RGBA, GL_UNSIGNED_BYTE, imgAtlas->getData());

	glBindTexture(GL_TEXTURE_2D, 0);

	dirtyFlag = true;
}

void NineBox::createVao()
{
	if (!dirtyFlag)
		return;

	struct BoxVert
	{
		Math::vec4 pos;
		Math::vec4 tex;
		Math::vec4 col;
		float z;
	};
	static std::vector<BoxVert> boxes(9);
	static VertexFormat format({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT}, {VertexAttrib::TEXCOORD, 1, 4, GL_FLOAT}, {VertexAttrib::COLOR, 2, 4, GL_FLOAT}, {VertexAttrib::OTHER, 3, 1, GL_FLOAT} });

	auto *boxTex = backgrounds[currBackground].boxTex;
	auto *boxColor = backgrounds[currBackground].boxColor;

	for (int i = 0; i < 9; i++)
	{
		if (hiddenFlag)
			boxes[i].pos = Math::vec4(0, 0, 0, 0);
		else
			boxes[i].pos = boxVert[i];
		boxes[i].tex = boxTex[i];
		boxes[i].col = boxColor[i];
		boxes[i].z = z;
	}

	if (!vao)
		vao = new VertexArrayObject(GL_POINTS);
	vao->create(format, &boxes[0].pos.x, sizeof(BoxVert) * 9, sizeof(BoxVert));

	dirtyFlag = false;
}

void NineBox::createShader()
{
	if (!shader)
	{
		shader = new Shader();
		auto vertSrc = ShaderSource("boxVert", getVertSrc());
		auto fragSrc = ShaderSource("boxFrag", getFragSrc());
		auto geomSrc = ShaderSource("boxGeom", getGeomSrc());
		shader->create(&vertSrc, &fragSrc, &geomSrc);
	}
}

bool NineBox::useBackground(size_t index)
{
	if (index >= backgrounds.size())
		return false;
	currBackground = index;

	setDirty(true);
	return true;
}

void NineBox::hide()
{
	if (hiddenFlag)
		return;
	hiddenFlag = true;

	dirtyFlag = true;
	if (pool)
	{
		pool->removeBox(this);
		poolIndex = 0;
	}
}

void NineBox::unhide()
{
	if (!hiddenFlag)
		return;
	hiddenFlag = false;

	dirtyFlag = true;
	if (pool)
		pool->addBox(this);
}

void NineBox::render()
{
	if (hiddenFlag)
		return;

	createShader();
	createVao();

	// Mostly just here for testing/debugging. Use a BoxPool to render everything at once (ideally).
	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glActiveTexture(GL_TEXTURE0);

	shader->bind();

	Math::mat4 pMatrix = Math::getOrtho<float>(0, Platform::Window::getWidth(), Platform::Window::getHeight(), 0, -1000, 1000);
	shader->setUniform("pMatrix", pMatrix);
	shader->setUniform("useTexture", imgAtlas != nullptr);

	if (imgAtlas)
	{
		glBindTexture(GL_TEXTURE_2D, atlasTexture);
		auto loc = shader->getUniform("texture");
		glUniform1i(loc, 0);
	}

	vao->render();

	glBindTexture(GL_TEXTURE_2D, 0);

	shader->unbind();

	glPopAttrib();
}

void NineBox::setDirty(bool dirtyFlag)
{
	if (!this->dirtyFlag && pool && dirtyFlag)
		pool->addDirty(poolIndex);
	this->dirtyFlag = dirtyFlag;
}

// TODO: Decide if z really needs to be an attribute, or if it's better as a uniform.
std::string NineBox::getVertSrc()
{
	return "\
	#version 430\n\
	\
	layout(location = 0) in vec4 pos;\
	layout(location = 1) in vec4 tex;\
	layout(location = 2) in vec4 col;\
	layout(location = 3) in float z;\
	\
	out vec4 vertPos;\
	out vec4 vertTex;\
	out vec4 vertCol;\
	out float vertZ;\
	\
	void main()\
	{\
		vertPos = pos;\
		vertTex = tex;\
		vertCol = col;\
		vertZ = z;\
	}\
	";
}

std::string NineBox::getGeomSrc()
{
	return "\
	#version 430\n\
	\
	layout(points) in;\n\
	layout(triangle_strip, max_vertices = 4) out;\n\
	\
	in vec4 vertPos[1];\
	in vec4 vertTex[1];\
	in vec4 vertCol[1];\
	in float vertZ[1];\
	\
	uniform mat4 pMatrix;\
	\
	out vec2 texCoord;\
	out vec4 color;\
	\
	void main()\
	{\
		vec4 esVert;\
		\
		esVert = vec4(vertPos[0].x, vertPos[0].y, vertZ[0], 1);\
		texCoord = vec2(vertTex[0].x, vertTex[0].w);\
		color = vertCol[0];\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vertPos[0].x, vertPos[0].w, vertZ[0], 1);\
		texCoord = vec2(vertTex[0].x, vertTex[0].y);\
		color = vertCol[0];\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vertPos[0].z, vertPos[0].y, vertZ[0], 1);\
		texCoord = vec2(vertTex[0].z, vertTex[0].w);\
		color = vertCol[0];\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		esVert = vec4(vertPos[0].z, vertPos[0].w, vertZ[0], 1);\
		texCoord = vec2(vertTex[0].z, vertTex[0].y);\
		color = vertCol[0];\
		gl_Position = pMatrix * esVert;\
		EmitVertex();\
		\
		EndPrimitive();\
	}\
	";
}

std::string NineBox::getFragSrc()
{
	return "\
	#version 430\n\
	\
	in vec2 texCoord;\
	in vec4 color;\
	\
	uniform sampler2D texture;\
	uniform bool useTexture;\
	\
	out vec4 fragColor;\
	\
	void main()\
	{\
		fragColor = color;\
		\
		if (useTexture) \
			fragColor += texture2D(texture, texCoord);\
	}";
}

