#pragma once

#include "../../../Math/src/Vector/VectorCommon.h"
#include "../../../Renderer/src/GL.h"

#include <vector>

namespace Rendering
{
	class Image;
	class ImageAtlas;
	class VertexArrayObject;
	class Shader;
}

namespace Gui
{
	class BoxPool;

	class NineBox
	{
	private:
		struct Background
		{
			Math::vec4 boxTex[9];
			Math::vec4 boxColor[9]; // TODO: Gradients.
		};

	public:
		// BL, L, TL, BR, R, TR, B, M, T.
		// Left, bottom, right, top.
		Math::vec4 boxVert[9]; // TODO: Corner rounding.
		std::vector<Background> backgrounds;
		float z;

		// Bottom left, width/height.
		Math::vec2 pos;
		Math::vec2 size;

		size_t currBackground;
		size_t backgroundSet;

		bool dirtyFlag;
		bool hiddenFlag;

		// Parent only useful for boxPool rendering (to ensure correct render order).
		NineBox *parent;

		BoxPool *pool;
		size_t poolIndex;

		// ImageAtlas/texture/vao/shader if the NineBox isn't using a BoxPool.
		Rendering::ImageAtlas *imgAtlas;
		GLuint atlasTexture;

		Rendering::VertexArrayObject *vao;
		Rendering::Shader *shader;

	public:
		NineBox();
		~NineBox();

		// h->horizontal, v->vertical, 1-4 from left to right or bottom to top.
		void setVerts(Math::vec4 bl, Math::vec4 l, Math::vec4 tl, Math::vec4 br, Math::vec4 r, Math::vec4 tr, Math::vec4 b, Math::vec4 m, Math::vec4 t);
		void setVerts(float h1, float h2, float h3, float h4, float v1, float v2, float v3, float v4);
		void setColors(Math::vec4 bl, Math::vec4 l, Math::vec4 tl, Math::vec4 br, Math::vec4 r, Math::vec4 tr, Math::vec4 b, Math::vec4 m, Math::vec4 t, size_t background = 0);

		void setBorder(float w, float h);
		void setSize(float w, float h);
		void setPos(float x, float y);

		// Only works if you use a pool, otherwise does nothing.
		void useImagesInPool(Rendering::Image *bl, Rendering::Image *l, Rendering::Image *tl, Rendering::Image *br, Rendering::Image *r, Rendering::Image *tr, Rendering::Image *b, Rendering::Image *m, Rendering::Image *t, size_t background = 0);

		// These functions are here if the NineBox doesn't use a BoxPool.
		void useImagesLocal(Rendering::Image *bl, Rendering::Image *l, Rendering::Image *tl, Rendering::Image *br, Rendering::Image *r, Rendering::Image *tr, Rendering::Image *b, Rendering::Image *m, Rendering::Image *t, size_t background = 0, bool owns = false);
		void createVao();
		void createShader();

		size_t getNBackgrounds() const { return backgrounds.size(); }
		bool useBackground(size_t index);

		void useBackgroundSet(size_t set) { backgroundSet = set; }
		void useStandardBackground() { useBackground(backgroundSet * 3 + 0); }
		void useHoverBackground() { useBackground(backgroundSet * 3 + 1); }
		void useClickBackground() { useBackground(backgroundSet * 3 + 2); }

		void hide();
		void unhide();

		// Can either render it directly here, or render using a BoxPool (faster for multiple boxes).
		void render();

		void setZ(float z) { this->z = z; setDirty(true); }
		float getZ() const { return z; }

		Math::vec2 getPos() const { return pos; }
		Math::vec2 getInnerPos() const { return Math::vec2(boxVert[7].x, boxVert[7].y); }
		Math::vec2 getSize() const { return size; }
		Math::vec2 getInnerSize() const { return Math::vec2(boxVert[7].z - boxVert[7].x, boxVert[7].w - boxVert[7].y); }

		bool isDirty() const { return dirtyFlag; }
		void setDirty(bool dirtyFlag);

		bool isHidden() const { return hiddenFlag; }
		bool isInside(int x, int y) const { return x >= pos.x && x <= (pos.x + size.x) && y >= pos.y && y <= (pos.y + size.y); }

		bool isEmpty() { return backgrounds.empty(); }

		Math::vec4 *getVerts() { return boxVert; }
		Math::vec4* getTexes(size_t background) { if (backgrounds.empty()) return nullptr; return backgrounds[background].boxTex; }
		Math::vec4* getColors(size_t background) { if (backgrounds.empty()) return nullptr; return backgrounds[background].boxColor; }

		Math::vec4* getTexes() { if (backgrounds.empty()) return nullptr; return backgrounds[currBackground].boxTex; }
		Math::vec4* getColors() { if (backgrounds.empty()) return nullptr; return backgrounds[currBackground].boxColor; }

		void setPool(BoxPool *pool, size_t index) { this->pool = pool; poolIndex = index; }
		size_t getPoolIndex() const { return poolIndex; }

		void setParent(NineBox *parent) { this->parent = parent; }
		NineBox *getParent() { return parent; }

		static std::string getVertSrc();
		static std::string getGeomSrc();
		static std::string getFragSrc();
	};
}

