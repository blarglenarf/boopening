#pragma once

#include "NineBox.h"

#include <vector>

namespace Gui
{
	class BoxPool
	{
	private:
		struct BoxVert
		{
			Math::vec4 pos;
			Math::vec4 tex;
			Math::vec4 col;
			float z;
		};

	private:
		// TODO: Decide if I want the boxes stored here or elsewhere.
		std::vector<NineBox*> boxes;
		std::vector<size_t> dirtyBoxes;

		std::vector<size_t> removedBoxes;

		// Covers all boxes in the pool.
		Rendering::ImageAtlas *imgAtlas;
		GLuint atlasTexture;

		Rendering::VertexArrayObject *vao;
		Rendering::Shader *shader;

		std::vector<BoxVert> boxVerts;
		bool dirtyFlag;

	public:
		BoxPool();
		~BoxPool();

		size_t addBox(NineBox *box);
		NineBox *getBox(size_t index);

		bool checkIntegrity();

		void removeBox(NineBox *nineBox);

		void addDirty(size_t index);

		void useImages(size_t index, size_t background, Rendering::Image *bl, Rendering::Image *l, Rendering::Image *tl, Rendering::Image *br, Rendering::Image *r, Rendering::Image *tr, Rendering::Image *b, Rendering::Image *m, Rendering::Image *t);
	
		void createVao();
		void createShader();

		void updateVao(size_t index);

		void render();
	};
}

