#pragma once

#include "../../../Math/src/Vector/VectorCommon.h"

namespace Gui
{
	class Style
	{
	private:
		Math::vec3 pos;
		Math::vec2 size;

	public:
		Style() {}
		~Style() {}

		void setPos(Math::vec3 pos) { this->pos = pos; }
		Math::vec3 getPos() const { return pos; }

		void setSize(Math::vec2 size) { this->size = size; }
		Math::vec2 getSize() const { return size; }
	};
}

