#include "TextPool.h"

#include "Text.h"

using namespace Gui;


size_t TextPool::addText(Text *text)
{
	// Only add the text if it's not already here.
	for (size_t i = 0; i < texts.size(); i++)
	{
		if (texts[i] == text)
		{
			texts[i]->setPool(this, i);
			return i;
		}
	}

	texts.push_back(text);
	size_t index = texts.size() - 1;
	texts[index]->setPool(this, index);
	return index;
}

Text *TextPool::getText(size_t index)
{
	if (index >= texts.size())
		return nullptr;
	return texts[index];
}

void TextPool::removeText(Text *text)
{
	// Only remove the text if it's here.
	// TODO: Could just use the index stored in the text I guess, not sure how trustworthy it is.
	size_t index = 0;
	for (auto it = texts.begin(); it != texts.end(); it++)
	{
		if (*it == text)
		{
			texts.erase(it);
			break;
		}
		index++;
	}
}

void TextPool::render()
{
	// TODO: Render texts all at once, rather than individually.
	for (auto *text : texts)
		text->render();
}

