#include "Text.h"

#include "TextPool.h"
#include "../../../Renderer/src/RenderResource/Font.h"

using namespace Gui;


void Text::hide()
{
	if (hiddenFlag)
		return;
	hiddenFlag = true;

	if (pool)
	{
		pool->removeText(this);
		poolIndex = 0;
	}
}

void Text::unhide()
{
	if (!hiddenFlag)
		return;
	hiddenFlag = false;

	if (pool)
		pool->addText(this);
}

void Text::render()
{
	if (hiddenFlag)
		return;

	if (cached || wrapFlag)
		font->renderFromCache(str, pos.x, pos.y, z, color.r, color.g, color.b, maxSize.width, maxSize.height, linePadding, vertScroll, sideScroll, wrapFlag);
	else
		font->renderFromAtlas(str, pos.x, pos.y, z, color.r, color.g, color.b, maxSize.width, maxSize.height, linePadding, vertScroll, sideScroll);
}

void Text::calcTextSize()
{
	if (str == "" || font == nullptr)
		textSize = Math::ivec2(0, 0);
	else
	{
		if (wrapFlag && cached)
			textSize = font->getTextSize(str, maxSize.width, maxSize.height, linePadding);
		else
			textSize = font->getTextSize(str, 0, 0, linePadding);
	}
}

Math::ivec3 Text::getCursorPos(int xOffset, int yOffset)
{
	if (str == "" || font == nullptr)
		return Math::ivec3(0, 0, 0);
	return font->getCursorPos(str, xOffset, yOffset, -1, maxSize.width, linePadding, vertScroll, sideScroll);
}

Math::ivec3 Text::getCursorPos(int charPos)
{
	if (str == "" || font == nullptr)
		return Math::ivec3(0, 0, 0);
	return font->getCursorPos(str, -1, -1, charPos, maxSize.width, linePadding, vertScroll, sideScroll);
}

int Text::getFontSize() const
{
	return font->getSize();
}

