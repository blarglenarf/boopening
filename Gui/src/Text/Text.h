#pragma once

#include "../../../Math/src/Vector/VectorCommon.h"

#include <string>

namespace Rendering
{
	class Font;
}

namespace Gui
{
	class TextPool;

	class Text
	{
	private:
		std::string str;
		bool cached;

		Rendering::Font *font;

		Math::ivec2 pos;
		Math::vec3 color;

		float z;

		Math::ivec2 maxSize;
		Math::ivec2 textSize;

		bool hiddenFlag;
		bool wrapFlag;

		TextPool *pool;
		size_t poolIndex;

		int linePadding;

		int vertScroll;
		int sideScroll;

	public:
		Text(const std::string &str = "", Rendering::Font *font = nullptr) : str(str), cached(false), font(font), color(1, 1, 1), z(-999.5f), maxSize(0, 0), hiddenFlag(false), wrapFlag(false), pool(nullptr), poolIndex(0), linePadding(0), vertScroll(0), sideScroll(0) {}
		virtual ~Text() {}

		void hide();
		void unhide();

		void render();

		void calcTextSize();
		Math::ivec2 getTextSize() const { return textSize; }

		Math::ivec3 getCursorPos(int xOffset, int yOffset);
		Math::ivec3 getCursorPos(int charPos);

		int getFontSize() const;

		int getWidth() const { return textSize.width; }
		int getHeight() const { return textSize.height; }

		void setWrapping(bool wrapFlag) { this->wrapFlag = wrapFlag; }
		bool isWrapping() const { return wrapFlag; }
		bool isHidden() const { return hiddenFlag; }

		void setCached(bool cached) { this->cached = cached; }
		bool isCached() const { return cached; }

		void setStr(const std::string &str) { this->str = str; calcTextSize(); }
		const std::string &getStr() const { return str; }

		void setFont(Rendering::Font *font) { this->font = font; calcTextSize(); }
		Rendering::Font *getFont() { return font; }

		void setPos(Math::ivec2 pos) { this->pos = pos; }
		Math::ivec2 getPos() const { return pos; }

		void setColor(Math::vec3 color) { this->color = color; }
		Math::vec3 getColor() const { return color; }

		float getZ() const { return z; }
		void setZ(float z) { this->z = z; }

		void setMaxSize(Math::ivec2 maxSize) { this->maxSize = maxSize; }
		Math::ivec2 getMaxSize() const { return maxSize; }

		void setPool(TextPool *pool, size_t index) { this->pool = pool; poolIndex = index; }
		size_t getPoolIndex() const { return poolIndex; }

		void setLinePadding(int linePadding) { this->linePadding = linePadding; }
		int getLinePadding() const { return linePadding; }

		void setVertScroll(int vertScroll) { this->vertScroll = vertScroll; }
		int getVertScroll() const { return vertScroll; }

		void setSideScroll(int sideScroll) { this->sideScroll = sideScroll; }
		int getSideScroll() const { return sideScroll; }
	};
}

