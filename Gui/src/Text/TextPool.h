#pragma once

#include "../../../Math/src/Vector/VectorCommon.h"

#include <vector>

namespace Rendering
{
	class Font;
}

namespace Gui
{
	class Text;

	class TextPool
	{
	private:
		Rendering::Font *font;

		std::vector<Text*> texts;

		//std::vector<size_t> dirtyBoxes;

		// Covers all boxes in the pool.
		//Rendering::ImageAtlas *imgAtlas;
		//GLuint atlasTexture;

		//Rendering::VertexArrayObject *vao;
		//Rendering::Shader *shader;

		//std::vector<BoxVert> boxVerts;
		//bool dirtyFlag;

	public:
		TextPool(Rendering::Font *font = nullptr) : font(font) {}
		virtual ~TextPool() {}

		size_t addText(Text *text);
		Text *getText(size_t index);

		void removeText(Text *text);

		//void addDirty(size_t index);

		//void useImages(size_t index, size_t background, Rendering::Image *bl, Rendering::Image *l, Rendering::Image *tl, Rendering::Image *br, Rendering::Image *r, Rendering::Image *tr, Rendering::Image *b, Rendering::Image *m, Rendering::Image *t);
	
		//void createVao();
		//void createShader();

		//void updateVao(size_t index);

		void render();
	};
}

