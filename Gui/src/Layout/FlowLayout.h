#pragma once

#include "LayoutStrat.h"

#include <vector>

#undef ABSOLUTE
#undef RELATIVE

namespace Gui
{
	class Widget;

	class FlowPos
	{
	public:
		enum Type
		{
			FREE, // No fancy layout, widgets are arranged side-by-side wherever there is room.
			ABSOLUTE, // Absolute pixel positions used.
			RELATIVE // Relative positions (percentages) used.
		};

	private:
		Type type;

		float x;
		float y;

		float width;
		float height;

		Widget *widget;

	public:
		FlowPos(Widget *widget = nullptr, Type type = ABSOLUTE, float x = 0, float y = 0, float width = 0, float height = 0) : type(type), x(x), y(y), width(width), height(height), widget(widget) {}

		void setWidget(Widget *widget, Type type) { this->widget = widget; this->type = type; }

		Type getType() const { return type; }
		Widget *getWidget() { return widget; }

		float getX() const { return x; }
		float getY() const { return y; }

		float getWidth() const { return width; }
		float getHeight() const { return height; }

		void setX(float x) { this->x = x; }
		void setY(float y) { this->y = y; }

		void setWidth(float width) { this->width = width; }
		void setHeight(float height) { this->height = height; }

		void arrange();

		bool overlaps(FlowPos &flowPos, float padding);
	};

	class FlowLayout : public LayoutStrat
	{
	private:
		std::vector<FlowPos> flow;

	public:
		FlowLayout() : LayoutStrat() {}
		virtual ~FlowLayout() {}

		void addWidget(Widget *widget, FlowPos::Type type, float x, float y, float width, float height);

		std::vector<FlowPos> &getFlow() { return flow; }
		FlowPos *getFlowPos(Widget *widget);

		virtual void hide() override;
		virtual void unhide() override;

		virtual void arrangeContents() override;

		virtual bool empty() override { return flow.empty(); }
	};
}

