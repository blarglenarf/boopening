#include "Layout.h"

using namespace Gui;

void Layout::setPos(float x, float y)
{
	switch (type)
	{
	case FLOW:
		flow.setPos(x, y);
		break;
	case GRID:
		grid.setPos(x, y);
		break;
	default:
		break;
	}
}

void Layout::setSize(float w, float h)
{
	switch (type)
	{
	case FLOW:
		flow.setSize(w, h);
		break;
	case GRID:
		grid.setSize(w, h);
		break;
	default:
		break;
	}
}

void Layout::setPadding(float x, float y)
{
	switch (type)
	{
	case FLOW:
		flow.setPadding(x, y);
		break;
	case GRID:
		grid.setPadding(x, y);
		break;
	default:
		break;
	}
}

void Layout::hide()
{
	switch (type)
	{
	case FLOW:
		flow.hide();
		break;
	case GRID:
		grid.hide();
		break;
	default:
		break;
	}
}

void Layout::unhide()
{
	switch (type)
	{
	case FLOW:
		flow.unhide();
		break;
	case GRID:
		grid.unhide();
		break;
	default:
		break;
	}
}

void Layout::arrangeContents()
{
	switch (type)
	{
	case FLOW:
		flow.arrangeContents();
		break;
	case GRID:
		grid.arrangeContents();
		break;
	default:
		break;
	}
}

bool Layout::empty()
{
	switch (type)
	{
	case FLOW:
		return flow.empty();
		break;
	case GRID:
		return grid.empty();
		break;
	default:
		break;
	}
	return true;
}

