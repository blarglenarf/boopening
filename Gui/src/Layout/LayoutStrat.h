#pragma once

namespace Gui
{
	class LayoutStrat
	{
	protected:
		float x;
		float y;

		float width;
		float height;

		float xPadding;
		float yPadding;

	public:
		LayoutStrat() : x(0), y(0), width(0), height(0), xPadding(0), yPadding(0) {}
		virtual ~LayoutStrat() {}

		float getX() const { return x; }
		float getY() const { return y; }

		float getWidth() const { return width; }
		float getHeight() const { return height; }

		float getXPadding() const { return xPadding; }
		float getYPadding() const { return yPadding; }

		virtual void setPos(float x, float y);
		virtual void setSize(float w, float h);

		virtual void setPadding(float x, float y);

		virtual void hide() = 0;
		virtual void unhide() = 0;

		virtual void arrangeContents() = 0;

		virtual bool empty() = 0;
	};
}

