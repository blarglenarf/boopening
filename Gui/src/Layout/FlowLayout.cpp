#include "FlowLayout.h"

#include "../Widgets/Widget.h"

#undef min
#undef max

#include <algorithm>

using namespace Gui;

void FlowPos::arrange()
{
	if (!widget)
		return;

	// TODO: Handling difference between absolute/relative positions should probably be done by the FlowLayout class.
	widget->setPos(x, y);
	widget->setSize(width, height);
}

bool FlowPos::overlaps(FlowPos &flowPos, float padding)
{
	if (!widget || !flowPos.widget)
		return false;

	float xMin1 = x;
	float yMin1 = y;
	float xMin2 = flowPos.x;
	float yMin2 = flowPos.y;

	float xMax1 = x + width + padding;
	float yMax1 = y + height + padding;
	float xMax2 = flowPos.x + flowPos.width + padding;
	float yMax2 = flowPos.y + flowPos.height + padding;

	//if (x < flowPos.x + flowPos.width + padding && x + width + padding > flowPos.x && y > flowPos.y + flowPos.height + padding && y + height < flowPos.y)
	if (xMax1 >= xMin2 && xMax2 >= xMin1 && yMax1 >= yMin2 && yMax2 >= yMin1)
		return true;

	return false;
}


void FlowLayout::addWidget(Widget *widget, FlowPos::Type type, float x, float y, float width, float height)
{
	flow.push_back(FlowPos(widget, type, x, y, width, height));

	arrangeContents();
}

FlowPos *FlowLayout::getFlowPos(Widget *widget)
{
	for (auto& pos : flow)
		if (pos.getWidget() == widget)
			return &pos;
	return nullptr;
}

void FlowLayout::hide()
{
	for (auto &pos : flow)
		pos.getWidget()->hide();
}

void FlowLayout::unhide()
{
	for (auto &pos : flow)
		pos.getWidget()->unhide();
}

void FlowLayout::arrangeContents()
{
	if (width <= 0)
		return;

	float xMin = x + xPadding;
	float yMin = y + yPadding;

	//float xMax = xMin + width - xPadding;
	//float yMax = yMin + height - yPadding;

	// Check if any widgets are too close or overlap or are outside the panel. If so move them.
	for (int i = 0; i < (int) flow.size(); i++)
	{
		//if (!flow[i].overlaps(flow[i - 1], xPadding))
		//	continue;

		// First make sure the widget is inside its allowed area, accounting for border/padding.
		// TODO: Handle both absolute and relative positions.
		auto &flowPos = flow[i];

		float flowX = flowPos.getX();
		float flowY = flowPos.getY();

		float flowWidth = flowPos.getWidth();
		float flowHeight = flowPos.getHeight();

		if (flowX < xMin)
			flowPos.setX(xMin);
		if (flowY < yMin)
			flowPos.setY(yMin);

		// Probably want to maintain a minimum size.
		if (flowPos.getWidget())
		{
			if (flowWidth > 0 && flowWidth > flowPos.getWidget()->getSize().x)
				flowPos.setWidth(flowWidth);
			if (flowHeight > 0 && flowHeight > flowPos.getWidget()->getSize().y)
				flowPos.setHeight(flowHeight);
		}

		bool overlapFlag = false;
		for (int j = 0; j < (int) flow.size(); j++)
		{
			if (i == j)
				continue;
			if (flow[i].overlaps(flow[j], xPadding))
			{
				overlapFlag = true;
				break;
			}
		}

		// Overlaps, move it (first widget can't overlap).
		if (overlapFlag && i != 0)
			flow[i].setX(flow[i - 1].getX() + flow[i - 1].getWidth() + xPadding);

		// Outside the panel? Move it to the next row.
		if (i > 0 && flow[i].getX() + flow[i].getWidth() > x + width)
		{
			// Keep track of row heights, since different widgets in a row can have varied height.
			flow[i].setX(x + xPadding);
			flow[i].setY(flow[i - 1].getY() + flow[i - 1].getHeight() + yPadding);
		}

		flow[i].arrange();
	}
}

