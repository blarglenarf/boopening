#include "LayoutStrat.h"

using namespace Gui;

void LayoutStrat::setPos(float x, float y)
{
	this->x = x;
	this->y = y;

	arrangeContents();
}

void LayoutStrat::setSize(float w, float h)
{
	width = w;
	height = h;

	arrangeContents();
}

void LayoutStrat::setPadding(float x, float y)
{
	xPadding = x;
	yPadding = y;

	arrangeContents();
}

