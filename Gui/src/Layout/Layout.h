#pragma once

#include "FlowLayout.h"
#include "GridLayout.h"

namespace Gui
{
	class Layout
	{
	public:
		enum Type
		{
			FLOW,
			GRID
		};

	private:
		FlowLayout flow;
		GridLayout grid;

		Type type;

	public:
		Layout() : type(GRID) {}
		~Layout() {}

		void setType(Layout::Type type) { this->type = type; }

		FlowLayout &flowLayout() { return flow; }
		GridLayout &gridLayout() { return grid; }

		// TODO: May want to split these now since flowLayout and gridLayout can be used simultaneously.
		void setPos(float x, float y);
		void setSize(float w, float h);

		void setPadding(float x, float y);

		void hide();
		void unhide();

		void arrangeContents();

		bool empty();
	};
}

