#include "GridLayout.h"

#include "../Widgets/Widget.h"

#undef min
#undef max

#include <algorithm>

using namespace Gui;


bool Column::arrange(float width, float height, float x, float y)
{
	this->width = width;
	this->height = height;

	if (!widget)
		return false;

	float leftX = x;
	float rightX = x + width - widget->getSize().x;
	float midX = (leftX + rightX) / 2.0f;

	float topY = y;
	float botY = y + height - widget->getSize().y;
	float midY = (topY + botY) / 2.0f;

	float newX = leftX;
	float newY = topY;

	if (xAlign == CENTER)
		newX = midX;
	else if (xAlign == RIGHT)
		newX = rightX;
	if (yAlign == CENTER)
		newY = midY;
	else if (yAlign == BOTTOM)
		newY = botY;

	widget->setPos(newX, newY);

	bool xSizeFlag = false;

	// If the widget is too large, it needs to be resized.
	// TODO: We may want the widget to fill the column.
	// TODO: If it's a dropdown, then the dropdown part can probably be ignored.
	auto widgetSize = widget->getSize();
	if (widgetSize.x > width)
		xSizeFlag = true;
	//if (widgetSize.x > width || widgetSize.y > height)
	//	widget->setSize(std::min(widgetSize.x, width), std::min(widgetSize.y, height));

	return xSizeFlag;
}


void Row::addColumn(int size)
{
	columns.push_back(Column(size));
}

void Row::addColumns(std::vector<int> columns)
{
	for (auto i : columns)
		this->columns.push_back(Column(i));
}

float Row::arrangeColumns(float x, float y, float width, float padding)
{
	this->x = x;
	this->y = y;
	this->width = width;

	if (columns.empty())
		return 0;

	// First determine how high the row needs to be.
	for (auto &column : columns)
		if (column.getWidget())
			height = std::max(height, column.getWidget()->getSize().y);

	// Remember that columns add to 12.
	float xPos = x + padding;
	for (auto &column : columns)
	{
		float colWidth = (width / 12.0f) * (float) column.getAmt();
		bool xSizeFlag = column.arrange(colWidth, height, xPos, y);
		float extra = xSizeFlag ? padding : 0;
		xPos += colWidth + extra;
	}

	return height;
}


void GridLayout::addWidget(Widget *widget, int row, int col, Column::Alignment xAlign, Column::Alignment yAlign)
{
	if (row >= (int) rows.size())
		return;
	auto &r = rows[row];

	if (col >= (int) r.size())
		return;
	auto &c = r.getColumn(col);

	c.setWidget(widget, xAlign, yAlign);

	arrangeContents();
}

void GridLayout::hide()
{
	for (auto &row : rows)
		for (int i = 0; i < (int) row.size(); i++)
			row.getColumn(i).getWidget()->hide();
}

void GridLayout::unhide()
{
	for (auto &row : rows)
		for (int i = 0; i < (int) row.size(); i++)
			row.getColumn(i).getWidget()->unhide();
}

void GridLayout::arrangeContents()
{
	// TODO: If everything doesn't fit, then it's the Panel's problem.
	if (width == 0 || height == 0)
		return;
	float yPos = y + yPadding;

	for (auto &row : rows)
	{
		// TODO: What if yPos is below y + height?
		yPos += row.arrangeColumns(x, yPos, width, xPadding) + yPadding;
	}
}

bool GridLayout::empty()
{
	if (rows.empty())
		return true;

	for (auto &row : rows)
		if (!row.empty())
			return false;

	return true;
}

