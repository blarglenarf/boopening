#pragma once

#include "LayoutStrat.h"

#include "../../../Math/src/Vector/VectorCommon.h"

#include <vector>

namespace Gui
{
	class Widget;

	class Column
	{
	public:
		enum Alignment
		{
			LEFT,
			RIGHT,
			TOP,
			BOTTOM,
			CENTER
		};

	private:
		Widget *widget;

		Alignment xAlign;
		Alignment yAlign;

		int amt;
		float width;
		float height;

	public:
		Column(int amt = 0) : widget(nullptr), xAlign(LEFT), yAlign(TOP), amt(amt), width(0), height(0) {}

		void setWidget(Widget *widget, Alignment xAlign = LEFT, Alignment yAlign = TOP) { this->widget = widget; this->xAlign = xAlign; this->yAlign = yAlign; }
		void setAmt(int amt) { this->amt = amt; }

		Widget *getWidget() { return widget; }
		int getAmt() const { return amt; }

		float getWidth() const { return width; }
		float getHeight() const { return height; }

		bool arrange(float width, float height, float x, float y);
	};

	class Row
	{
	private:
		// All Columns in a row add to 12.
		std::vector<Column> columns;

		float x;
		float y;

		float width;
		float height;
		float padding;

	public:
		Row() : x(0), y(0), width(0), height(0), padding(0) {}

		void addColumn(int size);
		void addColumns(std::vector<int> columns);

		void setHeight(float height) { this->height = height; }
		float getHeight() const { return height; }

		Column &getColumn(int column) { return columns[column]; }
		size_t size() const { return columns.size(); }

		void setPadding(float padding) { this->padding = padding; }
		float getPadding() const { return padding; }

		float arrangeColumns(float x, float y, float width, float padding);

		bool empty() { return columns.empty(); }
	};

	class GridLayout : public LayoutStrat
	{
	protected:
		std::vector<Row> rows;

	public:
		GridLayout() : LayoutStrat() {}
		virtual ~GridLayout() {}

		Row &addRow() { rows.push_back(Row()); return rows[rows.size() - 1]; }
		Row &getRow(int row) { return rows[row]; }

		void addWidget(Widget *widget, int row, int col, Column::Alignment xAlign = Column::LEFT, Column::Alignment yAlign = Column::TOP);

		virtual void hide() override;
		virtual void unhide() override;

		virtual void arrangeContents() override;

		virtual bool empty() override;
	};
}

