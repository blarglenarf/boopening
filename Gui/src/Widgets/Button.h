#pragma once

#include "Widget.h"
#include "TextOutput.h"

// Fuck you windows.
#undef min
#undef max
#undef DialogBox

namespace Gui
{
	// Pretty much just a label with text.
	class Button : public Label, public Clickable
	{
	public:
		Button() : Label(), Clickable() { hoverableFlag = true; type = "Button"; }
		virtual ~Button() {}

		//virtual void clickEvent(Platform::Event &ev) override { onClickFunc(this, ev); }
	};

	// Button that cycles through different text options.
	//class CycleButton : public Label, public Clickable // Used to not actually be a button, can't remember why.
	class CycleButton : public Button
	{
	protected:
		std::vector<std::string> options;
		size_t currOption;

	public:
		CycleButton(const std::vector<std::string> &options = std::vector<std::string>()) : Button(), options(options), currOption(0) { hoverableFlag = true; type = "CycleButton"; }
		virtual ~CycleButton() {}

		void setOptions(const std::vector<std::string> &options) { this->options = options; }
		void addOption(const std::string &option) { options.push_back(option); }
		void removeOption(const std::string &option);

		virtual void setStr(const std::string &str, int vertScroll = 0, int sideScroll = 0, bool resizeFlag = true) override;

		Math::ivec2 getMaxOptionSize();

		const std::string &getCurrOption() const;

		virtual bool clickEvent(Widget *w, Platform::Event &ev) override;
	};

	class SpinnerButton : public CycleButton
	{
		// How to do the button alignment...
	public:
		enum Alignment
		{
			TOP_TOP,
			TOP_BOT,
			BOT_TOP,
			BOT_BOT,
			LEFT_LEFT,
			LEFT_RIGHT,
			RIGHT_LEFT,
			RIGHT_RIGHT
		};

	protected:
		Button forward;
		Button back;

		Math::vec2 totalSize;
		Math::vec2 totalPos;

		bool initialPosFlag;
		bool initialSizeFlag;
		bool initialUpdateFlag;

		Alignment alignment;

	public:
		SpinnerButton(const std::vector<std::string> &options = std::vector<std::string>());
		virtual ~SpinnerButton() {}

		void setAlign(Alignment align) { alignment = align; updateButtonPos(); }

		void nextOption();
		void prevOption();

		Math::vec2 getRealPos() const { return CycleButton::getPos(); }
		Math::vec2 getRealSize() const { return CycleButton::getSize(); }

		virtual Math::vec2 getPos() const override { return totalPos; }
		virtual Math::vec2 getSize() const override { return totalSize; }

		void updateButtonPos();

		virtual void setPos(float x, float y) override; // Moves both the spinner and the buttons.
		virtual void setSize(float w, float h) override; // Needs to adjust size of the buttons.
		virtual void setBorder(float w, float h) override { forward.setBorder(w, h); back.setBorder(w, h); CycleButton::setBorder(w, h); }

		virtual bool clickEvent(Widget *w, Platform::Event &ev) override { onClickFunc(w, ev); return false; }

		Button &getForward() { return forward; }
		Button &getBack() { return back; }
	};

	// ToggleButton class needs to know about this.
	class ToggleGroup;

	// Button (can have text), and changes background when toggled on/off.
	class ToggleButton : public Button
	{
	protected:
		bool toggleFlag;

		ToggleGroup *toggleGroup;

	public:
		ToggleButton() : Button(), toggleFlag(false), toggleGroup(nullptr) { hoverableFlag = true; type = "ToggleButton"; }
		virtual ~ToggleButton() {}

		void updateBackground();

		bool isToggled() const { return toggleFlag; }
		void setToggled(bool toggleFlag) { this->toggleFlag = toggleFlag; }

		ToggleGroup *getGroup() { return toggleGroup; }
		void setGroup(ToggleGroup *toggleGroup) { this->toggleGroup = toggleGroup; }

		virtual bool clickEvent(Widget *w, Platform::Event &ev) override;
	};

	// Group of ToggleButtons, can either be used as radio buttons or checkboxes.
	class ToggleGroup : public Widget
	{
	protected:
		std::vector<ToggleButton*> toggleButtons;

		// Determines if only one can be selected, or multiple.
		bool uniqueFlag;

	public:
		ToggleGroup(bool uniqueFlag = false) : Widget(), uniqueFlag(uniqueFlag) { hoverableFlag = false; type = "ToggleGroup"; }
		virtual ~ToggleGroup() {}

		void setUnique(bool uniqueFlag) { this->uniqueFlag = uniqueFlag; }
		bool isUnique() const { return uniqueFlag; }

		ToggleButton *addButton(ToggleButton *button);
		ToggleButton *removeButton(ToggleButton *button);

		bool isToggled(ToggleButton *button) { return button->isToggled(); }
		void clearToggled() { for (auto *button : toggleButtons) { button->setToggled(false); button->updateBackground(); } }

		std::vector<ToggleButton*> &getButtons() { return toggleButtons; }
	};

	// Not super necessary, but I've put them in because they're fairly standard and expected widgets.
	class CheckboxGroup : public ToggleGroup
	{
	public:
		CheckboxGroup() : ToggleGroup(false) { type = "CheckboxGroup"; }
		virtual ~CheckboxGroup() {}
	};

	class RadioGroup : public ToggleGroup
	{
	public:
		RadioGroup() : ToggleGroup(true) { type = "RadioGroup"; }
		virtual ~RadioGroup() {}
	};

	class Slider; // SliderButton needs to know about it.

	// Really only intended to be used by the Slider class.
	class SliderButton : public Button, public Draggable
	{
	protected:
		// These are in real gui units, not an imaginary range.
		float min;
		float max;
		float curr;

		bool vertical;

		Slider *slider;

	public:
		SliderButton(float min = 0, float max = 0, bool vertical = true);
		virtual ~SliderButton() {}

		// This should only be used by the Slider class.
		void setSlider(Slider *slider) { this->slider = slider; }

		void setVertical(bool vertical) { this->vertical = vertical; }
		bool isVertical() const { return vertical; }

		// If we're attached to a slider, then update its current value based on position.
		void setRange(float min, float max, bool updateCurrFlag = false);
		void setCurr(float curr); // Sets the curr value of the button, and updates slider if there is one.

		float getMin() const { return min; }
		float getMax() const { return max; }
		float getCurr() const { return curr; }

		virtual void setPos(float x, float y) override; // Constrained by min/max and vertical/horizontal.
		void setRealPos(float x, float y); // Not constrained.
	};

	class Slider : public Button
	{
	protected:
		// Note: the Slider itself acts as the background button.
		SliderButton sliderButton;

		bool initialPosFlag;
		bool initialSizeFlag;

		// This is in any imaginary range you like.
		float min;
		float max;
		float curr;
		float initial;

		// Function that's called when the curr value changes.
		std::function<void(Widget *w, float val)> onValUpdateFunc;

	public:
		Slider(float min = 0, float max = 0, float initial = 0);
		virtual ~Slider() {}

		SliderButton &getButton() { return sliderButton; }

		void updateButtonPos();

		void setRange(float min, float max) { this->min = min; this->max = max; }
		void setCurr(float curr, bool updateButtonFlag = false);

		float getMin() const { return min; }
		float getMax() const { return max; }
		float getCurr() const { return curr; }

		void setVertical(bool vertical) { sliderButton.setVertical(vertical); }
		bool isVertical() const { return sliderButton.isVertical(); }

		virtual void setPos(float x, float y) override; // Moves both the background and the sliderButton.
		virtual void setSize(float w, float h) override; // Needs to adjust size of the sliderButton.
		virtual void setBorder(float w, float h) override { sliderButton.setBorder(w, h); Button::setBorder(w, h); }

		virtual void onValUpdate(std::function<void(Widget *w, float val)> f) { onValUpdateFunc = f; }
	};

	// A slider, but with left and right buttons.
	class Scrollbar : public Slider
	{
	protected:
		Button left;
		Button right;

		// Scrollbar needs a separate size/pos since it has left/right buttons.
		Math::vec2 pos;
		Math::vec2 size;

	public:
		Scrollbar(float min = 0, float max = 0, float initial = 0);
		virtual ~Scrollbar() {}

		void updateLeftRightPos();

		Math::vec2 getPos() const { return pos; }
		Math::vec2 getSize() const { return size; }

		Button &getLeftButton() { return left; }
		Button &getRightButton() { return right; }

		void goLeft(float amt);
		void goRight(float amt);

		virtual void update(float dt) override;

		virtual void setPos(float x, float y) override; // Moves left/right buttons as well.
		virtual void setSize(float w, float h) override; // Adjusts size/pos of the left/right buttons as well.
		virtual void setBorder(float w, float h) override { left.setBorder(w, h); right.setBorder(w, h); Slider::setBorder(w, h); }
	};

	// Not sure if this should be part of the the button stuff or not, but it doesn't have an obvious place.
	class DialogBox : public Widget
	{
	protected:
		Label title;
		Label info;

		std::vector<Button*> dialogButtons;

	public:
		DialogBox() : Widget() { hoverableFlag = false; type = "DialogBox"; }
		virtual ~DialogBox() {}

		void setTitleStr(const std::string& str);
		void setInfoStr(const std::string& str);

		Button* addButton(Button* button);
		Button* removeButton(Button* button);

		Label &getTitle() { return title; }
		Label &getInfo() { return info; }
		std::vector<Button*>& getButtons() { return dialogButtons; }
	};
}

