#pragma once

#include "Button.h"
#include "TextOutput.h"

#include <string>
#include <vector>

namespace Gui
{
	// Scrolling label.
	class ScrollLabel : public Label
	{
	protected:
		Scrollbar vertScrollbar;
		float vertScrollOffset;

		Scrollbar sideScrollbar;
		float sideScrollOffset;

		bool useVertFlag;
		bool useSideFlag;

		bool fitsVertFlag;
		bool fitsSideFlag;

	public:
		ScrollLabel();
		virtual ~ScrollLabel() {}

		Scrollbar &getVertScrollbar() { return vertScrollbar; }
		Scrollbar &getSideScrollbar() { return sideScrollbar; }

		bool fitsVert() const { return fitsVertFlag; }
		bool fitsSide() const { return fitsSideFlag; }

		void useVertScroll(bool useVertFlag);
		void useSideScroll(bool useSideFlag);

		void calcTextFits();
		void updateTextSize();

		virtual void unhide() override;

		virtual void setStr(const std::string &str, int vertScroll = 0, int sideScroll = 0, bool resizeFlag = true) override;

		virtual void updateVertScrollOffset(float vertScrollOffset);
		virtual void updateVertScrollbar();

		virtual void updateSideScrollOffset(float sideScrollOffset);
		virtual void updateSideScrollbar();

		virtual void setPos(float x, float y) override; // Moves scrollbars as well.
		virtual void setSize(float w, float h) override; // Adjusts size/pos of the scrollbars as well.
	};

	class ListBox : public ScrollLabel, public Clickable
	{
	protected:
		Widget selectedWidget;

		std::vector<std::string> entries;

		std::string str;

		int selected;

	public:
		ListBox();
		virtual ~ListBox() {}

		virtual void addEntry(const std::string &str);
		virtual void removeEntry(const std::string &str);
		virtual void removeEntry(size_t index);

		const std::vector<std::string> &getEntries() const { return entries; }
		void setEntries(std::vector<std::string> &entries) { this->entries = entries; }

		void setLinePadding(int linePadding) { text.setLinePadding(linePadding); }
		int getLinePadding() const { return text.getLinePadding(); }

		void updateText(bool resizeFlag);
		void updateSelected();

		int getSelected() const { return selected; }
		void setSelected(int selected) { this->selected = selected; } // Only use this if you know what you're doing.
		void selectEntry(int selected);

		const std::string &getSelectedEntry() const;

		virtual void unhide() override { ScrollLabel::unhide(); if (selected < 0) selectedWidget.hide(); }

		virtual void setStr(const std::string &str, int vertScroll = 0, int sideScroll = 0, bool resizeFlag = true) override;

		virtual bool clickEvent(Widget *w, Platform::Event &ev) override;
	};

	class DropDownList : public Button
	{
	protected:
		ListBox list;

		float listHeight;

		bool initialPosFlag;
		bool initialSizeFlag;

		bool showListFlag;

	public:
		DropDownList();
		virtual ~DropDownList() {}

		virtual void hide() override;
		virtual void unhide() override;

		float getListHeight() const { return listHeight; }
		void setListHeight(float h) { listHeight = h; }

		virtual void toggleList();

		virtual void updateListPos();

		virtual void setPos(float x, float y) override; // Moves list as well.
		virtual void setSize(float w, float h) override; // Adjusts size/pos of the list as well.

		ListBox &getList() { return list; }

		virtual void addEntry(const std::string &str) { list.addEntry(str); }
		virtual void removeEntry(const std::string &str) { list.removeEntry(str); }
		virtual void removeEntry(size_t index) { list.removeEntry(index); }

		const std::vector<std::string> &getEntries() const { return list.getEntries(); }
		void setEntries(std::vector<std::string> &entries) { list.setEntries(entries); }

		int getSelected() const { return list.getSelected(); }
		void selectEntry(int selected) { return list.selectEntry(selected); }
	};
}

