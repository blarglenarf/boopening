#pragma once

#include "Widget.h"

namespace Gui
{
	class Menu : public Widget
	{
	};

	class MenuBar : public Menu
	{
	};

	class ContextMenu : public Menu
	{
	};

	class PieMenu : public Menu
	{
	};
}

