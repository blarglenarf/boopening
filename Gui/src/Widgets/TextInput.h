#pragma once

#include "Widget.h"
#include "List.h"

namespace Gui
{
	// A kind of scroll label where you can enter text I guess.
	class TextBox : public ScrollLabel, public KeyPressable, public Clickable
	{
	protected:
		Widget cursor;

		int charPos;

		bool cursorActiveFlag;
		bool pressedFlag;

		float currTime;

	public:
		TextBox();
		virtual ~TextBox() {}

		virtual void update(float dt) override;

		virtual bool keyDownEvent(Widget *w, Platform::Event &ev) override;
		virtual bool keyUpEvent(Widget *w, Platform::Event &ev) override;
		virtual bool keyPressEvent(Widget *w, Platform::Event &ev) override;

		virtual bool clickEvent(Widget *w, Platform::Event &ev) override;

		virtual void beginSelect() override;
		virtual void endSelect() override;

		void updateCursorPos(int x = -1, int y = -1);
	};

	class ComboBox : public ListBox
	{
	protected:
		std::vector<std::string> realEntries;

		TextBox input;

	public:
		ComboBox();
		virtual ~ComboBox() {}

		virtual void addEntry(const std::string &str) override;
		virtual void removeEntry(const std::string &str) override;
		virtual void removeEntry(size_t index) override;

		TextBox &getInput() { return input; }

		virtual void setPos(float x, float y) override;
		virtual void setSize(float w, float h) override;

		void onKeyPress();
	};

	class DropDownCombo : public DropDownList
	{
	protected:
		std::vector<std::string> realEntries;

		TextBox input;

	public:
		DropDownCombo();
		virtual ~DropDownCombo() {}

		virtual void hide() override;
		virtual void unhide() override;

		virtual void toggleList() override;

		virtual void addEntry(const std::string &str) override;
		virtual void removeEntry(const std::string &str) override;
		virtual void removeEntry(size_t index) override;

		TextBox &getInput() { return input; }

		virtual void updateListPos() override;

		void onKeyPress();
	};

	class Console : public ScrollLabel
	{
	protected:
		std::vector<std::string> history;

		TextBox input;

		int height;
		int historyPos;

	public:
		Console();
		virtual ~Console() {}

		TextBox &getInput() { return input; }

		void setHeight(int height);

		void toggle() { if (isHidden()) unhide(); else hide(); }

		virtual void hide() override;
		virtual void unhide() override;

		// Don't use this function externally, use setHeight instead.
		virtual void setSize(float w, float h) override; // Adjusts size/pos of text input as well.

		void onKeyPress(Platform::Keyboard::Key key);
	};
}

