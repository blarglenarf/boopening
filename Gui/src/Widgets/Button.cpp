#include "Button.h"
#include "WidgetGroup.h"
#include "../Canvas.h"
#include "../../../Platform/src/Window.h"
#include "../../../Renderer/src/RenderResource/Font.h"

#include <algorithm>

using namespace Gui;


void CycleButton::removeOption(const std::string &option)
{
	for (auto it = options.begin(); it != options.end(); it++)
	{
		if (*it == option)
		{
			options.erase(it);
			break;
		}
	}
}

void CycleButton::setStr(const std::string &str, int vertScroll, int sideScroll, bool resizeFlag)
{
	(void) (resizeFlag);

	text.setStr(str);
	text.setVertScroll(vertScroll);
	text.setSideScroll(sideScroll);
	setBorder(borderWidth, borderHeight);
	auto maxSize = getMaxOptionSize();
	setSize(maxSize.x + borderWidth * 2, maxSize.y + borderHeight * 2);
}

Math::ivec2 CycleButton::getMaxOptionSize()
{
	size_t len = 0;
	std::string str;
	for (auto option : options)
	{
		if (option.size() > len)
		{
			len = option.size();
			str = option;
		}
	}

	if (len == 0)
		return Math::ivec2(0, 0);

	return text.getFont()->getTextSize(str, text.getMaxSize().width, text.getMaxSize().height);
}

const std::string &CycleButton::getCurrOption() const
{
	static std::string empty = "";
	if (currOption >= options.size())
		return empty;
	return options[currOption];
}

bool CycleButton::clickEvent(Widget *w, Platform::Event &ev)
{
	currOption++;
	if (currOption >= options.size())
		currOption = 0;

	text.setStr(options[currOption]);
	onClickFunc(w, ev);

	return true;
}

SpinnerButton::SpinnerButton(const std::vector<std::string> &options) : CycleButton(options), initialPosFlag(false), initialSizeFlag(false), initialUpdateFlag(false), alignment(TOP_TOP)
{
	addChild(&forward);
	addChild(&back);

	forward.onClicked([this](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); this->nextOption(); });
	back.onClicked([this](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); this->prevOption(); });

	// Just some initial values.
	forward.setSize(20, 20);
	back.setSize(20, 20);

	type = "SpinnerButton";
}

void SpinnerButton::nextOption()
{
	if (options.empty())
		return;
	currOption++;
	if (currOption >= options.size())
		currOption = 0;
	text.setStr(options[currOption]);
}

void SpinnerButton::prevOption()
{
	if (options.empty())
		return;
	currOption--; // A bit weird since this is unsigned, but should still work.
	if (currOption >= options.size())
		currOption = options.size() - 1;
	text.setStr(options[currOption]);
}

void SpinnerButton::updateButtonPos()
{
	if (!initialPosFlag || !initialSizeFlag)
		return;

	auto pos = getPos();
	auto size = getSize();

	float maxX = 20.0f;
	float maxY = 20.0f;
#if 0
	auto fSize = forward.getSize();
	auto bSize = back.getSize();

	auto maxX = std::max(fSize.x, bSize.x);
	auto maxY = std::max(fSize.y, bSize.y);
#endif

	// Do we want the buttons to stretch or not?
	switch (alignment)
	{
	case TOP_TOP: // Both side-by-side at the top.
		forward.setPos(pos.x + (size.x / 2.0f), pos.y);
		forward.setSize(size.x / 2.0f, maxY);
		back.setPos(pos.x, pos.y);
		back.setSize(size.x / 2.0f, maxY);
		CycleButton::setPos(pos.x, pos.y + maxY);
		CycleButton::setSize(size.x, size.y - maxY);
		break;
	case TOP_BOT: // Forward top, back bottom.
		forward.setPos(pos.x, pos.y);
		forward.setSize(size.x, maxY);
		back.setPos(pos.x, pos.y + size.y - maxY);
		back.setSize(size.x, maxY);
		CycleButton::setPos(pos.x, pos.y + maxY);
		CycleButton::setSize(size.x, size.y - maxY - maxY);
		break;
	case BOT_TOP: // Forward bottom, back top.
		forward.setPos(pos.x, pos.y + size.y - maxY);
		forward.setSize(size.x, maxY);
		back.setPos(pos.x, pos.y);
		back.setSize(size.x, maxY);
		CycleButton::setPos(pos.x, pos.y + maxY);
		CycleButton::setSize(size.x, size.y - maxY - maxY);
		break;
	case BOT_BOT: // Both side-by-side at the bottom.
		forward.setPos(pos.x + (size.x / 2.0f), pos.y + size.y - maxY);
		forward.setSize(size.x / 2.0f, maxY);
		back.setPos(pos.x, pos.y + size.y - maxY);
		back.setSize(size.x / 2.0f, maxY);
		CycleButton::setPos(pos.x, pos.y);
		CycleButton::setSize(size.x, size.y - maxY);
		break;
	case LEFT_LEFT: // Both on the left.
		forward.setPos(pos.x, pos.y);
		forward.setSize(maxX, size.y / 2.0f);
		back.setPos(pos.x, pos.y + size.y / 2.0f);
		back.setSize(maxX, size.y / 2.0f);
		CycleButton::setPos(pos.x + maxX, pos.y);
		CycleButton::setSize(size.x - maxX, size.y);
		break;
	case LEFT_RIGHT: // Forward left, back right.
		forward.setPos(pos.x, pos.y);
		forward.setSize(maxX, size.y);
		back.setPos(pos.x + size.x - maxX, pos.y);
		back.setSize(maxX, size.y);
		CycleButton::setPos(pos.x + maxX, pos.y);
		CycleButton::setSize(size.x - maxX - maxX, size.y);
		break;
	case RIGHT_LEFT: // Forward right, back left.
		forward.setPos(pos.x + size.x - maxX, pos.y);
		forward.setSize(maxX, size.y);
		back.setPos(pos.x, pos.y);
		back.setSize(maxX, size.y);
		CycleButton::setPos(pos.x + maxX, pos.y);
		CycleButton::setSize(size.x - maxX - maxX, size.y);
		break;
	case RIGHT_RIGHT: // Both on the right.
		forward.setPos(pos.x + size.x - maxX, pos.y);
		forward.setSize(maxX, size.y / 2.0f);
		back.setPos(pos.x + size.x - maxX, pos.y + size.y / 2.0f);
		back.setSize(maxX, size.y / 2.0f);
		CycleButton::setPos(pos.x, pos.y);
		CycleButton::setSize(size.x - maxX, size.y);
		break;
	default:
		break;
	}

	initialUpdateFlag = true;
}

void SpinnerButton::setPos(float x, float y)
{
	initialPosFlag = true;
	totalPos.x = x;
	totalPos.y = y;
	CycleButton::setPos(x, y);
	updateButtonPos();
}

void SpinnerButton::setSize(float w, float h)
{
	initialSizeFlag = true;
	totalSize.x = w;
	totalSize.y = h;
	CycleButton::setSize(w, h);
	updateButtonPos();
}


void ToggleButton::updateBackground()
{
	if (toggleFlag)
		nineBox.useBackgroundSet(1);
	else
		nineBox.useBackgroundSet(0);
	nineBox.useStandardBackground();
}

bool ToggleButton::clickEvent(Widget *w, Platform::Event &ev)
{
	// If attached to a toggleGroup, and the group is Unique, deselect the current one.
	if (toggleGroup && toggleGroup->isUnique())
	{
		// Unless we're the current one.
		if (toggleFlag)
			return true;
		toggleGroup->clearToggled();
	}

	toggleFlag = !toggleFlag;

	updateBackground();
	nineBox.useClickBackground();

	onClickFunc(w, ev);

	return true;
}


ToggleButton *ToggleGroup::addButton(ToggleButton *button)
{
	toggleButtons.push_back(button);
	button->setGroup(this);
	if (toggleButtons.size() == 1 && uniqueFlag)
	{
		button->setToggled(true);
		button->updateBackground();
	}
	// TODO: Adjust size of the group accordingly. But what if you're arranged horizontally instead of vertically?
	//auto w = std::max(getSize().x, button->getSize().x);
	//auto h = std::max(getSize().y, button->getSize().y);
	return button;
}

ToggleButton *ToggleGroup::removeButton(ToggleButton *button)
{
	for (auto it = toggleButtons.begin(); it != toggleButtons.end(); it++)
	{
		if (*it == button)
		{
			toggleButtons.erase(it);
			break;
		}
	}
	button->setGroup(nullptr);
	return button;
}


SliderButton::SliderButton(float min, float max, bool vertical) : Button(), Draggable(), min(min), max(max), curr(min), vertical(vertical), slider(nullptr)
{
	draggableFlag = true;
	type = "SliderButton";
}

void SliderButton::setRange(float min, float max, bool updateCurrFlag)
{
	this->min = min;
	this->max = max;

	if (!updateCurrFlag)
		return;

	float newCurr = curr;
	if (newCurr < min)
		newCurr = min;
	if (newCurr > max)
		newCurr = max;

	if (curr != newCurr)
		setCurr(newCurr);
}

void SliderButton::setCurr(float curr)
{
	if (curr == this->curr)
		return;

	this->curr = curr;

	if (curr < min)
		this->curr = min;
	if (curr > max)
		this->curr = max;

	// If we're attached to a slider, then update its current value based on position.
	if (!slider)
		return;

	if (max - min == 0)
	{
		slider->setCurr(slider->getMin(), false);
		return;
	}

	float percent = 0.0f;
	if (max - min != 0)
		percent = (this->curr - min) / (max - min);

	float sliderMin = slider->getMin();
	float sliderMax = slider->getMax();

	float newVal = sliderMin + ((sliderMax - sliderMin) * percent);

	slider->setCurr(newVal, false);
}

void SliderButton::setPos(float x, float y)
{
	auto currPos = nineBox.getPos();
	if (vertical)
	{
		x = currPos.x;
		if (y < min)
			y = min;
		if (y > max)
			y = max;
		setCurr(y);
	}
	else
	{
		y = currPos.y;
		if (x < min)
			x = min;
		if (x > max)
			x = max;
		setCurr(x);
	}
	Button::setPos(x, y);
}

void SliderButton::setRealPos(float x, float y)
{
	// I want to use this function to set the slider's position without also updating the curr value.
	//setCurr(curr);
	Button::setPos(x, y);

	type = "Slider";
}


Slider::Slider(float min, float max, float initial) : initialPosFlag(false), initialSizeFlag(false), min(min), max(max), curr(initial), initial(initial)
{
	onValUpdateFunc = [](Widget *w, float val) -> void { (void) (w); (void) (val); };

	if (initial < min)
		initial = min;
	if (initial > max)
		initial = max;
	curr = initial;

	sliderButton.setSlider(this);
	addChild(&sliderButton);

	onClicked([](Widget *w, Platform::Event &ev) -> void
	{
		// Move the slider's position based on where the button was clicked.
		Slider *slider = dynamic_cast<Slider*>(w);
		if (!slider)
			return;

		auto sliderSize = slider->sliderButton.getNineBox().getSize();
		if (slider->sliderButton.isVertical())
			slider->sliderButton.setPos((float) ev.x, (float) ev.y - (sliderSize.y / 2.0f));
		else
			slider->sliderButton.setPos((float) ev.x - (sliderSize.x / 2.0f), (float) ev.y);

		// Now have us dragging the slider.
		Canvas::instance().setClicked(&slider->sliderButton);
		Canvas::instance().setHovered(&slider->sliderButton);
		Canvas::instance().setDragged(&slider->sliderButton);

		slider->getNineBox().useBackground(0);
		slider->endHover();
	}); 
}

void Slider::updateButtonPos()
{
	if (!initialPosFlag || !initialSizeFlag)
		return;
	// Set position of slider button based on our current position.
	float percent = 0.0f;
	if (max - min != 0)
		percent = (curr - min) / (max - min);
	float sliderMin = sliderButton.getMin();
	float sliderMax = sliderButton.getMax();

	float sliderVal = sliderMin + ((sliderMax - sliderMin) * percent);
	auto sliderPos = sliderButton.getPos();
	if (sliderButton.isVertical())
		sliderButton.setRealPos(sliderPos.x, sliderVal);
	else
		sliderButton.setRealPos(sliderVal, sliderPos.y);
}

void Slider::setCurr(float curr, bool updateButtonFlag)
{
	if (curr == this->curr)
		return;

	if (curr < min)
		curr = min;
	if (curr > max)
		curr = max;

	this->curr = curr;

	onValUpdateFunc(this, this->curr);

	if (updateButtonFlag)
		updateButtonPos();
}

void Slider::setPos(float x, float y)
{
	// If we're not setting the initial position and are just moving the slider around, keep the button at its current relative position.
	// If we're setting the initial position, then set the button to the initial value.
	if (!initialPosFlag)
		curr = initial;

	if (initialSizeFlag)
	{
		// Set slider button range, only if we have a size.
		auto sliderSize = sliderButton.getNineBox().getSize();
		if (sliderButton.isVertical())
			sliderButton.setRange(y, y + nineBox.getSize().y - sliderSize.y);
		else
			sliderButton.setRange(x, x + nineBox.getSize().x - sliderSize.x);
	}

	sliderButton.setRealPos(x, y);

	// Want to update the button's position based on curr val.
	Button::setPos(x, y);

	if (initialSizeFlag)
		updateButtonPos();
	initialPosFlag = true;
}

void Slider::setSize(float w, float h)
{
	if (sliderButton.isVertical())
		sliderButton.setSize(w, w);
	else
		sliderButton.setSize(h, h);

	// If the slider has a position, then set its range.
	if (initialPosFlag)
	{
		if (sliderButton.isVertical())
			sliderButton.setRange(nineBox.getPos().y, nineBox.getPos().y + h - w);
		else
			sliderButton.setRange(nineBox.getPos().x, nineBox.getPos().x + w - h);
	}

	Button::setSize(w, h);

	if (initialPosFlag)
		updateButtonPos();
	initialSizeFlag = true;
}


Scrollbar::Scrollbar(float min, float max, float initial) : Slider(min, max, initial)
{
	// TODO: Include an increment amount.
	addChild(&left);
	addChild(&right);

	type = "Scrollbar";
}

void Scrollbar::updateLeftRightPos()
{
	// Need to take into account left/right button sizes.
	left.setPos(pos.x, pos.y);

	//auto leftSize = left.getNineBox().getSize();
	auto rightSize = right.getNineBox().getSize();

	if (sliderButton.isVertical())
		right.setPos(pos.x, pos.y + size.y - rightSize.y);
	else
		right.setPos(pos.x + size.x - rightSize.x, pos.y);
}

void Scrollbar::update(float dt)
{
	// If either the left or right buttons are being held down, process the appropriate event.
	// TODO: Ideally there should be a pause after the initial click.
	float speed = (max - min) / 2.0f; // 2 seconds to scroll fully.

	auto *hovered = Canvas::instance().getHovered();
	if (!hovered || !Platform::Mouse::isDown(Platform::Mouse::LEFT))
		return;

	if (hovered == &left)
		goLeft(speed * dt);
	if (hovered == &right)
		goRight(speed * dt);
}

void Scrollbar::goLeft(float amt)
{
	setCurr(curr - amt, true);
}

void Scrollbar::goRight(float amt)
{
	setCurr(curr + amt, true);
}

void Scrollbar::setPos(float x, float y)
{
	pos = Math::vec2(x, y);

	// Need to take into account left/right button sizes.
	if (initialSizeFlag)
	{
		// We have enough information to arrange all the parts of the scrollbar.
		updateLeftRightPos();

		// Take into account left button size.
		if (sliderButton.isVertical())
			Slider::setPos(x, y + left.getNineBox().getSize().y);
		else
			Slider::setPos(x + left.getNineBox().getSize().x, y);
	}
	else
		Slider::setPos(x, y);
}

void Scrollbar::setSize(float w, float h)
{
	size = Math::vec2(w, h);

	// First set the sizes of the left/right buttons.
	if (sliderButton.isVertical())
	{
		left.setSize(w, w);
		right.setSize(w, w);
	}
	else
	{
		left.setSize(h, h);
		right.setSize(h, h);
	}

	// We have enough information to arrange all the parts of the scrollbar.
	if (initialPosFlag)
	{
		updateLeftRightPos();

		// Take into account left button size.
		if (sliderButton.isVertical())
			Slider::setPos(pos.x, pos.y + left.getNineBox().getSize().y);
		else
			Slider::setPos(pos.x + left.getNineBox().getSize().x, pos.y);
	}

	// Take into account button sizes.
	if (sliderButton.isVertical())
		Slider::setSize(w, h - (left.getNineBox().getSize().y + right.getNineBox().getSize().y));
	else
		Slider::setSize(w - (left.getNineBox().getSize().x + right.getNineBox().getSize().x), h);
}


void DialogBox::setTitleStr(const std::string& str)
{
	title.setStr(str);
}

void DialogBox::setInfoStr(const std::string& str)
{
	info.setStr(str);
}

Button* DialogBox::addButton(Button* button)
{
	dialogButtons.push_back(button);
	//button->setGroup(this);
	return button;
}

Button* DialogBox::removeButton(Button* button)
{
	for (auto it = dialogButtons.begin(); it != dialogButtons.end(); it++)
	{
		if (*it == button)
		{
			dialogButtons.erase(it);
			break;
		}
	}
	//button->setGroup(nullptr);
	return button;
}
