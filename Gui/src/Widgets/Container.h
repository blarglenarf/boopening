#pragma once

#include "Widget.h"

#include "List.h"
#include "../Layout/Layout.h"
#include "../Layout/GridLayout.h"
#include "../Layout/FlowLayout.h"

#undef DialogBox

namespace Gui
{
	// Has scrollbars, but based on contents rather than text.
	class Panel : public ScrollLabel
	{
	protected:
		Layout layout;

	public:
		Panel();
		virtual ~Panel() {}

		virtual void hide() override;
		virtual void unhide() override;

		Layout &getLayout() { return layout; }

		virtual void setPos(float x, float y) override;
		virtual void setSize(float w, float h) override;
		virtual void setBorder(float w, float h) override;

		virtual void setPadding(float x, float y) { layout.setPadding(x, y); }

		virtual void calcContentsFits();
	};

	// Button that when clicked toggles a panel to be visible/invisible.
	class DropPanel : public Button
	{
	protected:
		Panel panel;

		float panelWidth;
		float panelHeight;

		bool initialPosFlag;
		bool initialSizeFlag;

		bool showPanelFlag;

		float xOffset;

	public:
		DropPanel();
		virtual ~DropPanel() {}

		virtual void hide() override;
		virtual void unhide() override;

		virtual void togglePanel();
		virtual void updatePanelPos();

		Panel &getPanel() { return panel; }
		Layout &getLayout() { return panel.getLayout(); }

		virtual void setPos(float x, float y) override;
		virtual void setSize(float w, float h) override;
		virtual void setBorder(float w, float h) override;

		void setPanelSize(float w, float h) { panelWidth = w; panelHeight = h; updatePanelPos(); }

		virtual void setPadding(float x, float y);

		virtual void calcContentsFits();

		void setXOffset(float xOffset) { this->xOffset = xOffset; }
		float getXOffset() const { return xOffset; }
	};

	// A series of panels with tab buttons above.
	class TabPanel : public Panel
	{
	protected:
		std::vector<DropPanel*> dropPanels;

		DropPanel *activePanel;

	public:
		TabPanel();
		virtual ~TabPanel();

		DropPanel &addTab(const std::string &name);
		void removeTab(DropPanel &panel);

		void fitTabs();

		void setActivePanel(DropPanel *panel) { activePanel = panel; }
		DropPanel *getActivePanel() { return activePanel; }

		virtual void setPos(float x, float y) override;
		virtual void setSize(float w, float h) override;
		virtual void setBorder(float w, float h) override;

		virtual void setPadding(float x, float y);
	};

	class Accordian : public Widget
	{
	};

	class TreeView : public Widget
	{
	};

	class GridView : public Widget
	{
	};

	class FileBrowser : public Widget
	{
	};
}

