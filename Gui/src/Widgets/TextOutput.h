#pragma once

#include "Widget.h"
#include "../Text/Text.h"

namespace Gui
{
	// Always visible.
	class Label : public Widget
	{
	protected:
		// TODO: Don't store the text here, just store a string (maybe, not sure).
		Text text;

		float borderWidth;
		float borderHeight;

		// Determines if size already takes border into account.
		bool borderFlag;

	public:
		Label();
		virtual ~Label();

		virtual void setPos(float x, float y) override;
		virtual void setSize(float w, float h) override;
		virtual void setBorder(float w, float h) override;

		Text &getText() { return text; }

		virtual void setStr(const std::string &str, int vertScroll = 0, int sideScroll = 0, bool resizeFlag = true);

		int getTextWidth() const { return text.getWidth(); }
		int getTextHeight() const { return text.getHeight(); }

		virtual void hide() { text.hide(); Widget::hide(); }
		virtual void unhide() { text.unhide(); Widget::unhide(); }

		virtual void setZ(float z) override { Widget::setZ(z); text.setZ(z + 0.5f); }
	};

	// Appears on mouse hover.
	class Tooltip : public Label
	{
	};

	// Not a progress bar, instead specifically for displaying status about something.
	class StatusBar : public Label
	{
	};

	// Hopefully fairly obvious.
	class ProgressBar : public Label
	{
	};

	// TODO: Doesn't really seem necessary, combine with StatusBar above perhaps?
	class InfoBar : public Label
	{
	};
}

