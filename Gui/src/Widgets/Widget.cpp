#include "Widget.h"
#include "WidgetGroup.h"
#include "../Canvas.h"
#include "../NineBox/NineBox.h"

using namespace Gui;

Widget::Widget(WidgetGroup *group) : group(group), parent(nullptr), hoverableFlag(false), draggableFlag(false), type("Widget")
{
	Canvas::instance().addNineBox(&nineBox);
}

Widget::~Widget()
{
	// Just in case this is in a group and hasn't been removed from it.
	if (group)
		group->removeWidget(this);

	Canvas::instance().removeNineBox(&nineBox);

	// Do we want to delete the children as well?
	// No, as the memory isn't necessarily owned by this widget, and they may not have been dynamically allocated.
#if 0
	for (auto *child : children)
	{
		Canvas::instance().removeWidgetMemory(child);
		delete child;
		child = nullptr;
	}
#endif
}

void Widget::setPos(float x, float y)
{
	nineBox.setPos(x, y);
}

void Widget::setSize(float w, float h)
{
	nineBox.setSize(w, h);
}

void Widget::setBorder(float w, float h)
{
	nineBox.setBorder(w, h);
}
#if 0
void Widget::setGroup(WidgetGroup *group)
{
	// Remove from previous group if necessary.
	if (this->group && this->group != group)
		this->group->removeWidget(this);

	this->group = group;

	// Remove the children from the other group and add to new group.
	for (auto *child : children)
	{
		if (!group && child->getGroup())
		{
			child->getGroup()->removeWidget(child);
			child->setGroup(nullptr);
		}

		if (group && group != child->getGroup())
		{
			if (child->getGroup())
				child->getGroup()->removeWidget(child);
			group->addWidget(child);
			child->setGroup(group);
		}
	}
}
#endif
Widget *Widget::addChild(Widget *widget)
{
	if (widget == nullptr)
		return nullptr;
	children.push_back(widget);
	widget->parent = this;
	if (group)
		group->addWidget(widget);
	widget->setZ(getZ() + 1.0f);
	widget->getNineBox().setParent(&nineBox);
	return widget;
}

Widget *Widget::removeChild(Widget *widget)
{
	for (auto it = children.begin(); it != children.end(); it++)
	{
		if (*it == widget)
		{
			children.erase(it);
			break;
		}
	}
	widget->nineBox.setParent(nullptr);
	widget->parent = nullptr;
	return widget;
}

