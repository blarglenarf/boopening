#include "WidgetGroup.h"
#include "Widget.h"

using namespace Gui;

WidgetGroup::~WidgetGroup()
{
	// Decided that it's not our job to delete these, memory needs to be managed elsewhere.
	widgets.clear();
}

Widget *WidgetGroup::addWidget(Widget *widget)
{
	// If widget alreay has a group, remove from that group.
	if (widget->getGroup())
		widget->getGroup()->removeWidget(widget);

	widgets.push_back(widget);
	widget->setGroup(this);

	// If widget has children, add them.
	for (auto *child : widget->getChildren())
		addWidget(child);

	return widget;
}

Widget *WidgetGroup::removeWidget(Widget *widget)
{
	for (auto it = widgets.begin(); it != widgets.end(); it++)
	{
		if (*it == widget)
		{
			widgets.erase(it);
			widget->setGroup(nullptr);

			// If widget has children, remove them from the group.
			for (auto *child : widget->getChildren())
				removeWidget(child);

			break;
		}
	}
	return widget;
}

void WidgetGroup::update(float dt, int x, int y)
{
	if (x != prevMouseX || y != prevMouseY)
		hoveredWidget = nullptr;

	float currZ = -1000000;
	for (auto *widget : widgets)
	{
		if (widget->isHidden())
			continue;

		widget->update(dt);

		// May as well check and see what's being hovered over by the mouse.
		if (x == prevMouseX && y == prevMouseY)
			continue;
		if (!widget->isHoverable())
			continue;
		if (!widget->isInside(x, y))
			continue;
		if (widget->getZ() < currZ)
			continue;
		hoveredWidget = widget;
		currZ = widget->getZ();
	}

	prevMouseX = x;
	prevMouseY = y;
}

void WidgetGroup::hide()
{
	if (hiddenFlag)
		return;

	for (auto *widget : widgets)
		widget->hide();
	hiddenFlag = true;
}

void WidgetGroup::unhide()
{
	if (!hiddenFlag)
		return;

	for (auto *widget : widgets)
		widget->unhide();
	hiddenFlag = false;
}

