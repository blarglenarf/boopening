#include "Container.h"

#include "../Canvas.h"

#include <algorithm>

using namespace Gui;


Panel::Panel() : ScrollLabel()
{
	layout.setType(Layout::GRID);
	hoverableFlag = false;

	type = "Panel";
}

void Panel::hide()
{
	ScrollLabel::hide();
	layout.hide();
}

void Panel::unhide()
{
	ScrollLabel::unhide();
	layout.unhide();
}

void Panel::setPos(float x, float y)
{
	ScrollLabel::setPos(x, y);

	auto innerPos = nineBox.getInnerPos();
	layout.setPos(innerPos.x, innerPos.y);
	layout.arrangeContents();
}

void Panel::setSize(float w, float h)
{
	ScrollLabel::setSize(w, h);

	auto innerSize = nineBox.getInnerSize();
	layout.setSize(innerSize.x, innerSize.y);
	layout.arrangeContents();
}

void Panel::setBorder(float w, float h)
{
	ScrollLabel::setBorder(w, h);

	auto pos = nineBox.getPos();
	auto size = nineBox.getSize();
	layout.setPos(pos.x + w, pos.y + h);
	layout.setSize(size.x - w * 2, size.y - h * 2);
	layout.arrangeContents();
}

void Panel::calcContentsFits()
{
	// TODO: Not sure if I'm going to need to use the layout here...
	float scrollX = 0, scrollY = 0;
	if (!vertScrollbar.isHidden())
		scrollX = vertScrollbar.getSize().x;
	if (!sideScrollbar.isHidden())
		scrollY = sideScrollbar.getSize().y;

	float innerX = nineBox.getSize().x - nineBox.getInnerSize().x;
	float innerY = nineBox.getSize().y - nineBox.getInnerSize().y;

	float maxX = std::max(0.0f, getTextWidth() - getSize().x + innerX + scrollX);
	float maxY = std::max(0.0f, getTextHeight() - getSize().y + innerY + scrollY);

	// Go through all the kids and see if they fit.
	for (auto *child : children)
	{
		maxX = std::max(maxX, child->getNineBox().getSize().x);
		maxY = std::max(maxY, child->getNineBox().getSize().y);
	}

	fitsVertFlag = maxY <= 0;
	fitsSideFlag = maxX <= 0;

	vertScrollbar.setRange(0, std::max(0.0f, maxY));
	sideScrollbar.setRange(0, std::max(0.0f, maxX));
}



DropPanel::DropPanel() : Button(), panelWidth(200), panelHeight(200), initialPosFlag(false), initialSizeFlag(false), showPanelFlag(false), xOffset(0)
{
	panel.setBorder(5, 5);

	panel.hide();

	onClicked([](Widget *w, Platform::Event &ev) -> void { (void) (ev); ((DropPanel*) (w))->togglePanel(); });

	addChild(&panel);

	type = "DropPanel";
}

void DropPanel::hide()
{
	nineBox.hide();
	for (auto *widget : children)
		widget->hide();
}

void DropPanel::unhide()
{
	nineBox.unhide();
	for (auto *widget : children)
	{
		if (widget == &panel)
		{
			if (showPanelFlag)
				widget->unhide();
		}
		else
			widget->unhide();
	}
}

void DropPanel::togglePanel()
{
	if (isHidden())
		return;

	showPanelFlag = !showPanelFlag;
	if (showPanelFlag)
		panel.unhide();
	else
		panel.hide();
}

void DropPanel::updatePanelPos()
{
	if (!initialPosFlag || !initialSizeFlag)
		return;

	auto pos = getPos();
	auto size = getSize();

	panel.setPos(pos.x + xOffset, pos.y + size.y);
	panel.setSize(panelWidth, panelHeight);
}

void DropPanel::setPos(float x, float y)
{
	Button::setPos(x, y);

	initialPosFlag = true;
	updatePanelPos();
}

void DropPanel::setSize(float w, float h)
{
	Button::setSize(w, h);

	initialSizeFlag = true;
	updatePanelPos();
}

void DropPanel::setBorder(float w, float h)
{
	Button::setBorder(w, h);
	panel.setBorder(w, h);
}

void DropPanel::setPadding(float x, float y)
{
	panel.setPadding(x, y);
}

void DropPanel::calcContentsFits()
{
	panel.calcContentsFits();
}



TabPanel::TabPanel() : Panel(), activePanel(nullptr)
{
	type = "TabPanel";
	layout.setType(Layout::FLOW);
}

TabPanel::~TabPanel()
{
	for (auto *panel : dropPanels)
		delete Canvas::instance().removeWidgetMemory(panel);
}

DropPanel &TabPanel::addTab(const std::string &name)
{
	DropPanel *panel = new DropPanel();
	dropPanels.push_back(panel);

	if (!activePanel)
	{
		activePanel = panel;
		activePanel->togglePanel();
	}

	// Set the onClicked method of the DropPanel, since it needs to perform a panel switch, rather than panel hide/unhide.
	panel->onClicked([](Widget *w, Platform::Event &ev) -> void {
		(void) (ev);
		auto *t = (TabPanel*) w->getParent();
		auto *active = t->getActivePanel();
		if (w == active)
			return;
		active->togglePanel();
		auto *p = (DropPanel*) w;
		p->togglePanel();
		t->setActivePanel(p);
	});

	Canvas::instance().addWidgetMemory(panel);

	panel->setStr(name);

	auto pos = getPos();
	auto size = getSize();

	panel->setPos(pos.x, pos.y);
	panel->setSize((float) panel->getTextWidth() + 10, (float) panel->getTextHeight() + 10);
	panel->setPanelSize(size.x - 10, size.y - 10);
	panel->setBorder(5, 5);
	panel->setPadding(5, 5);

	// Hopefully the flowLayout will make sure they don't just all get added to the same position...
	layout.flowLayout().addWidget(panel, FlowPos::FREE, pos.x, pos.y, (float) panel->getTextWidth() + 10, (float) panel->getTextHeight() + 10);

	addChild(panel);

	fitTabs();

	return *panel;
}

void TabPanel::removeTab(DropPanel &panel)
{
	// TODO: Implement me!
	(void) (panel);
	std::cout << "Whoops, haven't implemented tab removal yet\n";
}

void TabPanel::fitTabs()
{
	if (dropPanels.empty())
		return;

	float w = getSize().x;
	float h = getSize().y;

	// If the tabs are collectively too large or too small, resize them.
	float xPadding = layout.flowLayout().getXPadding();
	float totalWidth = xPadding;
	for (auto *p : dropPanels)
	{
		p->setPanelSize(w - 10, h - 10);
		totalWidth += p->getSize().x + xPadding;
	}

	// TODO: If there aren't many tabs then we probably don't want to stretch them too much.
	// FIXME: Why does this need the +3?
	float currX = getPos().x;
	float tabWidth = (w - xPadding * (float) (dropPanels.size() + 3)) / (float) dropPanels.size();
	for (auto &f : layout.flowLayout().getFlow())
	{
		auto *dropPanel = (DropPanel*) f.getWidget();

		f.setWidth(tabWidth);
		f.setX(currX);
		dropPanel->setXOffset(getPos().x - currX);
		currX += tabWidth + xPadding;
	}

	layout.arrangeContents();
}

void TabPanel::setPos(float x, float y)
{
	Panel::setPos(x, y);
}

void TabPanel::setSize(float w, float h)
{
	Panel::setSize(w, h);

	fitTabs();
}

void TabPanel::setBorder(float w, float h)
{
	Panel::setBorder(w, h);

	fitTabs();
}

void TabPanel::setPadding(float x, float y)
{
	Panel::setPadding(x, y);

	fitTabs();
}

