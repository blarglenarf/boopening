#pragma once

#include "../NineBox/NineBox.h"

#include "../../../Platform/src/Event.h"

#include <functional>
#include <iostream>


namespace Gui
{
	class WidgetGroup;

	class Widget
	{
	private:
		// So the WidgetGroup class can set the widget's group to null directly.
		friend class WidgetGroup;

	private:
		Widget(const Widget &widget);
		Widget &operator = (const Widget &widget);

	protected:
		NineBox nineBox;

		WidgetGroup *group;

		Widget *parent;
		std::vector<Widget*> children;

		bool hoverableFlag;
		bool draggableFlag;

		std::string type;

	public:
		Widget(WidgetGroup *group = nullptr);
		virtual ~Widget();

		NineBox &getNineBox() { return nineBox; }

		virtual void setPos(float x, float y);
		virtual void setSize(float w, float h);
		virtual void setBorder(float w, float h);

		virtual Math::vec2 getPos() const { return nineBox.getPos(); }
		virtual Math::vec2 getSize() const { return nineBox.getSize(); }

		virtual void hide() { nineBox.hide(); for (auto *child : children) child->hide(); }
		virtual void unhide() { nineBox.unhide(); for (auto *child : children) child->unhide(); }

		bool isHidden() const { return nineBox.isHidden(); }
		bool isInside(int x, int y) const { return nineBox.isInside(x, y); }

		float getZ() const { return nineBox.getZ(); }
		virtual void setZ(float z) { nineBox.setZ(z); for (auto *child : children) child->setZ(z); }

		bool isHoverable() const { return hoverableFlag; }
		bool isDraggable() const { return draggableFlag; }

		virtual void beginHover() {}
		virtual void endHover() {}

		virtual void beginSelect() {}
		virtual void endSelect() {}

		virtual void update(float dt) { (void) (dt); }

		//virtual void setGroup(WidgetGroup *group); // This will call WidgetGroup functions.
		//virtual void setGroupReal(WidgetGroup *group) { this->group = group; } // This will not call WidgetGroup functions.
		void setGroup(WidgetGroup *group) { this->group = group; }
		WidgetGroup *getGroup() { return group; }

		virtual Widget *addChild(Widget *widget);
		virtual Widget *removeChild(Widget *widget);

		Widget *getParent() { return parent; }
		std::vector<Widget*> getChildren() { return children; }

		const std::string &getType() { return type; }
	};

	// TODO: Need to determine which mouse buttons can be used to click this.
	// TODO: Decide if this should be a separate class, or just part of the Widget.
	class Clickable
	{
	protected:
		std::function<void(Widget *w, Platform::Event &ev)> onClickFunc;

	public:
		Clickable() { auto f = [](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); }; onClickFunc = f; }
		virtual ~Clickable() {}

		virtual void onClicked(std::function<void(Widget *w, Platform::Event &ev)> f) { onClickFunc = f; }
		virtual bool clickEvent(Widget *w, Platform::Event &ev) { onClickFunc(w, ev); return false; }
	};

	// TODO: Ditto for above.
	class KeyPressable
	{
	protected:
		std::function<void(Widget *w, Platform::Event &ev)> onDownFunc;
		std::function<void(Widget *w, Platform::Event &ev)> onUpFunc;
		std::function<void(Widget *w, Platform::Event &ev)> onPressFunc;

	public:
		KeyPressable() { auto f = [](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); }; onDownFunc = f; onUpFunc = f; onPressFunc = f; }
		virtual ~KeyPressable() {}

		virtual void onKeyDown(std::function<void(Widget *w, Platform::Event &ev)> f) { onDownFunc = f; }
		virtual void onKeyUp(std::function<void(Widget *w, Platform::Event &ev)> f) { onUpFunc = f; }
		virtual void onKeyPress(std::function<void(Widget *w, Platform::Event &ev)> f) { onPressFunc = f; }

		virtual bool keyDownEvent(Widget *w, Platform::Event &ev) { onDownFunc(w, ev); return false; }
		virtual bool keyUpEvent(Widget *w, Platform::Event &ev) { onUpFunc(w, ev); return false; }
		virtual bool keyPressEvent(Widget *w, Platform::Event &ev) { onPressFunc(w, ev); return false; }
	};

	// TODO: Determine which mouse buttons can be used for dragging.
	class Draggable
	{
	protected:
		std::function<void(Widget *w, Platform::Event &ev)> onDragFunc;
		std::function<void(Widget *w, Platform::Event &ev)> onBeginDragFunc;
		std::function<void(Widget *w, Platform::Event &ev)> onEndDragFunc;

		Platform::Event startEv;
		Platform::Event prevEv;

	public:
		Draggable() { auto f = [](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); }; onDragFunc = f; onBeginDragFunc = f; onEndDragFunc = f; }
		virtual ~Draggable() {}

		virtual void onDrag(std::function<void(Widget *w, Platform::Event &ev)> f) { onDragFunc = f; }
		virtual void onBeginDrag(std::function<void(Widget *w, Platform::Event &ev)> f) { onBeginDragFunc = f; }
		virtual void onEndDrag(std::function<void(Widget *w, Platform::Event &ev)> f) { onEndDragFunc = f; }

		virtual void dragEvent(Widget *w, Platform::Event &ev) { onDragFunc(w, ev); prevEv = ev; }
		virtual void beginDragEvent(Widget *w, Platform::Event &ev) { startEv = ev; prevEv = ev; onBeginDragFunc(w, ev); }
		virtual void endDragEvent(Widget *w, Platform::Event &ev) { onEndDragFunc(w, ev); }
	};
}

