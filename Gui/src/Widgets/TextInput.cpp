#include "TextInput.h"

#include "../../../Platform/src/Window.h"
#include "../../../Utils/src/StringUtils.h"

using namespace Gui;


TextBox::TextBox() : ScrollLabel(), KeyPressable(), Clickable(), charPos(0), cursorActiveFlag(false), pressedFlag(false), currTime(0)
{
	// Not sure if I want this to be hoverable or not...
	hoverableFlag = true;

	useVertScroll(true);

	// Not sure if I want side scrolling or not.
	useSideScroll(false);

	text.setCached(true);
	text.setWrapping(true);

	addChild(&cursor);

	cursor.hide();

	type = "TextBox";
}

void TextBox::update(float dt)
{
	if (!cursorActiveFlag)
		return;

	// Blink the cursor every so often (if we're active).
	float visibleInterval = 1.0f;
	float invisibleInterval = 0.5f;

	currTime += dt;

	if (currTime > visibleInterval + invisibleInterval)
	{
		cursor.unhide();
		currTime = 0;
	}
	else if (currTime > visibleInterval)
		cursor.hide();
}

bool TextBox::keyDownEvent(Widget *w, Platform::Event &ev)
{
	if (pressedFlag)
	{
		pressedFlag = false;
		return false;
	}

	// TODO: This will trigger onDownFunc and onPressFunc, should really only trigger one of them.
	onDownFunc(w, ev);
	return keyPressEvent(w, ev);
}

bool TextBox::keyUpEvent(Widget *w, Platform::Event &ev)
{
	onUpFunc(w, ev);
	return false;
}

bool TextBox::keyPressEvent(Widget *w, Platform::Event &ev)
{
	pressedFlag = true;

	currTime = 0;
	if (cursor.isHidden())
		cursor.unhide();

	auto str = text.getStr();
	auto key = ev.key;

	// If an arrow key is pressed, then use that, otherwise input text.
	if (ev.key == Platform::Keyboard::LEFT || ev.key == Platform::Keyboard::NUMPAD_LEFT)
		charPos -= 1;
	else if (ev.key == Platform::Keyboard::RIGHT || ev.key == Platform::Keyboard::NUMPAD_RIGHT)
		charPos += 1;
	else if (ev.key == Platform::Keyboard::UP || ev.key == Platform::Keyboard::NUMPAD_UP)
	{
		auto pos = cursor.getPos();
		pos.y -= text.getFontSize();
		updateCursorPos((int) pos.x, (int) pos.y);
	}
	else if (ev.key == Platform::Keyboard::DOWN || ev.key == Platform::Keyboard::NUMPAD_DOWN)
	{
		auto pos = cursor.getPos();
		pos.y += text.getFontSize();
		updateCursorPos((int) pos.x, (int) pos.y);
	}
	else
	{
		bool shiftFlag = Platform::Keyboard::isShift() || Platform::Keyboard::isCaps();

		auto c = Platform::Keyboard::getChar(key, shiftFlag);
		if (key == Platform::Keyboard::BACKSPACE)
		{
			if (!str.empty())
			{
				str.erase(charPos - 1, 1);
				charPos -= 1;
			}
		}
		else
		{
			str.insert(charPos, c);
			charPos += 1;
		}

		setStr(str, (int) vertScrollOffset, (int) sideScrollOffset, false);
	}

	updateCursorPos();

	onPressFunc(w, ev);

	return true;
}

bool TextBox::clickEvent(Widget *w, Platform::Event &ev)
{
	updateCursorPos(ev.x, ev.y);

	onClickFunc(w, ev);

	return false;
}

void TextBox::beginSelect()
{
	cursorActiveFlag = true;
	cursor.unhide();

	currTime = 0;
}

void TextBox::endSelect()
{
	cursorActiveFlag = false;
	cursor.hide();

	currTime = 0;
}

void TextBox::updateCursorPos(int x, int y)
{
	auto pos = getPos();
	int dx = (int) (pos.x + borderWidth);
	int dy = (int) (pos.y + borderHeight);

	Math::ivec3 cursorPos;

	if (x == -1 && y == -1)
		cursorPos = text.getCursorPos(charPos);
	else
		cursorPos = text.getCursorPos(x - dx, y - dy);

	charPos = cursorPos.z;

	cursor.setPos((float) (cursorPos.x + dx), (float) (cursorPos.y + dy + 2));
	cursor.setSize(2, (float) text.getFontSize());

	Math::vec4 col(1.0, 1.0, 1.0, 0.8);
	cursor.getNineBox().setColors(col, col, col, col, col, col, col, col, col, 0);
}


ComboBox::ComboBox() : ListBox()
{
	addChild(&input);

	input.setBorder(5, 5);
	input.setSize(20, (float) input.getText().getFontSize() + 20);

	input.useVertScroll(false);
	input.useSideScroll(false);

	input.onKeyPress([this](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); this->onKeyPress(); });

	type = "ComboBox";
}

void ComboBox::addEntry(const std::string &str)
{
	ListBox::addEntry(str);
	realEntries.push_back(str);
}

void ComboBox::removeEntry(const std::string &str)
{
	ListBox::removeEntry(str);
	for (auto it = realEntries.begin(); it != realEntries.end(); it++)
	{
		if (*it == str)
		{
			realEntries.erase(it);
			break;
		}
	}
}

void ComboBox::removeEntry(size_t index)
{
	ListBox::removeEntry(index);
	if (index >= realEntries.size())
		return;

	auto it = realEntries.begin() + index;
	realEntries.erase(it);
}

void ComboBox::setPos(float x, float y)
{
	float inputSize = (float) input.getText().getFontSize() + 20;
	ListBox::setPos(x, y + inputSize);
	input.setPos(x, y);
}

void ComboBox::setSize(float w, float h)
{
	float inputSize = (float) input.getText().getFontSize() + 20;
	ListBox::setSize(w, h - inputSize);
	input.setSize(w, inputSize);
}

void ComboBox::onKeyPress()
{
	auto inputStr = input.getText().getStr();
	inputStr = Utils::removeChars(inputStr, '\n');

	// Restrict visible entries based on input.
	std::vector<std::string> newEntries;
	bool selectFlag = false;
	for (auto &entry : realEntries)
	{
		if (selected >= 0 && selected < (int) entries.size() && !selectFlag && entry == entries[selected])
		{
			newEntries.push_back(entry);
			selected = (int) newEntries.size() - 1;
			selectFlag = true;
			continue;
		}

		size_t pos = Utils::findIgnoreCase(entry, inputStr);
		if (pos == std::string::npos)
			continue;

		newEntries.push_back(entry);
	}

	// Do something with this list of new entries...
	entries = newEntries;
	updateText(false);
}


DropDownCombo::DropDownCombo() : DropDownList()
{
	addChild(&input);

	input.setBorder(5, 5);
	input.setSize(20, (float) input.getText().getFontSize() + 20);

	input.useVertScroll(false);
	input.useSideScroll(false);

	input.hide();

	input.onKeyPress([this](Widget *w, Platform::Event &ev) -> void { (void) (w); (void) (ev); this->onKeyPress(); });

	type = "DropDownCombo";
}

void DropDownCombo::hide()
{
	DropDownList::hide();
}

void DropDownCombo::unhide()
{
	nineBox.unhide();

	for (auto *widget : children)
	{
		if (widget == &list || widget == &input)
		{
			if (showListFlag)
				widget->unhide();
		}
		else
			widget->unhide();
	}
}

void DropDownCombo::toggleList()
{
	if (isHidden())
		return;

	DropDownList::toggleList();
	if (showListFlag)
		input.unhide();
	else
		input.hide();
}

void DropDownCombo::addEntry(const std::string &str)
{
	DropDownList::addEntry(str);
	realEntries.push_back(str);
}

void DropDownCombo::removeEntry(const std::string &str)
{
	DropDownList::removeEntry(str);
	for (auto it = realEntries.begin(); it != realEntries.end(); it++)
	{
		if (*it == str)
		{
			realEntries.erase(it);
			break;
		}
	}
}

void DropDownCombo::removeEntry(size_t index)
{
	DropDownList::removeEntry(index);
	if (index >= realEntries.size())
		return;

	auto it = realEntries.begin() + index;
	realEntries.erase(it);
}

void DropDownCombo::updateListPos()
{
	if (!initialPosFlag || !initialSizeFlag)
		return;

	auto buttonPos = getPos();
	auto buttonSize = getSize();

	auto inputPos = buttonPos;
	inputPos.y += buttonSize.y;

	auto inputSize = buttonSize;
	inputSize.y = (float) input.getText().getFontSize() + 20;

	auto listPos = inputPos;
	listPos.y += inputSize.y;

	auto listSize = buttonSize;
	listSize.y = listHeight;

	input.setPos(inputPos.x, inputPos.y);
	input.setSize(inputSize.x, inputSize.y);

	list.setPos(listPos.x, listPos.y);
	list.setSize(listSize.x, listSize.y);
}

void DropDownCombo::onKeyPress()
{
	auto inputStr = input.getText().getStr();
	inputStr = Utils::removeChars(inputStr, '\n');

	// Restrict visible entries based on input.
	std::vector<std::string> newEntries;
	bool selectFlag = false;
	for (auto &entry : realEntries)
	{
		auto selected = list.getSelected();
		if (selected >= 0 && selected <= (int) list.getEntries().size() && !selectFlag && entry == list.getEntries()[selected])
		{
			newEntries.push_back(entry);
			list.setSelected((int) newEntries.size() - 1);
			selectFlag = true;
			continue;
		}

		size_t pos = Utils::findIgnoreCase(entry, inputStr);
		if (pos == std::string::npos)
			continue;

		newEntries.push_back(entry);
	}

	// Do something with this list of new entries...
	list.setEntries(newEntries);
	list.updateText(false);
}


Console::Console() : ScrollLabel(), height(0), historyPos(0)
{
	addChild(&input);

	input.setBorder(5, 5);

	input.useVertScroll(false);
	input.useSideScroll(false);

	input.onKeyPress([this](Widget *w, Platform::Event &ev) -> void { (void) (w); this->onKeyPress(ev.key); });

	useSideScroll(false);

	hide();

	type = "Console";
}

void Console::setHeight(int height)
{
	this->height = height;

	setPos(0, 0);
	setSize((float) Platform::Window::getWidth(), (float) height);
}

void Console::hide()
{
	ScrollLabel::hide();
}

void Console::unhide()
{
	// Going to piggyback on this function since I'm lazy.
	setHeight(this->height);
	ScrollLabel::unhide();
}

void Console::setSize(float w, float h)
{
	ScrollLabel::setSize(w, h);

	input.setPos(0, (float) height);
	input.setSize(w, (float) input.getText().getFontSize() + 20);
}

void Console::onKeyPress(Platform::Keyboard::Key key)
{
	auto str = getText().getStr();
	auto inputStr = input.getText().getStr();

	if (key == Platform::Keyboard::GRAVE || key == Platform::Keyboard::ASCIITILDE)
	{
		// Remove character from the text and hide the console.
		str.erase(str.length() - 1);
		toggle();
	}
	else if (key == Platform::Keyboard::ENTER)
	{
		// TODO: Run the command if possible.
		inputStr = Utils::removeChars(inputStr, '\n');

		if (inputStr != "")
			setStr(str + inputStr + "\n");

		history.push_back(inputStr);
		historyPos = (int) history.size();

		input.setStr("");
		input.updateCursorPos();
	}
	else if (key == Platform::Keyboard::UP || key == Platform::Keyboard::DOWN)
	{
		if (!history.empty())
		{
			if (key == Platform::Keyboard::UP)
				historyPos--;
			else
				historyPos++;

			if (historyPos > (int) history.size())
				historyPos = (int) history.size();
			if (historyPos < 0)
				historyPos = 0;

			if (historyPos == (int) history.size())
				inputStr = "";
			else
				inputStr = history[historyPos];
			input.setStr(inputStr);
			input.updateCursorPos(1000000, 1000000);
		}
	}
}

