#include "TextOutput.h"
#include "../Canvas.h"
#include "../../../Renderer/src/RenderResource/Font.h"

using namespace Gui;

Label::Label() : Widget(), borderWidth(0), borderHeight(0), borderFlag(false)
{
	Canvas::instance().addText(&text);

	// Set a default font and colour.
	auto *defaultFont = Canvas::instance().getFont("default");
	if (defaultFont)
		text.setFont(defaultFont);
	else
		std::cout << "Warning: default font not found\n";

	text.setColor({0, 0, 1});

	type = "Label";

	// May as well have a default border of 5 pixels.
	setBorder(5, 5);
}

Label::~Label()
{
	Canvas::instance().removeText(&text);
}

void Label::setPos(float x, float y)
{
	//auto innerPos = nineBox.getInnerPos();
	//auto pos = nineBox.getPos();
	//float dx = innerPos.x - pos.x;
	//float dy = innerPos.y - pos.y;

	Widget::setPos(x, y);

	// Need to take border into account when setting this.
	text.setPos({x + borderWidth, y + borderHeight});
}

void Label::setSize(float w, float h)
{
	Widget::setSize(w, h);

	text.setMaxSize({w, h});
}

void Label::setBorder(float w, float h)
{
	borderWidth = w;
	borderHeight = h;

	Widget::setBorder(w, h);

	// TODO: Make sure this is a valid size.
	auto pos = text.getPos();
	auto size = text.getMaxSize();
	text.setPos({pos.x + w, pos.y + h});
	text.setMaxSize({size.x - (w * 2), size.y - (h * 2)});
}

void Label::setStr(const std::string &str, int vertScroll, int sideScroll, bool resizeFlag)
{
	text.setPos({nineBox.getPos().x + borderWidth, nineBox.getPos().y + borderHeight});
	text.setVertScroll(vertScroll);
	text.setSideScroll(sideScroll);

	text.setStr(str);
	auto size = text.getMaxSize();

	if (!borderFlag)
	{
		text.setMaxSize({size.x - (borderWidth * 2), size.y - (borderHeight * 2)});
		borderFlag = true;
	}
	else
		text.setMaxSize({size.x, size.y});

	// Should I resize the label here or not. Kind of feels like I should.
	if (resizeFlag)
		setSize((float) getTextWidth() + 10, (float) getTextHeight() + 10);
}

