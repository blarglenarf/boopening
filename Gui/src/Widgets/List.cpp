#include "List.h"
#include "WidgetGroup.h"
#include "../Canvas.h"

#include "../../../Renderer/src/RenderResource/Font.h"

#include <algorithm>

using namespace Gui;


static void setBoxColors(NineBox &box)
{
	auto col = Math::vec4(0.7, 0.0, 0.7, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.9, 0.0, 0.9, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	box.setColors(col, col, col, col, col, col, col, col, col, 2);
}


ScrollLabel::ScrollLabel() : Label(), vertScrollOffset(0), sideScrollOffset(0), useVertFlag(true), useSideFlag(true), fitsVertFlag(true), fitsSideFlag(true)
{
	hoverableFlag = true;

	vertScrollbar.onValUpdate([this](Widget *w, float val) -> void { (void)(w); this->updateVertScrollOffset(val); });
	vertScrollbar.setVertical(true);
	vertScrollbar.setRange(0, 1);

	sideScrollbar.onValUpdate([this](Widget *w, float val) -> void { (void)(w); this->updateSideScrollOffset(val); });
	sideScrollbar.setVertical(false);
	sideScrollbar.setRange(0, 1);

	// TODO: Get defaults for all this.
	NineBox &vertScrollBox = vertScrollbar.getNineBox();
	auto col = Math::vec4(0.5, 0.0, 0.5, 0.8);
	vertScrollBox.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.8, 0.0, 0.8, 0.8);
	vertScrollBox.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	vertScrollBox.setColors(col, col, col, col, col, col, col, col, col, 2);

	setBoxColors(vertScrollbar.getButton().getNineBox());
	setBoxColors(vertScrollbar.getLeftButton().getNineBox());
	setBoxColors(vertScrollbar.getRightButton().getNineBox());

	NineBox &sideScrollBox = sideScrollbar.getNineBox();
	col = Math::vec4(0.5, 0.0, 0.5, 0.8);
	sideScrollBox.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.8, 0.0, 0.8, 0.8);
	sideScrollBox.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	sideScrollBox.setColors(col, col, col, col, col, col, col, col, col, 2);

	setBoxColors(sideScrollbar.getButton().getNineBox());
	setBoxColors(sideScrollbar.getLeftButton().getNineBox());
	setBoxColors(sideScrollbar.getRightButton().getNineBox());

	addChild(&vertScrollbar);
	addChild(&sideScrollbar);

	vertScrollbar.hide();
	sideScrollbar.hide();

	type = "ScrollLabel";
}

void ScrollLabel::useVertScroll(bool useVertFlag)
{
	this->useVertFlag = useVertFlag;

	if (!isHidden() && useVertFlag && !fitsVertFlag)
		vertScrollbar.unhide();
	else
		vertScrollbar.hide();

	updateTextSize();
}

void ScrollLabel::useSideScroll(bool useSideFlag)
{
	this->useSideFlag = useSideFlag;

	if (!isHidden() && useSideFlag && !fitsSideFlag)
		sideScrollbar.unhide();
	else
		sideScrollbar.hide();

	updateTextSize();
}

void ScrollLabel::calcTextFits()
{
	float scrollX = 0, scrollY = 0;
	if (!vertScrollbar.isHidden())
		scrollX = vertScrollbar.getSize().x;
	if (!sideScrollbar.isHidden())
		scrollY = sideScrollbar.getSize().y;

	float innerX = nineBox.getSize().x - nineBox.getInnerSize().x;
	float innerY = nineBox.getSize().y - nineBox.getInnerSize().y;
	float maxX = std::max(0.0f, getTextWidth() - getSize().x + innerX + scrollX);
	float maxY = std::max(0.0f, getTextHeight() - getSize().y + innerY + scrollY);

	fitsVertFlag = maxY <= 0;
	fitsSideFlag = maxX <= 0;

	vertScrollbar.setRange(0, std::max(0.0f, maxY));
	sideScrollbar.setRange(0, std::max(0.0f, maxX));
}

void ScrollLabel::updateTextSize()
{
	auto size = getSize();

	auto vertSize = vertScrollbar.getSize();
	auto sideSize = sideScrollbar.getSize();
	if (!vertScrollbar.isHidden())
		size.x -= vertSize.x;
	if (!sideScrollbar.isHidden())
		size.y -= sideSize.y;

	if (borderFlag)
		text.setMaxSize({size.x - (borderWidth * 2), size.y - (borderHeight * 2)});
	else
		text.setMaxSize({size.x, size.y});

	calcTextFits();
}

void ScrollLabel::unhide()
{
	Label::unhide();

	if (fitsVertFlag || !useVertFlag)
		vertScrollbar.hide();
	if (fitsSideFlag || !useSideFlag)
		sideScrollbar.hide();

	updateTextSize();
}

void ScrollLabel::setStr(const std::string &str, int vertScroll, int sideScroll, bool resizeFlag)
{
	Label::setStr(str, vertScroll, sideScroll, resizeFlag);

	calcTextFits();

	bool redoFlag = false;
	if (!isHidden())
	{
		if (!fitsVertFlag && useVertFlag)
		{
			redoFlag |= vertScrollbar.isHidden();
			vertScrollbar.unhide();
		}
		else
		{
			redoFlag |= !vertScrollbar.isHidden();
			vertScrollbar.hide();
		}
		if (!fitsSideFlag && useSideFlag)
		{
			redoFlag |= sideScrollbar.isHidden();
			sideScrollbar.unhide();
		}
		else
		{
			redoFlag |= !sideScrollbar.isHidden();
			sideScrollbar.hide();
		}
	}

	updateTextSize();

	if (redoFlag)
		setStr(str, vertScroll, sideScroll, resizeFlag);
}

void ScrollLabel::updateVertScrollOffset(float val)
{
	vertScrollOffset = std::max(0.0f, val);
	setStr(text.getStr(), (int) vertScrollOffset, (int) sideScrollOffset, false);
}

void ScrollLabel::updateVertScrollbar()
{
	auto size = getSize();
	auto pos = getPos();

	if ((size.x == 0 && size.y == 0) || (pos.x == 0 && pos.y == 0))
		return;

	if (!sideScrollbar.isHidden())
		vertScrollbar.setSize(20, size.y - sideScrollbar.getSize().y);
	else
		vertScrollbar.setSize(20, size.y);
	vertScrollbar.setPos(pos.x + size.x - 20, pos.y);
	vertScrollbar.setBorder(5, 5);
}

void ScrollLabel::updateSideScrollOffset(float val)
{
	sideScrollOffset = std::max(0.0f, val);
	setStr(text.getStr(), (int) vertScrollOffset, (int) sideScrollOffset, false);
}

void ScrollLabel::updateSideScrollbar()
{
	auto size = getSize();
	auto pos = getPos();

	if ((size.x == 0 && size.y == 0) || (pos.x == 0 && pos.y == 0))
		return;

	if (!vertScrollbar.isHidden())
		sideScrollbar.setSize(size.x - vertScrollbar.getSize().x, 20);
	else
		sideScrollbar.setSize(size.x, 20);
	sideScrollbar.setPos(pos.x, pos.y + size.y - 20);
	sideScrollbar.setBorder(5, 5);
}

void ScrollLabel::setPos(float x, float y)
{
	Label::setPos(x, y);
	updateVertScrollbar();
	updateSideScrollbar();

	updateTextSize();
}

void ScrollLabel::setSize(float w, float h)
{
	Label::setSize(w, h);
	updateVertScrollbar();
	updateSideScrollbar();

	updateTextSize();

	if (isHidden())
		return;

	if (fitsVertFlag)
		vertScrollbar.hide();
	else if (!fitsVertFlag && useVertFlag)
		vertScrollbar.unhide();

	if (fitsSideFlag)
		sideScrollbar.hide();
	else if (!fitsSideFlag && useSideFlag)
		sideScrollbar.unhide();
}


ListBox::ListBox() : ScrollLabel(), Clickable(), selected(-1)
{
	hoverableFlag = true;
	setLinePadding(5);

	useVertScroll(true);
	useSideScroll(false);

	// TODO: Some default stuff for the selected box.
	NineBox &selectedBox = selectedWidget.getNineBox();
	auto col = Math::vec4(0.7, 0.0, 0.7, 0.8);
	selectedBox.setColors(col, col, col, col, col, col, col, col, col, 0);
	col = Math::vec4(0.9, 0.0, 0.9, 0.8);
	selectedBox.setColors(col, col, col, col, col, col, col, col, col, 1);
	col = Math::vec4(1, 0, 1, 0.8);
	selectedBox.setColors(col, col, col, col, col, col, col, col, col, 2);

	addChild(&selectedWidget);

	selectedWidget.hide();

	type = "ListBox";
}

void ListBox::addEntry(const std::string &str)
{
	entries.push_back(str);
	updateText(false);
}

void ListBox::removeEntry(const std::string &str)
{
	for (auto it = entries.begin(); it != entries.end(); it++)
	{
		if (*it == str)
		{
			entries.erase(it);
			updateText(false);
			break;
		}
	}
}

void ListBox::removeEntry(size_t index)
{
	if (index >= entries.size())
		return;

	auto it = entries.begin() + index;
	entries.erase(it);
	updateText(false);
}

void ListBox::updateText(bool resizeFlag)
{
	std::string newStr;
	for (int i = 0; i < (int) entries.size() - 1; i++)
		newStr += entries[i] + "\n";
	if (entries.size() > 1)
		newStr += entries[entries.size() - 1];

	if (newStr == str)
		return;

	str = newStr;
	setStr(str, (int) vertScrollOffset, (int) sideScrollOffset, resizeFlag);

	updateVertScrollbar();
	updateSideScrollbar();
}

void ListBox::updateSelected()
{
	// FIXME: Seems to be a bit of wobbling when scrolling. What's up with that?
	if (selected < 0 || selected >= (int) entries.size())
	{
		selectedWidget.hide();
		return;
	}

	if (!isHidden() && selectedWidget.isHidden())
		selectedWidget.unhide();

	auto pos = getPos();
	auto size = getSize();
	auto origPos = pos;
	auto origSize = size;

	// Take border into account.
	pos.x += 5;
	pos.y += 5;

	// Support multi-line entries.
	size.y = (float) (text.getFont()->getSize() + text.getLinePadding() + 1);
	if (!vertScrollbar.isHidden())
		size.x -= (vertScrollbar.getSize().x + 10);
	else
		size.x -= 10;

	pos.y += selected * (text.getFont()->getSize() + text.getLinePadding()) - (int) vertScrollOffset;

	// Clamp top to within the listbox.
	float topDiff = pos.y - (origPos.y + 5);
	if (topDiff < 0)
	{
		pos.y -= topDiff;
		size.y += topDiff;
	}

	// No need to render if we're outside the box.
	float botScroll = sideScrollbar.isHidden() ? 0 : sideScrollbar.getSize().y;
	if (pos.y > origPos.y + origSize.y - 5 - botScroll || pos.y + size.y < origPos.y + 5)
	{
		selectedWidget.hide();
		return;
	}

	// Clamp to within bottom of listbox.
	float botDiff = pos.y + size.y - (origPos.y + origSize.y - 5) + botScroll;
	if (botDiff > 0)
	{
		size.y -= botDiff;
	}

	selectedWidget.setPos(pos.x, pos.y);
	selectedWidget.setSize(size.x, size.y);
}

void ListBox::selectEntry(int selected)
{
	if (this->selected == selected || selected >= (int) entries.size())
		return;

	this->selected = selected;
	updateSelected();
}

const std::string &ListBox::getSelectedEntry() const
{
	static std::string empty = "";
	if (selected < (int) entries.size())
		return entries[selected];
	return empty;
}

void ListBox::setStr(const std::string &str, int vertScroll, int sideScroll, bool resizeFlag)
{
	ScrollLabel::setStr(str, vertScroll, sideScroll, resizeFlag);
	updateSelected();
}

bool ListBox::clickEvent(Widget *w, Platform::Event &ev)
{
	// TODO: Select one of the entries if appropriate, and then that becomes part of the event I guess, or perhaps the widget.
	auto innerPos = nineBox.getInnerPos();
	auto innerSize = nineBox.getInnerSize();
	int xOffset = ev.x - (int) innerPos.x;
	int yOffset = ev.y - (int) innerPos.y;

	// FIXME: Doesn't appear to be completely correct.
	if (xOffset < 0 || yOffset < 0 || xOffset > innerPos.x + innerSize.x || yOffset > innerPos.y + innerSize.y)
		return false;

	yOffset += (int) vertScrollOffset;
	int newSelected = yOffset / (getLinePadding() + text.getFont()->getSize());

	selectEntry(newSelected);

	//std::cout << entries[selected] << "\n";

	onClickFunc(w, ev);

	return true;
}


DropDownList::DropDownList() : Button(), listHeight(200), initialPosFlag(false), initialSizeFlag(false), showListFlag(false)
{
	addChild(&list);

	list.setBorder(5, 5);

	list.hide();

	onClicked([](Widget *w, Platform::Event &ev) -> void { (void) (ev); ((DropDownList*) (w))->toggleList(); });

	type = "DropDownList";
}

void DropDownList::hide()
{
	nineBox.hide();
	for (auto *widget : children)
		widget->hide();
}

void DropDownList::unhide()
{
	nineBox.unhide();
	for (auto *widget : children)
	{
		if (widget == &list)
		{
			if (showListFlag)
				widget->unhide();
		}
		else
			widget->unhide();
	}
}

void DropDownList::toggleList()
{
	if (isHidden())
		return;

	showListFlag = !showListFlag;
	if (showListFlag)
		list.unhide();
	else
		list.hide();
}

void DropDownList::updateListPos()
{
	if (!initialPosFlag || !initialSizeFlag)
		return;

	auto pos = getPos();
	auto size = getSize();

	list.setPos(pos.x, pos.y + size.y);
	list.setSize(size.x, listHeight);
}

void DropDownList::setPos(float x, float y)
{
	Button::setPos(x, y);

	initialPosFlag = true;
	updateListPos();
}

void DropDownList::setSize(float w, float h)
{
	Button::setSize(w, h);

	initialSizeFlag = true;
	updateListPos();
}

