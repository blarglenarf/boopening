#pragma once

#include <vector>

namespace Gui
{
	class Widget;

	class WidgetGroup
	{
	private:
		std::vector<Widget*> widgets;

		// Note: multiple groups may be active at once, this is only the widget hovering for this group, not the whole gui.
		Widget *hoveredWidget;

		int prevMouseX;
		int prevMouseY;

		bool hiddenFlag;

	public:
		WidgetGroup() : hoveredWidget(nullptr), prevMouseX(0), prevMouseY(0), hiddenFlag(false) {}
		~WidgetGroup();

		Widget *addWidget(Widget *widget);
		Widget *removeWidget(Widget *widget);

		void update(float dt, int x, int y);

		Widget *getHoveredWidget() const { return hoveredWidget; }

		void hide();
		void unhide();

		bool isHidden() const { return hiddenFlag; }
	};
}

