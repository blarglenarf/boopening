#version 430

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;


in vec3 normal;
in vec3 esFrag;
in vec2 texCoord;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

out vec4 fragColor;


vec4 getAmbient(float alpha)
{
	vec4 ambient = vec4(Material.ambient.xyz * 0.4, Material.ambient.w);
	if (texFlag)
	{
		vec4 texColor = texture2D(diffuseTex, texCoord);
		ambient = texColor * 0.4;
		ambient.w = min(Material.ambient.w, texColor.w);
	}
	ambient.w = min(ambient.w, alpha);
	return ambient;
}

vec4 directionLight(vec3 viewDir, vec3 normal)
{
	vec4 color = vec4(0);

	//vec3 lightDir = normalize(-VertexIn.esFrag);
	vec3 lightDir = vec3(0, 0, 1);
	float cosTheta = clamp(dot(normal, lightDir), 0.0, 1.0);

	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float dp = clamp(cosTheta, 0.0, 1.0);

	if (dp > 0.0)
	{
		if (texFlag)
			diffuse = texture2D(diffuseTex, texCoord).xyz * dp;
		else
			diffuse = Material.diffuse.xyz * dp;
		vec3 reflection = reflect(-lightDir, normal);
		float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
		float intensity = pow(nDotH, Material.shininess);
		specular = Material.specular.xyz * intensity;
		color += vec4(diffuse * 0.5, 0);
	}

	return color;
}



void main()
{
	vec4 color = vec4(0);

	vec3 viewDir = normalize(-esFrag);
	vec3 normal = normalize(normal);

	color = getAmbient(0.3);

	if (color.w == 0)
	{
		discard;
		return;
	}
	color.w = 1.0;

	color += directionLight(viewDir, normal);
	fragColor = color;

	fragColor = vec4(1, 0, 0, 1);
}

