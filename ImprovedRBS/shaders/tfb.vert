#version 430

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normalIn;
layout(location = 2) in vec2 texCoordIn;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;


out vec3 normal;
out vec3 esFrag;
out vec2 texCoord;

void main()
{
	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

    esFrag = esVert.xyz;
	normal = nMatrix * normalize(normalIn);
	texCoord = texCoordIn;

	gl_Position = csVert;
}

