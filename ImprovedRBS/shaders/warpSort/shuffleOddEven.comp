#version 450

#extension GL_NV_shader_thread_shuffle : enable

#define N_THREADS 32


layout(local_size_x = N_THREADS, local_size_y = 1, local_size_z = 1) in;

#define SAVE_DEEP_DATA 0

#define MAX_FRAGS 0
#define MAX_FRAGS_OVERRIDE 0

//#define FRAG_SIZE 2
#define INDEX_BY_TILE 0

#define ACTUALLY_SORT 1

#define LFB_FRAG_TYPE vec2
#define USE_FLOAT 1

#import "lfb"

coherent buffer IntervalPixels
{
	uint intervalPixels[];
};

coherent buffer PixelData
{
	float pixelData[];
};

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

// To use different deep images, call replace(LFB_NAME, ...).
LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);


#if MAX_FRAGS_OVERRIDE != 0
	#define _MAX_FRAGS MAX_FRAGS_OVERRIDE
#else
	#define _MAX_FRAGS MAX_FRAGS
#endif

#define LFB_FRAG_DEPTH(frag) frag.y

#define LFB_GET_FRAG(LFB_NAME) LFB_GET_DATA(LFB_NAME)
//#define LFB_PUT_FRAG(LFB_NAME, frag) LFB_WRITE_DATA(LFB_NAME, frag)

#if USE_FLOAT
	#define GET_COLOR(val) floatToRGBA8(val)
#else
	#define GET_COLOR(val) uintToRGBA8(val)
#endif

#define MAX_REGISTERS (_MAX_FRAGS / N_THREADS)


LFB_FRAG_TYPE vals[MAX_REGISTERS];
LFB_FRAG_TYPE swapVals[2];


LFB_FRAG_TYPE tmp;

#define SWAP_FRAGS(a, b) {tmp = a; a = b; b = tmp;}
#define CSWAP(i, j) if (LFB_FRAG_DEPTH(vals[i]) > LFB_FRAG_DEPTH(vals[j])) {SWAP_FRAGS(vals[i], vals[j]);}



void loadRegisters(int pixel, int offset, int count)
{
	// TODO: Something with swapVals maybe?
	//#define LOAD_FRAG(i) if (i < count && LFB_ITER_CHECK(LFB_NAME)) { vals[i] = LFB_GET_DATA(LFB_NAME); LFB_ITER_INC(LFB_NAME); } else { LFB_FRAG_DEPTH(vals[i]) = 16777215; }
	//LFB_ITER_SET(LFB_NAME, pixel, offset);

	uint fragCount = LFB_COUNT_AT(LFB_NAME, pixel);
	uint fragOffset = LFB_OFFSET_AT(LFB_NAME, pixel);

	#define LOAD_FRAG(i) if (i < count && i + offset < fragCount) { vals[i] = data##LFB_NAME[fragOffset + (i + offset)]; } else { LFB_FRAG_DEPTH(vals[i]) = 16777215; }

	LOAD_FRAG(0);
	#if MAX_REGISTERS > 1
		LOAD_FRAG(1);
	#endif
	#if MAX_REGISTERS > 2
		LOAD_FRAG(2);LOAD_FRAG(3);
	#endif
	#if MAX_REGISTERS > 4
		LOAD_FRAG(4);LOAD_FRAG(5);LOAD_FRAG(6);LOAD_FRAG(7);
	#endif
	#if MAX_REGISTERS > 8
		LOAD_FRAG(8);LOAD_FRAG(9);LOAD_FRAG(10);LOAD_FRAG(11);LOAD_FRAG(12);LOAD_FRAG(13);LOAD_FRAG(14);LOAD_FRAG(15);
	#endif
	#if MAX_REGISTERS > 16
		LOAD_FRAG(16);LOAD_FRAG(17);LOAD_FRAG(18);LOAD_FRAG(19);LOAD_FRAG(20);LOAD_FRAG(21);LOAD_FRAG(22);LOAD_FRAG(23);
		LOAD_FRAG(24);LOAD_FRAG(25);LOAD_FRAG(26);LOAD_FRAG(27);LOAD_FRAG(28);LOAD_FRAG(29);LOAD_FRAG(30);LOAD_FRAG(31);
	#endif

	#undef LOAD_FRAG
}

// All threads will composite.
void writeRegisters(int pixel, int offset, int count)
{
	uint fragCount = LFB_COUNT_AT(LFB_NAME, pixel);
	uint fragOffset = LFB_OFFSET_AT(LFB_NAME, pixel);

	// Depends how you want the data ordered/composited.
	#define WRITE_REVERSE 1

	// FIXME: This won't work for linked list lfb format.
#if WRITE_REVERSE
	#define LFB_PUT_FRAG(i) data##LFB_NAME[fragOffset + fragCount - 1 - (i + offset)] = vals[i];
#else
	#define LFB_PUT_FRAG(i) data##LFB_NAME[fragOffset + (i + offset)] = vals[i];
#endif

	//#define WRITE_FRAG(i) if (i < count) { testData[offset + i] = vals[i]; }

	//#define LFB_PUT_FRAG(i) data##LFB_NAME[iter##LFB_NAME.end - 1 - iter##LFB_NAME.node] = vals[i];

	//#define WRITE_FRAG(i) if (i < count && i + offset < fragCount) { LFB_WRITE_DATA(LFB_NAME, vals[i]); LFB_ITER_INC(LFB_NAME); }
	//#define WRITE_FRAG(i) if (i < count && i + offset < fragCount) { LFB_PUT_FRAG(i); LFB_ITER_INC(LFB_NAME); }
	//LFB_ITER_SET(LFB_NAME, pixel, offset);

	#define WRITE_FRAG(i) if (i < count && i + offset < fragCount) { LFB_PUT_FRAG(i); }

	WRITE_FRAG(0);
	#if MAX_REGISTERS > 1
		WRITE_FRAG(1);
	#endif
	#if MAX_REGISTERS > 2
		WRITE_FRAG(2);WRITE_FRAG(3);
	#endif
	#if MAX_REGISTERS > 4
		WRITE_FRAG(4);WRITE_FRAG(5);WRITE_FRAG(6);WRITE_FRAG(7);
	#endif
	#if MAX_REGISTERS > 8
		WRITE_FRAG(8);WRITE_FRAG(9);WRITE_FRAG(10);WRITE_FRAG(11);WRITE_FRAG(12);WRITE_FRAG(13);WRITE_FRAG(14);WRITE_FRAG(15);
	#endif
	#if MAX_REGISTERS > 16
		WRITE_FRAG(16);WRITE_FRAG(17);WRITE_FRAG(18);WRITE_FRAG(19);WRITE_FRAG(20);WRITE_FRAG(21);WRITE_FRAG(22);WRITE_FRAG(23);
		WRITE_FRAG(24);WRITE_FRAG(25);WRITE_FRAG(26);WRITE_FRAG(27);WRITE_FRAG(28);WRITE_FRAG(29);WRITE_FRAG(30);WRITE_FRAG(31);
	#endif

	#undef WRITE_FRAG
}

float mixAlpha(float x, float y)
{
	// combined_alpha = 1 - (1 - a_1) * (1 - a_2)
	return 1.0 - (1.0 - x) * (1.0 - y);
}

// If you only want one thread to do the composite.
vec4 compositeRegisters(int thread, int pixel, int offset, int count)
{
	vec4 fragColor = vec4(0.0);

	LFB_FRAG_TYPE val;

#if 1
	float blockVis = 1.0;
	vec4 blockCol = vec4(0.0);

	// First composite registers from our block.
	//#define WRITE_FRAG(i) if (i < count && i + offset < LFB_COUNT_AT(LFB_NAME, pixel)) { val = vals[i]; vec4 col = floatToRGBA8(val.x); blockCol.rgb = mix(blockCol.rgb, col.rgb, col.a); blockCol.a = 1.0 - (1.0 - blockCol.a) * (1.0 - col.a); }
	//#define WRITE_FRAG(i) if (i < count) { val = vals[i]; vec4 col = uintToRGBA8(val.x); col.rgb *= col.a; blockVis *= (1.0 - col.a); blockCol.rgb += col.rgb * blockVis; blockCol.a = mixAlpha(blockCol.a, col.a); }
	#define WRITE_FRAG(i) if (i < count && i + offset < LFB_COUNT_AT(LFB_NAME, pixel)) { val = vals[i]; vec4 col = GET_COLOR(val.x); col.rgb *= col.a; blockCol.rgb += col.rgb * blockVis; blockVis *= (1.0 - col.a); blockCol.a = mixAlpha(blockCol.a, col.a); }

	// Composite needs to be from front to back (I think).
	WRITE_FRAG(0);
	#if MAX_REGISTERS > 1
		WRITE_FRAG(1);
	#endif
	#if MAX_REGISTERS > 2
		WRITE_FRAG(2);WRITE_FRAG(3);
	#endif
	#if MAX_REGISTERS > 4
		WRITE_FRAG(4);WRITE_FRAG(5);WRITE_FRAG(6);WRITE_FRAG(7);
	#endif
	#if MAX_REGISTERS > 8
		WRITE_FRAG(8);WRITE_FRAG(9);WRITE_FRAG(10);WRITE_FRAG(11);WRITE_FRAG(12);WRITE_FRAG(13);WRITE_FRAG(14);WRITE_FRAG(15);
	#endif
	#if MAX_REGISTERS > 16
		WRITE_FRAG(16);WRITE_FRAG(17);WRITE_FRAG(18);WRITE_FRAG(19);WRITE_FRAG(20);WRITE_FRAG(21);WRITE_FRAG(22);WRITE_FRAG(23);
		WRITE_FRAG(24);WRITE_FRAG(25);WRITE_FRAG(26);WRITE_FRAG(27);WRITE_FRAG(28);WRITE_FRAG(29);WRITE_FRAG(30);WRITE_FRAG(31);
	#endif

	#undef WRITE_FRAG

	//int shuffleUpNV(int data, uint delta, uint width, [out bool threadIdValid])
    //int shuffleDownNV(int data, uint delta, uint width, [out bool threadIdValid])

	// TODO: Test to see if it's better to shuffle vec4's or pack/unpack and shuffle floats.
	//for (int i = N_THREADS / 2; i >= 1; i /= 2) // 16, 8, 4, 2, 1.
	// All threads have blockColor, composite in a binary fashion.
	// * Step 1: Composite with thread + 1 (eg 0-1, 2-3, 4-5, 6-7, 8-9, 10-11, 12-13, 14-15, 16-17, 18-19, 20-21, 22-23, 24-25, 26-27, 28-29, 30-31).
	// * Step 2: Composite with thread + 2 (eg 0-2, 4-6, 8-10, 12-14, 16-18, 20-22, 24-26, 28-30).
	// * Step 3: Composite with thread + 4 (eg 0-4, 8-12, 16-20, 24-28).
	// * Step 4: Composite with thread + 8 (eg 0-8, 16-24).
	// * Step 5: Composite with thread + 16 (eg 0-16).
	for (int i = 1; i <= N_THREADS / 2; i *= 2)
	{
		//a = mixAlpha(blocks[idx1].alpha, blocks[idx2].alpha)
		//f = blocks[idx1].val + blocks[idx2].val * (1.0 - blocks[idx1].alpha)
		//vec4 col = shuffleXorNV(blockCol, i, N_THREADS);
		vec4 col = shuffleNV(blockCol, thread + i, N_THREADS);
		blockCol.rgb = blockCol.rgb + col.rgb * (1.0 - blockCol.a);
		blockCol.a = mixAlpha(blockCol.a, col.a);
	}

	#if 0
	// A brute-force approach.
	for (int t = N_THREADS - 1; t >= 0; t--)
	{
		vec4 newCol = shuffleNV(blockCol, t, N_THREADS);
		//newCol = vec4(1.0, 0.0, 0.0, 0.3);

		// If the thread contained no data, then ignore.
		if (t * MAX_REGISTERS >= LFB_COUNT_AT(LFB_NAME, pixel))
			continue;

		// TODO: Do I need to combine alpha as well?
		fragColor.rgb = mix(fragColor.rgb, newCol.rgb, newCol.a);
		//fragColor = newCol;
	}
	#else
	fragColor = blockCol;
	#endif
#else
	// All threads need to execute the shuffle, but only thread 0 composites.
	#define WRITE_FRAG(i, r, t) val = shuffleNV(vals[r], t, N_THREADS); if (thread == 0 && i < LFB_COUNT_AT(LFB_NAME, pixel)) { vec4 col = uintToRGBA8(val.x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); }
	//#define WRITE_FRAG(i, r, t) val = shuffleNV(vals[r], t, N_THREADS); if (thread == 0 && i < LFB_COUNT_AT(LFB_NAME, pixel)) { LFB_WRITE_DATA(LFB_NAME, val); LFB_ITER_INC(LFB_NAME); }

	//LFB_ITER_BEGIN(LFB_NAME, pixel);

	// Composite data from the other threads first.
	for (uint t = N_THREADS - 1; t > 0; t--)
	{
		// The thread may not actually have any data for us to use...
		if (t * MAX_REGISTERS >= LFB_COUNT_AT(LFB_NAME, pixel))
			continue;
	#if MAX_REGISTERS == 1
		WRITE_FRAG(t, 0, t);
	#elif MAX_REGISTERS == 2
		WRITE_FRAG(t * 2 + 1, 1, t);
		WRITE_FRAG(t * 2, 0, t);
	#elif MAX_REGISTERS == 4
		WRITE_FRAG(t * 4 + 3, 3, t);
		WRITE_FRAG(t * 4 + 2, 2, t);
		WRITE_FRAG(t * 4 + 1, 1, t);
		WRITE_FRAG(t * 4, 0, t);
	#elif MAX_REGISTERS == 8
		WRITE_FRAG(t * 8 + 7, 7, t);
		WRITE_FRAG(t * 8 + 6, 6, t);
		WRITE_FRAG(t * 8 + 5, 5, t);
		WRITE_FRAG(t * 8 + 4, 4, t);
		WRITE_FRAG(t * 8 + 3, 3, t);
		WRITE_FRAG(t * 8 + 2, 2, t);
		WRITE_FRAG(t * 8 + 1, 1, t);
		WRITE_FRAG(t * 8, 0, t);
	#elif MAX_REGISTERS == 16
		WRITE_FRAG(t * 16 + 15, 15, t);
		WRITE_FRAG(t * 16 + 14, 14, t);
		WRITE_FRAG(t * 16 + 13, 13, t);
		WRITE_FRAG(t * 16 + 12, 12, t);
		WRITE_FRAG(t * 16 + 11, 11, t);
		WRITE_FRAG(t * 16 + 10, 10, t);
		WRITE_FRAG(t * 16 + 9, 9, t);
		WRITE_FRAG(t * 16 + 8, 8, t);
		WRITE_FRAG(t * 16 + 7, 7, t);
		WRITE_FRAG(t * 16 + 6, 6, t);
		WRITE_FRAG(t * 16 + 5, 5, t);
		WRITE_FRAG(t * 16 + 4, 4, t);
		WRITE_FRAG(t * 16 + 3, 3, t);
		WRITE_FRAG(t * 16 + 2, 2, t);
		WRITE_FRAG(t * 16 + 1, 1, t);
		WRITE_FRAG(t * 16, 0, t);
	#endif
	}

	#undef WRITE_FRAG

	#define WRITE_FRAG(i) if (thread == 0 && i < LFB_COUNT_AT(LFB_NAME, pixel)) { val = vals[i]; vec4 col = uintToRGBA8(val.x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); }
	//#define WRITE_FRAG(i) if (thread == 0 && i < LFB_COUNT_AT(LFB_NAME, pixel)) { val = vals[i]; LFB_WRITE_DATA(LFB_NAME, val); LFB_ITER_INC(LFB_NAME); }

	// Finally, composite data from registers in this thread.
	#if MAX_REGISTERS > 16
		//LOAD_FRAG(16);LOAD_FRAG(17);LOAD_FRAG(18);LOAD_FRAG(19);LOAD_FRAG(20);LOAD_FRAG(21);LOAD_FRAG(22);LOAD_FRAG(23);
		//LOAD_FRAG(24);LOAD_FRAG(25);LOAD_FRAG(26);LOAD_FRAG(27);LOAD_FRAG(28);LOAD_FRAG(29);LOAD_FRAG(30);LOAD_FRAG(31);
	#endif
	#if MAX_REGISTERS > 8
		WRITE_FRAG(15);
		WRITE_FRAG(14);
		WRITE_FRAG(13);
		WRITE_FRAG(12);
		WRITE_FRAG(11);
		WRITE_FRAG(10);
		WRITE_FRAG(9);
		WRITE_FRAG(8);
	#endif
	#if MAX_REGISTERS > 4
		WRITE_FRAG(7);
		WRITE_FRAG(6);
		WRITE_FRAG(5);
		WRITE_FRAG(4);
	#endif
	#if MAX_REGISTERS > 2
		WRITE_FRAG(3);
		WRITE_FRAG(2);
	#endif
	#if MAX_REGISTERS > 1
		WRITE_FRAG(1);
	#endif
	WRITE_FRAG(0);

	#undef WRITE_FRAG
#endif

	return fragColor;
}



// Note: This won't be used to sort anything below 32 frags.
#if _MAX_FRAGS == 8

	#if 0
	void oddSort(int thread)
	{
		CSWAP(1,2);CSWAP(3,4);CSWAP(5,6);
	}

	void evenSort(int thread)
	{
		CSWAP(0,1);CSWAP(2,3);CSWAP(4,5);CSWAP(6,7);
	}
	#endif

#elif _MAX_FRAGS == 16

	#if 0
	void oddSort(int thread)
	{
		CSWAP(1,2);CSWAP(3,4);CSWAP(5,6);CSWAP(7,8);CSWAP(9,10);CSWAP(11,12);CSWAP(13,14);
	}

	void evenSort(int thread)
	{
		CSWAP(0,1);CSWAP(2,3);CSWAP(4,5);CSWAP(6,7);CSWAP(8,9);CSWAP(10,11);CSWAP(12,13);CSWAP(14,15);
	}
	#endif

#elif _MAX_FRAGS == 32

	// 1 register per-thread.
	// TODO: Implement me!

	void oddSort(int thread)
	{
		//CSWAP(1,2);CSWAP(3,4);CSWAP(5,6);CSWAP(7,8);CSWAP(9,10);CSWAP(11,12);CSWAP(13,14);CSWAP(15,16);CSWAP(17,18);
		//CSWAP(19,20);CSWAP(21,22);CSWAP(23,24);CSWAP(25,26);CSWAP(27,28);CSWAP(29,30);
	}

	void evenSort(int thread)
	{
		//CSWAP(0,1);CSWAP(2,3);CSWAP(4,5);CSWAP(6,7);CSWAP(8,9);CSWAP(10,11);CSWAP(12,13);CSWAP(14,15);CSWAP(16,17);
		//CSWAP(18,19);CSWAP(20,21);CSWAP(22,23);CSWAP(24,25);CSWAP(26,27);CSWAP(28,29);CSWAP(30,31);
	}

#elif _MAX_FRAGS == 64

	// 2 registers per-thread.

	void oddSort(int thread)
	{
		// Read value from previous thread, and next thread. Swap if necessary.
		swapVals[0] = shuffleNV(vals[1], thread - 1, N_THREADS);
		swapVals[1] = shuffleNV(vals[0], thread + 1, N_THREADS);

		if (thread > 0)
		{
			if (LFB_FRAG_DEPTH(swapVals[0]) > LFB_FRAG_DEPTH(vals[0]))
				vals[0] = swapVals[0];
		}
		if (thread < N_THREADS - 1)
		{
			if (LFB_FRAG_DEPTH(swapVals[1]) < LFB_FRAG_DEPTH(vals[1]))
				vals[1] = swapVals[1];
		}
	}

	void evenSort(int thread)
	{
		CSWAP(0,1);
	}

#elif _MAX_FRAGS == 128

	// 4 registers per-thread.

	void oddSort(int thread)
	{
		CSWAP(1, 2);

		// Read value from previous thread, and next thread. Swap if necessary.
		swapVals[0] = shuffleNV(vals[3], thread - 1, N_THREADS);
		swapVals[1] = shuffleNV(vals[0], thread + 1, N_THREADS);

		if (thread > 0)
		{
			if (LFB_FRAG_DEPTH(swapVals[0]) > LFB_FRAG_DEPTH(vals[0]))
				vals[0] = swapVals[0];
		}
		if (thread < N_THREADS - 1)
		{
			if (LFB_FRAG_DEPTH(swapVals[1]) < LFB_FRAG_DEPTH(vals[3]))
				vals[3] = swapVals[1];
		}
	}

	void evenSort(int thread)
	{
		CSWAP(0,1);CSWAP(2,3);
	}

#elif _MAX_FRAGS == 256

	// 8 registers per-thread.

	void oddSort(int thread)
	{
		CSWAP(1,2);CSWAP(3,4);CSWAP(5,6);

		// Read value from previous thread, and next thread. Swap if necessary.
		swapVals[0] = shuffleNV(vals[7], thread - 1, N_THREADS);
		swapVals[1] = shuffleNV(vals[0], thread + 1, N_THREADS);

		if (thread > 0)
		{
			if (LFB_FRAG_DEPTH(swapVals[0]) > LFB_FRAG_DEPTH(vals[0]))
				vals[0] = swapVals[0];
		}
		if (thread < N_THREADS - 1)
		{
			if (LFB_FRAG_DEPTH(swapVals[1]) < LFB_FRAG_DEPTH(vals[7]))
				vals[7] = swapVals[1];
		}
	}

	void evenSort(int thread)
	{
		CSWAP(0,1);CSWAP(2,3);CSWAP(4,5);CSWAP(6,7);
	}

#elif _MAX_FRAGS == 512

	// 16 registers per-thread.

	void oddSort(int thread)
	{
		CSWAP(1,2);CSWAP(3,4);CSWAP(5,6);CSWAP(7,8);CSWAP(9,10);CSWAP(11,12);CSWAP(13,14);

		// Read value from previous thread, and next thread. Swap if necessary.
		swapVals[0] = shuffleNV(vals[15], thread - 1, N_THREADS);
		swapVals[1] = shuffleNV(vals[0], thread + 1, N_THREADS);

		if (thread > 0)
		{
			if (LFB_FRAG_DEPTH(swapVals[0]) > LFB_FRAG_DEPTH(vals[0]))
				vals[0] = swapVals[0];
		}
		if (thread < N_THREADS - 1)
		{
			if (LFB_FRAG_DEPTH(swapVals[1]) < LFB_FRAG_DEPTH(vals[15]))
				vals[15] = swapVals[1];
		}
	}

	void evenSort(int thread)
	{
		CSWAP(0,1);CSWAP(2,3);CSWAP(4,5);CSWAP(6,7);CSWAP(8,9);CSWAP(10,11);CSWAP(12,13);CSWAP(14,15);
	}

#elif _MAX_FRAGS == 1024

	// 32 registers per-thread.

	void oddSort(int thread)
	{
		CSWAP(1,2);CSWAP(3,4);CSWAP(5,6);CSWAP(7,8);CSWAP(9,10);CSWAP(11,12);CSWAP(13,14);CSWAP(15,16);
		CSWAP(17,18);CSWAP(19,20);CSWAP(21,22);CSWAP(23,24);CSWAP(25,26);CSWAP(27,28);CSWAP(29,30);

		// Read value from previous thread, and next thread. Swap if necessary.
		swapVals[0] = shuffleNV(vals[31], thread - 1, N_THREADS);
		swapVals[1] = shuffleNV(vals[0], thread + 1, N_THREADS);

		if (thread > 0)
		{
			if (LFB_FRAG_DEPTH(swapVals[0]) > LFB_FRAG_DEPTH(vals[0]))
				vals[0] = swapVals[0];
		}
		if (thread < N_THREADS - 1)
		{
			if (LFB_FRAG_DEPTH(swapVals[1]) < LFB_FRAG_DEPTH(vals[31]))
				vals[31] = swapVals[1];
		}
	}

	void evenSort(int thread)
	{
		CSWAP(0,1);CSWAP(2,3);CSWAP(4,5);CSWAP(6,7);CSWAP(8,9);CSWAP(10,11);CSWAP(12,13);CSWAP(14,15);
		CSWAP(16,17);CSWAP(18,19);CSWAP(20,21);CSWAP(22,23);CSWAP(24,25);CSWAP(26,27);CSWAP(28,29);CSWAP(30,31);
	}

#endif



void sortRegisters(int thread)
{
	for (int i = 0; i < _MAX_FRAGS / 2; i++)
	{
		oddSort(thread);
		evenSort(thread);
	}
}



void main()
{
	int listIdx = int(gl_GlobalInvocationID.x) / N_THREADS;
	int thread = int(gl_LocalInvocationIndex);

	int pixel = int(intervalPixels[listIdx]);

	// Read all initial values to registers.
	int offset = thread * MAX_REGISTERS;
	loadRegisters(pixel, offset, MAX_REGISTERS);

	sortRegisters(thread);

#if SAVE_DEEP_DATA
	writeRegisters(pixel, offset, MAX_REGISTERS);
	//writeRegisters(offset, MAX_REGISTERS);
#else
	// All threads need to execute the shuffle, but only thread 0 will composite.
	vec4 fragColor = compositeRegisters(thread, pixel, offset, MAX_REGISTERS);
	if (thread == 0)
		pixelData[pixel] = rgba8ToFloat(fragColor);
#endif
}


