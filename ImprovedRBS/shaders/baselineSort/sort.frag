#version 430

#pragma optionNV(inline none)

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;


#define LFB_FRAG_TYPE vec2
#define USE_FLOAT 1

#define USE_ID 0


#define SORT_INSERT 0
#define SORT_MERGE 1
#define OPAQUE 0


#define SAVE_DEEP_DATA 0


#define MAX_FRAGS 128
#define _MAX_FRAGS 128

LFB_FRAG_TYPE frags[MAX_FRAGS];

#if SORT_MERGE
	LFB_FRAG_TYPE merge[MAX_FRAGS];
#endif


#define INDEX_BY_TILE 0

out vec4 fragColor;



#define COMPOSITE 0
#define COMPOSITE_LOCAL 0


#import "lfb"

#include "../utils.glsl"
#include "../lfb/tiles.glsl"


// To use different deep images, call replace(LFB_NAME, ...).
// uvec2 is more suitable when sorting indices.
#if !OPAQUE
	LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);
#endif





#define LFB_GET_FRAG(LFB_NAME) LFB_GET_DATA(LFB_NAME)
#define LFB_PUT_FRAG(LFB_NAME, frag) LFB_WRITE_DATA(LFB_NAME, frag)


#if USE_FLOAT
	#define GET_COLOR(val) floatToRGBA8(val)
#else
	#define GET_COLOR(val) uintToRGBA8(val)
#endif


#if !USE_ID
	#define LFB_FRAG_DEPTH(frag) frag.y
#else
	#define LFB_FRAG_DEPTH(depth) (depth & 0xFFFFFFU)
	#define LFB_FRAG_ID(depth) (depth >> 24U)
	#define LFB_FRAG_ID_GET(depth, id) ((id << 24U) | depth)
#endif



int readFrags(int pixel)
{
	int fragCount = 0;

	for (LFB_ITER_BEGIN(LFB_NAME, pixel); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		if (fragCount >= _MAX_FRAGS)
			break;
		else
		{
			frags[fragCount++] = LFB_GET_FRAG(LFB_NAME);
		}
	}

	return fragCount;
}



#if SORT_INSERT

void sortInsert(int pixel, int count)
{
	LFB_FRAG_TYPE tmp;

	//for i in range(min_pos + 1, min_pos + size):
	//	f = frags[i]
	//	j = i - 1
	//	while j >= min_pos and frags[j] > f:
	//		frags[j + 1] = frags[j]
	//		j -= 1
	//	frags[j + 1] = f
	for (int i = 1; i < count; i++)
	{
		tmp = frags[i];
		int j = i - 1;
		while (j >= 0 && LFB_FRAG_DEPTH(frags[j]) > LFB_FRAG_DEPTH(tmp))
		{
			frags[j + 1] = frags[j];
			j--;
		}
		frags[j + 1] = tmp;
	}

#if SAVE_DEEP_DATA
	LFB_ITER_BEGIN(LFB_NAME, pixel);
	for (int i = count - 1; i >= 0; i--)
	{
		LFB_PUT_FRAG(LFB_NAME, frags[i]);
		LFB_ITER_INC(LFB_NAME);
		//vec4 col = GET_COLOR(frags[i].x);
		//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#else
	// Composite.
	for (int i = count - 1; i >= 0; i--)
	{
		vec4 col = GET_COLOR(frags[i].x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif
}

#endif



#if SORT_MERGE

void sortMerge(int pixel, int count)
{
	int m = 0, i = 0, j = 0;
	int right = 0, left = 0, rend = 0;

	int k = 1;

	// Outer merges.
	while (k < count)
	{
		left = 0;
		// Inner merges.
		while (left + k < count)
		{
			right = left + k;
			rend = right + k;
			if (rend > count)
				rend = count;
			m = left;
			i = left;
			j = right;
			// Merging data from both lists.
			while (i < right && j < rend)
			{
				if (LFB_FRAG_DEPTH(frags[i]) <= LFB_FRAG_DEPTH(frags[j]))
					merge[m++] = frags[i++];
				else
					merge[m++] = frags[j++];
			}
			// Leftover values from the left side.
			while (i < right)
				merge[m++] = frags[i++];
			// Leftover values from the right size.
			while (j < rend)
				merge[m++] = frags[j++];
			// Copy the data back.
			for (int m = left; m < rend; m++)
				frags[m] = merge[m];
			left += k * 2;
		}
		k *= 2;
	}


#if SAVE_DEEP_DATA
	LFB_ITER_BEGIN(LFB_NAME, pixel);
	for (int i = count - 1; i >= 0; i--)
	{
		LFB_PUT_FRAG(LFB_NAME, frags[i]);
		LFB_ITER_INC(LFB_NAME);
		//vec4 col = GET_COLOR(frags[i].x);
		//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#else
	// Composite.
	for (int i = count - 1; i >= 0; i--)
	{
		vec4 col = GET_COLOR(frags[i].x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif
}

#endif




void main()
{
	fragColor = vec4(1.0);

#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

#if 0
	if (LFB_COUNT_AT(LFB_NAME, pixel) < 32)
	{
		return;
	}
#endif

	// Load into local memory and sort. This will also write to global memory if COMPOSITE_LOCAL is not defined.
	int fragCount = readFrags(pixel);
#if SORT_INSERT
	sortInsert(pixel, fragCount);
#endif
#if SORT_MERGE
	sortMerge(pixel, fragCount);
#endif


#if COMPOSITE
	// Read from global memory and composite.
	for (LFB_ITER_BEGIN(LFB_NAME, pixel); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		LFB_FRAG_TYPE f = LFB_GET_FRAG(LFB_NAME);
		vec4 col = uintToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif

#if COMPOSITE_LOCAL && 0
	#if _MAX_FRAGS <= LFB_BLOCK_SIZE // Dirty hack to account for rbs not sorting in place.
	for (int i = fragCount - 1; i >= 0; i--)
	{
		LFB_FRAG_TYPE f = frags[i];
		vec4 col = uintToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
	#endif
#endif
#if (COMPOSITE || COMPOSITE_LOCAL) && 0
	float dc = MAX_FRAGS_OVERRIDE / float(MAX_FRAGS);
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
	fragColor.a = 1.0;
#endif
}

