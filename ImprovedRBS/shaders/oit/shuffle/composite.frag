#version 430

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

#define INDEX_BY_TILE 0

out vec4 fragColor;

#import "lfb"

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

// Just standard forward rendering.
LFB_DEC(lfb, vec2);

void main()
{
	fragColor = vec4(1.0);

	// Composite geometry.
#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(lfb), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	// Read from global memory and composite.
	for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		vec2 f = LFB_GET_DATA(lfb);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
}

