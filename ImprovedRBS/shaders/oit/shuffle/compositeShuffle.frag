#version 430

// Needed to cull before writing to global memory.
//layout(early_fragment_tests) in;

#define INDEX_BY_TILE 0

buffer PixelData
{
	float pixelData[];
};

out vec4 fragColor;

uniform ivec2 size;

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

void main()
{
	fragColor = vec4(0.0);

	// Composite geometry.
#if INDEX_BY_TILE
	int pixel = tilesIndex(size, ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	float frag = pixelData[pixel];
	vec4 col = floatToRGBA8(frag);
	if (col.x == 0 && col.y == 0 && col.z == 0)
		discard;
	else
		fragColor = floatToRGBA8(frag);
}

