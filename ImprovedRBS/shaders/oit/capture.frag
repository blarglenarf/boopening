#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define INDEX_BY_TILE 0

#import "lfb"

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#define OPAQUE 0
#define LFB_NAME lfb

// uvec2 is more suitable when sorting indices.
#define LFB_FRAG_TYPE vec2
#define USE_FLOAT 1

#if !OPAQUE
	LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform bool normFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;


#include "../light/light.glsl"

#if OPAQUE
	out vec4 fragColor;
#endif


void main()
{
	vec4 color = vec4(0.0);

	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	color = getAmbient(0.3);
#if OPAQUE
	if (color.w == 0)
	{
		discard;
		return;
	}
	color.w = 1.0;
#endif

	color += directionLight(vec4(1), vec3(0, 0, 1), viewDir, normal);
	//color = toneMapColor(color, 1.0, 1.0);

#if !OPAQUE
	#if INDEX_BY_TILE
		int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
	#else
		int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	#endif
	#if USE_FLOAT
		LFB_ADD_DATA(LFB_NAME, pixel, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
	#else
		float depth = -VertexIn.esFrag.z; // val between 0 and 30 (viewport z range).
		float newDepth = (depth * 16777215.0) / 30.0; // val between 0 and 16,777,215 (24-bit max uint).
		LFB_ADD_DATA(LFB_NAME, pixel, uvec2(rgba8ToUint(color), uint(newDepth)));
	#endif
	discard;
#else
	fragColor = color;
#endif
}

