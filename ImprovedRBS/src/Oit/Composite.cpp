#include "Composite.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

#define COMPOSITE_LOCAL 1
#define INDEX_BY_TILE 1

#define USE_BMA 0

using namespace Rendering;


void Composite::render(App *app)
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	app->captureGeometry();

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Read the result back from the lfb and composite.
	// Should this be BMA'd or not? DEFINITELY NOT!!!
	app->getProfiler().start("Geometry Sort");
#if !USE_BMA
	compositeShader.bind();
	app->getGeometryLfb().composite(&compositeShader);
	compositeShader.unbind();
#else
	compositeBMA.createMask(&app->getGeometryLfb());
	compositeBMA.sort(&app->getGeometryLfb());
#endif
	app->getProfiler().time("Geometry Sort");

	glPopAttrib();
}

void Composite::update(float dt)
{
	(void) (dt);
}

void Composite::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

#if !USE_BMA
	auto vertComp = ShaderSourceCache::getShader("compBruteVert").loadFromFile("shaders/oit/standard/composite.vert");
	auto fragComp = ShaderSourceCache::getShader("compBruteFrag").loadFromFile("shaders/oit/standard/composite.frag");
	fragComp.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));

	compositeShader.release();
	compositeShader.create(&vertComp, &fragComp);
#else
	compositeBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
	compositeBMA.createShaders(&app->getGeometryLfb(), "shaders/oit/standard/composite.vert", "shaders/oit/standard/composite.frag", {{"COMPOSITE_LOCAL", "1"},
		{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}, {"USE_FLOAT", "1"}, {"LFB_FRAG_TYPE", "vec2"}});
	compositeBMA.setProfiler(&app->getProfiler());
#endif
}

void Composite::saveLFB(App *app)
{
	app->saveGeometryLFB();
}

