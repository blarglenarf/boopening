#include "Standard.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

#define COMPOSITE_LOCAL 1
#define INDEX_BY_TILE 1

#define CONSERVATIVE 0

// Do we want to save the full deep image data, or just pixel data?
#define SAVE_DEEP_DATA 0

using namespace Rendering;


Math::vec4 getEyeFromWindow(Math::vec3 p, Math::vec4 viewport, Math::mat4 invPMatrix)
{
	Math::vec3 ndcPos;
	ndcPos.xy = ((p.xy - viewport.xy) / viewport.zw) * 2.0f - 1.0f;
	//ndcPos.xy = ((p.xy * 2.0f) - (viewport.xy * 2.0f)) / (viewport.zw) - 1.0f;
	ndcPos.z = 1.0f;
	//std::cout << "ndc: " << ndcPos << "\n";

	Math::vec4 eyeDir = invPMatrix * Math::vec4(ndcPos, 1.0f);
	eyeDir /= eyeDir.w;
	Math::vec4 eyePos = Math::vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0f);
	return eyePos;
}


void Standard::render(App *app)
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
#if 1
	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
#endif

#if CONSERVATIVE
	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif
	if (!visualFlag)
		app->captureGeometry();
#if CONSERVATIVE
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#if COMPOSITE_LOCAL
	app->sortGeometry();
#else
	// Read the result back from the lfb and composite.
	if (!visualFlag)
	{
		app->sortGeometry(false);
		app->getProfiler().start("Composite");
		compositeShader.bind();
		app->getGeometryLfb().composite(&compositeShader);
		compositeShader.unbind();
		app->getProfiler().time("Composite");
	}

	// Testing the lfb visualiser, if we're using a linearized lfb.
	else
	{
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_DEPTH_TEST);
		app->getProfiler().start("Visualise");
		app->getGeometryLfb().drawData(origInvMvMatrix, origInvPMatrix, origViewport, Renderer::instance().getActiveCamera().getInverse());
		app->getProfiler().time("Visualise");
		glPopAttrib();

		// TODO: Read data from a particular fragment, make sure the data we're calculating in glsl is the same.
	#if 0
		auto &camera = Renderer::instance().getActiveCamera();
		auto view = camera.getViewport();
		Math::vec4 osPos(1, 1, 1, 1);
		Math::vec4 esPos = camera.getInverse() * osPos;
		Math::vec4 csPos = camera.getProjection() * esPos;
		float invW = 1.0 / csPos.w;
		Math::vec3 ndc(csPos.x * invW, csPos.y * invW, csPos.z * invW);
		Math::vec2 ssPos((ndc.x * 0.5 + 0.5) * camera.getWidth(), (ndc.y * 0.5 + 0.5) * camera.getHeight());

		std::cout << "osPos: " << osPos << "\n";
		std::cout << "esPos: " << esPos << "\n";
		std::cout << "csPos: " << csPos << "\n";
		std::cout << "ssPos: " << ssPos << "\n";

		Math::vec4 eye = getEyeFromWindow(Math::vec3(ssPos.x, ssPos.y, esPos.z), Math::vec4(view.x, view.y, view.z, view.w), camera.getInverseProj());
		std::cout << "eye: " << eye << "\n";
		Math::vec4 os = camera.getTransform() * eye;
		std::cout << "os: " << os << "\n";
	#endif
	}

	//vec4 eye = getEyeFromWindow(vec3(ssPos, -30), origViewport, origInvPMatrix);
	//vec3 dir = normalize(eye.xyz);
	//vec4 esPos = vec4(dir * depth, 1.0);
	//esPos.z = depth; // TODO: This may already be the right value, not sure (ideally it should be).
#endif

	glPopAttrib();
}

void Standard::update(float dt)
{
	(void) (dt);
}

void Standard::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	reloadShaders(app);
}

void Standard::reloadShaders(App* app)
{
	(void) (app);

	auto vertComp = ShaderSourceCache::getShader("compBruteVert").loadFromFile("shaders/oit/standard/composite.vert");
	auto fragComp = ShaderSourceCache::getShader("compBruteFrag").loadFromFile("shaders/oit/standard/composite.frag");
	fragComp.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));

	compositeShader.release();
	compositeShader.create(&vertComp, &fragComp);
}


void Standard::saveLFB(App *app)
{
	app->saveGeometryLFB();
}

void Standard::visualise(App *app)
{
	(void) (app);

	if (visualFlag)
	{
		// TODO: Stuff.
	}
	else
	{
	#if !COMPOSITE_LOCAL
		visualFlag = true;

		auto &camera = Renderer::instance().getActiveCamera();
		origInvMvMatrix = camera.getTransform();
		origInvPMatrix = camera.getInverseProj();
		origViewport = camera.getViewport();

		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		app->captureGeometry();
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		app->sortGeometry(false);
		glPopAttrib();
	#endif
	}
}

