#pragma once

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"

class Shuffle : public Oit
{
private:
	unsigned int nFrags;

	Rendering::Shader captureShader;
	Rendering::Shader compositeShader;

	Rendering::Shader testOddEvenShader;
	Rendering::Shader testBitShader;
	Rendering::Shader testRbsShader;

	Rendering::Shader testOddEvenShuffleShader;
	Rendering::Shader testBitShuffleShader;
	Rendering::Shader testRbsShuffleShader;

	Rendering::Shader testBitSharedShader;

	std::vector<Math::uvec2> testDataUnsorted;
	std::vector<Math::uvec2> testDataSorted;
	std::vector<unsigned int> fragCounts;
	std::vector<unsigned int> offsets;

	Rendering::StorageBuffer testOddEvenBuffer;
	Rendering::StorageBuffer testBitonicBuffer;
	Rendering::StorageBuffer testRbsBuffer;

	//Rendering::StorageBuffer fragCountBuffer;
	Rendering::StorageBuffer offsetBuffer;

	Rendering::BMA shuffleBMA;

	Rendering::Shader compositeShuffleShader;

	Rendering::StorageBuffer pixelData;

	Rendering::StorageBuffer colorData;

private:
	void testOddEven(App *app);
	void testBitonic(App *app);
	void testRbs(App *app);
	void testShuffleOddEven(App *app);
	void testShuffleBitonic(App *app);
	void testShuffleRbs(App *app);
	void testSharedBitonic(App *app);

	void testIsSorted(Rendering::StorageBuffer &buffer);

	void drawShufflePixels(App *app);
	void sort(App *app);

	void captureGeometry(App *app);

public:
	Shuffle(int opaqueFlag = 0) : Oit(opaqueFlag), nFrags(0) {}
	virtual ~Shuffle() {}

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;
	virtual void reloadShaders(App *app) override;

	virtual void saveLFB(App *app) override;
};

