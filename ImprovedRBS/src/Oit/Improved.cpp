#include "Improved.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/Random.h"
#include "../../../Utils/src/File.h"

#include "../../../Renderer/src/LFB/LFBLinear.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <string>

// TODO: Easier way to directly compare shuffle, odd-even and rbs both for saving and not saving data.

#define COMPOSITE_LOCAL 1
#define INDEX_BY_TILE 1 // TODO: Don't define this both here and in App.cpp :(
#define DATA_SIZE 512
#define N_LISTS (512 * 512)
#define RANDOM_SIZES 0 // Currently only implemented for odd-even warp sort.

#define USE_ID 0

#define CONSERVATIVE 0

// Note: RBS/Oddeven doesn't do indirect sorting.
#define TEST_ODDEVEN 0
#define TEST_RBS 0
#define TEST_BIT 0
#define TEST_SHUFFLE_ODDEVEN 0
#define TEST_SHUFFLE_RBS 0 // Blocked sorting.
#define TEST_SHUFFLE_BIT 0
#define TEST_SHARED_BIT 0

// Do we want to save the full deep image data, or just pixel data?
#define SAVE_DEEP_DATA 0 // TODO: Test in shuffleOddEven and oddEven.

#define CHECK_IS_SORTED 0
#define PRINT_SORTED 0

#define RBS_SHUFFLE 0
#define BIT_SHUFFLE 0
#define ODD_SHUFFLE 1

#define SHUFFLE_MIN 10


using namespace Rendering;

void Improved::testOddEven(App *app)
{
	testOddEvenBuffer.create(&testDataUnsorted[0], sizeof(testDataUnsorted[0]) * testDataUnsorted.size());
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("oddeven");

	testOddEvenShader.bind();
	testOddEvenShader.setUniform("TestData", &testOddEvenBuffer);
	testOddEvenShader.setUniform("PixelData", &pixelData);
	testOddEvenShader.setUniform("fragCount", (int) testDataUnsorted.size());

	glDispatchCompute(N_LISTS / 32, 1, 1);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	testOddEvenShader.unbind();

	app->getProfiler().time("oddeven");

	std::cout << "odd even sort time: " << app->getProfiler().getTime("oddeven") << "ms\n";

#if CHECK_IS_SORTED
	testIsSorted(testOddEvenBuffer);
#endif
}

void Improved::testBitonic(App *app)
{
	testBitonicBuffer.create(&testDataUnsorted[0], sizeof(testDataUnsorted[0]) * testDataUnsorted.size());
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("bitonic");

	testBitShader.bind();
	testBitShader.setUniform("TestData", &testBitonicBuffer);
	testBitShader.setUniform("PixelData", &pixelData);
	testBitShader.setUniform("fragCount", (int) testDataUnsorted.size());

	glDispatchCompute(N_LISTS / 32, 1, 1);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	testBitShader.unbind();

	app->getProfiler().time("bitonic");

	std::cout << "bitonic sort time: " << app->getProfiler().getTime("bitonic") << "ms\n";

#if CHECK_IS_SORTED
	testIsSorted(testBitonicBuffer);
#endif
}

void Improved::testRbs(App *app)
{
	testRbsBuffer.create(&testDataUnsorted[0], sizeof(testDataUnsorted[0]) * testDataUnsorted.size());
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("rbsSort");

	testRbsShader.bind();
	testRbsShader.setUniform("TestData", &testRbsBuffer);
	testRbsShader.setUniform("PixelData", &pixelData);
	testRbsShader.setUniform("fragCount", (int) testDataUnsorted.size());

	glDispatchCompute(N_LISTS / 32, 1, 1);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	testRbsShader.unbind();

	app->getProfiler().time("rbsSort");

	std::cout << "rbs sort time: " << app->getProfiler().getTime("rbsSort") << "ms\n";

#if CHECK_IS_SORTED
	testIsSorted(testRbsBuffer);
#endif
}

void Improved::testShuffleOddEven(App *app)
{
	testOddEvenBuffer.create(&testDataUnsorted[0], sizeof(testDataUnsorted[0]) * testDataUnsorted.size());
#if RANDOM_SIZES
	//fragCountBuffer.create(&fragCounts[0], sizeof(fragCounts[0]) * fragCounts.size());
	offsetBuffer.create(&offsets[0], sizeof(offsets[0]) * offsets.size());
#endif
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("oddEvenShuffle");

	testOddEvenShuffleShader.bind();
	testOddEvenShuffleShader.setUniform("TestData", &testOddEvenBuffer);
#if RANDOM_SIZES
	testOddEvenShuffleShader.setUniform("Offsets", &offsetBuffer);
#endif
	testOddEvenShuffleShader.setUniform("PixelData", &pixelData);
	//testOddEvenShuffleShader.setUniform("fragCount", (int) testDataUnsorted.size());

	glDispatchCompute(N_LISTS, 1, 1);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	testOddEvenShuffleShader.unbind();

	app->getProfiler().time("oddEvenShuffle");

	std::cout << "odd-even shuffle sort time: " << app->getProfiler().getTime("oddEvenShuffle") << "ms\n";

#if CHECK_IS_SORTED
	testIsSorted(testOddEvenBuffer);
#endif
}

void Improved::testShuffleBitonic(App *app)
{
	testBitonicBuffer.create(&testDataUnsorted[0], sizeof(testDataUnsorted[0]) * testDataUnsorted.size());
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("bitonicShuffle");

	testBitShuffleShader.bind();
	testBitShuffleShader.setUniform("TestData", &testBitonicBuffer);
	testBitShuffleShader.setUniform("PixelData", &pixelData);
	testBitShuffleShader.setUniform("fragCount", (int) testDataUnsorted.size());

	glDispatchCompute(N_LISTS, 1, 1);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	testBitShuffleShader.unbind();

	app->getProfiler().time("bitonicShuffle");

	std::cout << "bitonic shuffle sort time: " << app->getProfiler().getTime("bitonicShuffle") << "ms\n";

#if CHECK_IS_SORTED
	testIsSorted(testBitonicBuffer);
#endif
}

void Improved::testShuffleRbs(App *app)
{
	testRbsBuffer.create(&testDataUnsorted[0], sizeof(testDataUnsorted[0]) * testDataUnsorted.size());
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("rbsShuffle");

	testRbsShuffleShader.bind();
	testRbsShuffleShader.setUniform("TestData", &testRbsBuffer);
	testRbsShuffleShader.setUniform("PixelData", &pixelData);
	testRbsShuffleShader.setUniform("fragCount", (int) testDataUnsorted.size());

	glDispatchCompute(N_LISTS, 1, 1);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	testRbsShuffleShader.unbind();

	app->getProfiler().time("rbsShuffle");

	std::cout << "blocked shuffle sort time: " << app->getProfiler().getTime("rbsShuffle") << "ms\n";

#if CHECK_IS_SORTED
	testIsSorted(testRbsBuffer);
#endif
}

void Improved::testSharedBitonic(App *app)
{
	testBitonicBuffer.create(&testDataUnsorted[0], sizeof(testDataUnsorted[0]) * testDataUnsorted.size());
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("bitonicShared");

	testBitSharedShader.bind();
	testBitSharedShader.setUniform("TestData", &testBitonicBuffer);
	testBitSharedShader.setUniform("PixelData", &pixelData);
	testBitSharedShader.setUniform("fragCount", (int) testDataUnsorted.size());

	glDispatchCompute(N_LISTS, 1, 1);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	testBitSharedShader.unbind();

	app->getProfiler().time("bitonicShared");

	std::cout << "bitonic shared sort time: " << app->getProfiler().getTime("bitonicShared") << "ms\n";

#if CHECK_IS_SORTED
	testIsSorted(testBitonicBuffer);
#endif
}

void Improved::testIsSorted(Rendering::StorageBuffer &buffer)
{
#if PRINT_SORTED
	std::cout << "given:    ";
	for (auto i : testDataUnsorted)
		std::cout << i << " ";
	std::cout << "\n";

	std::cout << "expected: ";
	for (auto i : testDataSorted)
		std::cout << i << " ";
	std::cout << "\n";
#endif
	Math::uvec2 *result = (Math::uvec2*) buffer.read();

#if PRINT_SORTED
	std::cout << "result:   ";
#endif
	bool sorted = true;
	for (int i = 0; i < (int) testDataSorted.size(); i++)
	{
	#if PRINT_SORTED
		std::cout << result[i] << " ";
	#endif
		if (testDataSorted[i] != result[i])
		{
			sorted = false;
		#if !PRINT_SORTED
			break;
		#endif
		}
	}
#if PRINT_SORTED
	std::cout << "\n";
#endif

	if (!sorted)
		std::cout << "Failed to sort!\n";
	else
		std::cout << "Sorted!\n";

	exit(0);
}


void Improved::drawShufflePixels(App *app)
{
	(void) (app);
	auto &camera = Renderer::instance().getActiveCamera();

	compositeShuffleShader.bind();
	compositeShuffleShader.setUniform("PixelData", &pixelData);
	compositeShuffleShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
	Renderer::instance().drawQuad();
	compositeShuffleShader.unbind();
}

void Improved::sort(App *app)
{
	static Shader zeroShader;
	if (!zeroShader.isGenerated())
	{
		auto zeroSrc = ShaderSourceCache::getShader("zero").loadFromFile("../Data/shaders/zero.vert");
		zeroSrc.replace("DATA_TYPE", "float");
		zeroShader.create(&zeroSrc);
	}

	auto &camera = Renderer::instance().getActiveCamera();
	pixelData.create(nullptr, camera.getWidth() * camera.getHeight() * sizeof(float)); // FIXME: Does this need to be zeroed?

#if 1
	// In case you want to zero the pixel data.
	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("Data", &pixelData);
	glDrawArrays(GL_POINTS, 0, camera.getWidth() * camera.getHeight());
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
#endif

	app->getProfiler().start("Geometry Sort");

	auto &intervals = shuffleBMA.getIntervals();
	auto &intervalCounts = shuffleBMA.getIntervalCounts();
	auto *lfb = &app->getGeometryLfb();

#if 1
	glDisable(GL_STENCIL_TEST);

	// Sort larger intervals using shuffling.
	for (int i = (int) intervals.size() - 1; i >= SHUFFLE_MIN; i--)
	{
		if (intervalCounts[i].count <= 0)
			continue;
		app->getProfiler().start("bmaInterval" + Utils::toString(i));

		intervals[i].shader->bind();
		lfb->setBmaUniforms(intervals[i].shader);
		intervals[i].shader->setUniform("IntervalPixels", intervalCounts[i].pixels);
		intervals[i].shader->setUniform("PixelData", &pixelData);
	#if RBS_SHUFFLE || BIT_SHUFFLE || ODD_SHUFFLE
		glDispatchCompute((GLuint) intervalCounts[i].count, 1, 1);
		intervals[i].shader->unbind();
	#endif
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		//glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
		//drawShufflePixels(app); // TODO: Could just draw these all at once maybe?
		//glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
		app->getProfiler().time("bmaInterval" + Utils::toString(i));
	}
	#if COMPOSITE_LOCAL
		drawShufflePixels(app);
	#endif
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
#endif

#if 1
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	// Sort smaller intervals in the normal way.
	int count = std::min(SHUFFLE_MIN - 1, 6);
	if ((int) intervals.size() > count)
	{
		for (int i = count; i >= 0; i--)
		{
			app->getProfiler().start("bmaInterval" + Utils::toString(i));
			glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
			intervals[i].shader->bind();
			lfb->setBmaUniforms(intervals[i].shader);
			Rendering::Renderer::instance().drawQuad();
			intervals[i].shader->unbind();
			app->getProfiler().time("bmaInterval" + Utils::toString(i));
		}
	}
#endif
	glDisable(GL_STENCIL_TEST);

	app->getProfiler().time("Geometry Sort");
}


void Improved::render(App *app)
{
	// For a more synthetic test.
#if TEST_ODDEVEN
	testOddEven(app);
#endif
#if TEST_BIT
	testBitonic(app);
#endif
#if TEST_RBS
	testRbs(app);
#endif
#if TEST_SHUFFLE_ODDEVEN
	testShuffleOddEven(app);
#endif
#if TEST_SHUFFLE_BIT
	testShuffleBitonic(app);
#endif
#if TEST_SHUFFLE_RBS
	testShuffleRbs(app);
#endif
#if TEST_SHARED_BIT
	testSharedBitonic(app);
#endif

#if TEST_ODDEVEN || TEST_BIT || TEST_RBS || TEST_SHUFFLE_ODDEVEN || TEST_SHUFFLE_BIT || TEST_SHUFFLE_RBS || TEST_SHARED_BIT
	return;
#endif

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
#if 1
	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
#endif

#if CONSERVATIVE
	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif
	app->captureGeometry(&captureShader);
#if CONSERVATIVE
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// Before sorting, render fullscreen quad and count how many pixels are in each interval based on fragment counts.
	shuffleBMA.countIntervals(&app->getGeometryLfb());

	// Write pixel id's to each interval's buffer during the bma mask pass (or possibly during its own separate pass).
	shuffleBMA.createMask(&app->getGeometryLfb());

	// Now sort using bma+rbs for lower intervals, and bma+shuffle for higher intervals.
	sort(app);

#if !COMPOSITE_LOCAL
	app->getProfiler().start("Composite");
	compositeShader.bind();
	app->getGeometryLfb().composite(&compositeShader);
	compositeShader.unbind();
	app->getProfiler().time("Composite");
#endif

	glPopAttrib();
}

void Improved::update(float dt)
{
	(void) (dt);
}

void Improved::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	unsigned int totalData = DATA_SIZE * N_LISTS;

#if RANDOM_SIZES
	totalData = 0;
	fragCounts.resize(N_LISTS);
	offsets.resize(N_LISTS);
	for (int i = 0; i < (int) fragCounts.size(); i++)
	{
		fragCounts[i] = Utils::getRand(1U, (unsigned int) DATA_SIZE);
		totalData += fragCounts[i];
		offsets[i] = totalData;
	}
#endif

	// A fairer test is if all buffers have different data.
	testDataUnsorted.resize(totalData);
	for (int i = 0; i < (int) testDataUnsorted.size(); i++)
		testDataUnsorted[i] = Math::uvec2(0, Utils::getRand(0U, 1677215U));

#if CHECK_IS_SORTED
	testDataSorted = testDataUnsorted;
	size_t offset = 0;
	for (size_t i = 0; i < N_LISTS; i++)
	{
		auto start = testDataSorted.begin() + offset;
	#if RANDOM_SIZES
		auto end = testDataSorted.begin() + offset + fragCounts[i];
		offset += fragCounts[i];
	#else
		auto end = testDataSorted.begin() + offset + DATA_SIZE;
		offset += DATA_SIZE;
	#endif
		std::sort(start, end, [](const Math::uvec2 &a, const Math::uvec2 &b) { return a.y < b.y; });
	}
#endif
}

void Improved::reloadShaders(App *app)
{
	auto vertCap = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/oit/capture.vert");
	auto fragCap = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/oit/capture.frag");
	fragCap.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragCap.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragCap.setDefine("LFB_FRAG_TYPE", "vec2");
	fragCap.setDefine("USE_FLOAT", "1");

	captureShader.release();
	captureShader.create(&vertCap, &fragCap);

	auto vertComp = ShaderSourceCache::getShader("compBruteVert").loadFromFile("shaders/oit/shuffle/composite.vert");
	auto fragComp = ShaderSourceCache::getShader("compBruteFrag").loadFromFile("shaders/oit/shuffle/composite.frag");
	fragComp.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));

	compositeShader.release();
	compositeShader.create(&vertComp, &fragComp);

	auto vertShuff = ShaderSourceCache::getShader("compShuffVert").loadFromFile("shaders/oit/shuffle/compositeShuffle.vert");
	auto fragShuff = ShaderSourceCache::getShader("compShuffFrag").loadFromFile("shaders/oit/shuffle/compositeShuffle.frag");
	fragShuff.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));

	compositeShuffleShader.release();
	compositeShuffleShader.create(&vertShuff, &fragShuff);

	app->getGeometryLfb().setDataType(LFB::VEC2);

	shuffleBMA.createMaskShader("lfb", "shaders/oit/shuffle/bmaMask.vert", "shaders/oit/shuffle/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
	shuffleBMA.createShaders(&app->getGeometryLfb(), "shaders/basicSort/sort.vert", "shaders/basicSort/sort.frag",
		{{"COMPOSITE_LOCAL", Utils::toString(COMPOSITE_LOCAL)}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}, {"LFB_FRAG_TYPE", "vec2"}, {"USE_FLOAT", "1"}});

	// Just in case you want to use indirect sorting.
	//for (int i = 4; i < SHUFFLE_MIN; i++)
	//	shuffleBMA.setShader(i, &app->getGeometryLfb(), "shaders/basicSort/sort.vert", "shaders/basicSort/sort.frag",
	//		{{"COMPOSITE_LOCAL", "1"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}, {"LFB_FRAG_TYPE", "vec2"}, {"USE_FLOAT", "1"}, {"USE_ID", Utils::toString(USE_ID)}});

	shuffleBMA.createCounts(&app->getGeometryLfb(), "shaders/oit/shuffle/countIntervals.vert", "shaders/oit/shuffle/countIntervals.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});

#if RBS_SHUFFLE || BIT_SHUFFLE || ODD_SHUFFLE
	#if RBS_SHUFFLE
		std::string shaderName = "shaders/warpSort/shuffleRBS.comp";
	#elif BIT_SHUFFLE
		std::string shaderName = "shaders/warpSort/shuffleBit.comp";
	#elif ODD_SHUFFLE
		std::string shaderName = "shaders/warpSort/shuffleOddEven.comp";
	#endif
	for (int i = SHUFFLE_MIN; i <= 6; i++)
		shuffleBMA.setShader(i, &app->getGeometryLfb(), shaderName, {{"LFB_FRAG_TYPE", "vec2"}, {"USE_FLOAT", "1"}, {"SAVE_DEEP_DATA", Utils::toString(SAVE_DEEP_DATA)}});
#endif

	//Utils::File f;

#if TEST_ODDEVEN
	auto &testOddEvenComp = ShaderSourceCache::getShader("testOddEven").loadFromFile("shaders/test/oddEvenTest.comp");
	testOddEvenComp.setDefine("SIZE", Utils::toString(DATA_SIZE));
	testOddEvenComp.setDefine("CHECK_IS_SORTED", Utils::toString(CHECK_IS_SORTED));
	testOddEvenComp.setDefine("SAVE_DEEP_DATA", Utils::toString(SAVE_DEEP_DATA));
	testOddEvenShader.release();
	testOddEvenShader.createCompute(&testOddEvenComp);

	//f = Utils::File("oddEven.txt");
	//f.write(testOddEvenShader.getBinary());
#endif
#if TEST_RBS
	auto &testRbsComp = ShaderSourceCache::getShader("testRbs").loadFromFile("shaders/test/rbsTest.comp");
	testRbsComp.setDefine("SIZE", Utils::toString(DATA_SIZE));
	testRbsComp.setDefine("CHECK_IS_SORTED", Utils::toString(CHECK_IS_SORTED));
	testRbsComp.setDefine("SAVE_DEEP_DATA", Utils::toString(SAVE_DEEP_DATA));
	testRbsShader.release();
	testRbsShader.createCompute(&testRbsComp);

	//f = Utils::File("rbs.txt");
	//f.write(testRbsShader.getBinary());
#endif
#if TEST_BIT
	auto &testBitComp = ShaderSourceCache::getShader("testBit").loadFromFile("shaders/test/bitTest.comp");
	testBitComp.setDefine("SIZE", Utils::toString(DATA_SIZE));
	testBitComp.setDefine("CHECK_IS_SORTED", Utils::toString(CHECK_IS_SORTED));
	testBitComp.setDefine("USE_ID", Utils::toString(USE_ID));
	testBitShader.release();
	testBitShader.createCompute(&testBitComp);

	//f = Utils::File("bitonic.txt");
	//f.write(testBitShader.getBinary());
#endif
#if TEST_SHUFFLE_ODDEVEN
	auto &testShuffleOddEvenComp = ShaderSourceCache::getShader("testShuffleOddEven").loadFromFile("shaders/test/oddEvenShuffleTest.comp");
	testShuffleOddEvenComp.setDefine("SIZE", Utils::toString(DATA_SIZE));
	testShuffleOddEvenComp.setDefine("CHECK_IS_SORTED", Utils::toString(CHECK_IS_SORTED));
	testShuffleOddEvenComp.setDefine("SAVE_DEEP_DATA", Utils::toString(SAVE_DEEP_DATA));
	testShuffleOddEvenComp.setDefine("RANDOM_SIZES", Utils::toString(RANDOM_SIZES));
	testOddEvenShuffleShader.release();
	testOddEvenShuffleShader.createCompute(&testShuffleOddEvenComp);

	//f = Utils::File("warpOddEven.txt");
	//f.write(testOddEvenShuffleShader.getBinary());
#endif
#if TEST_SHUFFLE_RBS
	auto &testShuffleRbsComp = ShaderSourceCache::getShader("testShuffleRbs").loadFromFile("shaders/test/rbsShuffleTest.comp");
	testShuffleRbsComp.setDefine("SIZE", Utils::toString(DATA_SIZE));
	testShuffleRbsComp.setDefine("CHECK_IS_SORTED", Utils::toString(CHECK_IS_SORTED));
	testRbsShuffleShader.release();
	testRbsShuffleShader.createCompute(&testShuffleRbsComp);

	//f = Utils::File("warpRbs.txt");
	//f.write(testRbsShuffleShader.getBinary());
#endif
#if TEST_SHUFFLE_BIT
	auto &testShuffleBitComp = ShaderSourceCache::getShader("testShuffleBit").loadFromFile("shaders/test/bitShuffleTest.comp");
	testShuffleBitComp.setDefine("SIZE", Utils::toString(DATA_SIZE));
	testShuffleBitComp.setDefine("CHECK_IS_SORTED", Utils::toString(CHECK_IS_SORTED));
	testShuffleBitComp.setDefine("USE_ID", Utils::toString(USE_ID));
	testBitShuffleShader.release();
	testBitShuffleShader.createCompute(&testShuffleBitComp);

	//f = Utils::File("warpBitonic.txt");
	//f.write(testBitShuffleShader.getBinary());
#endif
#if TEST_SHARED_BIT
	auto &testSharedBitComp = ShaderSourceCache::getShader("testSharedBit").loadFromFile("shaders/test/bitSharedTest.comp");
	testSharedBitComp.setDefine("SIZE", Utils::toString(DATA_SIZE));
	testSharedBitComp.setDefine("CHECK_IS_SORTED", Utils::toString(CHECK_IS_SORTED));
	testSharedBitComp.setDefine("USE_ID", Utils::toString(USE_ID));
	testBitSharedShader.release();
	testBitSharedShader.createCompute(&testSharedBitComp);

	//f = Utils::File("sharedBitonic.txt");
	//f.write(testBitSharedShader.getBinary());
#endif
}

void Improved::saveLFB(App *app)
{
	app->saveGeometryLFB();
}

