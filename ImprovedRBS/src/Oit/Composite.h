#pragma once

#include "Oit.h"
#include "../../../Renderer/src/LFB/BMA.h"

class Composite : public Oit
{
private:
	Rendering::Shader compositeShader;

	Rendering::BMA compositeBMA;

public:
	Composite(int opaqueFlag = 0) : Oit(opaqueFlag) {}
	virtual ~Composite() {}

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

