#pragma once

#include "Oit.h"

class Standard : public Oit
{
private:
	Rendering::Shader compositeShader;

	bool visualFlag;

	Math::mat4 origInvMvMatrix;
	Math::mat4 origInvPMatrix;
	Math::ivec4 origViewport;

public:
	Standard(int opaqueFlag = 0) : Oit(opaqueFlag), visualFlag(false) {}
	virtual ~Standard() {}

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;
	virtual void reloadShaders(App* app) override;

	virtual void saveLFB(App *app) override;

	virtual void visualise(App *app) override;
};

