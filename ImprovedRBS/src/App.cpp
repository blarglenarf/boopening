#include "App.h"

#include "Oit/Oit.h"
#include "Oit/Composite.h"
#include "Oit/Baseline.h"
#include "Oit/Standard.h"
#include "Oit/Improved.h"
#include "Oit/Shuffle.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <iomanip>



using namespace Math;
using namespace Rendering;

// TODO: Get rid of this!
std::string currRbsStr;
std::string currLfbStr;
Flythrough anim;
Font testFont;
float tpf = 0;


// TODO: Keep it simple for now, add more frags later.
#define MAX_FRAGS 512
#define USE_BMA 1
#define INDEX_BY_TILE 1


void App::init()
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Powerplant full view.
	//camera.setZoom(8.48492f);
	//camera.setPos({7.27525f, -5.43613f, -0.992247f});
	//camera.setEulerRot({0.043634f, 1.56207f, 0.0f});

	// Powerplant deep view.
	//camera.setZoom(3.37033f);
	//camera.setPos({7.60963f, -4.83113f, -4.2693f});
	//camera.setEulerRot({-0.235619f, 2.00713f, 0.0f});

	// Hairball full view.
	//camera.setPos({-0.400156f,-0.300304f,-0.0183215f});
	//camera.setEulerRot({-0.157078f,0.0261799f,0});
	//camera.setZoom(15);

	// Hairball deep view.
	//camera.setPos({-0.5f, -0.8f, 0.0f});
	//camera.setEulerRot({-0.209439f, 0.0f, 0.0f});
	//camera.setZoom(6.68847f);

	// Front view for parallel planes.
	camera.setZoom(15);
	camera.setPos({0, 0, 0});
	camera.setEulerRot({0, 0, 0});

	// Atrium deep view.
	//camera.setZoom(6.7f);
	//camera.setPos({-1.74935f, -2.77251f, -0.747198f});
	//camera.setEulerRot({0.122174f, -0.0523599f, 0.0f});

	// Very deep power plant view.
	//camera.setPos({6.11006f, -3.99644f, -3.75149f});
	//camera.setEulerRot({0.087267f, 1.58825f, 0.0f});
	//camera.setZoom(3.14487f);

	// Very very deep power plant view.
	//camera.setPos({7.14135f, -4.37497f, -2.98195f});
	//camera.setEulerRot({0.113447f, 1.26536f, 0.0f});
	//camera.setZoom(1.80087f);

	auto &interpolator = anim.getInterpolator(anim.addInterpolator(const_cast<vec3*>(&camera.getEulerRot()), 10, vec3(-0.0523594,0.855212,0)));
	interpolator.addKeyFrame(0, vec3(-0.0523594,0.855212,0));
	interpolator.addKeyFrame(10, vec3(-0.061086,-0.715585,0));

#if 0
	anim.setAnimFilename("animations/anim.jpg");
#endif

	// TODO: Get rid of this!
	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");

	int maxLayers = 0;
	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxLayers);
	std::cout << "Max texture layers: " << maxLayers << "\n";

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glColor3f(1, 1, 1);

	//loadNextMesh();
	currMesh = useMesh(0);
	useRBS(Oit::COMPOSITE);
	//useLFB();

	auto vertBasic = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto geomBasic = ShaderSourceCache::getShader("basicGeom").loadFromFile("shaders/basic.geom");
	auto fragBasic = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.create(&vertBasic, &fragBasic, &geomBasic);

	//std::cout << basicShader.getBinary() << "\n";

	glClearColor(1.0, 1.0, 1.0, 1.0);
}

void App::setTmpViewport(int width, int height)
{
	auto &camera = Renderer::instance().getActiveCamera();

	tmpWidth = camera.getWidth();
	tmpHeight = camera.getHeight();

	camera.setViewport(0, 0, width, height);
	camera.uploadViewport();
	camera.update(0);
}

void App::restoreViewport()
{
	auto &camera = Renderer::instance().getActiveCamera();

	camera.setViewport(0, 0, tmpWidth, tmpHeight);
	camera.uploadViewport();
	camera.update(0);
}

void App::setCameraUniforms(Shader *shader)
{
	auto &camera = Renderer::instance().getActiveCamera();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	shader->setUniform("viewport", camera.getViewport());
	shader->setUniform("invPMatrix", camera.getInverseProj());
}

void App::captureGeometry(Shader *shader)
{
	if (!shader)
		shader = &captureGeometryShader;

	profiler.start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	if (opaqueFlag == 0)
		geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && geometryLFB.getType() != LFB::LINK_LIST)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags(INDEX_BY_TILE);
		countShader.bind();
		geometryLFB.setCountUniforms(&countShader);
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh.render(false);
		countShader.unbind();
		geometryLFB.endCountFrags();
	}

	if (opaqueFlag == 0)
		geometryLFB.beginCapture();

	shader->bind();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	if (opaqueFlag == 0)
		geometryLFB.setUniforms(shader);

	// Capture fragments into the lfb.
	gpuMesh.render();
	shader->unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && geometryLFB.endCapture())
	{
		geometryLFB.beginCapture();
		shader->bind();
		shader->setUniform("mvMatrix", camera.getInverse());
		shader->setUniform("pMatrix", camera.getProjection());
		shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		geometryLFB.setUniforms(shader);
		gpuMesh.render();
		shader->unbind();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	// TODO: Profile memory usage perhaps?

	profiler.time("Geometry Capture");
}

void App::sortGeometry(bool composite)
{
	if (opaqueFlag == 1)
		return;

	profiler.start("Geometry Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	if (composite)
	{
		geometryLocalBMA.createMask(&geometryLFB);
		geometryLocalBMA.sort(&geometryLFB);
	}
	else
	{
		geometryBMA.createMask(&geometryLFB);
		geometryBMA.sort(&geometryLFB);
	}
#else
	// Sort and composite normally.
	if (composite)
	{
		sortGeometryLocalShader.bind();
		geometryLFB.composite(&sortGeometryLocalShader); // TODO: change this to a sort method.
		sortGeometryLocalShader.unbind();
	}
	else
	{
		sortGeometryShader.bind();
		geometryLFB.composite(&sortGeometryShader); // TODO: change this to a sort method.
		sortGeometryShader.unbind();
	}
#endif

	profiler.time("Geometry Sort");
}

void App::render()
{
	static float fpsUpdate = 0;

#if 0
	testFont.blit();
	return;
#endif

	profiler.begin();

	profiler.start("Total");

	if (currOit)
	{
		currOit->render(this);
	}
	else
	{
		auto &camera = Renderer::instance().getActiveCamera();

		// Opaque Rendering.
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		basicShader.bind();
		basicShader.setUniform("mvMatrix", camera.getInverse());
		basicShader.setUniform("pMatrix", camera.getProjection());
		basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		basicShader.setUniform("lightDir", vec3(0, 0, 1));
		gpuMesh.render();
		basicShader.unbind();

		glPopAttrib();
	}

	profiler.time("Total");

	if (hudFlag)
	{
		// TODO: Easier smoothing maybe?
		float total = profiler.getTime("Total");
		float smoothing = 0.9f; // Larger => more smoothing.
		tpf = (tpf * smoothing) + (total * (1.0f - smoothing));
		float t = round(total);

		testFont.renderFromAtlas("Technique: " + currRbsStr +
			"\nLFB: " + currLfbStr +
			"\nTime per frame (ms): " + Utils::toString(t) +
			"\nCapture: " + Utils::toString(profiler.getTime("Geometry Capture")) +
			"\nSort: " + Utils::toString(profiler.getTime("Geometry Sort")) +
			"\nBMA0: " + Utils::toString(profiler.getTime("bmaInterval0")) +
			"\nBMA1: " + Utils::toString(profiler.getTime("bmaInterval1")) +
			"\nBMA2: " + Utils::toString(profiler.getTime("bmaInterval2")) +
			"\nBMA3: " + Utils::toString(profiler.getTime("bmaInterval3")) +
			"\nBMA4: " + Utils::toString(profiler.getTime("bmaInterval4")) +
			"\nBMA5: " + Utils::toString(profiler.getTime("bmaInterval5")) +
			"\nBMA6: " + Utils::toString(profiler.getTime("bmaInterval6")) +
			"\nFrames per second (fps): " + Utils::toString(1000.0 / t), 10, 10, 0, 0, 1);
	}

	float dt = Platform::Window::getDeltaTime();
	fpsUpdate += dt;
	if (fpsUpdate > 2)
	{
		profiler.print();
		fpsUpdate = 0;
	}

	if (hudFlag)
	{
		Renderer::instance().drawAxes();
		Renderer::instance().drawAxes(Renderer::instance().getActiveCamera().getPos(), 2);
	}
}

void App::update(float dt)
{
	if (currOit)
		currOit->update(dt);

	if (anim.isAnimating())
	{
		anim.update(dt);
		Renderer::instance().getActiveCamera().setDirty();
	}
}

void App::runBenchmarks()
{
	Benchmark benchmark(&profiler, [this]() -> void { this->render(); } );

	// Set camera view for benchmark.
	auto &camera = Renderer::instance().getActiveCamera();

	std::vector<std::string> times = {"Total", "Geometry Capture", "Geometry Sort", "bmaInterval0", "bmaInterval1", "bmaInterval2",
		"bmaInterval3", "bmaInterval4", "bmaInterval5", "bmaInterval6", "Counts", "Prefix Sums"};
	std::vector<std::string> data = {"lfbTotal"};

	float testTime = 10;

	// These are the new benchmarks
	// Make sure we are using linearised arrays when benchmarking.
#if 0
	// Atrium tests (32).
	benchmark.addTest("Atrium-Opaque",
		[&camera, this]() -> void
		{
			camera.setPos({ -0.5f, -0.8f, 0.0f });
			camera.setEulerRot({ -0.209439f,0,0 });
			camera.setZoom(6.68847f);
			camera.update(0.1f);
			this->toggleTransparency();
		}, testTime, times, data);
	benchmark.addTest("Atrium-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("Atrium-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("Atrium-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("Atrium-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("Atrium-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);
#endif
#if 1
	// Hairball full view tests (256).
	benchmark.addTest("HairballFull-Opaque",
		[&camera, this]() -> void
		{
			camera.setPos({ -0.400156f,-0.300304f,-0.0183215f });
			camera.setEulerRot({ -0.157078f,0.0261799f,0 });
			camera.setZoom(15);
			camera.update(0.1f);
			this->toggleTransparency();
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);

	// Hairball deep view tests (256).
	benchmark.addTest("HairballDeep-Opaque",
		[&camera, this]() -> void
		{
			camera.setPos({ -0.5f, -0.8f, 0.0f });
			camera.setEulerRot({ -0.209439f, 0.0f, 0.0f });
			camera.setZoom(6.68847f);
			camera.update(0.1f);
			this->toggleTransparency();
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);

	// Powerplant full view tests (512).
	benchmark.addTest("PowerplantFull-Opaque",
		[&camera, this]() -> void
		{
			camera.setZoom(8.48492f);
			camera.setPos({ 7.27525f, -5.43613f, -0.992247f });
			camera.setEulerRot({ 0.043634f, 1.56207f, 0.0f });
			camera.update(0.1f);
			this->loadNextMesh();
			this->toggleTransparency();
		}, testTime, times, data);

	benchmark.addTest("PowerplantFull-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);

	// Powerplant deep view tests (512).
	benchmark.addTest("PowerplantDeep-Opaque",
		[&camera, this]() -> void
		{
			camera.setZoom(3.37033f);
			camera.setPos({ 7.60963f, -4.83113f, -4.2693f });
			camera.setEulerRot({ -0.235619f, 2.00713f, 0.0f });
			camera.update(0.1f);
			this->toggleTransparency();
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);

	// Powerplant very deep view tests (512).
	benchmark.addTest("PowerplantVeryDeep-Opaque",
		[&camera, this]() -> void
		{
			camera.setPos({7.14135f, -4.37497f, -2.98195f});
			camera.setEulerRot({0.113447f, 1.26536f, 0.0f});
			camera.setZoom(1.80087f);
			camera.update(0.1f);
			this->toggleTransparency();
		}, testTime, times, data);
	benchmark.addTest("PowerplantVeryDeep-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantVeryDeep-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("PowerplantVeryDeep-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("PowerplantVeryDeep-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantVeryDeep-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);
#endif
	// Quads128 tests (128).
	benchmark.addTest("Quads128-Opaque",
		[&camera, this]() -> void
		{
			camera.setZoom(15);
			camera.setPos({ 0, 0, 0 });
			camera.setEulerRot({ 0, 0, 0 });
			camera.update(0.1f);
			//this->loadNextMesh(); // rungholt.
			//this->loadNextMesh(); // quads 64.
			//this->loadNextMesh(); // quads 128.
			this->toggleTransparency();
		}, testTime, times, data);
	benchmark.addTest("Quads128-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("Quads128-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("Quads128-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("Quads128-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("Quads128-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);

	// Quads256 tests (256).
	benchmark.addTest("Quads256-Opaque",
		[&camera, this]() -> void
		{
			camera.setZoom(15);
			camera.setPos({ 0, 0, 0 });
			camera.setEulerRot({ 0, 0, 0 });
			camera.update(0.1f);
			this->loadNextMesh();
			this->toggleTransparency();
		}, testTime, times, data);
	benchmark.addTest("Quads256-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("Quads256-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("Quads256-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("Quads256-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("Quads256-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);

	// Quads512 tests (512).
	benchmark.addTest("Quads512-Opaque",
		[&camera, this]() -> void
		{
			camera.setZoom(15);
			camera.setPos({ 0, 0, 0 });
			camera.setEulerRot({ 0, 0, 0 });
			camera.update(0.1f);
			this->loadNextMesh();
			this->toggleTransparency();
		}, testTime, times, data);
	benchmark.addTest("Quads512-LA-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("Quads512-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("Quads512-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::IMPROVED);
		}, testTime, times, data);
	benchmark.addTest("Quads512-LA-GRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
	benchmark.addTest("Quads512-LA-BASE",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::BASELINE);
		}, testTime, times, data);

	// These are the old benchmarks for the originally published paper.
	// Note: Assumes Atrium mesh is currently loaded, format is link list.
#if 0
	// Atrium Tests.
	benchmark.addTest("Atrium-Opaque",
		[&camera, this]() -> void
		{
			camera.setPos({-0.5f, -0.8f, 0.0f});
			camera.setEulerRot({-0.209439f,0,0});
			camera.setZoom(6.68847f);
			camera.update(0.1f);
			this->toggleTransparency();
		}, testTime, times, data);

	benchmark.addTest("Atrium-LL-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("Atrium-LL-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("Atrium-LL-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);

	// LA RBS/I.
	benchmark.addTest("Atrium-LA-COM",
		[&camera, this]() -> void
		{
			this->useNextLFB();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("Atrium-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("Atrium-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
#if 0
	// I RBS/I.
	benchmark.addTest("Atrium-I-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("Atrium-I-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#endif

	// Hairball full view tests.	
	benchmark.addTest("HairballFull-Opaque",
		[&camera, this]() -> void
		{
			camera.setPos({-0.400156f,-0.300304f,-0.0183215f});
			camera.setEulerRot({-0.157078f,0.0261799f,0});
			camera.setZoom(15);
			camera.update(0.1f);
			this->useNextLFB();
			this->loadNextMesh();
			this->toggleTransparency();
		}, testTime, times, data);

	benchmark.addTest("HairballFull-LL-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LL-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LL-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);

	// LA RBS/I.
	benchmark.addTest("HairballFull-LA-COM",
		[&camera, this]() -> void
		{
			this->useNextLFB();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("HairballFull-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
#if 0
	// I RBS/I.
	benchmark.addTest("HairballFull-I-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("HairballFull-I-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#endif

	// Hairball deep view tests.
	benchmark.addTest("HairballDeep-Opaque",
		[&camera, this]() -> void
		{
			camera.setPos({-0.5f, -0.8f, 0.0f});
			camera.setEulerRot({-0.209439f, 0.0f, 0.0f});
			camera.setZoom(6.68847f);
			camera.update(0.1f);
			this->useNextLFB();
			this->toggleTransparency();
		}, testTime, times, data);

	benchmark.addTest("HairballDeep-LL-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LL-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LL-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);

	// LA RBS/I.
	benchmark.addTest("HairballDeep-LA-COM",
		[&camera, this]() -> void
		{
			this->useNextLFB();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
#if 0
	// I RBS/I.
	benchmark.addTest("HairballDeep-I-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("HairballDeep-I-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#endif

	// Powerplant full view tests.
	benchmark.addTest("PowerplantFull-Opaque",
		[&camera, this]() -> void
		{
			camera.setZoom(8.48492f);
			camera.setPos({7.27525f, -5.43613f, -0.992247f});
			camera.setEulerRot({0.043634f, 1.56207f, 0.0f});
			camera.update(0.1f);
			this->useNextLFB();
			this->loadNextMesh();
			this->toggleTransparency();
		}, testTime, times, data);

	benchmark.addTest("PowerplantFull-LL-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-LL-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-LL-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);

	// LA RBS/I.
	benchmark.addTest("PowerplantFull-LA-COM",
		[&camera, this]() -> void
		{
			this->useNextLFB();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
#if 0
	// I RBS/I.
	benchmark.addTest("PowerplantFull-I-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("PowerplantFull-I-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#endif

	// Powerplant deep view tests.
	benchmark.addTest("PowerplantDeep-Opaque",
		[&camera, this]() -> void
		{
			camera.setZoom(3.37033f);
			camera.setPos({7.60963f, -4.83113f, -4.2693f});
			camera.setEulerRot({-0.235619f, 2.00713f, 0.0f});
			camera.update(0.1f);
			this->useNextLFB();
			this->toggleTransparency();
		}, testTime, times, data);

	benchmark.addTest("PowerplantDeep-LL-COM",
		[&camera, this]() -> void
		{
			this->toggleTransparency();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LL-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LL-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);

	// LA RBS/I.
	benchmark.addTest("PowerplantDeep-LA-COM",
		[&camera, this]() -> void
		{
			this->useNextLFB();
			this->useRBS(Oit::COMPOSITE);
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LA-RBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::STANDARD);
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-LA-IRBS",
		[&camera, this]() -> void
		{
			this->useRBS(Oit::SHUFFLE);
		}, testTime, times, data);
#if 0
	// I RBS/I.
	benchmark.addTest("PowerplantDeep-I-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("PowerplantDeep-I-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#endif

#if 0
	// Quads64 tests.
	benchmark.addTest("Quads64-LL-RBS",
		[&camera, this]() -> void
		{
			camera.setZoom(15);
			camera.setPos({0, 0, 0});
			camera.setEulerRot({0, 0, 0});
			camera.update(0.1f);
			this->cycleRBS();
			this->useNextLFB();
			this->loadNextMesh();
		}, testTime, times, data);
	benchmark.addTest("Quads64-LL-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);

	// LA RBS/I.
	benchmark.addTest("Quads64-LA-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("Quads64-LA-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#if 0
	// I RBS/I.
	benchmark.addTest("Quads64-I-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("Quads64-I-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#endif

	// Quads128 tests.
	benchmark.addTest("Quads128-LL-RBS",
		[&camera, this]() -> void
		{
			camera.setZoom(15);
			camera.setPos({0, 0, 0});
			camera.setEulerRot({0, 0, 0});
			camera.update(0.1f);
			this->cycleRBS();
			this->useNextLFB();
			this->loadNextMesh();
		}, testTime, times, data);
	benchmark.addTest("Quads128-LL-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);

	// LA RBS/I.
	benchmark.addTest("Quads128-LA-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("Quads128-LA-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#if 0
	// I RBS/I.
	benchmark.addTest("Quads128-I-RBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
			this->useNextLFB();
		}, testTime, times, data);
	benchmark.addTest("Quads128-I-IRBS",
		[&camera, this]() -> void
		{
			this->cycleRBS();
		}, testTime, times, data);
#endif

#endif
	// Rungholt tests?
#endif

	benchmark.setRenderFunc([this]() -> void
	{
		Renderer::instance().getActiveCamera().setViewport(0, 0, Platform::Window::getWidth(), Platform::Window::getHeight());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glLoadIdentity();
		Renderer::instance().getActiveCamera().update(1.0f);
		Renderer::instance().getActiveCamera().upload();
		this->render();
	});

	benchmark.run();
	benchmark.print();

	// Append data+time to the title, and save to a benchmarks folder.
	time_t t = time(0);
	struct tm buf;
	#if _WIN32
		localtime_s(&buf, &t);
	#else
		localtime_r(&t, &buf);
	#endif
	std::stringstream ss;
	ss << std::put_time(&buf, "-%d-%m-%Y-%H-%M");
	benchmark.writeCSV("benchmarks/benchmark" + ss.str() + ".csv", times, data);

	exit(0);
}

int App::useMesh(int currMesh)
{
	static std::vector<std::string> meshes = { "meshes/galleon.obj", "meshes/sponza/sponza.3ds",
		//"meshes/quads128.obj", "meshes/quads256.obj", "meshes/quads512.obj",
		"meshes/hairball.ctm", "meshes/powerplant-obj/powerplant.obj", "meshes/rungholt/rungholt.obj", "meshes/quads64.obj", "meshes/quads128.obj", "meshes/quads256.obj", "meshes/quads512.obj", "meshes/rungholt/rungholt.obj", "meshes/livingroom/living_room.obj",
		//"meshes/hairball.ctm", "meshes/powerplant/powerplant.ctm", "meshes/rungholt/rungholt.obj", "meshes/livingroom/living_room.obj",
		//"meshes/teapot/teapot.obj", "meshes/powerplant/powerplant.ctm", "meshes/hairball.ctm"
	};

	UniformBuffer::clearBufferGroup();

	if (currMesh > (int) meshes.size() - 1)
		currMesh = 0;
	else if (currMesh < 0)
		currMesh = (int) meshes.size() - 1;

	gpuMesh.release();

	std::cout << "Loading mesh: " << meshes[currMesh] << "\n";

	Mesh mesh;
	if (Resources::MeshLoader::load(&mesh, meshes[currMesh]))
	{
		if (meshes[currMesh] == "meshes/powerplant/powerplant.ctm" || meshes[currMesh] == "meshes/powerplant-obj/powerplant.obj")
		{
			mesh.scale(vec3(0.0001f, 0.0001f, 0.0001f));
			mesh.translate(vec3(7.5, -10, -6)); // Move the mesh so it's reasonably well centered.

			if (meshes[currMesh] == "meshes/powerplant-obj/powerplant.obj")
			{
			#if 0
				mesh = mesh.extractFromMaterials({"powerplant.mtlsec10_material_0", "powerplant.mtlsec11_material_1", "powerplant.mtlsec2_material_2",
					"powerplant.mtlsec6_material_4", "powerplant.mtlsec7_material_1", "powerplant.mtlsec1_material_1", });
			#endif
			#if 0
				mesh = mesh.extractFromMaterials({"powerplant.mtlsec3_material_3", "powerplant.mtlsec10_material_1", "powerplant.mtlsec3_material_2",
					"powerplant.mtlsec14_material_1", "powerplant.mtlsec14_material_2", "powerplant.mtlsec16_material_1", "powerplant.mtlsec21_material_3",
					"powerplant.mtlsec21_material_4", "powerplant.mtlsec21_material_5", "powerplant.mtlsec8_material_5"});
			#endif
			}
		}
		else if (meshes[currMesh] == "meshes/rungholt/rungholt.obj")
			mesh.scale(vec3(0.04f, 0.04f, 0.04f));
		else if (meshes[currMesh] == "meshes/hairball.ctm")
			mesh.scale(vec3(-10, -10, -10), vec3(10, 10, 10));
		else if (meshes[currMesh] == "meshes/sponza/sponza.3ds")
		{
			mesh.scale(vec3(-10, -5, -10), vec3(10.0f, 5.0f, 10.0f));
		#if 0
			mesh = mesh.extractFromMaterials({"sponzachain_ctx.pn", "sponzaflagpole_szf", "sponzafabric_c_szc", "sponzafabric_f_szc", "sponzaleaf_szthdi.", "sponzavase_vdi.png",
				"sponza16___Default", "sponzavase_round_v", "sponzavase_hanging", "sponzafabric_g_szc", "sponzafabric_e_szf", "sponzafabric_a_szf", "sponzafabric_d_szf",
				"sponzaMaterial__57"});
		#endif
		#if 0
			mesh = mesh.extractFromMaterials({"sponzafloor_szfoad", "sponzaMaterial__25", "sponzaMaterial__47", "sponzacolumn_a_szc", "sponzaarch_szadi.p", "sponzaroof_szrfdi.",
				"sponzabricks_snbad", "sponzadetails_szdt", "sponzacolumn_c_szc", "sponzaceiling_szcl", "sponzaMaterial__29", "sponzacolumn_b_szc"});
		#endif
		}
		else if (meshes[currMesh] == "meshes/galleon.obj")
			mesh.scale(vec3(0.2, 0.2, 0.2));
		else if (meshes[currMesh] == "meshes/teapot/teapot.obj")
			mesh.scale(vec3(2.0, 2.0, 2.0));
	#if 0
		std::cout << "Listing materials:\n";
		for (auto &faceset : mesh.getFacesets())
		{
			if (faceset.mat)
				std::cout << "Material: " << faceset.mat->getName() << " Count: " << faceset.end - faceset.start << "\n";
		}
	#endif

		std::cout << "no. triangles: " << mesh.getNumIndices() / 3 << "\n";

		gpuMesh.create(&mesh);
	}

	return currMesh;
}

int App::useLFB(int currLFB)
{
	profiler.clear();

#if USE_LINK_PAGES
	static std::vector<LFB::LFBType> types = { LFB::LINK_PAGES, LFB::LINK_LIST, LFB::LINEARIZED };
	static std::vector<std::string> typeStrs = { "Link Pages", "Link List", "Linearized" };
#else
	//static std::vector<LFB::LFBType> types = { LFB::LINK_LIST, LFB::LINEARIZED, LFB::COALESCED };
	//static std::vector<std::string> typeStrs = { "Link List", "Linearized", "Coalesced" };

	//static std::vector<LFB::LFBType> types = { LFB::LINK_LIST, LFB::LINEARIZED };
	//static std::vector<std::string> typeStrs = { "Link List", "Linearized" };

	static std::vector<LFB::LFBType> types = { LFB::LINK_LIST, LFB::LINEARIZED };
	static std::vector<std::string> typeStrs = { "Link List", "Linearized" };
#endif

	geometryLFB.release();

	delete currOit;
	currOit = nullptr;

	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();

	if (currLFB > (int) types.size() - 1)
		currLFB = 0;
	else if (currLFB < 0)
		currLFB = (int) types.size() - 1;

	LFB::LFBType type = types[currLFB];

	currLfbStr = typeStrs[currLFB];
	std::cout << "Using LFB: " << typeStrs[currLFB] << "\n";

	geometryLFB.setType(type, "lfb");
	geometryLFB.setDataType(LFB::VEC2);
	geometryLFB.setMaxFrags(MAX_FRAGS);
	geometryLFB.setProfiler(&profiler);

	switch (rbsType)
	{
	case Oit::COMPOSITE:
		currOit = new Composite(opaqueFlag);
		break;

	case Oit::BASELINE:
		currOit = new Baseline(opaqueFlag);
		break;

	case Oit::STANDARD:
		currOit = new Standard(opaqueFlag);
		break;

	case Oit::IMPROVED:
		currOit = new Improved(opaqueFlag);
		break;

	case Oit::SHUFFLE:
		currOit = new Shuffle(opaqueFlag);
		break;

	default:
		currOit = new Standard(opaqueFlag);
		break;
	}

	if (currOit)
	{
		currOit->init();
		currOit->useLFB(this, type);
	}

	reloadShaders();

	return currLFB;
}

void App::reloadShaders()
{
	auto vertCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/oit/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/oit/capture.frag");
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragCapture.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragCapture.setDefine("LFB_FRAG_TYPE", "vec2");
	fragCapture.setDefine("USE_FLOAT", "1");

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);
	
	// Sorts and writes to global memory.
	auto vertSort = ShaderSourceCache::getShader("sortVert").loadFromFile("shaders/basicSort/sort.vert");
	auto fragSort = ShaderSourceCache::getShader("sortFrag").loadFromFile("shaders/basicSort/sort.frag");
	fragSort.setDefine("MAX_FRAGS", Utils::toString(geometryLFB.getMaxFrags()));
	fragSort.setDefine("COMPOSITE_LOCAL", "0");
	fragSort.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragSort.setDefine("LFB_FRAG_TYPE", "vec2");
	fragSort.setDefine("USE_FLOAT", "1");
	fragSort.replace("LFB_NAME", "lfb");

	sortGeometryShader.release();
	sortGeometryShader.create(&vertSort, &fragSort);

	//std::cout << "Compiled one\n";

	// Sorts and composites.
	auto vertLocalSort = ShaderSourceCache::getShader("sortLocalVert").loadFromFile("shaders/sort/sort.vert");
	auto fragLocalSort = ShaderSourceCache::getShader("sortLocalFrag").loadFromFile("shaders/sort/sort.frag");
	fragLocalSort.setDefine("MAX_FRAGS", Utils::toString(geometryLFB.getMaxFrags()));
	fragLocalSort.setDefine("COMPOSITE_LOCAL", "1");
	fragLocalSort.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragLocalSort.setDefine("LFB_FRAG_TYPE", "vec2");
	fragLocalSort.setDefine("USE_FLOAT", "1");
	fragLocalSort.replace("LFB_NAME", "lfb");

	sortGeometryLocalShader.release();
	sortGeometryLocalShader.create(&vertLocalSort, &fragLocalSort);

	//Utils::File f("shader.txt");
	//f.write(sortGeometryLocalShader.getBinary());

	//std::cout << "Compiled two\n";

	// BMA shaders that write to global memory.
	geometryBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
	geometryBMA.createShaders(&geometryLFB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "0"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)},
		{"USE_FLOAT", "1"}, {"LFB_FRAG_TYPE", "vec2"}});
	geometryBMA.setProfiler(&profiler);

	//std::cout << "BMA one\n";

	// BMA shaders that sort and composite.
	geometryLocalBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
	geometryLocalBMA.createShaders(&geometryLFB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)},
		{"USE_FLOAT", "1"}, {"LFB_FRAG_TYPE", "vec2"}});
	geometryLocalBMA.setProfiler(&profiler);

	//std::cout << "BMA two\n";

	if (currOit)
	{
		std::cout << "Reloading shaders\n";
		currOit->reloadShaders(this);
	}
}

void App::toggleTransparency()
{
	opaqueFlag = opaqueFlag == 0 ? 1 : 0;
	currLFB = useLFB(currLFB);
}

void App::loadNextMesh()
{
	currMesh = useMesh(currMesh + 1);
}

void App::loadPrevMesh()
{
	currMesh = useMesh(currMesh - 1);
}

void App::useNextLFB()
{
	currLFB = useLFB(currLFB + 1);
}

void App::usePrevLFB()
{
	currLFB = useLFB(currLFB - 1);
}

void App::cycleRBS()
{
	static std::vector<Oit::RbsType> rbsTypes = {Oit::COMPOSITE, Oit::BASELINE, Oit::STANDARD, Oit::IMPROVED, Oit::SHUFFLE};

	static int currRbsType = 0;

	currRbsType++;
	if (currRbsType > (int) rbsTypes.size() - 1)
		currRbsType = 0;

	useRBS(rbsTypes[currRbsType]);
}

void App::useRBS(Oit::RbsType type)
{
	static std::map<Oit::RbsType, std::string> rbsTypes;
	if (rbsTypes.empty())
	{
		rbsTypes[Oit::COMPOSITE] = "Composite";
		rbsTypes[Oit::BASELINE] = "Baseline";
		rbsTypes[Oit::STANDARD] = "Standard";
		rbsTypes[Oit::IMPROVED] = "Improved";
		rbsTypes[Oit::SHUFFLE] = "Shuffle";
	}

	rbsType = type;
	currRbsStr = rbsTypes[type];
	std::cout << "Using RBS: " << currRbsStr << "\n";

	currLFB = useLFB(currLFB);
}

void App::playAnim()
{
	anim.play();
}

void App::saveGeometryLFB()
{
	if (opaqueFlag == 1)
		return;
	auto s = geometryLFB.getStr();
	//auto s = geometryLFB.getBinary();
	Utils::File f("lfb.txt");
	f.write(s);
}

void App::saveLFBData()
{
	currOit->saveLFB(this);
}

void App::saveImg()
{
	// Save the frame to a file.
	Image img("screenshot.png");
	img.screenshot();
	img.flip();
	Resources::ImageLoader::save(&img, img.getName());
}

void App::visualise()
{
	currOit->visualise(this);
}

