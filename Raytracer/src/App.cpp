#include "App.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include "Lighting/Standard.h"
#include "Lighting/DeepImg.h"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <iomanip>

#define DRAW_FPS 1
#define DRAW_AXES 1


using namespace Math;
using namespace Rendering;


// TODO: Get rid of this!
std::string currLightStr;
Flythrough anim;
Font testFont;
float tpf = 0;



void App::init()
{
	// TODO: Get rid of this!
	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");

	profiler.clear();

	Resources::SceneLoader::load(scenes, "scenes/scenes.xml");

	mainLightPos = vec3(2.77f, 7.95f, 12.47f);
	mainLightDir = -mainLightPos.unit();

#if 1
	lightCam.setType(Camera::Type::PERSP);
	lightCam.setPos(mainLightPos);
	lightCam.setViewDir(mainLightDir);
#else
	lightCam.setType(Camera::Type::ORTHO);
	lightCam.setOrtho(-15, 15, -15, 15);
	lightCam.setDistance(-15, 15);
	lightCam.setPos(mainLightPos);
	lightCam.setViewDir(mainLightDir);
	//float wsMin = -15;
	//float wsMax = 15;
	//float scale = 512.0f / (wsMax - wsMin);
	//lightCam.setType(Camera::Type::ORTHO);
	//lightCam.setDistance(0, 512);
	//lightCam.setPos({wsMin, wsMin, -wsMin});
	//lightCam.setZoom(scale);
	//lightCam.setViewport(0, 0, 512, 512);
	//lightCam.setViewDir(mainLightDir);
#endif
	lightCam.update(0.01f);

#if 0
	deepCams[index].setType(Camera::Type::ORTHO);
	deepCams[index].setDistance(0, (float) DEEP_RES);
	deepCams[index].setPos({wsMin, wsMin, -wsMin});
	deepCams[index].setZoom(scale);
	deepCams[index].setViewport(0, 0, DEEP_RES, DEEP_RES);
	deepCams[index].setViewDir(-viewDir);
	deepCams[index].update(0.1f);
#endif

	currLighting = useLighting(0);
	currScene = useScene(0);

	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);

	// Might as well set the initial camera pos.
	auto &camera = Renderer::instance().getActiveCamera();
	camera.setPos({-0.85f, -0.1f, 0.0f});
	camera.setZoom(6.25342f);
	camera.update(0.1f);

#if 0
	// TODO: Use this camera for the different deep image cameras!!!!
	float wsMin = -15;
	float wsMax = 15;
	float scale = (float) 128 / ((float) wsMax - (float) wsMin);

	camera.setType(Camera::Type::ORTHO);
	camera.setDistance(0, (float) 128);
	camera.setPos({wsMin, wsMin, -wsMin});
	//camera.setViewDir({-1, -1, -1});
	camera.setZoom(scale);
	camera.setViewport(0, 0, 128, 128);
	camera.update(0.1f);
#endif
}

void App::setTmpViewport(int width, int height)
{
	auto &camera = Renderer::instance().getActiveCamera();

	tmpWidth = camera.getWidth();
	tmpHeight = camera.getHeight();

	camera.setViewport(0, 0, width, height);
	camera.uploadViewport();
	camera.update(0);
}

void App::restoreViewport()
{
	auto &camera = Renderer::instance().getActiveCamera();

	camera.setViewport(0, 0, tmpWidth, tmpHeight);
	camera.uploadViewport();
	camera.update(0);
}

void App::drawMesh(const Camera &camera)
{
	if (!gpuMesh)
		return;

	//bloom.resize(camera.getWidth(), camera.getHeight());

	//bloom.beginCapture();
	setTmpViewport(camera.getWidth(), camera.getHeight());

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	basicShader.bind();
	basicShader.setUniform("mvMatrix", camera.getInverse());
	basicShader.setUniform("pMatrix", camera.getProjection());
	basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	basicShader.setUniform("lightDir", Math::vec3(0, 0, 1));
	gpuMesh->render();
	basicShader.unbind();

	glPopAttrib();

	restoreViewport();

	//bloom.endCapture();

	// Blur the bright texture and composite.
	//bloom.applyBlur();
	//bloom.composite();

	//bloom.debugRenderColor();
	//bloom.debugRenderBright();
	//bloom.debugRenderBlur(3);
}

void App::drawMesh()
{
	drawMesh(Renderer::instance().getActiveCamera());
}

void App::render()
{
	// TODO: Use deferred rendering for this, or maybe give an option for deferred or forward.
	static float fpsUpdate = 0;

	profiler.begin();
	profiler.start("Total");

	auto &camera = Renderer::instance().getActiveCamera();
	camera.upload();

	if (lighting)
		lighting->render(this);

	profiler.time("Total");

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);

#if DRAW_FPS
	if (hudFlag)
	{
		float total = profiler.getTime("Total");
		float smoothing = 0.9f; // Larger => more smoothing.
		tpf = (tpf * smoothing) + (total * (1.0f - smoothing));
		float t = round(total);
		#if 0
			testFont.renderFromAtlas("Hybrid Lighting\nTime per frame(ms): " + Utils::toString(t) +
				"\nFrames per second (fps): " + Utils::toString(1000.0 / t) +
				"\nLights: " + Utils::toString(lightColors.size()), 10, 10, 0, 0, 1);
		#else
			testFont.renderFromAtlas("Technique: " + currLightStr +
				"\nTime per frame (ms): " + Utils::toString(t) +
				"\nFrames per second (fps): " + Utils::toString(1000.0 / t), 10, 10, 0, 0, 0, 1);
		#endif
	}
#endif

	float dt = Platform::Window::getDeltaTime();
	fpsUpdate += dt;
	if (fpsUpdate > 2)
	{
		profiler.print();
		fpsUpdate = 0;
	}

#if 0
	if (t > 1000)
	{
		std::cout << "Slow frametime. Exiting.\n";
		exit(EXIT_SUCCESS);
	}
#endif

#if DRAW_AXES
	if (hudFlag)
	{
		Renderer::instance().drawAxes();
		Renderer::instance().drawAxes(Renderer::instance().getActiveCamera().getPos(), 2);
	}
#endif

	glPopAttrib();
}

void App::update(float dt)
{
	if (lighting)
		lighting->update(dt);
}

int App::useScene(int currScene)
{
	scenes[currScene].clearCachedMeshes();
	scenes[currScene].clearGpuMeshes();
	//scenes[currScene].clear();

	if (currScene > (int) scenes.size() - 1)
		currScene = 0;
	else if (currScene < 0)
		currScene = (int) scenes.size() - 1;

	std::cout << "Loading scene: " << scenes[currScene].getName() << "\n";

	scenes[currScene].loadMeshesToCache();
	scenes[currScene].loadGpuMeshes();

	mesh = scenes[currScene].getSceneObjects()[0].mesh;
	gpuMesh = scenes[currScene].getSceneObjects()[0].gpuMesh;

	if (!mesh)
		std::cout << "ohno!\n";
	if (!gpuMesh)
		std::cout << "ohnoagain!\n";

	nIndices = 0;
	vertexDataBuffer.release();
	indexDataBuffer.release();
	if (mesh)
	{
		// Need a buffer of geometry vertex positions.
		std::vector<Math::vec4> vertexPositions(mesh->getNumVertices());
		for (size_t i = 0; i < mesh->getNumVertices(); i++)
		{
			auto &pos = mesh->getVertex(i).pos;
			vertexPositions[i] = Math::vec4(pos.x, pos.y, pos.z, 1.0f);
		}
		vertexDataBuffer.create(&vertexPositions[0], vertexPositions.size() * sizeof(Math::vec4));

		// Also need an index buffer.
		std::vector<unsigned int> vertexIndices(mesh->getNumIndices());
		for (size_t i = 0; i < mesh->getNumIndices(); i++)
		{
			auto index = mesh->getIndex(i);
			vertexIndices[i] = index;
		}
		indexDataBuffer.create(&vertexIndices[0], vertexIndices.size() * sizeof(unsigned int));
		nIndices = (unsigned int) vertexIndices.size();
	}

	lighting->useScene(this);

	return currScene;
}

void App::useNextScene()
{
	currScene = useScene(currScene + 1);
}

void App::usePrevScene()
{
	currScene = useScene(currScene - 1);
}

int App::useLighting(int currLighting)
{
	static std::vector<std::string> lightStrs = { "standard", "Deep Img"  };
	static std::vector<Lighting::Type> lightTypes = { Lighting::STANDARD, Lighting::DEEP_IMG };

	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();

	if (currLighting > (int) lightStrs.size() - 1)
		currLighting = 0;
	else if (currLighting < 0)
		currLighting = (int) lightStrs.size() - 1;

	std::cout << "Using lighting: " << lightStrs[currLighting] << "\n";

	if (lighting)
		delete lighting;
	Lighting::Type type = lightTypes[currLighting];

	switch (type)
	{
	case Lighting::STANDARD:
		lighting = new Standard();
		break;
	case Lighting::DEEP_IMG:
		lighting = new DeepImg();
		break;
	default:
		lighting = new Standard();
		break;
	}

	lighting->init(this);
	lighting->useLighting(this);

	return currLighting;
}

void App::useNextLighting()
{
	currLighting = useLighting(currLighting + 1);
}

void App::usePrevLighting()
{
	currLighting = useLighting(currLighting - 1);
}

void App::setLightPos()
{
	auto &camera = Renderer::instance().getActiveCamera();

	auto pos = camera.getPos();
	auto lightDir = camera.getForward();

	mainLightPos = pos - (lightDir.unit() * camera.getZoom());
	mainLightDir = lightDir.unit();

#if 1
	lightCam.setPos(mainLightPos);
	lightCam.setViewDir(mainLightDir);
#else
	float wsMin = -15;
	float wsMax = 15;
	float scale = 512.0f / (wsMax - wsMin);
	lightCam.setDistance(0, 512);
	lightCam.setPos({wsMin, wsMin, -wsMin});
	lightCam.setZoom(scale);
	lightCam.setViewport(0, 0, 512, 512);
	lightCam.setViewDir(mainLightDir);
#endif
	lightCam.update(0.01f);

	std::cout << "New light pos: " << mainLightPos << "\n";
	std::cout << "New light dir: " << mainLightDir << "\n";
}

void App::reloadShaders()
{
#if 0
	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();
#endif

	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);

	if (lighting)
	{
		std::cout << "Reloading shaders\n";
		lighting->reloadShaders(this);
	}
}

void App::saveData()
{
	if (lighting)
		lighting->saveData(this);
}

void App::runBenchmarks()
{
	Benchmark benchmark(&profiler, [this]() -> void { this->render(); } );

	// Set camera view for benchmark.
	auto &camera = Renderer::instance().getActiveCamera();

	std::vector<std::string> times = {"Capture g-buffer", "Composite", "LFB create", "LFB sort", "Total"};
	std::vector<std::string> data = {"lfbTotal", "g-buffer"};

	float testTime = 10;

	// Note: Assumes Atrium mesh is currently loaded, format is linked list and technique is stepwise.

	// Atrium Tests.
	// Atrium LL-S.
	benchmark.addTest("Atrium",
		[&camera]() -> void
		{
			camera.setPos({-0.0967992f, -2.74396f, -0.180925f});
			camera.setEulerRot({0.008727f, 3.10668f, 0.0f});
			camera.setZoom(5.9259f);
			camera.update(0.1f);
		}, testTime, times, data);

	// Powerplant tests.
	// Power LL-S.
	benchmark.addTest("Rungholt",
		[&camera, this]() -> void
		{
			// Rungholt camera.
			camera.setPos({2.90805,3.25054,3.62739});
			camera.setEulerRot({-0.706858f, 0.733033f, 0.0f});
			camera.setZoom(0.0f);
			camera.update(0.1f);

			// TODO: Rungholt light pos.

			this->useNextScene();
		}, testTime, times, data);

	benchmark.setRenderFunc([this]() -> void
	{
		Renderer::instance().getActiveCamera().setViewport(0, 0, Platform::Window::getWidth(), Platform::Window::getHeight());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glLoadIdentity();
		Renderer::instance().getActiveCamera().update(1.0f);
		Renderer::instance().getActiveCamera().upload();
		this->render();
	});

	benchmark.run();
	benchmark.print();

	// Append data+time to the title, and save to a benchmarks folder.
	time_t t = time(0);
	struct tm buf;
	#if _WIN32
		localtime_s(&buf, &t);
	#else
		localtime_r(&t, &buf);
	#endif
	std::stringstream ss;
	ss << std::put_time(&buf, "-%d-%m-%Y-%H-%M");
	benchmark.writeCSV("benchmarks/benchmark" + ss.str() + ".csv", times, data);

	exit(0);
}

void App::savePathTracedImg()
{
}

