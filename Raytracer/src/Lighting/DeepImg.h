#pragma once

#include "Lighting.h"

#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Renderer/src/RenderObject/Texture.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/LFB/Cluster.h"
#include "../../../Renderer/src/LFB/ClusterMask.h"
#include "../../../Renderer/src/Lighting/GBuffer.h"
#include "../../../Renderer/src/Lighting/RSM.h"
#include "../../../Renderer/src/Lighting/ShadowMap.h"
#include "../../../Renderer/src/Camera.h"
#include "../../../Renderer/src/Voxel/VoxelGrid.h"

#include "../../../Renderer/src/LFB/LFB.h"
#include "../../../Renderer/src/LFB/BMA.h"

#include <vector>

class DeepImg : public Lighting
{
private:
	// Uniform grid of voxels.
	Rendering::VoxelGrid voxelGrid;
	Rendering::Shader buildVoxelShader;

	// For rendering multiple deep images simultaneously.
	Rendering::Camera deepCamZ;
	std::vector<Rendering::Camera> deepCams;
	Rendering::StorageBuffer deepCamBuffer;
	Rendering::StorageBuffer invDeepCamBuffer;
	Rendering::FrameBuffer sortFbo;
	Rendering::Texture2D sortTex;
	Rendering::RenderBuffer sortDepth;

	// Need a deep image to render the geometry to.
	// TODO: How do I handle multiple resolutions for the different voxel grid levels?
	// TODO: Put them all in the one lfb?
	// TODO: Or have a different lfb for each level and sort them separately?
	// TODO: How do regular mipmapped textures work?
	Rendering::LFB lfb; // Lfb that we capture geometry to.
	Rendering::BMA bma;

	Rendering::LFB upperLfb[4]; // TODO: Something better, for now just store an array for the upper CDI levels.
	int upperCols[4];
	int upperRows[4];

	// Building the CDI normally.
	Rendering::Shader captureLfbShader;
	Rendering::Shader countLfbShader;
	Rendering::Shader drawLfbShader;

	// Building the CDI from voxels.
	Rendering::Shader countLfbVoxelShader;
	Rendering::Shader captureLfbVoxelShader;

	// RSM texture data, captured from light's perspective.
	Rendering::RSM rsm;

	// Alternatively, can just use a shadow map.
	Rendering::ShadowMap shadowMap;
	Rendering::Shader drawShadowShader;

	Rendering::Shader realDirShader;

	Rendering::StorageBuffer realDirBuffer;

	Rendering::GBuffer gBuffer;
	Rendering::Shader shadowShader;
	Rendering::Shader pathTraceShader;
	Rendering::Shader coneTraceShader;

	Rendering::Shader basicShader;
	Rendering::Shader debugDirShader;

	Rendering::Shader bruteShadowShader;

	Rendering::Shader debugRaysShader;

	int stacks;
	int slices;

	int deepCols;
	int deepRows;

	bool captureVoxelFlag;
	bool captureDeepFlag;

private:
	void createCameras();
	void drawCameraDirs(App *app);
	void drawCameraGeom(App *app);

	void buildVoxelGrid(App *app);
	void drawVoxelGrid(App *app);

	void drawRays(App *app);

	void captureDeepImages(App *app);
	void buildDeepImagesFromVoxels(App *app);

	void captureRSM(App *app);
	void renderRSM(App *app);

	void captureShadows(App *app);
	void renderShadows(App *app);

	void renderDeepImages(App *app);
	void renderDeepImage(App *app, int index, int pixel = -1);

	void testDDA(App *app);

	void testDuplicates(App *app);
	void buildRayMask(App *app);

	void captureGeometry(App *app);

	void raytraceShadows(App *app);
	void pathTrace(App *app);
	void coneTrace(App *app);

	void bruteForceShadows(App *app);

public:
	DeepImg() : Lighting(), stacks(0), slices(0), deepCols(0), deepRows(0), captureVoxelFlag(false), captureDeepFlag(false) {}
	virtual ~DeepImg() {}

	virtual void init(App *app) override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLighting(App *app) override;
	virtual void useScene(App *app) override { (void) (app); captureVoxelFlag = false; captureDeepFlag = false; }

	virtual void reloadShaders(App *app) override;
	virtual void saveData(App *app) override;
};

