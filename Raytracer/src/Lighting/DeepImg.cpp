#include "DeepImg.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"
#include "../../../Utils/src/Random.h"

#include "../../../Resources/src/ResourcesCommon.h"
#include "../../../Platform/src/Window.h"

#include "../../../Renderer/src/LFB/LFBLinear.h"
#include "../../../Renderer/src/LFB/LFBList.h"

#include <string>
#include <cassert>
#include <bitset>


// TODO: Modify the TEST_DDA code to include depth ranges.

// DONE: Capture to a linked list, sort into a linearised array. Doesn't seem to be much faster.
// DONE: Brute force raytraced shadows are suuuper sloooow.
// DONE: Do we need to store triangle indices or not? It seems so.
// DONE: Is it much slower if I use conservative rasterization? Yes, much slower :(

// DONE: Does odd-even warp sort allow faster compound deep image building?

// TODO: Raycasting depth values instead of triangles can be done, but requires a little more work.
// TODO: Only store one value per-fragment (the depth value).
// TODO: Can we get away with an approximate 1 byte depth value? I doubt it but worth trying.
// TODO: This can't be used for shadows, but it could be used for indirect lighting.

// TODO: Possible to trace against surfels rather than triangles?
// TODO: A surfel is a disk with a position, normal and radius.
// TODO: Can give all surfels the same radius to reduce data requirements.
// TODO: Position of the surfel is determined by screen-space pixel and eye-space depth.
// TODO: Normal vector can be packed into a single float (with loss of precision).
// TODO: Surfel radius should be large enough to cover the pixel (or maybe slightly smaller, not sure).
// TODO: Possible to store a more dynamic surfel shape instead?
// TODO: Draw the surfels so we can see if they're correct or not.

// TODO: Start with raytraced shadows.
// DONE: Need a tiled deep image indexing system so I can handle a larger number of images.
// DONE: Rasterizing separate triangles for each direction can't be done in one go using the geometry shader.
// DONE: For a very low directional precision, 1024 directions seems like it should be enough.
// DONE: So far, it looks like you need to step between pixels to fix the gaps.
// DONE: My DDA appears to be missing some pixels. Check it to make sure it's correct.
// DONE: Maybe use the approach from that grid pdf, it may be more correct.
// DONE: Capture triangles and depths, then sort and raycast. Is this faster?
// TODO: Use a different direction indexing system, from that other paper.
// TODO: This probably requires an array of directions, not 100% sure how I'm going to index it.
// TODO: What about view independent rasterization?
// TODO: Need to be able to sort lists > 512 fragments for the atrium.
// TODO: How large is the atrium's longest fragment list?

// TODO: Raytraced reflections.
// TODO: One ray per-pixel.
// TODO: Fewer directions, non-conservative.
// TODO: Should give an approximate result, possibly noisy.
// TODO: This needs colour data, can this be retrieved from the triangle or does it need to be per-fragment.
// TODO: If it needs to be per-fragment, can we get away with not raycasting against triangles?

// TODO: How do we deal with duplicates?
// DONE: The vast majority of the data consists of duplicates.
// DONE: Conservative rasterization adds about 10x the amount of data.
// TODO: If you ignore duplicates, is conservative rasterization a problem?

// Data testing with 512 deep images at 64x64 res.
//
// Duplicate data for the Galleon:
// Without conservative rasterization: Total vals: 1363456, total uniques: 1304553, percent unique: 95.6799
// With conservative rasterization: Total vals: 11014696, total uniques: 8690534, percent unique: 78.8994
// With custom rasterization: Total vals: 1739313, total uniques: 1739313, percent unique: 100
//
// Duplicate data for the Atrium:
// Without conservative rasterization: Total vals: 13072384, total uniques: 8611895, percent unique: 65.8785
// With conservative rasterization: Total vals: 213080570, total uniques: 195165247, percent unique: 91.5922
// With custom rasterization: Total vals: 14269366, total uniques: 14269366, percent unique: 100

// Data testing with 512 deep images at 256x256 res.
//
// Duplicate data for the Atrium:
// Without conservative rasterization: Total vals: 22560768, total uniques: 16497241, percent unique: 73.1236
// With conservative rasterization: Total vals: 585231872, total uniques: 432429734, percent unique: 73.8903
// With custom rasterization: Total vals: 171957613, total uniques: 171957613, percent unique: 100
//
// Conservative rasterization gives me 20X the amount of data!!!

// TODO: Get shadow map or maybe RSM from light's perspective.
// TODO: Use that to help with the path traced indirect lighting.

// Try storing voxels in the CDI rather than rasterized geometry.
// TODO: Mipmapped CDI? How do I downsample?
// TODO: Cone tracing instead of path tracing?
// TODO: That just means reading from a higher CDI level based on distance right?

// TODO: Can we raster at twice LFB res rather than conservatively?

// TODO: Try path tracing.
// TODO: Start with implementing path tracing on the CPU. See what it looks like and how it works.
// TODO: Then move onto doing it on the GPU using a CDI.

// TODO: Mipmap the CDI using voxel grid data first.
// TODO: Then mipmap the CDI using CDI fragment data only.
// TODO: Is there a difference?
// TODO: What's an appropriate memory layout for the CDI?
// TODO: Do we have different deep images for different lods?
// TODO: Do we put extra lod data at the end of each fragment list?


// TODO: What if we store a list of triangles for each voxel?
// TODO: And then check those triangles when we hit a voxel during CDI casting?
// TODO: What does the CDI data actually look like in this case?


// Not sure how large I want the shadow map to be...
// TODO: Instead of using an RSM, try using an actual shadow map.
#define RSM_RES 2048
#define CAPTURE_RSM 0
#define RENDER_RSM 0
#define USE_RSM 0

#define SHADOW_RES 4096
#define CAPTURE_SHADOW_MAP 0
#define RENDER_SHADOWS 0
#define USE_SHADOW_MAP 0

// 256 or maybe even 512 would be ok for static geometry. A spearate CDI of 128 is needed for dynamic geometry.
#define DEEP_RES 256 // TODO: What is an actually good resolution?
#define N_DEEPS 256 // TODO: Implement a better directional partitioning.
#define SORT_DEEP 1
#define CONSERVATIVE 1
#define SINGLE_VAL 0
#define USE_NDC 0
#define BASIC_INDEXING 0 // Doesn't appear to be a significant difference when using a different indexing system.

#define USE_VOXELS 1
#define VOXEL_LIST 1

// TODO: Just mipmap the voxels using the grid data I guess.
// TODO: Ideally having a list for each level would be much better, and probably faster.
// TODO: May want to reduce the number of directions at each CDI level.
#define MULTI_CDI 0

#define RAYCAST_DEPTH_ONLY 0
#define RAYCAST_SURFELS 0
#define PATH_TRACE 1
#define CONE_TRACE 0

#define RASTER_CUSTOM 0 // Do our own per-triangle rasterization in the geometry shader (could use the vertex shader too I guess).
#define DEEP_MAX_RES 8192 // 4096, 8192, 16384, 32768
#define CAPTURE_ONCE 1
#define DISCARD_DUPS 0 // FIXME: This still maintains previous lfb offsets.
#define CAPTURE_COLOR 1

#define DRAW_DEEPS 0
#define DRAW_VOXELS 0
#define DRAW_CAM_DIRS 0
#define DRAW_CAM_GEOM 0
#define RAY_MASK 0
#define TEST_DDA 0
#define TEST_DUPLICATES 0
#define TEST_RAY_DIRS 0

#define SHUFFLE_MIN 3


using namespace Rendering;
using namespace Math;


// FIXME: Seems to result in intersections not on the line.
bool testTriangleRayIntersection(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 end, vec3 &pos)
{
	// If we don't normalise this, then we know t values > 1 will be past the end point.
	vec3 d = (end - o);

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	vec3 h = d.cross(e2);
	float a = e1.dot(h);

	if (a > -0.00001 && a < 0.0001)
	{
		return false;
	}

	float f = 1.0f / a;
	vec3 s = o - v0;
	float u = f * s.dot(h);

	if (u < 0.0 || u > 1.0)
	{
		return false;
	}

	vec3 q = s.cross(e1);
	float v = f * d.dot(q);

	if (v < 0.0 || u + v > 1.0)
	{
		return false;
	}

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * e2.dot(q);

	// Intersection.
	if (t > 0.00001 && t < 1)
	{
		pos = o + (d * t);
		return true;
	}

	// Line intersection but not ray intersection.
	else
	{
		return false;
	}

	return false;
}

vec4 calcDeepDir(int stack, int slice, bool reverseFlag)
{
	int stacks = (int) sqrtf(N_DEEPS);
	int slices = (int) sqrtf(N_DEEPS);

	float theta = (float(slice) / float(slices)) * 2.0f * (float) M_PI;
	float phi = (float(stack) / float(stacks)) * 0.5f * (float) M_PI;

	float x = cos(theta) * sin(phi);
	float z = sin(theta) * sin(phi);
	float y = cos(phi);

	if (reverseFlag)
	{
		x = -x;
		y = -y;
		z = -z;
	}

	vec4 deepDir = vec4(x, y, z, 0);
	return deepDir;
}

std::pair<int, vec4> getDeepImgIndex(vec3 osDir, bool reverseFlag)
{
	int stacks = (int) sqrtf(N_DEEPS);
	int slices = (int) sqrtf(N_DEEPS);

	float p = acosf(osDir.y);
	float t = atan2(osDir.z, osDir.x);
	if (t < 0)
		t += 2.0f * (float) M_PI;

	int st = int(round((p / (0.5f * (float) M_PI)) * float(stacks)));
	int sl = int(round((t / (2.0f * (float) M_PI)) * float(slices)));

	vec4 deepDir = calcDeepDir(st, sl, reverseFlag);

	int index = sl * (stacks + 1) + st;
	return {index, deepDir};
}

vec2 getScreenFromClip(vec4 csPos, vec2 size)
{
	return vec2(((vec2(csPos.xy / csPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(size.x, size.y));
}

int getPixel(vec4 esPos)
{
	// TODO: Projection won't be identity matrix anymore.
	//vec4 esPos = deepCams[index] * vec4(osPos, 1);

	// Projection is identity matrix.
	vec2 ssPos = getScreenFromClip(esPos, vec2(DEEP_RES, DEEP_RES));

	int index = int((int) ssPos.y * DEEP_RES + (int) ssPos.x);
	return index;
}

vec4 getEyeFromWindow(vec3 p, ivec4 viewport, mat4 invPMatrix)
{
	vec3 ndcPos;
	ndcPos.xy = ((p.xy - vec2(viewport.x, viewport.y)) / vec2(viewport.z, viewport.w)) * 2.0 - 1.0;
	//ndcPos.z = 1.0;
	ndcPos.z = p.z;

	vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0);
	eyeDir /= eyeDir.w;
	vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0);
	return eyePos;
}

std::vector<vec4> rangeTraverse(vec3 pos, vec3 tMax, vec3 td, vec3 s)
{
	size_t i = 0;
	std::vector<vec4> rangeHits;

	for (int canary = 0; canary < 4000; canary++)
	{
		rangeHits.push_back(vec4());
		rangeHits[i].x = pos.x;
		rangeHits[i].y = pos.y;
		//std::cout << pos.x << ", " << pos.y << ": ";

		vec2 tmpRange = vec2(pos.z, pos.z);
		float tm = min(tMax.x, tMax.y);
		if (tMax.z <= tm)
		{
			float amt = ceil((tm - tMax.z) / td.z);
			tMax.z += td.z * amt;
			pos.z += s.z * amt;
		}
		tmpRange.y = pos.z;
		vec2 depthRange = vec2(min(tmpRange.x, tmpRange.y), max(tmpRange.x, tmpRange.y));

		//std::cout << depthRange.x << ", " << depthRange.y << "\n";
		rangeHits[i].z = depthRange.x;
		rangeHits[i].w = depthRange.y;
		i++;
		
		if (tMax.x > 1 && tMax.y > 1)
			break;

		if (tMax.x < tMax.y)
		{
			tMax.x += td.x;
			pos.x += s.x;
		}
		else
		{
			tMax.y += td.y;
			pos.y += s.y;
		}
	}

	return rangeHits;
}

std::vector<vec3> gridTraverse(vec3 pos, vec3 tMax, vec3 td, vec3 s)
{
	std::vector<vec3> gridHits;

	for (int canary = 0; canary < 4000; canary++)
	{
		//std::cout << pos.x << ", " << pos.y << ", " << pos.z << "\n";
		gridHits.push_back(pos);

		if (tMax.x > 1 && tMax.y > 1 && tMax.z > 1)
			break;

		if (tMax.x < tMax.y)
		{
			if (tMax.x < tMax.z)
			{
				tMax.x += td.x;
				pos.x += s.x;
			}
			else
			{
				tMax.z += td.z;
				pos.z += s.z;
			}
		}
		else
		{
			if (tMax.y < tMax.z)
			{
				tMax.y += td.y;
				pos.y += s.y;
			}
			else
			{
				tMax.z += td.z;
				pos.z += s.z;
			}
		}
	}

	return gridHits;
}



void DeepImg::createCameras()
{
	stacks = (int) sqrt(N_DEEPS);
	slices = (int) sqrt(N_DEEPS);

	// FIXME: I've noticed that indexing is going up to 13x13 rather than 12x12...
	//deepCams.resize((stacks + 1) * slices);
	deepCams.resize((stacks + 1) * (slices + 1));

	std::vector<mat4> matrices;
	std::vector<mat4> invMatrices;
	//matrices.resize((stacks + 1) * slices);
	matrices.resize((stacks + 1) * (slices + 1));
	invMatrices.resize(matrices.size());

	int index = 0;
	for (int slice = 0; slice <= slices; slice++)
	{
		for (int stack = 0; stack <= stacks; stack++) // Need the bottom row.
		{
			float theta = ((float) slice / (float) slices) * 2.0f * (float) M_PI;
			float phi = ((float) stack / (float) stacks) * 0.5f * (float) M_PI;
			float x = cosf(theta) * sinf(phi);
			float z = sinf(theta) * sinf(phi);
			float y = cosf(phi);

		#if USE_NDC
			deepCams[index].setType(Camera::Type::NORMAL);
			deepCams[index].setZoom(0.06f); // FIXME: This zoom doesn't cover everything :(
			deepCams[index].setViewport(0, 0, DEEP_RES, DEEP_RES);
			deepCams[index].setViewDir({x, y, z});
			deepCams[index].update(0.1f);
		#else
			vec3 viewDir(x, y, z);
			float wsMin = -15;
			float wsMax = 15;
			float scale = (float) DEEP_RES / ((float) wsMax - (float) wsMin);

			deepCams[index].setType(Camera::Type::ORTHO);
			deepCams[index].setDistance(0, (float) DEEP_RES);
			deepCams[index].setPos({wsMin, wsMin, -wsMin});
			deepCams[index].setZoom(scale);
			deepCams[index].setViewport(0, 0, DEEP_RES, DEEP_RES);
			deepCams[index].setViewDir(-viewDir);
			deepCams[index].update(0.1f);
		#endif

			matrices[index] = deepCams[index].getInverse();
			invMatrices[index] = deepCams[index].getTransform();

			index++;
		}
	}

	deepCamBuffer.release();
	deepCamBuffer.create(&matrices[0], matrices.size() * sizeof(mat4));

	invDeepCamBuffer.release();
	invDeepCamBuffer.create(&invMatrices[0], invMatrices.size() * sizeof(mat4));

#if USE_NDC
	deepCamZ.setType(Camera::Type::NORMAL);
	deepCamZ.setZoom(0.06f);
	deepCamZ.setViewport(0, 0, DEEP_RES, DEEP_RES);
	deepCamZ.update(0.1f);
#else
	float wsMin = -15;
	float wsMax = 15;
	float scale = (float) DEEP_RES / ((float) wsMax - (float) wsMin);

	deepCamZ.setType(Camera::Type::ORTHO);
	deepCamZ.setDistance(0, (float) DEEP_RES);
	deepCamZ.setPos({wsMin, wsMin, -wsMin});
	deepCamZ.setZoom(scale);
	deepCamZ.setViewport(0, 0, DEEP_RES, DEEP_RES);
	deepCamZ.update(0.1f);
#endif
}

void DeepImg::drawCameraDirs(App *app)
{
	(void) (app);

	auto &cam = Renderer::instance().getActiveCamera();
	//auto &cam = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_TRANSFORM_BIT | GL_CURRENT_BIT);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glPointSize(4);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glLoadMatrixf(cam.getProjection());

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glLoadMatrixf(cam.getInverse());

	glBegin(GL_POINTS);

	// Top hemisphere, uniformly sampled.
	float currIndex = 0;
	Utils::seedRand(0);
	for (int slice = 0; slice < slices; slice++)
	{
		float r = Utils::getRand(0.0f, 1.0f);
		float g = Utils::getRand(0.0f, 1.0f);
		float b = Utils::getRand(0.0f, 1.0f);
		glColor3f(r, g, b);

		for (int stack = 0; stack <= stacks; stack++) // Need the bottom row.
		{
			float theta = ((float) slice / (float) slices) * 2.0f * (float) M_PI;
			float phi = ((float) stack / (float) stacks) * 0.5f * (float) M_PI;
			float x = cosf(theta) * sinf(phi);
			float z = sinf(theta) * sinf(phi);
			float y = cosf(phi);
			glVertex3f(x, y, z);
			//glVertex3f(-x, -y, -z); // For the bottom half.

			// Now calculate theta/phi from these values, make sure they're the same.
			// Then calculate the index based on that number.
			// Special case vector for (0, 1, 0) always gives t,p = 0,0 as others are irrelevant.
			// Note: If y is negative, negate the direction vector, then lookup.

			float p = acosf(y);
			float t = atan2f(z, x);
			if (t < 0)
				t += 2.0f * (float) M_PI;

			int st = (int) (round((p / (0.5 * M_PI)) * (float) stacks));
			int sl = (int) (round((t / (2.0 * M_PI)) * (float) slices));
			if (x != 0 && z != 0)
			{
				assert(sl == slice && st == stack);
				int index = sl * (stacks + 1) + st;
				assert(index == currIndex);
			}
			currIndex++;
		}
	}

	glEnd();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();

	// Draw geometry as well, to make sure it's correct?
	//app->getGpuMesh().render();
	//app->drawMesh(cam);
}

void DeepImg::drawCameraGeom(App *app)
{
	(void) (app);
	if (deepCams.empty())
		return;

	// Draw geometry from the direction of each camera. For making sure the cameras are actually correct.

	// TODO: Start by cycling through them, to make sure each individual direction is correct.
	static float totalT = 0;
	static int index = 0;

	totalT += Platform::Window::getDeltaTime();
	if (totalT > 5)
	{
		totalT = 0;
		index++;
		if (index >= (int) deepCams.size())
			index = 0;

		// Print the matching view direction.
		std::cout << "camera " << index << ": " << deepCams[index].getForward() << "\n";
	}

	// Can either draw the voxels or the triangle mesh.
	auto &camera = deepCams[index];

#if USE_VOXELS
	app->setTmpViewport(DEEP_RES, DEEP_RES);
	voxelGrid.debugRender(camera, -1, VOXEL_LIST == 1);
	app->restoreViewport();

	//app->drawMesh(camera);
#else
	app->drawMesh(camera);
#endif

	//app->drawMesh(Renderer::instance().getActiveCamera());

	// TODO: Does this match the expected direction?
}

void DeepImg::buildVoxelGrid(App *app)
{
#if CAPTURE_ONCE
	if (captureVoxelFlag)
		return;
	captureVoxelFlag = true;
#endif

	app->setTmpViewport(DEEP_RES, DEEP_RES);

	voxelGrid.beginCapture();

	// Render the scene using the same projection as the CDI.
	buildVoxelShader.bind();
	buildVoxelShader.setUniform("mvMatrix", deepCamZ.getInverse());
	buildVoxelShader.setUniform("pMatrix", deepCamZ.getProjection());
	voxelGrid.setUniforms(&buildVoxelShader);
	app->getGpuMesh().render();
	buildVoxelShader.unbind();

	voxelGrid.endCapture(MULTI_CDI == 1);

	app->restoreViewport();

#if VOXEL_LIST
	// Now build the global voxel list.
	voxelGrid.buildVoxelList();
#endif
}

void DeepImg::drawVoxelGrid(App *app)
{
	(void) (app);

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_POLYGON_BIT);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	voxelGrid.debugRender(camera, -1, VOXEL_LIST == 1);

	glPopAttrib();

	// For checking to make sure it's correct.
	//app->drawMesh(camera);
}

void DeepImg::drawRays(App *app)
{
	// TODO: Test the random rays to make sure those are correct.

	auto &camera = Renderer::instance().getActiveCamera();

	static Math::vec3 lightPos;
	static StorageBuffer debugRaysBuffer;
	static int nRays;

	// Create storage buffer with the debug ray dirs (leave the start point as the mainLightPos).
	if (lightPos != app->getLightPos())
	{
		lightPos = app->getLightPos();

		std::vector<Math::vec4> rayDirs(6);

		// TODO: Choose random pixels to spawn rays at, or choose random directions from a light source.
		// TODO: Random pixels to spawn rays at require a self-index.
		// Random directions from a light source require no self-index.
	#if 1
		rayDirs[0] = Math::vec4(0, 1, 0, 0);
		rayDirs[1] = Math::vec4(0.707107, 0.707107, 0, 0);
		rayDirs[2] = Math::vec4(0.218509, 0.707107, 0.672498, 0);
		rayDirs[3] = Math::vec4(-0.57206, 0.707107, 0.415628, 0);
		rayDirs[4] = Math::vec4(-0.572063, 0.707107, -0.415625, 0);
		rayDirs[5] = Math::vec4(0.218505, 0.707107, -0.672499, 0);
	#else
		rayDirs[0] = Math::vec4(0, 1, 0, 0);
		rayDirs[1] = Math::vec4(0, 0.5, 0.866025, 0);
		rayDirs[2] = Math::vec4(0.823639, 0.5, 0.267617, 0);
		rayDirs[3] = Math::vec4(0.509037, 0.5, -0.700629, 0);
		rayDirs[4] = Math::vec4(-0.509037, 0.5, -0.700629, 0);
		rayDirs[5] = Math::vec4(-0.823639, 0.5, 0.267617, 0);
		// A bunch of w vals: pi/4, 3pi/20, 3pi/20, 3pi/20, 3pi/20, 3pi/20
	#endif

		//vec3 b3 = normalize(osNorm);
		//vec3 different = abs(b3.x) < 0.5 ? vec3(1, 0, 0) : vec3(0, 1, 0);
		//vec3 b1 = normalize(cross(b3, different));
		//vec3 b2 = normalize(cross(b1, b3));

	#if 1
		// TODO: Can't I just use the lightDir as the basis vector?
		auto lightDir = app->getLightDir();
		lightDir.normalize();
		Math::vec3 up = lightDir.y * lightDir.y > 0.95f ? Math::vec3(0, 0, 1) : Math::vec3(0, 1, 0);
		Math::vec3 right = lightDir.cross(up);
		up = lightDir.cross(right);
		right.normalize();
		up.normalize();

		rayDirs[0] = Math::vec4(lightDir.x, lightDir.y, lightDir.z, 0);
		for (int i = 1; i < 6; i++)
		{
			Math::vec3 rayDir = lightDir.xyz;
			rayDir.x += (rayDirs[i].x * right + rayDirs[i].z * up).x;
			rayDir.y += (rayDirs[i].x * right + rayDirs[i].z * up).y;
			rayDir.z += (rayDirs[i].x * right + rayDirs[i].z * up).z;
			rayDir.normalize();
			rayDirs[i] = Math::vec4(rayDir.x, rayDir.y, rayDir.z, 0);
		}
	#else
		auto lightDir = app->getLightDir();
		lightDir.normalize();

		Math::vec3 lightZ = lightDir;
		Math::vec3 lightY = Math::vec3(0, 1, 0);
		Math::vec3 lightX = lightZ.cross(lightY);
		lightY = lightZ.cross(lightX);
		lightX.normalize();
		lightY.normalize();
		lightZ.normalize();

		rayDirs[0] = Math::vec4(lightDir.x, lightDir.y, lightDir.z, 0);
		for (int i = 1; i < 6; i++)
		{
			Math::vec3 rx = rayDirs[i].x * lightX;
			Math::vec3 ry = rayDirs[i].y * lightY;
			Math::vec3 rz = rayDirs[i].z * lightZ;
			rayDirs[i].x = rx.x + ry.x + rz.x;
			rayDirs[i].y = rx.y + ry.y + rz.y;
			rayDirs[i].z = rx.z + ry.z + rz.z;
			rayDirs[i].w = 0;
			rayDirs[i].normalize();
		}
	#endif

	#if 0
		auto lightDir = app->getLightDir();
		lightDir.normalize();
		Math::vec3 b3 = lightDir;
		Math::vec3 different = fabsf(b3.x) < 0.5f ? Math::vec3(1, 0, 0) : Math::vec3(0, 1, 0);
		Math::vec3 b1 = b3.cross(different);
		b1.normalize();
		Math::vec3 b2 = b1.cross(b3);
		b2.normalize();

		rayDirs[0] = Math::vec4(lightDir.x, lightDir.y, lightDir.z, 0);
		for (int i = 0; i < 6; i++)
		{
			//Math::vec3 rayDir = lightDir.xyz;
			//sampleDir = randX * b1 + randY * b2 + randZ * b3;
			//sampleDir = normalize(sampleDir);
			//rayDir += rayDirs[i].xyz * right + rayDirs[i].z * up;
			Math::vec3 rayDir = rayDirs[i].x * -b1 + rayDirs[i].y * -b2 + rayDirs[i].z * -b3;
			rayDir.normalize();
			rayDirs[i] = Math::vec4(rayDir.x, rayDir.y, rayDir.z, 0);
		}
	#endif

		debugRaysBuffer.release();
		debugRaysBuffer.create(&rayDirs[0], rayDirs.size() * sizeof(Math::vec4));
		nRays = (int) rayDirs.size();

		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	// Draw the geometry first.
	basicShader.bind();
	basicShader.setUniform("mvMatrix", camera.getInverse());
	basicShader.setUniform("pMatrix", camera.getProjection());
	basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	basicShader.setUniform("viewport", camera.getViewport());
	app->getGpuMesh().render();
	basicShader.unbind();

	//glDisable(GL_DEPTH_TEST);

	glLineWidth(3);
	debugRaysShader.bind();

	lfb.setUniforms(&debugRaysShader);

	// Deep Image stuff for raytracing.
	debugRaysShader.setUniform("stacks", stacks);
	debugRaysShader.setUniform("slices", slices);
	debugRaysShader.setUniform("DeepCams", &deepCamBuffer);
	debugRaysShader.setUniform("InvDeepCams", &invDeepCamBuffer);
	debugRaysShader.setUniform("deepCamProj", deepCamZ.getProjection());
	debugRaysShader.setUniform("invDeepCamProj", deepCamZ.getInverseProj());

	// General rendering stuff.
	debugRaysShader.setUniform("mainLightPos", app->getLightPos());
	//debugRaysShader.setUniform("mainLightDir", app->getLightDir());
	debugRaysShader.setUniform("mvMatrix", camera.getInverse());
	debugRaysShader.setUniform("pMatrix", camera.getProjection());
	//debugRaysShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	debugRaysShader.setUniform("viewport", camera.getViewport());
	debugRaysShader.setUniform("invPMatrix", camera.getInverseProj());
	debugRaysShader.setUniform("invMvMatrix", camera.getTransform());

	// Geometry data.
	debugRaysShader.setUniform("nIndices", app->getNIndices());
	debugRaysShader.setUniform("IndexData", &app->getIndexData());
	debugRaysShader.setUniform("VertexData", &app->getVertexData());

	// Debug rays.
	debugRaysShader.setUniform("DebugRays", &debugRaysBuffer);
	glDrawArrays(GL_POINTS, 0, nRays);

	debugRaysShader.unbind();
}

void DeepImg::captureDeepImages(App *app)
{
#if CAPTURE_ONCE
	if (captureDeepFlag)
		return;
	captureDeepFlag = true;
#endif

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

#if CONSERVATIVE && !RASTER_CUSTOM
	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	app->getProfiler().start("LFB create");

	app->setTmpViewport(DEEP_RES, DEEP_RES);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// One lfb per camera, arranged vertically or tiled.
#if BASIC_INDEXING
	lfb.resize(DEEP_RES, DEEP_RES * (int) deepCams.size());
#else
	lfb.resize(deepCols * DEEP_RES, deepRows * DEEP_RES);
#endif

	if (lfb.getType() != LFB::LINK_LIST)
	{
		// Count the number of frags for capture.
		lfb.beginCountFrags();
		countLfbShader.bind();
		lfb.setCountUniforms(&countLfbShader);
		countLfbShader.setUniform("mvMatrix", deepCamZ.getInverse());
		countLfbShader.setUniform("pMatrix", deepCamZ.getProjection());
		countLfbShader.setUniform("DeepCams", &deepCamBuffer);
	#if RASTER_CUSTOM
		glEnable(GL_RASTERIZER_DISCARD);
	#endif
		app->getGpuMesh().render(false);
	#if RASTER_CUSTOM
		glDisable(GL_RASTERIZER_DISCARD);
	#endif

		countLfbShader.unbind();

		lfb.endCountFrags();
	}

	app->getProfiler().start("Capture");

	lfb.beginCapture();

	captureLfbShader.bind();
	lfb.setUniforms(&captureLfbShader);
	captureLfbShader.setUniform("mvMatrix", deepCamZ.getInverse());
	captureLfbShader.setUniform("pMatrix", deepCamZ.getProjection());
	captureLfbShader.setUniform("DeepCams", &deepCamBuffer);
#if RASTER_CUSTOM
	glEnable(GL_RASTERIZER_DISCARD);
#endif
#if DRAW_DEEPS || CAPTURE_COLOR
	app->getGpuMesh().render();
#else
	app->getGpuMesh().render(false);
#endif
#if RASTER_CUSTOM
	glDisable(GL_RASTERIZER_DISCARD);
#endif

	captureLfbShader.unbind();

	if (lfb.endCapture())
	{
		lfb.beginCapture();

		captureLfbShader.bind();
		lfb.setUniforms(&captureLfbShader);
		captureLfbShader.setUniform("mvMatrix", deepCamZ.getInverse());
		captureLfbShader.setUniform("pMatrix", deepCamZ.getProjection());
		captureLfbShader.setUniform("DeepCams", &deepCamBuffer);
	#if RASTER_CUSTOM
		glEnable(GL_RASTERIZER_DISCARD);
	#endif
	#if DRAW_DEEPS || CAPTURE_COLOR
		app->getGpuMesh().render();
	#else
		app->getGpuMesh().render(false);
	#endif
	#if RASTER_CUSTOM
		glDisable(GL_RASTERIZER_DISCARD);
	#endif

		captureLfbShader.unbind();

		if (lfb.endCapture())
			std::cout << "Something really bad happened.\n";
	}

	app->getProfiler().time("Capture");

	app->restoreViewport();

#if CONSERVATIVE && !RASTER_CUSTOM
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
#endif

	app->getProfiler().time("LFB create");

#if 0
	// Do the prefix sums make sense?
	// Note: the sort probably assumes prefix sums have been incremented during a capture pass.
	unsigned int *testData = (unsigned int*) lfbTo->getOffsets()->read();
	int c = 0;
	for (int i = 0; i < 2000; i++)
	{
		if (testData[i] != 0)
		{
			std::cout << testData[i] << " ";
			c++;
		}
		if (c >= 30)
			break;
	}
	std::cout << "\n";
#endif

#if SORT_DEEP
	// Now sort.
	app->getProfiler().start("LFB sort");

	sortFbo.bind();
	#if BASIC_INDEXING
		app->setTmpViewport(DEEP_RES, DEEP_RES * (int) deepCams.size());
	#else
		app->setTmpViewport(deepCols * DEEP_RES, deepRows * DEEP_RES);
	#endif
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	bma.countIntervals(&lfb);
	bma.createMask(&lfb);
#if 0
	bma.sort(&lfb);
#else
	auto &intervals = bma.getIntervals();
	auto &intervalCounts = bma.getIntervalCounts();

	glDisable(GL_STENCIL_TEST);

	for (int i = (int) intervals.size() - 1; i >= SHUFFLE_MIN; i--)
	{
		if (intervalCounts[i].count <= 0)
			continue;

		intervals[i].shader->bind();
		lfb.setBmaUniforms(intervals[i].shader);
		intervals[i].shader->setUniform("IntervalPixels", intervalCounts[i].pixels);
		glDispatchCompute((GLuint) intervalCounts[i].count, 1, 1);
		intervals[i].shader->unbind();
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	int count = std::min(SHUFFLE_MIN - 1, 6);
	if ((int) intervals.size() > count)
	{
		for (int i = count; i >= 0; i--)
		{
			glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
			intervals[i].shader->bind();
			lfb.setBmaUniforms(intervals[i].shader);
			Rendering::Renderer::instance().drawQuad();
			intervals[i].shader->unbind();
		}
	}

	glDisable(GL_STENCIL_TEST);
#endif

	app->restoreViewport();
	sortFbo.unbind();

	app->getProfiler().time("LFB sort");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
#endif
	glPopAttrib();
}

void DeepImg::buildDeepImagesFromVoxels(App *app)
{
#if MULTI_CDI
	int lvlDim = 0, nCams = 0;
#endif
	unsigned int startIndex = 0; (void) (startIndex);

	// FIXME: Past Jesse -> This function is causing some pretty serious bugs. Not sure why...
	// FIXME: Present Jesse -> Are you sure? I can't see any bugs.
#if CAPTURE_ONCE
	if (captureDeepFlag)
		return;
	captureDeepFlag = true;
#endif

	if (lfb.getType() == LFB::LINK_LIST)
	{
		std::cout << "You can't use a linked list for a CDI, it just doesn't work.\n";
		return;
	}

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
	glDisable(GL_DEPTH_TEST);

	app->getProfiler().start("LFB create");

	app->setTmpViewport(DEEP_RES, DEEP_RES);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// One lfb per camera, arranged vertically or tiled.
#if BASIC_INDEXING
	lfb.resize(DEEP_RES, DEEP_RES * (int) deepCams.size());
	#if MULTI_CDI
		lvlDim = DEEP_RES / 2;
		nCams = (int) deepCams.size();// / 2; // Should we halve the cams at each level or not?
		for (int i = 0; i < 4; i++)
		{
			upperLfb[i].resize(lvlDim, lvlDim * nCams);
			lvlDim /= 2;
			//nCams /= 2;
		}
	#endif
#else
	lfb.resize(deepCols * DEEP_RES, deepRows * DEEP_RES);
	#if MULTI_CDI
		lvlDim = DEEP_RES / 2;
		for (int i = 0; i < 4; i++)
		{
			upperLfb[i].resize(upperCols[i] * lvlDim, upperRows[i] * lvlDim);
			//std::cout << "i, cols, rows: " << i << ", " << upperCols[i] << ", " << upperRows[i] << "\n";
			lvlDim /= 2;
		}
	#endif
#endif

	// Count the number of frags for capture.
	lfb.beginCountFrags();
	countLfbVoxelShader.bind();
	lfb.setCountUniforms(&countLfbVoxelShader);
	countLfbVoxelShader.setUniform("lvlDim", 0);
	countLfbVoxelShader.setUniform("nCams", 0);
	countLfbVoxelShader.setUniform("nCols", 0);
	countLfbVoxelShader.setUniform("nRows", 0);
	countLfbVoxelShader.setUniform("DeepCams", &deepCamBuffer);
#if VOXEL_LIST
	countLfbVoxelShader.setUniform("startIndex", 0);
	countLfbVoxelShader.setUniform("VoxelList", &voxelGrid.getVoxelList());
#else
	startIndex = voxelGrid.getStartIndex(voxelGrid.getNLevels() - 1);
	countLfbVoxelShader.setUniform("startIndex", (int) startIndex);
	countLfbVoxelShader.setUniform("VoxelGrid", &voxelGrid.getVoxelGrid());
#endif
	countLfbVoxelShader.setUniform("pMatrix", deepCamZ.getProjection());

	glEnable(GL_RASTERIZER_DISCARD);
#if VOXEL_LIST
	glDrawArrays(GL_POINTS, 0, voxelGrid.getNListVoxels());
#else
	glDrawArrays(GL_POINTS, 0, DEEP_RES * DEEP_RES * DEEP_RES);
#endif
	glDisable(GL_RASTERIZER_DISCARD);

	countLfbVoxelShader.unbind();

	lfb.endCountFrags();

	// Need to account for different voxel levels in the upper cdi levels.
#if MULTI_CDI
	// We only want 4 levels, not all of them.
	lvlDim = DEEP_RES;
	nCams = N_DEEPS;
	for (int i = 0; i < 4; i++)
	{
		// nLevels - 1 is the highest grid resolution.
		int level = voxelGrid.getNLevels() - 1 - (i + 1);

		lvlDim /= 2;
		//nCams /= 2;

		// Should never be a linked list, but I'll leave it here anyway.
		if (upperLfb[i].getType() == LFB::LINK_LIST)
			continue;

		// Count the number of frags for capture. Can never be a list of voxels.
		upperLfb[i].beginCountFrags();
		countLfbVoxelShader.bind();
		upperLfb[i].setCountUniforms(&countLfbVoxelShader);
		countLfbVoxelShader.setUniform("lvlDim", lvlDim);
		countLfbVoxelShader.setUniform("nCams", nCams);
		countLfbVoxelShader.setUniform("nCols", upperCols[i]);
		countLfbVoxelShader.setUniform("nRows", upperRows[i]);
		countLfbVoxelShader.setUniform("startIndex", (int) voxelGrid.getStartIndex(level));
		countLfbVoxelShader.setUniform("DeepCams", &deepCamBuffer);
		countLfbVoxelShader.setUniform("VoxelGrid", &voxelGrid.getVoxelGrid());
		countLfbVoxelShader.setUniform("pMatrix", deepCamZ.getProjection());

		glEnable(GL_RASTERIZER_DISCARD);
		glDrawArrays(GL_POINTS, 0, lvlDim * lvlDim * lvlDim);
		glDisable(GL_RASTERIZER_DISCARD);

		countLfbVoxelShader.unbind();

		upperLfb[i].endCountFrags();
	}
#endif

	app->getProfiler().start("Capture");

	lfb.beginCapture();

	captureLfbVoxelShader.bind();
	lfb.setUniforms(&captureLfbVoxelShader);
	captureLfbVoxelShader.setUniform("lvlDim", 0);
	captureLfbVoxelShader.setUniform("nCams", 0);
	captureLfbVoxelShader.setUniform("nCols", 0);
	captureLfbVoxelShader.setUniform("nRows", 0);
	captureLfbVoxelShader.setUniform("startIndex", 0);
	captureLfbVoxelShader.setUniform("DeepCams", &deepCamBuffer);
#if VOXEL_LIST
	captureLfbVoxelShader.setUniform("startIndex", 0);
	captureLfbVoxelShader.setUniform("VoxelList", &voxelGrid.getVoxelList());
#else
	startIndex = voxelGrid.getStartIndex(voxelGrid.getNLevels() - 1);
	captureLfbVoxelShader.setUniform("startIndex", (int) startIndex);
	captureLfbVoxelShader.setUniform("VoxelGrid", &voxelGrid.getVoxelGrid());
#endif
	captureLfbVoxelShader.setUniform("pMatrix", deepCamZ.getProjection());

	glEnable(GL_RASTERIZER_DISCARD);
	// TODO: Need to account for different voxel levels.
#if VOXEL_LIST
	glDrawArrays(GL_POINTS, 0, voxelGrid.getNListVoxels());
#else
	glDrawArrays(GL_POINTS, 0, DEEP_RES * DEEP_RES * DEEP_RES);
#endif
	glDisable(GL_RASTERIZER_DISCARD);

	captureLfbVoxelShader.unbind();

	lfb.endCapture();

#if MULTI_CDI
	// We only want 4 levels, not all of them.
	lvlDim = DEEP_RES;
	nCams = N_DEEPS;
	for (int i = 0; i < 4; i++)
	{
		// nLevels - 1 is the highest grid resolution.
		int level = voxelGrid.getNLevels() - 1 - (i + 1);

		lvlDim /= 2;
		//nCams /= 2;

		// Should never be a linked list, but I'll leave it here anyway.
		if (upperLfb[i].getType() == LFB::LINK_LIST)
			continue;

		// Count the number of frags for capture. Can never be a list of voxels.
		upperLfb[i].beginCapture();
		captureLfbVoxelShader.bind();
		upperLfb[i].setUniforms(&captureLfbVoxelShader);
		captureLfbVoxelShader.setUniform("lvlDim", lvlDim);
		captureLfbVoxelShader.setUniform("nCams", nCams);
		captureLfbVoxelShader.setUniform("nCols", upperCols[i]);
		captureLfbVoxelShader.setUniform("nRows", upperRows[i]);
		captureLfbVoxelShader.setUniform("startIndex", (int) voxelGrid.getStartIndex(level));
		captureLfbVoxelShader.setUniform("DeepCams", &deepCamBuffer);
		captureLfbVoxelShader.setUniform("VoxelGrid", &voxelGrid.getVoxelGrid());
		captureLfbVoxelShader.setUniform("pMatrix", deepCamZ.getProjection());

		glEnable(GL_RASTERIZER_DISCARD);
		glDrawArrays(GL_POINTS, 0, lvlDim * lvlDim * lvlDim);
		glDisable(GL_RASTERIZER_DISCARD);

		captureLfbVoxelShader.unbind();

		upperLfb[i].endCapture();
	}
#endif

	app->getProfiler().time("Capture");

	app->restoreViewport();

	app->getProfiler().time("LFB create");

#if 0
	// Do the prefix sums make sense?
	// Note: the sort probably assumes prefix sums have been incremented during a capture pass.
	unsigned int *testData = (unsigned int*) lfbTo->getOffsets()->read();
	int c = 0;
	for (int i = 0; i < 2000; i++)
	{
		if (testData[i] != 0)
		{
			std::cout << testData[i] << " ";
			c++;
		}
		if (c >= 30)
			break;
	}
	std::cout << "\n";
#endif

	// TODO: How will I sort the other CDI images?

	// FIXME: I think there's something wrong with my atomic counters here... not sure exactly what though.
#if SORT_DEEP
	// Now sort.
	app->getProfiler().start("LFB sort");

	sortFbo.bind();
	#if BASIC_INDEXING
		app->setTmpViewport(DEEP_RES, DEEP_RES * (int) deepCams.size());
	#else
		app->setTmpViewport(deepCols * DEEP_RES, deepRows * DEEP_RES);
	#endif
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	bma.countIntervals(&lfb);
	bma.createMask(&lfb);
#if 0
	bma.sort(&lfb);
#else
	auto &intervals = bma.getIntervals();
	auto &intervalCounts = bma.getIntervalCounts();

	glDisable(GL_STENCIL_TEST);

	for (int i = (int) intervals.size() - 1; i >= SHUFFLE_MIN; i--)
	{
		if (intervalCounts[i].count <= 0)
			continue;

		intervals[i].shader->bind();
		lfb.setBmaUniforms(intervals[i].shader);
		intervals[i].shader->setUniform("IntervalPixels", intervalCounts[i].pixels);
		glDispatchCompute((GLuint) intervalCounts[i].count, 1, 1);
		intervals[i].shader->unbind();
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	int count = std::min(SHUFFLE_MIN - 1, 6);
	if ((int) intervals.size() > count)
	{
		for (int i = count; i >= 0; i--)
		{
			glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
			intervals[i].shader->bind();
			lfb.setBmaUniforms(intervals[i].shader);
			Rendering::Renderer::instance().drawQuad();
			intervals[i].shader->unbind();
		}
	}

	#if MULTI_CDI
		for (int lfbIdx = 0; lfbIdx < 4; lfbIdx++)
		{
			auto &lfb = upperLfb[lfbIdx];
			for (int i = (int) intervals.size() - 1; i >= SHUFFLE_MIN; i--)
			{
				if (intervalCounts[i].count <= 0)
					continue;

				intervals[i].shader->bind();
				lfb.setBmaUniforms(intervals[i].shader);
				intervals[i].shader->setUniform("IntervalPixels", intervalCounts[i].pixels);
				glDispatchCompute(intervalCounts[i].count, 1, 1);
				intervals[i].shader->unbind();
				glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
			}

			glEnable(GL_STENCIL_TEST);
			glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

			int count = std::min(SHUFFLE_MIN - 1, 6);
			if ((int) intervals.size() > count)
			{
				for (int i = count; i >= 0; i--)
				{
					glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
					intervals[i].shader->bind();
					lfb.setBmaUniforms(intervals[i].shader);
					Rendering::Renderer::instance().drawQuad();
					intervals[i].shader->unbind();
				}
			}
		}
	#endif

	glDisable(GL_STENCIL_TEST);
#endif

	app->restoreViewport();
	sortFbo.unbind();

	app->getProfiler().time("LFB sort");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
#endif
	glPopAttrib();
}

void DeepImg::captureRSM(App *app)
{
	// TODO: If we're actually doing shadows in the standard way, then I may want to implement some kind of shadow mapping.
	auto &camera = app->getLightCam();
	auto &shader = rsm.getCaptureShader();

	rsm.resize(RSM_RES, RSM_RES);

	// Needs to be rendered at lower res.
	app->setTmpViewport(RSM_RES, RSM_RES);

	rsm.beginCapture();

	shader.bind();
	shader.setUniform("lightPos", app->getLightPos());
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	app->getGpuMesh().render();
	shader.unbind();

	rsm.endCapture();

	app->restoreViewport();
}

void DeepImg::renderRSM(App *app)
{
	(void) (app);
	rsm.debugRenderPositions();
}

void DeepImg::captureShadows(App *app)
{
	auto &camera = app->getLightCam();
	auto &shader = shadowMap.getCaptureShader();

	shadowMap.resize(SHADOW_RES, SHADOW_RES);

	// Needs to be rendered at lower res.
	app->setTmpViewport(SHADOW_RES, SHADOW_RES);

	shadowMap.beginCapture();

	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	app->getGpuMesh().render();
	shader.unbind();

	shadowMap.endCapture();

	app->restoreViewport();
}

void DeepImg::renderShadows(App *app)
{
	// TODO: Render geometry as normal from the g-buffer, apply shadows to it.
	auto &camera = Renderer::instance().getActiveCamera();

	auto &lightCam = app->getLightCam();
	auto lightMat = lightCam.getInverse() * lightCam.getProjection();
	lightMat = lightMat * Math::getBias<float>();

	// Deferred rendering of the scene, applying shadows using the compound deep image.
	app->getProfiler().start("Shadows");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	gBuffer.bindTextures();
	shadowMap.bindTextures();

	drawShadowShader.bind();

	gBuffer.setUniforms(&drawShadowShader);
	shadowMap.setUniforms(&drawShadowShader);

	// General rendering stuff. Don't really need all of these.
	drawShadowShader.setUniform("mainLightPos", app->getLightPos());
	drawShadowShader.setUniform("mvMatrix", camera.getInverse());
	drawShadowShader.setUniform("pMatrix", camera.getProjection());
	drawShadowShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	drawShadowShader.setUniform("viewport", camera.getViewport());
	drawShadowShader.setUniform("invPMatrix", camera.getInverseProj());
	drawShadowShader.setUniform("invMvMatrix", camera.getTransform());
	drawShadowShader.setUniform("lightMatrix", lightMat);

	Renderer::instance().drawQuad();

	drawShadowShader.unbind();

	shadowMap.unbindTextures();
	gBuffer.unbindTextures();

#if 0
	tmpLfb.beginComposite();

	pathTraceShader.bind();
	gBuffer.bindTextures();
#if USE_RSM
	rsm.getLodTexture()->bind();
#endif
#if USE_SHADOW_MAP
	shadowMap.bindTextures();
#endif

	gBuffer.setUniforms(&pathTraceShader);
	tmpLfb.setUniforms(&pathTraceShader);

	// Deep Image stuff for path tracing.
	pathTraceShader.setUniform("stacks", stacks);
	pathTraceShader.setUniform("slices", slices);
	pathTraceShader.setUniform("DeepCams", &deepCamBuffer);
	pathTraceShader.setUniform("InvDeepCams", &invDeepCamBuffer);
	pathTraceShader.setUniform("deepCamProj", deepCamZ.getProjection());
	pathTraceShader.setUniform("invDeepCamProj", deepCamZ.getInverseProj());

	// General rendering stuff.
	pathTraceShader.setUniform("mainLightPos", app->getLightPos());
	pathTraceShader.setUniform("mvMatrix", camera.getInverse());
	pathTraceShader.setUniform("pMatrix", camera.getProjection());
	pathTraceShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	pathTraceShader.setUniform("viewport", camera.getViewport());
	pathTraceShader.setUniform("invPMatrix", camera.getInverseProj());
	pathTraceShader.setUniform("invMvMatrix", camera.getTransform());

#if USE_RSM
	// RSM data and light matrix, so we don't accumulate colour from unlit geometry.
	pathTraceShader.setUniform("rsmLodTex", rsm.getLodTexture());

	auto &lightCam = app->getLightCam();

	// Not sure if the bias is needed here or not.
	auto lightMat = lightCam.getInverse() * lightCam.getProjection();
	//lightMat = lightMat * Math::getBias<float>();

	pathTraceShader.setUniform("lightMat", lightMat);
#endif
#if USE_SHADOW_MAP
	// RSM data and light matrix, so we don't accumulate colour from unlit geometry.
	pathTraceShader.setUniform("shadowTex", shadowMap.getDepthTexture());

	auto &lightCam = app->getLightCam();

	// Not sure if the bias is needed here or not.
	auto lightMat = lightCam.getInverse() * lightCam.getProjection();
	lightMat = lightMat * Math::getBias<float>();

	pathTraceShader.setUniform("lightMat", lightMat);
#endif

	// Geometry data.
	pathTraceShader.setUniform("nIndices", app->getNIndices());
	pathTraceShader.setUniform("IndexData", &app->getIndexData());
	pathTraceShader.setUniform("VertexData", &app->getVertexData());

	Renderer::instance().drawQuad();

#if USE_RSM
	rsm.getLodTexture()->unbind();
#endif
#if USE_SHADOW_MAP
	shadowMap.unbindTextures();
#endif
	gBuffer.unbindTextures();

	pathTraceShader.unbind();

	tmpLfb.endComposite();
#endif
	app->getProfiler().time("Shadows");
}

void DeepImg::renderDeepImages(App *app)
{
	// TODO: Cycle through each deep image and print the direction.
	app->getProfiler().start("LFB draw");

	// Draw the contents of the deep images. So we can make sure they're correct.
#if BASIC_INDEXING
	app->setTmpViewport(DEEP_RES, DEEP_RES * (int) deepCams.size());
#else
	app->setTmpViewport(deepCols * DEEP_RES, deepRows * DEEP_RES);
#endif

	drawLfbShader.bind();
	//lfb.composite(&drawLfbShader);
	drawLfbShader.setUniform("pixelCheck", -1);

	// TODO: May want to draw other cdi levels if we've built them from voxels.
	//lfb.setUniforms(&drawLfbShader);
	upperLfb[0].setUniforms(&drawLfbShader);

	Renderer::instance().drawQuad();
	drawLfbShader.unbind();
	app->restoreViewport();

	app->getProfiler().time("LFB draw");
}

void DeepImg::renderDeepImage(App *app, int index, int pixel)
{
	(void) (app);

	drawLfbShader.bind();
	drawLfbShader.setUniform("deepIndex", index);
	drawLfbShader.setUniform("pixelCheck", pixel);
	lfb.setUniforms(&drawLfbShader);
	Renderer::instance().drawQuad();
	drawLfbShader.unbind();
}

void DeepImg::testDDA(App *app)
{
	// TODO: Are calculated depth ranges aproximately within a given epsilon of the expected depth ranges?
	// TODO: Need to read the actual CDI data and test against some random rays.
	// TODO: Then check if the reported collisions are the same as expected from a brute force test.
	// TODO: If they are different, how/why?
	// TODO: Finding this out may require a synthetic test scene.

	(void) (app);

	// Correct voxel traversal algorithm:
	// https://stackoverflow.com/questions/12367071/how-do-i-initialize-the-t-variables-in-a-fast-voxel-traversal-algorithm-for-ray

	Utils::seedRand(10);
	vec3 startPos = vec3(Utils::getRand(0, 256), Utils::getRand(0, 256), Utils::getRand(0.0f, 256.0f));
	vec3 endPos = vec3(Utils::getRand(0, 256), Utils::getRand(0, 256), Utils::getRand(0.0f, 256.0f));

	vec3 pos = startPos;

	#define sign(x) (x > 0 ? 1.0f : (x < 0 ? -1 : 0))
	#define frac0(x) (x - floorf(x))
	#define frac1(x) (1 - x + floorf(x))

	vec3 dd = vec3(endPos.x - startPos.x, endPos.y - startPos.y, endPos.z - startPos.z);
	vec3 s = vec3(sign(dd.x), sign(dd.y), sign(dd.z));
	vec3 td, tMax;

	if (dd.x != 0)
		td.x = fmin(s.x / dd.x, 10000000.0f);
	else
		td.x = 10000000.0f;
	if (dd.x > 0)
		tMax.x = td.x * frac1(startPos.x);
	else
		tMax.x = td.x * frac0(startPos.x);

	if (dd.y != 0)
		td.y = fmin(s.y / dd.y, 10000000.0f);
	else
		td.y = 10000000.0f;
	if (dd.y > 0)
		tMax.y = td.y * frac1(startPos.y);
	else
		tMax.y = td.y * frac0(startPos.y);

	if (dd.z != 0)
		td.z = fmin(s.z / dd.z, 10000000.0f);
	else
		td.z = 10000000.0f;
	if (dd.z > 0)
		tMax.z = td.z * frac1(startPos.z);
	else
		tMax.z = td.z * frac0(startPos.z);

	#undef sign
	#undef frac0
	#undef frac1

	// Option 1: Traverse 2d pixel grid and find depth ranges for each pixel.
	// Option 2: Traverse 3d grid, treating depth ranges as discretized 3d cells.
	auto rangeHits = rangeTraverse(pos, tMax, td, s);
	auto gridHits = gridTraverse(pos, tMax, td, s);

#if 1
	// Comparing given vs expected depth ranges for given pixels.
	std::vector<vec2> exHits(rangeHits.size());
	vec2 expected = vec2(gridHits[0].z, gridHits[0].z);
	size_t curr = 0;
	for (size_t i = 1; i < gridHits.size(); i++)
	{
		expected.y = gridHits[i].z;
		if (gridHits[i].xy != gridHits[i - 1].xy)
		{
			exHits[curr++] = expected;
			expected.x = gridHits[i].z;
		}
	}
	exHits[curr] = expected;
	
	bool failFlag = false;
	for (size_t i = 0; i < rangeHits.size(); i++)
	{
		vec2 r = vec2(std::min(rangeHits[i].z, rangeHits[i].w), std::max(rangeHits[i].z, rangeHits[i].w));
		vec2 e = vec2(std::min(exHits[i].x, exHits[i].y), std::max(exHits[i].x, exHits[i].y));
		if (r.x < e.x - 1 || r.y > e.y + 1)
			failFlag = true;
		//std::cout << rangeHits[i].xy << ": " << rangeHits[i].z << "," << rangeHits[i].w << "; ";
		//std::cout << exHits[i] << "\n";
	}
	if (failFlag)
		std::cout << "Approximation failed!\n";
	else
		std::cout << "Approximation passed!\n";
#else
	// In case you want to print the grid hits as well.
	size_t n = gridHits.size();
	size_t curr = 0;
	vec2 expected = vec2(gridHits[0].z, gridHits[0].z);
	for (size_t i = 0; i < n; i++)
	{
		if (i > 0)
		{
			expected.y = gridHits[i].z;
			if (gridHits[i].xy != gridHits[i - 1].xy)
				expected.x = gridHits[i].z;
		}

		std::cout << gridHits[i] << "; ";
		
		if (rangeHits[curr].xy != gridHits[i].xy && curr < n - 1)
			curr++;
		std::cout << rangeHits[curr].xy << ": " << rangeHits[curr].z << "," << rangeHits[curr].w << "; ";
		std::cout << expected << "\n";
	}
#endif

	std::cout << "\n";
	exit(0);
}

void DeepImg::testDuplicates(App *app)
{
	(void) (app);

	unsigned int totalVals = 0;
	unsigned int totalUniques = 0;

	// Grab offsets and fragment data.
	auto *lfbTmp = (LFBLinear*) lfb.getBase();

	auto *offsets = (unsigned int*) lfbTmp->getOffsets()->read();
	auto *data = (float*) lfbTmp->getData()->read(); // Note: These are actually uints converted to floats.

	// For each list, calc no. of unique vals and total no. of vals.
	for (unsigned int pixel = 0; pixel < lfb.getWidth() * lfb.getHeight(); pixel++)
	{
		unsigned int node = pixel > 0 ? offsets[pixel - 1] : 0;
		unsigned int count = offsets[pixel] - (pixel > 0 ? offsets[pixel - 1] : 0);

		if (count == 0)
			continue;

		std::set<unsigned int> uniques;
		for (unsigned int i = node; i < node + count; i++)
		{
			unsigned int id = 0;
			memcpy(&id, &data[i], sizeof(id));
			uniques.insert(id);
		}

		// Compute running total of both.
		totalVals += count;
		totalUniques += (unsigned int) uniques.size();
	}

	std::cout << "Total vals: " << totalVals << ", total uniques: " << totalUniques << ", percent unique: " << ((float) totalUniques / (float) totalVals) * 100.0 << "\n";
	exit(0);
}

void DeepImg::buildRayMask(App *app)
{
	// Figure out what direction each pixel in the g-buffer needs.
	// How many unique directions are there?
	// Can we get away with reducing the number of directions? If so, how?
	auto &camera = Renderer::instance().getActiveCamera();

	if (!realDirShader.isGenerated())
	{
		auto vertSrc = ShaderSource().loadFromFile("shaders/deep/dirMask.vert");
		auto fragSrc = ShaderSource().loadFromFile("shaders/deep/dirMask.frag");
		realDirShader.create(&vertSrc, &fragSrc);
	}

	if (!realDirBuffer.isGenerated())
	{
		realDirBuffer.create(nullptr, camera.getWidth() * camera.getHeight() * sizeof(float) * 4);
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("Rays");

	realDirShader.bind();
	gBuffer.bindTextures();
	gBuffer.setUniforms(&realDirShader);

	// General rendering stuff.
	realDirShader.setUniform("mainLightPos", app->getLightPos());
	realDirShader.setUniform("mvMatrix", camera.getInverse());
	realDirShader.setUniform("pMatrix", camera.getProjection());
	realDirShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	realDirShader.setUniform("viewport", camera.getViewport());
	realDirShader.setUniform("invPMatrix", camera.getInverseProj());
	realDirShader.setUniform("invMvMatrix", camera.getTransform());

	realDirShader.setUniform("RealDirs", &realDirBuffer);
	//realDirShader.setUniform("mainLightPos", mainLightPos);

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();
	realDirShader.unbind();

	app->getProfiler().time("Rays");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Count the individual directions to a given precision.
	Math::vec4* realDirs = (Math::vec4*) realDirBuffer.read();

	// No easy way to compare two vec3 variables.
	auto comp = [](const Math::vec3 &a, const Math::vec3 &b) { return std::make_tuple(a.x, a.y, a.z) < std::make_tuple(b.x, b.y, b.z); };

	int precision = 1;
	std::map<Math::vec3, int, decltype(comp)> dirCounts(comp);

	for (int i = 0; i < camera.getWidth() * camera.getHeight(); i++)
	{
		if (realDirs[i].x == 0 && realDirs[i].y == 0 && realDirs[i].z == 0)
			continue;
		if (realDirs[i].y < 0)
			realDirs[i] = -realDirs[i];

		int ix = (int) (realDirs[i].x * (pow(10, precision)));
		int iy = (int) (realDirs[i].y * (pow(10, precision)));
		int iz = (int) (realDirs[i].z * (pow(10, precision)));
		float x = (float) ix / (float) pow(10, precision);
		float y = (float) iy / (float) pow(10, precision);
		float z = (float) iz / (float) pow(10, precision);

		dirCounts[Math::vec3(x, y, z)] = 0;
	}

	//for (auto it : dirCounts)
	//	std::cout << it.first << "\n";

	std::cout << "Total possible: " << camera.getWidth() * camera.getHeight() << "\n";
	std::cout << "Unique counts: " << dirCounts.size() << "\n";
	//exit(0);
}

void DeepImg::captureGeometry(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	auto &shader = gBuffer.getCaptureShader();

	app->getProfiler().start("Capture g-buffer");

	// Capture data into the g-buffer.
	gBuffer.beginCapture();
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	//gBuffer.setUniforms(&shader); // This is for using the g-buffer, not capturing it.
	app->getGpuMesh().render();

	shader.unbind();
	gBuffer.endCapture();

	app->getProfiler().time("Capture g-buffer");
}

void DeepImg::raytraceShadows(App *app)
{
	auto &tmpLfb = lfb;

	auto &camera = Renderer::instance().getActiveCamera();

	// Deferred rendering of the scene, applying shadows using the compound deep image.
	app->getProfiler().start("Shadows");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	tmpLfb.beginComposite();

	shadowShader.bind();
	gBuffer.bindTextures();
	gBuffer.setUniforms(&shadowShader);
	tmpLfb.setUniforms(&shadowShader);

	// Deep Image stuff for raytracing.
	shadowShader.setUniform("stacks", stacks);
	shadowShader.setUniform("slices", slices);
	shadowShader.setUniform("DeepCams", &deepCamBuffer);
	shadowShader.setUniform("InvDeepCams", &invDeepCamBuffer);
	shadowShader.setUniform("deepCamProj", deepCamZ.getProjection());
	shadowShader.setUniform("invDeepCamProj", deepCamZ.getInverseProj());

	// General rendering stuff.
	shadowShader.setUniform("mainLightPos", app->getLightPos());
	shadowShader.setUniform("mvMatrix", camera.getInverse());
	shadowShader.setUniform("pMatrix", camera.getProjection());
	shadowShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	shadowShader.setUniform("viewport", camera.getViewport());
	shadowShader.setUniform("invPMatrix", camera.getInverseProj());
	shadowShader.setUniform("invMvMatrix", camera.getTransform());

	// Geometry data.
	shadowShader.setUniform("nIndices", app->getNIndices());
	shadowShader.setUniform("IndexData", &app->getIndexData());
	shadowShader.setUniform("VertexData", &app->getVertexData());

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();

	shadowShader.unbind();

	tmpLfb.endComposite();

	app->getProfiler().time("Shadows");
}

void DeepImg::pathTrace(App *app)
{
	auto &tmpLfb = lfb;

	auto &camera = Renderer::instance().getActiveCamera();

	// Deferred rendering of the scene, applying shadows using the compound deep image.
	app->getProfiler().start("Path tracing");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	tmpLfb.beginComposite();

	pathTraceShader.bind();
	gBuffer.bindTextures();
#if USE_RSM
	rsm.getLodTexture()->bind();
#endif
#if USE_SHADOW_MAP
	shadowMap.bindTextures();
#endif

	gBuffer.setUniforms(&pathTraceShader);
	tmpLfb.setUniforms(&pathTraceShader);

	// Deep Image stuff for path tracing.
	pathTraceShader.setUniform("stacks", stacks);
	pathTraceShader.setUniform("slices", slices);
	pathTraceShader.setUniform("DeepCams", &deepCamBuffer);
	pathTraceShader.setUniform("InvDeepCams", &invDeepCamBuffer);
	pathTraceShader.setUniform("deepCamProj", deepCamZ.getProjection());
	pathTraceShader.setUniform("invDeepCamProj", deepCamZ.getInverseProj());

	// General rendering stuff.
	pathTraceShader.setUniform("mainLightPos", app->getLightPos());
	pathTraceShader.setUniform("mvMatrix", camera.getInverse());
	pathTraceShader.setUniform("pMatrix", camera.getProjection());
	pathTraceShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	pathTraceShader.setUniform("viewport", camera.getViewport());
	pathTraceShader.setUniform("invPMatrix", camera.getInverseProj());
	pathTraceShader.setUniform("invMvMatrix", camera.getTransform());

#if USE_RSM
	// RSM data and light matrix, so we don't accumulate colour from unlit geometry.
	pathTraceShader.setUniform("rsmLodTex", rsm.getLodTexture());

	auto &lightCam = app->getLightCam();

	// Not sure if the bias is needed here or not.
	auto lightMat = lightCam.getInverse() * lightCam.getProjection();
	//lightMat = lightMat * Math::getBias<float>();

	pathTraceShader.setUniform("lightMat", lightMat);
#endif
#if USE_SHADOW_MAP
	// RSM data and light matrix, so we don't accumulate colour from unlit geometry.
	pathTraceShader.setUniform("shadowTex", shadowMap.getDepthTexture());

	auto &lightCam = app->getLightCam();

	// Not sure if the bias is needed here or not.
	auto lightMat = lightCam.getInverse() * lightCam.getProjection();
	lightMat = lightMat * Math::getBias<float>();

	pathTraceShader.setUniform("lightMat", lightMat);
#endif

	// Geometry data.
	pathTraceShader.setUniform("nIndices", app->getNIndices());
	pathTraceShader.setUniform("IndexData", &app->getIndexData());
	pathTraceShader.setUniform("VertexData", &app->getVertexData());

	Renderer::instance().drawQuad();

#if USE_RSM
	rsm.getLodTexture()->unbind();
#endif
#if USE_SHADOW_MAP
	shadowMap.unbindTextures();
#endif
	gBuffer.unbindTextures();

	pathTraceShader.unbind();

	tmpLfb.endComposite();

	app->getProfiler().time("Path tracing");
}

void DeepImg::coneTrace(App *app)
{
	(void) (app);

	auto &camera = Renderer::instance().getActiveCamera();

	// Deferred rendering of the scene, applying shadows using the compound deep image.
	app->getProfiler().start("Cone tracing");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Primary hi-res CDI.
	lfb.beginComposite();

	// Lo-res CDIs.
	for (int i = 0; i < 4; i++)
		upperLfb[i].beginComposite();

	coneTraceShader.bind();
	gBuffer.bindTextures();
	gBuffer.setUniforms(&coneTraceShader);

	lfb.setUniforms(&coneTraceShader);
	for (int i = 0; i < 4; i++)
		upperLfb[i].setUniforms(&coneTraceShader, false, "upperLfb" + Utils::toString(i));

	// TODO: What are the stacks/slices for the other CDI levels? Can I just divide them or not?
	for (int i = 0; i < 4; i++)
	{
		coneTraceShader.setUniform("upperCols" + Utils::toString(i), upperCols[i]);
		coneTraceShader.setUniform("upperRows" + Utils::toString(i), upperRows[i]);
	}

	// Deep Image stuff for cone tracing.
	coneTraceShader.setUniform("stacks", stacks);
	coneTraceShader.setUniform("slices", slices);
	coneTraceShader.setUniform("DeepCams", &deepCamBuffer);
	coneTraceShader.setUniform("InvDeepCams", &invDeepCamBuffer);
	coneTraceShader.setUniform("deepCamProj", deepCamZ.getProjection());
	coneTraceShader.setUniform("invDeepCamProj", deepCamZ.getInverseProj());

	// General rendering stuff.
	coneTraceShader.setUniform("mainLightPos", app->getLightPos());
	coneTraceShader.setUniform("mvMatrix", camera.getInverse());
	coneTraceShader.setUniform("pMatrix", camera.getProjection());
	coneTraceShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	coneTraceShader.setUniform("viewport", camera.getViewport());
	coneTraceShader.setUniform("invPMatrix", camera.getInverseProj());
	coneTraceShader.setUniform("invMvMatrix", camera.getTransform());

	// Geometry data. Not necessary for cone tracing.
	//coneTraceShader.setUniform("nIndices", app->getNIndices());
	//coneTraceShader.setUniform("IndexData", &app->getIndexData());
	//coneTraceShader.setUniform("VertexData", &app->getVertexData());

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();

	coneTraceShader.unbind();

	for (int i = 0; i < 4; i++)
		upperLfb[i].endComposite();

	lfb.endComposite();

	app->getProfiler().time("Cone tracing");
}

void DeepImg::bruteForceShadows(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// Deferred rendering of the scene, applying shadows using the compound deep image.
	app->getProfiler().start("Shadows");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	bruteShadowShader.bind();

	gBuffer.bindTextures();
	gBuffer.setUniforms(&bruteShadowShader);

	// General rendering stuff.
	bruteShadowShader.setUniform("mainLightPos", app->getLightPos());
	bruteShadowShader.setUniform("mvMatrix", camera.getInverse());
	bruteShadowShader.setUniform("pMatrix", camera.getProjection());
	bruteShadowShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	bruteShadowShader.setUniform("viewport", camera.getViewport());
	bruteShadowShader.setUniform("invPMatrix", camera.getInverseProj());
	bruteShadowShader.setUniform("invMvMatrix", camera.getTransform());

	// Geometry data.
	bruteShadowShader.setUniform("nIndices", app->getNIndices());
	bruteShadowShader.setUniform("IndexData", &app->getIndexData());
	bruteShadowShader.setUniform("VertexData", &app->getVertexData());

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();

	bruteShadowShader.unbind();

	app->getProfiler().time("Shadows");
}


void DeepImg::init(App *app)
{
	(void) (app);

	// Good test scene: https://renderman.pixar.com/resources/RenderMan_20/images/figures.26/softshadfig1.gif
	//auto &camera = Renderer::instance().getActiveCamera();

#if 0
	// Atrium camera.
	camera.setPos({-0.0967992f, -2.74396f, -0.180925f});
	camera.setEulerRot({0.008727f, 3.10668f, 0.0f});
	camera.setZoom(5.9259f);
	camera.update(0.1f);
#endif

#if 0
	// Rungholt camera.
	camera.setPos({2.90805,3.25054,3.62739});
	camera.setEulerRot({-0.706858f, 0.733033f, 0.0f});
	camera.setZoom(0.0f);
	camera.update(0.1f);
#endif

	// Bunch of cameras for creating the directional deep images.
	createCameras();

	// How many deep image rows and columns do we need?
	int maxCols = std::max(DEEP_MAX_RES / DEEP_RES, 1);
	deepCols = std::min((int) deepCams.size(), maxCols);
	deepRows = (int) deepCams.size() / deepCols;
	if (deepRows * DEEP_RES > DEEP_MAX_RES)
		std::cout << "Warning: CDI exceeds maximum texture size.\n";

#if MULTI_CDI
	int lvlDim = DEEP_RES / 2;
	int lvlCams = (int) deepCams.size();// / 2;
	for (int i = 0; i < 4; i++)
	{
		maxCols = DEEP_MAX_RES / lvlDim;
		int lvlCols = std::min(lvlCams, maxCols);
		int lvlRows = lvlCams / lvlCols;
		upperCols[i] = lvlCols;
		upperRows[i] = lvlRows;

		lvlDim /= 2;
		//lvlCams /= 2;
	}
#endif

	// Render target for the sort image, since it *will* be larger than the window size.
	// TODO: Should I have different framebuffer objects for each cdi level?
#if BASIC_INDEXING
	sortFbo.setSize(DEEP_RES, DEEP_RES * deepCams.size());
#else
	sortFbo.setSize(deepCols * DEEP_RES, deepRows * DEEP_RES);
#endif
	sortFbo.create(GL_RGBA, &sortTex, &sortDepth, true);

	// Deep image stuff.
	lfb.release();
	lfb.setType(LFB::LINEARIZED, "lfb");
#if SINGLE_VAL
	lfb.setDataType(LFB::FLOAT);
#else
	lfb.setDataType(LFB::VEC2);
#endif
	lfb.setMaxFrags(512);
	lfb.setProfiler(&app->getProfiler());

#if MULTI_CDI
	for (int i = 0; i < 4; i++)
	{
		upperLfb[i].release();
		upperLfb[i].setType(LFB::LINEARIZED, "lfb");
		upperLfb[i].setDataType(LFB::VEC2);
		upperLfb[i].setMaxFrags(512);
		upperLfb[i].setProfiler(&app->getProfiler());
	}
#endif

	// Voxel stuff.
	voxelGrid.release();
	voxelGrid.resize(DEEP_RES);
	voxelGrid.setProfiler(&app->getProfiler());

#if CAPTURE_RSM
	// RSM data.
	rsm.setProfiler(&app->getProfiler());
	rsm.resize(RSM_RES, RSM_RES);
#endif

#if CAPTURE_RSM
	// Or a shadow map.
	shadowMap.setProfiler(&app->getProfiler());
	shadowMap.resize(SHADOW_RES, SHADOW_RES);
#endif
}

void DeepImg::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	gBuffer.resize(camera.getWidth(), camera.getHeight());

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

#if TEST_DDA
	testDDA(app);
	glPopAttrib();
	return;
#endif

#if DRAW_CAM_DIRS
	drawCameraDirs(app);
	glPopAttrib();
	return;
#endif

#if USE_VOXELS
	buildVoxelGrid(app);
#endif

#if DRAW_VOXELS
	drawVoxelGrid(app);
	glPopAttrib();
	return;
#endif

#if DRAW_CAM_GEOM
	drawCameraGeom(app);
	glPopAttrib();
	return;
#endif

#if CAPTURE_RSM
	captureRSM(app);
#endif

#if CAPTURE_SHADOW_MAP
	captureShadows(app);
#endif

#if RENDER_RSM
	renderRSM(app);
	glPopAttrib();
	return;
#endif

	captureGeometry(app);

#if RENDER_SHADOWS
	renderShadows(app);
	glPopAttrib();
	return;
#endif

#if RAY_MASK
	buildRayMask(app);
	glPopAttrib();
	return;
#endif

#if USE_VOXELS
	buildDeepImagesFromVoxels(app);
#else
	captureDeepImages(app);
#endif

#if TEST_DUPLICATES
	testDuplicates(app);
	glPopAttrib();
	return;
#endif

#if DRAW_DEEPS
	renderDeepImages(app);
	glPopAttrib();
	return;
#endif

#if TEST_RAY_DIRS
	drawRays(app);
	glPopAttrib();
	return;
#endif

#if CONE_TRACE
	coneTrace(app);
	glPopAttrib();
	return;
#endif

	//drawConeVectors(app);
#if PATH_TRACE
	pathTrace(app);
#else
	raytraceShadows(app);
#endif
	//bruteForceShadows(app);

	glPopAttrib();
}

void DeepImg::update(float dt)
{
	(void) (dt);
}

void DeepImg::useLighting(App *app)
{
	app->getProfiler().clear();

	gBuffer.setProfiler(&app->getProfiler());
	lfb.setProfiler(&app->getProfiler());

#if MULTI_CDI
	for (int i = 0; i < 4; i++)
		upperLfb[i].setProfiler(&app->getProfiler());
#endif

	reloadShaders(app);
}

void DeepImg::reloadShaders(App *app)
{
	(void) (app);

#if CAPTURE_ONCE
	captureDeepFlag = false;
	captureVoxelFlag = false;
#endif

#if CAPTURE_RSM
	rsm.release();
#endif

#if CAPTURE_SHADOW_MAP
	shadowMap.release();
#endif

	voxelGrid.releaseShaders();

	// Just basic rendering.
	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);

	// Building the voxel grid.
	auto buildVoxelVert = ShaderSourceCache::getShader("buildVoxelVert").loadFromFile("shaders/voxel/buildVoxels.vert");
	auto buildVoxelGeom = ShaderSourceCache::getShader("buildVoxelGeom").loadFromFile("shaders/voxel/buildVoxels.geom");
	auto buildVoxelFrag = ShaderSourceCache::getShader("buildVoxelFrag").loadFromFile("shaders/voxel/buildVoxels.frag");
	buildVoxelFrag.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	buildVoxelShader.release();
	buildVoxelShader.create(&buildVoxelVert, &buildVoxelFrag, &buildVoxelGeom);

	// Building the lfb from the voxel list/grid.
	auto countLfbVoxelVert = ShaderSourceCache::getShader("countLfbVoxelVert").loadFromFile("shaders/deep/lfbVoxels.vert");
	countLfbVoxelVert.setDefine("GET_COUNTS", "1");
	countLfbVoxelVert.setDefine("VOXEL_LIST", Utils::toString(VOXEL_LIST));
	countLfbVoxelVert.setDefine("USE_NDC", Utils::toString(USE_NDC));
	countLfbVoxelVert.setDefine("INDEX_BY_TILE", "0");
	#if SINGLE_VAL
		countLfbVoxelVert.setDefine("LFB_FRAG_TYPE", "float");
	#else
		countLfbVoxelVert.setDefine("LFB_FRAG_TYPE", "vec2");
	#endif
	countLfbVoxelVert.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	countLfbVoxelVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	countLfbVoxelVert.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	countLfbVoxelVert.setDefine("DEEP_COLS", Utils::toString(deepCols));
	countLfbVoxelVert.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	countLfbVoxelVert.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	countLfbVoxelVert.replace("LFB_NAME", "lfb");
	countLfbVoxelShader.release();
	countLfbVoxelShader.create(&countLfbVoxelVert);

	auto captureLfbVoxelVert = ShaderSourceCache::getShader("captureLfbVoxelVert").loadFromFile("shaders/deep/lfbVoxels.vert");
	captureLfbVoxelVert.setDefine("GET_COUNTS", "0");
	captureLfbVoxelVert.setDefine("VOXEL_LIST", Utils::toString(VOXEL_LIST));
	captureLfbVoxelVert.setDefine("USE_NDC", Utils::toString(USE_NDC));
	captureLfbVoxelVert.setDefine("INDEX_BY_TILE", "0");
	#if SINGLE_VAL
		captureLfbVoxelVert.setDefine("LFB_FRAG_TYPE", "float");
	#else
		captureLfbVoxelVert.setDefine("LFB_FRAG_TYPE", "vec2");
	#endif
	captureLfbVoxelVert.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	captureLfbVoxelVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	captureLfbVoxelVert.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	captureLfbVoxelVert.setDefine("DEEP_COLS", Utils::toString(deepCols));
	captureLfbVoxelVert.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	captureLfbVoxelVert.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	captureLfbVoxelVert.replace("LFB_NAME", "lfb");
	captureLfbVoxelShader.release();
	captureLfbVoxelShader.create(&captureLfbVoxelVert);

	// Capturing lfb data from rasterized geometry.
	auto vertCount = ShaderSourceCache::getShader("countVert").loadFromFile("shaders/deep/capture.vert");
	auto geomCount = ShaderSourceCache::getShader("countGeom").loadFromFile("shaders/deep/capture.geom");
	auto fragCount = ShaderSourceCache::getShader("countFrag").loadFromFile("shaders/deep/capture.frag");
	fragCount.setDefine("GET_COUNTS", "1");
	geomCount.setDefine("GET_COUNTS", "1");
	geomCount.setDefine("RAYCAST_SURFELS", Utils::toString(RAYCAST_SURFELS));
	fragCount.setDefine("RAYCAST_SURFELS", Utils::toString(RAYCAST_SURFELS));
	fragCount.setDefine("PATH_TRACE", Utils::toString(PATH_TRACE));
	geomCount.setDefine("RASTER_CUSTOM", Utils::toString(RASTER_CUSTOM));
	geomCount.setDefine("USE_NDC", Utils::toString(USE_NDC));
	fragCount.setDefine("USE_NDC", Utils::toString(USE_NDC));
	fragCount.setDefine("INDEX_BY_TILE", "0");
	geomCount.setDefine("INDEX_BY_TILE", "0");
	#if SINGLE_VAL
		fragCount.setDefine("LFB_FRAG_TYPE", "float");
		geomCount.setDefine("LFB_FRAG_TYPE", "float");
	#else
		fragCount.setDefine("LFB_FRAG_TYPE", "vec2");
		geomCount.setDefine("LFB_FRAG_TYPE", "vec2");
	#endif
	fragCount.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	geomCount.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	fragCount.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	geomCount.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	fragCount.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	geomCount.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	fragCount.setDefine("DEEP_COLS", Utils::toString(deepCols));
	geomCount.setDefine("DEEP_COLS", Utils::toString(deepCols));
	fragCount.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	geomCount.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	fragCount.setDefine("PATH_TRACE", Utils::toString(PATH_TRACE));
	fragCount.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	fragCount.replace("LFB_NAME", "lfb");
	geomCount.replace("LFB_NAME", "lfb");

	countLfbShader.release();
	countLfbShader.create(&vertCount, &fragCount, &geomCount);

	auto vertCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/deep/capture.vert");
	auto geomCapture = ShaderSourceCache::getShader("captureGeom").loadFromFile("shaders/deep/capture.geom");
	auto fragCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/deep/capture.frag");
	fragCapture.setDefine("GET_COUNTS", "0");
	geomCapture.setDefine("GET_COUNTS", "0");
	geomCapture.setDefine("RAYCAST_SURFELS", Utils::toString(RAYCAST_SURFELS));
	fragCapture.setDefine("RAYCAST_SURFELS", Utils::toString(RAYCAST_SURFELS));
	fragCapture.setDefine("PATH_TRACE", Utils::toString(PATH_TRACE));
	geomCapture.setDefine("RASTER_CUSTOM", Utils::toString(RASTER_CUSTOM));
	geomCapture.setDefine("USE_NDC", Utils::toString(USE_NDC));
	fragCapture.setDefine("USE_NDC", Utils::toString(USE_NDC));
	fragCapture.setDefine("INDEX_BY_TILE", "0");
	geomCapture.setDefine("INDEX_BY_TILE", "0");
	#if SINGLE_VAL
		fragCapture.setDefine("LFB_FRAG_TYPE", "float");
		geomCapture.setDefine("LFB_FRAG_TYPE", "float");
	#else
		fragCapture.setDefine("LFB_FRAG_TYPE", "vec2");
		geomCapture.setDefine("LFB_FRAG_TYPE", "vec2");
	#endif
	fragCapture.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	geomCapture.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	fragCapture.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	geomCapture.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	fragCapture.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	geomCapture.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	fragCapture.setDefine("DEEP_COLS", Utils::toString(deepCols));
	geomCapture.setDefine("DEEP_COLS", Utils::toString(deepCols));
	fragCapture.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	geomCapture.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	fragCapture.setDefine("DRAW_DEEPS", Utils::toString(DRAW_DEEPS || CAPTURE_COLOR));
	fragCapture.setDefine("PATH_TRACE", Utils::toString(PATH_TRACE));
	fragCapture.setDefine("BASIC_INDEXING", Utils::toString(BASIC_INDEXING));
	fragCapture.replace("LFB_NAME", "lfb");
	geomCapture.replace("LFB_NAME", "lfb");

	captureLfbShader.release();
	captureLfbShader.create(&vertCapture, &fragCapture, &geomCapture);

#if SORT_DEEP
	// Sorting lfb data.
	// TODO: What about sorting the upper lfb levels? Do I really need separate bma stuff for that? Ideally no.
	bma.release();
	bma.createMaskShader("lfb", "shaders/warpSort/bmaMask.vert", "shaders/warpSort/bmaMask.frag", {{"INDEX_BY_TILE", "0"}});
	bma.createShaders(&lfb, "shaders/basicSort/sort.vert", "shaders/basicSort/sort.frag", {{"COMPOSITE_LOCAL", "0"}, {"INDEX_BY_TILE", "0"},
		{"DISCARD_DUPS", Utils::toString(DISCARD_DUPS)},
	#if SINGLE_VAL
		{"LFB_FRAG_TYPE", "float"},
	#else
		{"LFB_FRAG_TYPE", "vec2"},
	#endif
		{"USE_FLOAT", "1"}, {"SINGLE_VAL", Utils::toString(SINGLE_VAL)}});

	bma.createCounts(&lfb, "shaders/warpSort/countIntervals.vert", "shaders/warpSort/countIntervals.frag", {{"INDEX_BY_TILE", "0"}});
	for (int i = SHUFFLE_MIN; i <= 6; i++)
		bma.setShader(i, &lfb, "shaders/warpSort/shuffleOddEven.comp", {
	#if SINGLE_VAL
		{"LFB_FRAG_TYPE", "float"},
	#else
		{"LFB_FRAG_TYPE", "vec2"},
	#endif
		{"USE_FLOAT", "1"}, {"SAVE_DEEP_DATA", "1"}});

	bma.setProfiler(&app->getProfiler());
#endif

	// Drawing the lfb, for debugging.
	auto vertLfb = ShaderSourceCache::getShader("drawLfbVert").loadFromFile("shaders/deep/drawLfb.vert");
	auto fragLfb = ShaderSourceCache::getShader("drawLfbFrag").loadFromFile("shaders/deep/drawLfb.frag");
	fragLfb.replace("LFB_NAME", "lfb");
	fragLfb.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
#if SINGLE_VAL
	fragLfb.setDefine("LFB_FRAG_TYPE", "float");
#else
	fragLfb.setDefine("LFB_FRAG_TYPE", "vec2");
#endif
	drawLfbShader.release();
	drawLfbShader.create(&vertLfb, &fragLfb);

	// Shadow raytracing shader using the compound deep image.
	auto shadowVert = ShaderSourceCache::getShader("shadowVert").loadFromFile("shaders/deep/shadow.vert");
	auto shadowFrag = ShaderSourceCache::getShader("shadowFrag").loadFromFile("shaders/deep/shadow.frag");
	shadowFrag.setDefine("RAYCAST_SURFELS", Utils::toString(RAYCAST_SURFELS));
	shadowFrag.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	shadowFrag.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	shadowFrag.setDefine("DEEP_COLS", Utils::toString(deepCols));
	shadowFrag.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	shadowFrag.setDefine("INDEX_BY_TILE", "0");
	#if SINGLE_VAL
		shadowFrag.setDefine("LFB_FRAG_TYPE", "float");
	#else
		shadowFrag.setDefine("LFB_FRAG_TYPE", "vec2");
	#endif
	shadowFrag.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	//shadowFrag.setDefine("RAYCAST_DEPTH_ONLY", Utils::toString(RAYCAST_DEPTH_ONLY));
	//shadowFrag.setDefine("USE_FLOAT", "1");
	shadowFrag.replace("LFB_NAME", "lfb");
	shadowShader.release();
	shadowShader.create(&shadowVert, &shadowFrag);

	// Path tracing shader using the compound deep image.
	auto pathTraceVert = ShaderSourceCache::getShader("pathTraceVert").loadFromFile("shaders/deep/pathTrace.vert");
	auto pathTraceFrag = ShaderSourceCache::getShader("pathTraceFrag").loadFromFile("shaders/deep/pathTrace.frag");
	pathTraceFrag.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	pathTraceFrag.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	pathTraceFrag.setDefine("DEEP_COLS", Utils::toString(deepCols));
	pathTraceFrag.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	pathTraceFrag.setDefine("USE_RSM", Utils::toString(USE_RSM));
	pathTraceFrag.setDefine("USE_SHADOW_MAP", Utils::toString(USE_SHADOW_MAP));
	pathTraceFrag.setDefine("INDEX_BY_TILE", "0");
	pathTraceFrag.setDefine("LFB_FRAG_TYPE", "vec2");
	pathTraceFrag.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	//pathTraceFrag.setDefine("RAYCAST_DEPTH_ONLY", Utils::toString(RAYCAST_DEPTH_ONLY));
	//pathTraceFrag.setDefine("USE_FLOAT", "1");
	pathTraceFrag.replace("LFB_NAME", "lfb");
	pathTraceShader.release();
	pathTraceShader.create(&pathTraceVert, &pathTraceFrag);

	// Cone tracing using a set of compound deep images.
	auto coneTraceVert = ShaderSourceCache::getShader("coneTraceVert").loadFromFile("shaders/deep/coneTrace.vert");
	auto coneTraceFrag = ShaderSourceCache::getShader("coneTraceFrag").loadFromFile("shaders/deep/coneTrace.frag");
	coneTraceFrag.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	coneTraceFrag.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	coneTraceFrag.setDefine("DEEP_COLS", Utils::toString(deepCols));
	coneTraceFrag.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	coneTraceFrag.setDefine("INDEX_BY_TILE", "0");
	coneTraceFrag.setDefine("LFB_FRAG_TYPE", "vec2");
	coneTraceFrag.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));
	//coneTraceFrag.setDefine("RAYCAST_DEPTH_ONLY", Utils::toString(RAYCAST_DEPTH_ONLY));
	//coneTraceFrag.setDefine("USE_FLOAT", "1");
	coneTraceFrag.replace("LFB_NAME", "lfb");
	coneTraceShader.release();
	coneTraceShader.create(&coneTraceVert, &coneTraceFrag);

	// Shadow raytracing shader using brute force.
	auto bruteShadowVert = ShaderSourceCache::getShader("bruteShadowVert").loadFromFile("shaders/bruteForce/shadow.vert");
	auto bruteShadowFrag = ShaderSourceCache::getShader("bruteShadowFrag").loadFromFile("shaders/bruteForce/shadow.frag");
	bruteShadowShader.release();
	bruteShadowShader.create(&bruteShadowVert, &bruteShadowFrag);

	// Raycast debugging.
	auto debugRayVert = ShaderSourceCache::getShader("debugRaysVert").loadFromFile("shaders/deep/debugRays.vert");
	auto debugRayGeom = ShaderSourceCache::getShader("debugRaysGeom").loadFromFile("shaders/deep/debugRays.geom");
	auto debugRayFrag = ShaderSourceCache::getShader("debugRaysFrag").loadFromFile("shaders/deep/debugRays.frag");
	debugRayGeom.setDefine("RAYCAST_SURFELS", Utils::toString(RAYCAST_SURFELS));
	debugRayGeom.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	debugRayGeom.setDefine("N_DEEPS", Utils::toString(deepCams.size()));
	debugRayGeom.setDefine("DEEP_COLS", Utils::toString(deepCols));
	debugRayGeom.setDefine("DEEP_ROWS", Utils::toString(deepRows));
	debugRayGeom.setDefine("INDEX_BY_TILE", "0");
	debugRayGeom.setDefine("USE_NDC", Utils::toString(USE_NDC));
	#if SINGLE_VAL
		debugRayGeom.setDefine("LFB_FRAG_TYPE", "float");
	#else
		debugRayGeom.setDefine("LFB_FRAG_TYPE", "vec2");
	#endif
	debugRayGeom.setDefine("SINGLE_VAL", Utils::toString(SINGLE_VAL));

	debugRaysShader.release();
	debugRaysShader.create(&debugRayVert, &debugRayFrag, &debugRayGeom);

	// Applying the shadow map to the g-buffer, for debugging.
	auto drawShadowVert = ShaderSourceCache::getShader("drawShadowVert").loadFromFile("shaders/deep/drawShadows.vert");
	auto drawShadowFrag = ShaderSourceCache::getShader("drawShadowFrag").loadFromFile("shaders/deep/drawShadows.frag");
	drawShadowShader.release();
	drawShadowShader.create(&drawShadowVert, &drawShadowFrag);

#if 0
	// Debugging light directions.
	auto debugDirVert = ShaderSourceCache::getShader("debugDirVert").loadFromFile("shaders/deep/debugDirs.vert");
	auto debugDirGeom = ShaderSourceCache::getShader("debugDirGeom").loadFromFile("shaders/deep/debugDirs.geom");
	auto debugDirFrag = ShaderSourceCache::getShader("debugDirFrag").loadFromFile("shaders/deep/debugDirs.frag");
	debugDirVert.setDefine("DEEP_RES", Utils::toString(DEEP_RES));
	debugDirShader.release();
	debugDirShader.create(&debugDirVert, &debugDirFrag, &debugDirGeom);
#endif
}

void DeepImg::saveData(App *app)
{
	(void) (app);

	//auto str = lfb.getStr();
	auto str = upperLfb[0].getStr();
	Utils::File f("data.txt");
	f.write(str);
}

