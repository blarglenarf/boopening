#pragma once

class App;

class Lighting
{
public:
	enum Type
	{
		STANDARD,
		DEEP_IMG,
		BVH
	};

public:
	Lighting() {}
	virtual ~Lighting() {}

	virtual void init(App *app) = 0;

	virtual void render(App *app) = 0;
	virtual void update(float dt) = 0;

	virtual void useLighting(App *app) = 0;
	virtual void useScene(App *app) { (void) (app); }

	virtual void reloadShaders(App *app) = 0;
	virtual void saveData(App *app) { (void) (app); }

	virtual void savePathTracedImg(App *app) { (void) (app); }
};

