#include "Standard.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

#undef near
#undef far

#define PATH_TRACE 1
#define N_BOUNCES 1
#define N_SAMPLES 5

using namespace Rendering;
using namespace Math;


struct TriangleTest
{
	vec3 v0, v1, v2, r0, r1;
	TriangleTest() {}
	TriangleTest(vec3 v0, vec3 v1, vec3 v2, vec3 r0, vec3 r1) : v0(v0), v1(v1), v2(v2), r0(r0), r1(r1) {}
};
std::vector<TriangleTest> triangleTests;
int currTestIndex = 0;
TriangleTest currTest;
vec3 testPos;


// FIXME: Seems to result in intersections not on the line.
static bool testTriangleRayIntersection(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 end, vec3 &pos)
{
	// If we don't normalise this, then we know t values > 1 will be past the end point.
	vec3 d = (end - o);

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	vec3 h = d.cross(e2);
	float a = e1.dot(h);

	if (a > -0.00001 && a < 0.0001)
	{
		return false;
	}

	float f = 1.0f / a;
	vec3 s = o - v0;
	float u = f * s.dot(h);

	if (u < 0.0 || u > 1.0)
	{
		return false;
	}

	vec3 q = s.cross(e1);
	float v = f * d.dot(q);

	if (v < 0.0 || u + v > 1.0)
	{
		return false;
	}

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * e2.dot(q);

	// Intersection.
	if (t > 0.00001 && t < 1)
	{
		pos = o + (d * t);
		return true;
	}

	// Line intersection but not ray intersection.
	else
	{
		return false;
	}

	return false;
}

bool checkGeometry(size_t index, vec3 o, vec3 d, Vertex *vertexData, unsigned int *indexData, vec3 &hitPos, float &t, float &u, float &v)
{
	vec3 v0 = vertexData[indexData[index * 3 + 0]].pos.xyz;
	vec3 v1 = vertexData[indexData[index * 3 + 1]].pos.xyz;
	vec3 v2 = vertexData[indexData[index * 3 + 2]].pos.xyz;

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.0001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	t = f * dot(e2, q);

	// Intersection.
	//if (t > 0.00001 && t < 1)
	if (t > 0.00001)
	{
		hitPos = o + (d * t);
		return true;
	}

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}


void Standard::drawMeshRay(App *app, vec3 r0, vec3 r1)
{
	(void) (r0); (void) (r1);
	glPushAttrib(GL_TRANSFORM_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT | GL_LINE_BIT | GL_POLYGON_BIT | GL_VIEWPORT_BIT);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	Renderer::instance().getActiveCamera().upload();

	// OK, Just render the whole atrium mesh and raytrace between a point and the light.
	app->drawMesh();
#if 1
	auto &mesh = app->getMesh();

	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	glVertex3fv(&r0.x);
	glVertex3fv(&r1.x);
	glEnd();

	vec3 p;
	for (size_t i = 0; i < mesh.getNumIndices(); i += 3)
	{
		auto v0 = mesh.getVertex(mesh.getIndex(i + 0));
		auto v1 = mesh.getVertex(mesh.getIndex(i + 1));
		auto v2 = mesh.getVertex(mesh.getIndex(i + 2));

		if (testTriangleRayIntersection(v0.pos, v1.pos, v2.pos, r0, r1, p))
		{
			glPointSize(4);
			glColor3f(1, 0, 0);
			glBegin(GL_POINTS);
			glVertex3fv(&p.x);
			glEnd();
		}
	}
#endif
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
}

void Standard::drawTriangleRay(vec3 v0, vec3 v1, vec3 v2, vec3 r0, vec3 r1)
{
	glPushAttrib(GL_TRANSFORM_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT | GL_LINE_BIT | GL_POLYGON_BIT | GL_VIEWPORT_BIT);

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	Renderer::instance().getActiveCamera().upload();

	glColor3f(0, 1, 0);
	glBegin(GL_TRIANGLES);
	glVertex3fv(&v0.x);
	glVertex3fv(&v1.x);
	glVertex3fv(&v2.x);
	glEnd();

	glPointSize(4);
	glColor3f(0, 0, 1);
	glBegin(GL_POINTS);
	glVertex3fv(&r0.x);
	glEnd();

	glColor3f(0, 0, 1);
	glBegin(GL_LINES);
	glVertex3fv(&r0.x);
	glVertex3fv(&r1.x);
	glEnd();

	Math::vec3 p;
	if (testTriangleRayIntersection(v0, v1, v2, r0, r1, p))
	{
		glPointSize(4);
		glColor3f(1, 0, 0);
		glBegin(GL_POINTS);
		glVertex3fv(&p.x);
		glEnd();
	}

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopAttrib();
}

void Standard::selectTriangleTest(int test)
{
	if (triangleTests.empty())
	{
		triangleTests.push_back({{-2.70431,0.134009,9.04459}, {8.3239,1.35712,4.34594}, {-7.16795,1.06969,-9.67399}, {-3.29554,2.6823,-4.44451}, {1.0794,-0.226029,2.57742}}); // Yes.
		triangleTests.push_back({{-7.82382,4.98925,-5.63486}, {0.258648,3.39112,2.2528}, {-4.07937,1.37552,0.485744}, {-5.14226,-3.62768,6.08354}, {-6.86642,-0.990556,-7.40419}}); // No.
		triangleTests.push_back({{-1.99543,3.91529,-4.3337}, {-2.95083,3.07725,8.38053}, {-8.60489,4.49327,0.519907}, {-0.12834,4.72775,-4.14966}, {5.42715,0.26745,5.39828}}); // No.
		triangleTests.push_back({{-9.59954,-0.422983,-8.73808}, {-5.2344,4.70634,8.04416}, {7.0184,-2.33334,0.795207}, {-8.27888,-3.07786,3.26454}, {7.80465,-1.51107,-8.71657}}); // No.
		triangleTests.push_back({{-1.24725,4.31835,8.6162}, {4.41905,-2.15707,4.77069}, {2.79958,-1.45951,3.75723}, {-2.49586,2.60249,0.250707}, {3.35448,0.316064,-9.21439}}); // No.
		triangleTests.push_back({{7.86745,-1.4964,3.7334}, {9.12936,0.886401,3.14608}, {7.17353,-0.604401,8.4794}, {-6.68052,-0.598955,7.6015}, {6.58402,-1.69663,-5.42064}}); // No.
		triangleTests.push_back({{9.00505,4.20128,-7.0468}, {7.62124,1.41081,-1.36093}, {2.39193,-2.18941,5.72004}, {-2.03127,3.14767,3.68437}, {8.21944,-0.175093,-5.6835}}); // No.
		triangleTests.push_back({{-1.66997,-3.30393,8.13608}, {-7.93658,-3.73925,-0.0911187}, {5.2095,4.84752,8.70008}, {-3.85084,-0.529664,-5.47787}, {-6.24934,-2.23765,1.12888}}); // No.
		triangleTests.push_back({{1.68977,-2.55587,-6.9522}, {4.64297,-3.74525,5.86941}, {-6.71796,2.45071,-8.5094}, {3.6889,-1.16812,4.99542}, {-2.62673,-2.0584,-5.35477}}); // Yes.
		triangleTests.push_back({{4.65309,1.56564,9.3481}, {2.78917,2.59735,-8.13039}, {-7.30195,0.202101,-8.43536}, {9.00208,-4.47471,0.431268}, {-6.47579,-2.59938,5.95596}}); // No.
	}

	currTestIndex = test;
	if (currTestIndex >= (int) triangleTests.size())
		currTestIndex = 0;
	if (currTestIndex < 0)
		currTestIndex = (int) triangleTests.size() - 1;
	currTest = triangleTests[currTestIndex];
	testPos = triangleTests[currTestIndex].r0;
}

void Standard::captureGeometry(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	auto &shader = gBuffer.getCaptureShader();

	app->getProfiler().start("Capture g-buffer");

	// Capture data into the g-buffer.
	gBuffer.beginCapture();
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	//gBuffer.setUniforms(&shader);
	app->getGpuMesh().render();

	shader.unbind();
	gBuffer.endCapture();

	app->getProfiler().time("Capture g-buffer");
}

void Standard::bruteForceShadows(App *app)
{
	// TODO: I'd prefer to use a BVH of some kind rather than simple brute force.
	// TODO: A simple sparse voxel octree may work best here.
	// TODO: How should I build/use it?
	// TODO: Even a simple z-sort for a depth based partitioning or a uniform grid might be good enough.
	auto &camera = Renderer::instance().getActiveCamera();

	// Need a buffer to store the shadow result.
	if (!resultBuffer.isGenerated() || width != camera.getWidth() || height != camera.getHeight())
	{
		width = camera.getWidth();
		height = camera.getHeight();

		// One float per pixel, initialised to zero.
		// TODO: May want to just zero this in a shader.
		std::vector<int> zeroes(width * height, 0);
		resultBuffer.create(&zeroes[0], width * height * sizeof(int));

		mvMat = camera.getInverse();
		pMat = camera.getProjection();
		lightPos = app->getLightPos();

		startIndex = 0;
		endIndex = std::min(1000U, app->getNIndices());
		finished = false;
	}

	// Start again if the camera or light change.
	if (mvMat != camera.getInverse() || pMat != camera.getProjection() || app->getLightPos() != lightPos)
	{
		std::vector<int> zeroes(width * height, 0);
		resultBuffer.create(&zeroes[0], width * height * sizeof(int));

		mvMat = camera.getInverse();
		pMat = camera.getProjection();
		lightPos = app->getLightPos();

		startIndex = 0;
		endIndex = std::min(1000U, app->getNIndices());
		finished = false;
	}
	// Otherwise keep going.
	else if (endIndex < app->getNIndices())
	{
		startIndex = endIndex;
		endIndex += 1000;
		endIndex = std::min(endIndex, app->getNIndices());
		finished = false;
	}
	else
		finished = true;

	// Deferred rendering of the scene, applying shadows using the compound deep image.
	app->getProfiler().start("Shadows");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	bruteShadowShader.bind();

	gBuffer.bindTextures();
	gBuffer.setUniforms(&bruteShadowShader);

	// General rendering stuff.
	bruteShadowShader.setUniform("mainLightPos", app->getLightPos());
	bruteShadowShader.setUniform("mvMatrix", camera.getInverse());
	bruteShadowShader.setUniform("pMatrix", camera.getProjection());
	bruteShadowShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	bruteShadowShader.setUniform("viewport", camera.getViewport());
	bruteShadowShader.setUniform("invPMatrix", camera.getInverseProj());
	bruteShadowShader.setUniform("invMvMatrix", camera.getTransform());

	// Raycasting data.
	bruteShadowShader.setUniform("startIndex", startIndex);
	bruteShadowShader.setUniform("endIndex", endIndex);

	bruteShadowShader.setUniform("finished", finished);

	// Geometry data.
	bruteShadowShader.setUniform("nIndices", app->getNIndices());
	bruteShadowShader.setUniform("IndexData", &app->getIndexData());
	bruteShadowShader.setUniform("VertexData", &app->getVertexData());

	bruteShadowShader.setUniform("Result", &resultBuffer);

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();

	bruteShadowShader.unbind();

	app->getProfiler().time("Shadows");
}

void Standard::bruteForcePathTrace(App *app)
{
	// TODO: should be width * height * N_SAMPLES right?

	// TODO: I'd prefer to use a BVH of some kind rather than simple brute force.
	// TODO: A simple sparse voxel octree may work best here.
	// TODO: How should I build/use it?
	// TODO: Even a simple z-sort for a depth based partitioning or a uniform grid might be good enough.
	auto &camera = Renderer::instance().getActiveCamera();

	// Need a buffer to store the shadow result.
	if (!resultBuffer.isGenerated() || !accumBuffer.isGenerated() || width != camera.getWidth() || height != camera.getHeight())
	{
		width = camera.getWidth();
		height = camera.getHeight();

		// One float per pixel, initialised to zero.
		std::vector<int> zeroes(width * height, 0);

		// TODO: Going to need a value per sample I think.
		resultBuffer.create(&zeroes[0], width * height * sizeof(unsigned int));
		accumBuffer.create(&zeroes[0], width * height * sizeof(unsigned int));

		startIndex = 0;
		endIndex = std::min(1000U, app->getNIndices());
		finished = false;
		currBounce = 0;
	}

	// Start again if the camera or light change.
	if (mvMat != camera.getInverse() || pMat != camera.getProjection() || app->getLightPos() != lightPos)
	{
		std::vector<int> zeroes(width * height, 0);

		// TODO: Going to need a value per sample I think.
		resultBuffer.create(&zeroes[0], width * height * sizeof(unsigned int));
		accumBuffer.create(&zeroes[0], width * height * sizeof(unsigned int));

		mvMat = camera.getInverse();
		pMat = camera.getProjection();
		lightPos = app->getLightPos();

		startIndex = 0;
		endIndex = std::min(1000U, app->getNIndices());
		finished = false;
		currBounce = 0;
	}
	// Otherwise keep going.
	else if (endIndex < app->getNIndices())
	{
		startIndex = endIndex;
		endIndex += 1000;
		endIndex = std::min(endIndex, app->getNIndices());
		finished = false;
	}
	else
	{
		finished = true;
	}

	// Deferred rendering of the scene, applying shadows using the compound deep image.
	app->getProfiler().start("Path trace");

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	brutePathTraceShader.bind();

	gBuffer.bindTextures();
	gBuffer.setUniforms(&brutePathTraceShader);

	// General rendering stuff.
	brutePathTraceShader.setUniform("mainLightPos", app->getLightPos());
	brutePathTraceShader.setUniform("mvMatrix", camera.getInverse());
	brutePathTraceShader.setUniform("pMatrix", camera.getProjection());
	brutePathTraceShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	brutePathTraceShader.setUniform("viewport", camera.getViewport());
	brutePathTraceShader.setUniform("invPMatrix", camera.getInverseProj());
	brutePathTraceShader.setUniform("invMvMatrix", camera.getTransform());

	// Raycasting data.
	brutePathTraceShader.setUniform("startIndex", startIndex);
	brutePathTraceShader.setUniform("endIndex", endIndex);

	brutePathTraceShader.setUniform("finished", finished);
	brutePathTraceShader.setUniform("nBounces", N_BOUNCES);
	brutePathTraceShader.setUniform("currBounce", currBounce);

	// Geometry data.
	brutePathTraceShader.setUniform("nIndices", app->getNIndices());
	brutePathTraceShader.setUniform("IndexData", &app->getIndexData());
	brutePathTraceShader.setUniform("VertexData", &app->getVertexData());

	brutePathTraceShader.setUniform("CurrResult", &resultBuffer);
	brutePathTraceShader.setUniform("AccumResult", &accumBuffer);

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();

	brutePathTraceShader.unbind();

	app->getProfiler().time("Path trace");

	// If we've finished checking all the geometry, go to the next bounce.
	if (finished && currBounce < N_BOUNCES)
	{
		currBounce++;
		startIndex = 0;
		endIndex = 0;
		finished = false;
	}
}

Math::vec4 Standard::traceRay(App *app, Math::Ray ray, int depth)
{
	(void) (app);
	(void) (ray);
	(void) (depth);
	return vec4();
#if 0
	// Have we bounced enough times?
	if (depth >= N_BOUNCES)
		return vec4(); // black.

	// Check mesh geometry to see if we hit anything.
	// TODO: Ideally this data should be in a BVH of some kind.

	// Did we hit anything?
	// ray.getNearestHit
	// if !ray.hitSomething return black
	auto &mesh = app->getMesh();
	for (size_t i = 0; i < mesh.getNumIndices() / 3; i++)
	{
		// TODO: Need to get the following:
		// TODO: Hit location.
		// TODO: Material of hit geometry (including textures, diffuse and normal).
		// TODO: Texture coords at hit location.
		// TODO: Normal at hit location.
		vec3 hitPos;
		float t = 1000, u = 0, v = 0;
		// FIXME: This needs to check all geometry, and it needs to return only the closest intersection point.
		bool hit = checkGeometry(i, ray.origin, ray.dir, mesh.getVertices(), mesh.getIndices(), hitPos, t, u, v);
		if (!hit)
			return vec4(); // black.
		float w = 1 - u - v;

		// Need info about the geometry at the hit location.
		vec3 n1 = mesh.getVertices()[i * 3 + 0].norm;
		vec3 n2 = mesh.getVertices()[i * 3 + 1].norm;
		vec3 n3 = mesh.getVertices()[i * 3 + 2].norm;

		vec2 t1 = mesh.getVertices()[i * 3 + 0].tex;
		vec2 t2 = mesh.getVertices()[i * 3 + 1].tex;
		vec2 t3 = mesh.getVertices()[i * 3 + 2].tex;

		// TODO: Need to get the colour of the texture at t.
		vec3 n = (n1 * u + n2 * v + n3 * w);
		vec2 t = (t1 * u + t2 * v + t3 * w);
	}

	// Material of hit geometry. Emittance is if we hit a light source I guess.
	// material = ray.thingHit.material
	// emittance = material.emittance

	// Pick a random direction at the hit location and keep going.
	// This is only good for diffuse surfaces, for specular reflections you need to use importance sampling.
	// ray newRay
	// newRay.origin = ray.pointWhereObjWasHit
	// newRay.dir = RandomUnitVectorInHemisphereOf(ray.normalWhereObjWasHit)
	//Ray newRay;
	//newRay.origin = something
	//newRay.dir = blah();

	// Probability of new ray.
	// const float p = 1 / (2 * M_PI);

	// Compute BRDF for this ray (can use any BRDF I guess, Cook-Torrence is probably best).
	// float cosTheta = dot(newRay.dir, ray.normalWhereObjWasHit);
	// color BRDF = material.reflectance / M_PI;

	// Recursively trace reflected light sources.
	// color incoming = traceRay(newRay.origin, newRay.dir, depth + 1);

	// Apply the rendering equation here.
	// return emittance + (BRDF * incoming * cosTheta / p);

	return vec4();
#endif
}

void Standard::cpuPathTrace(App *app)
{
	// Note: This traces from the camera. Ideally we want to trace from the rendered geometry.
	(void) (app);

	// get all geometry data, including positions, tex coords and normals
	// get all texture data (for all textures in the mesh)
	//auto &indexBuffer = app->getIndexData();
	//auto &vertexBuffer = app->getVertexData();
	//int nIndices = app->getNIndices();

	auto &camera = Renderer::instance().getActiveCamera();

	auto model = camera.getInverse();
	auto proj = camera.getProjection();
	auto view = camera.getViewport();
	float near = camera.getNear();
	float far = camera.getFar();

	vec4 pixelColors[512 * 512];

	for (int y = 0; y < 512; y++)
	{
		for (int x = 0; x < 512; x++)
		{
			int pixelIndex = x + y * 512;

			// Get ray from camera to pixel.
			// TODO: But then how does this give you shadows?
			Ray ray = Ray::screenToRay(x, y, model, proj, view, near, far);
			vec4 color;

			for (int i = 0; i < N_SAMPLES; i++)
				color += traceRay(app, ray, 0);
			color /= N_SAMPLES;
			pixelColors[pixelIndex] = color;
		}
	}

	// TODO: Something with the pixelColors array.
}


void Standard::init(App *app)
{
	(void) (app);

	//selectTriangleTest(0);

	startIndex = 0;
	endIndex = 0;
	finished = false;

	width = 0;
	height = 0;
}

void Standard::render(App *app)
{
	//drawMeshRay(app, testPos, {0.0f, 0.0f, 2.0f});
	//drawTriangleRay(currTest.v0, currTest.v1, currTest.v2, currTest.r0, currTest.r1);

	auto &camera = Renderer::instance().getActiveCamera();

	gBuffer.resize(camera.getWidth(), camera.getHeight());

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	captureGeometry(app);

#if PATH_TRACE
	bruteForcePathTrace(app);
#else
	bruteForceShadows(app);
#endif

	glPopAttrib();
}

void Standard::update(float dt)
{
	(void) (dt);
}

void Standard::useLighting(App *app)
{
	reloadShaders(app);
}
#if 0
void App::nextTest()
{
	selectTriangleTest(currTestIndex + 1);
}

void App::prevTest()
{
	selectTriangleTest(currTestIndex - 1);
}
#endif
void Standard::reloadShaders(App *app)
{
	(void) (app);

	startIndex = 0;
	endIndex = 0;
	finished = false;

	width = 0;
	height = 0;

	resultBuffer.release();
	accumBuffer.release();

	mvMat = mat4::getIdentity();
	pMat = mat4::getIdentity();

	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);

	// Shadow raytracing shader using brute force.
	auto bruteShadowVert = ShaderSourceCache::getShader("bruteShadowVert").loadFromFile("shaders/bruteForce/shadow.vert");
	auto bruteShadowFrag = ShaderSourceCache::getShader("bruteShadowFrag").loadFromFile("shaders/bruteForce/shadow.frag");
	bruteShadowShader.release();
	bruteShadowShader.create(&bruteShadowVert, &bruteShadowFrag);

	auto brutePathVert = ShaderSourceCache::getShader("brutePathVert").loadFromFile("shaders/bruteForce/pathTrace.vert");
	auto brutePathFrag = ShaderSourceCache::getShader("brutePathFrag").loadFromFile("shaders/bruteForce/pathTrace.frag");
	brutePathTraceShader.release();
	brutePathTraceShader.create(&brutePathVert, &brutePathFrag);
}

void Standard::savePathTracedImg(App *app)
{
	// TODO: Save this to an image.
	cpuPathTrace(app);
}

