#pragma once

#include "Lighting.h"

#include "../../../Renderer/src/Lighting/GBuffer.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Math/src/Ray.h"

class Standard : public Lighting
{
private:
	//Rendering::Bloom bloom;

	// TODO: Looking up texture data for different geometry means I need a texture array.
	// TODO: Is it faster to just use a texture array and render all geometry at once?

	Rendering::Shader basicShader;

	Rendering::GBuffer gBuffer;
	Rendering::Shader bruteShadowShader;
	Rendering::Shader brutePathTraceShader;

	Rendering::StorageBuffer resultBuffer;
	Rendering::StorageBuffer accumBuffer;

	unsigned int startIndex;
	unsigned int endIndex;
	bool finished;

	int currBounce;

	int width;
	int height;

	Math::mat4 mvMat;
	Math::mat4 pMat;
	Math::vec3 lightPos;

private:
	void drawMeshRay(App *app, Math::vec3 r0, Math::vec3 r1);

	void drawTriangleRay(Math::vec3 v0, Math::vec3 v1, Math::vec3 v2, Math::vec3 r0, Math::vec3 r1);
	void selectTriangleTest(int test);

	void captureGeometry(App *app);
	void bruteForceShadows(App *app);
	void bruteForcePathTrace(App *app);

	Math::vec4 traceRay(App *app, Math::Ray ray, int depth);
	void cpuPathTrace(App *app);

public:
	Standard() : Lighting(), startIndex(0), endIndex(0), finished(false), currBounce(0), width(0), height(0) {}
	virtual ~Standard() {}

	virtual void init(App *app) override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLighting(App *app) override;

	virtual void reloadShaders(App *app) override;

	virtual void savePathTracedImg(App *app) override;
};

