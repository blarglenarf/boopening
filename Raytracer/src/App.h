#pragma once

#include "Lighting/Lighting.h"

#include "../../Renderer/src/Camera.h"
#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/RenderResource/Mesh.h"
#include "../../Renderer/src/RenderResource/Scene.h"
#include "../../Renderer/src/Profiler.h"

#include <vector>

class App
{
private:
	int currScene;
	int currLighting;

	int tmpWidth;
	int tmpHeight;

	bool hudFlag;

	Rendering::Camera lightCam;

	Math::vec3 mainLightPos;
	Math::vec3 mainLightDir;

	Rendering::Shader basicShader;

	//Rendering::Mesh mesh;
	//Rendering::GPUMesh gpuMesh;
	Rendering::Mesh *mesh;
	Rendering::GPUMesh *gpuMesh;

	unsigned int nIndices;
	Rendering::StorageBuffer indexDataBuffer;
	Rendering::StorageBuffer vertexDataBuffer;

	std::vector<Rendering::Scene> scenes;

	Rendering::Profiler profiler;

	Lighting *lighting;

public:
	App() : currScene(0), currLighting(0), tmpWidth(0), tmpHeight(0), hudFlag(true), mesh(nullptr), gpuMesh(nullptr), nIndices(0), lighting(nullptr) {}
	~App() {}

	void init();

	void setTmpViewport(int width, int height);
	void restoreViewport();

	void drawMesh(const Rendering::Camera &camera);
	void drawMesh();

	void render();
	void update(float dt);

	int useScene(int currScene);
	void useNextScene();
	void usePrevScene();

	int useLighting(int currLighting);
	void useNextLighting();
	void usePrevLighting();

	void setLightPos();

	void reloadShaders();
	void saveData();

	void runBenchmarks();

	void savePathTracedImg();

	Rendering::GPUMesh &getGpuMesh() { return *gpuMesh; }
	Rendering::Mesh &getMesh() { return *mesh; }
	Rendering::Profiler &getProfiler() { return profiler; }

	unsigned int getNIndices() const { return nIndices; }
	Rendering::StorageBuffer &getIndexData() { return indexDataBuffer; }
	Rendering::StorageBuffer &getVertexData() { return vertexDataBuffer; }

	Math::vec3 getLightPos() const { return mainLightPos; }
	Math::vec3 getLightDir() const { return mainLightDir; }
	Rendering::Camera &getLightCam() { return lightCam; }

	void toggleHud() { hudFlag = !hudFlag; }
};

