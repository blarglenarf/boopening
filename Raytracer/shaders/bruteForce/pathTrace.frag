#version 450

#define USE_G_BUFFER 1

#include "../utils.glsl"


uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;
uniform sampler2D positionTex;


uniform uint nIndices;

uniform uint startIndex;
uniform uint endIndex;

uniform bool finished;

uniform int nBounces;
uniform int currBounce;

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

coherent buffer CurrResult
{
	uint currResult[];
};

coherent buffer AccumResult
{
	uint accumResult[];
};


//int nSamples = 1;




#include "../light/light.glsl"

out vec4 fragColor;


// Triangle intersections.
bool checkTriangle(uint id, vec3 o, vec3 d, out float t)
{
	t = 0;

	// TODO: Return something other than true/false for soft shadows.

	// Line intersection test with triangle given by id.
	// Code from http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/ and https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm

	// Triangle.
	// TODO: Convert triangle to eye space so this works properly.
	// FIXME: Reading these is really slow. Find another way...
	vec3 v0 = vertexData[indexData[id * 3 + 0]].xyz;
	vec3 v1 = vertexData[indexData[id * 3 + 1]].xyz;
	vec3 v2 = vertexData[indexData[id * 3 + 2]].xyz;

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.0001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	float u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	float v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	//float t = f * dot(e2, q);
	t = f * dot(e2, q);	

	// Intersection.
	if (t > 0.00001)
	{
		//pos = o + (d * t);
		return true;
	}

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}

//bool raycast(uint selfIndex, vec3 osFrom, vec3 osTo)
bool raycast(uint selfIndex, vec3 osFrom, vec3 osDir, out float t)
{
	t = 0;

	// If we don't normalise this, then we know t values > 1 will be past the end point.
	//vec3 osDir = osTo - osFrom;

	//for (uint i = 0; i < nIndices / 3; i++)
	for (uint i = startIndex / 3; i < endIndex / 3; i++)
	{
		// No self-intersections.
		if (i == selfIndex)
			continue;

		// TODO: Need the colour of the geometry and the point of intersection.
		if (checkTriangle(i, osFrom, osDir, t))
			return true;
	}

	return false;
}

vec3 getRandomDir(vec3 normal)
{
	return normal;
}

vec4 pathTrace(int fragIndex, uint selfIndex, vec3 osFrom, vec3 osLight, vec3 osNorm, vec4 directColor)
{
	// TODO: Redo this so that this is as close as possible to my deep image implementation.
	// TODO: Just do the indirect rays, start with many directions, one bounce.
	// TODO: Shadows should be done in a separate step, since they'll be a separate step in the deep image version.

	// TODO: Might want to simplify this using some kind of BVH, even just a uniform grid or z partition would help.
	// TODO: Uniform grid may be best since then I can test my DDA for the deep image approach.
	vec4 accumColor = uintToRGBA8(accumResult[fragIndex]);

	// Keep going until we've bounced enough times.
	if (currBounce < nBounces)
	{
		// TODO: Could probably do all the samples here, not sure if we need a separate pass per-sample or not.
		// TODO: This would require a value per-sample per-pixel in the storage buffer. Probably not a big deal.

		// Finished checking geometry for this ray, did we hit anything?
		if (finished)
		{
			// We hit something, accumulate it.
			if (currResult[fragIndex] != 0)
			{
				vec4 currCol = uintToRGBA8(currResult[fragIndex]);
				vec4 c = currCol + accumColor;
				accumResult[fragIndex] = rgba8ToUint(c);
				currResult[fragIndex] = 0;
			}
		}
		// Haven't finished yet, keep checking.
		else
		{
			// Raycast in sample direction.
			// TODO: Many samples, maybe 10 to 30. This requires storing hit locations and maybe directions for each sample.
			float t = 0;
			bool hitGeom = raycast(selfIndex, osFrom, osNorm, t);

			// If we hit something, compare with currently stored distance, if the new hit is closer, overwrite.
			if (hitGeom)
			{
				vec4 currCol = uintToRGBA8(currResult[fragIndex]);
				if (currCol.w == 0 || currCol.w > t)
				{
					// TODO: Get the colour of the geometry at the hit location.
					// TODO: Need to multiple this by a dot product, probably.
					currCol = vec4(0.1, 0.0, 0.0, t);
					currResult[fragIndex] = rgba8ToUint(currCol);
				}
			}
		}
	}

	// Once we've bounced enough times we need to cast from the pixel to the light source and see if we're in shadow.
	// TODO: Need to multiply by dot product here I guess?
	else
	{
		// We're in shadow, just return the current accumulated colour.
		if (currResult[fragIndex] == 1)
			return accumColor;

		// Finished checking geometry and not in shadow, return the accumulated and direct colour.
		if (finished)
			return accumColor + directColor;

		// Not finished checking and not yet in shadow, keep checking.
		float t = 0;
		vec3 osDir = osLight - osFrom;
		bool inShadow = raycast(selfIndex, osFrom, osDir, t);
		if (inShadow)
		{
			currResult[fragIndex] = 1;
			return accumColor;
		}
	}

	return accumColor;
}




void main()
{
	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	fragColor = getAmbient(material, 0.2) * 0.5;
	//return;

	// No geometry here.
	if (depth == 0)
		return;

	// Need some info to path trace.
	vec4 osPos = invMvMatrix * vec4(esFrag, 1);
	//vec4 osNorm = invMvMatrix * vec4(normal, 1);
	vec4 osNorm = vec4(normal, 1);
	vec4 osLight = vec4(mainLightPos.xyz, 1);

	// What colour would the fragment be if it were lit?
	vec3 viewDir = normalize(-esFrag);
	vec4 esLightPos = mvMatrix * osLight;

	vec4 lightColor = vec4(1);
	float dist = distance(esLightPos.xyz, esFrag);

	float att = 0;
	vec4 directColor = sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, 50, dist, true, att);

	int fragIndex = int(gl_FragCoord.x) + int(gl_FragCoord.y) * int(viewport.z);
	vec4 col = pathTrace(fragIndex, selfIndex, osPos.xyz, osLight.xyz, osNorm.xyz, directColor);

	// FIXME: I probably shouldn't apply the fragment's ambient colour now...
	fragColor += col;

#if 0
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

