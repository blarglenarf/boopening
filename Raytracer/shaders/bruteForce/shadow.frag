#version 450

#define USE_G_BUFFER 1

#include "../utils.glsl"


uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;
uniform sampler2D positionTex;


uniform uint nIndices;

uniform uint startIndex;
uniform uint endIndex;

uniform bool finished;

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

coherent buffer Result
{
	int result[];
};




#include "../light/light.glsl"

out vec4 fragColor;


// Triangle intersections.
bool checkTriangle(uint id, vec3 o, vec3 d)
{
	// TODO: Return something other than true/false for soft shadows.

	// Line intersection test with triangle given by id.
	// Code from http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/ and https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm

	// Triangle.
	// TODO: Convert triangle to eye space so this works properly.
	// FIXME: Reading these is really slow. Find another way...
	vec3 v0 = vertexData[indexData[id * 3 + 0]].xyz;
	vec3 v1 = vertexData[indexData[id * 3 + 1]].xyz;
	vec3 v2 = vertexData[indexData[id * 3 + 2]].xyz;

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.0001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	float u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	float v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * dot(e2, q);

	// Intersection.
	if (t > 0.00001 && t < 1)
	{
		//pos = o + (d * t);
		return true;
	}

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}

bool raycast(uint selfIndex, vec3 osFrom, vec3 osTo)
{
	// If we don't normalise this, then we know t values > 1 will be past the end point.
	vec3 osDir = osTo - osFrom;

	//for (uint i = 0; i < nIndices / 3; i++)
	for (uint i = startIndex / 3; i < endIndex / 3; i++)
	{
		// No self-intersections.
		if (i == selfIndex)
			continue;

		if (checkTriangle(i, osFrom, osDir))
			return true;
	}

	return false;
}




void main()
{
	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	fragColor = getAmbient(material, 0.2) * 0.5;
	//return;

	if (depth == 0)
		return;

	// Are we in shadow?
	vec4 osPos = invMvMatrix * vec4(esFrag, 1);
	vec4 osLight = vec4(mainLightPos.xyz, 1);

	int fragCoord = int(gl_FragCoord.x) + int(gl_FragCoord.y) * int(viewport.z);
	if (result[fragCoord] == 1)
		return;

	if (!finished)
	{
		bool shadowFlag = raycast(selfIndex, osPos.xyz, osLight.xyz);
		if (shadowFlag)
		{
			result[fragCoord] = 1;
			return;
		}
	}

	vec3 viewDir = normalize(-esFrag);
	vec4 esLightPos = mvMatrix * osLight;

	vec4 lightColor = vec4(1);
	float dist = distance(esLightPos.xyz, esFrag);

	float att = 0;
	vec4 directColor = sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, 50, dist, true, att);
	//directColor /= att;

	fragColor += directColor;

#if 0
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

