#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define DEEP_RES 64


uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;


in VertexData
{
	vec3 osFrag;
	vec2 texCoord;
} VertexIn;


coherent buffer VoxelGrid
{
	uint voxelGrid[];
};


uniform bool texFlag;
uniform bool normFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

uniform uint startIndex;

#include "../light/light.glsl"

#include "../voxel/voxelIndex.glsl"



void main()
{
	//uint polyID = startIndex / 3 + uint(gl_PrimitiveID);
	// TODO: Actual index uses osFrag...
	vec3 ssFrag = vec3(	((VertexIn.osFrag.x + 15.0) / 30.0) * float(DEEP_RES),
						((VertexIn.osFrag.y + 15.0) / 30.0) * float(DEEP_RES),
						((VertexIn.osFrag.z + 15.0) / 30.0) * float(DEEP_RES));

	// Need 3D index, this assume gl_FragCoord.z is something sensible, which it may not be.
	//uint index = (uint(ssFrag.y) * DEEP_RES * DEEP_RES + uint(ssFrag.x) * DEEP_RES + uint(ssFrag.z)) * 4;
	//int index = getIndex(int(ssFrag.x), int(ssFrag.y), int(ssFrag.z), DEEP_RES, DEEP_RES) * 4;
	int level = getLevel(DEEP_RES);
	int startIndex = getStartIndex(level);
	int index = (getIndex(int(ssFrag.x), int(ssFrag.y), int(ssFrag.z), DEEP_RES) + startIndex) * 4;

	// TODO: Not sure if this should take lighting into account or not.
	// Need diffuse rather than ambient, since this has no texture info.
	//vec4 color = getAmbient(0.3);
	vec4 ambient = vec4(Material.ambient.xyz, Material.ambient.w);
	if (texFlag)
	{
		vec4 texColor = texture2D(diffuseTex, VertexIn.texCoord);
		ambient = texColor;
		ambient.w = min(Material.ambient.w, texColor.w);
	}
	ambient.w = 1;
	vec4 color = ambient;

	// Need to add ints rather than floats.
	uint r = uint(color.x * 255);
	uint g = uint(color.y * 255);
	uint b = uint(color.z * 255);

	// Add the colours and the count.
	atomicAdd(voxelGrid[index + 0], r);
	atomicAdd(voxelGrid[index + 1], g);
	atomicAdd(voxelGrid[index + 2], b);
	atomicAdd(voxelGrid[index + 3], 1);

	discard;
}

