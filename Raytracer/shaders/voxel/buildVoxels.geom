#version 450

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;


in vec3 osVert[3];
//in vec3 osNorm[3];
in vec2 texCoord[3];


out VertexData
{
	vec3 osFrag;
	vec2 texCoord;
} VertexOut;


uniform mat4 mvMatrix;
uniform mat4 pMatrix;


void main()
{
	vec4 esVert;

	vec3 osNorm = normalize(cross(osVert[0] - osVert[1], osVert[2] - osVert[1]));

	// This approach just rasterizes along the z axis and swizzles components to get approximately good pixel coverage.
	// Which axis does the primitive face?
	vec3 xAxis = vec3(1, 0, 0);
	vec3 yAxis = vec3(0, 1, 0);
	vec3 zAxis = vec3(0, 0, 1);

	// Swizzle the components based on which dot product is the largest.
	float dpX = abs(dot(xAxis, osNorm));
	float dpY = abs(dot(yAxis, osNorm));
	float dpZ = abs(dot(zAxis, osNorm));

	// X-axis major, swap x and z.
	bool xFlag = dpX > dpY && dpX > dpZ;

	// Y-axis major, swap y and z.
	bool yFlag = dpY > dpX && dpY > dpZ;

	for (int i = 0; i < gl_in.length(); i++)
	{
		vec4 osVertSwiz = vec4(osVert[i], 1);
		VertexOut.osFrag = osVertSwiz.xyz;
		VertexOut.texCoord = texCoord[i];

		gl_PrimitiveID = gl_PrimitiveIDIn;

		// Note: This assumes the geometry is being rendered along the z-axis.
		if (xFlag)
			osVertSwiz.xyzw = osVertSwiz.zyxw;
		else if (yFlag)
			osVertSwiz.xyzw = osVertSwiz.xzyw;

		vec4 esVert = mvMatrix * osVertSwiz;
		vec4 csVert = pMatrix * esVert;
		gl_Position = csVert;
		EmitVertex();
	}
	EndPrimitive();
}

