#version 430

#define VOXEL_LIST 0

#define INDEX_BY_TILE 0
#define GET_COUNTS 0
#define SINGLE_VAL 0
#define USE_FLOAT 1

#define USE_NDC 0

#define LFB_FRAG_TYPE float

#if !GET_COUNTS
	#import "lfb"
#endif

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#include "index.glsl"
#include "../voxel/voxelIndex.glsl"

#define LFB_NAME lfb
#define DEEP_RES 64
#define N_DEEPS 3

#define DEEP_COLS 1
#define DEEP_ROWS 1

#if GET_COUNTS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);
#endif


// Separate camera for each deep image direction.
readonly buffer DeepCams
{
	mat4 deepCams[];
};

#if VOXEL_LIST
	// List of unique voxels.
	readonly buffer VoxelList
	{
		vec4 voxelList[];
	};
#else
	// Full voxel grid.
	readonly buffer VoxelGrid
	{
		uint voxelGrid[];
	};
#endif

uniform int startIndex;

uniform int lvlDim;
uniform int nCams;

uniform int nCols;
uniform int nRows;

uniform mat4 pMatrix;



void main()
{
	int deepRes = DEEP_RES;
	int nDeeps = N_DEEPS;
	int deepCols = DEEP_COLS;
	int deepRows = DEEP_ROWS;

	if (lvlDim > 0)
		deepRes = lvlDim;
	if (nCams > 0)
		nDeeps = nCams;
	if (nCols > 0)
		deepCols = nCols;
	if (nRows > 0)
		deepRows = nRows;

	ivec3 voxelPos = ivec3(0);
	float col = 0;
#if VOXEL_LIST
	vec4 voxelData = voxelList[gl_VertexID];

	voxelPos = ivec3(voxelData.xyz);
	col = voxelData.w;
#else
	// TODO: This step needs to happen for lower levels even if VOXEL_LIST is defined.
	// TODO: What is startIndex in this context? It depends what level we're rendering.
	int index = gl_VertexID;
	int dataIndex = (startIndex + index) * 4;

	uint r = voxelGrid[dataIndex + 0];
	uint g = voxelGrid[dataIndex + 1];
	uint b = voxelGrid[dataIndex + 2];
	uint c = voxelGrid[dataIndex + 3];

	if (c == 0)
		return;

	r /= c;
	g /= c;
	b /= c;
	col = rgba8ToFloat(vec4(float(r) / 255.0, float(g) / 255.0, float(b) / 255.0, 1));

	voxelPos = getCoord(index, deepRes);
#endif

	// FIXME: For now I'm assuming world coords from -15 to 15 on xyz.
	vec3 osFrag = vec3(	float(voxelPos.x) / float(deepRes) * 30.0 - 15.0,
						float(voxelPos.y) / float(deepRes) * 30.0 - 15.0,
						float(voxelPos.z) / float(deepRes) * 30.0 - 15.0);

	// I guess each voxel is of size 30 / DEEP_RES.
	float voxSize = (30.0 / float(deepRes)) / 2.0;

	// Not sure why this is necessary but whatevs.
	osFrag.xyz += voxSize;

	// Need the size of an individual image.
	ivec2 imgSize = ivec2(deepRes);
	ivec2 deepSize = imgSize * ivec2(deepCols, deepRows);

	// Fragment may cover many different pixels, these need to be calculated for each direction.
	int camStep = N_DEEPS / nDeeps;
	for (int i = 0; i < nDeeps; i++)
	{
		// Number of CDI directions decreases with lower CDI 
		vec4 esFrag = deepCams[i * camStep] * vec4(osFrag, 1);
	#if USE_NDC
		vec2 ssFrag = getScreenFromClip(esFrag, vec2(imgSize.x, imgSize.y));
	#else
		vec4 csFrag = pMatrix * esFrag;
		vec2 ssFrag = getScreenFromClip(csFrag, vec2(imgSize.x, imgSize.y));
	#endif

		int pixel = pixelIndex(i, ivec2(ssFrag), deepSize, deepCols, deepRes);
	#if GET_COUNTS
		atomicAdd(offsets[pixel], 1);
	#else
		#if USE_NDC
			float d = esFrag.z;
		#else
			// If I'm not using ndc then I want to negate the depth values.
			float d = -esFrag.z;
		#endif

		#if SINGLE_VAL
			//LFB_ADD_DATA(LFB_NAME, pixel, d);
			LFB_ADD_DATA(LFB_NAME, pixel, col);
		#else
			LFB_ADD_DATA(LFB_NAME, pixel, LFB_FRAG_TYPE(col, d));
		#endif
	#endif
	}
}

