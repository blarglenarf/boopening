#version 450

layout(points) in;

layout(line_strip, max_vertices = 2) out;

#define USE_G_BUFFER 1

#define USE_NDC 0

#define RAYCAST_DEPTH_ONLY 0
#define RAYCAST_SURFELS 0

#define USE_DEPTH_RANGE 1

#define DEEP_RES 64
#define N_DEEPS 144

#define DEEP_COLS 1
#define DEEP_ROWS 1

#define SINGLE_VAL 1
#define USE_FLOAT 1

#define PIXEL_STEP 0

#define LFB_FRAG_TYPE vec2

#include "../utils.glsl"

#include "index.glsl"


uniform vec3 mainLightPos;
//uniform vec3 mainLightDir;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

uniform mat4 deepCamProj;
uniform mat4 invDeepCamProj;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;


#import "lfb"

uniform int stacks;
uniform int slices;

readonly buffer DeepCams
{
	mat4 deepCams[];
};

readonly buffer InvDeepCams
{
	mat4 invDeepCams[];
};


uniform uint nIndices;

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

LFB_DEC(lfb, LFB_FRAG_TYPE);


#include "../light/light.glsl"



in vec4 debugRay[1];

out vec4 color;



// Triangle intersections.
bool checkTriangle(uint id, vec3 o, vec3 d, out vec3 hitPos)
{
	// TODO: Possible to get a colour value here?

	// Line intersection test with triangle given by id.
	// Code from
	// http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/
	// and
	// https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm

	// Triangle.
	// Convert triangle to eye space so this works properly.
	// FIXME: Reading these is really slow. Find another way...
	vec3 v0 = vertexData[indexData[id * 3 + 0]].xyz;
	vec3 v1 = vertexData[indexData[id * 3 + 1]].xyz;
	vec3 v2 = vertexData[indexData[id * 3 + 2]].xyz;

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.00001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	float u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	float v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * dot(e2, q);

	// Intersection.
	if (t > 0.001 && t < 1)
	{
		// Compute actual intersection pos.
		hitPos = o + d * t;
		return true;
	}

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}

// TODO: Return end position, or a range value.
bool raycast(uint selfIndex, vec3 osFrom, vec3 osTo, out vec3 hitPos, out vec4 col)
{
	hitPos = vec3(0);
	col = vec4(0);

	vec3 osDir = osTo - osFrom;
	vec3 osRealDir = normalize(osDir);
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

	vec4 esStartPos = deepCams[index] * vec4(osFrom, 1);
	vec4 esEndPos = deepCams[index] * vec4(osTo, 1);

#if USE_NDC
	// Undo scaling.
	float zStart = esStartPos.z / 0.06;
	float zEnd = esEndPos.z / 0.06;
#else
	// I'm using positive z values.
	float zStart = -esStartPos.z;
	float zEnd = -esEndPos.z;
#endif

#if !PIXEL_STEP
	// No stepping between pixels.

	// What's a good epsilon value for an orthographic projection?
	float epsilon = 1;

	vec4 esPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csPos = deepCamProj * esPos;
	vec2 ssPos = getScreenFromClip(csPos, vec2(DEEP_RES, DEEP_RES));
	int pixel = getPixel(ivec2(ssPos), index, DEEP_RES, DEEP_COLS, DEEP_ROWS);
	// Step through the fragment list to find the next fragment after osFrom.
	float d = -esPos.z;

	// Stepping direction is reversed if reverseFlag is true.
	for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA_REV(lfb);
			float z = data.y;
			if (d < z - epsilon)
			{
				col = floatToRGBA8(data.x);
			#if 1
				//float hitDist = -(d - z);
				hitPos = osFrom + osDir * ((z - zStart) / 256.0); // Assuming the ortho projection is 256 units deep.
			#else
				float t = (z - zStart) / (zEnd - zStart);
				hitPos = osFrom + osDir * t;
			#endif
				return true;
				//return -(d - z);
			}
		}
		else
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA(lfb);
			float z = data.y;
			if (z < d - epsilon)
			{
				col = floatToRGBA8(data.x);
			#if 1
				//float hitDist = -(z - d);
				hitPos = osFrom + osDir * ((-z + zStart) / 256.0);
			#else
				float t = (z - zStart) / (zEnd - zStart);
				hitPos = osFrom + osDir * t;
			#endif
				return true;
				//return -(z - d);
			}
		}
	}

	// Didn't hit anything...
	return false;
#else
	// TODO: Implement me. See below.
#endif

#if 0
	// https://stackoverflow.com/questions/24679963/precise-subpixel-line-drawing-algorithm-rasterization-algorithm/24680894#24680894
	// http://www.flipcode.com/archives/Raytracing_Topics_Techniques-Part_4_Spatial_Subdivisions.shtml
	// http://www.cse.yorku.ca/~amana/research/grid.pdf
	vec3 osReal = osTo - osFrom;
	vec3 osRealDir = normalize(osReal);
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
#if 1
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
#endif
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

	vec4 esStartPos = deepCams[index] * vec4(osFrom, 1);
	vec4 esEndPos = deepCams[index] * vec4(osTo, 1);

#if USE_NDC
	ivec2 ssStartPos = ivec2(getScreenFromClip(esStartPos, vec2(DEEP_RES, DEEP_RES)));
	ivec2 ssEndPos = ivec2(getScreenFromClip(esEndPos, vec2(DEEP_RES, DEEP_RES)));
#else
	vec4 csStartPos = deepCamProj * esStartPos;
	vec4 csEndPos = deepCamProj * esEndPos;

	ivec2 ssStartPos = ivec2(getScreenFromClip(csStartPos, vec2(DEEP_RES, DEEP_RES)));
	ivec2 ssEndPos = ivec2(getScreenFromClip(csEndPos, vec2(DEEP_RES, DEEP_RES)));
#endif

	ssStartPos.x = clamp(ssStartPos.x, 0, DEEP_RES - 1);
	ssStartPos.y = clamp(ssStartPos.y, 0, DEEP_RES - 1);

	ssEndPos.x = clamp(ssEndPos.x, 0, DEEP_RES - 1);
	ssEndPos.y = clamp(ssEndPos.y, 0, DEEP_RES - 1);

#if USE_NDC
	// Undo scaling.
	float zStart = esStartPos.z / 0.06;
	float zEnd = esEndPos.z / 0.06;
#else
	// I'm using positive z values.
	float zStart = -esStartPos.z;
	float zEnd = -esEndPos.z;
#endif

	// Correct voxel traversal algorithm:
	// https://stackoverflow.com/questions/12367071/how-do-i-initialize-the-t-variables-in-a-fast-voxel-traversal-algorithm-for-ray

	vec3 ssPos = vec3(ssStartPos.x, ssStartPos.y, zStart);

	//#define sign(x) (x > 0 ? 1 : (x < 0 ? -1 : 0))
	#define frac0(x) (x - floor(x))
	#define frac1(x) (1 - x + floor(x))

	vec3 dd = vec3(ssEndPos.x - ssStartPos.x, ssEndPos.y - ssStartPos.y, zEnd - zStart);
	vec3 s = vec3(sign(dd.x), sign(dd.y), sign(dd.z));
	vec3 td, tMax;

	if (dd.x != 0)
		td.x = min(s.x / dd.x, 10000000.0);
	else
		td.x = 10000000.0;
	if (dd.x > 0)
		tMax.x = td.x * frac1(ssStartPos.x);
	else
		tMax.x = td.x * frac0(ssStartPos.x);

	if (dd.y != 0)
		td.y = min(s.y / dd.y, 10000000.0);
	else
		td.y = 10000000.0;
	if (dd.y > 0)
		tMax.y = td.y * frac1(ssStartPos.y);
	else
		tMax.y = td.y * frac0(ssStartPos.y);

	if (dd.z != 0)
		td.z = min(s.z / dd.z, 10000000.0);
	else
		td.z = 10000000.0;
	if (dd.z > 0)
		tMax.z = td.z * frac1(zStart);
	else
		tMax.z = td.z * frac0(zStart);

	#undef frac0
	#undef frac1

	float epsilon = 1;

	// FIXME: This still occasionally misses geometry...
	for (int canary = 0; canary < 40; canary++)
	{
	#if USE_DEPTH_RANGE
		// Get the depth range for this pixel.
		vec2 tmpRange = vec2(ssPos.z, ssPos.z);
		float tm = min(tMax.x, tMax.y);
		while (tMax.z <= tm)
		{
			tMax.z += td.z;
			ssPos.z += s.z;
		}
		tmpRange.y = ssPos.z;
		vec2 depthRange = vec2(min(tmpRange.x, tmpRange.y), max(tmpRange.x, tmpRange.y));
	#endif

		int pixel = getPixel(ivec2(ssPos.x, ssPos.y), index, DEEP_RES, DEEP_COLS, DEEP_ROWS);
		for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
		{
			if (reverseFlag)
			{
			#if SINGLE_VAL
				float z = LFB_GET_DATA(lfb);
				uint id = floatBitsToUint(z);
				if (id == selfIndex)
					continue;
				//if (zStart - epsilon < z)
				{
					if (checkTriangle(id, osFrom, osReal, hitPos))
						return true;
				}
			#else
				LFB_FRAG_TYPE data = LFB_GET_DATA_REV(lfb);
				#if RAYCAST_SURFELS
					// TODO: Account for depth range.
					float z = data.y;
					if (zStart - epsilon < z)
					{
						vec4 n = floatToRGBA8(data.x);
						n.xyz = normalize(vec3(n.x * 2.0 - 1.0, n.y * 2.0 - 1.0, n.z * 2.0 - 1.0));
						if (checkSurfel(ssPos, z, osFrom, osReal, n.xyz, index))
							return true;
					}
					else
						break;
				#else
					uint id = floatBitsToUint(data.x);
					//if (id == selfIndex)
					//	continue; // TODO: return false;
					#if USE_NDC
						float z = data.y / 0.06;
						if (zStart + epsilon < z)
						{
							float t = zStart - z;
							hitPos = osFrom + osRealDir * -t;
							return true;
						}
					#else
						float z = data.y;

						// If the fragment is before the light, keep going.
						if (z < zStart - epsilon)
							continue;
					#if USE_DEPTH_RANGE
						// FIXME: Not sure about the order here.
						// Is the fragment depth between the ray's depth range for this pixel?
						if (z < depthRange.x)
							continue;
						if (z > depthRange.y)
							break;
					#endif
						// Otherwise check geometry.
						else
						{
						#if !RAYCAST_DEPTH_ONLY
							// Check triangle for hitPos.
							if (checkTriangle(id, osFrom, osReal, hitPos))
							{
								return true;
							}
						#else
							// Check z value for hitPos.
							float t = (z - zStart) / (zEnd - zStart);
							hitPos = osFrom + osReal * t;
							return true;
						#endif
						}
					#endif
				#endif
			#endif
			}
			else
			{
			#if SINGLE_VAL
				float z = LFB_GET_DATA(lfb);
				uint id = floatBitsToUint(z);
				if (id == selfIndex)
					continue;
				//if (zStart - epsilon < z)
				{
					if (checkTriangle(id, osFrom, osReal, hitPos))
						return true;
				}
			#else
				LFB_FRAG_TYPE data = LFB_GET_DATA(lfb);
				#if RAYCAST_SURFELS
					// TODO: Account for depth range.
					float z = data.y;
					if (z - epsilon < d)
					{
						// TODO: Make sure n is correct... somehow.
						vec4 n = floatToRGBA8(data.x);
						n.xyz = normalize(vec3(n.x * 2.0 - 1.0, n.y * 2.0 - 1.0, n.z * 2.0 - 1.0));
						if (checkSurfel(ssPos, z, osFrom, osReal, n.xyz, index))
							return true;
					}
					else
						break;
				#else
					uint id = floatBitsToUint(data.x);
					//if (id == selfIndex)
					//	continue; // TODO: return false;
					#if USE_NDC
						float z = data.y / 0.06;
						if (z < zStart - epsilon)
						{
							float t = z - zStart;
							hitPos = osFrom + osRealDir * -t;
							return true;
						}
					#else
						float z = data.y;

						// If the fragment is before the light, keep going.
						if (z > zStart - epsilon)
							continue;
					#if USE_DEPTH_RANGE
						// FIXME: Not sure about the order here.
						// Is the fragment depth between the ray's depth range for this pixel?
						if (z < depthRange.x)
							continue;
						if (z > depthRange.y)
							break;
					#endif
						// Otherwise check geometry.
						else
						{
						#if !RAYCAST_DEPTH_ONLY
							// Check triangle for hitPos.
							if (checkTriangle(id, osFrom, osReal, hitPos))
								return true;
						#else
							// Check z value for hitPos.
							float t = (z - zStart) / (zEnd - zStart);
							hitPos = osFrom + osReal * t;
							return true;
						#endif
						}
					#endif
				#endif
			#endif
			}
		}

		if (tMax.x > 1 && tMax.y > 1)
			break;

		if (tMax.x < tMax.y)
		{
			tMax.x += td.x;
			ssPos.x += s.x;
		}
		else
		{
			tMax.y += td.y;
			ssPos.y += s.y;
		}
	}
#endif
	// Didn't hit anything. I guess we're fine then.
	return false;
}


void main()
{
	// osPos is just lightDir * some range (maybe infinite).
	vec4 osPos = vec4(mainLightPos + (debugRay[0].xyz * 30.0), 1);
	vec4 osLight = vec4(mainLightPos.xyz, 1);

	vec3 hitPos = vec3(0);
	vec4 col = vec4(0);

	// In this case selfIndex doesn't matter, but for raycasted shadows it does.
	bool shadowFlag = raycast(0, osLight.xyz, osPos.xyz, hitPos, col);

	vec3 start = osLight.xyz;
	vec3 end = shadowFlag ? hitPos : osPos.xyz;

	vec4 osStart = vec4(start, 1);
	vec4 osEnd = vec4(end, 1);

	vec4 esStart = mvMatrix * osStart;
	vec4 esEnd = mvMatrix * osEnd;

	vec4 csStart = pMatrix * esStart;
	vec4 csEnd = pMatrix * esEnd;

	// Output two vertices (start, end).
	gl_Position = csStart;
	color = vec4(0, 1, 0, 1);
	EmitVertex();
	//color = vec4(col.xyz, 1);
	color = vec4(1, 0, 0, 1);
	gl_Position = csEnd;
	EmitVertex();
	EndPrimitive();
}

