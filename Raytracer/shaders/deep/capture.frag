#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define INDEX_BY_TILE 0
#define GET_COUNTS 0
#define SINGLE_VAL 1
#define USE_FLOAT 1

#define USE_NDC 0

#define RAYCAST_SURFELS 0
#define PATH_TRACE 0

#define DRAW_DEEPS 0

#define LFB_FRAG_TYPE float

#if !GET_COUNTS
	#import "lfb"
#endif

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#include "index.glsl"

#define LFB_NAME lfb
#define DEEP_RES 64
#define N_DEEPS 3

#define DEEP_COLS 1
#define DEEP_ROWS 1

#if GET_COUNTS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);
#endif


uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;


in VertexData
{
	vec3 osFrag;
	vec2 texCoord;
#if RAYCAST_SURFELS
	vec3 normal;
#endif
} VertexIn;


// Separate camera for each deep image direction.
readonly buffer DeepCams
{
	mat4 deepCams[];
};


uniform mat4 pMatrix;

uniform bool texFlag;
uniform bool normFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

uniform uint startIndex;

#include "../light/light.glsl"


out vec4 fragColor;



void main()
{
	uint polyID = startIndex / 3 + uint(gl_PrimitiveID);

	vec4 color = getAmbient(0.3);

	// Need the size of an individual image.
	ivec2 imgSize = ivec2(DEEP_RES);
	ivec2 deepSize = imgSize * ivec2(DEEP_COLS, DEEP_ROWS);

	// Fragment may cover many different pixels, these need to be calculated for each direction.
	// FIXME: This can result in a lot of duplication of fragments across neighbouring pixels. Not sure how to fix this.
	for (int i = 0; i < N_DEEPS; i++)
	{
		// FIXME: Does this have a swizzling problem? Because the values seem wrong to me...
		vec4 esFrag = deepCams[i] * vec4(VertexIn.osFrag, 1);
	#if USE_NDC
		vec2 ssFrag = getScreenFromClip(esFrag, vec2(imgSize.x, imgSize.y));
	#else
		vec4 csFrag = pMatrix * esFrag;
		vec2 ssFrag = getScreenFromClip(csFrag, vec2(imgSize.x, imgSize.y));
	#endif

		int pixel = pixelIndex(i, ivec2(ssFrag), deepSize, DEEP_COLS, DEEP_RES);
	#if GET_COUNTS
		atomicAdd(offsets[pixel], 1);
	#else

		#if USE_NDC
			float d = esFrag.z;
		#else
			// If I'm not using ndc then I want to negate the depth values.
			float d = -esFrag.z;
		#endif

		#if RAYCAST_SURFELS
			vec4 n = vec4((VertexIn.normal.x + 1.0) / 2.0, (VertexIn.normal.y + 1.0) / 2.0, (VertexIn.normal.z + 1.0) / 2.0, 0);
			float f = rgba8ToFloat(n);
		#else
			float f = uintBitsToFloat(polyID);
		#endif

			//color = vec4(0, 0, 1, 1);
		#if DRAW_DEEPS || PATH_TRACE || CAPTURE_COLOR
			// TODO: If we're path tracing, then the data looks a little different.
			// TODO: Need 3 bytes for color, 2 bytes for depth, 3 bytes for normal.
			f = rgba8ToFloat(color);
		#endif

		#if SINGLE_VAL
			//LFB_ADD_DATA(LFB_NAME, pixel, d);
			LFB_ADD_DATA(LFB_NAME, pixel, f);
		#else
			LFB_ADD_DATA(LFB_NAME, pixel, LFB_FRAG_TYPE(f, d));
		#endif
	#endif
	}

	//color = getAmbient(0.3); // Not sure if this is correct now or not...
	discard;

	//fragColor = color;
}

