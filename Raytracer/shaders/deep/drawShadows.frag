#version 450

#define USE_G_BUFFER 1

#include "../utils.glsl"

#include "index.glsl"


uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

uniform mat4 deepCamProj;
uniform mat4 invDeepCamProj;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;

// Shadow map uniforms.
uniform sampler2DShadow shadowTex;

uniform mat4 lightMatrix;


#include "../light/light.glsl"

out vec4 fragColor;



void main()
{
	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);

	//uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	vec4 ambient = getAmbient(material, 0.2);

	if (depth == 0)
		return;

	// TODO: Are we in shadow?
	vec4 osPos = invMvMatrix * vec4(esFrag, 1);
	vec4 osLight = vec4(mainLightPos.xyz, 1);

	vec3 osNorm = normalize((invMvMatrix * vec4(normal, 0)).xyz);


	vec3 viewDir = normalize(-esFrag);
	vec4 esLightPos = mvMatrix * osLight;

	vec4 lightColor = vec4(1);
	float dist = distance(esLightPos.xyz, esFrag);

	float att = 0;
	vec4 directColor = sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, 50, dist, true, att);
	//directColor /= att;


	//fragColor += directColor;
	vec4 lightFrag = lightMatrix * vec4(osPos.xyz, 1);
	lightFrag.xyz /= lightFrag.w; // Especially necessary for a perspective projection.
	//lightFrag.xyz = lightFrag.xyz * 0.5 + 0.5;

	float visibility = texture(shadowTex, lightFrag.xyz).x;
	if (visibility > 0)
		fragColor += directColor;
	//fragColor += indirectColor * 0.3;
}

