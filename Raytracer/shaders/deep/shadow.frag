#version 450

#define USE_G_BUFFER 1

#define RAYCAST_DEPTH_ONLY 0

#define USE_DEPTH_RANGE 1

#define DEEP_RES 64
#define N_DEEPS 144

#define DEEP_COLS 1
#define DEEP_ROWS 1

#define SINGLE_VAL 1
#define USE_FLOAT 1

#define LFB_FRAG_TYPE float

#include "../utils.glsl"

#include "index.glsl"


uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

uniform mat4 deepCamProj;
uniform mat4 invDeepCamProj;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;


#import "lfb"

uniform int stacks;
uniform int slices;

readonly buffer DeepCams
{
	mat4 deepCams[];
};

readonly buffer InvDeepCams
{
	mat4 invDeepCams[];
};


uniform uint nIndices;

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

LFB_DEC(lfb, LFB_FRAG_TYPE);




#include "../light/light.glsl"

out vec4 fragColor;


// Triangle intersections.
bool checkTriangleGeom(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 d)
{
	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.00001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	float u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	float v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * dot(e2, q);

	// Intersection.
	if (t > 0.001 && t < 1)
		return true;

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}

bool checkTriangle(uint id, vec3 o, vec3 d)
{
	// TODO: Possible to get a colour value here?

	// Line intersection test with triangle given by id.
	// Code from http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/ and https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm

	// Triangle.
	// Convert triangle to eye space so this works properly.
	// FIXME: Reading these is really slow. Find another way...
	vec3 v0 = vertexData[indexData[id * 3 + 0]].xyz;
	vec3 v1 = vertexData[indexData[id * 3 + 1]].xyz;
	vec3 v2 = vertexData[indexData[id * 3 + 2]].xyz;

	return checkTriangleGeom(v0, v1, v2, o, d);
}

bool raycast(uint selfIndex, vec3 osFrom, vec3 osTo)
{
	// https://stackoverflow.com/questions/24679963/precise-subpixel-line-drawing-algorithm-rasterization-algorithm/24680894#24680894
	// http://www.flipcode.com/archives/Raytracing_Topics_Techniques-Part_4_Spatial_Subdivisions.shtml
	// http://www.cse.yorku.ca/~amana/research/grid.pdf
	vec3 osReal = osTo - osFrom;
	vec3 osRealDir = normalize(osReal);
	//vec3 osDeepDir = osRealDir;
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
#if 1
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
#endif
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

	vec4 esStartPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csStartPos = deepCamProj * esStartPos;

	vec4 esEndPos = deepCams[index] * vec4(osTo, 1);
	vec4 csEndPos = deepCamProj * esEndPos;

	ivec2 ssStartPos = ivec2(getScreenFromClip(csStartPos, vec2(DEEP_RES, DEEP_RES)));
	ivec2 ssEndPos = ivec2(getScreenFromClip(csEndPos, vec2(DEEP_RES, DEEP_RES)));

	ssStartPos.x = clamp(ssStartPos.x, 0, DEEP_RES - 1);
	ssStartPos.y = clamp(ssStartPos.y, 0, DEEP_RES - 1);

	ssEndPos.x = clamp(ssEndPos.x, 0, DEEP_RES - 1);
	ssEndPos.y = clamp(ssEndPos.y, 0, DEEP_RES - 1);

	// I'm using positive z values.
	// FIXME: I think these depth values are different from what is being stored in the CDI...
	float zStart = -esStartPos.z;
	float zEnd = -esEndPos.z;

	// Correct voxel traversal algorithm:
	// https://stackoverflow.com/questions/12367071/how-do-i-initialize-the-t-variables-in-a-fast-voxel-traversal-algorithm-for-ray

	vec3 ssPos = vec3(ssStartPos.x, ssStartPos.y, zStart);

	//#define sign(x) (x > 0 ? 1 : (x < 0 ? -1 : 0))
	#define frac0(x) (x - floor(x))
	#define frac1(x) (1 - x + floor(x))

	vec3 dd = vec3(ssEndPos.x - ssStartPos.x, ssEndPos.y - ssStartPos.y, zEnd - zStart);
	vec3 s = vec3(sign(dd.x), sign(dd.y), sign(dd.z));
	vec3 td, tMax;

	if (dd.x != 0)
		td.x = min(s.x / dd.x, 10000000.0);
	else
		td.x = 10000000.0;
	if (dd.x > 0)
		tMax.x = td.x * frac1(ssStartPos.x);
	else
		tMax.x = td.x * frac0(ssStartPos.x);

	if (dd.y != 0)
		td.y = min(s.y / dd.y, 10000000.0);
	else
		td.y = 10000000.0;
	if (dd.y > 0)
		tMax.y = td.y * frac1(ssStartPos.y);
	else
		tMax.y = td.y * frac0(ssStartPos.y);

	if (dd.z != 0)
		td.z = min(s.z / dd.z, 10000000.0);
	else
		td.z = 10000000.0;
	if (dd.z > 0)
		tMax.z = td.z * frac1(zStart);
	else
		tMax.z = td.z * frac0(zStart);

	#undef frac0
	#undef frac1

	float epsilon = 5;

	for (int canary = 0; canary < 100; canary++)
	{
	#if USE_DEPTH_RANGE
		// Get the depth range for this pixel.
		vec2 tmpRange = vec2(ssPos.z, ssPos.z);
		float tm = min(tMax.x, tMax.y);
		if (tMax.z <= tm)
		{
			float amt = ceil((tm - tMax.z) / td.z);
			tMax.z += td.z * amt;
			ssPos.z += s.z * amt;
		}
		tmpRange.y = ssPos.z;
		vec2 depthRange = vec2(min(tmpRange.x, tmpRange.y), max(tmpRange.x, tmpRange.y));
	#endif

		int pixel = getPixel(ivec2(ssPos), index, DEEP_RES, DEEP_COLS, DEEP_ROWS);
		for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
		{
			if (reverseFlag)
			{
				//return true;
			#if SINGLE_VAL
				float z = LFB_GET_DATA(lfb);
				uint id = floatBitsToUint(z);
				if (id == selfIndex)
					continue;
				//if (zStart - epsilon < z)
				{
					if (checkTriangle(id, osFrom, osReal))
						return true;
				}
			#else
				LFB_FRAG_TYPE data = LFB_GET_DATA_REV(lfb);

				uint id = floatBitsToUint(data.x);
				if (id == selfIndex)
					continue; // FIXME: Technically this should mean we're not in shadow right?
				float z = data.y;

				// FIXME: We're going in reverse here, so isn't this check wrong?
				// If the fragment is before the light, keep going.
				if (z < min(zStart, zEnd) - epsilon)
					continue;
				// If the fragment is past the geometry, stop.
				if (z > max(zStart, zEnd) + epsilon)
					continue;

				#if USE_DEPTH_RANGE
					// Is the fragment depth between the ray's depth range for this pixel?
					// FIXME: This calculation doesn't work for some reason...
					// FIXME: Only way to fix this is to test it on the CPU.
					if (z < min(depthRange.x, depthRange.y) - epsilon)
						continue;
					if (z > max(depthRange.x, depthRange.y) + epsilon)
						continue;
				#endif

				// Otherwise check geometry.
				#if !RAYCAST_DEPTH_ONLY
					if (checkTriangle(id, osFrom, osReal))
						return true;
				#else
					// FIXME: selfIndex isn't enough here, need to just ignore geometry within a certain distance of osTo.
					float dist = max(zEnd, z) - min(zEnd, z);
					if (dist < epsilon)
						return false;
					return true;
				#endif
			#endif
			}
			else
			{
				//return true;
			#if SINGLE_VAL
				float z = LFB_GET_DATA(lfb);
				uint id = floatBitsToUint(z);
				if (id == selfIndex)
					continue;
				//if (zStart - epsilon < z)
				{
					if (checkTriangle(id, osFrom, osReal))
						return true;
				}
			#else
				LFB_FRAG_TYPE data = LFB_GET_DATA(lfb);
				uint id = floatBitsToUint(data.x);
				if (id == selfIndex)
					continue; // FIXME: Technically this should mean we're not in shadow right?
				float z = data.y;

				// If the fragment is before the light, keep going.
				if (z < min(zStart, zEnd) - epsilon)
					continue;
				// If the fragment is past the geometry, stop.
				if (z > max(zStart, zEnd) + epsilon)
					continue;

				#if USE_DEPTH_RANGE
					// Is the fragment depth between the ray's depth range for this pixel?
					if (z < min(depthRange.x, depthRange.y) - epsilon)
						continue;
					if (z > max(depthRange.x, depthRange.y) + epsilon)
						continue;
				#endif

				// Otherwise check geometry.
				#if !RAYCAST_DEPTH_ONLY
					if (checkTriangle(id, osFrom, osReal))
						return true;
				#else
					// FIXME: selfIndex isn't enough here, need to just ignore geometry within a certain distance of osTo.
					float dist = max(zEnd, z) - min(zEnd, z);
					if (dist < epsilon)
						return false;
					return true;
				#endif
			#endif
			}
		}

		if (tMax.x > 1 && tMax.y > 1)
			break;

		if (tMax.x < tMax.y)
		{
			tMax.x += td.x;
			ssPos.x += s.x;
		}
		else
		{
			tMax.y += td.y;
			ssPos.y += s.y;
		}
	}

	// Didn't hit anything. I guess we're fine then.
	return false;
}



void main()
{
	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	fragColor = getAmbient(material, 0.2) * 0.5;

	if (depth == 0)
		return;

	// Are we in shadow?
	vec4 osPos = invMvMatrix * vec4(esFrag, 1);
	vec4 osLight = vec4(mainLightPos.xyz, 1);

	bool shadowFlag = raycast(selfIndex, osLight.xyz, osPos.xyz);
	//bool shadowFlag = raycast(selfIndex, osPos.xyz, osLight.xyz);

	if (shadowFlag)
	{
		//fragColor.x = 1;
		return;
	}

	vec3 viewDir = normalize(-esFrag);
	vec4 esLightPos = mvMatrix * osLight;

	vec4 lightColor = vec4(1);
	float dist = distance(esLightPos.xyz, esFrag);

	float att = 0;
	vec4 directColor = sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, 50, dist, true, att);
	//directColor /= att;

	fragColor += directColor;

#if 0
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

