#version 450

#define USE_G_BUFFER 1

#define RAYCAST_DEPTH_ONLY 1

#define USE_DEPTH_RANGE 1

#define DEEP_RES 256
#define N_DEEPS 256

#define DEEP_COLS 1
#define DEEP_ROWS 1

#define SINGLE_VAL 0
#define USE_FLOAT 1

#define N_SAMPLES 6

#define LFB_FRAG_TYPE float

#include "../utils.glsl"

#include "index.glsl"


uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

uniform mat4 deepCamProj;
uniform mat4 invDeepCamProj;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;


#import "lfb"

uniform int stacks;
uniform int slices;

readonly buffer DeepCams
{
	mat4 deepCams[];
};

readonly buffer InvDeepCams
{
	mat4 invDeepCams[];
};


uniform uint nIndices;

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

// The primary hi-res CDI.
LFB_DEC(lfb, LFB_FRAG_TYPE);


// The other lo-res CDI levels.
LFB_DEC(upperLfb0, LFB_FRAG_TYPE);
LFB_DEC(upperLfb1, LFB_FRAG_TYPE);
LFB_DEC(upperLfb2, LFB_FRAG_TYPE);
LFB_DEC(upperLfb3, LFB_FRAG_TYPE);

// Rows/cols for lo-res CDI levels.
uniform int upperCols0;
uniform int upperRows0;

uniform int upperCols1;
uniform int upperRows1;

uniform int upperCols2;
uniform int upperRows2;

uniform int upperCols3;
uniform int upperRows3;




#include "../light/light.glsl"

out vec4 fragColor;





bool raycast3(vec3 osFrom, vec3 osDir, out vec4 color, out float hitDist)
{
	color = vec4(0);

	vec3 osRealDir = normalize(osDir);
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

	// No stepping between pixels.

	// What's a good epsilon value for an orthographic projection?
	float epsilon = 4;

	vec4 esPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csPos = deepCamProj * esPos;

	vec2 ssPos = getScreenFromClip(csPos, vec2(DEEP_RES / 16, DEEP_RES / 16));
	int pixel = getPixel(ivec2(ssPos), index, DEEP_RES / 16, upperCols3, upperRows3);

	// Step through the fragment list to find the next fragment after osFrom.
	float d = -esPos.z;

	// Stepping direction is reversed if reverseFlag is true.
	for (LFB_ITER_BEGIN(upperLfb3, pixel); LFB_ITER_CHECK(upperLfb3); LFB_ITER_INC(upperLfb3))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA_REV(upperLfb3);
			float z = data.y;
			if (d < z - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(d - z);
				return true;
			}
		}
		else
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA(upperLfb3);
			float z = data.y;
			if (z < d - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(z - d);
				return true;
			}
		}
	}

	// Didn't hit anything...
	return false;
}






bool raycast2(vec3 osFrom, vec3 osDir, out vec4 color, out float hitDist)
{
	color = vec4(0);

	vec3 osRealDir = normalize(osDir);
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

	// No stepping between pixels.

	// What's a good epsilon value for an orthographic projection?
	float epsilon = 4;

	vec4 esPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csPos = deepCamProj * esPos;

	vec2 ssPos = getScreenFromClip(csPos, vec2(DEEP_RES / 8, DEEP_RES / 8));
	int pixel = getPixel(ivec2(ssPos), index, DEEP_RES / 8, upperCols2, upperRows2);

	// Step through the fragment list to find the next fragment after osFrom.
	float d = -esPos.z;

	// Stepping direction is reversed if reverseFlag is true.
	for (LFB_ITER_BEGIN(upperLfb2, pixel); LFB_ITER_CHECK(upperLfb2); LFB_ITER_INC(upperLfb2))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA_REV(upperLfb2);
			float z = data.y;
			if (d < z - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(d - z);
				return true;
			}
		}
		else
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA(upperLfb2);
			float z = data.y;
			if (z < d - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(z - d);
				return true;
			}
		}
	}

	// Didn't hit anything...
	return false;
}





bool raycast1(vec3 osFrom, vec3 osDir, out vec4 color, out float hitDist)
{
	color = vec4(0);

	vec3 osRealDir = normalize(osDir);
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

	// No stepping between pixels.

	// What's a good epsilon value for an orthographic projection?
	float epsilon = 4;

	vec4 esPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csPos = deepCamProj * esPos;

	vec2 ssPos = getScreenFromClip(csPos, vec2(DEEP_RES / 4, DEEP_RES / 4));
	int pixel = getPixel(ivec2(ssPos), index, DEEP_RES / 4, upperCols1, upperRows1);

	// Step through the fragment list to find the next fragment after osFrom.
	float d = -esPos.z;

	// Stepping direction is reversed if reverseFlag is true.
	for (LFB_ITER_BEGIN(upperLfb1, pixel); LFB_ITER_CHECK(upperLfb1); LFB_ITER_INC(upperLfb1))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA_REV(upperLfb1);
			float z = data.y;
			if (d < z - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(d - z);
				return true;
			}
		}
		else
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA(upperLfb1);
			float z = data.y;
			if (z < d - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(z - d);
				return true;
			}
		}
	}

	// Didn't hit anything...
	return false;
}



bool raycast0(vec3 osFrom, vec3 osDir, out vec4 color, out float hitDist)
{
	color = vec4(0);

	vec3 osRealDir = normalize(osDir);
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

	// No stepping between pixels.

	// What's a good epsilon value for an orthographic projection?
	float epsilon = 4;

	vec4 esPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csPos = deepCamProj * esPos;

	vec2 ssPos = getScreenFromClip(csPos, vec2(DEEP_RES / 2, DEEP_RES / 2));
	int pixel = getPixel(ivec2(ssPos), index, DEEP_RES / 2, upperCols0, upperRows0);

	// Step through the fragment list to find the next fragment after osFrom.
	float d = -esPos.z;

	// Stepping direction is reversed if reverseFlag is true.
	for (LFB_ITER_BEGIN(upperLfb0, pixel); LFB_ITER_CHECK(upperLfb0); LFB_ITER_INC(upperLfb0))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA_REV(upperLfb0);
			float z = data.y;
			if (d < z - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(d - z);
				return true;
			}
		}
		else
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA(upperLfb0);
			float z = data.y;
			if (z < d - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(z - d);
				return true;
			}
		}
	}

	// Didn't hit anything...
	return false;
}






bool raycast(vec3 osFrom, vec3 osDir, out vec4 color, out float hitDist)
{
	color = vec4(0);

	vec3 osRealDir = normalize(osDir);
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

	// No stepping between pixels.

	// What's a good epsilon value for an orthographic projection?
	float epsilon = 2;

	vec4 esPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csPos = deepCamProj * esPos;

	// TODO: If we want a different CDI level, need to adjust res/cols/rows.
#if 1
	vec2 ssPos = getScreenFromClip(csPos, vec2(DEEP_RES, DEEP_RES));
	int pixel = getPixel(ivec2(ssPos), index, DEEP_RES, DEEP_COLS, DEEP_ROWS);
#else
	vec2 ssPos = getScreenFromClip(csPos, vec2(DEEP_RES / 2, DEEP_RES / 2));
	int pixel = getPixel(ivec2(ssPos), index, DEEP_RES / 2, upperCols0, upperRows0);
#endif

	// Step through the fragment list to find the next fragment after osFrom.
	float d = -esPos.z;

	// TODO: If we want a different CDI level, need to pass a different lfb arg.
	// Stepping direction is reversed if reverseFlag is true.
	for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA_REV(lfb);
			float z = data.y;
			if (d < z - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(d - z);
				return true;
			}
		}
		else
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA(lfb);
			float z = data.y;
			if (z < d - epsilon)
			{
				color = floatToRGBA8(data.x);
				hitDist = -(z - d);
				return true;
			}
		}
	}

	// Didn't hit anything...
	return false;
}



void main()
{
	// TODO: For this to work, we need 2 byte depth, 3 byte color, 3 byte normal.

	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	//uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	//fragColor = getAmbient(material, 0.2) * 0.5;

	if (depth == 0)
		return;

	// TODO: Instead of doing a full path trace. Instead we can trace from the fragment in a number of random directions and accumulate colour.
	// TODO: Kind of similar to cone tracing I guess.

	// Are we in shadow?
	vec4 osPos = invMvMatrix * vec4(esFrag, 1);
	vec4 osLight = vec4(mainLightPos.xyz, 1);

	vec3 osNorm = normalize((invMvMatrix * vec4(normal, 0)).xyz);

	// TODO: Do I need osNorm or esNorm here?

	// TODO: Pick a bunch of random directions in a hemisphere around the normal, get the colours of the intersected geometry.
	// TODO: Accumulate colour, divide by no. of hits.
	// TODO: Do we differentiate between geometry that is/isn't in shadow?
	int nHits = 0;
	vec4 indirectColor = vec4(0);
	vec3 sampleDir = osNorm;
#if 1
	vec3 v[6];
	v[0] = vec3(0, 1, 0);
	v[1] = vec3(0.707107, 0.707107, 0);
	v[2] = vec3(0.218509, 0.707107, 0.672498);
	v[3] = vec3(-0.57206, 0.707107, 0.415628);
	v[4] = vec3(-0.572063, 0.707107, -0.415625);
	v[5] = vec3(0.218505, 0.707107, -0.672499);
#endif
	for (int i = 0; i < N_SAMPLES; i++)
	{
	#if 0
		float randX = fract(sin(dot(sampleDir.yz, vec2(12.9898, 78.233))) * 43758.5453);
		float randY = fract(sin(dot(sampleDir.xz, vec2(12.9898, 78.233))) * 43758.5453);
		float randZ = fract(sin(dot(sampleDir.xy, vec2(12.9898, 78.233))) * 43758.5453);
	#else
		float randX = v[i].x;
		float randY = v[i].y;
		float randZ = v[i].z;
	#endif

	#if 0
		vec3 up = normalize(osNorm.y * osNorm.y > 0.95 ? vec3(0, 0, 1) : vec3(0, 1, 0));
		vec3 right = normalize(cross(osNorm, up));
		up = normalize(cross(osNorm, right));

		sampleDir = osNorm;
		sampleDir += vec3(randX, randY, randZ) * right + randZ * up;
		sampleDir = normalize(sampleDir);
	#endif

		sampleDir = normalize(osNorm);
		vec3 up = sampleDir.y * sampleDir.y > 0.95f ? vec3(0, 0, 1) : vec3(0, 1, 0);
		vec3 right = normalize(cross(sampleDir, up));
		up = normalize(cross(sampleDir, right));

		sampleDir.x += (randX * right + randZ * up).x;
		sampleDir.y += (randX * right + randZ * up).y;
		sampleDir.z += (randX * right + randZ * up).z;

		//if (dot(sampleDir, osNorm) < 0)
		//	continue;

		// TODO: Ideally you only add the colour if you hit a surface that is lit.
		// TODO: Maybe cast again from the hit location to the light source and see if anything is in the way.
		vec4 col = vec4(0);
		float hitDist = 0;

		float voxSize = (30.0 / float(DEEP_RES)) / 2.0;
		float coneHalfAngle = 3.14159 * 0.5 * 0.33;

		float upperSize0 = 30.0 / float(DEEP_RES / 2);
		float upperSize1 = 30.0 / float(DEEP_RES / 4);
		float upperSize2 = 30.0 / float(DEEP_RES / 8);
		float upperSize3 = 30.0 / float(DEEP_RES / 16);

		// FIXME: This may need pixel stepping to work correctly...
		bool hitFlag = raycast(osPos.xyz, sampleDir, col, hitDist);
		if (hitFlag)
		{
			// TODO: We can now lookup the appropriate CDI based on hitDist.
			// TODO: Errr, how do we do this?
			// TODO: May also need to quadrilinearly interpolate all voxels in the radius given by distance... that sounds bad...
			float coneDiameter = (2.0 * tan(coneHalfAngle) * hitDist) / 30.0;
			//float mip = log2(coneDiameter / voxSize);

			if (coneDiameter >= upperSize3)
			{
				raycast3(osPos.xyz, sampleDir, col, hitDist);
			}
			else if (coneDiameter >= upperSize2)
			{
				raycast2(osPos.xyz, sampleDir, col, hitDist);
			}
			else if (coneDiameter >= upperSize1)
			{
				raycast1(osPos.xyz, sampleDir, col, hitDist);
			}
			else if (coneDiameter >= upperSize0)
			{
				raycast0(osPos.xyz, sampleDir, col, hitDist);
			}

			// TODO: Determine if the hit location can see the light source or not.
			// TODO: Can't just add the color here, needs to take the dot product into account.
			// TODO: What about falloff based on distance?
			//float att = 1.0 / ((hitDist / 10.0) * (hitDist / 10.0));
			//att *= att;
			//float att = 1.0;
			//indirectColor += col * dot(sampleDir, osNorm) * (hitDist / 30.0);
			//indirectColor += vec4(hitDist / 60.0);
			//indirectColor += vec4(mip / 30.0);
			indirectColor += col;
			nHits++;
		}
	}
	if (nHits > 0)
		indirectColor /= vec4(nHits);
	indirectColor = clamp(indirectColor, vec4(0), vec4(1));

	vec3 viewDir = normalize(-esFrag);
	vec4 esLightPos = mvMatrix * osLight;

	vec4 lightColor = vec4(1);
	float dist = distance(esLightPos.xyz, esFrag);

	float att = 0;
	vec4 directColor = sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, 50, dist, true, att);
	//directColor /= att;

	//fragColor += directColor;
	fragColor += indirectColor;

#if 0
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

