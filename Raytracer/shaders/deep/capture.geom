#version 450

#define N_DEEPS 1

#define RASTER_SEPARATE 0
#define RASTER_CUSTOM 0

#define RAYCAST_SURFELS 0

#define USE_NDC 0

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;


#if RASTER_CUSTOM


#define INDEX_BY_TILE 0
#define GET_COUNTS 0
#define SINGLE_VAL 1
#define USE_FLOAT 1

#define SINGLE_VAL 0

#define DEEP_RES 64

#define LFB_FRAG_TYPE float

#if !GET_COUNTS
	#import "lfb"
#endif


#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#include "index.glsl"

#define LFB_NAME lfb
#define DEEP_RES 64
#define N_DEEPS 3

#define DEEP_COLS 1
#define DEEP_ROWS 1

#if GET_COUNTS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);
#endif

uniform uint startIndex;

#endif



in vec3 osVert[3];
//in vec3 osNorm[3];
in vec2 texCoord[3];

out VertexData
{
	vec3 osFrag;
	vec2 texCoord;
#if RAYCAST_SURFELS
	vec3 normal;
#endif
} VertexOut;


uniform mat4 mvMatrix;
uniform mat4 pMatrix;

#if RASTER_CUSTOM
	// Separate camera for each deep image direction.
	readonly buffer DeepCams
	{
		mat4 deepCams[];
	};
#endif

#if 0
// TODO: These will need to be a buffer of matrices. Could alternatively calculate these in the shader.
uniform mat4 mvMatrix0;
uniform mat4 mvMatrix1;
uniform mat4 mvMatrix2;


// Projections are identity matrices.
#define TRI(mvMatrix, layer) \
	for (int i = 0; i < 3; i++) \
	{ \
		esVert = mvMatrix * vec4(osVert[i], 1); \
		VertexOut.esFrag = esVert.xyz; \
		VertexOut.texCoord = texCoord[i]; \
		gl_Position = esVert; \
		gl_Layer = layer; \
		EmitVertex(); \
	} \
	EndPrimitive();
#endif


void main()
{
	vec4 esVert;


#if RASTER_CUSTOM

	uint polyID = startIndex + uint(gl_PrimitiveIDIn);

	// Need the size of an individual image.
	ivec2 imgSize = ivec2(DEEP_RES);
	ivec2 deepSize = imgSize * ivec2(DEEP_COLS, DEEP_ROWS);

	// Rasterize our own per-direction triangles.
	for (int dirIndex = 0; dirIndex < N_DEEPS; dirIndex++)
	{
		// Standard conversion from object space to screen space.
		vec4 osVert0 = vec4(osVert[0], 1);
		vec4 osVert1 = vec4(osVert[1], 1);
		vec4 osVert2 = vec4(osVert[2], 1);

		vec4 esVert0 = deepCams[dirIndex] * osVert0;
		vec4 esVert1 = deepCams[dirIndex] * osVert1;
		vec4 esVert2 = deepCams[dirIndex] * osVert2;

		vec4 csVert0 = pMatrix * esVert0;
		vec4 csVert1 = pMatrix * esVert1;
		vec4 csVert2 = pMatrix * esVert2;

		vec2 ssVert0 = getScreenFromClip(csVert0, vec2(DEEP_RES));
		vec2 ssVert1 = getScreenFromClip(csVert1, vec2(DEEP_RES));
		vec2 ssVert2 = getScreenFromClip(csVert2, vec2(DEEP_RES));

		vec2 ssMin = vec2(min(min(ssVert0.x, ssVert1.x), ssVert2.x), min(min(ssVert0.y, ssVert1.y), ssVert2.y));
		vec2 ssMax = vec2(max(max(ssVert0.x, ssVert1.x), ssVert2.x), max(max(ssVert0.y, ssVert1.y), ssVert2.y));

		// Get barycentric coords for interpolation I guess.
		float area = 0.5 * (-ssVert1.y * ssVert2.x + ssVert0.y * (-ssVert1.x + ssVert2.x) + ssVert0.x * (ssVert1.y - ssVert2.y) + ssVert1.x * ssVert2.y);

		float a1 = 1.0 / (2.0 * area);
		float s1 = ssVert0.y * ssVert2.x - ssVert0.x * ssVert2.y;
		float s2 = ssVert2.y - ssVert0.y;
		float s3 = ssVert0.x - ssVert2.x;
		float t1 = ssVert0.x * ssVert1.y - ssVert0.y * ssVert1.x;
		float t2 = ssVert0.y - ssVert1.y;
		float t3 = ssVert1.x - ssVert0.x;
		//float s = a1 * (s1 + s2 * p.x + s3 * p.y);
		//float t = a1 * (t1 + t2 * p.x + t3 * p.y);
		//s = 1.0 / (2.0 * area) * (ssVert0.y * ssVert2.x - ssVert0.x * ssVert2.y + (ssVert2.y - ssVert0.y) * p.x + (ssVert0.x - ssVert2.x) * p.y);
		//t = 1.0 / (2.0 * area) * (ssVert0.x * ssVert1.y - ssVert0.y * ssVert1.x + (ssVert0.y - ssVert1.y) * p.x + (ssVert1.x - ssVert0.x) * p.y);

		// TODO: Add the triangle to all pixels between the screen-space verts.
		// TODO: Just start with a bounding box I guess.
		// TODO: This needs to be conservative...
		for (int x = int(ssMin.x); x <= int(ssMax.x); x++)
		{
			for (int y = int(ssMin.y); y <= int(ssMax.y); y++)
			{
				// Is this point in the triangle?
				// TODO: Test all four edges of the pixel, if at least one is in then we're fine.
				float s = a1 * (s1 + s2 * float(x) + s3 * float(y));
				float t = a1 * (s1 + s2 * float(x) + s3 * float(y));
				if (s < 0 || t < 0 || 1.0 - s - t < 0)
					continue;

				int pixel = pixelIndex(dirIndex, ivec2(x, y), deepSize, DEEP_COLS, DEEP_RES);
			#if GET_COUNTS
				atomicAdd(offsets[pixel], 1);
			#else
				// If I'm not using ndc then I want to negate the depth values.
				// TODO: I'll need to calculate this for each screen space fragment I guess.
				//float d = -esFrag.z;
				float d = 0;

				#if SINGLE_VAL
					//LFB_ADD_DATA(LFB_NAME, pixel, d);
					LFB_ADD_DATA(LFB_NAME, pixel, uintBitsToFloat(polyID));
				#else
					LFB_ADD_DATA(LFB_NAME, pixel, LFB_FRAG_TYPE(uintBitsToFloat(polyID), d));
				#endif
			#endif
			}
		}
	}

	return;

#endif

	vec3 osNorm = normalize(cross(osVert[0] - osVert[1], osVert[2] - osVert[1]));

	// This approach just rasterizes along the z axis and swizzles components to get approximately good pixel coverage.
	// Which axis does the primitive face?
	vec3 xAxis = vec3(1, 0, 0);
	vec3 yAxis = vec3(0, 1, 0);
	vec3 zAxis = vec3(0, 0, 1);

	// Swizzle the components based on which dot product is the largest.
	float dpX = abs(dot(xAxis, osNorm));
	float dpY = abs(dot(yAxis, osNorm));
	float dpZ = abs(dot(zAxis, osNorm));

	// X-axis major, swap x and z.
	bool xFlag = dpX > dpY && dpX > dpZ;

	// Y-axis major, swap y and z.
	bool yFlag = dpY > dpX && dpY > dpZ;

	for (int i = 0; i < gl_in.length(); i++)
	{
		vec4 osVertSwiz = vec4(osVert[i], 1);
		VertexOut.osFrag = osVertSwiz.xyz;
		VertexOut.texCoord = texCoord[i];
	#if RAYCAST_SURFELS
		VertexOut.normal = osNorm;
	#endif

		gl_PrimitiveID = gl_PrimitiveIDIn;

		// Note: This assumes the geometry is being rendered along the z-axis.
		if (xFlag)
			osVertSwiz.xyzw = osVertSwiz.zyxw;
		else if (yFlag)
			osVertSwiz.xyzw = osVertSwiz.xzyw;

	#if USE_NDC
		vec4 esVert = mvMatrix * osVertSwiz;
		gl_Position = esVert;
	#else
		vec4 esVert = mvMatrix * osVertSwiz;
		vec4 csVert = pMatrix * esVert;
		gl_Position = csVert;
	#endif
		EmitVertex();
	}
	EndPrimitive();
}

