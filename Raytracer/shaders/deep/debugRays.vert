#version 430

readonly buffer DebugRays
{
	vec4 debugRays[];
};


out vec4 debugRay;


void main()
{
	debugRay = debugRays[gl_VertexID];
}

