#version 450

#define USE_G_BUFFER 1

#define RAYCAST_DEPTH_ONLY 1

#define USE_DEPTH_RANGE 1

#define DEEP_RES 64
#define N_DEEPS 144

#define DEEP_COLS 1
#define DEEP_ROWS 1

#define USE_RSM 0
#define USE_SHADOW_MAP 0

#define SINGLE_VAL 0
#define USE_FLOAT 1

#define PIXEL_STEP 1

#define N_SAMPLES 30

#define LFB_FRAG_TYPE float

#include "../utils.glsl"

#include "index.glsl"


uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

uniform mat4 deepCamProj;
uniform mat4 invDeepCamProj;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;

#if USE_RSM
	// Uniforms for the rsm data, if we're using it.
	uniform sampler2D rsmLodTex;

	uniform mat4 lightMat;
#endif

#if USE_SHADOW_MAP
	uniform sampler2DShadow shadowTex;

	uniform mat4 lightMat;
#endif


#import "lfb"

uniform int stacks;
uniform int slices;

readonly buffer DeepCams
{
	mat4 deepCams[];
};

readonly buffer InvDeepCams
{
	mat4 invDeepCams[];
};


uniform uint nIndices;

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

LFB_DEC(lfb, LFB_FRAG_TYPE);




#include "../light/light.glsl"

out vec4 fragColor;


bool raycast(vec3 osFrom, vec3 osDir, out vec4 color, out vec3 hitPos)
{
	color = vec4(0);

	vec3 osRealDir = normalize(osDir);
	vec3 osDeepDir = calcDeepDir(osRealDir, stacks, slices);

	// If osDeepDir.y is negative, ray casting is reversed.
	bool reverseFlag = false;
	if (osDeepDir.y < 0)
	{
		osDeepDir = -osDeepDir;
		reverseFlag = true;
	}
	int index = getDeepImgIndex(osDeepDir, stacks, slices);

#if !PIXEL_STEP
	// No stepping between pixels.

	// What's a good epsilon value for an orthographic projection?
	float epsilon = 5;

	vec4 esPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csPos = deepCamProj * esPos;
	vec2 ssPos = getScreenFromClip(csPos, vec2(DEEP_RES, DEEP_RES));
	int pixel = getPixel(ivec2(ssPos), index, DEEP_RES, DEEP_COLS, DEEP_ROWS);
	// Step through the fragment list to find the next fragment after osFrom.
	float d = -esPos.z;
	float zStart = -esPos.z;

	// Stepping direction is reversed if reverseFlag is true.
	for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		// Get the fragment after the light.
		if (reverseFlag)
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA_REV(lfb);
			float z = data.y;
			if (d < z - epsilon)
			{
				color = floatToRGBA8(data.x);
				//hitDist = -(d - z);
				hitPos = osFrom + osDir * ((z - zStart) / float(DEEP_RES));
				return true;
			}
		}
		else
		{
			LFB_FRAG_TYPE data = LFB_GET_DATA(lfb);
			float z = data.y;
			if (z < d - epsilon)
			{
				color = floatToRGBA8(data.x);
				//hitDist = -(z - d);
				hitPos = osFrom + osDir * ((-z + zStart) / float(DEEP_RES));
				return true;
			}
		}
	}

	// Didn't hit anything...
	return false;
#else
	// Stepping between pixels.

	// https://stackoverflow.com/questions/24679963/precise-subpixel-line-drawing-algorithm-rasterization-algorithm/24680894#24680894
	// http://www.flipcode.com/archives/Raytracing_Topics_Techniques-Part_4_Spatial_Subdivisions.shtml
	// http://www.cse.yorku.ca/~amana/research/grid.pdf

	// TODO: I'm not 100% sure if index should be the same for start/end here. I think they should but not 100% sure.
	vec4 esStartPos = deepCams[index] * vec4(osFrom, 1);
	vec4 csStartPos = deepCamProj * esStartPos;
	float d = -esStartPos.z;

	// TODO: How do I calculate a screen space direction without an end position?
	// TODO: For now I'll just use the osDir with a large enough distance, but might want to correct this later.
	vec3 osTo = osFrom + osDeepDir * 15.0;
	vec4 esEndPos = deepCams[index] * vec4(osTo, 1);
	vec4 csEndPos = deepCamProj * esEndPos;

	ivec2 ssStartPos = ivec2(getScreenFromClip(csStartPos, vec2(DEEP_RES, DEEP_RES)));
	ivec2 ssEndPos = ivec2(getScreenFromClip(csEndPos, vec2(DEEP_RES, DEEP_RES)));

	ssStartPos.x = clamp(ssStartPos.x, 0, DEEP_RES - 1);
	ssStartPos.y = clamp(ssStartPos.y, 0, DEEP_RES - 1);

	ssEndPos.x = clamp(ssEndPos.x, 0, DEEP_RES - 1);
	ssEndPos.y = clamp(ssEndPos.y, 0, DEEP_RES - 1);

	// I'm using positive z values.
	// FIXME: I think these depth values are different from what is being stored in the CDI...
	float zStart = -esStartPos.z;
	float zEnd = -esEndPos.z;

	// Correct voxel traversal algorithm:
	// https://stackoverflow.com/questions/12367071/how-do-i-initialize-the-t-variables-in-a-fast-voxel-traversal-algorithm-for-ray

	vec3 ssPos = vec3(ssStartPos.x, ssStartPos.y, zStart);

	//#define sign(x) (x > 0 ? 1 : (x < 0 ? -1 : 0))
	#define frac0(x) (x - floor(x))
	#define frac1(x) (1 - x + floor(x))

	vec3 dd = vec3(ssEndPos.x - ssStartPos.x, ssEndPos.y - ssStartPos.y, zEnd - zStart);
	vec3 s = vec3(sign(dd.x), sign(dd.y), sign(dd.z));
	vec3 td, tMax;

	if (dd.x != 0)
		td.x = min(s.x / dd.x, 10000000.0);
	else
		td.x = 10000000.0;
	if (dd.x > 0)
		tMax.x = td.x * frac1(ssStartPos.x);
	else
		tMax.x = td.x * frac0(ssStartPos.x);

	if (dd.y != 0)
		td.y = min(s.y / dd.y, 10000000.0);
	else
		td.y = 10000000.0;
	if (dd.y > 0)
		tMax.y = td.y * frac1(ssStartPos.y);
	else
		tMax.y = td.y * frac0(ssStartPos.y);

	if (dd.z != 0)
		td.z = min(s.z / dd.z, 10000000.0);
	else
		td.z = 10000000.0;
	if (dd.z > 0)
		tMax.z = td.z * frac1(zStart);
	else
		tMax.z = td.z * frac0(zStart);

	#undef frac0
	#undef frac1

	float epsilon = 5;

	for (int canary = 0; canary < 100; canary++)
	{
		// Get the depth range for this pixel. This is actually in eye-space even though it says ssPos.
		vec2 tmpRange = vec2(ssPos.z, ssPos.z);
		float tm = min(tMax.x, tMax.y);
		if (tMax.z <= tm)
		{
			float amt = ceil((tm - tMax.z) / td.z);
			tMax.z += td.z * amt;
			ssPos.z += s.z * amt;
		}
		tmpRange.y = ssPos.z;
		vec2 depthRange = vec2(min(tmpRange.x, tmpRange.y), max(tmpRange.x, tmpRange.y));

		int pixel = getPixel(ivec2(ssPos), index, DEEP_RES, DEEP_COLS, DEEP_ROWS);
		for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
		{
			// Get the fragment after the light.
			if (reverseFlag)
			{
				LFB_FRAG_TYPE data = LFB_GET_DATA_REV(lfb);
				float z = data.y;
				//float hitDist = (z - zStart) / float(DEEP_RES);
				//if (hitDist < min(depthRange.x, depthRange.y) - epsilon)
				//	continue;
				//if (hitDist > max(depthRange.x, depthRange.y) + epsilon)
				//	continue;
				if (d < z - epsilon)
				{
					color = floatToRGBA8(data.x);
					//hitDist = -(d - z);
					hitPos = osFrom + osDir * ((z - zStart) / float(DEEP_RES));
					return true;
				}
			}
			else
			{
				LFB_FRAG_TYPE data = LFB_GET_DATA_REV(lfb);
				float z = data.y;
				//if (z < min(depthRange.x, depthRange.y) - epsilon)
				//	continue;
				//if (z > max(depthRange.x, depthRange.y) + epsilon)
				//	continue;
				if (z < d - epsilon)
				{
					color = floatToRGBA8(data.x);
					//hitDist = -(z - d);
					hitPos = osFrom + osDir * ((-z + zStart) / float(DEEP_RES));
					return true;
				}
			}
		}
	#if 0
	#if USE_DEPTH_RANGE
		// Get the depth range for this pixel.
		vec2 tmpRange = vec2(ssPos.z, ssPos.z);
		float tm = min(tMax.x, tMax.y);
		if (tMax.z <= tm)
		{
			float amt = ceil((tm - tMax.z) / td.z);
			tMax.z += td.z * amt;
			ssPos.z += s.z * amt;
		}
		tmpRange.y = ssPos.z;
		vec2 depthRange = vec2(min(tmpRange.x, tmpRange.y), max(tmpRange.x, tmpRange.y));
	#endif

		int pixel = getPixel(ivec2(ssPos), index, DEEP_RES, DEEP_COLS, DEEP_ROWS);
		for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
		{
			if (reverseFlag)
			{
				LFB_FRAG_TYPE data = LFB_GET_DATA_REV(lfb);
				vec4 col = floatToRGBA8(data.x);
				float z = data.y;

				// FIXME: We're going in reverse here, so isn't this check wrong?
				// If the fragment is before the light, keep going.
				if (z < min(zStart, zEnd) - epsilon)
					continue;
				// If the fragment is past the geometry, stop.
				if (z > max(zStart, zEnd) + epsilon)
					continue;

				#if USE_DEPTH_RANGE
					// Is the fragment depth between the ray's depth range for this pixel?
					// FIXME: This calculation doesn't work for some reason...
					// FIXME: Only way to fix this is to test it on the CPU.
					if (z < min(depthRange.x, depthRange.y) - epsilon)
						continue;
					if (z > max(depthRange.x, depthRange.y) + epsilon)
						continue;
				#endif

				// Otherwise check geometry.
				#if !RAYCAST_DEPTH_ONLY
					if (checkTriangle(id, osFrom, osReal))
						return true;
				#else
					float dist = max(zEnd, z) - min(zEnd, z);
					if (dist < epsilon)
						return false;
					return true;
				#endif
			}
			else
			{
				LFB_FRAG_TYPE data = LFB_GET_DATA(lfb);
				vec4 col = floatToRGBA8(data.x);
				float z = data.y;

				// If the fragment is before the light, keep going.
				if (z < min(zStart, zEnd) - epsilon)
					continue;
				// If the fragment is past the geometry, stop.
				if (z > max(zStart, zEnd) + epsilon)
					continue;

			#if USE_DEPTH_RANGE
				// Is the fragment depth between the ray's depth range for this pixel?
				if (z < min(depthRange.x, depthRange.y) - epsilon)
					continue;
				if (z > max(depthRange.x, depthRange.y) + epsilon)
					continue;
			#endif

				// Otherwise check geometry.
				#if !RAYCAST_DEPTH_ONLY
					if (checkTriangle(id, osFrom, osReal))
						return true;
				#else
					float dist = max(zEnd, z) - min(zEnd, z);
					if (dist < epsilon)
						return false;
					return true;
				#endif
			}
		}
	#endif

		if (tMax.x > 1 && tMax.y > 1)
			break;

		if (tMax.x < tMax.y)
		{
			tMax.x += td.x;
			ssPos.x += s.x;
		}
		else
		{
			tMax.y += td.y;
			ssPos.y += s.y;
		}
	}

	// Didn't hit anything.
	return false;
#endif
	return false;
}



void main()
{
	// TODO: For this to work, we need 2 byte depth, 3 byte color, 3 byte normal.
	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	//uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	//fragColor = getAmbient(material, 0.2) * 0.5;

	if (depth == 0)
		return;

	// TODO: Instead of doing a full path trace. Instead we can trace from the fragment in a number of random directions and accumulate colour.
	// TODO: Kind of similar to cone tracing I guess.

	// TODO: Are we in shadow?
	vec4 osPos = invMvMatrix * vec4(esFrag, 1);
	vec4 osLight = vec4(mainLightPos.xyz, 1);

	vec3 osNorm = normalize((invMvMatrix * vec4(normal, 0)).xyz);

	// TODO: Do I need osNorm or esNorm here?

	// TODO: Pick a bunch of random directions in a hemisphere around the normal, get the colours of the intersected geometry.
	// TODO: Accumulate colour, divide by no. of hits.
	// TODO: Do we differentiate between geometry that is/isn't in shadow?
	int nHits = 0;
	vec4 indirectColor = vec4(0);
	vec3 sampleDir = osNorm;
#if 0
	vec3 v[6];
	v[0] = vec3(0, 1, 0);
	v[1] = vec3(0.707107, 0.707107, 0);
	v[2] = vec3(0.218509, 0.707107, 0.672498);
	v[3] = vec3(-0.57206, 0.707107, 0.415628);
	v[4] = vec3(-0.572063, 0.707107, -0.415625);
	v[5] = vec3(0.218505, 0.707107, -0.672499);
#endif
	for (int i = 0; i < N_SAMPLES; i++)
	{
	#if 1
		// TODO: Maybe replace this with a noise texture?
		float randX = fract(sin(dot(sampleDir.yz, vec2(12.9898, 78.233))) * 43758.5453);
		float randY = fract(sin(dot(sampleDir.xz, vec2(12.9898, 78.233))) * 43758.5453);
		float randZ = fract(sin(dot(sampleDir.xy, vec2(12.9898, 78.233))) * 43758.5453);
	#else
		float randX = v[i].x;
		float randY = v[i].y;
		float randZ = v[i].z;
	#endif

		sampleDir = normalize(osNorm);
		vec3 up = sampleDir.y * sampleDir.y > 0.95f ? vec3(0, 0, 1) : vec3(0, 1, 0);
		vec3 right = normalize(cross(sampleDir, up));
		up = normalize(cross(sampleDir, right));

		sampleDir.x += (randX * right + randZ * up).x;
		sampleDir.y += (randX * right + randZ * up).y;
		sampleDir.z += (randX * right + randZ * up).z;
		sampleDir = normalize(sampleDir);

		if (dot(sampleDir, osNorm) < 0)
			sampleDir = -sampleDir;

		// TODO: Ideally you only add the colour if you hit a surface that is lit.
		// TODO: Maybe cast again from the hit location to the light source and see if anything is in the way.
		vec4 col = vec4(0);
		vec3 hitPos = vec3(0);
		float hitDist = 0;
		//bool hitFlag = raycast(osPos.xyz, sampleDir, col, hitDist);
		bool hitFlag = raycast(osPos.xyz, sampleDir, col, hitPos);
		if (hitFlag)
		{
			// TODO: Determine if the hit location can see the light source or not.
			// TODO: Get the world space position of the pixel, convert to light space.
			// TODO: Check light space depth at the given pixel to see if it's in shadow or not.
		#if USE_RSM
			//vec4 lightFragPos = lightMat * osPos;
			//float cosTheta = clamp(dot(normal, vLightDir), 0.0, 1.0);
			//float bias = 0.005;
			//bias = 0.005 * tan(acos(cosTheta));
			//bias = clamp(bias, 0.0, 0.01);
			vec4 lightHitPos = lightMat * vec4(hitPos.xyz, 1);
			float actualZ = lightHitPos.z;
			//float visibility = 0.3;
			//visibility += texture(shadowMap, vShadowCoord.xyz).x;
			// TODO: Do proper shadows instead of this nonsense.
			vec2 ssLightFrag = getScreenFromClip(lightHitPos, vec2(1)); // This needs to be vec2(2) if you've used a bias matrix.
			float storedZ = texture(rsmLodTex, ssLightFrag, 0).x;
			//indirectColor = vec4(visibility);
			//continue;
			if (actualZ < storedZ)
			{
				indirectColor += col * dot(sampleDir, osNorm);
				nHits++;
			}
		#elif USE_SHADOW_MAP && 0
			vec4 lightHitPos = lightMat * vec4(hitPos.xyz, 1);
			lightHitPos.xyz /= lightHitPos.w;
			float visibility = texture(shadowTex, lightHitPos.xyz).x;
			if (visibility > 0)
			{
				indirectColor += dot(sampleDir, osNorm) * col;
				nHits++;
			}
		#else
			// TODO: Can't just add the color here, needs to take the dot product into account.
			// TODO: What about falloff based on distance?
			//float hitDist = distance(hitPos, osPos.xyz) * 30.0;
			//float att = 1.0 / (hitDist * hitDist);
			//att *= att;
			//float att = 1.0;
			//indirectColor += col * dot(sampleDir, osNorm) * att;
			//indirectColor += vec4(hitDist / 60.0);
			indirectColor += dot(sampleDir, osNorm) * col;
			nHits++;
		#endif
		}
	}
	if (nHits > 0)
	{
		indirectColor /= vec4(nHits);
		indirectColor = clamp(indirectColor, vec4(0), vec4(1));
	}
	else
		indirectColor = vec4(0);

	vec3 viewDir = normalize(-esFrag);
	vec4 esLightPos = mvMatrix * osLight;

	vec4 lightColor = vec4(1);
	float dist = distance(esLightPos.xyz, esFrag);

	float att = 0;
	vec4 directColor = sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, 50, dist, true, att);
	//directColor /= att;

	//fragColor += directColor;
#if USE_RSM
	vec4 lightFrag = lightMat * vec4(osPos.xyz, 1);
	float actualZ = lightFrag.z;
	// TODO: This texture call will only work with an orthographic projection. Will need to use one.
	// TODO: For now just convert lightFrag.xy into a screen space frag coord.
	vec2 ssLightFrag = getScreenFromClip(lightFrag, vec2(1)); // This needs to be vec2(2) if you've used a bias matrix.
	float storedZ = texture(rsmLodTex, ssLightFrag, 0).x;
	if (actualZ < storedZ)
		fragColor += directColor;
	//fragColor = vec4(storedZ);
	//if (texture(rsmLodTex, lightFrag.xy, 0).x < lightFrag.z)
	//	fragColor += directColor;
	//if (lightZ > 0)
	//	fragColor += directColor;
#elif USE_SHADOW_MAP
	vec4 lightFrag = lightMat * vec4(osPos.xyz, 1);
	lightFrag.xyz /= lightFrag.w;
	float visibility = texture(shadowTex, lightFrag.xyz).x;
	if (visibility > 0)
		fragColor += directColor;
#endif
	fragColor += indirectColor * 0.2;

#if 0
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

