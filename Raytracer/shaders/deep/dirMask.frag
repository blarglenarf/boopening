#version 450

#include "../utils.glsl"

coherent buffer RealDirs
{
	vec4 realDirs[];
};

uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;


out vec4 fragColor;


void main()
{
	fragColor = vec4(0);

	// TODO: What if there's no actual geometry here? We don't want to record directions in that case.

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	//uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	//fragColor = getAmbient(material, 0.2) * 0.5;

	if (depth == 0)
	{
		return;
	}

	vec4 osFrag = invMvMatrix * vec4(esFrag, 1);
	vec4 osNorm = invMvMatrix * vec4(normal, 0);

	vec3 lightDir = osFrag.xyz - mainLightPos;
	vec3 rayDir = reflect(normalize(-lightDir), normalize(osNorm.xyz));

	int index = viewport.z * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	realDirs[index] = vec4(rayDir, 0);

	fragColor.xyz = rayDir;
}

