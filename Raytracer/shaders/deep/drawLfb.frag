#version 450

#define DEEP_RES 64

#define LFB_FRAG_TYPE vec2

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

uniform int pixelCheck;

out vec4 fragColor;

#import "lfb"

#include "../utils.glsl"

// Just standard forward rendering.
LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);

void main()
{
	fragColor = vec4(1.0);

	// Composite geometry.
	ivec2 imgSize = ivec2(DEEP_RES);

	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	if (pixel == pixelCheck)
	{
		fragColor = vec4(1, 0, 0, 1);
		return;
	}

	//pixel += layerOffset;

	// Read from global memory and composite.
	// Might as well double check that it's in sorted order.
	float depthCheck = 100000;
	int nFrags = 0;
	for (LFB_ITER_BEGIN(LFB_NAME, pixel); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		//vec4 col = vec4(0, 0, 1, 0.3);
		LFB_FRAG_TYPE f = LFB_GET_DATA(LFB_NAME);
		float d = float(f.y);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, 0.1);
	#if 1
		if (d > depthCheck)
		{
			fragColor = vec4(1, 0, 0, 1);
			return;
		}
		depthCheck = d;
	#endif
		nFrags++;
	}
#if 0
	//float dc = MAX_FRAGS_OVERRIDE/float(_MAX_FRAGS);
	float dc = nFrags / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

