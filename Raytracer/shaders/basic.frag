#version 450

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
	mat3 tbn;
} VertexIn;

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;


uniform bool texFlag;
uniform bool normFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

uniform mat4 mvMatrix;


#include "utils.glsl"
#include "light/light.glsl"


out vec4 fragColor;


vec4 applyLight(vec4 pos, vec4 color, vec3 viewDir)
{
	vec4 lightColor = vec4(0);

	// deferred: vec4 sphereLight(vec3 material, vec4 lightColor, vec4 lightPos, vec3 esFrag, vec3 normal, vec3 viewDir, float lightRadius, float dist)
	// forward: vec4 sphereLight(vec4 lightColor, vec4 lightPos, vec3 esFrag, vec3 normal, vec3 viewDir, float lightRadius, float dist)
	vec4 lightPos = mvMatrix * pos;

	float dist = distance(lightPos.xyz, VertexIn.esFrag);
	if (dist <= 10)
		lightColor += sphereLight(color, lightPos, VertexIn.esFrag, VertexIn.normal, viewDir, 10, dist);
	return lightColor;
}


void main()
{
	vec4 color = vec4(0);

	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	if (normFlag)
	{
		normal = texture2D(normalTex, VertexIn.texCoord).rgb;
		normal = normalize(normal * 2.0 - 1.0);
		normal = normalize(VertexIn.tbn * normal);
	}

	vec4 testLightPos = vec4(1.27416, 5.84406, -0.638992, 1);
	vec4 esLightPos = mvMatrix * testLightPos;
	vec3 lightDir = esLightPos.xyz - VertexIn.esFrag;

	color = getAmbient(0.3);
	color += directionLight(vec4(0.5), lightDir, viewDir, normal);
	//color += applyLight(vec4(2, 0, 2, 1), vec4(1, 1, 1, 0), viewDir);
	//color += applyLight(vec4(-2, 0, 2, 1), vec4(1, 0, 0, 0), viewDir);

	// TODO: Alternative, colors 0-1 map to 0-0.8 and >1 maps to 0.8-1.

	// Check whether color is higher than given threshold.
	// sRGB values: 0.2126, 0.7152, 0.0722
	//vec3 upper = vec3(0.2126, 0.7152, 0.0722);
	// RGB values: 0.299, 0.587, 0.114
	vec3 upper = vec3(0.299, 0.587, 0.114);
	float brightness = dot(color.xyz, upper);

	// TODO: Pick based on brightness of the whole scene (not just the pixel).
	// Doing this after applying bloom.
	//color = toneMapColor(color, 1.0, 1.0);

	//float luminance = dot(luminanceVector, color.xyz);
	//luminance = max(0.0, luminance - brightPassThreshold);
	//color.xyz *= sign(luminance);

#if 0
	// color.
	fragColor[0] = color;

	// bright (can decide on a different brightness val I guess).
	if (brightness > 1)
		fragColor[1] = color;
	else
		fragColor[1] = vec4(0, 0, 0, 1);
#else
	fragColor = color;
#endif
}

