#version 430

#define FRAG_SIZE 2
#define INDEX_BY_TILE 0

// FIXME: Binding 0, offset 0 already used?
uniform layout(binding = 1, offset = 0) atomic_uint interval8;
uniform layout(binding = 2, offset = 0) atomic_uint interval16;
uniform layout(binding = 3, offset = 0) atomic_uint interval32;
uniform layout(binding = 4, offset = 0) atomic_uint interval64;
uniform layout(binding = 5, offset = 0) atomic_uint interval128;
uniform layout(binding = 6, offset = 0) atomic_uint interval256;
uniform layout(binding = 7, offset = 0) atomic_uint interval512;

#import "lfb"

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#if FRAG_SIZE == 1
	LFB_DEC_1(LFB_NAME);
#elif FRAG_SIZE == 4
	LFB_DEC_4(LFB_NAME);
#else
	LFB_DEC_2(LFB_NAME);
#endif

void main()
{
#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif
	int fragCount = int(LFB_COUNT_AT(LFB_NAME, pixel));

	if (fragCount <= 8)
		atomicCounterIncrement(interval8);
	else if (fragCount <= 16)
		atomicCounterIncrement(interval16);
	else if (fragCount <= 32)
		atomicCounterIncrement(interval32);
	else if (fragCount <= 64)
		atomicCounterIncrement(interval64);
	else if (fragCount <= 128)
		atomicCounterIncrement(interval128);
	else if (fragCount <= 256)
		atomicCounterIncrement(interval256);
	else if (fragCount <= 512)
		atomicCounterIncrement(interval512);
	discard;
}

