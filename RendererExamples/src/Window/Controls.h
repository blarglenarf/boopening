#pragma once

class DebugControls
{
public:
	static bool wireframeFlag;
	static bool cullFlag;

public:
	static void toggleWireframe();
	static void toggleCulling();
};
