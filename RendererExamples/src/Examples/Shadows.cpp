#include "../App.h"

#if SHADOWS

#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Math/src/MathCommon.h"

#include <iostream>

using namespace Math;
using namespace Rendering;

const int shadowSize = 1024;

VertexArrayObject vaoCube, vaoGrid, vaoQuad;
Shader shader;
Shader shaderShadow;
Texture2D tex;
RenderBuffer rb;
FrameBuffer fbo;
TextureBuffer justOneInt;
StorageBuffer anotherInt;
int nSamples = 4;

vec3 lightPos(0, 1, 5);
vec3 lightDir(0, -0.5, -1);
mat4 lightProj, lightMV, texMat;

GLuint fboID;

struct MaterialUniform
{
	vec4 ambientMat;
	vec4 diffuseMat;
	vec4 specularMat;
};

UniformBuffer cubeMatUniform, gridMatUniform;
AtomicBuffer atomicCounter;

void App::init()
{
	MaterialUniform cubeMat = { vec4(0.0f, 0.0f, 0.4f, 1.0f), vec4(0.0f, 0.0f, 0.6f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f) };
	MaterialUniform gridMat = { vec4(0.0f, 0.2f, 0.0f, 1.0f), vec4(0.0f, 0.4f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f) };

	cubeMatUniform.create(&cubeMat, sizeof(cubeMat));
	gridMatUniform.create(&gridMat, sizeof(gridMat));

	int one = 1;
	unsigned int uOne = 1;
	justOneInt.create(&one, sizeof(one));
	anotherInt.create(&one, sizeof(one));
	atomicCounter.create(&uOne, sizeof(uOne));

	glEnable(GL_TEXTURE_2D);
	glColor3f(1, 1, 1);

	auto vertShadow = ShaderSourceCache::getShader("shadowVert").loadFromFile("shaders/shadow.vert");
	auto fragShadow = ShaderSourceCache::getShader("shadowFrag").loadFromFile("shaders/shadow.frag");
	shaderShadow.create(&vertShadow, &fragShadow);

	//shaderShadow.bind();
	//auto pos = shaderShadow.getUniform("shadowMap");
	//shaderShadow.unbind();

	lightDir.normalize();

	// Using the camera stuff requires more code, but it means you don't have to guess position/rotations etc.
	// For some reason this doesn't work with a perspective projection... not sure why.
	Camera lightCam;
	lightCam.setType(Camera::Type::ORTHO);
	lightCam.setPos(lightPos);
	lightCam.setViewDir(-lightDir);
	lightCam.setViewport(0, 0, shadowSize, shadowSize);
	lightCam.setOrtho(-5, 5, -5, 5);
	lightCam.setDistance(-10, 10);
	lightCam.update(0.01f);

	lightProj = lightCam.getProjection();
	lightMV = lightCam.getInverse();

	// Alternative is setting up the matrices manually.
	//lightProj = getOrtho<float>(-5, 5, -5, 5, -10, 10);
	//lightMV.identity();
	//lightMV.rotateX((float) pi / 10.0f);
	//lightMV.translate(0, 0, -5);

	auto vertDepth = ShaderSourceCache::getShader("depthVert");
	auto fragDepth = ShaderSourceCache::getShader("depthFrag");
	shader.create(&vertDepth, &fragDepth);

	auto cube = Mesh::getSphere();
	auto grid = Mesh::getGrid();
	auto quad = Mesh::getSquare();

	vaoCube.create(&cube);
	vaoGrid.create(&grid);
	vaoQuad.create(&quad);

	fbo.setSize(shadowSize, shadowSize);
	tex.create(nullptr, GL_DEPTH_COMPONENT16, shadowSize, shadowSize);
	tex.setFilters(GL_CLAMP_TO_EDGE);
	//rb.create(GL_DEPTH24_STENCIL8, shadowSize, shadowSize);

	fbo.generate();
	fbo.bind();
	//fbo.attachColorBuffer(&tex);
	fbo.attachDepthBuffer(&tex);
	fbo.unbind();

	tex.bind();
	tex.useAsShadowMap();
	tex.unbind();

	Renderer::instance().getActiveCamera().updateZoom(20);
	Renderer::instance().getActiveCamera().updateRotation(30, -30);
}

void App::render()
{
	fbo.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, shadowSize, shadowSize);

	// Render scene from light position
	shader.bind();
	shader.setUniform("mvMatrix", lightMV);
	shader.setUniform("pMatrix", lightProj);

	glCullFace(GL_FRONT);
	vaoCube.render();
	vaoGrid.render();
	glCullFace(GL_BACK);

	shader.unbind();
	fbo.unbind();

	// Render scene from camera
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#if 1
	auto &camera = Renderer::instance().getActiveCamera();
	camera.upload();
	
	Renderer::instance().drawAxes();

	shaderShadow.bind();
	tex.bind();

	texMat = lightMV * lightProj;
	texMat = texMat * getBias<float>();

	shaderShadow.setUniform("mvMatrix", camera.getInverse());
	shaderShadow.setUniform("pMatrix", camera.getProjection());

	shaderShadow.setUniform("tMatrix", texMat);
	shaderShadow.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	shaderShadow.setUniform("lightPos", lightPos);
	shaderShadow.setUniform("shadowMap", &tex);
	
	shaderShadow.setUniform("justOneInt", &justOneInt);
	shaderShadow.setUniform("AnotherInt", &anotherInt);
	shaderShadow.setUniform("atomicCounter", &atomicCounter);

	shaderShadow.setUniform("MaterialBlock", &cubeMatUniform);
	
	vaoCube.render();

	shaderShadow.setUniform("MaterialBlock", &gridMatUniform);

	vaoGrid.render();

	tex.unbind();
	shaderShadow.unbind();

#if 0
	int tmp = *((int*) justOneInt.read());
	int anotherTmp = *((int*) anotherInt.read());
	unsigned int uTmp = *((int*) atomicCounter.read());

	std::cout << tmp << ", " << anotherTmp << ", " << uTmp << "\n";

	int one = 1;
	anotherInt.bufferData(&one, sizeof(int));
	atomicCounter.bufferData(&one, sizeof(int));
#endif

#else
	tex.blit();
#endif
}

void App::update(float dt)
{
	(void) (dt);
}

void App::loadNextMesh() {}
void App::loadPrevMesh() {}
void App::useNextLFB() {}
void App::usePrevLFB() {}


#endif
