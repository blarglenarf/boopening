#include "../App.h"

#if MULTISAMPLE

#include "../../Renderer/src/RendererCommon.h"

Rendering::VertexArrayObject vaoCube, vaoGrid, vaoQuad;
Rendering::Shader shader;
Rendering::Shader shaderMS;
Rendering::Texture tex;
Rendering::FrameBufferObject fbo(512, 512);
int nSamples = 4;

GLuint fboID;

void App::init()
{
	glEnable(GL_TEXTURE_2D);
	glColor3f(1, 1, 1);
	
	auto vert = Rendering::ShaderSource::getPhongVertShader();
	auto frag = Rendering::ShaderSource::getPhongFragShader();
	shader.create(&vert, &frag);

	auto msVert = Rendering::ShaderSource::getMultiSampleVertShader();
	auto msFrag = Rendering::ShaderSource::getMultiSampleFragShader();
	shaderMS.create(&msVert, &msFrag);

	auto cube = Rendering::Mesh::getSphere();
	auto grid = Rendering::Mesh::getGrid();
	auto quad = Rendering::Mesh::getSquare();

	vaoCube.create(&cube);
	vaoGrid.create(&grid);
	vaoQuad.create(&quad);

	tex.toggleMultisampling(nSamples);
	fbo.create(&tex, GL_RGBA, true, nSamples);

	Rendering::Renderer::instance().getActiveCamera().updateZoom(-20);
	Rendering::Renderer::instance().getActiveCamera().updateRotation(30, 30);
}

void App::render()
{
	fbo.bind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	auto &camera = Rendering::Renderer::instance().getActiveCamera();
	camera.upload();

	glViewport(0, 0, 512, 512);

	shader.bind();
	// TODO: not get the uniform locations every frame
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose()); // Transpose of inverse of modelview

	shader.setUniform("ambientMat", Math::vec3f(0.2f, 0.0f, 0.0f));
	shader.setUniform("diffuseMat", Math::vec3f(0.5f, 0.0f, 0.0f));
	shader.setUniform("specularMat", Math::vec3f(1, 1, 1));

	vaoCube.render();
	vaoGrid.render();

	shader.unbind();
	
	Rendering::Renderer::instance().drawAxes();
	fbo.unbind();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	camera.upload();

#if 0
	fbo.render();
#else
	tex.bind();
	shaderMS.bind();
	shaderMS.setUniform("mvMatrix", Math::mat4f::getIdentity());
	shaderMS.setUniform("pMatrix", Math::mat4f::getIdentity());
	shaderMS.setUniform("texture", tex);
	shaderMS.setUniform("nSamples", nSamples);

	vaoQuad.render();

	shaderMS.unbind();
	tex.unbind();
#endif
}

void App::update(float dt)
{
	(void) (dt);
}


#endif

