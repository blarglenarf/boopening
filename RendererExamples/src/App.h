#pragma once

#define STANDARD 0
#define SHADOWS 1
#define MULTISAMPLE 0

class App
{
private:
	int currLFB;
	int currMesh;

	bool transparencyFlag;

public:
	App() : currLFB(1), currMesh(0), transparencyFlag(false) {}
	~App() {}

	void init();

	void render();
	void update(float dt);

	void toggleTransparency() { transparencyFlag = !transparencyFlag; }

	void loadNextMesh();
	void loadPrevMesh();

	void useNextLFB();
	void usePrevLFB();
};

