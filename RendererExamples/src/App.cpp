#include "App.h"

#if STANDARD

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Renderer/src/LFB/LFB.h"

#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include <iostream>
#include <stdexcept>
#include <vector>


using namespace Math;
using namespace Rendering;

VertexArrayObject vaoMesh;
VertexBuffer vboLights;

Shader captureShader, compositeShader, basicShader, lightCaptureShader;

UniformBuffer sphereMatUniform, gridMatUniform, meshMatUniform;
StorageBuffer lightPosBuffer, lightColorBuffer;

float fpsUpdate = 0;

std::vector<vec3> lightPositions;
std::vector<vec3> lightColors;

#define DEFERRED 0

struct MaterialUniform
{
	vec4 ambientMat;
	vec4 diffuseMat;
	vec4 specularMat;
};

LFB geometryLFB;

static void createLights()
{
	lightPositions = {{0, 0.5f, 0}, {1, 0.5f, 0}, {-1, 0.5f, 0}, {0, 0.5f, 1}, {0, 0.5f, -1}};
	lightColors = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}, {1, 1, 0}, {0, 1, 1}};
}


void App::init()
{
	useNextLFB();

	MaterialUniform sphereMat = { vec4(0.0f, 0.0f, 0.2f, 1.0f), vec4(0.0f, 0.0f, 0.6f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f) };
	MaterialUniform gridMat = { vec4(0.0f, 0.2f, 0.0f, 1.0f), vec4(0.0f, 0.4f, 0.0f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f) };
	MaterialUniform meshMat = { vec4(0.2f, 0.2f, 0.2f, 1.0f), vec4(0.2f, 0.2f, 0.2f, 1.0f), vec4(1.0f, 1.0f, 1.0f, 1.0f) };

	sphereMatUniform.create(&sphereMat, sizeof(sphereMat));
	gridMatUniform.create(&gridMat, sizeof(gridMat));
	meshMatUniform.create(&meshMat, sizeof(meshMat));

	auto vertBasic = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto geomBasic = ShaderSourceCache::getShader("basicGeom").loadFromFile("shaders/basic.geom");
	auto fragBasic = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.create(&vertBasic, &fragBasic, &geomBasic);

	loadNextMesh();

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glColor3f(1, 1, 1);

	Renderer::instance().getActiveCamera().updateZoom(20);
	Renderer::instance().getActiveCamera().updateRotation(30, -30);

	auto vertLight = ShaderSourceCache::getShader("lightVert").loadFromFile("shaders/light.vert");
	auto geomLight = ShaderSourceCache::getShader("lightGeom").loadFromFile("shaders/light.geom");
	auto fragLight = ShaderSourceCache::getShader("lightFrag").loadFromFile("shaders/light.frag");
	lightCaptureShader.create(&vertLight, &fragLight, &geomLight);
	
	createLights();
	vboLights.setMode(GL_POINTS);
	vboLights.create(VertexFormat({ {VertexAttrib::POSITION, 0, 3, GL_FLOAT} }), &lightPositions[0], lightPositions.size() * sizeof(vec3), sizeof(vec3));
	glEnableVertexAttribArray(0);

	lightPosBuffer.create(&lightPositions[0], lightPositions.size() * sizeof(vec3));
	lightColorBuffer.create(&lightColors[0], lightColors.size() * sizeof(vec3));
}

void App::render()
{
	auto &camera = Renderer::instance().getActiveCamera();
	camera.upload();

	if (transparencyFlag)
	{
		// Transparency Rendering.
		geometryLFB.resize(camera.getWidth(), camera.getHeight());

		if (geometryLFB.getType() == LFB::LINEARIZED)
		{
			// Count the number of frags for capture.
			auto &countShader = geometryLFB.beginCountFrags();
			countShader.setUniform("mvMatrix", camera.getInverse());
			countShader.setUniform("pMatrix", camera.getProjection());
			vaoMesh.render();
			geometryLFB.endCountFrags();
		}

		captureShader.bind();
		captureShader.setUniform("mvMatrix", camera.getInverse());
		captureShader.setUniform("pMatrix", camera.getProjection());
		captureShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	#if !DEFERRED
		//captureShader.setUniform("lightDir", vec3(0, 0, 1));
		captureShader.setUniform("nLights", (unsigned int) lightPositions.size());
		captureShader.setUniform("LightPositions", &lightPosBuffer);
		captureShader.setUniform("LightColors", &lightColorBuffer);
	#endif

		// Capture fragments into the lfb.
		geometryLFB.beginCapture();
		captureShader.bind();
	#if !DEFERRED
		captureShader.setUniform("MaterialBlock", &meshMatUniform);
	#endif
		vaoMesh.render();

		// Sometimes may need to re-render if the lfb allocated too much/too little.
		if (geometryLFB.endCapture())
		{
			geometryLFB.beginCapture();

		#if !DEFERRED
			captureShader.setUniform("MaterialBlock", &meshMatUniform);
		#endif
			vaoMesh.render();

			if (geometryLFB.endCapture())
				std::cout << "Something very very wrong just happened...\n";
		}
		captureShader.unbind();

		// Sort and composite.
		compositeShader.bind();
	#if DEFERRED
		compositeShader.setUniform("lightDir", vec3(0, 0, 1));
		compositeShader.setUniform("viewport", camera.getViewport());
		compositeShader.setUniform("pMatrix", camera.getProjection());
		compositeShader.setUniform("invPMatrix", camera.getProjection().getInverse());
		compositeShader.setUniform("MaterialBlock", &meshMatUniform);
	#endif
		geometryLFB.composite(&compositeShader);
		compositeShader.unbind();

		//auto s = geometryLFB.getStrSorted();
		//Utils::File f("lfb" + Utils::toString(c) + ".txt");
		//f.writeLine(s);
	}
	else
	{
		// Opaque Rendering.
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		basicShader.bind();
		basicShader.setUniform("mvMatrix", camera.getInverse());
		basicShader.setUniform("pMatrix", camera.getProjection());
		basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		basicShader.setUniform("lightDir", vec3(0, 0, 1));

		captureShader.setUniform("MaterialBlock", &meshMatUniform);
		vaoMesh.render();

		basicShader.unbind();

		// Render light volumes.
		lightCaptureShader.bind();
		lightCaptureShader.setUniform("mvMatrix", camera.getInverse());
		lightCaptureShader.setUniform("pMatrix", camera.getProjection());
		vboLights.bind();
		vboLights.render();
		vboLights.unbind();
		lightCaptureShader.unbind();

		glPopAttrib();
	}

	Renderer::instance().drawAxes();

	fpsUpdate += Platform::Window::getDeltaTime();
	if (fpsUpdate > 2)
	{
		std::cout << Platform::Window::getDeltaTime() * 1000 << "ms\n";
		fpsUpdate = 0;
	}
}

void App::update(float dt)
{
	(void) (dt);
}


static int useMesh(int currMesh)
{
	static std::vector<std::string> meshes = { "meshes/galleon.obj", "meshes/sponza/sponza.3ds", "meshes/powerplant/powerplant.ctm", "meshes/hairball.ctm" };

	if (currMesh > (int) meshes.size() - 1)
		currMesh = 0;
	else if (currMesh < 0)
		currMesh = (int) meshes.size() - 1;

	vaoMesh.release();

	std::cout << "Loading mesh: " << meshes[currMesh] << "\n";

	Mesh mesh;
	if (Resources::MeshLoader::load(&mesh, meshes[currMesh]))
	{
		if (currMesh == 2)
			mesh.scale(Math::vec3(0.001f, 0.001f, 0.001f));
		else if (currMesh == 3)
			mesh.scale(Math::vec3(-100, -100, -100), Math::vec3(100, 100, 100));
		vaoMesh.create(&mesh);
	}

	return currMesh;
}

void App::loadNextMesh()
{
	currMesh = useMesh(currMesh + 1);
}

void App::loadPrevMesh()
{
	currMesh = useMesh(currMesh - 1);
}


static int useLFB(int currLFB)
{
	static std::vector<LFB::LFBType> types = { LFB::LINK_LIST, LFB::LINEARIZED };
	static std::vector<std::string> typeStrs = { "Link List", "Linearized" };

	if (currLFB > (int) types.size() - 1)
		currLFB = 0;
	else if (currLFB < 0)
		currLFB = (int) types.size() - 1;

	std::cout << "Using LFB: " << typeStrs[currLFB] << "\n";
	geometryLFB.setType(types[currLFB]);

#if DEFERRED
	auto vertCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/captureDeferred.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/captureDeferred.frag");
	auto vertComp = ShaderSourceCache::getShader("compVert").loadFromFile("shaders/compositeDeferred.vert");
	auto fragComp = ShaderSourceCache::getShader("compFrag").loadFromFile("shaders/compositeDeferred.frag");
#else
	auto vertCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/capture.frag");
	auto vertComp = ShaderSourceCache::getShader("compVert").loadFromFile("shaders/composite.vert");
	auto fragComp = ShaderSourceCache::getShader("compFrag").loadFromFile("shaders/composite.frag");
#endif
	captureShader.release();
	captureShader.create(&vertCapture, &fragCapture);

	compositeShader.release();
	compositeShader.create(&vertComp, &fragComp);

	return currLFB;
}

void App::useNextLFB()
{
	currLFB = useLFB(currLFB + 1);
}

void App::usePrevLFB()
{
	currLFB = useLFB(currLFB - 1);
}

#endif

