#ifndef UTILS_GLSL
#define UTILS_GLSL

uint rgba8ToUInt(vec4 v)
{
	uvec4 i = clamp(uvec4(v * 255.0), 0U, 255U);
	return (i.x << 24U) | (i.y << 16U) | (i.z << 8U) | i.w;
}

float rgba8ToFloat(vec4 v)
{
	uint i = rgba8ToUInt(v);
	return uintBitsToFloat(i);
}

vec4 uintToRGBA8(uint i)
{
	return vec4((float(i >> 24U)) / 255.0f,
		(float((i >> 16U) & 0xFFU)) / 255.0f,
		(float((i >> 8U) & 0xFFU)) / 255.0f,
		(float(i & 0xFFU)) / 255.0f
	);
}

vec4 floatToRGBA8(float f)
{
	uint i = floatBitsToUint(f);
	return uintToRGBA8(i);
}


#endif

