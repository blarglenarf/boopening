#version 450

uniform MaterialBlock
{
	vec4 ambientMat;
	vec4 diffuseMat;
	vec4 specularMat;
} Material;

in VertexData
{
	vec3 normal;
	//vec3 lightDir;
	vec3 viewDir;
	vec3 esFrag;
} VertexIn;

#import "lfb"

#include "utils.glsl"

LFB_DEC(lfb);


uniform uint nLights;
uniform mat4 mvMatrix;

readonly buffer LightPositions
{
	vec3 lightPositions[];
};

readonly buffer LightColors
{
	vec3 lightColors[];
};

// TODO: draw points for each light.


void main()
{
	float lightRadius = 2;
	vec4 color = vec4(Material.ambientMat.xyz, 0.2);

	for (int i = 0; i < nLights; i++)
	{
		vec4 lightPos = mvMatrix * vec4(lightPositions[i], 1);
		vec3 lightDir = lightPos.xyz - VertexIn.esFrag;
		float dist = distance(lightPos.xyz, VertexIn.esFrag);

		if (dist > lightRadius)
			continue;

		float cosTheta = clamp(dot(VertexIn.normal, lightDir), 0.0, 1.0);

		//vec3 ambient = Material.ambientMat.xyz;
		vec3 diffuse = vec3(0.0);
		vec3 specular = vec3(0.0);

		float dp = max(cosTheta, 0.0);

		if (dp > 0.0)
		{
			diffuse = Material.diffuseMat.xyz * dp * lightColors[i];
			vec3 reflection = reflect(-lightDir, VertexIn.normal);
			float nDotH = max(dot(normalize(VertexIn.viewDir), normalize(reflection)), 0.0);
			float shininess = 64.0;
			float intensity = pow(nDotH, shininess);
			specular = Material.specularMat.xyz * intensity * lightColors[i];

			float att = max(0, 1 - (dist * dist) / (lightRadius * lightRadius));
			att *= att;
			color += vec4(att * (diffuse + specular), 0);
		}

		//vec4 color = vec4(ambient + diffuse + specular, 0.2);
	}

	LFB_ADD_DATA(lfb, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
}

