#version 450

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat4 tMatrix;
uniform mat3 nMatrix;

uniform vec3 lightPos;

out vec4 vShadowCoord;
out vec3 vNormal;
out vec3 vLightDir;
out vec3 vViewDir;

void main()
{
    vec4 osVert = vec4(vertex, 1.0);
    vec4 esVert = mvMatrix * osVert;
    vec4 csVert = pMatrix * esVert;

    vShadowCoord = tMatrix * osVert;
    vec3 esLightPos = (mvMatrix * vec4(lightPos, 1)).xyz;
    vLightDir = esLightPos - esVert.xyz;
    vViewDir = -esVert.xyz;
    vNormal = nMatrix * normalize(normal);

    gl_Position = csVert;
}

