#version 450

in vec4 vShadowCoord;

in vec3 vNormal;
in vec3 vLightDir;
in vec3 vViewDir;

uniform sampler2DShadow shadowMap;

uniform vec3 lightPos;

uniform MaterialBlock
{
	vec4 ambientMat;
	vec4 diffuseMat;
	vec4 specularMat;
} Material;

out vec4 fragColor;

uniform layout(r32i) iimageBuffer justOneInt;
uniform layout(binding = 0, offset = 0) atomic_uint atomicCounter;

buffer AnotherInt
{
	int anotherInt;
};

vec2 poissonDisk[16] = vec2[16](
    vec2(-0.94201624,  -0.39906216),
    vec2( 0.94558609,  -0.76890725),
    vec2(-0.094184101, -0.92938870),
    vec2( 0.34495938,   0.29387760),
    vec2(-0.91588581,   0.45771432),
    vec2(-0.81544232,  -0.87912464),
    vec2(-0.38277543,   0.27676845),
    vec2( 0.97484398,   0.75648379),
    vec2( 0.44323325,  -0.97511554),
    vec2( 0.53742981,  -0.47373420),
    vec2(-0.26496911,  -0.41893023),
    vec2( 0.79197514,   0.19090188),
    vec2(-0.24188840,   0.99706507),
    vec2(-0.81409955,   0.91437590),
    vec2( 0.19984126,   0.78641367),
    vec2( 0.14383161,  -0.14100790)
);

float random(vec3 seed, int i)
{
    vec4 seed4 = vec4(seed, i);
    float dp = dot(seed4, vec4(12.9898, 78.233, 45.164, 94.673));
    return fract(sin(dp) * 43758.5453);
}

void main()
{
	float cosTheta = clamp(dot(vNormal, vLightDir), 0.0, 1.0);

	float bias = 0.005;
	bias = 0.005 * tan(acos(cosTheta));
	bias = clamp(bias, 0.0, 0.01);

#if 0
	// Can either start fully visible and darken, or start dark and add visibility. Both give the same result.
	#if 0
		float visibility = 1.0;
		for (int i = 0; i < 16; i++)
		{
			//int index = int(16.0 * random(gl_FragCoord.xyz, i)) % 16;
			int index = i;
			visibility -= 0.05 * (1.0 - texture(shadowMap, vec3(vShadowCoord.xy + poissonDisk[index] / 700.0, (vShadowCoord.z - bias) / vShadowCoord.w)));
		}
	#else
		float visibility = 0.3;
		for (int i = 0; i < 16; i++)
		{
			int index = i;
			visibility += 0.05 * (texture(shadowMap, vec3(vShadowCoord.xy + poissonDisk[index] / 700.0, (vShadowCoord.z - bias) / vShadowCoord.w)));
		}
	#endif
#else
	float visibility = 0.3;
	visibility += texture(shadowMap, vShadowCoord.xyz).x;
	//if (texture(shadowMap, vShadowCoord.xy, 0).x < vShadowCoord.z)
	//	visibility = 0.3;
#endif
	visibility = clamp(visibility, 0.3, 1.0);

	vec3 ambient = Material.ambientMat.xyz;
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float dp = max(cosTheta, 0.0);
	if (dp > 0.0)
	{
		diffuse = Material.diffuseMat.xyz * dp;
		vec3 reflection = reflect(-vLightDir, vNormal);
		float nDotH = max(dot(normalize(vViewDir), normalize(reflection)), 0.0);
		float shininess = 128.0;
		float intensity = pow(nDotH, shininess);
		specular = Material.specularMat.xyz * intensity;
	}

	vec3 shadowColor = visibility * diffuse + visibility * specular;
	vec4 color = vec4(ambient + shadowColor, 1.0);

	imageStore(justOneInt, 0, ivec4(54));
	atomicAdd(anotherInt, 1);
	atomicCounterIncrement(atomicCounter);

	fragColor = color;
}

