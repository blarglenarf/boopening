#version 450

out vec4 fragColor;

#import "lfb"

#include "utils.glsl"

LFB_DEC(lfb);


#define MAX_FRAGS 64
vec2 frags[MAX_FRAGS];

void main()
{
	int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	// Load into local array.
	int fragCount = 0;
	for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		if (fragCount >= MAX_FRAGS)
			break;
		else
		{
			frags[fragCount++] = LFB_GET_DATA(lfb);
		}
	}

	// Sort.
	for (int i = 1; i < fragCount; i++)
	{
		vec2 f = frags[i];
		int j = i - 1;
		while (j >= 0 && frags[j].y > f.y)
		{
			frags[j + 1] = frags[j];
			j--;
		}
		frags[j + 1] = f;
	}

	// Composite.
	fragColor = vec4(0.0);
	for (int i = fragCount - 1; i >= 0; i--)
	{
		vec2 f = frags[i];
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
}

