#version 450

layout(points) in;
layout(triangle_strip, max_vertices = 60) out;

uniform mat4 pMatrix;
uniform mat4 mvMatrix;

in VertexData
{
	vec3 osVert;
} VertexIn[1];

out VertexData
{
	vec3 esVert;
} VertexOut;

// TODO: look at icosphere code from http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html.
// TODO: normalise vertices.

#define TRI(v1, v2, v3) \
	esVert = mvMatrix * vec4(verts[v1] + VertexIn[0].osVert, 1); \
	VertexOut.esVert = esVert.xyz; \
	gl_Position = pMatrix * esVert; \
	EmitVertex(); \
	esVert = mvMatrix * vec4(verts[v2] + VertexIn[0].osVert, 1); \
	VertexOut.esVert = esVert.xyz; \
	gl_Position = pMatrix * esVert; \
	EmitVertex(); \
	esVert = mvMatrix * vec4(verts[v3] + VertexIn[0].osVert, 1); \
	VertexOut.esVert = esVert.xyz; \
	gl_Position = pMatrix * esVert; \
	EmitVertex(); \
	EndPrimitive();

void main()
{
	// Create 12 vertices of a icosahedron.
	float size = 0.1f;
	float t = (1.0f + sqrt(5.0f)) / 2.0f * size;
	vec4 esVert;

	vec3 verts[12];
	verts[0]  = vec3(-size,  t,  0);
	verts[1]  = vec3( size,  t,  0);
	verts[2]  = vec3(-size, -t,  0);
	verts[3]  = vec3( size, -t,  0);
	verts[4]  = vec3( 0, -size,  t);
	verts[5]  = vec3( 0,  size,  t);
	verts[6]  = vec3( 0, -size, -t);
	verts[7]  = vec3( 0,  size, -t);
	verts[8]  = vec3( t,  0, -size);
	verts[9]  = vec3( t,  0,  size);
	verts[10] = vec3(-t,  0, -size);
	verts[11] = vec3(-t,  0,  size);

	// Create 20 triangles of the icosahedron.

	// 5 faces around point 0.
	TRI(0, 11, 5);
	TRI(0, 5, 1);
	TRI(0, 1, 7);
	TRI(0, 7, 10);
	TRI(0, 10, 11);

	// 5 adjacent faces.
	TRI(1, 5, 9);
	TRI(5, 11, 4);
	TRI(11, 10, 2);
	TRI(10, 7, 6);
	TRI(7, 1, 8);

	// 5 faces around point 3.
	TRI(3, 9, 4);
	TRI(3, 4, 2);
	TRI(3, 2, 6);
	TRI(3, 6, 8);
	TRI(3, 8, 9);

	// 5 adjacent faces.
	TRI(4, 9, 5);
	TRI(2, 4, 11);
	TRI(6, 2, 10);
	TRI(8, 6, 7);
	TRI(9, 8, 1);
}

