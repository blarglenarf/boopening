#version 450

out vec4 fragColor;

#import "lfb"

#include "utils.glsl"

uniform vec3 lightDir;
uniform ivec4 viewport;

uniform mat4 pMatrix;
uniform mat4 invPMatrix;

uniform MaterialBlock
{
	vec4 ambientMat;
	vec4 diffuseMat;
	vec4 specularMat;
} Material;


#define MAX_FRAGS 64
vec2 frags[MAX_FRAGS];

vec4 getEyeFromWindow(vec3 p)
{
	vec3 ndcPos;
	ndcPos.xy = ((2.0 * p.xy) - (2.0 * viewport.xy)) / (viewport.zw) - 1;
	//ndcPos.z = (2.0 * p.z - gl_DepthRange.near - gl_DepthRange.far) / (gl_DepthRange.far - gl_DepthRange.near);
	ndcPos.z = (2.0 * p.z - gl_DepthRange.far - gl_DepthRange.near) / (gl_DepthRange.far - gl_DepthRange.near) - 1;

	vec4 clipPos;
	clipPos.w = pMatrix[3][2] / (ndcPos.z - (pMatrix[2][2] / pMatrix[2][3]));
	clipPos.xyz = ndcPos * clipPos.w;

	return invPMatrix * clipPos;
}


void main()
{
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	// Load into local array.
	int fragCount = 0;
	for (LFB_ITER_BEGIN(pixel); LFB_ITER_CHECK(); LFB_ITER_INC())
	{
		if (fragCount >= MAX_FRAGS)
			break;
		else
		{
			frags[fragCount++] = LFB_GET_DATA();
		}
	}

	// Sort.
	for (int i = 1; i < fragCount; i++)
	{
		vec2 f = frags[i];
		int j = i - 1;
		while (j >= 0 && frags[j].y > f.y)
		{
			frags[j + 1] = frags[j];
			j--;
		}
		frags[j + 1] = f;
	}

	// Composite.
	fragColor = vec4(0.0);
	for (int i = fragCount - 1; i >= 0; i--)
	{
		vec2 f = frags[i];
		vec4 d = floatToRGBA8(f.x);
		vec3 n = d.xyz;

		// Coords are 0 to 1, convert to -1 to 1.
		n.x = n.x * 2.0 - 1.0;
		n.y = n.y * 2.0 - 1.0;
		n.z = n.z * 2.0 - 1.0;

		float cosTheta = clamp(dot(n, lightDir), 0.0, 1.0);

		vec3 ambient = Material.ambientMat.xyz;
		vec3 diffuse = vec3(0.0);
		vec3 specular = vec3(0.0);

		float dp = max(cosTheta, 0.0);

		if (dp > 0.0)
		{
			vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, f.y)).xyz;
			vec3 viewDir = -esFrag;
			diffuse = Material.diffuseMat.xyz * dp;
			vec3 reflection = reflect(-lightDir, n);
			float nDotH = max(dot(normalize(viewDir), normalize(reflection)), 0.0);
			float shininess = 128.0;
			float intensity = pow(nDotH, shininess);
			specular = Material.specularMat.xyz * intensity;
		}

		vec4 col = vec4(ambient + diffuse + specular, d.w);

		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
}
