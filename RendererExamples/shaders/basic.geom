#version 450

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec3 viewDir;
} VertexIn[3];

out VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec3 viewDir;
} VertexOut;


void main()
{
	for (int i = 0; i < gl_in.length(); i++)
	{
		gl_Position = gl_in[i].gl_Position;
		VertexOut.normal = VertexIn[i].normal;
		VertexOut.esFrag = VertexIn[i].esFrag;
		VertexOut.viewDir = VertexIn[i].viewDir;

		EmitVertex();
	}
}

