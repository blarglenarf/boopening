#version 450

in vec3 vNormal;
in vec3 esFrag;

#import "lfb"

#include "utils.glsl"


void main()
{
	vec4 d = vec4(vNormal, 0.2);

	// TODO: come up with a better way of packing this!!!
	// Convert to coords between 0 and 1, then pack into a float.
	d.x = (d.x + 1.0) * 0.5;
	d.y = (d.y + 1.0) * 0.5;
	d.z = (d.z + 1.0) * 0.5;
	addFragToLFB(vec2(rgba8ToFloat(d), -esFrag.z));
}

