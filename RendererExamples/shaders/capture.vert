#version 450

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;

//uniform vec3 lightDir;

out VertexData
{
	vec3 normal;
	//vec3 lightDir;
	vec3 viewDir;
	vec3 esFrag;
} VertexOut;

void main()
{
	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

    VertexOut.esFrag = esVert.xyz;

	//VertexOut.lightDir = lightDir;
	//VertexOut.lightDir = (mvMatrix * vec4(lightDir, 0)).xyz;
	VertexOut.viewDir = -esVert.xyz;
	VertexOut.normal = nMatrix * normalize(normal);

	gl_Position = csVert;
}

