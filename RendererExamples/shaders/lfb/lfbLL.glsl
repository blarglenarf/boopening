#ifndef LFB_LL_GLSL
#define LFB_LL_GLSL

#define STORAGE_BUFFERS 0

struct LFBIterator
{
	uint node;
};


#if STORAGE_BUFFERS

// No extra atomic counters, since binding must be hard coded...
uniform layout(binding = 0, offset = 0) atomic_uint fragCount;

#define LFB_DEC(name) \
\
coherent buffer HeadPtrs##name \
{ \
	uint headPtrs##name[]; \
}; \
\
buffer NextPtrs##name \
{ \
	uint nextPtrs##name[]; \
}; \
\
buffer Data##name \
{ \
	vec2 data##name[]; \
}; \
\
LFBIterator iter##name; \
uniform ivec2 size##name; \
uniform uint fragAlloc##name;

#define LFB_ITER_BEGIN(name, pixel) iter##name.node = headPtrs##name[pixel]
#define LFB_ITER_INC(name) iter##name.node = nextPtrs##name[iter##name.node]
#define LFB_ITER_CHECK(name) iter##name.node != 0
#define LFB_GET_DATA(name) data##name[iter##name.node]
#define LFB_GET_SIZE(name) size##name
#define LFB_ADD_DATA(name, frag) \
	uint currFrag = atomicCounterIncrement(fragCount); \
	if (currFrag < fragAlloc##name) \
	{ \
		uint pixel = size##name.x * uint(gl_FragCoord.y) + uint(gl_FragCoord.x); \
		uint currHead = atomicExchange(headPtrs##name[pixel], currFrag); \
		nextPtrs##name[currFrag] = currHead; \
		data##name[currFrag] = frag; \
	}


#else


// No extra atomic counters, since binding must be hard coded...
uniform layout(binding = 0, offset = 0) atomic_uint fragCount;

#define HEAD_TYPE layout(r32ui) coherent uimageBuffer
#define NEXT_TYPE layout(r32ui) uimageBuffer
#define DATA_TYPE layout(rg32f) imageBuffer

#define LFB_DEC(name) \
\
uniform HEAD_TYPE HeadPtrs##name; \
uniform NEXT_TYPE NextPtrs##name; \
uniform DATA_TYPE Data##name; \
\
LFBIterator iter##name; \
uniform ivec2 size##name; \
uniform uint fragAlloc##name;

void addFragToLFB(in ivec2 size, in vec2 frag, in uint alloc, in HEAD_TYPE HeadPtrs, in NEXT_TYPE NextPtrs, in DATA_TYPE Data)
{
	uint currFrag = atomicCounterIncrement(fragCount);
	if (currFrag < alloc)
	{
		int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
		uint currHead = imageAtomicExchange(HeadPtrs, pixel, currFrag).r;

		imageStore(NextPtrs, int(currFrag), uvec4(currHead));
		imageStore(Data, int(currFrag), vec4(frag, 0, 0));
	}
}

#define LFB_ITER_BEGIN(name, pixel) iter##name.node = imageLoad(HeadPtrs##name, int(pixel)).r
#define LFB_ITER_INC(name) iter##name.node = imageLoad(NextPtrs##name, int(iter##name.node)).r
#define LFB_ITER_CHECK(name) iter##name.node != 0
#define LFB_GET_DATA(name) imageLoad(Data##name, int(iter##name.node)).rg
#define LFB_ADD_DATA(name, frag) addFragToLFB(size##name, frag, fragAlloc##name, HeadPtrs##name, NextPtrs##name, Data##name)
#define LFB_GET_SIZE(name) size##name

#endif
#endif


