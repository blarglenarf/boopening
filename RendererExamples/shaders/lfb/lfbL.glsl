#ifndef LFB_L_GLSL
#define LFB_L_GLSL

#define STORAGE_BUFFERS 0

// So that data will be in the same order as the link list lfb.
#define LFB_REVERSE 1

struct LFBIterator
{
	uint start;
	uint node;
	uint end;
};


// Note: These can't be passed as args to functions...
#if STORAGE_BUFFERS

#define LFB_DEC(name) \
\
coherent buffer Offsets##name \
{ \
	uint offsets##name[]; \
}; \
\
buffer Data##name \
{ \
	vec2 data##name[]; \
}; \
\
LFBIterator iter##name; \
uniform ivec2 size##name;

#define LFB_ITER_BEGIN(name, pixel) iter##name.start = pixel > 0 ? offsets##name[pixel - 1] : 0, iter##name.end = offsets##name[pixel], iter##name.node = 0
#define LFB_ITER_INC(name) iter##name.node++
#define LFB_ITER_CHECK(name) iter##name.node + iter##name.start < iter##name.end

#if LFB_REVERSE
#define LFB_GET_DATA(name) data##name[iter##name.end - 1 - iter##name.node]
#else
#define LFB_GET_DATA(name) data##name[iter##name.node + iter##name.start]
#endif

#define LFB_GET_SIZE(name) size##name
#define LFB_ADD_DATA(name, frag) \
	int pixel = size##name.x * int(gl_FragCoord.y) + int(gl_FragCoord.x); \
	uint index = atomicAdd(offsets##name[pixel], 1); \
	data##name[index] = frag;


#else


#define OFFSETS_TYPE layout(r32ui) coherent uimageBuffer
#define DATA_TYPE layout(rg32f) imageBuffer

#define LFB_DEC(name) \
\
uniform OFFSETS_TYPE Offsets##name; \
uniform DATA_TYPE Data##name; \
\
LFBIterator iter##name; \
uniform ivec2 size##name;

void addFragToLFB(in ivec2 size, in vec2 frag, in OFFSETS_TYPE offsets, in DATA_TYPE data)
{
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	uint index = imageAtomicAdd(offsets, pixel, 1U);
	imageStore(data, int(index), vec4(frag, 0, 0));
}

#define LFB_ITER_BEGIN(name, pixel) iter##name.start = pixel > 0 ? imageLoad(Offsets##name, int(pixel) - 1).r : 0, iter##name.end = imageLoad(Offsets##name, int(pixel)).r, iter##name.node = 0
#define LFB_ITER_INC(name) iter##name.node++
#define LFB_ITER_CHECK(name) iter##name.node + iter##name.start < iter##name.end

#if LFB_REVERSE
#define LFB_GET_DATA(name) imageLoad(Data##name, int(iter##name.end - 1 - iter##name.node)).rg
#else
#define LFB_GET_DATA(name) imageLoad(Data##name, int(iter##name.node + iter##name.start)).rg
#endif

#define LFB_ADD_DATA(name, frag) addFragToLFB(size##name, frag, Offsets##name, Data##name)
#define LFB_GET_SIZE(name) size##name


#endif
#endif

