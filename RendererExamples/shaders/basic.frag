#version 450

uniform vec3 lightDir;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec3 viewDir;
} VertexIn;

uniform MaterialBlock
{
	vec4 ambientMat;
	vec4 diffuseMat;
	vec4 specularMat;
} Material;

out vec4 fragColor;


void main()
{
	float cosTheta = clamp(dot(VertexIn.normal, lightDir), 0.0, 1.0);

	vec3 ambient = Material.ambientMat.xyz;
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float dp = max(cosTheta, 0.0);

	if (dp > 0.0)
	{
		diffuse = Material.diffuseMat.xyz * dp;
		vec3 reflection = reflect(-lightDir, VertexIn.normal);
		float nDotH = max(dot(normalize(VertexIn.viewDir), normalize(reflection)), 0.0);
		float shininess = 128.0;
		float intensity = pow(nDotH, shininess);
		specular = Material.specularMat.xyz * intensity;
	}

	vec4 color = vec4(ambient + diffuse + specular, 0.1);
	fragColor = color;
}

