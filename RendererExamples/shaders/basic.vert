#version 450

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;

out VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec3 viewDir;
} VertexOut;


void main()
{
	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

	//vLightDir = lightDir;//(mvMatrix * vec4(lightPos, 0)).xyz;
	VertexOut.normal = nMatrix * normalize(normal);
	VertexOut.viewDir = -esVert.xyz;
	VertexOut.esFrag = esVert.xyz;

	gl_Position = csVert;
}

