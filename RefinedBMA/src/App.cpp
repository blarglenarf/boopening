#include "App.h"

#include "Oit/Oit.h"
#include "Oit/Standard.h"
#include "Oit/Planes.h"
#include "Oit/Refined.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <iomanip>


// TODO:
// TODO: Counts/sums pass for linearised and coalesced seems to take far too long. Fix it!
// TODO: Add different camera views (eg powerplant).
// TODO: Make sure tile indexing isn't slower for the powerplant.
// TODO: Decide what kind of deep images will be good for merging (at this point you should move to the collision project, maybe rename it).
// TODO: Powerplant scaling causes z fighting during composite, instead of scaling just use a larger frustum.
// 
// Refined BMA.
//
// Powerplant view:
// type: PERSPECTIVE
// pos: -4.9,3.85,0
// euler rot: -0.0261794,1.56207,0
// quat rot: -0.00921509, 0.703955, -0.00929585, 0.710124
// zoom: 7.22657
// fov/near/far: 1.309, 0.01, 30
// viewport: 0,0,1920,1016


using namespace Math;
using namespace Rendering;

// TODO: Get rid of this!
std::string currBmaStr;
std::string currLfbStr;
Flythrough anim;
Font testFont;
float tpf = 0;


// TODO: Keep it simple for now, add more frags later.
#define MAX_FRAGS 512
#define USE_BMA 1
#define INDEX_BY_TILE 1



void App::init()
{
	auto &camera = Renderer::instance().getActiveCamera();
#if 0
	camera.reset();
#endif

#if 0
	#if 1
		camera.setZoom(6.68847f);
		camera.setEulerRot(vec3(-0.209439f, 0, 0));
	#else
		camera.setZoom(6.97035);
		camera.setEulerRot(vec3(-0.0523594,0.855212,0));
	#endif
#endif

	auto &interpolator = anim.getInterpolator(anim.addInterpolator(const_cast<vec3*>(&camera.getEulerRot()), 10, vec3(-0.0523594,0.855212,0)));
	interpolator.addKeyFrame(0, vec3(-0.0523594,0.855212,0));
	interpolator.addKeyFrame(10, vec3(-0.061086,-0.715585,0));

#if 0
	anim.setFilename("animations/anim.jpg");
#endif

	// TODO: Get rid of this!
	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");

	int maxLayers = 0;
	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxLayers);
	std::cout << "Max texture layers: " << maxLayers << "\n";

	loadNextMesh();
	useBMA(Oit::STANDARD);
	//useLFB();

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glColor3f(1, 1, 1);

	auto vertBasic = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto geomBasic = ShaderSourceCache::getShader("basicGeom").loadFromFile("shaders/basic.geom");
	auto fragBasic = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.create(&vertBasic, &fragBasic, &geomBasic);

	//std::cout << basicShader.getBinary() << "\n";
}

void App::setTmpViewport(int width, int height)
{
	auto &camera = Renderer::instance().getActiveCamera();

	tmpWidth = camera.getWidth();
	tmpHeight = camera.getHeight();

	camera.setViewport(0, 0, width, height);
	camera.uploadViewport();
	camera.update(0);
}

void App::restoreViewport()
{
	auto &camera = Renderer::instance().getActiveCamera();

	camera.setViewport(0, 0, tmpWidth, tmpHeight);
	camera.uploadViewport();
	camera.update(0);
}

void App::setCameraUniforms(Shader *shader)
{
	auto &camera = Renderer::instance().getActiveCamera();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	shader->setUniform("viewport", camera.getViewport());
	shader->setUniform("invPMatrix", camera.getInverseProj());
}

void App::captureGeometry(Shader *shader)
{
	if (!shader)
		shader = &captureGeometryShader;

	profiler.start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	if (opaqueFlag == 0)
		geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && geometryLFB.getType() != LFB::LINK_LIST)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags(INDEX_BY_TILE);
		countShader.bind();
		geometryLFB.setCountUniforms(&countShader);
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh.render(false);
		countShader.unbind();
		geometryLFB.endCountFrags();
	}

	if (opaqueFlag == 0)
		geometryLFB.beginCapture();

	shader->bind();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	if (opaqueFlag == 0)
		geometryLFB.setUniforms(shader);

	// Capture fragments into the lfb.
	gpuMesh.render();
	shader->unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && geometryLFB.endCapture())
	{
		geometryLFB.beginCapture();
		shader->bind();
		shader->setUniform("mvMatrix", camera.getInverse());
		shader->setUniform("pMatrix", camera.getProjection());
		shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		geometryLFB.setUniforms(shader);
		gpuMesh.render();
		shader->unbind();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	// TODO: Profile memory usage perhaps?

	profiler.time("Geometry Capture");
}

void App::sortGeometry(bool composite)
{
	if (opaqueFlag == 1)
		return;

	profiler.start("Geometry Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	if (composite)
	{
		geometryLocalBMA.createMask(&geometryLFB);
		geometryLocalBMA.sort(&geometryLFB);
	}
	else
	{
		geometryBMA.createMask(&geometryLFB);
		geometryBMA.sort(&geometryLFB);
	}
#else
	// Sort and composite normally.
	if (composite)
	{
		sortGeometryLocalShader.bind();
		geometryLFB.composite(&sortGeometryLocalShader); // TODO: change this to a sort method.
		sortGeometryLocalShader.unbind();
	}
	else
	{
		sortGeometryShader.bind();
		geometryLFB.composite(&sortGeometryShader); // TODO: change this to a sort method.
		sortGeometryShader.unbind();
	}
#endif

	profiler.time("Geometry Sort");
}

void App::render()
{
	static float fpsUpdate = 0;

#if 0
	testFont.blit();
	return;
#endif

	profiler.begin();

	profiler.start("Total");

	if (currOit)
	{
		currOit->render(this);
	}
	else
	{
		auto &camera = Renderer::instance().getActiveCamera();

		// Opaque Rendering.
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		basicShader.bind();
		basicShader.setUniform("mvMatrix", camera.getInverse());
		basicShader.setUniform("pMatrix", camera.getProjection());
		basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		basicShader.setUniform("lightDir", vec3(0, 0, 1));
		gpuMesh.render();
		basicShader.unbind();

		glPopAttrib();
	}

	profiler.time("Total");

#if 1
	float total = profiler.getTime("Total");
	float smoothing = 0.9f; // Larger => more smoothing.
	tpf = (tpf * smoothing) + (total * (1.0f - smoothing));
	float t = round(total);

	testFont.renderFromAtlas("Technique: " + currBmaStr +
		"\nLFB: " + currLfbStr +
		"\nTime per frame (ms): " + Utils::toString(t) +
		"\nFrames per second (fps): " + Utils::toString(1000.0 / t), 10, 10, 0, 0, 1);
#endif

	float dt = Platform::Window::getDeltaTime();
	fpsUpdate += dt;
	if (fpsUpdate > 2)
	{
		profiler.print();
		fpsUpdate = 0;
	}

#if 1
	Renderer::instance().drawAxes();
	Renderer::instance().drawAxes(Renderer::instance().getActiveCamera().getPos(), 2);
#endif
}

void App::update(float dt)
{
	if (currOit)
		currOit->update(dt);

	if (anim.isAnimating())
	{
		anim.update(dt);
		Renderer::instance().getActiveCamera().setDirty();
	}
}

void App::runBenchmarks()
{
}

void App::useMesh()
{
	static std::vector<std::string> meshes = { "meshes/galleon.obj", "meshes/sponza/sponza.3ds", "meshes/quadsLarge.obj",
		"meshes/powerplant-obj/powerplant.obj", "meshes/rungholt/rungholt.obj", "meshes/livingroom/living_room.obj",
		"meshes/hairball.ctm", "meshes/teapot/teapot.obj", "meshes/powerplant/powerplant.ctm", "meshes/hairball.ctm" };

	UniformBuffer::clearBufferGroup();

	if (currMesh > (int) meshes.size() - 1)
		currMesh = 0;
	else if (currMesh < 0)
		currMesh = (int) meshes.size() - 1;

	gpuMesh.release();

	std::cout << "Loading mesh: " << meshes[currMesh] << "\n";

	Mesh mesh;
	if (Resources::MeshLoader::load(&mesh, meshes[currMesh]))
	{
		if (meshes[currMesh] == "meshes/powerplant/powerplant.ctm" || meshes[currMesh] == "meshes/powerplant-obj/powerplant.obj")
		{
			mesh.scale(vec3(0.0001f, 0.0001f, 0.0001f));
			mesh.translate(vec3(7.5, -10, -6)); // Move the mesh so it's reasonably well centered.

			if (meshes[currMesh] == "meshes/powerplant-obj/powerplant.obj")
			{
			#if 0
				mesh = mesh.extractFromMaterials({"powerplant.mtlsec10_material_0", "powerplant.mtlsec11_material_1", "powerplant.mtlsec2_material_2",
					"powerplant.mtlsec6_material_4", "powerplant.mtlsec7_material_1", "powerplant.mtlsec1_material_1", });
			#endif
			#if 0
				mesh = mesh.extractFromMaterials({"powerplant.mtlsec3_material_3", "powerplant.mtlsec10_material_1", "powerplant.mtlsec3_material_2",
					"powerplant.mtlsec14_material_1", "powerplant.mtlsec14_material_2", "powerplant.mtlsec16_material_1", "powerplant.mtlsec21_material_3",
					"powerplant.mtlsec21_material_4", "powerplant.mtlsec21_material_5", "powerplant.mtlsec8_material_5"});
			#endif
			}
		}
		else if (meshes[currMesh] == "meshes/rungholt/rungholt.obj")
			mesh.scale(vec3(0.04f, 0.04f, 0.04f));
		else if (meshes[currMesh] == "meshes/hairball.ctm")
			mesh.scale(vec3(-10, -10, -10), vec3(10, 10, 10));
		else if (meshes[currMesh] == "meshes/sponza/sponza.3ds")
		{
			mesh.scale(vec3(-10, -5, -10), vec3(10.0f, 5.0f, 10.0f));
		#if 0
			mesh = mesh.extractFromMaterials({"sponzachain_ctx.pn", "sponzaflagpole_szf", "sponzafabric_c_szc", "sponzafabric_f_szc", "sponzaleaf_szthdi.", "sponzavase_vdi.png",
				"sponza16___Default", "sponzavase_round_v", "sponzavase_hanging", "sponzafabric_g_szc", "sponzafabric_e_szf", "sponzafabric_a_szf", "sponzafabric_d_szf",
				"sponzaMaterial__57"});
		#endif
		#if 0
			mesh = mesh.extractFromMaterials({"sponzafloor_szfoad", "sponzaMaterial__25", "sponzaMaterial__47", "sponzacolumn_a_szc", "sponzaarch_szadi.p", "sponzaroof_szrfdi.",
				"sponzabricks_snbad", "sponzadetails_szdt", "sponzacolumn_c_szc", "sponzaceiling_szcl", "sponzaMaterial__29", "sponzacolumn_b_szc"});
		#endif
		}
		else if (meshes[currMesh] == "meshes/galleon.obj")
			mesh.scale(vec3(0.2, 0.2, 0.2));
		else if (meshes[currMesh] == "meshes/teapot/teapot.obj")
			mesh.scale(vec3(2.0, 2.0, 2.0));
	#if 0
		std::cout << "Listing materials:\n";
		for (auto &faceset : mesh.getFacesets())
		{
			if (faceset.mat)
				std::cout << "Material: " << faceset.mat->getName() << " Count: " << faceset.end - faceset.start << "\n";
		}
	#endif
		gpuMesh.create(&mesh);
	}
}

void App::useLFB()
{
	profiler.clear();

#if USE_LINK_PAGES
	static std::vector<LFB::LFBType> types = { LFB::LINK_PAGES, LFB::LINK_LIST, LFB::LINEARIZED };
	static std::vector<std::string> typeStrs = { "Link Pages", "Link List", "Linearized" };
#else
	#if 1
		static std::vector<LFB::LFBType> types = { LFB::LINEARIZED, LFB::LINK_LIST, LFB::COALESCED };
		static std::vector<std::string> typeStrs = { "Linearized", "Link List", "Coalesced" };
	#else
		static std::vector<LFB::LFBType> types = { LFB::LINK_LIST, LFB::LINEARIZED, LFB::COALESCED };
		static std::vector<std::string> typeStrs = { "Link List", "Linearized", "Coalesced" };
	#endif
#endif

	geometryLFB.release();

	delete currOit;
	currOit = nullptr;

	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();

	if (currLFB > (int) types.size() - 1)
		currLFB = 0;
	else if (currLFB < 0)
		currLFB = (int) types.size() - 1;
	LFB::LFBType type = types[currLFB];

	currLfbStr = typeStrs[currLFB];
	std::cout << "Using LFB: " << typeStrs[currLFB] << "\n";

	if (opaqueFlag == 0)
	{
		geometryLFB.setType(type, "lfb");
		geometryLFB.setMaxFrags(MAX_FRAGS);
		geometryLFB.setProfiler(&profiler);
	}

	auto vertCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/oit/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/oit/capture.frag");
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragCapture.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);

	if (opaqueFlag == 0)
	{
		// Sorts and writes to global memory.
		auto vertSort = ShaderSourceCache::getShader("sortVert").loadFromFile("shaders/sort/sort.vert");
		auto fragSort = ShaderSourceCache::getShader("sortFrag").loadFromFile("shaders/sort/sort.frag");
		fragSort.setDefine("MAX_FRAGS", Utils::toString(geometryLFB.getMaxFrags()));
		fragSort.setDefine("COMPOSITE_LOCAL", "0");
		fragSort.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
		fragSort.replace("LFB_NAME", "lfb");

		sortGeometryShader.release();
		sortGeometryShader.create(&vertSort, &fragSort);

		//std::cout << "Compiled one\n";

		// Sorts and composites.
		auto vertLocalSort = ShaderSourceCache::getShader("sortLocalVert").loadFromFile("shaders/sort/sort.vert");
		auto fragLocalSort = ShaderSourceCache::getShader("sortLocalFrag").loadFromFile("shaders/sort/sort.frag");
		fragLocalSort.setDefine("MAX_FRAGS", Utils::toString(geometryLFB.getMaxFrags()));
		fragLocalSort.setDefine("COMPOSITE_LOCAL", "1");
		fragLocalSort.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
		fragLocalSort.replace("LFB_NAME", "lfb");

		sortGeometryLocalShader.release();
		sortGeometryLocalShader.create(&vertLocalSort, &fragLocalSort);

		Utils::File f("shader.txt");
		f.write(sortGeometryLocalShader.getBinary());

		//std::cout << "Compiled two\n";

		// BMA shaders that write to global memory.
		geometryBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
		geometryBMA.createShaders(&geometryLFB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "0"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});

		//std::cout << "BMA one\n";

		// BMA shaders that sort and composite.
		geometryLocalBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
		geometryLocalBMA.createShaders(&geometryLFB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});

		//std::cout << "BMA two\n";
	}

	switch (bmaType)
	{
	case Oit::STANDARD:
		currOit = new Standard(opaqueFlag);
		break;

	case Oit::PLANES:
		currOit = new Planes(opaqueFlag);
		break;

	case Oit::REFINED:
		currOit = new Refined(opaqueFlag);
		break;

	default:
		currOit = new Standard(opaqueFlag);
		break;
	}

	if (currOit)
	{
		currOit->init();
		currOit->useLFB(this, type);
	}
}

void App::toggleTransparency()
{
	opaqueFlag = opaqueFlag == 0 ? 1 : 0;
	useLFB();
}

void App::loadNextMesh()
{
	currMesh++;
	useMesh();
}

void App::loadPrevMesh()
{
	currMesh--;
	useMesh();
}

void App::useNextLFB()
{
	currLFB++;
	useLFB();
}

void App::usePrevLFB()
{
	currLFB--;
	useLFB();
}

void App::cycleBMA()
{
	static std::vector<Oit::BmaType> bmaTypes = {Oit::STANDARD, Oit::PLANES, Oit::REFINED};

	static int currBmaType = 0;

	currBmaType++;
	if (currBmaType > (int) bmaTypes.size() - 1)
		currBmaType = 0;
	useBMA(bmaTypes[currBmaType]);
}

void App::useBMA(Oit::BmaType type)
{
	static std::map<Oit::BmaType, std::string> bmaTypes;
	if (bmaTypes.empty())
	{
		bmaTypes[Oit::STANDARD] = "Standard";
		bmaTypes[Oit::PLANES] = "Planes";
		bmaTypes[Oit::REFINED] = "Refined";
	}

	bmaType = type;
	currBmaStr = bmaTypes[type];
	std::cout << "Using BMA: " << currBmaStr << "\n";

	useLFB();

}

void App::playAnim()
{
	anim.play();
}

void App::saveGeometryLFB()
{
	if (opaqueFlag == 1)
		return;
	auto s = geometryLFB.getStr();
	//auto s = geometryLFB.getBinary();
	Utils::File f("lfb.txt");
	f.write(s);
}

void App::saveLFBData()
{
	currOit->saveLFB(this);
}

