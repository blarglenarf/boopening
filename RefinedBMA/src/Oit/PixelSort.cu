#include "PixelSort.h"

#include "../../../Renderer/src/GL.h"

#include <cuda_runtime.h>
#include <cuda.h>
#include <cudaGL.h>
#include <cuda_gl_interop.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/sort.h>

struct PixelKeyComp
{
	__host__ __device__
	bool operator()(const PixelKey &a, const PixelKey &b)
	{
		return b.count > a.count;
	}
};

cudaGraphicsResource *resource = NULL;
PixelKey *devPtr = NULL;
size_t size = 0;

void sortPixelKeysThrust(std::vector<PixelKey> &pixelKeys)
{
	// Copy the PixelKeys to the GPU.
	thrust::device_vector<PixelKey> deviceKeys(pixelKeys.size());
	thrust::copy(pixelKeys.begin(), pixelKeys.end(), deviceKeys.begin());

	thrust::sort(deviceKeys.begin(), deviceKeys.end(), PixelKeyComp());

	// Get the PixelKeys back from the GPU.
	thrust::copy(deviceKeys.begin(), deviceKeys.end(), pixelKeys.begin());
}

void createPixelKeysCudaResource(unsigned int pixelKeysBuffer)
{
	cudaGraphicsGLRegisterBuffer(&resource, pixelKeysBuffer, cudaGraphicsMapFlagsNone);
}

void destroyPixelKeysCudaResource()
{
	if (resource != NULL)
		cudaGraphicsUnregisterResource(resource);
	resource = NULL;
}

void sortPixelKeysThrust(size_t nPixelKeys)
{
	// FIXME: Probably shouldn't do this here...
	//cudaGLSetGLDevice(0);
	cudaGraphicsMapResources(1, &resource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**) &devPtr, &size, resource);
	if (devPtr != NULL)
	{
		thrust::device_ptr<PixelKey> deviceKeys = thrust::device_pointer_cast(devPtr);
		thrust::sort(deviceKeys, deviceKeys + nPixelKeys, PixelKeyComp());
	}
	cudaGraphicsUnmapResources(1, &resource, NULL);
}

