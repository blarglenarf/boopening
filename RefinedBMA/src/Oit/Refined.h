#pragma once

#include "Oit.h"

#include <vector>

class Refined : public Oit
{
private:
	struct Interval
	{
		int start;
		int end;
		Rendering::Shader *shader;
		Interval(int start = 0, int end = 0, Rendering::Shader *shader = nullptr) : start(start), end(end), shader(shader) {}
	};

private:
	int width;
	int height;

	Rendering::StorageBuffer pixelKeyBuffer;
	Rendering::Shader pixelKeyShader;

	Rendering::StorageBuffer resultBuffer;
	Rendering::Shader sortGeometryLocalShader;
	Rendering::Shader compositeShader;

	std::vector<Interval> intervals;
	std::vector<unsigned int> intervalOffsets;

private:
	void createIntervalShaders(App *app);

	void createPixelIDs(App *app);
	void createOffsets(App *app);

	void sortFragments(App *app);
	void sortGeometry(App *app);

public:
	Refined(int opaqueFlag = 0) : Oit(opaqueFlag), width(0), height(0) {}
	virtual ~Refined();

	virtual void init() override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

