#pragma once

#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/LFB/LFB.h"

class App;

class Oit
{
public:
	enum BmaType
	{
		STANDARD,
		PLANES,
		REFINED
	};

protected:
	int opaqueFlag;

public:
	Oit(int opaqueFlag = 0) : opaqueFlag(opaqueFlag) {}
	virtual ~Oit() {}

	virtual void init() = 0;

	virtual void render(App *app) = 0;
	virtual void update(float dt) = 0;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) = 0;

	virtual void saveLFB(App *app) = 0;
};

