#pragma once

#include <vector>

struct PixelKey
{
	unsigned int id;
	unsigned int count;
};

// This one is slow since it has to copy data to/from the GPU.
void sortPixelKeysThrust(std::vector<PixelKey> &pixelKeys);

// Note: the opengl buffer has to be bound before calling this function.
void createPixelKeysCudaResource(unsigned int pixelKeysBuffer);
void destroyPixelKeysCudaResource();

// Note: the opengl buffer has to be bound before calling this function.
void sortPixelKeysThrust(size_t nPixelKeys);

