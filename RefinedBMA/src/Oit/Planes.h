#pragma once

#include "Oit.h"

#include <vector>

class Planes : public Oit
{
private:
	Rendering::Shader compositeShader;

	std::vector<Rendering::Mesh> planeMeshes;
	std::vector<Rendering::GPUMesh*> planes;

private:
	void capturePlanes(App *app);

public:
	Planes(int opaqueFlag = 0) : Oit(opaqueFlag) {}
	virtual ~Planes();

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

