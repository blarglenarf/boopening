#pragma once

#include "Oit.h"

class Standard : public Oit
{
private:
	Rendering::Shader compositeShader;

public:
	Standard(int opaqueFlag = 0) : Oit(opaqueFlag) {}
	virtual ~Standard() {}

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

