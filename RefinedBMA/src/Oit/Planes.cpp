#include "Planes.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

#define COMPOSITE_LOCAL 1
#define N_PLANES 512
#define INDEX_BY_TILE 1

using namespace Rendering;


void Planes::capturePlanes(App *app)
{
	Shader *shader = &app->getCaptureGeometryShader();

	app->getProfiler().start("Geometry Capture");

	// TODO: Maybe just use an identity.
	auto &camera = Renderer::instance().getActiveCamera();
	auto &geometryLFB = app->getGeometryLfb();

	if (opaqueFlag == 0)
		geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && geometryLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags();
		countShader.bind();
		geometryLFB.setCountUniforms(&countShader);
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		for (size_t i = 0; i < N_PLANES; i++)
			planes[i]->render(false);
		countShader.unbind();
		geometryLFB.endCountFrags();
	}

	if (opaqueFlag == 0)
		geometryLFB.beginCapture();

	shader->bind();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	if (opaqueFlag == 0)
		geometryLFB.setUniforms(shader);

	// Capture fragments into the lfb.
	for(size_t i = 0; i < N_PLANES; i++)
		planes[i]->render();
	shader->unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && geometryLFB.endCapture())
	{
		geometryLFB.beginCapture();
		shader->bind();
		shader->setUniform("mvMatrix", camera.getInverse());
		shader->setUniform("pMatrix", camera.getProjection());
		shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		geometryLFB.setUniforms(shader);
		for (size_t i = 0; i < N_PLANES; i++)
			planes[i]->render();
		shader->unbind();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	app->getProfiler().time("Geometry Capture");
}


Planes::~Planes()
{
	for (auto *plane : planes)
		delete plane;
}

void Planes::render(App *app)
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	// Don't capture normal geometry, render a bunch of coherent planes.
	capturePlanes(app);

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#if COMPOSITE_LOCAL
	app->sortGeometry();
#else
	// Read the result back from the lfb and composite.
	app->sortGeometry(false);
	app->getProfiler().start("Composite");
	compositeShader.bind();
	app->getGeometryLfb().composite(&compositeShader);
	compositeShader.unbind();
	app->getProfiler().time("Composite");
#endif

	glPopAttrib();
}

void Planes::update(float dt)
{
	(void) (dt);
}

void Planes::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
		auto vertComp = ShaderSourceCache::getShader("compBruteVert").loadFromFile("shaders/oit/standard/composite.vert");
		auto fragComp = ShaderSourceCache::getShader("compBruteFrag").loadFromFile("shaders/oit/standard/composite.frag");

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp);
	}

	if (planeMeshes.empty())
	{
		planeMeshes.resize(N_PLANES);
		planes.resize(N_PLANES);

		for (size_t i = 0; i < N_PLANES; i++)
		{
			planeMeshes[i] = Mesh::getSquare(-((float) i / 256.0));
			planes[i] = new GPUMesh();
			planes[i]->create(&planeMeshes[i]);
		}
	}
}

void Planes::saveLFB(App *app)
{
	app->saveGeometryLFB();
}

