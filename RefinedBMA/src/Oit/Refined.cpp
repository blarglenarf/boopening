#include "Refined.h"
#include "PixelSort.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Renderer/src/LFB/LFBList.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <string>
#include <algorithm>

#define COMPOSITE_LOCAL 1
#define NO_CUDA 1

using namespace Rendering;

void Refined::createIntervalShaders(App *app)
{
	int maxBmaInterval = 1 << Utils::ceilLog2(app->getGeometryLfb().getMaxFrags());
	int prev = 0;

	for (auto &interval : intervals)
		delete interval.shader;
	intervals.clear();

	// TODO: More fine-grained intervals.
	std::vector<int> splits;
	for (int interval = 8; interval <= maxBmaInterval; interval *= 2)
		splits.push_back(interval);
	std::sort(splits.begin(), splits.end());

	intervals.resize(splits.size());

	for (size_t i = 0; i < splits.size(); i++)
	{
		int intervalEnd = splits[i];
		intervals[i].start = prev;
		intervals[i].end = intervalEnd;
		intervals[i].shader = new Shader();
		auto &bmaVert = ShaderSourceCache::getShader("bmaVert" + Utils::toString(intervalEnd) + app->getGeometryLfb().getName()).loadFromFile("shaders/oit/refined/sort.vert");
		bmaVert.setDefine("MAX_FRAGS", Utils::toString(app->getGeometryLfb().getMaxFrags()));
		bmaVert.setDefine("MAX_FRAGS_OVERRIDE", Utils::toString(intervalEnd));
		bmaVert.setDefine("COMPOSITE_LOCAL", "1");
		//bmaFrag.setDefine("COMPOSITE_LOCAL", Utils::toString(compositeLocal));
		//bmaFrag.setDefine("GRID_CELL_SIZE", Utils::toString(gridCellSize));
		//for (auto &fragDefine : fragDefines)
		//	bmaFrag.setDefine(fragDefine.tag, fragDefine.value);
		bmaVert.replace("LFB_NAME", app->getGeometryLfb().getName());
		intervals[i].shader->create(&bmaVert);
		prev = intervalEnd;
	}
}

void Refined::createPixelIDs(App *app)
{
	app->getProfiler().start("Create IDs");

	if (!pixelKeyShader.isGenerated())
	{
		auto pixelSrc = ShaderSourceCache::getShader("pixelKeyCreate").loadFromFile("shaders/oit/refined/pixelKey.vert");
		pixelKeyShader.create(&pixelSrc);
	}

	auto &camera = Renderer::instance().getActiveCamera();
	// TODO: Determine if camera view has changed, rather than just the dimensions.
	//if (camera.getWidth() == width && camera.getHeight() == height)
	//	return;

	width = camera.getWidth();
	height = camera.getHeight();

#if !NO_CUDA
	destroyPixelKeysCudaResource();
	pixelKeyBuffer.create(nullptr, width * height * sizeof(PixelKey));
	pixelKeyBuffer.bind();
	createPixelKeysCudaResource(pixelKeyBuffer.getObject());
	pixelKeyBuffer.unbind();

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_RASTERIZER_DISCARD);
	pixelKeyShader.bind();
	pixelKeyShader.setUniform("linearisedFlag", app->getGeometryLfb().getType() == LFB::LINEARIZED);
	pixelKeyShader.setUniform("PixelKeys", &pixelKeyBuffer);
	app->getGeometryLfb().setCountUniforms(&pixelKeyShader);
	glDrawArrays(GL_POINTS, 0, (GLsizei) (width * height));
	pixelKeyShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glDisable(GL_RASTERIZER_DISCARD);
	glPopAttrib();

	app->getProfiler().time("Create IDs");

	// TODO: If sorting is a problem, find a way to do it without sorting.
	app->getProfiler().start("Sort IDs");
	pixelKeyBuffer.bind();
	sortPixelKeysThrust(width * height);
	pixelKeyBuffer.unbind();
	app->getProfiler().time("Sort IDs");
#else
	// Only do this if absolutely necessary.
	auto *lfbList = (LFBList*) app->getGeometryLfb().getBase();
	auto *countBuffer = lfbList->getCounts();
	auto *counts = (unsigned int*) countBuffer->read();

	std::vector<PixelKey> pixelKeys(width * height);
	for (int i = 0; i < width * height; i++)
	{
		pixelKeys[i].id = i;
		pixelKeys[i].count = counts[i];
	}
	app->getProfiler().time("Create IDs");

	app->getProfiler().start("Sort IDs");
#if !NO_CUDA
	sortPixelKeysThrust(pixelKeys);
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	pixelKeyBuffer.create(&pixelKeys[0], sizeof(PixelKey) * pixelKeys.size());
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	app->getProfiler().time("Sort IDs");
#endif
}

void Refined::createOffsets(App *app)
{
	app->getProfiler().start("Offsets");

	// TODO: All the data is on the GPU, would be nice if it could stay there...
	auto *pixelKeys = (PixelKey*) pixelKeyBuffer.read();

	intervalOffsets.resize(intervals.size());
	size_t currOffset = 0;
	for (size_t i = 0; i < intervals.size(); i++)
	{
		auto &interval = intervals[i];

		// Find first instance of interval.end + 1, that's the point where this interval stops sorting.
		auto lower = std::lower_bound(pixelKeys + currOffset, pixelKeys + (width * height), interval.end + 1, [](const PixelKey &a, const unsigned int &b) -> bool { return a.count < b; });
		auto index = std::distance(pixelKeys, lower);
		intervalOffsets[i] = index;
	}

	app->getProfiler().time("Offsets");
}

void Refined::sortFragments(App *app)
{
	app->getProfiler().start("Sort fragments");

	// Be nice to be able to rasterize directly to specific pixels, but for now have to use a buffer.
	// TODO: Just use a texture instead and use imageStore to write to it. Then blit.
	resultBuffer.create(nullptr, width * height * sizeof(float));

	//glPushAttrib(GL_ENABLE_BIT);
#if 0
	glEnable(GL_RASTERIZER_DISCARD);
	sortGeometryLocalShader.bind();
	app->getGeometryLfb().beginComposite();
	app->getGeometryLfb().setUniforms(&sortGeometryLocalShader);
	sortGeometryLocalShader.setUniform("ResultBuffer", &resultBuffer);
	sortGeometryLocalShader.setUniform("PixelKeys", &pixelKeyBuffer);
	glDrawArrays(GL_POINTS, 0, (GLsizei) (width * height));
	app->getGeometryLfb().endComposite();
	sortGeometryLocalShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);
#else
	glEnable(GL_RASTERIZER_DISCARD);
	unsigned int currOffset = 0;
	for (size_t i = 0; i < intervalOffsets.size(); i++)
	{
		auto *shader = intervals[i].shader;
		shader->bind();
		app->getGeometryLfb().beginComposite();
		app->getGeometryLfb().setUniforms(shader);
		shader->setUniform("ResultBuffer", &resultBuffer);
		shader->setUniform("PixelKeys", &pixelKeyBuffer);
		shader->setUniform("offset", currOffset);
		glDrawArrays(GL_POINTS, 0, (GLsizei) (intervalOffsets[i] - currOffset));
		app->getGeometryLfb().endComposite();
		shader->unbind();
		currOffset = intervalOffsets[i];
	}
	glDisable(GL_RASTERIZER_DISCARD);
#endif
	//glPopAttrib();

	app->getProfiler().time("Sort fragments");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().start("Result");

	// Render result.
	compositeShader.bind();
	compositeShader.setUniform("ResultBuffer", &resultBuffer);
	compositeShader.setUniform("size", Math::ivec2(width, height));
	Renderer::instance().drawQuad();
	compositeShader.unbind();

	app->getProfiler().time("Result");
}

void Refined::sortGeometry(App *app)
{
	// Need an array of pixel ids (if done before prefix sums turn them into offsets, then requires one less read).
	createPixelIDs(app);

	// Need to mark the position where 8 ends, then 16, 32, 64, 128, 256, 512, etc. so we can use the shader with appropriate memory.
	createOffsets(app);

	// Sort lists in order of the pixel ids and render result.
	sortFragments(app);

	// TODO: If that works, try blocking up larger lists into smaller chunks.
}


Refined::~Refined()
{
#if !NO_CUDA
	destroyPixelKeysCudaResource();
#endif

	for (auto &interval : intervals)
		delete interval.shader;
	intervals.clear();
}

void Refined::init()
{
}

void Refined::render(App *app)
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	
	app->captureGeometry();

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#if COMPOSITE_LOCAL
	//app->sortGeometry();
	sortGeometry(app);
#else
	#if 0
	app->sortGeometry(false);
	// Read the result back from the lfb and composite.
	app->getProfiler().start("Composite");
	compositeShader.bind();
	app->getGeometryLfb().composite(&compositeShader);
	compositeShader.unbind();
	app->getProfiler().time("Composite");
	#endif
#endif

	glPopAttrib();
}

void Refined::update(float dt)
{
	(void) (dt);
}

void Refined::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
		// Single sort shader.
		auto vertLocalSort = ShaderSourceCache::getShader("sortLocalFineVert").loadFromFile("shaders/oit/refined/sort.vert");
		vertLocalSort.setDefine("MAX_FRAGS", Utils::toString(app->getGeometryLfb().getMaxFrags()));
		vertLocalSort.setDefine("COMPOSITE_LOCAL", "1");
		vertLocalSort.replace("LFB_NAME", "lfb");
		sortGeometryLocalShader.release();
		sortGeometryLocalShader.create(&vertLocalSort);

		// Fine-grained BMA sort shaders.
		createIntervalShaders(app);

		// Composite.
		auto vertComp = ShaderSourceCache::getShader("compFineVert").loadFromFile("shaders/oit/refined/composite.vert");
		auto fragComp = ShaderSourceCache::getShader("compFineFrag").loadFromFile("shaders/oit/refined/composite.frag");
		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp);
	}
}

void Refined::saveLFB(App *app)
{
	app->saveGeometryLFB();
}

