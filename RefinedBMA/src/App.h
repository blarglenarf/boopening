#pragma once

#define GRID_CELL_SIZE 16
#define CLUSTERS 32
#define MAX_EYE_Z 30.0
#define MASK_SIZE (CLUSTERS / 32)

#include "Oit/Oit.h"

#include "../../Renderer/src/LFB/LFB.h"
#include "../../Renderer/src/LFB/BMA.h"
#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/Profiler.h"

#include "../../Math/src/Vector/Vec4.h"

#include <vector>

class App
{
private:
	int currLFB;
	int currMesh;

	int opaqueFlag;

	Oit::BmaType bmaType;
	Oit *currOit;

	Rendering::LFB geometryLFB;
	Rendering::BMA geometryBMA;
	Rendering::BMA geometryLocalBMA;

	Rendering::Shader basicShader;
	Rendering::Shader sortGeometryShader;
	Rendering::Shader sortGeometryLocalShader;
	Rendering::Shader captureGeometryShader;

	Rendering::GPUMesh gpuMesh;

	Rendering::Profiler profiler;

	int width;
	int height;

	int tmpWidth;
	int tmpHeight;

public:
	App() : currLFB(1), currMesh(0), opaqueFlag(0), bmaType(Oit::STANDARD), currOit(nullptr), width(0), height(0), tmpWidth(0), tmpHeight(0) {}
	~App() {}

	void init();

	void setTmpViewport(int width, int height);
	void restoreViewport();

	void setCameraUniforms(Rendering::Shader *shader);

	void captureGeometry(Rendering::Shader *shader = nullptr);
	void sortGeometry(bool composite = true);

	void render();
	void update(float dt);

	void runBenchmarks();

	void useMesh();
	void useLFB();

	void toggleTransparency();

	void loadNextMesh();
	void loadPrevMesh();

	void useNextLFB();
	void usePrevLFB();

	void cycleBMA();
	void useBMA(Oit::BmaType type);

	void playAnim();

	void saveGeometryLFB();
	void saveLFBData();

	Rendering::LFB &getGeometryLfb() { return geometryLFB; }
	Rendering::GPUMesh &getGpuMesh() { return gpuMesh; }

	Rendering::Shader &getCaptureGeometryShader() { return captureGeometryShader; }

	Rendering::Profiler &getProfiler() { return profiler; }
};

