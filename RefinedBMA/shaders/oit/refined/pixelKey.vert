#version 430
	
struct PixelKey
{
	uint id;
	uint count;
};

buffer PixelKeys
{
	PixelKey pixelKeys[];
};

buffer Offsets
{
	uint offsets[];
};

buffer Counts
{
	uint counts[];
};

uniform bool linearisedFlag;

void main()
{
	pixelKeys[gl_VertexID].id = uint(gl_VertexID);

	// FIXME: If this is done before prefix sums turn the counts into offsets, then it requires one less read.
	if (linearisedFlag)
	{
		uint count = uint(offsets[gl_VertexID]) - (gl_VertexID > 0 ? uint(offsets[gl_VertexID - 1]) : 0);
		pixelKeys[gl_VertexID].count = count;
	}
	else
	{
		pixelKeys[gl_VertexID].count = uint(counts[gl_VertexID]);
	}
}

