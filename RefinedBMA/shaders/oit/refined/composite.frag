#version 430

out vec4 fragColor;

buffer ResultBuffer
{
	float resultBuffer[];
};

#include "../../utils.glsl"

// Just standard forward rendering.
uniform ivec2 size;

void main()
{
	// Composite geometry.
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	fragColor = floatToRGBA8(resultBuffer[pixel]);
}

