#version 430

struct PixelKey
{
	uint id;
	uint count;
};

vec4 fragColor;

#define FRAG_SIZE 2

#import "lfb"

#include "../../utils.glsl"

// To use different deep images, call replace(LFB_NAME, ...).
#if FRAG_SIZE == 1
	LFB_DEC_1(LFB_NAME);
#elif FRAG_SIZE == 4
	LFB_DEC_4(LFB_NAME);
#else
	LFB_DEC_2(LFB_NAME);
#endif

#define COMPOSITE 0
#define COMPOSITE_LOCAL 0

#define MAX_FRAGS 0
#define MAX_FRAGS_OVERRIDE 0

#if MAX_FRAGS_OVERRIDE != 0
	#define _MAX_FRAGS MAX_FRAGS_OVERRIDE
#else
	#define _MAX_FRAGS MAX_FRAGS
#endif

#define LFB_FRAG_TYPE vec2
#define LFB_FRAG_DEPTH(frag) frag.y

LFB_FRAG_TYPE frags[_MAX_FRAGS];

#if FRAG_SIZE == 1
	#define LFB_GET_FRAG(LFB_NAME) decodeLight(LFB_GET_DATA(LFB_NAME))
	#define LFB_PUT_FRAG(LFB_NAME, frag) LFB_WRITE_DATA(LFB_NAME, encodeLight(floatBitsToUint(frag.x), frag.y))
#elif FRAG_SIZE == 4
	#error I have no idea what I'm doing...
#else
	#define LFB_GET_FRAG(LFB_NAME) LFB_GET_DATA(LFB_NAME)
	#define LFB_PUT_FRAG(LFB_NAME, frag) LFB_WRITE_DATA(LFB_NAME, frag)
#endif

buffer ResultBuffer
{
	float resultBuffer[];
};

buffer PixelKeys
{
	PixelKey pixelKeys[];
};


#include "sort.glsl"

uniform uint offset;


void main()
{
	fragColor = vec4(0.0);

	//int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	int pixel = int(pixelKeys[gl_VertexID + offset].id);

	// Load into local memory and sort. This will also write to global memory.
	int fragCount = sort(pixel);

#if COMPOSITE
	// Read from global memory and composite.
	for (LFB_ITER_BEGIN(LFB_NAME, pixel); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		LFB_FRAG_TYPE f = LFB_GET_FRAG(LFB_NAME);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif

#if COMPOSITE_LOCAL
	#if _MAX_FRAGS <= 32 // Dirty hack to account for rbs not sorting in place.
	for (int i = fragCount - 1; i >= 0; i--)
	{
		LFB_FRAG_TYPE f = frags[i];
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
	#endif
#endif

#if (COMPOSITE || COMPOSITE_LOCAL) && 1
	float dc = MAX_FRAGS_OVERRIDE / float(MAX_FRAGS);
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
	fragColor.a = 1.0;
#endif

	resultBuffer[pixel] = rgba8ToFloat(fragColor);
}

