#pragma once

#include "../../Renderer/src/RenderObject/Shader.h"

namespace SpaceStrat
{
	class Map
	{
	private:
		int rows;
		int cols;

		float width;
		float height;

		Rendering::Shader shader;

	public:
		Map() : rows(0), cols(0), width(0), height(0) {}
		~Map() {}

		void init();

		void update(float dt);
		void render();
	};
}

