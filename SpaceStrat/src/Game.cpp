#include "Game.h"

using namespace SpaceStrat;

Game Game::game;

Game &Game::instance()
{
	return game;
}

void Game::init()
{
	map.init();
}

void Game::update(float dt)
{
	map.update(dt);
}

void Game::render()
{
	map.render();
}

