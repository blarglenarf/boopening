#pragma once

#include "Map.h"

namespace SpaceStrat
{
	class Game
	{
	private:
		Map map;

		static Game game;

	private:
		Game() {}

	public:
		~Game() {}

		static Game &instance();

		void init();
		void update(float dt);
		void render();
	};
}

