#include "Map.h"

#include "../../Renderer/src/Renderer.h"
#include "../../Renderer/src/Camera.h"
#include "../../Renderer/src/RenderResource/RenderResourceCache.h"
#include "../../Renderer/src/RenderResource/ShaderSource.h"

#include "../../Platform/src/Mouse.h"

using namespace SpaceStrat;

void Map::init()
{
	rows = 1000;
	cols = 1000;

	width = 10;
	height = 10;

	// Create the map shader. Above variables can be #defines.
	auto &vertSrc = Rendering::ShaderSourceCache::getShader("mapVert").loadFromFile("shaders/map/map.vert");
	auto &fragSrc = Rendering::ShaderSourceCache::getShader("mapFrag").loadFromFile("shaders/map/map.frag");
	fragSrc.setDefine("ROWS", Utils::toString(rows));
	fragSrc.setDefine("COLS", Utils::toString(cols));
	fragSrc.setDefine("WIDTH", Utils::toString(width));
	fragSrc.setDefine("HEIGHT", Utils::toString(height));
	shader.create(&vertSrc, &fragSrc);
}

void Map::update(float dt)
{
	// TODO: Implement me!
	(void) (dt);
}

void Map::render()
{
	auto &camera = Rendering::Renderer::instance().getActiveCamera();

	shader.bind();

	// Set a bunch of camera related uniforms I guess.
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("invMvMatrix", camera.getTransform());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("invPMatrix", camera.getInverseProj());
	shader.setUniform("viewport", camera.getViewport());
	shader.setUniform("cursorPos", Math::ivec2(Platform::Mouse::x, Platform::Mouse::y));

	Rendering::Renderer::instance().drawQuad();

	shader.unbind();
}

