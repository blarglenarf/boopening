// TODO: Decide what actually needs to go here.

#include <cstdlib>

#include "../../Renderer/src/RendererCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"
#include "../../Gui/src/GuiCommon.h"
#include "../../Gui/src/NineBox/BoxPool.h"

#include "Game.h"

#define UNUSED(x) (void)(x)

using namespace Platform;
using namespace Rendering;
using namespace Resources;
using namespace Gui;
using namespace SpaceStrat;

static void update(float dt)
{
	Renderer::instance().getActiveCamera().update(dt);

	Canvas::instance().update(dt, Platform::Mouse::x, Platform::Window::getHeight() - Platform::Mouse::y, Platform::Mouse::dx, -Platform::Mouse::dy);

	Game::instance().update(dt);
}

static void render()
{
	Renderer::instance().getActiveCamera().setViewport(0, 0, Window::getWidth(), Window::getHeight());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glLoadIdentity();
	Renderer::instance().getActiveCamera().upload();

	Game::instance().render();

	Canvas::instance().render();

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);
	Renderer::instance().drawAxes();
	glPopAttrib();
}

static bool mouseDrag(Event &ev)
{
	Canvas::instance().handleMouseDragEvent(ev);

	if (Canvas::instance().isDragging() || Canvas::instance().isClicking())
		return false;

	if (Mouse::isDown(Mouse::LEFT))
		Renderer::instance().getActiveCamera().updateRotation((float) ev.dx, (float) ev.dy);
	if (Mouse::isDown(Mouse::RIGHT))
		Renderer::instance().getActiveCamera().updateZoom((float) ev.dy);
	if (Mouse::isDown(Mouse::MIDDLE))
		Renderer::instance().getActiveCamera().updatePan((float) ev.dx, (float) ev.dy);

	return false;
}

static bool mousePressed(Event &ev)
{
	Canvas::instance().handleMouseClickEvent(ev);
	return false;
}

static bool mouseDown(Event &ev)
{
	Canvas::instance().handleMouseDownEvent(ev);
	return false;
}

static bool mouseUp(Event &ev)
{
	Canvas::instance().handleMouseUpEvent(ev);
	return false;
}

static bool keyPress(Event &ev)
{
	if (ev.key == Keyboard::ESCAPE)
		exit(EXIT_SUCCESS);

	if (Canvas::instance().handleKeyPressEvent(ev))
		return true;

	switch (ev.key)
	{
	case Keyboard::q:
		exit(EXIT_SUCCESS);
		break;

	case Keyboard::p:
		//toggleWireframe();
		break;

	case Keyboard::LEFT:
		//app.usePrevLFB();
		break;

	case Keyboard::RIGHT:
		//app.useNextLFB();
		break;

	case Keyboard::UP:
		//app.doubleLights();
		//app.loadNextMesh();
		break;

	case Keyboard::DOWN:
		//app.halveLights();
		//app.loadPrevMesh();
		break;

	case Keyboard::t:
		//app.toggleTransparency();
		break;

	case Keyboard::l:
		//app.cycleLighting();
		break;

	case Keyboard::b:
		//app.runBenchmarks();
		break;

	case Keyboard::c:
		std::cout << Renderer::instance().getActiveCamera() << "\n";
		break;

	case Keyboard::o:
		//app.saveLFBData();
		break;

	case Keyboard::m:
		//app.loadNextMesh();
		break;

	case Keyboard::a:
		//app.playAnim();
		break;

	case Keyboard::GRAVE:
		Canvas::instance().toggleConsole();
		break;

	default:
		break;
	}

	return false;
}

static bool keyDown(Event &ev)
{
	return Canvas::instance().handleKeyDownEvent(ev);
}

static bool keyUp(Event &ev)
{
	return Canvas::instance().handleKeyUpEvent(ev);
}


int main(int argc, char **argv)
{
	UNUSED(argc);
	UNUSED(argv);

	std::atexit([]()-> void {
		Rendering::Renderer::instance().clear();
		Window::destroyWindow();
	});

	Window::createWindow(0, 0, 512, 512);
	Renderer::instance().init();
	Canvas::instance().init();

	Renderer::instance().addCamera("main", Camera(Camera::Type::PERSP));

	Profiler profiler;
	float fpsUpdate = 0;

	Game::instance().init();

	bool finished = false;
	while (!finished)
	{
		float dt = Window::getDeltaTime();
		Window::processEvents();

		for (auto &ev : EventHandler::events)
		{
			switch (ev.type)
			{
			case Event::Type::WINDOW_RESIZED:
				Platform::Window::setWindowSize(ev);
				break;

			case Event::Type::KEY_PRESSED:
				keyPress(ev);
				break;

			case Event::Type::KEY_DOWN:
				keyDown(ev);
				break;

			case Event::Type::KEY_UP:
				keyUp(ev);
				break;

			case Event::Type::MOUSE_DRAG:
				mouseDrag(ev);
				break;

			case Event::Type::MOUSE_PRESSED:
				mousePressed(ev);
				break;

			case Event::Type::MOUSE_DOWN:
				mouseDown(ev);
				break;

			case Event::Type::MOUSE_UP:
				mouseUp(ev);
				break;

			default:
				break;
			}
		}

		profiler.begin();

		profiler.start("update");
		update(dt);
		profiler.time("update");

		profiler.start("render");
		render();
		profiler.time("render");

		fpsUpdate += dt;
		if (fpsUpdate > 2)
		{
			profiler.print();
			fpsUpdate = 0;
		}

		Window::swapBuffers();
	}

	return EXIT_SUCCESS;
}

