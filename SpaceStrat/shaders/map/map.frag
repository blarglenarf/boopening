#version 430

// Defined here.
#define THICKNESS 1

// Defined by application on init.
#define ROWS 10
#define COLS 10
#define WIDTH 10
#define HEIGHT 10

#include "../utils.glsl"

// Set at runtime.
uniform mat4 mvMatrix;
uniform mat4 invMvMatrix;

uniform mat4 pMatrix;
uniform mat4 invPMatrix;

uniform ivec4 viewport;
uniform ivec2 cursorPos;



in vec2 texCoord;

out vec4 fragColor;


void main()
{
	fragColor = vec4(0);

	ivec2 cellSize = ivec2(COLS / WIDTH, ROWS / HEIGHT);

	// TODO: Apply camera transforms to the grid.
	// TODO: Apply camera transforms to the cursor.

	// Render lines (wifreframe grid).
	// TODO: Take camera into account.

	// TODO: Transform screen-space coord into object space, then apply camera transformations.
	// TODO: Assume coord is already transformed using the camera, apply inverse transforms.
	vec2 ssFrag = gl_FragCoord.xy;
	vec4 esFrag = getEyeFromWindow(vec3(ssFrag.x, ssFrag.y, -10), viewport, invPMatrix);
	// TODO: Eye-space distance is based on camera zoom (I think).
	vec4 osFrag = invMvMatrix * esFrag;

	vec2 gridFrag = vec2(osFrag.x, osFrag.y);
	ivec2 ssCoord = ivec2(gridFrag);
	if (ssCoord.x == 0 || ssCoord.y == 0)
	{
		fragColor = vec4(1, 0, 0, 1);
	}
	return;
#if 0
	vec4 osFrag = vec4(gl_FragCoord.x / float(viewport.z) * float(WIDTH), gl_FragCoord.y / float(viewport.w) * float(HEIGHT), 0, 1);
	vec4 esFrag = mvMatrix * osFrag;
	vec4 csFrag = pMatrix * esFrag;
	vec2 ssFrag = getScreenFromClip(csFrag, vec2(viewport.z, viewport.w));

	ivec2 ssCoord = ivec2(ssFrag);
#endif
	for (int i = -THICKNESS; i <= THICKNESS; i++)
	{
		if ((ssCoord.x + i) % cellSize.x == 0)
			fragColor = vec4(1);
		if ((ssCoord.y + i) % cellSize.y == 0)
			fragColor = vec4(1);
	}

	// Highlight grid cell using cursorPos.
	//if (coord / cellSize == cursorPos / cellSize)
	//	fragColor = vec4(1, 0, 0, 1);

	// TODO: Any fancy rendering for interior pixels?
}

