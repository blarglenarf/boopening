#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#include <iostream>

// From: https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Instance
// Also worth looking at: https://github.com/SaschaWillems/Vulkan/blob/master/base/vulkanexamplebase.cpp

int main()
{
	glfwInit();

	if (glfwVulkanSupported())
		std::cout << "Vulkan Supported\n";
	else
	{
		std::cout << "Vulkan not supported\n";
		return 0;
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	GLFWwindow *window = glfwCreateWindow(800, 600, "Vulkan window", nullptr, nullptr);

	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

	std::cout << extensionCount << " extensions supported" << std::endl;

	while(!glfwWindowShouldClose(window))
		glfwPollEvents();

	glfwDestroyWindow(window);

	glfwTerminate();

	return 0;
}

