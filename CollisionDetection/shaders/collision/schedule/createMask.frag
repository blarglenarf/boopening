#version 430

#define FRAG_SIZE 2
#define INDEX_BY_TILE 0

#define LFB_NAME_A lfbA
#define LFB_NAME_B lfbB

#import "lfb"

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

#if FRAG_SIZE == 1
	LFB_DEC_1(LFB_NAME_A);
	LFB_DEC_1(LFB_NAME_B);
#elif FRAG_SIZE == 4
	LFB_DEC_4(LFB_NAME_A);
	LFB_DEC_4(LFB_NAME_B);
#else
	LFB_DEC_2(LFB_NAME_A);
	LFB_DEC_2(LFB_NAME_B);
#endif

uniform int interval;

#define DEBUG 0

#if DEBUG
	out vec4 fragColor;
#endif

// Note: this assumes both lfb's are the same resolution.
void main()
{
#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME_A), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(LFB_NAME_A).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif
	int fragCountA = int(LFB_COUNT_AT(LFB_NAME_A, pixel));
	int fragCountB = int(LFB_COUNT_AT(LFB_NAME_B, pixel));

	//int fragCount = int((fragCountA + fragCountB) / 2.0);
	int fragCount = max(fragCountA, fragCountB);
	//int fragCount = fragCountA + fragCountB;

#if DEBUG
	if (fragCount == 0)
		fragColor = vec4(0.5, 0.0, 0.0, 1.0);
	else if (fragCount <= interval)
		fragColor = vec4(debugColLog(fragCount), 1.0);
	else
		fragColor = vec4(debugColLog(fragCount), 1.0);
#else
	if (fragCount <= interval)
		discard;
#endif
}

