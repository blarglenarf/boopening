#version 430

#define DRAW_GEOMETRY 1
#define DRAW_SPHERES 1
#define INDEX_BY_TILE 0

// TODO: If you want to test loading to lmem, then you're going to need to BMA it.
#define USE_REGISTERS 1
#define LOAD_TO_LMEM 0
#define FRAG_SIZE 32

// Start with blocks of size 4 (2 blocks makes 8 registers).
#define BLOCK_SIZE 4

// This shader here for geometry/sphere compositing.

//out vec4 fragColor;

#if DRAW_GEOMETRY || DRAW_SPHERES
	#import "lfb"
#endif

#if DRAW_GEOMETRY
	LFB_DEC(lfb, vec2);
#endif

#if DRAW_SPHERES
	LFB_DEC(sphere, vec2);
#endif

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"


vec2 sphereData[BLOCK_SIZE];
vec2 lfbData[BLOCK_SIZE];

#if LOAD_TO_LMEM
vec2 sphereFrags[FRAG_SIZE];
vec2 lfbFrags[FRAG_SIZE];
#endif

uniform ivec2 size;
//uniform layout(r32f) writeonly imageBuffer compositeTex;

coherent buffer TexBuffer
{
	float texBuffer[];
};

void main()
{
	vec4 fragColor = vec4(1.0);

	int spherePixel = gl_VertexID;
	int lfbPixel = gl_VertexID;

	// TODO: Index by tile will give a different coord.
	ivec2 fragCoord = ivec2(gl_VertexID % size.x, gl_VertexID / size.x);

#if DRAW_GEOMETRY && DRAW_SPHERES

	#if LOAD_TO_LMEM
		int sphereFragCount = 0;
		for (LFB_ITER_BEGIN(sphere, spherePixel); LFB_ITER_CHECK(sphere); LFB_ITER_INC(sphere))
		{
			if (sphereFragCount < FRAG_SIZE)
				sphereFrags[sphereFragCount++] = LFB_GET_DATA(sphere);
		}
		int lfbFragCount = 0;
		for (LFB_ITER_BEGIN(lfb, lfbPixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
		{
			if (lfbFragCount < FRAG_SIZE)
				lfbFrags[lfbFragCount++] = LFB_GET_DATA(lfb);
		}
	#endif

	#if USE_REGISTERS
		// Need to read from both lfb's simultaneously and composite from them in order.
		vec2 f = vec2(0);

		#if LOAD_TO_LMEM
			int currSpherePos = 0;
			int currLfbPos = 0;
		#else
			LFB_ITER_BEGIN(sphere, spherePixel);
			LFB_ITER_BEGIN(lfb, lfbPixel);
		#endif

			int sphereSize = 0, lfbSize = 0;
			int spherePos = 0, lfbPos = 0;

		#if LOAD_TO_LMEM
			#define LOAD_FRAG_A(i) if (i < BLOCK_SIZE && (currSpherePos < sphereFragCount)) { sphereData[i] = sphereFrags[currSpherePos++]; sphereSize++; }
			#define LOAD_FRAG_B(i) if (i < BLOCK_SIZE && (currLfbPos < lfbFragCount)) { lfbData[i] = lfbFrags[currLfbPos++]; lfbSize++; }
		#else
			#define LOAD_FRAG_A(i) if (i < BLOCK_SIZE && (LFB_ITER_CHECK(sphere))) { sphereData[i] = LFB_GET_DATA(sphere); sphereSize++; LFB_ITER_INC(sphere); }
			#define LOAD_FRAG_B(i) if (i < BLOCK_SIZE && (LFB_ITER_CHECK(lfb))) { lfbData[i] = LFB_GET_DATA(lfb); lfbSize++; LFB_ITER_INC(lfb); }
		#endif

			int canary = 1000;
		#if LOAD_TO_LMEM
			while (currSpherePos < sphereFragCount || currLfbPos < lfbFragCount)
		#else
			while ((LFB_ITER_CHECK(sphere)) || (LFB_ITER_CHECK(lfb)))
		#endif
		{
			if (canary-- < 0)
				break;
			// Load the next four fragments from each (or until we reach the end).
			// Apparently this part doesn't need to be unrolled, I would've thought that it did.
			if (sphereSize == 0)
			{
				LOAD_FRAG_A(0)
				LOAD_FRAG_A(1)
				LOAD_FRAG_A(2)
				LOAD_FRAG_A(3)
				#if BLOCK_SIZE > 4
					LOAD_FRAG_A(4)
					LOAD_FRAG_A(5)
					LOAD_FRAG_A(6)
					LOAD_FRAG_A(7)
					#if BLOCK_SIZE > 8
						LOAD_FRAG_A(8)
						LOAD_FRAG_A(9)
						LOAD_FRAG_A(10)
						LOAD_FRAG_A(11)
						LOAD_FRAG_A(12)
						LOAD_FRAG_A(13)
						LOAD_FRAG_A(14)
						LOAD_FRAG_A(15)
						#if BLOCK_SIZE > 16
							LOAD_FRAG_A(16)
							LOAD_FRAG_A(17)
							LOAD_FRAG_A(18)
							LOAD_FRAG_A(19)
							LOAD_FRAG_A(20)
							LOAD_FRAG_A(21)
							LOAD_FRAG_A(22)
							LOAD_FRAG_A(23)
							LOAD_FRAG_A(24)
							LOAD_FRAG_A(25)
							LOAD_FRAG_A(26)
							LOAD_FRAG_A(27)
							LOAD_FRAG_A(28)
							LOAD_FRAG_A(29)
							LOAD_FRAG_A(30)
							LOAD_FRAG_A(31)
						#endif
					#endif
				#endif
			}
			if (lfbSize == 0)
			{
				LOAD_FRAG_B(0)
				LOAD_FRAG_B(1)
				LOAD_FRAG_B(2)
				LOAD_FRAG_B(3)
				#if BLOCK_SIZE > 4
					LOAD_FRAG_B(4)
					LOAD_FRAG_B(5)
					LOAD_FRAG_B(6)
					LOAD_FRAG_B(7)
					#if BLOCK_SIZE > 8
						LOAD_FRAG_B(8)
						LOAD_FRAG_B(9)
						LOAD_FRAG_B(10)
						LOAD_FRAG_B(11)
						LOAD_FRAG_B(12)
						LOAD_FRAG_B(13)
						LOAD_FRAG_B(14)
						LOAD_FRAG_B(15)
						#if BLOCK_SIZE > 16
							LOAD_FRAG_B(16)
							LOAD_FRAG_B(17)
							LOAD_FRAG_B(18)
							LOAD_FRAG_B(19)
							LOAD_FRAG_B(20)
							LOAD_FRAG_B(21)
							LOAD_FRAG_B(22)
							LOAD_FRAG_B(23)
							LOAD_FRAG_B(24)
							LOAD_FRAG_B(25)
							LOAD_FRAG_B(26)
							LOAD_FRAG_B(27)
							LOAD_FRAG_B(28)
							LOAD_FRAG_B(29)
							LOAD_FRAG_B(30)
							LOAD_FRAG_B(31)
						#endif
					#endif
				#endif
			}
			if (sphereSize == 0 || lfbSize == 0)
				break;

		#define GET_AI(i) if (spherePos == i) { fA = sphereData[i]; }
		#define GET_A(i) else if (spherePos == i) { fA = sphereData[i]; }
		#define GET_BI(i) if (lfbPos == i) { fB = lfbData[i]; }
		#define GET_B(i) else if (lfbPos == i) { fB = lfbData[i]; }

			// Iterate over these and composite until one list is empty.
			vec2 fA, fB;
			for (int i = 0; i < BLOCK_SIZE * 2; i++)
			{
				fA = vec2(0, 0);
				fB = vec2(0, 0);
				if (spherePos < sphereSize && lfbPos < lfbSize)
				{
					GET_AI(0)
					GET_A(1)
					GET_A(2)
					GET_A(3)
					#if BLOCK_SIZE > 4
						GET_A(4)
						GET_A(5)
						GET_A(6)
						GET_A(7)
						#if BLOCK_SIZE > 8
							GET_A(8)
							GET_A(9)
							GET_A(10)
							GET_A(11)
							GET_A(12)
							GET_A(13)
							GET_A(14)
							GET_A(15)
							#if BLOCK_SIZE > 16
								GET_A(16)
								GET_A(17)
								GET_A(18)
								GET_A(19)
								GET_A(20)
								GET_A(21)
								GET_A(22)
								GET_A(23)
								GET_A(24)
								GET_A(25)
								GET_A(26)
								GET_A(27)
								GET_A(28)
								GET_A(29)
								GET_A(30)
								GET_A(31)
							#endif
						#endif
					#endif
					GET_BI(0)
					GET_B(1)
					GET_B(2)
					GET_B(3)
					#if BLOCK_SIZE > 4
						GET_B(4)
						GET_B(5)
						GET_B(6)
						GET_B(7)
						#if BLOCK_SIZE > 8
							GET_B(8)
							GET_B(9)
							GET_B(10)
							GET_B(11)
							GET_B(12)
							GET_B(13)
							GET_B(14)
							GET_B(15)
							#if BLOCK_SIZE > 16
								GET_B(16)
								GET_B(17)
								GET_B(18)
								GET_B(19)
								GET_B(20)
								GET_B(21)
								GET_B(22)
								GET_B(23)
								GET_B(24)
								GET_B(25)
								GET_B(26)
								GET_B(27)
								GET_B(28)
								GET_B(29)
								GET_B(30)
								GET_B(31)
							#endif
						#endif
					#endif
					if (fA.y >= fB.y)
					{
						spherePos++;
						vec4 col = floatToRGBA8(fA.x);
						fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
					}
					else
					{
						lfbPos++;
						vec4 col = floatToRGBA8(fB.x);
						fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
					}
				}
			}
			if (spherePos == sphereSize)
			{
				spherePos = 0;
				sphereSize = 0;
			}
			if (lfbPos == lfbSize)
			{
				lfbPos = 0;
				lfbSize = 0;
			}
		}

	#undef GET_A
	#undef GET_B
	#define GET_A(i) if (i < sphereSize && i >= spherePos) { vec4 col = floatToRGBA8(sphereData[i].x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); }
	#define GET_B(i) if (i < lfbSize && i >= lfbPos) { vec4 col = floatToRGBA8(lfbData[i].x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); }

		// Composite any fragments left in the blocks.
		if (spherePos < sphereSize)
		{
			GET_A(0)
			GET_A(1)
			GET_A(2)
			GET_A(3)
			#if BLOCK_SIZE > 4
				GET_A(4)
				GET_A(5)
				GET_A(6)
				GET_A(7)
				#if BLOCK_SIZE > 8
					GET_A(8)
					GET_A(9)
					GET_A(10)
					GET_A(11)
					GET_A(12)
					GET_A(13)
					GET_A(14)
					GET_A(15)
					#if BLOCK_SIZE > 16
						GET_A(16)
						GET_A(17)
						GET_A(18)
						GET_A(19)
						GET_A(20)
						GET_A(21)
						GET_A(22)
						GET_A(23)
						GET_A(24)
						GET_A(25)
						GET_A(26)
						GET_A(27)
						GET_A(28)
						GET_A(29)
						GET_A(30)
						GET_A(31)
					#endif
				#endif
			#endif
		}
		if (lfbPos < lfbSize)
		{
			GET_B(0)
			GET_B(1)
			GET_B(2)
			GET_B(3)
			#if BLOCK_SIZE > 4
				GET_B(4)
				GET_B(5)
				GET_B(6)
				GET_B(7)
				#if BLOCK_SIZE > 8
					GET_B(8)
					GET_B(9)
					GET_B(10)
					GET_B(11)
					GET_B(12)
					GET_B(13)
					GET_B(14)
					GET_B(15)
					#if BLOCK_SIZE > 16
						GET_B(16)
						GET_B(17)
						GET_B(18)
						GET_B(19)
						GET_B(20)
						GET_B(21)
						GET_B(22)
						GET_B(23)
						GET_B(24)
						GET_B(25)
						GET_B(26)
						GET_B(27)
						GET_B(28)
						GET_B(29)
						GET_B(30)
						GET_B(31)
					#endif
				#endif
			#endif
		}
		// If any list still has fragments in it, composite them.
		// TODO: Not sure if this needs to be (or even can be) optimised.
		#if LOAD_TO_LMEM
			while (currSpherePos < sphereFragCount)
			{
				f = sphereFrags[currSpherePos++];
				vec4 col = floatToRGBA8(f.x);
				fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
			}
			while (currLfbPos < lfbFragCount)
			{
				f = lfbFrags[currLfbPos++];
				vec4 col = floatToRGBA8(f.x);
				fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
			}
		#else
			while ((LFB_ITER_CHECK(sphere)))
			{
				f = LFB_GET_DATA(sphere);
				LFB_ITER_INC(sphere);
				vec4 col = floatToRGBA8(f.x);
				fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
			}
			while ((LFB_ITER_CHECK(lfb)))
			{
				f = LFB_GET_DATA(lfb);
				LFB_ITER_INC(lfb);
				vec4 col = floatToRGBA8(f.x);
				fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
			}
		#endif
	#else
		// Need to read from both lfb's simultaneously and composite from them in order.
		vec2 f = vec2(0);

		LFB_ITER_BEGIN(sphere, spherePixel);
		LFB_ITER_BEGIN(lfb, lfbPixel);

		int sphereSize = 0, lfbSize = 0;
		int spherePos = 0, lfbPos = 0;

		int canary = 1000;
		while ((LFB_ITER_CHECK(sphere)) || (LFB_ITER_CHECK(lfb)))
		{
			if (canary-- < 0)
				break;
			// Load the next four fragments from each (or until we reach the end).
			if (sphereSize == 0)
			{
				for (int i = 0; i < BLOCK_SIZE; i++)
				{
					if (!(LFB_ITER_CHECK(sphere)))
						break;
					sphereData[i] = LFB_GET_DATA(sphere);
					sphereSize++;
					LFB_ITER_INC(sphere);
				}
			}
			if (lfbSize == 0)
			{
				for (int i = 0; i < BLOCK_SIZE; i++)
				{
					if (!(LFB_ITER_CHECK(lfb)))
						break;
					lfbData[i] = LFB_GET_DATA(lfb);
					lfbSize++;
					LFB_ITER_INC(lfb);
				}
			}
			if (sphereSize == 0 || lfbSize == 0)
				break;

			// Iterate over these and composite until one list is empty.
			f = vec2(0);
			if (sphereSize > 0 && lfbSize > 0)
			{
				while (spherePos < sphereSize && lfbPos < lfbSize)
				{
					if (sphereData[spherePos].y >= lfbData[lfbPos].y)
						f = sphereData[spherePos++];
					else
						f = lfbData[lfbPos++];
					vec4 col = floatToRGBA8(f.x);
					fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
				}
			}
			if (spherePos == sphereSize)
			{
				spherePos = 0;
				sphereSize = 0;
			}
			if (lfbPos == lfbSize)
			{
				lfbPos = 0;
				lfbSize = 0;
			}
		}

		// Composite any fragments left in the blocks.
		while (spherePos < sphereSize)
		{
			f = sphereData[spherePos++];
			vec4 col = floatToRGBA8(f.x);
			fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		}
		while (lfbPos < lfbSize)
		{
			f = lfbData[lfbPos++];
			vec4 col = floatToRGBA8(f.x);
			fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		}
		// If any list still has fragments in it, composite them.
		while ((LFB_ITER_CHECK(sphere)))
		{
			f = LFB_GET_DATA(sphere);
			LFB_ITER_INC(sphere);
			vec4 col = floatToRGBA8(f.x);
			fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		}
		while ((LFB_ITER_CHECK(lfb)))
		{
			f = LFB_GET_DATA(lfb);
			LFB_ITER_INC(lfb);
			vec4 col = floatToRGBA8(f.x);
			fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		}
	#endif

#elif DRAW_GEOMETRY
	// Read from geometry global memory and composite.
	for (LFB_ITER_BEGIN(lfb, lfbPixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		vec2 f = LFB_GET_DATA(lfb);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#elif DRAW_SPHERES
	// Read from sphere global memory and composite.
	for (LFB_ITER_BEGIN(sphere, spherePixel); LFB_ITER_CHECK(sphere); LFB_ITER_INC(sphere))
	{
		vec2 f = LFB_GET_DATA(sphere);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif

	//imageStore(compositeTex, fragCoord, fragColor);
	//imageStore(compositeTex, lfbPixel, fragColor);
	float c = rgba8ToFloat(fragColor);
	texBuffer[gl_VertexID] = c;
}

