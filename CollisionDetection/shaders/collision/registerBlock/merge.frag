#version 430

#define INDEX_BY_TILE 0


// Start with blocks of size 4 (2 blocks makes 8 registers).
#define BLOCK_SIZE 4

#define BLOCK_WRITE 0


// This shader here for geometry/sphere compositing.

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;


#import "lfb"

LFB_DEC(lfb0, vec2);
LFB_DEC(lfb1, vec2);

LFB_DEC(lfb, vec2);

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

uniform int reverseOrder;


vec2 lfb0Data[BLOCK_SIZE];
vec2 lfb1Data[BLOCK_SIZE];


int writeSize;

#if BLOCK_WRITE
	vec2 lfbWrite[BLOCK_SIZE];
#endif


void writeBlock(int pixel)
{
#if BLOCK_WRITE
	#define WRITE_FRAG(i) if (i < writeSize) { LFB_ADD_DATA(lfb, pixel, lfbWrite[i]); }

	WRITE_FRAG(0)
	WRITE_FRAG(1)
	WRITE_FRAG(2)
	WRITE_FRAG(3)
	#if BLOCK_SIZE > 4
		WRITE_FRAG(4)
		WRITE_FRAG(5)
		WRITE_FRAG(6)
		WRITE_FRAG(7)
		#if BLOCK_SIZE > 8
			WRITE_FRAG(8)
			WRITE_FRAG(9)
			WRITE_FRAG(10)
			WRITE_FRAG(11)
			WRITE_FRAG(12)
			WRITE_FRAG(13)
			WRITE_FRAG(14)
			WRITE_FRAG(15)
		#endif
	#endif

	#undef WRITE_FRAG
#endif
	writeSize = 0;
}

void writeData(int pixel, vec2 f)
{
#if BLOCK_WRITE
	#define WRITE_FRAG(i) else if (i == n) { lfbWrite[i] = f; }

	int n = writeSize++;

	if (0 == n) { lfbWrite[0] = f; }
	WRITE_FRAG(1)
	WRITE_FRAG(2)
	WRITE_FRAG(3)
	#if BLOCK_SIZE > 4
		WRITE_FRAG(4)
		WRITE_FRAG(5)
		WRITE_FRAG(6)
		WRITE_FRAG(7)
		#if BLOCK_SIZE > 8
			WRITE_FRAG(8)
			WRITE_FRAG(9)
			WRITE_FRAG(10)
			WRITE_FRAG(11)
			WRITE_FRAG(12)
			WRITE_FRAG(13)
			WRITE_FRAG(14)
			WRITE_FRAG(15)
		#endif
	#endif

	#undef WRITE_FRAG

	if (writeSize >= BLOCK_SIZE)
		writeBlock(pixel);
#else
	LFB_ADD_DATA(lfb, pixel, f);
#endif
}


void main()
{
	writeSize = 0;

	//fragColor = vec4(1.0);

#if INDEX_BY_TILE
	int lfbPixel = tilesIndex(LFB_GET_SIZE(lfb), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int lfbPixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	// Need to read from both lfb's simultaneously and composite from them in order.
	vec2 f = vec2(0);

	int lfbSize[2];
	lfbSize[0] = 0;
	lfbSize[1] = 0;

	int lfbPos[2];
	lfbPos[0] = 0;
	lfbPos[1] = 0;

	LFB_ITER_BEGIN(lfb0, lfbPixel);
	LFB_ITER_BEGIN(lfb1, lfbPixel);

	#define LOAD_FRAG_A(i) if (i < BLOCK_SIZE && (LFB_ITER_CHECK(lfb0))) { lfb0Data[i] = LFB_GET_DATA(lfb0); lfbSize[0]++; LFB_ITER_INC(lfb0); }
	#define LOAD_FRAG_B(i) if (i < BLOCK_SIZE && (LFB_ITER_CHECK(lfb1))) { lfb1Data[i] = LFB_GET_DATA(lfb1); lfbSize[1]++; LFB_ITER_INC(lfb1); }

	int canary = 1000;
	while ((LFB_ITER_CHECK(lfb0)) || (LFB_ITER_CHECK(lfb1)))
	{
		if (canary-- < 0)
			break;

		// Load the next four fragments from each (or until we reach the end).
		// Apparently this part doesn't need to be unrolled, I would've thought that it did.
		if (lfbSize[0] == 0)
		{
			LOAD_FRAG_A(0)
			LOAD_FRAG_A(1)
			LOAD_FRAG_A(2)
			LOAD_FRAG_A(3)
			#if BLOCK_SIZE > 4
				LOAD_FRAG_A(4)
				LOAD_FRAG_A(5)
				LOAD_FRAG_A(6)
				LOAD_FRAG_A(7)
				#if BLOCK_SIZE > 8
					LOAD_FRAG_A(8)
					LOAD_FRAG_A(9)
					LOAD_FRAG_A(10)
					LOAD_FRAG_A(11)
					LOAD_FRAG_A(12)
					LOAD_FRAG_A(13)
					LOAD_FRAG_A(14)
					LOAD_FRAG_A(15)
				#endif
			#endif
		}
		if (lfbSize[1] == 0)
		{
			LOAD_FRAG_B(0)
			LOAD_FRAG_B(1)
			LOAD_FRAG_B(2)
			LOAD_FRAG_B(3)
			#if BLOCK_SIZE > 4
				LOAD_FRAG_B(4)
				LOAD_FRAG_B(5)
				LOAD_FRAG_B(6)
				LOAD_FRAG_B(7)
				#if BLOCK_SIZE > 8
					LOAD_FRAG_B(8)
					LOAD_FRAG_B(9)
					LOAD_FRAG_B(10)
					LOAD_FRAG_B(11)
					LOAD_FRAG_B(12)
					LOAD_FRAG_B(13)
					LOAD_FRAG_B(14)
					LOAD_FRAG_B(15)
				#endif
			#endif
		}
		if (lfbSize[0] == 0 || lfbSize[1] == 0)
			break;

	#define GET_AI(i) if (lfbPos[0] == i) { f0 = lfb0Data[i]; }
	#define GET_A(i) else if (lfbPos[0] == i) { f0 = lfb0Data[i]; }
	#define GET_BI(i) if (lfbPos[1] == i) { f1 = lfb1Data[i]; }
	#define GET_B(i) else if (lfbPos[1] == i) { f1 = lfb1Data[i]; }

		// Iterate over these and composite until one list is empty.
		vec2 f0, f1;
		for (int i = 0; i < BLOCK_SIZE * 2; i++)
		{
			f0 = vec2(0, 0);
			f1 = vec2(0, 0);
			if (lfbPos[0] < lfbSize[0] && lfbPos[1] < lfbSize[1])
			{
				GET_AI(0)
				GET_A(1)
				GET_A(2)
				GET_A(3)
				#if BLOCK_SIZE > 4
					GET_A(4)
					GET_A(5)
					GET_A(6)
					GET_A(7)
					#if BLOCK_SIZE > 8
						GET_A(8)
						GET_A(9)
						GET_A(10)
						GET_A(11)
						GET_A(12)
						GET_A(13)
						GET_A(14)
						GET_A(15)
					#endif
				#endif
				GET_BI(0)
				GET_B(1)
				GET_B(2)
				GET_B(3)
				#if BLOCK_SIZE > 4
					GET_B(4)
					GET_B(5)
					GET_B(6)
					GET_B(7)
					#if BLOCK_SIZE > 8
						GET_B(8)
						GET_B(9)
						GET_B(10)
						GET_B(11)
						GET_B(12)
						GET_B(13)
						GET_B(14)
						GET_B(15)
					#endif
				#endif

				if (reverseOrder == 1)
				{
					if (f0.y < f1.y)
					{
						lfbPos[0]++;
						writeData(lfbPixel, f0);
						//vec4 col = floatToRGBA8(f0.x);
						//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
					}
					else
					{
						lfbPos[1]++;
						writeData(lfbPixel, f1);
						//vec4 col = floatToRGBA8(f1.x);
						//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
					}
				}
				else
				{
					if (f0.y >= f1.y)
					{
						lfbPos[0]++;
						writeData(lfbPixel, f0);
						//vec4 col = floatToRGBA8(f0.x);
						//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
					}
					else
					{
						lfbPos[1]++;
						writeData(lfbPixel, f1);
						//vec4 col = floatToRGBA8(f1.x);
						//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
					}
				}
			}
		}
		if (lfbPos[0] == lfbSize[0])
		{
			lfbPos[0] = 0;
			lfbSize[0] = 0;
		}
		if (lfbPos[1] == lfbSize[1])
		{
			lfbPos[1] = 0;
			lfbSize[1] = 0;
		}
	}

#undef GET_A
#undef GET_B

// TODO: This needs to write data instead.
//#define GET_A(i) if (i < lfbSize[0] && i >= lfbPos[0]) { vec4 col = floatToRGBA8(lfb0Data[i].x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); }
//#define GET_B(i) if (i < lfbSize[1] && i >= lfbPos[1]) { vec4 col = floatToRGBA8(lfb1Data[i].x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); }

#define GET_A(i) if (i < lfbSize[0] && i >= lfbPos[0]) { writeData(lfbPixel, lfb0Data[i]); }
#define GET_B(i) if (i < lfbSize[1] && i >= lfbPos[1]) { writeData(lfbPixel, lfb1Data[i]); }

	// Composite any fragments left in the blocks.
	// FIXME: What if there are fragments in both blocks? Can this even happen?
	if (lfbPos[0] < lfbSize[0] && lfbPos[1] < lfbSize[1])
	{
		//fragColor = vec4(0.0, 1.0, 0.0, 1.0);
		// If there are fragments left in the write block, write them.
		writeBlock(lfbPixel);
		return;
	}
	if (lfbPos[0] < lfbSize[0])
	{
		GET_A(0)
		GET_A(1)
		GET_A(2)
		GET_A(3)
		#if BLOCK_SIZE > 4
			GET_A(4)
			GET_A(5)
			GET_A(6)
			GET_A(7)
			#if BLOCK_SIZE > 8
				GET_A(8)
				GET_A(9)
				GET_A(10)
				GET_A(11)
				GET_A(12)
				GET_A(13)
				GET_A(14)
				GET_A(15)
			#endif
		#endif
	}
	if (lfbPos[1] < lfbSize[1])
	{
		GET_B(0)
		GET_B(1)
		GET_B(2)
		GET_B(3)
		#if BLOCK_SIZE > 4
			GET_B(4)
			GET_B(5)
			GET_B(6)
			GET_B(7)
			#if BLOCK_SIZE > 8
				GET_B(8)
				GET_B(9)
				GET_B(10)
				GET_B(11)
				GET_B(12)
				GET_B(13)
				GET_B(14)
				GET_B(15)
			#endif
		#endif
	}

	// If any list still has fragments in it, composite them.
	// TODO: This now needs to write data instead.
	while ((LFB_ITER_CHECK(lfb0)))
	{
		f = LFB_GET_DATA(lfb0);
		LFB_ITER_INC(lfb0);
		writeData(lfbPixel, f);
		//vec4 col = floatToRGBA8(f.x);
		//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
	while ((LFB_ITER_CHECK(lfb1)))
	{
		f = LFB_GET_DATA(lfb1);
		LFB_ITER_INC(lfb1);
		writeData(lfbPixel, f);
		//vec4 col = floatToRGBA8(f.x);
		//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}

	// Any frags left in the block?
	writeBlock(lfbPixel);
}

