#version 430

#define DRAW_LFB_A 1
#define DRAW_LFB_B 1
#define INDEX_BY_TILE 0


// Start with blocks of size 4 (2 blocks makes 8 registers).
#define BLOCK_SIZE 4

// This shader here for geometry/sphere compositing.

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;

#define LFB_A lfbA
#define LFB_B lfbB

#if DRAW_LFB_A || DRAW_LFB_B
	#import "lfb"
#endif

#if DRAW_LFB_A
	LFB_DEC(LFB_A, vec2);
#endif

#if DRAW_LFB_B
	LFB_DEC(LFB_B, vec2);
#endif

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"


vec2 lfbAData[BLOCK_SIZE];
vec2 lfbBData[BLOCK_SIZE];

void main()
{
	fragColor = vec4(1.0);

#if INDEX_BY_TILE
	#if DRAW_LFB_A
		int lfbAPixel = tilesIndex(LFB_GET_SIZE(LFB_A), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
	#endif
	#if DRAW_LFB_B
		int lfbBPixel = tilesIndex(LFB_GET_SIZE(LFB_B), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
	#endif
#else
	#if DRAW_LFB_A
		int lfbAPixel = LFB_GET_SIZE(LFB_A).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	#endif
	#if DRAW_LFB_B
		int lfbBPixel = LFB_GET_SIZE(LFB_B).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	#endif
#endif

#if DRAW_LFB_A && DRAW_LFB_B
	// Need to read from both lfb's simultaneously and composite from them in order.
	vec2 f = vec2(0);

	LFB_ITER_BEGIN(LFB_A, lfbAPixel);
	LFB_ITER_BEGIN(LFB_B, lfbBPixel);

	int lfbASize = 0, lfbBSize = 0;
	int lfbAPos = 0, lfbBPos = 0;

	#define LOAD_FRAG_A(i) if (i < BLOCK_SIZE && (LFB_ITER_CHECK(LFB_A))) { lfbAData[i] = LFB_GET_DATA(LFB_A); lfbASize++; LFB_ITER_INC(LFB_A); }
	#define LOAD_FRAG_B(i) if (i < BLOCK_SIZE && (LFB_ITER_CHECK(LFB_B))) { lfbBData[i] = LFB_GET_DATA(LFB_B); lfbBSize++; LFB_ITER_INC(LFB_B); }

	int canary = 1000;
	while ((LFB_ITER_CHECK(LFB_A)) || (LFB_ITER_CHECK(LFB_B)))
	{
		if (canary-- < 0)
			break;
		// Load the next four fragments from each (or until we reach the end).
		// Apparently this part doesn't need to be unrolled, I would've thought that it did.
		if (lfbASize == 0)
		{
			LOAD_FRAG_A(0)
			LOAD_FRAG_A(1)
			LOAD_FRAG_A(2)
			LOAD_FRAG_A(3)
			#if BLOCK_SIZE > 4
				LOAD_FRAG_A(4)
				LOAD_FRAG_A(5)
				LOAD_FRAG_A(6)
				LOAD_FRAG_A(7)
				#if BLOCK_SIZE > 8
					LOAD_FRAG_A(8)
					LOAD_FRAG_A(9)
					LOAD_FRAG_A(10)
					LOAD_FRAG_A(11)
					LOAD_FRAG_A(12)
					LOAD_FRAG_A(13)
					LOAD_FRAG_A(14)
					LOAD_FRAG_A(15)
					#if BLOCK_SIZE > 16
						LOAD_FRAG_A(16)
						LOAD_FRAG_A(17)
						LOAD_FRAG_A(18)
						LOAD_FRAG_A(19)
						LOAD_FRAG_A(20)
						LOAD_FRAG_A(21)
						LOAD_FRAG_A(22)
						LOAD_FRAG_A(23)
						LOAD_FRAG_A(24)
						LOAD_FRAG_A(25)
						LOAD_FRAG_A(26)
						LOAD_FRAG_A(27)
						LOAD_FRAG_A(28)
						LOAD_FRAG_A(29)
						LOAD_FRAG_A(30)
						LOAD_FRAG_A(31)
					#endif
				#endif
			#endif
		}
		if (lfbBSize == 0)
		{
			LOAD_FRAG_B(0)
			LOAD_FRAG_B(1)
			LOAD_FRAG_B(2)
			LOAD_FRAG_B(3)
			#if BLOCK_SIZE > 4
				LOAD_FRAG_B(4)
				LOAD_FRAG_B(5)
				LOAD_FRAG_B(6)
				LOAD_FRAG_B(7)
				#if BLOCK_SIZE > 8
					LOAD_FRAG_B(8)
					LOAD_FRAG_B(9)
					LOAD_FRAG_B(10)
					LOAD_FRAG_B(11)
					LOAD_FRAG_B(12)
					LOAD_FRAG_B(13)
					LOAD_FRAG_B(14)
					LOAD_FRAG_B(15)
					#if BLOCK_SIZE > 16
						LOAD_FRAG_B(16)
						LOAD_FRAG_B(17)
						LOAD_FRAG_B(18)
						LOAD_FRAG_B(19)
						LOAD_FRAG_B(20)
						LOAD_FRAG_B(21)
						LOAD_FRAG_B(22)
						LOAD_FRAG_B(23)
						LOAD_FRAG_B(24)
						LOAD_FRAG_B(25)
						LOAD_FRAG_B(26)
						LOAD_FRAG_B(27)
						LOAD_FRAG_B(28)
						LOAD_FRAG_B(29)
						LOAD_FRAG_B(30)
						LOAD_FRAG_B(31)
					#endif
				#endif
			#endif
		}
		if (lfbASize == 0 || lfbBSize == 0)
			break;

	#define GET_AI(i) if (lfbAPos == i) { fA = lfbAData[i]; }
	#define GET_A(i) else if (lfbAPos == i) { fA = lfbAData[i]; }
	#define GET_BI(i) if (lfbBPos == i) { fB = lfbBData[i]; }
	#define GET_B(i) else if (lfbBPos == i) { fB = lfbBData[i]; }

		// Iterate over these and composite until one list is empty.
		vec2 fA, fB;
		for (int i = 0; i < BLOCK_SIZE * 2; i++)
		{
			fA = vec2(0, 0);
			fB = vec2(0, 0);
			if (lfbAPos < lfbASize && lfbBPos < lfbBSize)
			{
				GET_AI(0)
				GET_A(1)
				GET_A(2)
				GET_A(3)
				#if BLOCK_SIZE > 4
					GET_A(4)
					GET_A(5)
					GET_A(6)
					GET_A(7)
					#if BLOCK_SIZE > 8
						GET_A(8)
						GET_A(9)
						GET_A(10)
						GET_A(11)
						GET_A(12)
						GET_A(13)
						GET_A(14)
						GET_A(15)
						#if BLOCK_SIZE > 16
							GET_A(16)
							GET_A(17)
							GET_A(18)
							GET_A(19)
							GET_A(20)
							GET_A(21)
							GET_A(22)
							GET_A(23)
							GET_A(24)
							GET_A(25)
							GET_A(26)
							GET_A(27)
							GET_A(28)
							GET_A(29)
							GET_A(30)
							GET_A(31)
						#endif
					#endif
				#endif
				GET_BI(0)
				GET_B(1)
				GET_B(2)
				GET_B(3)
				#if BLOCK_SIZE > 4
					GET_B(4)
					GET_B(5)
					GET_B(6)
					GET_B(7)
					#if BLOCK_SIZE > 8
						GET_B(8)
						GET_B(9)
						GET_B(10)
						GET_B(11)
						GET_B(12)
						GET_B(13)
						GET_B(14)
						GET_B(15)
						#if BLOCK_SIZE > 16
							GET_B(16)
							GET_B(17)
							GET_B(18)
							GET_B(19)
							GET_B(20)
							GET_B(21)
							GET_B(22)
							GET_B(23)
							GET_B(24)
							GET_B(25)
							GET_B(26)
							GET_B(27)
							GET_B(28)
							GET_B(29)
							GET_B(30)
							GET_B(31)
						#endif
					#endif
				#endif
				if (fA.y >= fB.y)
				{
					lfbAPos++;
					vec4 col = floatToRGBA8(fA.x);
					fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
				}
				else
				{
					lfbBPos++;
					vec4 col = floatToRGBA8(fB.x);
					fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
				}
			}
		}
		if (lfbAPos == lfbASize)
		{
			lfbAPos = 0;
			lfbASize = 0;
		}
		if (lfbBPos == lfbBSize)
		{
			lfbBPos = 0;
			lfbBSize = 0;
		}
	}

#undef GET_A
#undef GET_B
#define GET_A(i) if (i < lfbASize && i >= lfbAPos) { vec4 col = floatToRGBA8(lfbAData[i].x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); }
#define GET_B(i) if (i < lfbBSize && i >= lfbBPos) { vec4 col = floatToRGBA8(lfbBData[i].x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); }

	// Composite any fragments left in the blocks.
	// FIXME: What if there are fragments in both blocks? Can this even happen?
	if (lfbAPos < lfbASize && lfbBPos < lfbBSize)
	{
		fragColor = vec4(0.0, 1.0, 0.0, 1.0);
		return;
	}
	if (lfbAPos < lfbASize)
	{
		GET_A(0)
		GET_A(1)
		GET_A(2)
		GET_A(3)
		#if BLOCK_SIZE > 4
			GET_A(4)
			GET_A(5)
			GET_A(6)
			GET_A(7)
			#if BLOCK_SIZE > 8
				GET_A(8)
				GET_A(9)
				GET_A(10)
				GET_A(11)
				GET_A(12)
				GET_A(13)
				GET_A(14)
				GET_A(15)
				#if BLOCK_SIZE > 16
					GET_A(16)
					GET_A(17)
					GET_A(18)
					GET_A(19)
					GET_A(20)
					GET_A(21)
					GET_A(22)
					GET_A(23)
					GET_A(24)
					GET_A(25)
					GET_A(26)
					GET_A(27)
					GET_A(28)
					GET_A(29)
					GET_A(30)
					GET_A(31)
				#endif
			#endif
		#endif
	}
	if (lfbBPos < lfbBSize)
	{
		GET_B(0)
		GET_B(1)
		GET_B(2)
		GET_B(3)
		#if BLOCK_SIZE > 4
			GET_B(4)
			GET_B(5)
			GET_B(6)
			GET_B(7)
			#if BLOCK_SIZE > 8
				GET_B(8)
				GET_B(9)
				GET_B(10)
				GET_B(11)
				GET_B(12)
				GET_B(13)
				GET_B(14)
				GET_B(15)
				#if BLOCK_SIZE > 16
					GET_B(16)
					GET_B(17)
					GET_B(18)
					GET_B(19)
					GET_B(20)
					GET_B(21)
					GET_B(22)
					GET_B(23)
					GET_B(24)
					GET_B(25)
					GET_B(26)
					GET_B(27)
					GET_B(28)
					GET_B(29)
					GET_B(30)
					GET_B(31)
				#endif
			#endif
		#endif
	}
	// If any list still has fragments in it, composite them.
	// TODO: Not sure if this needs to be (or even can be) optimised.
	while ((LFB_ITER_CHECK(LFB_A)))
	{
		f = LFB_GET_DATA(LFB_A);
		LFB_ITER_INC(LFB_A);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
	while ((LFB_ITER_CHECK(LFB_B)))
	{
		f = LFB_GET_DATA(LFB_B);
		LFB_ITER_INC(LFB_B);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#elif DRAW_LFB_A
	// Read from geometry global memory and composite.
	for (LFB_ITER_BEGIN(LFB_A, lfbAPixel); LFB_ITER_CHECK(LFB_A); LFB_ITER_INC(LFB_A))
	{
		vec2 f = LFB_GET_DATA(LFB_A);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#elif DRAW_LFB_B
	// Read from sphere global memory and composite.
	for (LFB_ITER_BEGIN(LFB_B, lfbBPixel); LFB_ITER_CHECK(LFB_B); LFB_ITER_INC(LFB_B))
	{
		vec2 f = LFB_GET_DATA(LFB_B);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif
}

