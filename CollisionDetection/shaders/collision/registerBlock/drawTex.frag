#version 430

#include "../../utils.glsl"

out vec4 fragColor;

//uniform sampler2D compositeTex;
uniform ivec2 size;

readonly buffer TexBuffer
{
	float texBuffer[];
};

void main()
{
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	float color = texBuffer[pixel];

	//fragColor = texelFetch(compositeTex, ivec2(gl_FragCoord.xy), 0);
	vec4 c = floatToRGBA8(color);
	fragColor = c;
}

