#version 430

#define DRAW_GEOMETRY 1
#define DRAW_SPHERES 1
#define INDEX_BY_TILE 0

// This shader here for geometry/sphere compositing.

#if DRAW_GEOMETRY || DRAW_SPHERES
	#import "lfb"
#endif

#if DRAW_GEOMETRY
	LFB_DEC(lfb, vec2);
#endif

#if DRAW_SPHERES
	LFB_DEC(sphere, vec2);
#endif

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

uniform ivec2 size;
//uniform layout(r32f) writeonly imageBuffer compositeTex;

coherent buffer TexBuffer
{
	float texBuffer[];
};

void main()
{
	vec4 fragColor = vec4(1.0);

	int spherePixel = gl_VertexID;
	int lfbPixel = gl_VertexID;

	// TODO: Index by tile will give a different coord.
	ivec2 fragCoord = ivec2(gl_VertexID % size.x, gl_VertexID / size.x);

#if DRAW_GEOMETRY && DRAW_SPHERES
	// Need to read from both lfb's simultaneously and composite from them in order.
	vec2 currSphereFrag = vec2(0);
	vec2 currLfbFrag = vec2(0);

	LFB_ITER_BEGIN(sphere, spherePixel);
	LFB_ITER_BEGIN(lfb, lfbPixel);

	bool sphereFlag = true, lfbFlag = true;

	int canary = 1000;
	while ((LFB_ITER_CHECK(sphere)) || (LFB_ITER_CHECK(lfb)))
	{
		if (canary-- < 0)
			break;
		if (sphereFlag && (LFB_ITER_CHECK(sphere)))
		{
			currSphereFrag = LFB_GET_DATA(sphere);
			sphereFlag = false;
			LFB_ITER_INC(sphere);
		}
		if (lfbFlag && (LFB_ITER_CHECK(lfb)))
		{
			currLfbFrag = LFB_GET_DATA(lfb);
			lfbFlag = false;
			LFB_ITER_INC(lfb);
		}

		vec2 f = vec2(0);
		if (currSphereFrag.y >= currLfbFrag.y)
		{
			f = currSphereFrag;
			currSphereFrag = vec2(0, 0);
			sphereFlag = true;
		}
		else
		{
			f = currLfbFrag;
			currLfbFrag = vec2(0, 0);
			lfbFlag = true;
		}
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}

	// Write remaining value if necessary.
	if (!sphereFlag)
	{
		vec4 col = floatToRGBA8(currSphereFrag.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
	if (!lfbFlag)
	{
		vec4 col = floatToRGBA8(currLfbFrag.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#elif DRAW_GEOMETRY
	// Read from geometry global memory and composite.
	for (LFB_ITER_BEGIN(lfb, lfbPixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		vec2 f = LFB_GET_DATA(lfb);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#elif DRAW_SPHERES
	// Read from sphere global memory and composite.
	for (LFB_ITER_BEGIN(sphere, spherePixel); LFB_ITER_CHECK(sphere); LFB_ITER_INC(sphere))
	{
		vec2 f = LFB_GET_DATA(sphere);
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif

	float c = rgba8ToFloat(fragColor);
	texBuffer[gl_VertexID] = c;
}

