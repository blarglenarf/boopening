#version 430

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

// FIXME: Get rid of anything that uses this!
//#define FRAG_SIZE 2
#define LFB_BLOCK_SIZE 32
#define INDEX_BY_TILE 0

#define LFB_FRAG_TYPE vec2
#define USE_FLOAT 1

out vec4 fragColor;

#import "lfb"

#include "../../../utils.glsl"
#include "../../../lfb/tiles.glsl"

// To use different deep images, call replace(LFB_NAME, LFB_FRAG_TYPE).
LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);

// This is the deep image that has already been saved, to be merged with this one.
LFB_DEC(lfbA, LFB_FRAG_TYPE);


#define COMPOSITE 0
#define COMPOSITE_LOCAL 0

#define MAX_FRAGS 0
#define MAX_FRAGS_OVERRIDE 0

#if MAX_FRAGS_OVERRIDE != 0
	#define _MAX_FRAGS MAX_FRAGS_OVERRIDE
#else
	#define _MAX_FRAGS MAX_FRAGS
#endif

//#define LFB_FRAG_TYPE vec2
#define LFB_FRAG_DEPTH(frag) frag.y

LFB_FRAG_TYPE frags[_MAX_FRAGS];


#define LFB_GET_FRAG(LFB_NAME) LFB_GET_DATA(LFB_NAME)
#define LFB_PUT_FRAG(LFB_NAME, frag) LFB_WRITE_DATA(LFB_NAME, frag)


#if USE_FLOAT
	#define GET_COLOR(val) floatToRGBA8(val)
#else
	#define GET_COLOR(val) uintToRGBA8(val)
#endif


#include "sortMerge.glsl"


void main()
{
	fragColor = vec4(0.0);

#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

#if 0
	if (LFB_COUNT_AT(LFB_NAME, pixel) < 128)
	{
		return;
	}
#endif

	// Load into local memory and sort. This will also write to global memory.
	int fragCount = sort(pixel);

#if COMPOSITE
	// Read from global memory and composite.
	for (LFB_ITER_BEGIN(LFB_NAME, pixel); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		LFB_FRAG_TYPE f = LFB_GET_FRAG(LFB_NAME);
		vec4 col = GET_COLOR(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif

#if COMPOSITE_LOCAL && 0
	#if _MAX_FRAGS <= LFB_BLOCK_SIZE // Dirty hack to account for rbs not sorting in place.
	for (int i = fragCount - 1; i >= 0; i--)
	{
		LFB_FRAG_TYPE f = frags[i];
		vec4 col = GET_COLOR(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
	#endif
#endif
#if (COMPOSITE || COMPOSITE_LOCAL) && 0
	float dc = MAX_FRAGS_OVERRIDE / float(MAX_FRAGS);
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
	fragColor.a = 1.0;
#endif
}

