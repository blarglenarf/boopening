#version 430

#define INDEX_BY_TILE 0

// This shader here for geometry/sphere compositing.

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;

#define N_DEEP_IMAGES 4

#import "lfb"

LFB_DEC(lfb0, vec2);
LFB_DEC(lfb1, vec2);
LFB_DEC(lfb2, vec2);
LFB_DEC(lfb3, vec2);



#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

void main()
{
	fragColor = vec4(1.0);

#if INDEX_BY_TILE
	int lfbPixel = tilesIndex(LFB_GET_SIZE(lfb0), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int lfbPixel = LFB_GET_SIZE(lfb0).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	// Need to read from both lfb's simultaneously and composite from them in order.
	vec2 currLfbFrags[N_DEEP_IMAGES];
	currLfbFrags[0] = vec2(0);
	currLfbFrags[1] = vec2(0);
	currLfbFrags[2] = vec2(0);
	currLfbFrags[3] = vec2(0);

	bool lfbFlags[N_DEEP_IMAGES];
	lfbFlags[0] = false;
	lfbFlags[1] = false;
	lfbFlags[2] = false;
	lfbFlags[3] = false;

	LFB_ITER_BEGIN(lfb0, lfbPixel);
	LFB_ITER_BEGIN(lfb1, lfbPixel);
	LFB_ITER_BEGIN(lfb2, lfbPixel);
	LFB_ITER_BEGIN(lfb3, lfbPixel);

	#define LFB_ITER_GRAB(i) \
		if (!lfbFlags[i] && (LFB_ITER_CHECK(lfb##i))) \
		{ \
			currLfbFrags[i] = LFB_GET_DATA(lfb##i); \
			lfbFlags[i] = true; \
			LFB_ITER_INC(lfb##i); \
		}

	LFB_ITER_GRAB(0);
	LFB_ITER_GRAB(1);
	LFB_ITER_GRAB(2);
	LFB_ITER_GRAB(3);

	int canary = 1000;
	// Continue until there is no more data in the registers.
	while (	lfbFlags[0] ||
			lfbFlags[1] ||
			lfbFlags[2] ||
			lfbFlags[3]
			)
	{
		if (canary-- < 0)
			break;

		// Find the fragment with the largest depth value.
		vec2 f = vec2(0);
		int n = -1;
		if (lfbFlags[0] && currLfbFrags[0].y >= f.y) { f = currLfbFrags[0]; n = 0; }
		if (lfbFlags[1] && currLfbFrags[1].y >= f.y) { f = currLfbFrags[1]; n = 1; }
		if (lfbFlags[2] && currLfbFrags[2].y >= f.y) { f = currLfbFrags[2]; n = 2; }
		if (lfbFlags[3] && currLfbFrags[3].y >= f.y) { f = currLfbFrags[3]; n = 3; }

		if (n == -1)
			break;

		// Reset the register holding that fragment.
		if (n == 0) { lfbFlags[0] = false; currLfbFrags[0] = vec2(0); }
		if (n == 1) { lfbFlags[1] = false; currLfbFrags[1] = vec2(0); }
		if (n == 2) { lfbFlags[2] = false; currLfbFrags[2] = vec2(0); }
		if (n == 3) { lfbFlags[3] = false; currLfbFrags[3] = vec2(0); }

		// Composite the fragment.
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);

		// Only grab data if the register doesn't already have a fragment.
		LFB_ITER_GRAB(0);
		LFB_ITER_GRAB(1);
		LFB_ITER_GRAB(2);
		LFB_ITER_GRAB(3);
	}
#if 0
	#define WRITE_REMAIN(i) \
		while(LFB_ITER_CHECK(lfb##i)) \
		{ \
			vec2 f = LFB_GET_DATA(lfb##i); \
			LFB_ITER_INC(lfb##i); \
			vec4 col = floatToRGBA8(currLfbFrags[i].x); \
			fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a); \
		}

	// Write remaining values if necessary.
	WRITE_REMAIN(0);
	WRITE_REMAIN(1);
	WRITE_REMAIN(2);
	WRITE_REMAIN(3);
#endif
}

