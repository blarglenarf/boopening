#version 430

#define DRAW_LFB_A 1
#define DRAW_LFB_B 1
#define INDEX_BY_TILE 0

// This shader here for geometry/sphere compositing.

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;

#define LFB_A lfbA
#define LFB_B lfbB

#if DRAW_LFB_A || DRAW_LFB_B
	#import "lfb"
#endif

#if DRAW_LFB_A
	LFB_DEC(LFB_A, vec2);
#endif

#if DRAW_LFB_B
	LFB_DEC(LFB_B, vec2);
#endif

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"


// Need this so I can save some screenshots with correct alpha values.
float mixAlpha(float x, float y)
{
	// combined_alpha = 1 - (1 - a_1) * (1 - a_2)
	return 1.0 - (1.0 - x) * (1.0 - y);
}


void main()
{
	//fragColor = vec4(1.0, 1.0, 1.0, 1.0);
	//fragColor = vec4(1.0, 1.0, 1.0, 0.0);
	fragColor = vec4(1.0);

	int c = 0;

#if INDEX_BY_TILE
	#if DRAW_LFB_A
		int lfbAPixel = tilesIndex(LFB_GET_SIZE(LFB_A), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
	#endif
	#if DRAW_LFB_B
		int lfbBPixel = tilesIndex(LFB_GET_SIZE(LFB_B), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
	#endif
#else
	#if DRAW_LFB_A
		int lfbAPixel = LFB_GET_SIZE(LFB_A).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	#endif
	#if DRAW_LFB_B
		int lfbBPixel = LFB_GET_SIZE(LFB_B).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	#endif
#endif

#if DRAW_LFB_A && DRAW_LFB_B
	// Need to read from both lfb's simultaneously and composite from them in order.
	vec2 currLfbFrags[2];
	currLfbFrags[0] = vec2(0);
	currLfbFrags[1] = vec2(0);

	bool lfbFlags[2];
	lfbFlags[0] = false;
	lfbFlags[1] = false;

	LFB_ITER_BEGIN(LFB_A, lfbAPixel);
	LFB_ITER_BEGIN(LFB_B, lfbBPixel);

	#define LFB_ITER_GRAB(lfb, i) \
		if (!lfbFlags[i] && (LFB_ITER_CHECK(lfb))) \
		{ \
			currLfbFrags[i] = LFB_GET_DATA(lfb); \
			lfbFlags[i] = true; \
			LFB_ITER_INC(lfb); \
		}

	LFB_ITER_GRAB(LFB_A, 0);
	LFB_ITER_GRAB(LFB_B, 1);

	int canary = 1000;
	// Continue until there is no more data in the registers.
	while (lfbFlags[0] || lfbFlags[1])
	{
		if (canary-- < 0)
			break;

		// Find the fragment with the largest depth value.
		vec2 f = vec2(0);
		int n = -1;
		if (lfbFlags[0] && currLfbFrags[0].y >= f.y) { f = currLfbFrags[0]; n = 0; }
		if (lfbFlags[1] && currLfbFrags[1].y >= f.y) { f = currLfbFrags[1]; n = 1; }

		if (n == -1)
			break;

		// Reset the register holding that fragment.
		if (n == 0) { lfbFlags[0] = false; currLfbFrags[0] = vec2(0); }
		if (n == 1) { lfbFlags[1] = false; currLfbFrags[1] = vec2(0); }

		// Composite the fragment.
		vec4 col = floatToRGBA8(f.x);
		//col.a = 0.5;
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		//fragColor.a = mixAlpha(fragColor.a, col.a);
		fragColor.a = 1.0;
		c++;

		// Only grab data if the register doesn't already have a fragment.
		LFB_ITER_GRAB(LFB_A, 0);
		LFB_ITER_GRAB(LFB_B, 1);
	}
#if 0
	// Need to read from both lfb's simultaneously and composite from them in order.
	vec2 currLfbAFrag = vec2(0);
	vec2 currLfbBFrag = vec2(0);

	LFB_ITER_BEGIN(LFB_A, lfbAPixel);
	LFB_ITER_BEGIN(LFB_B, lfbBPixel);

	bool lfbAFlag = true, lfbBFlag = true;

	int canary = 1000;
	while ((LFB_ITER_CHECK(LFB_A)) || (LFB_ITER_CHECK(LFB_B)))
	{
		if (canary-- < 0)
			break;
		if (lfbAFlag && (LFB_ITER_CHECK(LFB_A)))
		{
			currLfbAFrag = LFB_GET_DATA(LFB_A);
			lfbAFlag = false;
			LFB_ITER_INC(LFB_A);
		}
		if (lfbBFlag && (LFB_ITER_CHECK(LFB_B)))
		{
			currLfbBFrag = LFB_GET_DATA(LFB_B);
			lfbBFlag = false;
			LFB_ITER_INC(LFB_B);
		}

		vec2 f = vec2(0);
		if (currLfbAFrag.y >= currLfbBFrag.y)
		{
			f = currLfbAFrag;
			currLfbAFrag = vec2(0, 0);
			lfbAFlag = true;
		}
		else
		{
			f = currLfbBFrag;
			currLfbBFrag = vec2(0, 0);
			lfbBFlag = true;
		}
		vec4 col = floatToRGBA8(f.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}

	// Write remaining value if necessary.
	if (!lfbAFlag)
	{
		vec4 col = floatToRGBA8(currLfbAFrag.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
	if (!lfbBFlag)
	{
		vec4 col = floatToRGBA8(currLfbBFrag.x);
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
#endif

//vec4 col = GET_COLOR(val.x); col.rgb *= col.a; blockCol.rgb += col.rgb * blockVis; blockVis *= (1.0 - col.a); blockCol.a = mixAlpha(blockCol.a, col.a); }


#elif DRAW_LFB_A
	// What about doing this in reverse?
	float vis = 1.0;
	for (LFB_ITER_BEGIN(LFB_A, lfbAPixel); LFB_ITER_CHECK(LFB_A); LFB_ITER_INC(LFB_A))
	{
		vec2 f = LFB_GET_DATA(LFB_A);
		vec4 col = floatToRGBA8(f.x);
	#if 0
		col.a = 0.5;
		col.rgb *= col.a;
		fragColor.rgb += col.rgb * vis;
		vis *= (1.0 - col.a);
		fragColor.a = mixAlpha(fragColor.a, col.a);
	#else
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		//fragColor.a = col.a;
		fragColor.a = mixAlpha(fragColor.a, col.a);
		//fragColor.a = 0.9;
	#endif
		c++;
	}
#elif DRAW_LFB_B
	float vis = 1.0;
	for (LFB_ITER_BEGIN(LFB_B, lfbBPixel); LFB_ITER_CHECK(LFB_B); LFB_ITER_INC(LFB_B))
	{
		vec2 f = LFB_GET_DATA(LFB_B);
		vec4 col = floatToRGBA8(f.x);
	#if 0
		col.a = 0.5;
		col.rgb *= col.a;
		fragColor.rgb += col.rgb * vis;
		vis *= (1.0 - col.a);
		fragColor.a = mixAlpha(fragColor.a, col.a);
	#else
		fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		//fragColor.a = col.a;
		fragColor.a = mixAlpha(fragColor.a, col.a);
		//fragColor.a = 0.9;
	#endif
		c++;
	}
#endif

#if 0
	float maxOver = 8;
	if (c > 8) maxOver = 16;
	if (c > 16) maxOver = 32;
	if (c > 32) maxOver = 64;
	if (c > 64) maxOver = 128;
	if (c > 128) maxOver = 256;
	//if (c > 256) maxOver = 512;

	float dc = maxOver / 256.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
	fragColor.a = 1.0;
#endif
}

