#version 430

#define INDEX_BY_TILE 0

// This shader here for geometry/sphere compositing.

// Needed to cull before writing to global memory.
//layout(early_fragment_tests) in;

//out vec4 fragColor;

#import "lfb"

LFB_DEC(lfb0, vec2);
LFB_DEC(lfb1, vec2);

LFB_DEC(lfb, vec2);

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

uniform int reverseOrder;

void main()
{
	//fragColor = vec4(1.0);

#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(lfb), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	// Need to read from both lfb's simultaneously and composite from them in order.
	vec2 currLfbFrags[2];
	currLfbFrags[0] = vec2(0);
	currLfbFrags[1] = vec2(0);

	bool lfbFlags[2];
	lfbFlags[0] = false;
	lfbFlags[1] = false;

	LFB_ITER_BEGIN(lfb0, pixel);
	LFB_ITER_BEGIN(lfb1, pixel);

	#define LFB_ITER_GRAB(lfbidx, i) \
		if (!lfbFlags[i] && (LFB_ITER_CHECK(lfbidx))) \
		{ \
			currLfbFrags[i] = LFB_GET_DATA(lfbidx); \
			lfbFlags[i] = true; \
			LFB_ITER_INC(lfbidx); \
		}

	LFB_ITER_GRAB(lfb0, 0);
	LFB_ITER_GRAB(lfb1, 1);

	int canary = 1000;
	// Continue until there is no more data in the registers.
	while (lfbFlags[0] || lfbFlags[1])
	{
		if (canary-- < 0)
			break;

		// Find the fragment with the largest depth value.
		vec2 f = vec2(0);
		int n = -1;
		if (reverseOrder == 1)
		{
			f = vec2(999999);
			if (lfbFlags[0] && currLfbFrags[0].y < f.y) { f = currLfbFrags[0]; n = 0; }
			if (lfbFlags[1] && currLfbFrags[1].y < f.y) { f = currLfbFrags[1]; n = 1; }
		}
		else
		{
			if (lfbFlags[0] && currLfbFrags[0].y >= f.y) { f = currLfbFrags[0]; n = 0; }
			if (lfbFlags[1] && currLfbFrags[1].y >= f.y) { f = currLfbFrags[1]; n = 1; }
		}

		if (n == -1)
			break;

		// Reset the register holding that fragment.
		if (n == 0) { lfbFlags[0] = false; currLfbFrags[0] = vec2(0); }
		if (n == 1) { lfbFlags[1] = false; currLfbFrags[1] = vec2(0); }

		// Composite the fragment.
		LFB_ADD_DATA(lfb, pixel, f);

		//vec4 col = floatToRGBA8(f.x);
		//fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);

		// Only grab data if the register doesn't already have a fragment.
		LFB_ITER_GRAB(lfb0, 0);
		LFB_ITER_GRAB(lfb1, 1);
	}
}

