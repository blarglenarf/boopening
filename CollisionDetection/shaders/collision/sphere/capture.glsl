#ifndef CAPTURE_SPHERE
#define CAPTURE_SPHERE

// Billboarded quad or sphere?
// Note: Billboarded quads seem to be much faster, even though they result in more pixels covered.
#define USE_QUAD 1
#define MAX_EYE_Z 30.0

layout(points) in;

#if USE_QUAD
	layout(triangle_strip, max_vertices = 4) out;
#else
	layout(triangle_strip, max_vertices = 60) out;
#endif

uniform mat4 pMatrix;

#if USE_QUAD
	in SphereData
	{
		vec4 esVert;
		vec4 csVert;
	#if STORE_ID
		flat uint id;
	#endif
		flat float radius;
	} VertexIn[1];
#else
	in SphereData
	{
		vec4 osVert;
		flat float w;
	#if STORE_ID
		flat uint id;
	#endif
		flat float radius;
	} VertexIn[1];
#endif

out VertexData
{
	flat vec4 esSpherePos;
	flat vec4 csSpherePos;
#if STORE_ID
	flat uint id;
#endif
	flat float radius;
} VertexOut;

#if !USE_QUAD
	uniform mat4 mvMatrix;
#endif


#if USE_QUAD

	#if STORE_ID
		#define VERT(v) \
			esVert = vec4(verts[v] + VertexIn[0].esVert.xyz, 1); \
			VertexOut.esSpherePos = VertexIn[0].esVert; \
			VertexOut.csSpherePos = VertexIn[0].csVert; \
			VertexOut.id = VertexIn[0].id; \
			VertexOut.radius = VertexIn[0].radius; \
			gl_Position = pMatrix * esVert; \
			EmitVertex();
	#else
		#define VERT(v) \
			esVert = vec4(verts[v] + VertexIn[0].esVert.xyz, 1); \
			VertexOut.esSpherePos = VertexIn[0].esVert; \
			VertexOut.csSpherePos = VertexIn[0].csVert; \
			VertexOut.radius = VertexIn[0].radius; \
			gl_Position = pMatrix * esVert; \
			EmitVertex();
	#endif

#else

	#if STORE_ID
		#define TRI(v1, v2, v3) \
			esVert = mvMatrix * vec4(verts[v1] + VertexIn[0].osVert.xyz, 1); \
			if (-esVert.z < 0 && -esVert.z + size >= 0) \
			{ \
				esVert.z = -0.1; \
			} \
			else if (-esVert.z > MAX_EYE_Z && -esVert.z - size <= MAX_EYE_Z) \
			{ \
				esVert.z = -(MAX_EYE_Z - 0.1); \
			} \
			VertexOut.esSpherePos = esSpherePos; \
			VertexOut.csSpherePos = csSpherePos; \
			VertexOut.id = VertexIn[0].id; \
			VertexOut.radius = VertexIn[0].radius; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			esVert = mvMatrix * vec4(verts[v2] + VertexIn[0].osVert.xyz, 1); \
			if (-esVert.z < 0 && -esVert.z + size >= 0) \
			{ \
				esVert.z = -0.1; \
			} \
			else if (-esVert.z > MAX_EYE_Z && -esVert.z - size <= MAX_EYE_Z) \
			{ \
				esVert.z = -(MAX_EYE_Z - 0.1); \
			} \
			VertexOut.esSpherePos = esSpherePos; \
			VertexOut.csSpherePos = csSpherePos; \
			VertexOut.id = VertexIn[0].id; \
			VertexOut.radius = VertexIn[0].radius; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			esVert = mvMatrix * vec4(verts[v3] + VertexIn[0].osVert.xyz, 1); \
			if (-esVert.z < 0 && -esVert.z + size >= 0) \
			{ \
				esVert.z = -0.1; \
			} \
			else if (-esVert.z > MAX_EYE_Z && -esVert.z - size <= MAX_EYE_Z) \
			{ \
				esVert.z = -(MAX_EYE_Z - 0.1); \
			} \
			VertexOut.esSpherePos = esSpherePos; \
			VertexOut.csSpherePos = csSpherePos; \
			VertexOut.id = VertexIn[0].id; \
			VertexOut.radius = VertexIn[0].radius; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			EndPrimitive();
	#else
		#define TRI(v1, v2, v3) \
			esVert = mvMatrix * vec4(verts[v1] + VertexIn[0].osVert.xyz, 1); \
			if (-esVert.z < 0 && -esVert.z + size >= 0) \
			{ \
				esVert.z = -0.1; \
			} \
			else if (-esVert.z > MAX_EYE_Z && -esVert.z - size <= MAX_EYE_Z) \
			{ \
				esVert.z = -(MAX_EYE_Z - 0.1); \
			} \
			VertexOut.esSpherePos = esSpherePos; \
			VertexOut.csSpherePos = csSpherePos; \
			VertexOut.radius = VertexIn[0].radius; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			esVert = mvMatrix * vec4(verts[v2] + VertexIn[0].osVert.xyz, 1); \
			if (-esVert.z < 0 && -esVert.z + size >= 0) \
			{ \
				esVert.z = -0.1; \
			} \
			else if (-esVert.z > MAX_EYE_Z && -esVert.z - size <= MAX_EYE_Z) \
			{ \
				esVert.z = -(MAX_EYE_Z - 0.1); \
			} \
			VertexOut.esSpherePos = esSpherePos; \
			VertexOut.csSpherePos = csSpherePos; \
			VertexOut.radius = VertexIn[0].radius; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			esVert = mvMatrix * vec4(verts[v3] + VertexIn[0].osVert.xyz, 1); \
			if (-esVert.z < 0 && -esVert.z + size >= 0) \
			{ \
				esVert.z = -0.1; \
			} \
			else if (-esVert.z > MAX_EYE_Z && -esVert.z - size <= MAX_EYE_Z) \
			{ \
				esVert.z = -(MAX_EYE_Z - 0.1); \
			} \
			VertexOut.esSpherePos = esSpherePos; \
			VertexOut.csSpherePos = csSpherePos; \
			VertexOut.radius = VertexIn[0].radius; \
			gl_Position = pMatrix * esVert; \
			EmitVertex(); \
			EndPrimitive();
	#endif

#endif

#define PERSPECTIVE_BIAS 1

void main()
{
	// Need to add some leeway to account for perspective.
	float size = VertexIn[0].radius;

#if PERSPECTIVE_BIAS
	#if USE_QUAD
		size *= 2.5;
	#else
		size *= 1.2;
	#endif
#endif

	vec4 esVert;

#if USE_QUAD
	vec3 verts[4];
	verts[0] = vec3(size, -size, 0);
	verts[1] = vec3(-size, -size, 0);
	verts[2] = vec3(-size, size, 0);
	verts[3] = vec3(size, size, 0);

	VERT(1);
	VERT(2);
	VERT(0);
	VERT(3);
	EndPrimitive();
#else
	vec4 esSpherePos = mvMatrix * vec4(VertexIn[0].osVert.xyz, 1.0);
	vec4 csSpherePos = pMatrix * esSpherePos;
	esSpherePos.w = VertexIn[0].w;

	vec3 verts[12];
	verts[0]  = vec3(-size,  size,	 0);
	verts[1]  = vec3( size,  size,	 0);
	verts[2]  = vec3(-size, -size,	 0);
	verts[3]  = vec3( size, -size,	 0);
	verts[4]  = vec3( 0,	-size,	 size);
	verts[5]  = vec3( 0,	 size,	 size);
	verts[6]  = vec3( 0,	-size,	-size);
	verts[7]  = vec3( 0,	 size,	-size);
	verts[8]  = vec3( size,  0,		-size);
	verts[9]  = vec3( size,  0,		 size);
	verts[10] = vec3(-size,  0,		-size);
	verts[11] = vec3(-size,  0,		 size);

	// Create 20 triangles of the icosahedron.

	// 5 faces around point 0.
	TRI(0, 11, 5);
	TRI(0, 5, 1);
	TRI(0, 1, 7);
	TRI(0, 7, 10);
	TRI(0, 10, 11);

	// 5 adjacent faces.
	TRI(1, 5, 9);
	TRI(5, 11, 4);
	TRI(11, 10, 2);
	TRI(10, 7, 6);
	TRI(7, 1, 8);

	// 5 faces around point 3.
	TRI(3, 9, 4);
	TRI(3, 4, 2);
	TRI(3, 2, 6);
	TRI(3, 6, 8);
	TRI(3, 8, 9);

	// 5 adjacent faces.
	TRI(4, 9, 5);
	TRI(2, 4, 11);
	TRI(6, 2, 10);
	TRI(8, 6, 7);
	TRI(9, 8, 1);
#endif
}

#endif

