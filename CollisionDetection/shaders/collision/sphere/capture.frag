#version 430

#define MAX_EYE_Z 30.0
#define INDEX_BY_TILE 0
#define LFB_NAME sphere

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

#import "lfb"

LFB_DEC(LFB_NAME, vec2);

readonly buffer SphereColors
{
	float sphereColors[];
};

in VertexData
{
	flat vec4 esSpherePos;
	flat vec4 csSpherePos;
	flat uint id;
	flat float radius;
} VertexIn;


uniform ivec4 viewport;
uniform mat4 invPMatrix;
uniform mat4 pMatrix;

uniform ivec2 size;

uniform vec3 col;

out vec4 fragColor;



// Store two fragments, one for front and one for back.
void main()
{
	fragColor = vec4(1);

#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	// Sphere screen space position.
	vec2 ssPos = gl_FragCoord.xy;
	vec2 ssSpherePos = vec2(0, 0);

	vec3 esSpherePos = VertexIn.esSpherePos.xyz;
	vec4 csSpherePos = VertexIn.csSpherePos;
	float radius = VertexIn.radius;

	vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -30.0), viewport, invPMatrix).xyz);

	float frontDepth, backDepth;

	if (!sphereIntersection(esSpherePos, radius, dir, frontDepth, backDepth))
	{
		discard;
		return;
	}

	// Front and back sphere fragments.
	vec4 color = floatToRGBA8(sphereColors[VertexIn.id]);
	color.rgb = col;
	color.w = 0.4;
	float c = rgba8ToFloat(color);
	{ LFB_ADD_DATA(LFB_NAME, pixel, vec2(c, frontDepth)); }
	{ LFB_ADD_DATA(LFB_NAME, pixel, vec2(c, backDepth)); }
}

