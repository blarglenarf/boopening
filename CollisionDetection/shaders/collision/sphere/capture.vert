#version 430

// Billboarded quad or sphere?
// Note: Billboarded quads seem to be much faster, even though they result in more pixels covered.
#define USE_QUAD 1

#define MAX_EYE_Z 30.0

layout(location = 0) in vec4 vertex;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform uint indexOffset;

uniform int sphereOffset;

readonly buffer SpherePositions
{
	vec4 spherePositions[];
};

readonly buffer SphereRadii
{
	float sphereRadii[];
};

#if USE_QUAD
	out SphereData
	{
		vec4 esVert;
		vec4 csVert;
		flat uint id;
		flat float radius;
	} VertexOut;
#else
	out SphereData
	{
		vec4 osVert;
		flat float w;
		flat uint id;
		flat float radius;
	} VertexOut;
#endif


void main()
{
	// TODO: This will likely be faster if you use an index buffer instead of gl_VertexID.
	vec4 vertex = spherePositions[gl_VertexID + sphereOffset];
	vec4 osVert = vec4(vertex.xyz, 1.0);

#if USE_QUAD
	vec4 esVert = mvMatrix * osVert;
	VertexOut.esVert = vec4(esVert.xyz, vertex.w);
	VertexOut.csVert = pMatrix * esVert;
#else
	VertexOut.osVert = osVert;
	VertexOut.w = vertex.w;
#endif
	VertexOut.id = uint(gl_VertexID + indexOffset + sphereOffset);
	VertexOut.radius = sphereRadii[gl_VertexID + sphereOffset];


#if USE_QUAD
	// Sphere is outside the frustum range but overlaps the frustum?
	if (-VertexOut.esVert.z < 0 && -VertexOut.esVert.z + VertexOut.radius >= 0)
		VertexOut.esVert.z = -0.1;
	else if (-VertexOut.esVert.z > MAX_EYE_Z && -VertexOut.esVert.z - VertexOut.radius <= MAX_EYE_Z)
		VertexOut.esVert.z = -(MAX_EYE_Z - 0.1);
#endif
}

