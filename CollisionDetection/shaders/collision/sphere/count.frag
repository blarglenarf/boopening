#version 430

#define STORAGE_BUFFERS 1
#define INDEX_BY_TILE 0

#define MAX_EYE_Z 30.0

#include "../../utils.glsl"
#include "../../lfb/tiles.glsl"

#if STORAGE_BUFFERS
	buffer Offsets
	{
		uint offsets[];
	};
#else
	uniform layout(r32ui) coherent uimageBuffer Offsets;
#endif


in VertexData
{
	flat vec4 esSpherePos;
	flat vec4 csSpherePos;
	flat float radius;
} VertexIn;

uniform ivec4 viewport;
uniform mat4 invPMatrix;
uniform mat4 pMatrix;

uniform ivec2 size;

out vec4 fragColor;


// Count two fragments, one for front and one for back.
void main()
{
	fragColor = vec4(1);

	// Sphere screen space position.
	vec2 ssPos = gl_FragCoord.xy;
	vec2 ssSpherePos = vec2(0, 0);

	vec3 esSpherePos = VertexIn.esSpherePos.xyz;
	vec4 csSpherePos = VertexIn.csSpherePos;
	float radius = VertexIn.radius;

	vec3 dir = normalize(getEyeFromWindow(vec3(ssPos, -30.0), viewport, invPMatrix).xyz);

	float frontDepth, backDepth;

	if (!sphereIntersection(esSpherePos, radius, dir, frontDepth, backDepth))
	{
		discard;
		return;
	}

#if INDEX_BY_TILE
	int pixel = tilesIndex(size, ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

#if STORAGE_BUFFERS
	atomicAdd(offsets[pixel], 1);
	atomicAdd(offsets[pixel], 1);
#else
	imageAtomicAdd(Offsets, pixel, 1U);
	imageAtomicAdd(Offsets, pixel, 1U);
#endif
}

