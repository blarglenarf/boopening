#version 430

#define INDEX_BY_TILE 0

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

out vec4 fragColor;

#import "lfb"

LFB_DEC(lfb, vec2);


#include "../utils.glsl"
#include "../lfb/tiles.glsl"

uniform int reverse;

void main()
{
	fragColor = vec4(0.0);

#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(lfb), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	// TODO: What if the lfb is in reverse order?
	float vis = 1.0;

	for (LFB_ITER_BEGIN(lfb, pixel); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		vec2 f = LFB_GET_DATA(lfb);
		vec4 col = floatToRGBA8(f.x);
		if (reverse == 1)
		{
			col.rgb *= col.a;
			fragColor.rgb += col.rgb * vis;
			vis *= (1.0 - col.a);
		}
		else
			fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
	}
}

