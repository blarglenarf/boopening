#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define INDEX_BY_TILE 0

#import "lfb"

#include "../utils.glsl"
#include "../lfb/tiles.glsl"

#define OPAQUE 0
#define LFB_NAME lfb


#if !OPAQUE
	LFB_DEC(LFB_NAME, vec2);
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;


#include "../light/light.glsl"

#if OPAQUE
	out vec4 fragColor;
#endif


void main()
{
#if INDEX_BY_TILE
	int pixel = tilesIndex(LFB_GET_SIZE(LFB_NAME), ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

#if 0
	vec3 normal = normalize(VertexIn.normal);
	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 lightDirN = normalize(viewDir);

	float cosTheta = clamp(dot(normal, lightDirN), 0.0, 1.0);

	//vec3 texColor = texture2DArray(diffuseTex, vec3(VertexIn.texCoord, VertexIn.matIndex)).xyz;

	//vec3 ambient = matAmbient[VertexIn.matIndex].xyz * 0.2;
	vec3 ambient = vec3(0.2, 0.2, 0.2);
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float dp = max(cosTheta, 0.0);

	//if (matTexFlag[VertexIn.matIndex])
	//	ambient = texColor * 0.2;

	if (dp > 0.0)
	{
		//if (!matTexFlag[VertexIn.matIndex])
		//	diffuse = matDiffuse[VertexIn.matIndex].xyz * dp;
		//else
		//	diffuse = texColor * dp;
		diffuse = vec3(0.6, 0.6, 0.6) * dp;
		vec3 reflection = normalize(reflect(-lightDirN, normal));
		float nDotH = max(dot(viewDir, reflection), 0.0);
		//float intensity = pow(nDotH, matShininess[VertexIn.matIndex]);
		float intensity = pow(nDotH, 128.0);
		//specular = matSpecular[VertexIn.matIndex].xyz * intensity;
		specular = vec3(1.0) * intensity;
	}

	vec4 color = vec4(ambient + diffuse + specular, 0.4);
	#if !OPAQUE
		LFB_ADD_DATA(LFB_NAME, gl_FragCoord.xy, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
		discard;
	#else
		fragColor = color;
	#endif
#else
	vec4 color = vec4(0);

	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	color = getAmbient(0.3);
	#if OPAQUE
		if (color.w == 0)
		{
			discard;
			return;
		}
		color.w = 1.0;
	#endif

	color += directionLight(vec4(1.0), viewDir, viewDir, normal);

	#if !OPAQUE
		LFB_ADD_DATA(LFB_NAME, pixel, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z));
		discard;
	#else
		fragColor = color;
	#endif
#endif
}

