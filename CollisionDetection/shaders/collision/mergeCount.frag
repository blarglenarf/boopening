#version 450

// I don't think this extension is supported on my card :(
//#extension GL_ARB_fragment_shader_interlock: enable

#define INDEX_BY_TILE 0

#import "lfb"

LFB_DEC(lfb0, vec2);
LFB_DEC(lfb1, vec2);

#include "../lfb/tiles.glsl"

buffer Offsets
{
	uint offsets[];
};

uniform ivec2 size;

void main()
{
#if INDEX_BY_TILE
	int pixel = tilesIndex(size, ivec2(INDEX_TILE_SIZE), ivec2(gl_FragCoord.xy));
#else
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
#endif

	offsets[pixel] = LFB_COUNT_AT(lfb0, pixel) + LFB_COUNT_AT(lfb1, pixel);
}

