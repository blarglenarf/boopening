#version 430

#define SIZE 4

int data[SIZE];

buffer InputData
{
	int inputData[];
};

uniform int count;

void main()
{
	for (int i = 0; i < SIZE && i < count; i++)
		data[i] = inputData[gl_VertexID * count + i];

	#if 0
	for (int i = 1; i < count; i++)
	{
		vec2 f = data[i];
		float depth = f.y;
		int j = i - 1;
		while (j >= 0 && data[j].y > depth)
		{
			data[j + 1] = data[j];
			j--;
		}
		data[j + 1] = f;
	}
	#endif

	for (int i = 0; i < SIZE && i < count; i++)
		inputData[gl_VertexID * count + i] = data[i];
}

