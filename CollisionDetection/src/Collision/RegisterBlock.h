#pragma once

#include "Collision.h"
#include "../../../Renderer/src/LFB/BMA.h"

class RegisterBlock : public Collision
{
private:
	Rendering::Shader compositeShader;
	Rendering::Shader compositeNShader;
	Rendering::Shader compositeTexShader;
	Rendering::Shader drawTexShader;

	// Iterated binary merging, or just saving deep image result.
	Rendering::Shader mergeCountShader;
	Rendering::Shader mergeShader;
	Rendering::Shader drawImgShader;

	Rendering::BMA mergeBma;

	//Rendering::TextureBuffer compositeTex;
	Rendering::StorageBuffer texBuffer;

	Rendering::LFB mergeLfb;

	// For iterative merging.
	std::vector<Rendering::LFB> mergeLfbs;

private:
	void composite(App *app);
	void sortMerge(App *app);

	void mergePair(App *app, Rendering::LFB *a, Rendering::LFB *b, Rendering::LFB *lfb, int reverseOrder = 0);
	void iteratedMerge(App *app);

public:
	RegisterBlock(int opaqueFlag = 0) : Collision(opaqueFlag) {}
	virtual ~RegisterBlock() {}

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

