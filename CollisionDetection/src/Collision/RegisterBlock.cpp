#include "RegisterBlock.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"

#include <string>

#define DRAW_LFB_A 1
#define DRAW_LFB_B 1

#define INDEX_BY_TILE 1
#define COMPOSITE_TEX 0

#define USE_MASK 0

// Sorts lfbA and merges during sort of lfbB.
#define SORT_MERGE 0

// Composites from multiple source deep images.
#define K_WAY_MERGE 0
#define ITERATED_MERGE 0
#define N_DEEP_IMAGES 2

using namespace Rendering;


void RegisterBlock::composite(App *app)
{
#if COMPOSITE_TEX
	auto &camera = Renderer::instance().getActiveCamera();
	//compositeTex.create(nullptr, GL_RGBA, camera.getWidth(), camera.getHeight());
	//compositeTex.create(nullptr, camera.getWidth() * camera.getHeight() * sizeof(float));
	texBuffer.create(nullptr, camera.getWidth() * camera.getHeight() * sizeof(float));
#endif

#if USE_MASK
	// First create schedule stencil mask.
	app->getProfiler().start("Mask");
	app->createScheduleMask();
	app->getProfiler().time("Mask");
#endif

	app->getProfiler().start("Composite");

#if COMPOSITE_TEX
	auto &shader = compositeTexShader;
#else
	auto &shader = compositeShader;
#endif

	// Composite for light and geometry drawing.
	shader.bind();

#if DRAW_LFB_A
	app->getLfbA().beginComposite();
#endif
#if DRAW_LFB_B
	app->getLfbB().beginComposite();
#endif

#if DRAW_LFB_A
	app->getLfbA().setUniforms(&shader, false);
#endif

#if DRAW_LFB_B
	app->getLfbB().setUniforms(&shader, false);
#endif

#if COMPOSITE_TEX
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_TEXTURE_FETCH_BARRIER_BIT);
	glEnable(GL_RASTERIZER_DISCARD);
	//compositeTex.bind();
	shader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
	//shader.setUniform("compositeTex", &compositeTex);
	shader.setUniform("TexBuffer", &texBuffer);
	glDrawArrays(GL_POINTS, 0, camera.getWidth() * camera.getHeight());
	//compositeTex.unbind();
	glDisable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT | GL_SHADER_IMAGE_ACCESS_BARRIER_BIT | GL_TEXTURE_FETCH_BARRIER_BIT);
#else
	// Full screen quad to spawn a thread per pixel.
	#if !USE_MASK
		Renderer::instance().drawQuad();
	#else
		glEnable(GL_STENCIL_TEST);
		glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
		auto &intervals = app->getIntervals();
		for (int i = (int) intervals.size() - 1; i >= 0; i--)
		{
			glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
			Rendering::Renderer::instance().drawQuad();
		}
		glDisable(GL_STENCIL_TEST);
	#endif
#endif

#if DRAW_LFB_A
	app->getLfbA().endComposite();
#endif
#if DRAW_LFB_B
	app->getLfbB().endComposite();
#endif

	shader.unbind();

#if COMPOSITE_TEX
	// Draw the resulting texture.
	drawTexShader.bind();
	//compositeTex.bind();
	//drawTexShader.setUniform("compositeTex", &compositeTex);
	drawTexShader.setUniform("TexBuffer", &texBuffer);
	drawTexShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
	Renderer::instance().drawQuad();
	//compositeTex.unbind();
	drawTexShader.unbind();
#endif

	app->getProfiler().time("Composite");
}

void RegisterBlock::sortMerge(App *app)
{
	if (opaqueFlag == 1)
		return;

	auto *lfbA = &app->getLfbA();
	auto *lfbB = &app->getLfbB();

	glPushAttrib(GL_POLYGON_BIT | GL_ENABLE_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// First capture and sort lfb A.
	app->captureLfbA();
	app->sortLfbA(false);

	// Second capture and sort lfb B, merging with lfb A.
	app->captureLfbB();

	// TODO: Instead of this step, do our own custom bma sort.
	app->getProfiler().start("B Sort");

	mergeBma.createMask(lfbB);
	//mergeBma.sort(&lfbB);

	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	auto &intervals = mergeBma.getIntervals();
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		app->getProfiler().start("bmaInterval" + Utils::toString(i));
		glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
		intervals[i].shader->bind();
		lfbA->setBmaUniforms(intervals[i].shader);
		lfbB->setBmaUniforms(intervals[i].shader);
		Rendering::Renderer::instance().drawQuad();
		intervals[i].shader->unbind();
		app->getProfiler().time("bmaInterval" + Utils::toString(i));
	}
	glDisable(GL_STENCIL_TEST);

	app->getProfiler().time("B Sort");

	glPopAttrib();
}

void RegisterBlock::mergePair(App *app, Rendering::LFB *a, Rendering::LFB *b, Rendering::LFB *lfb, int reverseOrder)
{
	(void) (app);

	auto &camera = Renderer::instance().getActiveCamera();
	lfb->resize(camera.getWidth(), camera.getHeight());

	if (lfb->getType() != LFB::LINK_LIST)
	{
		// Count the number of frags for capture.
		lfb->beginCountFrags(INDEX_BY_TILE);
		mergeCountShader.bind();
		a->beginComposite();
		b->beginComposite();
		a->setUniforms(&mergeCountShader, false, "lfb0");
		b->setUniforms(&mergeCountShader, false, "lfb1");
		lfb->setCountUniforms(&mergeCountShader);
		Renderer::instance().drawQuad();
		a->endComposite();
		b->endComposite();
		mergeCountShader.unbind();
		lfb->endCountFrags();
	}

	lfb->beginCapture();

	mergeShader.bind();

	lfb->setUniforms(&mergeShader, true, "lfb");

	mergeShader.setUniform("reverseOrder", reverseOrder);

	a->beginComposite();
	b->beginComposite();

	a->setUniforms(&mergeShader, false, "lfb0");
	b->setUniforms(&mergeShader, false, "lfb1");

	Renderer::instance().drawQuad();

	a->endComposite();
	b->endComposite();

	mergeShader.unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (lfb->endCapture())
	{
		lfb->beginCapture();
		mergeShader.bind();
		lfb->setUniforms(&mergeShader, true, "lfb");
		mergeShader.setUniform("reverseOrder", reverseOrder);
		a->beginComposite();
		b->beginComposite();
		a->setUniforms(&mergeShader, false);
		b->setUniforms(&mergeShader, false);
		Renderer::instance().drawQuad();
		a->endComposite();
		b->endComposite();
		mergeShader.unbind();

		if (lfb->endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}
}

void RegisterBlock::iteratedMerge(App *app)
{
	if (opaqueFlag == 1)
		return;

	app->getProfiler().start("Composite");

	int reverseComposite = 0;

#if N_DEEP_IMAGES == 4
	// First merge all the original deep images pairwise.
	int result = 0;
	auto &lfbs = app->getLfbs();
	for (int i = 0; i < N_DEEP_IMAGES; i += 2)
		mergePair(app, &lfbs[i], &lfbs[i + 1], &mergeLfbs[result++]);

	if (mergeLfb.getType() == LFB::LINK_LIST)
		mergePair(app, &mergeLfbs[0], &mergeLfbs[1], &mergeLfb, 1);
	else
		mergePair(app, &mergeLfbs[0], &mergeLfbs[1], &mergeLfb, 0);
#else
	// FIXME: Spheres are not in reverse order!
	if (mergeLfb.getType() == LFB::LINK_LIST)
	{
		mergePair(app, &app->getLfbA(), &app->getLfbB(), &mergeLfb, 0);
		reverseComposite = 1;
	}
	else
		mergePair(app, &app->getLfbA(), &app->getLfbB(), &mergeLfb, 0);
#endif

	app->getProfiler().time("Composite");

	// Render the resulting image to make sure it's correct.
	auto *lfb = &mergeLfb;
	drawImgShader.bind();
	drawImgShader.setUniform("reverse", reverseComposite);
	lfb->beginComposite();
	lfb->setUniforms(&drawImgShader, false, "lfb");
	Renderer::instance().drawQuad();
	lfb->endComposite();
	drawImgShader.unbind();
}


void RegisterBlock::render(App *app)
{
#if SORT_MERGE
	sortMerge(app);
#elif K_WAY_MERGE
	// TODO: Implement me!
#elif ITERATED_MERGE
	glPushAttrib(GL_POLYGON_BIT | GL_ENABLE_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	#if N_DEEP_IMAGES == 4
		app->captureNLfbs();
		app->sortNLfbs();
	#else
		#if DRAW_LFB_A
			app->captureLfbA();
			app->sortLfbA(false);
		#endif
		#if DRAW_LFB_B
			app->captureLfbB();
			app->sortLfbB(false);
		#endif
	#endif

	iteratedMerge(app);

	glPopAttrib();
#else
	if (opaqueFlag == 1)
	{
		// FIXME: Can't really do anything here, it's not supposed to be opaque...
		return;

		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		// Capture geometry and sort.
		app->captureLfbA();
		app->sortLfbA();

		glPopAttrib();
	}
	else
	{
		glPushAttrib(GL_POLYGON_BIT | GL_ENABLE_BIT);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	#if DRAW_LFB_A
		app->captureLfbA();
		app->sortLfbA(false);
	#endif

	#if DRAW_LFB_B
		app->captureLfbB();
		app->sortLfbB(false);
	#endif

		// Lastly, composite.
		composite(app);

		glPopAttrib();
	}
#endif
}

void RegisterBlock::update(float dt)
{
	(void) (dt);
}

void RegisterBlock::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
	#if ITERATED_MERGE
		mergeLfbs.resize(N_DEEP_IMAGES);
		for (size_t i = 0; i < mergeLfbs.size(); i++)
		{
			mergeLfbs[i].release();
			mergeLfbs[i].setType(type, "mergeLfb" + Utils::toString(i));
			mergeLfbs[i].setMaxFrags(512);
		}	
	#endif
		mergeLfb.release();
		mergeLfb.setType(type, "lfb");
		mergeLfb.setMaxFrags(512);

	#if ITERATED_MERGE
		auto vertCount = ShaderSourceCache::getShader("mergeCountVert").loadFromFile("shaders/collision/mergeCount.vert");
		auto fragCount = ShaderSourceCache::getShader("mergeCountFrag").loadFromFile("shaders/collision/mergeCount.frag");
		fragCount.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));

		mergeCountShader.release();
		mergeCountShader.create(&vertCount, &fragCount);

		auto vertMerge = ShaderSourceCache::getShader("mergeVert").loadFromFile("shaders/collision/registerBlock/merge.vert");
		auto fragMerge = ShaderSourceCache::getShader("mergeFrag").loadFromFile("shaders/collision/registerBlock/merge.frag");
		fragMerge.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));

		mergeShader.release();
		mergeShader.create(&vertMerge, &fragMerge);
	#endif

		auto vertDraw = ShaderSourceCache::getShader("drawVert").loadFromFile("shaders/collision/drawImg.vert");
		auto fragDraw = ShaderSourceCache::getShader("drawFrag").loadFromFile("shaders/collision/drawImg.frag");
		fragDraw.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));

		drawImgShader.release();
		drawImgShader.create(&vertDraw, &fragDraw);

		auto vertComp = ShaderSourceCache::getShader("compRegVert").loadFromFile("shaders/collision/registerBlock/composite.vert");
		auto fragComp = ShaderSourceCache::getShader("compRegFrag").loadFromFile("shaders/collision/registerBlock/composite.frag");
		fragComp.setDefine("DRAW_LFB_A", Utils::toString(DRAW_LFB_A));
		fragComp.setDefine("DRAW_LFB_B", Utils::toString(DRAW_LFB_B));
		fragComp.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
		fragComp.setDefine("LFB_A", "lfbA");
		fragComp.setDefine("LFB_B", "lfbB");

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp);

		auto vertTexComp = ShaderSourceCache::getShader("compRegTexVert").loadFromFile("shaders/collision/registerBlock/compositeTex.vert");
		vertTexComp.setDefine("DRAW_LFB_A", Utils::toString(DRAW_LFB_A));
		vertTexComp.setDefine("DRAW_LFB_B", Utils::toString(DRAW_LFB_B));
		//fragComp.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
		vertTexComp.setDefine("LFB_A", "lfbA");
		vertTexComp.setDefine("LFB_B", "lfbB");

		compositeTexShader.release();
		compositeTexShader.create(&vertTexComp);

		auto vertTexDraw = ShaderSourceCache::getShader("vertTexDraw").loadFromFile("shaders/collision/registerBlock/drawTex.vert");
		auto fragTexDraw = ShaderSourceCache::getShader("fragTexDraw").loadFromFile("shaders/collision/registerBlock/drawTex.frag");

		drawTexShader.release();
		drawTexShader.create(&vertTexDraw, &fragTexDraw);

		// BMA shaders that merge during sort.
		mergeBma.createMaskShader("lfbB", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
		mergeBma.createShaders(&app->getLfbB(), "shaders/collision/bruteForce/sort/sortMerge.vert", "shaders/collision/bruteForce/sort/sortMerge.frag",
			{{"COMPOSITE_LOCAL", "1"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});

		auto str = compositeShader.getBinary();
		Utils::File f("compositeShader.txt");
		f.write(str);
	}
}

void RegisterBlock::saveLFB(App *app)
{
	app->saveLfbA();
}

