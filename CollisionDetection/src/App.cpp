#include "App.h"

#include "Collision/Collision.h"
#include "Collision/BruteForce.h"
#include "Collision/RegisterBlock.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <iomanip>


// DONE: We already have merge+composite of 2 deep images, keep that.
// DONE: Compare just merging 2 deep images, saving the resulting deep images.
// DONE: Do this for hairball+spheres as well.
// DONE: Then compare iterated merging of 2 sets of 2 deep images.
// DONE: Make sure that you can benchmark all this.
// DONE: Do the benchmarks.
// TODO: Add new results to paper.
// TODO: Talk about the new techniques, and the new results.
// TODO: Screenshots of depth complexities for each view.
// TODO: Double-check that reviewer comments have been addressed.


using namespace Math;
using namespace Rendering;

// TODO: Get rid of this!
std::string currColStr;
std::string currLfbStr;
Flythrough anim;
Font testFont;
float tpf = 0;
float smoothMerge = 0;


#define MAX_FRAGS 512
#define INIT_SPHERES 2048
#define MIXED_SPHERES 0
#define MEDIUM_SPHERES 1
#define LARGE_SPHERES 0
#define SMALL_SPHERES 0
#define ANIM_SPHERES 0
#define SPHERE_SPEED 1.0f
#define COLLISION_TIME 0.5f
#define INDEX_BY_TILE 1
#define PRINT_TIMES 1

#define MERGE_MESHES 1
#define MERGE_MESH_SPHERE 0
#define MERGE_SPHERES 0

// Composites from multiple source deep images.
#define N_DEEP_IMAGES 0


static vec3 genPos(const vec3 &min, const vec3 &max)
{
	float x = Utils::getRand(min.x, max.x);
	float y = Utils::getRand(min.y, max.y);
	float z = Utils::getRand(min.z, max.z);
	return vec3(x, y, z);
}

static vec3 genColor()
{
#if 1
	float r = Utils::getRand(0.0f, 1.0f);
	float g = Utils::getRand(0.0f, 1.0f);
	float b = Utils::getRand(0.0f, 1.0f);
	return vec3(r, g, b);
#else
	return vec3(1.0f, 0.0f, 0.0f);
#endif
}

static float genRadius()
{
	// 5% probability > 1.0.
	// 10% probability > 0.5.
	float p = Utils::getRand(0.0f, 100.0f);
#if MIXED_SPHERES
	if (p <= 5)
		return Utils::getRand(2.0f, 4.0f);
	if (p <= 15)
		return Utils::getRand(1.0f, 2.0f);
	else
		return Utils::getRand(0.2f, 1.0f);
#elif MEDIUM_SPHERES
	if (p <= 5)
		return Utils::getRand(1.5f, 2.0f);
	if (p <= 15)
		return Utils::getRand(1.0f, 1.5f);
	else
		return Utils::getRand(0.5f, 1.0f);
#elif LARGE_SPHERES
	if (p <= 5)
		return Utils::getRand(8.0f, 10.0f);
	if (p <= 15)
		return Utils::getRand(6.0f, 8.0f);
	else
		return Utils::getRand(4.0f, 6.0f);
#elif SMALL_SPHERES
	if (p <= 5)
		return Utils::getRand(0.3f, 0.5f);
	if (p <= 15)
		return Utils::getRand(0.2f, 0.3f);
	else
		return Utils::getRand(0.1f, 0.2f);
#endif
}

static vec3 getRandomDir()
{
	vec3 dir(Utils::getRand(-1.0f, 1.0f), Utils::getRand(-1.0f, 1.0f), Utils::getRand(-1.0f, 1.0f));
	dir.normalize();
	return dir;
}

static void collideSphere(vec4 &pos, vec4 &dir, float &colTime, float dt)
{
	colTime -= dt;
	if (colTime < 0)
		colTime = 0;
	if (colTime > 0)
		return;

	pos.x = clamp(pos.x, -10.0f, 10.0f);
	pos.y = clamp(pos.y, -5.0f, 5.0f);
	pos.z = clamp(pos.z, -10.0f, 10.0f);

	// FIXME: For now this just assumes we're using the atrium.
	// Which edge did we hit?
	if (pos.x <= -10)
	{
		dir.xyz.reflect(vec3(1, 0, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.x >= 10)
	{
		dir.xyz.reflect(vec3(-1, 0, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.y <= -5)
	{
		dir.xyz.reflect(vec3(0, 1, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.y >= 5)
	{
		dir.xyz.reflect(vec3(0, -1, 0));
		colTime = COLLISION_TIME;
	}

	else if (pos.z <= -10)
	{
		dir.xyz.reflect(vec3(0, 0, 1));
		colTime = COLLISION_TIME;
	}

	else if(pos.z >= 10)
	{
		dir.xyz.reflect(vec3(0, 0, -1));
		colTime = COLLISION_TIME;
	}
}



void App::createSpheres(size_t nSpheres)
{
	sphereRadii.resize(nSpheres);
	spherePositions.resize(nSpheres);
	sphereColors.resize(nSpheres);

	Utils::seedRand(0);

	vec3 min(-10, -5, -10), max(10, 5, 10);

	for (size_t i = 0; i < nSpheres; i++)
	{
		// ID or type could be used in the fourth value, even the radius if you're only using spheres.
		sphereRadii[i] = genRadius();
		spherePositions[i] = vec4(genPos(min, max), 0.0);
	}

	for (size_t i = 0; i < nSpheres; i++)
		sphereColors[i] = Utils::rgba8ToFloat(vec4(genColor(), 1.0f));

	if (sphereVectors.size() != sphereColors.size())
	{
		sphereVectors.resize(sphereColors.size());
		sphereCollisions.resize(sphereColors.size(), 0.0f);
	}
}

void App::createSphereBuffers()
{
	spherePosBuffer.release();
	sphereRadiusBuffer.release();
	sphereColorBuffer.release();
	sphereVelBuffer.release();

	if (!spherePositions.empty())
	{
		spherePosBuffer.create(&spherePositions[0], spherePositions.size() * sizeof(vec4));
		sphereRadiusBuffer.create(&sphereRadii[0], sphereRadii.size() * sizeof(float));
	}

	// Note: calling this without actually creating a vbo that uses it causes glDrawArrays (without any bound buffers) to
	// fail when drawing 256 points on my windows machine, but not at uni, and not when drawing > 256 points. No idea why.
	// glEnableVertexAttribArray(0);

	if (!sphereColors.empty())
		sphereColorBuffer.create(&sphereColors[0], sphereColors.size() * sizeof(float));
	if (!sphereVectors.empty())
		sphereVelBuffer.create(&sphereVectors[0], sphereVectors.size() * sizeof(vec4));
}

void App::setSphereVelocities(float speed)
{
	sphereVectors.resize(spherePositions.size());
	sphereCollisions.resize(sphereVectors.size(), 0.0f);

	for (size_t i = 0; i < spherePositions.size(); i++)
		sphereVectors[i] = vec4(getRandomDir() * speed, 0.0f);
}

void App::updateSpherePositions(float dt)
{
	for (size_t i = 0; i < spherePositions.size(); i++)
	{
		spherePositions[i].xyz += sphereVectors[i].xyz * dt;
		collideSphere(spherePositions[i], sphereVectors[i], sphereCollisions[i], dt);
	}

	if (spherePosBuffer.isGenerated())
		spherePosBuffer.create(&spherePositions[0], spherePositions.size() * sizeof(vec4));

	if (sphereVelBuffer.isGenerated())
		sphereVelBuffer.create(&sphereVectors[0], sphereVectors.size() * sizeof(vec4));
}



void App::init()
{
	auto &interpolatorA = anim.getInterpolator(anim.addInterpolator(&lfbAPos, 25, vec3(8.0f, 0.0f, 0.0f)));
	interpolatorA.addKeyFrame(0, vec3(8.0f, 0.0f, 0.0f));
	interpolatorA.addKeyFrame(20, vec3(0.0f, 0.0f, 0.0f));
	interpolatorA.addKeyFrame(25, vec3(0.0f, 0.0f, 0.0f));

	auto &interpolatorB = anim.getInterpolator(anim.addInterpolator(&lfbBPos, 25, vec3(-8.0f, 0.0f, 0.0f)));
	interpolatorB.addKeyFrame(0, vec3(-8.0f, 0.0f, 0.0f));
	interpolatorB.addKeyFrame(20, vec3(0.0f, 0.0f, 0.0f));
	interpolatorA.addKeyFrame(25, vec3(0.0f, 0.0f, 0.0f));

#if 0
	// Powerplant view.	
	auto &camera = Renderer::instance().getActiveCamera();
	camera.setPos({7.13556f,-4.95272f,-3.71761f});
	camera.setEulerRot({-0.061086f,1.83259f,0});
	camera.setZoom(5.09826f);
	camera.update(0.1f);
#endif
#if 0
	// Hairball view.
	auto &camera = Renderer::instance().getActiveCamera();
	camera.setPos({-0.400156f,-0.300304f,-0.0183215f});
	camera.setEulerRot({-0.157078f,0.0261799f,0});
	camera.setZoom(15);
	camera.update(0.1f);
#endif

#if 0
	anim.setAnimFilename("animations/anim.jpg");
#endif

	// TODO: Get rid of this!
	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");

	int maxBuffers = 0;
	glGetIntegerv(GL_MAX_SHADER_STORAGE_BUFFER_BINDINGS, &maxBuffers);
	std::cout << "Max storage buffers: " << maxBuffers << "\n";
	
	createSpheres(INIT_SPHERES);
	createSphereBuffers();
	setSphereVelocities(SPHERE_SPEED);

	loadNextMesh();
	useCollision(Collision::BRUTE_FORCE);
	//useLFB();

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glColor3f(1, 1, 1);

	auto vertBasic = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto geomBasic = ShaderSourceCache::getShader("basicGeom").loadFromFile("shaders/basic.geom");
	auto fragBasic = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.create(&vertBasic, &fragBasic, &geomBasic);

	//std::cout << basicShader.getBinary() << "\n";

	// Special shader for counting sphere fragments
	auto vertCount = ShaderSourceCache::getShader("sphereCountVert").loadFromFile("shaders/collision/sphere/count.vert");
	auto geomCount = ShaderSourceCache::getShader("sphereCountGeom").loadFromFile("shaders/collision/sphere/count.geom");
	auto fragCount = ShaderSourceCache::getShader("sphereCountFrag").loadFromFile("shaders/collision/sphere/count.frag");
	fragCount.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	countSphereShader.create(&vertCount, &fragCount, &geomCount);
}

void App::setTmpViewport(int width, int height)
{
	auto &camera = Renderer::instance().getActiveCamera();

	tmpWidth = camera.getWidth();
	tmpHeight = camera.getHeight();

	camera.setViewport(0, 0, width, height);
	camera.uploadViewport();
	camera.update(0);
}

void App::restoreViewport()
{
	auto &camera = Renderer::instance().getActiveCamera();

	camera.setViewport(0, 0, tmpWidth, tmpHeight);
	camera.uploadViewport();
	camera.update(0);
}

void App::setCameraUniforms(Shader *shader)
{
	auto &camera = Renderer::instance().getActiveCamera();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	shader->setUniform("viewport", camera.getViewport());
	shader->setUniform("invPMatrix", camera.getInverseProj());
}

void App::captureLfbA()
{
#if MERGE_MESHES
	captureGeometry(&captureShaderA, &lfbA, &gpuMeshA);
#endif
#if MERGE_MESH_SPHERE
	captureGeometry(&captureShaderA, &lfbA, &gpuMeshA);
#endif
#if MERGE_SPHERES
	captureSpheres(&captureShaderA, &lfbA, vec3(0, 0, 1), 0);
#endif
}

void App::captureLfbB()
{
#if MERGE_MESHES
	captureGeometry(&captureShaderB, &lfbB, &gpuMeshB);
#endif
#if MERGE_MESH_SPHERE
	captureSpheres(&captureShaderB, &lfbB, vec3(0, 0, 1), (int) (spherePositions.size() / 2));
#endif
#if MERGE_SPHERES
	captureSpheres(&captureShaderB, &lfbB, vec3(1, 0, 0), (int) (spherePositions.size() / 2));
#endif
}

void App::captureGeometry(Shader *shader, Rendering::LFB *lfb, Rendering::GPUMesh *gpuMesh, Math::vec3 col)
{
	(void) (col);
	if (!shader)
		shader = &captureShaderA;
	if (!lfb)
		lfb = &lfbA;
	if (!gpuMesh)
		gpuMesh = &gpuMeshA;

	profiler.start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	if (opaqueFlag == 0)
		lfb->resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && lfb->getType() != LFB::LINK_LIST)
	{
		// Count the number of frags for capture.
		auto &countShader = lfb->beginCountFrags(INDEX_BY_TILE);
		countShader.bind();
		lfb->setCountUniforms(&countShader);
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh->render(false);
		countShader.unbind();
		lfb->endCountFrags();
	}

	if (opaqueFlag == 0)
		lfb->beginCapture();

	shader->bind();

//#if USE_MESH_B
//	shader->setUniform("col", col);
//#endif
	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	if (opaqueFlag == 0)
		lfb->setUniforms(shader);

	// Capture fragments into the lfb.
	gpuMesh->render();
	shader->unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && lfb->endCapture())
	{
		lfb->beginCapture();
		shader->bind();
	//#if USE_MESH_B
	//	shader->setUniform("col", col);
	//#endif
		shader->setUniform("mvMatrix", camera.getInverse());
		shader->setUniform("pMatrix", camera.getProjection());
		shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		lfb->setUniforms(shader);
		gpuMesh->render();
		shader->unbind();

		if (lfb->endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	profiler.time("Geometry Capture");
}

void App::sortLfbA(bool composite)
{
	if (opaqueFlag == 1)
		return;

	profiler.start("A Sort");

	// Sort using bma+rbs, and write the result back into the lfb (or composite locally).
	if (composite)
	{
		localBmaA.createMask(&lfbA);
		localBmaA.sort(&lfbA);
	}
	else
	{
		bmaA.createMask(&lfbA);
		bmaA.sort(&lfbA);
	}

	profiler.time("A Sort");
}

void App::captureSpheres(Rendering::Shader *shader, Rendering::LFB *lfb, Math::vec3 col, int sphereOffset)
{
	if (!shader)
		shader = &captureShaderB;
	if (!lfb)
		lfb = &lfbB;

	profiler.start("Sphere Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	if (opaqueFlag == 0)
		lfb->resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && lfb->getType() != LFB::LINK_LIST)
	{
		// Count the number of frags for capture.
		lfb->beginCountFrags(INDEX_BY_TILE);
		countSphereShader.bind();
		setCameraUniforms(&countSphereShader);
		countSphereShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
		lfb->setCountUniforms(&countSphereShader);
		countSphereShader.setUniform("sphereOffset", sphereOffset);
		countSphereShader.setUniform("SpherePositions", &spherePosBuffer);
		countSphereShader.setUniform("SphereRadii", &sphereRadiusBuffer);
		glDrawArrays(GL_POINTS, 0, (GLsizei) spherePositions.size() / 2);
		countSphereShader.unbind();
		lfb->endCountFrags();
	}

	if (opaqueFlag == 0)
		lfb->beginCapture();

	shader->bind();

	setCameraUniforms(shader);
	shader->setUniform("col", col);
	shader->setUniform("sphereOffset", sphereOffset);
	shader->setUniform("SpherePositions", &spherePosBuffer);
	shader->setUniform("SphereRadii", &sphereRadiusBuffer);
	shader->setUniform("SphereColors", &sphereColorBuffer);

	if (opaqueFlag == 0)
		lfb->setUniforms(shader);

	// Capture fragments into the lfb.
	glDrawArrays(GL_POINTS, 0, (GLsizei) spherePositions.size() / 2);
	shader->unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && lfb->endCapture())
	{
		lfb->beginCapture();
		shader->bind();
		setCameraUniforms(shader);
		shader->setUniform("col", col);
		shader->setUniform("sphereOffset", sphereOffset);
		shader->setUniform("SpherePositions", &spherePosBuffer);
		shader->setUniform("SphereRadii", &sphereRadiusBuffer);
		shader->setUniform("SphereColors", &sphereColorBuffer);
		lfb->setUniforms(shader);
		glDrawArrays(GL_POINTS, 0, (GLsizei) spherePositions.size() / 2);
		shader->unbind();

		if (lfb->endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	profiler.time("Sphere Capture");
}

void App::sortLfbB(bool composite)
{
	if (opaqueFlag == 1)
		return;

	profiler.start("B Sort");

	// Sort using bma+rbs, and write the result back into the lfb (or composite locally).
	if (composite)
	{
		localBmaB.createMask(&lfbB);
		localBmaB.sort(&lfbB);
	}
	else
	{
		bmaB.createMask(&lfbB);
		bmaB.sort(&lfbB);
	}

	profiler.time("B Sort");
}

void App::captureNLfbs()
{
	for (int i = 0; i < N_DEEP_IMAGES; i++)
		captureGeometry(captureShaders[i], &lfbs[i], gpuMeshes[i]);
}

void App::sortNLfbs()
{
	for (int i = 0; i < N_DEEP_IMAGES; i++)
	{
		profiler.start(Utils::toString(i) + " Sort");

		bmas[i].createMask(&lfbs[i]);
		bmas[i].sort(&lfbs[i]);

		profiler.time(Utils::toString(i) + " Sort");
	}
}

void App::createScheduleIntervals()
{
	auto scheduleVert = ShaderSourceCache::getShader("scheduleVert").loadFromFile("shaders/collision/schedule/createMask.vert");
	auto scheduleFrag = ShaderSourceCache::getShader("scheduleFrag").loadFromFile("shaders/collision/schedule/createMask.frag");
	scheduleFrag.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	scheduleFrag.setDefine("LFB_NAME_A", "lfbA");
	scheduleFrag.setDefine("LFB_NAME_B", "lfbB");
	scheduleShader.release();
	scheduleShader.create(&scheduleVert, &scheduleFrag);

	int maxBmaInterval = 1 << Utils::ceilLog2(MAX_FRAGS);
	int prev = 0;

	intervals.clear();

	std::vector<int> splits;
	for (int interval = 8; interval <= maxBmaInterval; interval *= 2)
		splits.push_back(interval);
	std::sort(splits.begin(), splits.end());

	intervals.resize(splits.size());

	for (size_t i = 0; i < splits.size(); i++)
	{
		int intervalEnd = splits[i];
		intervals[i].start = prev;
		intervals[i].end = intervalEnd;
	#if 0
		intervals[i].shader = new Shader();
		auto &bmaVert = ShaderSourceCache::getShader("bmaVert" + Utils::toString(intervalEnd) + lfb->getName()).loadFromFile(vert);
		auto &bmaFrag = ShaderSourceCache::getShader("bmaFrag" + Utils::toString(intervalEnd) + lfb->getName()).loadFromFile(frag);
		bmaFrag.setDefine("MAX_FRAGS", Utils::toString(lfb->getMaxFrags()));
		bmaFrag.setDefine("MAX_FRAGS_OVERRIDE", Utils::toString(intervalEnd));
		for (auto &fragDefine : fragDefines)
			bmaFrag.setDefine(fragDefine.tag, fragDefine.value);
		bmaFrag.replace("LFB_NAME", lfb->getName());
		intervals[i].shader->create(&bmaVert, &bmaFrag);
	#endif
		prev = intervalEnd;
	}
}

void App::createScheduleMask()
{
#if 1
	glClear(GL_STENCIL_BUFFER_BIT);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);
#endif

	scheduleShader.bind();
	lfbA.setBmaUniforms(&scheduleShader);
	lfbB.setBmaUniforms(&scheduleShader);
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		scheduleShader.setUniform("interval", intervals[i].start);
		glStencilFunc(GL_GREATER, 1<<i, 0xFF);
		Rendering::Renderer::instance().drawQuad();
	}
	scheduleShader.unbind();

#if 1
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDisable(GL_STENCIL_TEST);
#endif
}

void App::render()
{
	static float fpsUpdate = 0;

	glPointSize(3);

	profiler.begin();

	profiler.start("Total");

	if (currCol)
	{
		currCol->render(this);
	}
	else
	{
		auto &camera = Renderer::instance().getActiveCamera();

		// Opaque Rendering.
		glPushAttrib(GL_ENABLE_BIT);
		//glEnable(GL_DEPTH_TEST);
		//glEnable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);

		basicShader.bind();
		basicShader.setUniform("mvMatrix", camera.getInverse());
		basicShader.setUniform("pMatrix", camera.getProjection());
		basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		basicShader.setUniform("lightDir", vec3(0, 0, 1));
		gpuMeshA.render();
		basicShader.unbind();

		glPopAttrib();
	}

	profiler.time("Total");

#if PRINT_TIMES
	if (hudFlag)
	{
		float total = profiler.getTime("Total");
		float smoothing = 0.9f; // Larger => more smoothing.
		tpf = (tpf * smoothing) + (total * (1.0f - smoothing));
		float t = round(total);

		float mergeTime = profiler.getTime("Composite");
		smoothMerge = (mergeTime * smoothing) + (mergeTime * (1.0f - smoothing));

		testFont.renderFromAtlas("Technique: " + currColStr +
			"\nLFB Type: " + currLfbStr +
			"\nMerge time (ms): " + Utils::toString(mergeTime) +
			"\nTime per frame (ms): " + Utils::toString(t) +
			"\nFrames per second (fps): " + Utils::toString(1000.0 / t), 10, 10, 0, 1, 0);
	}
#endif

	float dt = Platform::Window::getDeltaTime();
	fpsUpdate += dt;
	if (fpsUpdate > 2)
	{
		profiler.print();
		fpsUpdate = 0;
	}


	if (axisFlag && hudFlag)
	{
		Renderer::instance().drawAxes();
		Renderer::instance().drawAxes(Renderer::instance().getActiveCamera().getPos(), 2);
	}
}

void App::update(float dt)
{
	currCol->update(dt);

	if (anim.isAnimating())
	{
		anim.update(dt);
		Renderer::instance().getActiveCamera().setDirty();
	}

#if ANIM_SPHERES
	// Animate the spheres as a particle system, have them bounce around when they hit the bounds.
	if (anim.isAnimating() && dt > 0)
		updateSpherePositions(dt);
	else
		updateSpherePositions(std::min(0.2f, dt));
#endif
}

void App::runBenchmarks()
{
	Benchmark benchmark(&profiler, [this]() -> void { this->render(); } );

	// Set camera view for benchmark.
	auto &camera = Renderer::instance().getActiveCamera();

	std::vector<std::string> times = {"Composite"};
	std::vector<std::string> data = {"lfbATotal", "lfbBTotal"};

	float testTime = 10;

#if MERGE_MESHES
	// Note: Assumes Atrium mesh is currently loaded, format is linked list and technique is stepwise.

	// Atrium Tests.
	// Atrium LL-S.
	benchmark.addTest("Atrium-LL-S",
		[&camera]() -> void
		{
			camera.setPos({-0.5f, -0.8f, 0.0f});
			camera.setEulerRot({-0.209439f,0,0});
			camera.setZoom(6.68847f);
			camera.update(0.1f);
		}, testTime, times, data);

	// Atrium LL-RB.
	benchmark.addTest("Atrium-LL-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Atrium I-S.
	benchmark.addTest("Atrium-I-S",
		[&camera, this]() -> void
		{
			this->cycleCollision();
			this->useNextLFB();
		}, testTime, times, data);

	// Atrium I-RB.
	benchmark.addTest("Atrium-I-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Atrium L-S.
	benchmark.addTest("Atrium-L-S",
		[&camera, this]() -> void
		{
			this->cycleCollision();
			this->useNextLFB();
		}, testTime, times, data);

	// Atrium L-RB.
	benchmark.addTest("Atrium-L-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Powerplant tests.
	// Power LL-S.
	benchmark.addTest("Power-LL-S",
		[&camera, this]() -> void
		{
			camera.setPos({7.13556f,-4.95272f,-3.71761f});
			camera.setEulerRot({-0.061086f,1.83259f,0});
			camera.setZoom(5.09826f);
			camera.update(0.1f);
			this->cycleCollision();
			this->useNextLFB();
			this->loadNextMesh();
		}, testTime, times, data);

	// Power LL-RB.
	benchmark.addTest("Power-LL-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Power I-S.
	benchmark.addTest("Power-I-S",
		[&camera, this]() -> void
		{
			this->cycleCollision();
			this->useNextLFB();
		}, testTime, times, data);

	// Power I-RB.
	benchmark.addTest("Power-I-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Power L-S.
	benchmark.addTest("Power-L-S",
		[&camera, this]() -> void
		{
			this->cycleCollision();
			this->useNextLFB();
		}, testTime, times, data);

	// Power L-RB.
	benchmark.addTest("Power-L-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);
#endif
#if MERGE_MESH_SPHERE
	// Note: Assumes Hairball mesh is currently loaded, format is linked list and technique is stepwise.

	// Hairball tests.
	// Hairball LL-S.
	benchmark.addTest("Hairball-LL-S",
		[&camera]() -> void
		{
			camera.setPos({-0.400156f,-0.300304f,-0.0183215f});
			camera.setEulerRot({-0.157078f,0.0261799f,0});
			camera.setZoom(15);
			camera.update(0.1f);
		}, testTime, times, data);

	// Hairball LL-RB.
	benchmark.addTest("Hairball-LL-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Hairball I-S.
	benchmark.addTest("Hairball-I-S",
		[&camera, this]() -> void
		{
			this->cycleCollision();
			this->useNextLFB();
		}, testTime, times, data);

	// Hairball I-RB.
	benchmark.addTest("Hairball-I-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Hairball L-S.
	benchmark.addTest("Hairball-L-S",
		[&camera, this]() -> void
		{
			this->cycleCollision();
			this->useNextLFB();
		}, testTime, times, data);

	// Hairball L-RB.
	benchmark.addTest("Hairball-L-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Screen-aligned quads tests.
	// Quads LL-S.
	benchmark.addTest("Quads-LL-S",
		[&camera, this]() -> void
		{
			camera.setPos({0, 0, 0});
			camera.setEulerRot({0, 0, 0});
			camera.setZoom(14);
			camera.update(0.1f);
			this->cycleCollision();
			this->useNextLFB();
			this->loadNextMesh();
		}, testTime, times, data);

	// Hairball LL-RB.
	benchmark.addTest("Quads-LL-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Quads I-S.
	benchmark.addTest("Quads-I-S",
		[&camera, this]() -> void
		{
			this->cycleCollision();
			this->useNextLFB();
		}, testTime, times, data);

	// Quads I-RB.
	benchmark.addTest("Quads-I-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);

	// Quads L-S.
	benchmark.addTest("Quads-L-S",
		[&camera, this]() -> void
		{
			this->cycleCollision();
			this->useNextLFB();
		}, testTime, times, data);

	// Quads L-RB.
	benchmark.addTest("Quads-L-RB",
		[&camera, this]() -> void
		{
			this->cycleCollision();
		}, testTime, times, data);
#endif

	benchmark.setRenderFunc([this]() -> void
	{
		Renderer::instance().getActiveCamera().setViewport(0, 0, Platform::Window::getWidth(), Platform::Window::getHeight());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glLoadIdentity();
		Renderer::instance().getActiveCamera().update(1.0f);
		Renderer::instance().getActiveCamera().upload();
		this->render();
	});

	benchmark.run();
	benchmark.print();

	// Append data+time to the title, and save to a benchmarks folder.
	time_t t = time(0);
	struct tm buf;
	#if _WIN32
		localtime_s(&buf, &t);
	#else
		localtime_r(&t, &buf);
	#endif
	std::stringstream ss;
	ss << std::put_time(&buf, "-%d-%m-%Y-%H-%M");
	benchmark.writeCSV("benchmarks/benchmark" + ss.str() + ".csv", times, data);

	exit(0);
}

void App::useMesh()
{
	static std::vector<std::string> meshes = { "meshes/galleon.obj", "meshes/sponza/sponza.3ds", "meshes/powerplant-obj/powerplant.obj",
		"meshes/hairball.ctm", "meshes/quadsLarge.obj", "meshes/quads1.obj", "meshes/dragon.obj", "meshes/teapot/teapot.obj", "meshes/powerplant/powerplant.ctm" };

	UniformBuffer::clearBufferGroup();

	if (currMesh > (int) meshes.size() - 1)
		currMesh = 0;
	else if (currMesh < 0)
		currMesh = (int) meshes.size() - 1;

	gpuMeshA.release();
	gpuMeshB.release();

#if 0
	Powerplant materials:
	Material: powerplant.mtlsec3_material_3 Count: 271200 // No idea, a bunch of wires?
	Material: powerplant.mtlsec10_material_0 Count: 19831578 // Blue stuff.
	Material: powerplant.mtlsec10_material_1 Count: 1554918 // Silo thingies.
	Material: powerplant.mtlsec11_material_1 Count: 7918164 // Whole lot of pipes (main set).
	Material: powerplant.mtlsec2_material_2 Count: 649422 // Don't know, internal yellow conveyor type thing?
	Material: powerplant.mtlsec6_material_4 Count: 240132 // Pipes related to above yellow thing.
	Material: powerplant.mtlsec7_material_1 Count: 3399300 // Internal yellow pipes next to the main set.
	Material: powerplant.mtlsec1_material_1 Count: 2243844 // More internal pipes.
	Material: powerplant.mtlsec3_material_2 Count: 43500 // External pipes around the blue stuff.
	Material: powerplant.mtlsec14_material_1 Count: 77646 // White-ish walls and platforms.
	Material: powerplant.mtlsec14_material_2 Count: 622836 // Exterior Walls for conveyor stuff.
	Material: powerplant.mtlsec16_material_1 Count: 63924 // Exterior pipes under silos.
	Material: powerplant.mtlsec21_material_3 Count: 272586 // Exterior crane thing.
	Material: powerplant.mtlsec21_material_4 Count: 1053936 // External pipes and chimney.
	Material: powerplant.mtlsec21_material_5 Count: 510 // Ground platform.
	Material: powerplant.mtlsec8_material_5 Count: 34242 // External walls and chimney.
#endif
#if 0
	Atrium materials:
	Material: sponzachain_ctx.pn Count: 96
	Material: sponzaflagpole_szf Count: 49488
	Material: sponzafloor_szfoad Count: 108
	Material: sponzafabric_c_szc Count: 56832
	Material: sponzafabric_f_szc Count: 43008
	Material: sponzaMaterial__25 Count: 9126
	Material: sponzaMaterial__47 Count: 54
	Material: sponzaleaf_szthdi. Count: 94308
	Material: sponzavase_vdi.png Count: 27552
	Material: sponza16___Default Count: 50688
	Material: sponzaMaterial__57 Count: 10416
	Material: sponzacolumn_a_szc Count: 8448
	Material: sponzaarch_szadi.p Count: 30504
	Material: sponzaroof_szrfdi. Count: 43452
	Material: sponzabricks_snbad Count: 2388
	Material: sponzadetails_szdt Count: 2640
	Material: sponzavase_round_v Count: 53064
	Material: sponzacolumn_c_szc Count: 21264
	Material: sponzavase_hanging Count: 59484
	Material: sponzaceiling_szcl Count: 17628
	Material: sponzafabric_g_szc Count: 43008
	Material: sponzafabric_e_szf Count: 49536
	Material: sponzafabric_a_szf Count: 33024
	Material: sponzaMaterial__29 Count: 12258
	Material: sponzafabric_d_szf Count: 49536
	Material: sponzacolumn_b_szc Count: 69624
#endif
	// Possible Atrium materials for splitting:
	// First group:
	// sponzachain_ctx.pn, sponzaflagpole_szf, sponzafabric_c_szc, sponzafabric_f_szc, sponzaleaf_szthdi., sponzavase_vdi.png,
	// sponza16___Default, sponzavase_round_v, sponzavase_hanging, sponzafabric_g_szc, sponzafabric_e_szf, sponzafabric_a_szf,
	// sponzafabric_d_szf,
	// Second group:
	//
	// Possible Powerplant materials for splitting:
	// First group:
	// powerplant.mtlsec10_material_0, powerplant.mtlsec10_material_1
	//
	// powerplant.mtlsec3_material_3, powerplant.mtlsec8_material_2, powerplant.mtlsec9_material_4
	// Second group:
	std::cout << "Loading mesh: " << meshes[currMesh] << "\n";

	Mesh mesh;
	if (Resources::MeshLoader::load(&mesh, meshes[currMesh]))
	{
		if (meshes[currMesh] == "meshes/powerplant/powerplant.ctm" || meshes[currMesh] == "meshes/powerplant-obj/powerplant.obj")
		{
			mesh.scale(vec3(0.0001f, 0.0001f, 0.0001f));
			mesh.translate(vec3(7.5, -10, -6)); // Move the mesh so it's reasonably well centered.

			if (meshes[currMesh] == "meshes/powerplant-obj/powerplant.obj")
			{
			#if MERGE_MESHES
				#if N_DEEP_IMAGES
				if (gpuMeshes.empty())
					gpuMeshes.resize(N_DEEP_IMAGES);
				Mesh tmpMeshes[N_DEEP_IMAGES];
				tmpMeshes[0] = mesh.extractFromMaterials({"powerplant.mtlsec10_material_0", "powerplant.mtlsec11_material_1", "powerplant.mtlsec2_material_2", "powerplant.mtlsec6_material_4"});
				tmpMeshes[1] = mesh.extractFromMaterials({"powerplant.mtlsec7_material_1", "powerplant.mtlsec1_material_1", "powerplant.mtlsec3_material_3", "powerplant.mtlsec10_material_1"});
				tmpMeshes[2] = mesh.extractFromMaterials({"powerplant.mtlsec3_material_2", "powerplant.mtlsec14_material_1", "powerplant.mtlsec14_material_2", "powerplant.mtlsec16_material_1"});
				tmpMeshes[3] = mesh.extractFromMaterials({"powerplant.mtlsec21_material_3", "powerplant.mtlsec21_material_4", "powerplant.mtlsec21_material_5", "powerplant.mtlsec8_material_5"});
				for (int i = 0; i < N_DEEP_IMAGES; i++)
				{
					gpuMeshes[i] = new GPUMesh();
					gpuMeshes[i]->create(&tmpMeshes[i]);
				}
				#else
				// Decide what the appropriate materials are for the internals.
				auto meshA = mesh.extractFromMaterials({"powerplant.mtlsec10_material_0", "powerplant.mtlsec11_material_1", "powerplant.mtlsec2_material_2",
					"powerplant.mtlsec6_material_4", "powerplant.mtlsec7_material_1", "powerplant.mtlsec1_material_1", });
				auto meshB = mesh.extractFromMaterials({"powerplant.mtlsec3_material_3", "powerplant.mtlsec10_material_1", "powerplant.mtlsec3_material_2",
					"powerplant.mtlsec14_material_1", "powerplant.mtlsec14_material_2", "powerplant.mtlsec16_material_1", "powerplant.mtlsec21_material_3",
					"powerplant.mtlsec21_material_4", "powerplant.mtlsec21_material_5", "powerplant.mtlsec8_material_5"});
				mesh = meshA;
				gpuMeshB.create(&meshB);
				#endif
			#endif
			}
		}
		else if (meshes[currMesh] == "meshes/hairball.ctm")
			mesh.scale(vec3(-10, -10, -10), vec3(10, 10, 10));
		else if (meshes[currMesh] == "meshes/sponza/sponza.3ds")
		{
			mesh.scale(vec3(-10, -5, -10), vec3(10.0f, 5.0f, 10.0f));
		#if MERGE_MESHES
			#if N_DEEP_IMAGES
			if (gpuMeshes.empty())
				gpuMeshes.resize(N_DEEP_IMAGES);
			Mesh tmpMeshes[N_DEEP_IMAGES];
			tmpMeshes[0] = mesh.extractFromMaterials({"sponzachain_ctx.pn", "sponzaflagpole_szf", "sponzafabric_c_szc", "sponzafabric_f_szc", "sponzaleaf_szthdi.", "sponzavase_vdi.png"});
			tmpMeshes[1] = mesh.extractFromMaterials({"sponza16___Default", "sponzavase_round_v", "sponzavase_hanging", "sponzafabric_g_szc", "sponzafabric_e_szf", "sponzafabric_a_szf"});
			tmpMeshes[2] = mesh.extractFromMaterials({"sponzafabric_d_szf", "sponzaMaterial__57", "sponzafloor_szfoad", "sponzaMaterial__25", "sponzaMaterial__47", "sponzacolumn_a_szc", "sponzaarch_szadi.p"});
			tmpMeshes[3] = mesh.extractFromMaterials({"sponzabricks_snbad", "sponzadetails_szdt", "sponzacolumn_c_szc", "sponzaceiling_szcl", "sponzaMaterial__29", "sponzacolumn_b_szc", "sponzaroof_szrfdi."});
			for (int i = 0; i < N_DEEP_IMAGES; i++)
			{
				gpuMeshes[i] = new GPUMesh();
				gpuMeshes[i]->create(&tmpMeshes[i]);
			}
			#else
			auto meshA = mesh.extractFromMaterials({"sponzachain_ctx.pn", "sponzaflagpole_szf", "sponzafabric_c_szc", "sponzafabric_f_szc", "sponzaleaf_szthdi.", "sponzavase_vdi.png",
				"sponza16___Default", "sponzavase_round_v", "sponzavase_hanging", "sponzafabric_g_szc", "sponzafabric_e_szf", "sponzafabric_a_szf", "sponzafabric_d_szf",
				"sponzaMaterial__57"});
			auto meshB = mesh.extractFromMaterials({"sponzafloor_szfoad", "sponzaMaterial__25", "sponzaMaterial__47", "sponzacolumn_a_szc", "sponzaarch_szadi.p", "sponzaroof_szrfdi.",
				"sponzabricks_snbad", "sponzadetails_szdt", "sponzacolumn_c_szc", "sponzaceiling_szcl", "sponzaMaterial__29", "sponzacolumn_b_szc"});
			mesh = meshA;
			gpuMeshB.create(&meshB);
			#endif
		#endif
		}
		else if (meshes[currMesh] == "meshes/galleon.obj")
			mesh.scale(vec3(0.2, 0.2, 0.2));
		else if (meshes[currMesh] == "meshes/teapot/teapot.obj")
			mesh.scale(vec3(2.0, 2.0, 2.0));
		else if (meshes[currMesh] == "meshes/dragon.obj")
			mesh.scale(vec3(10.0, 10.0, 10.0));
	#if !N_DEEP_IMAGES
		gpuMeshA.create(&mesh);
	#endif
	}
}

void App::useLFB()
{
	profiler.clear();

	static std::vector<LFB::LFBType> types = { LFB::LINEARIZED, LFB::LINK_LIST, LFB::COALESCED };
	static std::vector<std::string> typeStrs = { "Linearized", "Link List", "Coalesced" };

	lfbA.release();
	lfbB.release();

	delete currCol;
	currCol = nullptr;

	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();

	if (currLFB > (int) types.size() - 1)
		currLFB = 0;
	else if (currLFB < 0)
		currLFB = (int) types.size() - 1;
	LFB::LFBType type = types[currLFB];

	currLfbStr = typeStrs[currLFB];
	std::cout << "Using LFB: " << currLfbStr << "\n";

	if (opaqueFlag == 0)
	{
		lfbA.setType(type, "lfbA");
		lfbA.setMaxFrags(MAX_FRAGS);

		lfbB.setType(type, "lfbB", LFB::VEC2, false); // TODO: May need to set the overrideDefault flag to false.
		lfbB.setMaxFrags(MAX_FRAGS);

		lfbA.setProfiler(&profiler);
		lfbB.setProfiler(&profiler);
	}

#if N_DEEP_IMAGES
	if (lfbs.empty())
		lfbs.resize(N_DEEP_IMAGES);
	for (int i = 0; i < N_DEEP_IMAGES; i++)
	{
		lfbs[i].setType(type, "lfb" + Utils::toString(i));
		lfbs[i].setMaxFrags(MAX_FRAGS);
		lfbs[i].setProfiler(&profiler);
	}
#endif

#if MERGE_MESHES
	// Capturing geometry.
	auto vertGeomCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/collision/capture.vert");
	auto fragGeomCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/collision/capture.frag");
	fragGeomCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragGeomCapture.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragGeomCapture.setDefine("LFB_NAME", "lfbA");

	captureShaderA.release();
	captureShaderA.create(&vertGeomCapture, &fragGeomCapture);

	auto vertGeomCaptureB = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/collision/capture.vert");
	auto fragGeomCaptureB = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/collision/capture.frag");
	fragGeomCaptureB.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragGeomCaptureB.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragGeomCaptureB.setDefine("LFB_NAME", "lfbB");

	captureShaderB.release();
	captureShaderB.create(&vertGeomCaptureB, &fragGeomCaptureB);

	#if N_DEEP_IMAGES
	if (captureShaders.empty())
		captureShaders.resize(N_DEEP_IMAGES);
	else
		for (auto *shader : captureShaders)
			delete shader;
	for (int i = 0; i < N_DEEP_IMAGES; i++)
	{
		auto vertGeomNCapture = ShaderSourceCache::getShader("captureVert" + Utils::toString(i)).loadFromFile("shaders/collision/capture.vert");
		auto fragGeomNCapture = ShaderSourceCache::getShader("captureFrag" + Utils::toString(i)).loadFromFile("shaders/collision/capture.frag");
		fragGeomNCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
		fragGeomNCapture.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
		fragGeomNCapture.setDefine("LFB_NAME", "lfb" + Utils::toString(i));

		captureShaders[i] = new Shader();
		captureShaders[i]->release();
		captureShaders[i]->create(&vertGeomNCapture, &fragGeomNCapture);
	}
	#endif
#endif
#if MERGE_MESH_SPHERE
	// Capturing geometry.
	auto vertGeomCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/collision/capture.vert");
	auto fragGeomCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/collision/capture.frag");
	fragGeomCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragGeomCapture.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragGeomCapture.setDefine("LFB_NAME", "lfbA");

	captureShaderA.release();
	captureShaderA.create(&vertGeomCapture, &fragGeomCapture);

	// Capturing spheres.
	auto vertSphereCapture = ShaderSourceCache::getShader("sphereVert").loadFromFile("shaders/collision/sphere/capture.vert");
	auto geomSphereCapture = ShaderSourceCache::getShader("sphereGeom").loadFromFile("shaders/collision/sphere/capture.geom");
	auto fragSphereCapture = ShaderSourceCache::getShader("sphereFrag").loadFromFile("shaders/collision/sphere/capture.frag");
	fragSphereCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragSphereCapture.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragSphereCapture.setDefine("LFB_NAME", "lfbB");

	captureShaderB.release();
	captureShaderB.create(&vertSphereCapture, &fragSphereCapture, &geomSphereCapture);
#endif
#if MERGE_SPHERES
	// Capturing spheres.
	auto vertSphereCaptureB = ShaderSourceCache::getShader("sphereVertB").loadFromFile("shaders/collision/sphere/capture.vert");
	auto geomSphereCaptureB = ShaderSourceCache::getShader("sphereGeomB").loadFromFile("shaders/collision/sphere/capture.geom");
	auto fragSphereCaptureB = ShaderSourceCache::getShader("sphereFragB").loadFromFile("shaders/collision/sphere/capture.frag");
	fragSphereCaptureB.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragSphereCaptureB.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragSphereCaptureB.setDefine("LFB_NAME", "lfbA");

	captureShaderA.release();
	captureShaderA.create(&vertSphereCaptureB, &fragSphereCaptureB, &geomSphereCaptureB);

	// Capturing spheres.
	auto vertSphereCapture = ShaderSourceCache::getShader("sphereVert").loadFromFile("shaders/collision/sphere/capture.vert");
	auto geomSphereCapture = ShaderSourceCache::getShader("sphereGeom").loadFromFile("shaders/collision/sphere/capture.geom");
	auto fragSphereCapture = ShaderSourceCache::getShader("sphereFrag").loadFromFile("shaders/collision/sphere/capture.frag");
	fragSphereCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	fragSphereCapture.setDefine("INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE));
	fragSphereCapture.setDefine("LFB_NAME", "lfbB");

	captureShaderB.release();
	captureShaderB.create(&vertSphereCapture, &fragSphereCapture, &geomSphereCapture);
#endif

	// Example shader to produce some SASS for the paper.
	Shader exampleShader;
	auto exampleVert = ShaderSourceCache::getShader("exampleVert").loadFromFile("shaders/example.vert");
	exampleShader.create(&exampleVert);
	auto str = exampleShader.getBinary();
	Utils::File f("exampleShader.txt");
	f.write(str);

	if (opaqueFlag == 0)
	{
		// BMA shaders that sort and write to global memory.
		bmaA.createMaskShader("lfbA", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
		bmaA.createShaders(&lfbA, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "0"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});

		bmaB.createMaskShader("lfbB", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
		bmaB.createShaders(&lfbB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "0"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});

		// BMA shaders that sort and composite locally.
		localBmaA.createMaskShader("lfbA", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
		localBmaA.createShaders(&lfbA, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});

		localBmaB.createMaskShader("lfbB", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
		localBmaB.createShaders(&lfbB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});

	#if N_DEEP_IMAGES
		if (bmas.empty())
			bmas.resize(N_DEEP_IMAGES);
		for (int i = 0; i < N_DEEP_IMAGES; i++)
		{
			bmas[i].createMaskShader("lfb" + Utils::toString(i), "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag", {{"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
			bmas[i].createShaders(&lfbs[i], "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "0"}, {"INDEX_BY_TILE", Utils::toString(INDEX_BY_TILE)}});
		}
	#endif
	}

	// Schedule shader and intervals, similar to BMA.
	createScheduleIntervals();

	switch (colType)
	{
	case Collision::BRUTE_FORCE:
		currCol = new BruteForce(opaqueFlag);
		break;

	case Collision::REGISTER_BLOCK:
		currCol = new RegisterBlock(opaqueFlag);
		break;

	default:
		currCol = new BruteForce(opaqueFlag);
		break;
	}

	if (currCol)
	{
		currCol->init();
		currCol->useLFB(this, type);
	}
}

void App::toggleTransparency()
{
	opaqueFlag = opaqueFlag == 0 ? 1 : 0;
	useLFB();
}

void App::loadNextMesh()
{
	currMesh++;
	useMesh();
}

void App::loadPrevMesh()
{
	currMesh--;
	useMesh();
}

void App::useNextLFB()
{
	currLFB++;
	useLFB();
}

void App::usePrevLFB()
{
	currLFB--;
	useLFB();
}

void App::cycleCollision()
{
	static std::vector<Collision::CollisionType> colTypes = {Collision::BRUTE_FORCE, Collision::REGISTER_BLOCK};

	static int currColType = 0;

	currColType++;
	if (currColType > (int) colTypes.size() - 1)
		currColType = 0;
	useCollision(colTypes[currColType]);
}

void App::useCollision(Collision::CollisionType type)
{
	static std::map<Collision::CollisionType, std::string> colTypes;
	if (colTypes.empty())
	{
		colTypes[Collision::BRUTE_FORCE] = "Stepwise Merge";
		colTypes[Collision::REGISTER_BLOCK] = "Blockwise Register Merge";
	}

	colType = type;
	currColStr = colTypes[type];
	std::cout << "Using Approach: " << currColStr << "\n";

	useLFB();
}

void App::playAnim()
{
	anim.play();
}

void App::saveLfbA()
{
	if (opaqueFlag == 1)
		return;
	//auto s = geometryLFB.getStr();
	//auto s = geometryLFB.getBinary();
	auto s = lfbA.getStr();
	Utils::File f("lfbA.txt");
	f.write(s);
}

void App::saveLfbB()
{
	if (opaqueFlag == 1)
		return;

	auto s = lfbB.getStr();
	Utils::File f("lfbB.txt");
	f.write(s);
}

void App::saveLFBData()
{
	currCol->saveLFB(this);
}

void App::saveImage()
{
	// Save the frame to a file.
	Image img("screenshot.png");
	img.screenshot();
	img.flip();
	Resources::ImageLoader::save(&img, img.getName());
}

void App::visualise()
{
	currCol->visualise(this);
}

