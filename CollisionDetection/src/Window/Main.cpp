#include <cstdlib>

#include "../App.h"

#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Platform/src/PlatformCommon.h"

#include <iostream>

#define UNUSED(x) (void)(x)

using namespace Rendering;
using namespace Platform;

bool wireframeFlag = false;


static void render(App &app)
{
	Renderer::instance().getActiveCamera().setViewport(0, 0, Window::getWidth(), Window::getHeight());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glLoadIdentity();
	Renderer::instance().getActiveCamera().upload();

	app.render();
}

static void update(App &app, float dt)
{
	app.update(dt);
	Renderer::instance().getActiveCamera().update(dt);
}

static void toggleWireframe()
{
	wireframeFlag = !wireframeFlag;
	if (wireframeFlag)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

static bool mouseDrag(Event &ev)
{
	if (Mouse::isDown(Mouse::LEFT))
		Renderer::instance().getActiveCamera().updateRotation((float) ev.dx, (float) ev.dy);
	if (Mouse::isDown(Mouse::RIGHT))
		Renderer::instance().getActiveCamera().updateZoom((float) ev.dy);
	if (Mouse::isDown(Mouse::MIDDLE))
		Renderer::instance().getActiveCamera().updatePan((float) ev.dx, (float) ev.dy);
	return false;
}

static bool keyPress(App &app, Event &ev)
{
	switch (ev.key)
	{
	case Keyboard::ESCAPE:
	case Keyboard::q:
		exit(EXIT_SUCCESS);
		break;

	case Keyboard::p:
		//toggleWireframe();
		app.saveImage();
		break;

	case Keyboard::LEFT:
		app.usePrevLFB();
		break;

	case Keyboard::RIGHT:
		app.useNextLFB();
		break;

	case Keyboard::UP:
		//app.doubleLights();
		//app.loadNextMesh();
		break;

	case Keyboard::DOWN:
		//app.halveLights();
		//app.loadPrevMesh();
		break;

	case Keyboard::t:
		app.toggleTransparency();
		break;

	case Keyboard::l:
		app.cycleCollision();
		break;

	case Keyboard::b:
		app.runBenchmarks();
		break;

	case Keyboard::c:
		std::cout << Renderer::instance().getActiveCamera() << "\n";
		break;

	case Keyboard::o:
		app.saveLFBData();
		break;

	case Keyboard::m:
		app.loadNextMesh();
		break;

	case Keyboard::a:
		app.playAnim();
		app.toggleAxes();
		break;

	case Keyboard::v:
		app.visualise();
		break;

	case Keyboard::h:
		app.toggleHud();
		break;

	default:
		break;
	}

	return false;
}


int main(int argc, char **argv)
{
	UNUSED(argc);
	UNUSED(argv);

	// Cleanup code.
	std::atexit([]()-> void {
		Rendering::Renderer::instance().clear();
		Window::destroyWindow();
	});

	Window::createWindow(0, 0, 512, 512);
	Renderer::instance().init();

	App app;
	auto &camera = Renderer::instance().addCamera("main", Camera(Camera::Type::PERSP));

	// Atrium balcony view.
	camera.setPos({-0.5f, -0.8f, 0.0f});
	camera.setEulerRot({-0.209439f, 0.0f, 0.0f});
	camera.setZoom(6.68847f);
	camera.update(0.1f);

#if 0
	EventHandler::addHandler(Event::Type::KEY_PRESSED, &keyPress);
	EventHandler::addHandler(Event::Type::MOUSE_DRAG, &mouseDrag);
#endif

	app.init();

	bool finished = false;
	while (!finished)
	{
		float dt = Window::getDeltaTime();

	#if 0
		// First approach for event handling, using callback functions provided above (like glut).
		//finished = Window::handleEvents();
	#elif 1
		// Alternative approach, loop through the events and handle each in the switch statement (like SDL).
		Window::processEvents();
		for (auto &ev : EventHandler::events)
		{
			switch (ev.type)
			{
			case Event::Type::WINDOW_RESIZED:
				Platform::Window::setWindowSize(ev);
				break;

			case Event::Type::KEY_PRESSED:
				keyPress(app, ev);
				break;

			case Event::Type::MOUSE_DRAG:
				mouseDrag(ev);
				break;

			default:
				break;
			}
		}
	#elif 0
		// Yet another alternative approach, just ask the keyboard and mouse if something has happened.
		Window::processEvents();
		if (Keyboard::isPressed(Keyboard::ESCAPE) || Keyboard::isPressed(Keyboard::q))
			exit(EXIT_SUCCESS);
		if (Keyboard::isPressed(Keyboard::p))
		{
			DebugControls::wireframeFlag = !DebugControls::wireframeFlag;
			DebugControls::toggleWireframe();
		}
		if (Keyboard::isPressed(Keyboard::c))
		{
			DebugControls::cullFlag = !DebugControls::cullFlag;
			DebugControls::toggleCulling();
		}
		if (Mouse::isDown(Mouse::LEFT))
			Renderer::instance().getActiveCamera().updateRotation(Mouse::dx, Mouse::dy);
		if (Mouse::isDown(Mouse::RIGHT))
			Renderer::instance().getActiveCamera().updateZoom(Mouse::dy);
		if (Mouse::isDown(Mouse::MIDDLE))
			Renderer::instance().getActiveCamera().updatePan(Mouse::dx, Mouse::dy);
	#endif
		update(app, dt);
		render(app);

		CHECKERRORS;

		Window::swapBuffers();
	}

	Window::destroyWindow();

	return EXIT_SUCCESS;
}

