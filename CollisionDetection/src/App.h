#pragma once

#define GRID_CELL_SIZE 16
#define CLUSTERS 32
#define MAX_EYE_Z 30.0
#define MASK_SIZE (CLUSTERS / 32)

#include "Collision/Collision.h"

#include "../../Renderer/src/LFB/LFB.h"
#include "../../Renderer/src/LFB/BMA.h"
#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/Profiler.h"

#include "../../Math/src/Vector/Vec4.h"

#include <vector>

class App
{
public:
	struct Interval
	{
		int start;
		int end;
		Interval(int start = 0, int end = 0) : start(start), end(end) {}
	};

private:
	int currLFB;
	int currMesh;

	int opaqueFlag;

	bool axisFlag;

	Collision::CollisionType colType;
	Collision *currCol;

	Rendering::LFB lfbA;
	Rendering::BMA bmaA;
	Rendering::BMA localBmaA;

	Rendering::LFB lfbB;
	Rendering::BMA bmaB;
	Rendering::BMA localBmaB;

	Rendering::Shader scheduleShader;
	std::vector<Interval> intervals;

	Rendering::Shader basicShader;
	Rendering::Shader captureShaderA;
	Rendering::Shader captureShaderB;
	Rendering::Shader countSphereShader;

	Rendering::StorageBuffer spherePosBuffer;
	Rendering::StorageBuffer sphereRadiusBuffer;
	Rendering::StorageBuffer sphereColorBuffer;
	Rendering::StorageBuffer sphereVelBuffer;

	std::vector<Math::vec4> spherePositions;
	std::vector<float> sphereRadii;

	std::vector<Math::vec4> sphereVectors;
	std::vector<float> sphereCollisions;
	std::vector<float> sphereColors;

	Rendering::GPUMesh gpuMeshA;
	Rendering::GPUMesh gpuMeshB;

	// For merging k deep images.
	std::vector<Rendering::LFB> lfbs;
	std::vector<Rendering::BMA> bmas;
	// TODO: Put some local bma's in here (maybe).
	std::vector<Rendering::Shader*> captureShaders;
	std::vector<Rendering::GPUMesh*> gpuMeshes;

	Rendering::Profiler profiler;

	int width;
	int height;

	int tmpWidth;
	int tmpHeight;

	bool hudFlag;

private:
	void createSpheres(size_t nSpheres);
	void createSphereBuffers();

	void setSphereVelocities(float speed);
	void updateSpherePositions(float dt);

public:
	App() : currLFB(1), currMesh(0), opaqueFlag(0), axisFlag(true), colType(Collision::BRUTE_FORCE), currCol(nullptr), width(0), height(0), tmpWidth(0), tmpHeight(0), lfbAPos(8.0f, 0.0f, 0.0f), lfbBPos(-8.0f, 0.0f, 0.0f), hudFlag(true) {}
	~App() {}

	void init();

	void setTmpViewport(int width, int height);
	void restoreViewport();

	void setCameraUniforms(Rendering::Shader *shader);

	void captureLfbA();
	void captureLfbB();

	void captureGeometry(Rendering::Shader *shader = nullptr, Rendering::LFB *lfb = nullptr, Rendering::GPUMesh *gpuMesh = nullptr, Math::vec3 col = Math::vec3(0.0f, 0.0f, 0.0f));
	void sortLfbA(bool composite = true);

	void captureSpheres(Rendering::Shader *shader = nullptr, Rendering::LFB *lfb = nullptr, Math::vec3 col = Math::vec3(0.0f, 0.0f, 1.0f), int sphereOffset = 0);
	void sortLfbB(bool composite = true);

	void captureNLfbs();
	void sortNLfbs();

	void createScheduleIntervals();
	void createScheduleMask();

	void render();
	void update(float dt);

	void runBenchmarks();

	void useMesh();
	void useLFB();

	void toggleTransparency();

	void loadNextMesh();
	void loadPrevMesh();

	void useNextLFB();
	void usePrevLFB();

	void cycleCollision();
	void useCollision(Collision::CollisionType type);

	void playAnim();

	void saveLfbA();
	void saveLfbB();
	void saveLFBData();
	void saveImage();

	void visualise();

	Rendering::LFB &getLfbA() { return lfbA; }
	Rendering::LFB &getLfbB() { return lfbB; }
	Rendering::GPUMesh &getGpuMesh() { return gpuMeshA; }

	Rendering::GPUMesh &getGpuMeshA() { return gpuMeshA; }
	Rendering::GPUMesh &getGpuMeshB() { return gpuMeshB; }

	std::vector<Rendering::LFB> &getLfbs() { return lfbs; }
	std::vector<Rendering::BMA> &getBmas() { return bmas; }
	std::vector<Rendering::GPUMesh*> &getGpuMeshes() { return gpuMeshes; }

	Rendering::Profiler &getProfiler() { return profiler; }

	std::vector<Interval> &getIntervals() { return intervals; }

	void toggleAxes() { axisFlag = !axisFlag; }

	void toggleHud() { hudFlag = !hudFlag; }

	Math::vec3 lfbAPos, lfbBPos;
};

