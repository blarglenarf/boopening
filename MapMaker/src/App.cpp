#include "App.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <iomanip>


// TODO:
// Map stuff:
// Background texture.
// Texture loading.
// Faster grid outline rendering.
// Populating grid cells with textures.
// Random object placement.
// Saving maps.
//
// Gui stuff:
// Markdown support where appropriate.
// Some sort of style system, possibly based on css3.
// Some kind of gui file format thingy.
// Gui interaction with a database of some kind, and different files.
// Button: Button, CycleButton, RadioButton, Checkbox, Slider, Scrollbar.
// List: ListBox, DropDownList.
// Text input: ComboBox, Spinner, DropDownCombo, TextBox, Console.
// Menu: Menu, MenuBar, ContextMenu, PieMenu.
// Container: Window, DialogBox, Tab, Accordian, TreeView, GridView, FileBrowser, Toolbar?.
// Text output: Label, Tooltip, StatusBar, InfoBar (TODO: some formatting for the display of text?).
// Interaction: Click, move, drag, hover, scroll, key press, key hold, widget drag, widget resize, interaction modes.
// Ability to interact with objects not part of the gui (eg a game object), including different controls for it, and text info.
// Possible styles:
// Positioning using a grid view (eg dividing into columns, kind of like how bootstrap works).
// Transparency.
// Corner rounding.
// Background image.
// Color.
// Gradients (possibly with shapes).
// Shadow effects.
// Text wrapping.
// 2d and 3d transforms.
// Animation/transitions.
// Some kind of support for different resolution sizes (eg, different layouts maybe?).
// Text sizes, need a way of making things bigger on larger monitors (eg 4K).
// Templates.


using namespace Math;
using namespace Rendering;

// TODO: Get rid of this!
Font testFont;

vec4 getEyeFromWindow(vec3 p, vec4 viewport, mat4 pMatrix)
{
	mat4 invPMatrix = pMatrix.getInverse();

	vec3 ndcPos;
	ndcPos.xy = ((p.xy - viewport.xy) / viewport.zw) * 2.0f - 1.0f;
	//ndcPos.xy = ((p.xy * 2.0f) - (viewport.xy * 2.0f)) / (viewport.zw) - 1.0f;
	ndcPos.z = 1.0f;
	//std::cout << "ndc: " << ndcPos << "\n";

	vec4 eyeDir = invPMatrix * vec4(ndcPos, 1.0f);
	eyeDir /= eyeDir.w;
	vec4 eyePos = vec4(eyeDir.xyz * p.z / eyeDir.z, 1.0f);
	return eyePos;
}

vec4 getObjectFromWindow(int x, int y, float z, vec4 viewport, mat4 mvMatrix, mat4 pMatrix)
{
	vec4 eye = getEyeFromWindow(vec3(x, y, z), viewport, pMatrix);

	mat4 invMvMatrix = mvMatrix.getInverse();
	vec4 obj = invMvMatrix * eye;

	return obj;
}


Math::ivec2 App::screenToGridCoord(int x, int y)
{
	auto &camera = Renderer::instance().getActiveCamera();
	auto view = camera.getViewport();
	vec4 obj = getObjectFromWindow(x, y, -camera.getZoom(), vec4(view.x, view.y, view.z, view.w), camera.getInverse(), camera.getProjection());
	return Math::ivec2((int) obj.x, (int) obj.y);
}

void App::renderGrid()
{
	glPushAttrib(GL_POLYGON_BIT | GL_CURRENT_BIT | GL_LINE_BIT | GL_ENABLE_BIT);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDisable(GL_DEPTH_TEST);

	glBegin(GL_QUADS);
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			glVertex2i(x, y);
			glVertex2i(x + 1, y);
			glVertex2i(x + 1, y + 1);
			glVertex2i(x, y + 1);
		}
	}
	glEnd();

	glLineWidth(4);
	if (currX >= 0 && currX < width && currY >= 0 && currY < height)
	{
		glColor3f(0, 0, 1);
		glBegin(GL_QUADS);
		glVertex2i(currX, currY);
		glVertex2i(currX + 1, currY);
		glVertex2i(currX + 1, currY + 1);
		glVertex2i(currX, currY + 1);
		glEnd();
	}

	glPopAttrib();
}


void App::init()
{
	// TODO: Get rid of this!
	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");
}

void App::render()
{
	renderGrid();

#if 0
	Renderer::instance().drawAxes();
#endif
}

void App::update(float dt)
{
	(void) (dt);

	auto mouseCoord = screenToGridCoord(Platform::Mouse::x, Platform::Mouse::y);
	currX = mouseCoord.x;
	currY = mouseCoord.y;
}

