#pragma once

#include "../../Renderer/src/LFB/LFB.h"
#include "../../Renderer/src/LFB/BMA.h"
#include "../../Renderer/src/LFB/Cluster.h"
#include "../../Renderer/src/LFB/ClusterMask.h"
#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/Profiler.h"

#include <vector>

class App
{
private:
	int width;
	int height;

	int currX;
	int currY;

private:
	Math::ivec2 screenToGridCoord(int x, int y);
	void renderGrid();

public:
	App() : width(100), height(100), currX(-1), currY(-1) {}
	~App() {}

	void init();

	void render();
	void update(float dt);
};

