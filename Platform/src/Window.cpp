#include "Window.h"
#include "EventHandler.h"
#include "Win32/WinWindow.h"
#include "Linux/LinWindow.h"

using namespace Platform;

Utils::Timer Platform::Window::timer;

int Platform::Window::width = 0;
int Platform::Window::height = 0;
float Platform::Window::deltaTime = 0;


void Platform::Window::createWindow(int x, int y, int width, int height)
{
	Platform::Window::width = width;
	Platform::Window::height = height;

	PlatformWindow::createWindow(x, y, width, height);

	for (int i = 0; i < 10; i++)
		EventHandler::addHandlerGroup();
	EventHandler::getHandlerGroup(0).addHandler(Event::Type::WINDOW_RESIZED, &Platform::Window::setWindowSize);
}

void Platform::Window::destroyWindow()
{
	PlatformWindow::destroyWindow();
}

float Platform::Window::getDeltaTime()
{
	return deltaTime;
}

void Platform::Window::resetDeltaTime()
{
	timer.time();
	deltaTime = timer.time() * 0.001f;
}

void Platform::Window::processEvents()
{
	EventHandler::processEvents();
}

bool Platform::Window::handleEvents()
{
	return EventHandler::handleEvents();
}

void Platform::Window::swapBuffers()
{
	deltaTime = timer.time() * 0.001f;
	PlatformWindow::swapBuffers();
}

void Platform::Window::resize(int width, int height)
{
	PlatformWindow::resize(width, height);
}

