#pragma once

#include "Event.h"

#include <vector>
#include <map>
#include <functional>

namespace Platform
{
	class EventHandler;

	class EventHandlerGroup
	{
		friend class EventHandler;

	private:
		// TODO: testing indicates that std::function doesn't have a significant performance penalty, but I could be wrong...
		struct Handler
		{
			size_t id;
			std::function<bool (Event &ev)> func;
		};

	private:
		std::map<Event::Type, std::vector<Handler>> eventHandlers;

		std::vector<std::pair<Event::Type, Handler>> addQueue;
		std::vector<std::pair<Event::Type, size_t>> removeQueue;

		size_t groupID;
		size_t maxHandlerID;

	private:
		void handleAddQueue();
		void handleRemoveQueue();

	public:
		EventHandlerGroup(size_t groupID = 0) : groupID(groupID), maxHandlerID(0) {}

		size_t addHandler(Event::Type type, std::function<bool (Event &ev)> handler);
		void removeHandler(Event::Type type, size_t id);
		void clearHandlers() { eventHandlers.clear(); }

		size_t getGroupID() const { return groupID; }
	};

	class EventHandler
	{
	private:
		static std::vector<EventHandlerGroup> handlerGroups;

	public:
		static std::vector<Event> events;

	public:
		// Note: call one function or the other, not both. See Main.cpp of RendererExamples for example usage.
		static void processEvents();
		static bool handleEvents();

		static size_t handlerGroupSize() { return handlerGroups.size(); }
		static void resizeHandlerGroups(size_t size) { handlerGroups.resize(size); }
		static EventHandlerGroup &addHandlerGroup();
		static void removeHandlerGroup(size_t pos);
		static EventHandlerGroup &getHandlerGroup(size_t pos) { return handlerGroups[pos]; }

		static size_t addHandler(Event::Type type, std::function<bool(Event &ev)> handler, size_t groupID = 0) { return getHandlerGroup(groupID).addHandler(type, handler); }
		static void removeHandler(Event::Type type, size_t id, size_t groupID = 0) { return getHandlerGroup(groupID).removeHandler(type, id); }
	};
}

