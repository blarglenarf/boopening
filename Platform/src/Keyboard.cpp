#include "Keyboard.h"

using namespace Platform;

bool Keyboard::states[Keyboard::NUM_STATES];

std::list<Keyboard::Key> Keyboard::keyPresses;
std::map<std::string, Keyboard::Key> Keyboard::keyMap;
std::map<std::string, Keyboard::Key> Keyboard::shiftKeyMap;
std::map<Keyboard::Key, std::string> Keyboard::charMap;
std::map<Keyboard::Key, std::string> Keyboard::shiftCharMap;

bool Keyboard::capsLock = false;

bool keyB = Keyboard::init();

bool Keyboard::init()
{
	for (int tmp = 0; tmp < Keyboard::NUM_STATES; tmp++)
		states[tmp] = false;

	// sigh
	keyMap["NONE"]					= NONE;
	// ASCII commands
	keyMap["BACKSPACE"]				= BACKSPACE;
	keyMap["TAB"]					= TAB;
	charMap[TAB]					= "\t";
	keyMap["LINEFEED"]				= LINEFEED;
	charMap[LINEFEED]				= "\r";
	keyMap["CLEAR"]					= CLEAR;
	keyMap["ENTER"]					= ENTER;      // synonym
	charMap[ENTER]					= "\n";
	keyMap["PAUSE"]					= PAUSE;
	keyMap["SCROLLLOCK"]			= SCROLLLOCK;
	keyMap["SYSREQ"]				= SYSREQ;
	keyMap["ESCAPE"]				= ESCAPE;
	//SPACE				= 0xff20,

	// Cursor control and motion
	keyMap["HOME"]					= HOME;
	keyMap["LEFT"]					= LEFT;
	keyMap["UP"]					= UP;
	keyMap["RIGHT"]					= RIGHT;
	keyMap["DOWN"]					= DOWN;
	keyMap["PAGEUP"]				= PAGEUP;
	keyMap["PAGEDOWN"]				= PAGEDOWN;
	keyMap["END"]					= END;
	keyMap["BEGIN"]					= BEGIN;
#if 0
	// Misc functions
	DELETE				= 0xffff,
	SELECT				= 0xff60,
	PRINT				= 0xff61,
	EXECUTE				= 0xff62,
	INSERT				= 0xff63,
	UNDO				= 0xff65,
	REDO				= 0xff66,
	MENU				= 0xff67,
	FIND				= 0xff68,
	CANCEL				= 0xff69,
	HELP				= 0xff6a,
	BREAK				= 0xff6b,
	MODESWITCH			= 0xff7e,
	SCRIPTSWITCH		= 0xff7e,
#endif
	// Number pad
	keyMap["NUMPADLOCK"]			= NUMPADLOCK;
	keyMap["NUMPAD_SPACE"]			= NUMPAD_SPACE;
	charMap[NUMPAD_SPACE]			= " ";
	keyMap["NUMPAD_TAB"]			= NUMPAD_TAB;
	charMap[NUMPAD_TAB]				= "\t";
	keyMap["NUMPAD_ENTER"]			= NUMPAD_ENTER;
	charMap[NUMPAD_ENTER]			= "\n";
	keyMap["NUMPAD_F1"]				= NUMPAD_F1;
	keyMap["NUMPAD_F2"]				= NUMPAD_F2;
	keyMap["NUMPAD_F3"]				= NUMPAD_F3;
	keyMap["NUMPAD_F4"]				= NUMPAD_F4;
	keyMap["NUMPAD_HOME"]			= NUMPAD_HOME;
	keyMap["NUMPAD_LEFT"]			= NUMPAD_LEFT;
	keyMap["NUMPAD_UP"]				= NUMPAD_UP;
	keyMap["NUMPAD_RIGHT"]			= NUMPAD_RIGHT;
	keyMap["NUMPAD_DOWN"]			= NUMPAD_DOWN;
	keyMap["NUMPAD_PRIOR"]			= NUMPAD_PRIOR;
	keyMap["NUMPAD_PAGE_UP"]		= NUMPAD_PAGE_UP;
	keyMap["NUMPAD_NEXT"]			= NUMPAD_NEXT;
	keyMap["NUMPAD_PAGE_DOWN"]		= NUMPAD_PAGE_DOWN;
	keyMap["NUMPAD_END"]			= NUMPAD_END;
	keyMap["NUMPAD_BEGIN"]			= NUMPAD_BEGIN;
	keyMap["NUMPAD_INSERT"]			= NUMPAD_INSERT;
	keyMap["NUMPAD_DELETE"]			= NUMPAD_DELETE;
	keyMap["NUMPAD_EQUAL"]			= NUMPAD_EQUAL;
	charMap[NUMPAD_EQUAL]			= "=";
	keyMap["NUMPAD_MULTIPLY"]		= NUMPAD_MULTIPLY;
	charMap[NUMPAD_MULTIPLY]		= "*";
	keyMap["NUMPAD_PLUS"]			= NUMPAD_PLUS;
	charMap[NUMPAD_PLUS]			= "+";
	keyMap["NUMPAD_SEPARATOR"]		= NUMPAD_SEPARATOR;
	charMap[NUMPAD_SEPARATOR]		= "\n";
	keyMap["NUMPAD_MINUS"]			= NUMPAD_MINUS;
	charMap[NUMPAD_MINUS]			= "-";
	keyMap["NUMPAD_DECIMAL"]		= NUMPAD_DECIMAL;
	charMap[NUMPAD_DECIMAL]			= ".";
	keyMap["NUMPAD_DIVIDE"]			= NUMPAD_DIVIDE;
	charMap[NUMPAD_DIVIDE]			= "/";

	keyMap["NUMPAD_0"]				= NUMPAD_0;
	charMap[NUMPAD_0]				= "0";
	keyMap["NUMPAD_1"]				= NUMPAD_1;
	charMap[NUMPAD_1]				= "1";
	keyMap["NUMPAD_2"]				= NUMPAD_2;
	charMap[NUMPAD_2]				= "2";
	keyMap["NUMPAD_3"]				= NUMPAD_3;
	charMap[NUMPAD_3]				= "3";
	keyMap["NUMPAD_4"]				= NUMPAD_4;
	charMap[NUMPAD_4]				= "4";
	keyMap["NUMPAD_5"]				= NUMPAD_5;
	charMap[NUMPAD_5]				= "5";
	keyMap["NUMPAD_6"]				= NUMPAD_6;
	charMap[NUMPAD_6]				= "6";
	keyMap["NUMPAD_7"]				= NUMPAD_7;
	charMap[NUMPAD_7]				= "7";
	keyMap["NUMPAD_8"]				= NUMPAD_8;
	charMap[NUMPAD_8]				= "8";
	keyMap["NUMPAD_9"]				= NUMPAD_9;
	charMap[NUMPAD_9]				= "9";

	// Function KEYs
	keyMap["F1"]					= F1;
	keyMap["F2"]					= F2;
	keyMap["F3"]					= F3;
	keyMap["F4"]					= F4;
	keyMap["F5"]					= F5;
	keyMap["F6"]					= F6;
	keyMap["F7"]					= F7;
	keyMap["F8"]					= F8;
	keyMap["F9"]					= F9;
	keyMap["F10"]					= F10;
	keyMap["F11"]					= F11;
	keyMap["F12"]					= F12;
	keyMap["F13"]					= F13;
	keyMap["F14"]					= F14;
	keyMap["F15"]					= F15;
	keyMap["F16"]					= F16;

	// Modifiers
	keyMap["LSHIFT"]				= LSHIFT;
	keyMap["RSHIFT"]				= RSHIFT;
	keyMap["LCTRL"]					= LCTRL;
	keyMap["RCTRL"]					= RCTRL;
	keyMap["CAPSLOCK"]				= CAPSLOCK;
	keyMap["LMETA"]					= LMETA;
	keyMap["RMETA"]					= RMETA;
	keyMap["LALT"]					= LALT;
	keyMap["RALT"]					= RALT;
	keyMap["LWINDOWS"]				= LWINDOWS;
	keyMap["RWINDOWS"]				= RWINDOWS;
	keyMap["LCOMMAND"]				= LCOMMAND;
	keyMap["RCOMMAND"]				= RCOMMAND;
	keyMap["LOPTION"]				= LOPTION;
	keyMap["ROPTION"]				= ROPTION;

	// Latin-1
	keyMap["SPACE"]					= SPACE;
	charMap[SPACE]					= " ";
	keyMap["EXCLAMATION"]			= EXCLAMATION;
	charMap[EXCLAMATION]			= "!";
	keyMap["DOUBLEQUOTE"]			= DOUBLEQUOTE;
	charMap[DOUBLEQUOTE]			= "\"";
	keyMap["HASH"]					= HASH;
	charMap[HASH] 					= "#";
	keyMap["POUND"]					= POUND;  // synonym
	charMap[POUND]					= "#";
	keyMap["DOLLAR"]				= DOLLAR;
	charMap[DOLLAR]					= "$";
	keyMap["PERCENT"]				= PERCENT;
	charMap[PERCENT]				= "%";
	keyMap["AMPERSAND"]				= AMPERSAND;
	charMap[AMPERSAND]				= "&";

	keyMap["APOSTROPHE"]			= APOSTROPHE;
	shiftKeyMap["APOSTROPHE"]		= DOUBLEQUOTE;
	charMap[APOSTROPHE]				= "\'";
	shiftCharMap[APOSTROPHE]		= "\"";

	keyMap["PARENLEFT"]				= PARENLEFT;
	charMap[PARENLEFT]				= "(";
	keyMap["PARENRIGHT"]			= PARENRIGHT;
	charMap[PARENRIGHT]				= ")";
	keyMap["ASTERISK"]				= ASTERISK;
	charMap[ASTERISK]				= "*";

	keyMap["PLUS"]					= PLUS;
	shiftKeyMap["PLUS"]				= EQUAL;
	charMap[PLUS]					= "+";
	shiftCharMap[PLUS]				= "=";

	keyMap["COMMA"]					= COMMA;
	shiftKeyMap["COMMA"]			= LESS;
	charMap[COMMA]					= ",";
	shiftCharMap[COMMA]				= "<";

	keyMap["MINUS"]					= MINUS;
	shiftKeyMap["MINUS"]			= UNDERSCORE;
	charMap[MINUS]					= "-";
	shiftCharMap[MINUS]				= "_";

	keyMap["PERIOD"]				= PERIOD;
	shiftKeyMap["PERIOD"]			= GREATER;
	charMap[PERIOD]					= ".";
	shiftCharMap[PERIOD]			= ">";

	keyMap["SLASH"]					= SLASH;
	shiftKeyMap["SLASH"]			= QUESTION;
	charMap[SLASH]					= "/";
	shiftCharMap[SLASH]				= "?";

	keyMap["0"]						= KEY_0;
	shiftKeyMap["0"]				= PARENRIGHT;
	charMap[KEY_0]					= "0";
	shiftCharMap[KEY_0]				= ")";

	keyMap["1"]						= KEY_1;
	shiftKeyMap["1"]				= EXCLAMATION;
	charMap[KEY_1]					= "1";
	shiftCharMap[KEY_1]				= "!";

	keyMap["2"]						= KEY_2;
	shiftKeyMap["2"]				= AT;
	charMap[KEY_2]					= "2";
	shiftCharMap[KEY_2]				= "@";

	keyMap["3"]						= KEY_3;
	keyMap["3"]						= HASH;
	charMap[KEY_3]					= "3";
	shiftCharMap[KEY_3]				= "#";

	keyMap["4"]						= KEY_4;
	shiftKeyMap["4"]				= DOLLAR;
	charMap[KEY_4]					= "4";
	shiftCharMap[KEY_4]				= "$";

	keyMap["5"]						= KEY_5;
	shiftKeyMap["5"]				= PERCENT;
	charMap[KEY_5]					= "5";
	shiftCharMap[KEY_5]				= "%";

	keyMap["6"]						= KEY_6;
	shiftKeyMap["6"]				= HAT;
	charMap[KEY_6]					= "6";
	shiftCharMap[KEY_6]				= "^";

	keyMap["7"]						= KEY_7;
	shiftKeyMap["7"]				= AMPERSAND;
	charMap[KEY_7]					= "7";
	shiftCharMap[KEY_7]				= "&";

	keyMap["8"]						= KEY_8;
	shiftKeyMap["8"]				= ASTERISK;
	charMap[KEY_8]					= "8";
	shiftCharMap[KEY_8]				= "*";

	keyMap["9"]						= KEY_9;
	shiftKeyMap["9"]				= PARENLEFT;
	charMap[KEY_9]					= "9";
	shiftCharMap[KEY_9]				= "(";

	keyMap["COLON"]					= COLON;
	charMap[COLON]					= ":";
	keyMap["SEMICOLON"]				= SEMICOLON;
	shiftKeyMap["SEMICOLON"]		= COLON;
	charMap[SEMICOLON]				= ";";
	shiftCharMap[SEMICOLON]			= ":";

	keyMap["LESS"]					= LESS;
	charMap[LESS]					= "<";
	keyMap["EQUAL"]					= EQUAL;
	charMap[EQUAL]					= "=";
	keyMap["GREATER"]				= GREATER;
	charMap[GREATER]				= ">";
	keyMap["QUESTION"]				= QUESTION;
	charMap[QUESTION]				= "?";
	keyMap["AT"]					= AT;
	charMap[AT]						= "@";

	keyMap["BRACKETLEFT"]			= BRACKETLEFT;
	shiftKeyMap["BRACKETLEFT"]		= BRACELEFT;
	charMap[BRACKETLEFT]			= "[";
	shiftCharMap[BRACKETLEFT]		= "{";

	keyMap["BACKSLASH"]				= BACKSLASH;
	shiftKeyMap["BACKSLASH"]		= BAR;
	charMap[BACKSLASH]				= "\\";
	shiftCharMap[BACKSLASH]			= "|";

	keyMap["BRACKETRIGHT"]			= BRACKETRIGHT;
	shiftKeyMap["BRACKETRIGHT"]		= BRACERIGHT;
	charMap[BRACKETRIGHT]			= "]";
	shiftCharMap[BRACKETRIGHT]		= "}";

	keyMap["ASCIICIRCUM"]			= ASCIICIRCUM;
	charMap[ASCIICIRCUM]			= "^";
	keyMap["HAT"]					= HAT;
	charMap[HAT]					= "^";
	keyMap["UNDERSCORE"]			= UNDERSCORE;
	charMap[UNDERSCORE]				= "_";

	keyMap["GRAVE"]					= GRAVE;
	shiftKeyMap["GRAVE"]			= ASCIITILDE;
	charMap[GRAVE]					= "`";
	shiftCharMap[GRAVE]				= "~";
	keyMap["QUOTELEFT"]				= QUOTELEFT;
	shiftKeyMap["QUOTELEFT"]		= ASCIITILDE;
	charMap[QUOTELEFT]				= "`";
	shiftCharMap[QUOTELEFT]			= "~";

	keyMap["a"]						= a;
	charMap[a]						= "a";
	keyMap["b"]						= b;
	charMap[b]						= "b";
	keyMap["c"]						= c;
	charMap[c]						= "c";
	keyMap["d"]						= d;
	charMap[d]						= "d";
	keyMap["e"]						= e;
	charMap[e]						= "e";
	keyMap["f"]						= f;
	charMap[f]						= "f";
	keyMap["g"]						= g;
	charMap[g]						= "g";
	keyMap["h"]						= h;
	charMap[h]						= "h";
	keyMap["i"]						= i;
	charMap[i]						= "i";
	keyMap["j"]						= j;
	charMap[j]						= "j";
	keyMap["k"]						= k;
	charMap[k]						= "k";
	keyMap["l"]						= l;
	charMap[l]						= "l";
	keyMap["m"]						= m;
	charMap[m]						= "m";
	keyMap["n"]						= n;
	charMap[n]						= "n";
	keyMap["o"]						= o;
	charMap[o]						= "o";
	keyMap["p"]						= p;
	charMap[p]						= "p";
	keyMap["q"]						= q;
	charMap[q]						= "q";
	keyMap["r"]						= r;
	charMap[r]						= "r";
	keyMap["s"]						= s;
	charMap[s]						= "s";
	keyMap["t"]						= t;
	charMap[t]						= "t";
	keyMap["u"]						= u;
	charMap[u]						= "u";
	keyMap["v"]						= v;
	charMap[v]						= "v";
	keyMap["w"]						= w;
	charMap[w]						= "w";
	keyMap["x"]						= x;
	charMap[x]						= "x";
	keyMap["y"]						= y;
	charMap[y]						= "y";
	keyMap["z"]						= z;
	charMap[z]						= "z";
	keyMap["BRACELEFT"]				= BRACELEFT;
	charMap[BRACELEFT]				= "{";
	keyMap["BAR"]					= BAR;
	keyMap["BRACERIGHT"]			= BRACERIGHT;
	charMap[BRACERIGHT]				= "}";
	keyMap["ASCIITILDE"]			= ASCIITILDE;
	charMap[ASCIITILDE]				= "~";

	keyMap["A"]						= A;
	charMap[A]						= "A";
	keyMap["B"]						= B;
	charMap[B]						= "B";
	keyMap["C"]						= C;
	charMap[C]						= "C";
	keyMap["D"]						= D;
	charMap[D]						= "D";
	keyMap["E"]						= E;
	charMap[E]						= "E";
	keyMap["F"]						= F;
	charMap[F]						= "F";
	keyMap["G"]						= G;
	charMap[G]						= "G";
	keyMap["H"]						= H;
	charMap[H]						= "H";
	keyMap["I"]						= I;
	charMap[I]						= "I";
	keyMap["J"]						= J;
	charMap[J]						= "J";
	keyMap["K"]						= K;
	charMap[K]						= "K";
	keyMap["L"]						= L;
	charMap[L]						= "L";
	keyMap["M"]						= M;
	charMap[M]						= "M";
	keyMap["N"]						= N;
	charMap[N]						= "N";
	keyMap["O"]						= O;
	charMap[O]						= "O";
	keyMap["P"]						= P;
	charMap[P]						= "P";
	keyMap["Q"]						= Q;
	charMap[Q]						= "Q";
	keyMap["R"]						= R;
	charMap[R]						= "R";
	keyMap["S"]						= S;
	charMap[S]						= "S";
	keyMap["T"]						= T;
	charMap[T]						= "T";
	keyMap["U"]						= U;
	charMap[U]						= "U";
	keyMap["V"]						= V;
	charMap[V]						= "V";
	keyMap["W"]						= W;
	charMap[W]						= "W";
	keyMap["X"]						= X;
	charMap[X]						= "X";
	keyMap["Y"]						= Y;
	charMap[Y]						= "Y";
	keyMap["Z"]						= Z;
	charMap[Z]						= "Z";

	shiftKeyMap["a"]				= A;
	shiftCharMap[a]					= "A";
	shiftKeyMap["b"]				= B;
	shiftCharMap[b]					= "B";
	shiftKeyMap["c"]				= C;
	shiftCharMap[c]					= "C";
	shiftKeyMap["d"]				= D;
	shiftCharMap[d]					= "D";
	shiftKeyMap["e"]				= E;
	shiftCharMap[e]					= "E";
	shiftKeyMap["f"]				= F;
	shiftCharMap[f]					= "F";
	shiftKeyMap["g"]				= G;
	shiftCharMap[g]					= "G";
	shiftKeyMap["h"]				= H;
	shiftCharMap[h]					= "H";
	shiftKeyMap["i"]				= I;
	shiftCharMap[i]					= "I";
	shiftKeyMap["j"]				= J;
	shiftCharMap[j]					= "J";
	shiftKeyMap["k"]				= K;
	shiftCharMap[k]					= "K";
	shiftKeyMap["l"]				= L;
	shiftCharMap[l]					= "L";
	shiftKeyMap["m"]				= M;
	shiftCharMap[m]					= "M";
	shiftKeyMap["n"]				= N;
	shiftCharMap[n]					= "N";
	shiftKeyMap["o"]				= O;
	shiftCharMap[o]					= "O";
	shiftKeyMap["p"]				= P;
	shiftCharMap[p]					= "P";
	shiftKeyMap["q"]				= Q;
	shiftCharMap[q]					= "Q";
	shiftKeyMap["r"]				= R;
	shiftCharMap[r]					= "R";
	shiftKeyMap["s"]				= S;
	shiftCharMap[s]					= "S";
	shiftKeyMap["t"]				= T;
	shiftCharMap[t]					= "T";
	shiftKeyMap["u"]				= U;
	shiftCharMap[u]					= "U";
	shiftKeyMap["v"]				= V;
	shiftCharMap[v]					= "V";
	shiftKeyMap["w"]				= W;
	shiftCharMap[w]					= "W";
	shiftKeyMap["x"]				= X;
	shiftCharMap[x]					= "X";
	shiftKeyMap["y"]				= Y;
	shiftCharMap[y]					= "Y";
	shiftKeyMap["z"]				= Z;
	shiftCharMap[z]					= "Z";

	return true;
}

bool Keyboard::getState(const std::string &key)
{
	auto k = translate(key);
	if (k > 0 && k < NUM_STATES)
		return states[k];
	return false;
}

void Keyboard::setState(Key key, bool state)
{
	if (key == CAPSLOCK && !states[CAPSLOCK])
		capsLock = !capsLock;

	states[key] = state;
}

void Keyboard::setState(const std::string &key, bool state)
{
	auto k = translate(key);

	if (k == CAPSLOCK && states[CAPSLOCK] != state)
		capsLock = !capsLock;

	if (k > 0 && k < NUM_STATES)
		states[k] = state;
}

Keyboard::Key Keyboard::translate(const std::string &key, bool shiftFlag)
{
	auto &mapRef = shiftFlag ? shiftKeyMap : keyMap;
	auto it = mapRef.find(key);
	if (it == mapRef.end())
		return NUM_STATES;
	return it->second;
}

std::string Keyboard::translate(Keyboard::Key key, bool shiftFlag)
{
	auto &mapRef = shiftFlag ? shiftKeyMap : keyMap;
	// Your fault, not mine
	for (auto &keyPair : mapRef)
		if (keyPair.second == key)
			return keyPair.first;

	return "Unknown";
}

std::string Keyboard::getChar(Key key, bool shiftFlag)
{
	auto &mapRef = shiftFlag ? shiftCharMap : charMap;
	auto it = mapRef.find(key);
	if (it == mapRef.end())
		return "";
	return it->second;
}

void Keyboard::addPressed(Key key)
{
	if (!states[key])
		keyPresses.push_back(key);
}

bool Keyboard::isPressed(Key key)
{
	for (Key k : keyPresses)
		if (k == key)
			return true;
	return false;
}

void Keyboard::clearPressed()
{
	keyPresses.clear();
}

