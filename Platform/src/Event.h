#pragma once

#include "Mouse.h"
#include "Keyboard.h"

namespace Platform
{
	class Event
	{
	public:
		enum class Type
		{
			MOUSE_MOVE,
			MOUSE_DOWN,
			MOUSE_UP,
			MOUSE_PRESSED,
			MOUSE_WHEEL,
			MOUSE_DRAG,

			KEY_DOWN,
			KEY_UP,
			KEY_PRESSED,

			WINDOW_RESIZED,
			WINDOW_MOVED,
			WINDOW_CLOSED,

			CUSTOM,

			NUM_TYPES
		};

	public:
		Type type;
		int x, y;
		int dx, dy;
		Mouse::Button button;
		Keyboard::Key key;
		int scroll;
		void *data;

	public:
		Event(Event::Type type = Type::CUSTOM, int x = 0, int y = 0, int dx = 0, int dy = 0, Mouse::Button button = Mouse::NUM_STATES, Keyboard::Key key = Keyboard::NUM_STATES, int scroll = 0, void *data = nullptr) : type(type), x(x), y(y), dx(dx), dy(dy), button(button), key(key), scroll(scroll), data(data) {}
	};
}
