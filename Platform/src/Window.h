#pragma once

#include "Event.h"

#include "../../Utils/src/Timer.h"


namespace Platform
{
	class Window
	{
	private:
		static Utils::Timer timer;
		static float deltaTime;
		static int width;
		static int height;

	public:
		static bool setWindowSize(Event &ev) { Window::width = ev.x; Window::height = ev.y; return false; }

		static void createWindow(int x, int y, int width, int height);
		static void destroyWindow();
		static float getDeltaTime();
		static void resetDeltaTime();
		static void processEvents();
		static bool handleEvents();
		static void swapBuffers();
		static void resize(int width, int height);

		static int getWidth() { return width; }
		static int getHeight() { return height; }
	};
}

