#pragma once

#include <string>
#include <list>
#include <map>

namespace Platform
{
	class Keyboard
	{
	public:
		enum Key
		{
			// Key symbol constants
			NONE = 0x0000,
			// ASCII commands
			BACKSPACE			= 0xff08,
			TAB					= 0xff09,
			LINEFEED			= 0xff0a,
			CLEAR				= 0xff0b,
			ENTER				= 0xff0d,
			PAUSE				= 0xff13,
			SCROLLLOCK			= 0xff14,
			SYSREQ				= 0xff15,
			ESCAPE				= 0xff1b,
			//SPACE				= 0xff20,

			// Cursor control and motion
			HOME				= 0xff50,
			LEFT				= 0xff51,
			UP					= 0xff52,
			RIGHT				= 0xff53,
			DOWN				= 0xff54,
			PAGEUP				= 0xff55,
			PAGEDOWN			= 0xff56,
			END					= 0xff57,
			BEGIN				= 0xff58,
	#if 0
			// Misc functions
			DELETE				= 0xffff,
			SELECT				= 0xff60,
			PRINT				= 0xff61,
			EXECUTE				= 0xff62,
			INSERT				= 0xff63,
			UNDO				= 0xff65,
			REDO				= 0xff66,
			MENU				= 0xff67,
			FIND				= 0xff68,
			CANCEL				= 0xff69,
			HELP				= 0xff6a,
			BREAK				= 0xff6b,
			MODESWITCH			= 0xff7e,
			SCRIPTSWITCH		= 0xff7e,
	#endif
			// Number pad
			NUMPADLOCK			= 0xff7f,
			NUMPAD_SPACE		= 0xff80,
			NUMPAD_TAB			= 0xff89,
			NUMPAD_ENTER		= 0xff8d,
			NUMPAD_F1			= 0xff91,
			NUMPAD_F2			= 0xff92,
			NUMPAD_F3			= 0xff93,
			NUMPAD_F4			= 0xff94,
			NUMPAD_HOME			= 0xff95,
			NUMPAD_LEFT			= 0xff96,
			NUMPAD_UP			= 0xff97,
			NUMPAD_RIGHT		= 0xff98,
			NUMPAD_DOWN			= 0xff99,
			NUMPAD_PRIOR		= 0xff9a,
			NUMPAD_PAGE_UP		= 0xff9a,
			NUMPAD_NEXT			= 0xff9b,
			NUMPAD_PAGE_DOWN	= 0xff9b,
			NUMPAD_END			= 0xff9c,
			NUMPAD_BEGIN		= 0xff9d,
			NUMPAD_INSERT		= 0xff9e,
			NUMPAD_DELETE		= 0xff9f,
			NUMPAD_EQUAL		= 0xffbd,
			NUMPAD_MULTIPLY		= 0xffaa,
			NUMPAD_PLUS			= 0xffab,
			NUMPAD_SEPARATOR	= 0xffac,
			NUMPAD_MINUS		= 0xffad,
			NUMPAD_DECIMAL		= 0xffae,
			NUMPAD_DIVIDE		= 0xffaf,

			NUMPAD_0			= 0xffb0,
			NUMPAD_1			= 0xffb1,
			NUMPAD_2			= 0xffb2,
			NUMPAD_3			= 0xffb3,
			NUMPAD_4			= 0xffb4,
			NUMPAD_5			= 0xffb5,
			NUMPAD_6			= 0xffb6,
			NUMPAD_7			= 0xffb7,
			NUMPAD_8			= 0xffb8,
			NUMPAD_9			= 0xffb9,

			// Function keys
			F1					= 0xffbe,
			F2					= 0xffbf,
			F3					= 0xffc0,
			F4					= 0xffc1,
			F5					= 0xffc2,
			F6					= 0xffc3,
			F7					= 0xffc4,
			F8					= 0xffc5,
			F9					= 0xffc6,
			F10					= 0xffc7,
			F11					= 0xffc8,
			F12					= 0xffc9,
			F13					= 0xffca,
			F14					= 0xffcb,
			F15					= 0xffcc,
			F16					= 0xffcd,

			// Modifiers
			LSHIFT				= 0xffe1,
			RSHIFT				= 0xffe2,
			LCTRL				= 0xffe3,
			RCTRL				= 0xffe4,
			CAPSLOCK			= 0xffe5,
			LMETA				= 0xffe7,
			RMETA				= 0xffe8,
			LALT				= 0xffe9,
			RALT				= 0xffea,
			LWINDOWS			= 0xffeb,
			RWINDOWS			= 0xffec,
			LCOMMAND			= 0xffed,
			RCOMMAND			= 0xffee,
			LOPTION				= 0xffd0,
			ROPTION				= 0xffd1,

			// Latin-1
			SPACE				= 0x020,
			EXCLAMATION			= 0x021,
			DOUBLEQUOTE			= 0x022,
			HASH				= 0x023,
			POUND				= 0x023,  // synonym
			DOLLAR				= 0x024,
			PERCENT				= 0x025,
			AMPERSAND			= 0x026,
			APOSTROPHE			= 0x027,
			PARENLEFT			= 0x028,
			PARENRIGHT			= 0x029,
			ASTERISK			= 0x02a,
			PLUS				= 0x02b,
			COMMA				= 0x02c,
			MINUS				= 0x02d,
			PERIOD				= 0x02e,
			SLASH				= 0x02f,
			KEY_0				= 0x030,
			KEY_1				= 0x031,
			KEY_2				= 0x032,
			KEY_3				= 0x033,
			KEY_4				= 0x034,
			KEY_5				= 0x035,
			KEY_6				= 0x036,
			KEY_7				= 0x037,
			KEY_8				= 0x038,
			KEY_9				= 0x039,
			COLON				= 0x03a,
			SEMICOLON			= 0x03b,
			LESS				= 0x03c,
			EQUAL				= 0x03d,
			GREATER				= 0x03e,
			QUESTION			= 0x03f,
			AT					= 0x040,
			BRACKETLEFT			= 0x05b,
			BACKSLASH			= 0x05c,
			BRACKETRIGHT		= 0x05d,
			ASCIICIRCUM			= 0x05e,
			HAT					= 0x05e,  // synonym
			UNDERSCORE			= 0x05f,
			GRAVE				= 0x060,
			QUOTELEFT			= 0x060,
			a					= 0x061,
			b					= 0x062,
			c					= 0x063,
			d					= 0x064,
			e					= 0x065,
			f					= 0x066,
			g					= 0x067,
			h					= 0x068,
			i					= 0x069,
			j					= 0x06a,
			k					= 0x06b,
			l					= 0x06c,
			m					= 0x06d,
			n					= 0x06e,
			o					= 0x06f,
			p					= 0x070,
			q					= 0x071,
			r					= 0x072,
			s					= 0x073,
			t					= 0x074,
			u					= 0x075,
			v					= 0x076,
			w					= 0x077,
			x					= 0x078,
			y					= 0x079,
			z					= 0x07a,
			BRACELEFT			= 0x07b,
			BAR					= 0x07c,
			BRACERIGHT			= 0x07d,
			ASCIITILDE			= 0x07e,

			A					= 0x041,
			B					= 0x042,
			C					= 0x043,
			D					= 0x044,
			E					= 0x045,
			F					= 0x046,
			G					= 0x047,
			H					= 0x048,
			I					= 0x049,
			J					= 0x04a,
			K					= 0x04b,
			L					= 0x04c,
			M					= 0x04d,
			N					= 0x04e,
			O					= 0x04f,
			P					= 0x050,
			Q					= 0x051,
			R					= 0x052,
			S					= 0x053,
			T					= 0x054,
			U					= 0x055,
			V					= 0x056,
			W					= 0x057,
			X					= 0x058,
			Y					= 0x059,
			Z					= 0x05a,

			NUM_STATES			= 256*256,
			ANY
		};

	private:
		// Stores the state of each key (true for down, false for up).
		static bool states[NUM_STATES];

		// Map of key enums to strings.
		static std::map<std::string, Key> keyMap;
		static std::map<std::string, Key> shiftKeyMap;

		// For getting text from keys.
		static std::map<Key, std::string> charMap;
		static std::map<Key, std::string> shiftCharMap;

		static std::list<Key> keyPresses;

		static bool capsLock;

	public:
		// Initialises all key states to down, called automatically.
		static bool init();

		// Check for shift down or capslock on.
		static bool isShift() { return states[LSHIFT] || states[RSHIFT]; }
		static bool isCaps() { return capsLock; }

		// Returns the current state of the given key (true for down, false for up).
		static bool getState(Key key) { return states[key]; }
		static bool getState(const std::string &key);
		static bool isDown(Key key) { return getState(key); }
		static bool isDown(const std::string &key) { return getState(key); }

		// Sets the state of the given key, called automatically.
		static void setState(Key key, bool state);
		static void setState(const std::string &key, bool state);
		
		static void addPressed(Key key);
		static bool isPressed(Key key);
		static void clearPressed();

		// Returns a key enum matching the given key string, returns NUM_STATES if the key string is invalid.
		static Key translate(const std::string &key, bool shiftFlag = false);

		// Returns a key string matching the given key enum, returns Unknown if the given key enum is invalid.
		static std::string translate(Key key, bool shiftFlag = false);

		// Returns the character for the given key.
		static std::string getChar(Key key, bool shiftFlag = false);
	};
}
