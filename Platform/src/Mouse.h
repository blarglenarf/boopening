#pragma once

#include <string>
#include <list>

namespace Platform
{
	class Mouse
	{
	public:
		enum Button
		{
			LEFT,
			MIDDLE,
			RIGHT,

			NUM_STATES
		};

	private:
		static bool states[NUM_STATES];
			
		static std::list<Button> keyPresses;

	public:
		static int x;
		static int y;
		static int dx;
		static int dy;
		static int scroll;
		static int dScroll;
		
		// Initialises all mouse states to down, called automatically.
		static bool init();

		// Returns the current state of the given mouse button (true for down, false for up).
		static bool getState(Button button) { return states[button]; }
		static bool getState(const std::string &button);
		static bool isDown(Button button) { return getState(button); }
		static bool isDown(const std::string &button) { return getState(button); }

		// Sets the state of the given mouse button, called automatically.
		static void setState(Button button, bool state) { states[button] = state; }
		static void setState(const std::string &button, bool state);
		
		static void addPressed(Button key);
		static bool isPressed(Button key);
		static void clearPressed();

		static void addScrolled(int scroll);
		static bool isScrolled();
		static int getScrolled();
		static void clearScrolled();

		// Returns a button enum matching the given button string, returns NUM_STATES if the button string is invalid.
		static Button translate(const std::string &button);

		// Returns a string matching the given button enum, returns Unknown if the button enum is invalid.
		static std::string translate(Button button);
	};
}
