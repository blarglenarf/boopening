#include "Mouse.h"

#include "../../Utils/src/StringUtils.h"

using namespace Platform;

bool Mouse::states[Mouse::NUM_STATES];

std::list<Mouse::Button> Mouse::keyPresses;

int Mouse::x = 0;
int Mouse::y = 0;
int Mouse::dx = 0;
int Mouse::dy = 0;
int Mouse::scroll = 0;
int Mouse::dScroll = 0;

bool mouseB = Mouse::init();


bool Mouse::init()
{
	for (int i = 0; i < Mouse::NUM_STATES; i++)
		states[i] = false;
	return true;
}

bool Mouse::getState(const std::string &button)
{
	auto k = translate(button);
	if (k > 0 && k < NUM_STATES)
		return states[k];
	return false;
}

void Mouse::setState(const std::string &button, bool state)
{
	auto k = translate(button);
	if (k > 0 && k < NUM_STATES)
		states[k] = state;
}

Mouse::Button Mouse::translate(const std::string &button)
{
	if (Utils::compareIgnoreCase(button, "left"))
		return Mouse::LEFT;
	if (Utils::compareIgnoreCase(button, "right"))
		return Mouse::RIGHT;
	if (Utils::compareIgnoreCase(button, "middle"))
		return Mouse::MIDDLE;

	return Mouse::NUM_STATES;
}

std::string Mouse::translate(Mouse::Button button)
{
	switch (button)
	{
	case LEFT:
		return "left";
	case RIGHT:
		return "right";
	case MIDDLE:
		return "middle";
	default:
		return "unkown";
	}
}

void Mouse::addPressed(Button key)
{
	if (!states[key])
		keyPresses.push_back(key);
}

bool Mouse::isPressed(Button key)
{
	for (Button k : keyPresses)
		if (k == key)
			return true;
	return false;
}

void Mouse::clearPressed()
{
	keyPresses.clear();
}

void Mouse::addScrolled(int scroll)
{
	dScroll = scroll;
	scroll += dScroll;
}

bool Mouse::isScrolled()
{
	return dScroll != 0;
}

int Mouse::getScrolled()
{
	return dScroll;
}

void Mouse::clearScrolled()
{
	dScroll = 0;
}