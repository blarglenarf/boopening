#pragma once

#if _LINUX

#include <X11/XKBlib.h>
#include <X11/extensions/xf86vmode.h>
#include <GL/glx.h>

namespace Platform
{
	class PlatformWindow
	{
	private:
		struct GLWindow
		{
			Display *display;
			int screen;
			Window win;
			GLXContext context;
			XSetWindowAttributes attr;
			XF86VidModeModeInfo deskMode;
			int x, y;
			unsigned int width, height, depth;
		};

	private:
		static GLWindow GLWin;

	private:
		PlatformWindow();

	public:
		static void createWindow(int x, int y, int width, int height);
		static void destroyWindow();
		static void swapBuffers();
		static void processEvents();
		static void resize(int width, int height);
	};
}

#endif

