#if _LINUX

#include "LinEvent.h"
//#include "LinWindow.h"

#include "../Event.h"
#include "../EventHandler.h"
#include "../Window.h"
#include "../Mouse.h"
#include "../Keyboard.h"

using namespace Platform;

void linEventProc(XEvent &ev)
{
	static int lastX = 0, lastY = 0;
	int x = 0, y = 0;
	Mouse::Button button;
	unsigned int key = 0;

	switch (ev.type)
	{
	case MotionNotify:
		x = ev.xmotion.x;
		y = Platform::Window::getHeight() - ev.xmotion.y;
		EventHandler::events.push_back(Event(Event::Type::MOUSE_MOVE, x, y, x - lastX, y - lastY));
		if (Mouse::getState(Mouse::LEFT) || Mouse::getState(Mouse::RIGHT) || Mouse::getState(Mouse::MIDDLE))
			EventHandler::events.push_back(Event(Event::Type::MOUSE_DRAG, x, y, x - lastX, y - lastY, Mouse::getState(Mouse::LEFT) ? Mouse::LEFT : Mouse::getState(Mouse::RIGHT) ? Mouse::RIGHT : Mouse::MIDDLE));
		Mouse::x = x;
		Mouse::y = y;
		Mouse::dx += x - lastX;
		Mouse::dy += y - lastY;
		lastX = x;
		lastY = y;
		break;
		#if 0
		static int scroll;
		int actualScrollValue;

		scroll += GET_WHEEL_DELTA_WPARAM(wParam);
		actualScrollValue = scroll / WHEEL_DELTA;
		scroll %= WHEEL_DELTA;
		Mouse::addScrolled(actualScrollValue);
		EventHandler::events.push_back(Event(Event::Type::MOUSE_WHEEL, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, Keyboard::NUM_STATES, actualScrollValue, nullptr));
		break;
		#endif

	case ButtonPress:
		button = (Mouse::Button)(ev.xbutton.button - 1);
		if (button == 3 || button == 4)
		{
			// begin scroll event
			// TODO: something better than this
			int actualScrollValue = button == 3 ? 1 : -1;
			
			Mouse::addScrolled(actualScrollValue);
			EventHandler::events.push_back(Event(Event::Type::MOUSE_WHEEL, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, Keyboard::NUM_STATES, actualScrollValue, nullptr));
			break;
		}
		x = ev.xmotion.x;
		y = Platform::Window::getHeight() - ev.xmotion.y;
		Mouse::addPressed(button);
		Mouse::setState(button, true);
		if (Mouse::isPressed(button))
			EventHandler::events.push_back(Event(Event::Type::MOUSE_PRESSED, x, y, x - lastX, y - lastY, button));
		EventHandler::events.push_back(Event(Event::Type::MOUSE_DOWN, x, y, x - lastX, y - lastY, button));
		break;

	case ButtonRelease:
		button = (Mouse::Button)(ev.xbutton.button - 1);
		if (button == 3 || button == 4)
		{
			// end scroll event
			break;
		}
		x = ev.xmotion.x;
		y = Platform::Window::getHeight() - ev.xmotion.y;
		Mouse::setState(button, false);
		EventHandler::events.push_back(Event(Event::Type::MOUSE_UP, x, y, x - lastX, y - lastY, button));
		break;

	case KeyPress:
		key = XLookupKeysym(&ev.xkey, 0);
		Keyboard::addPressed((Keyboard::Key) key);
		Keyboard::setState((Keyboard::Key) key, true);
		if (Keyboard::isPressed((Keyboard::Key) key))
			EventHandler::events.push_back(Event(Event::Type::KEY_PRESSED, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, (Keyboard::Key) key));
		EventHandler::events.push_back(Event(Event::Type::KEY_DOWN, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, (Keyboard::Key) key));
		break;

	case KeyRelease:
		key = XLookupKeysym(&ev.xkey, 0);
		Keyboard::setState((Keyboard::Key) key, false);
		EventHandler::events.push_back(Event(Event::Type::KEY_UP, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, (Keyboard::Key) key));
		break;

	case ConfigureNotify:
		// TODO: only send the event at the end
		EventHandler::events.push_back(Event(Event::Type::WINDOW_RESIZED, ev.xconfigure.width, ev.xconfigure.height));
		break;

	case ClientMessage:
		//if (*XGetAtomName(GLWin->display, event.xclient.message_type) ==  *"WM_PROTOCOLS")
		EventHandler::events.push_back(Event(Event::Type::WINDOW_CLOSED, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy));
		break;

	default:
		break;
	}
}

#endif

