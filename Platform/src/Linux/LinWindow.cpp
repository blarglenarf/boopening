#if _LINUX

#include "LinWindow.h"
#include "LinEvent.h"

#include <GL/gl.h>
#include <stdexcept>

using namespace Platform;

PlatformWindow::GLWindow PlatformWindow::GLWin;

std::string title = "linuxapp";

static int attrList[] = {
	GLX_RGBA,
	GLX_DOUBLEBUFFER,
	GLX_RED_SIZE, 8,
	GLX_GREEN_SIZE, 8,
	GLX_BLUE_SIZE, 8,
	GLX_DEPTH_SIZE, 24,
	GLX_STENCIL_SIZE, 8,
	None
};


void PlatformWindow::createWindow(int x, int y, int width, int height)
{
	// Get a connection to the x server.
	GLWin.display = XOpenDisplay(0);
	if (!GLWin.display)
		throw std::runtime_error("Failed to open X Display");
	GLWin.screen = DefaultScreen(GLWin.display);

	// For fullscreen stuff.
#if 0
	//int modeNum;
	//int vidModeMajorVersion, vidModeMinorVersion;
	//XF86VidModeModeInfo **modes;
	//int bestMode = 0;
	XF86VidModeQueryVersion(GLWin.display, &vidModeMajorVersion, &vidModeMinorVersion);
	XF86VidModeGetAllModeLines(GLWin.display, GLWin.screen, &modeNum, &modes);
	GLWin.deskMode = *modes[0];*/

	// Get mode with requested resolution (only useful if fullscreening).
	for (int i = 0; i < modeNum; i++)
	{
		if ((modes[i]->hdisplay == GameWindow::defaultWidth()) && (modes[i]->vdisplay == GameWindow::defaultHeight()))
		{
			bestMode = i;
		}
	}
#endif

	// Create glx visual.
	XVisualInfo *vi = glXChooseVisual(GLWin.display, GLWin.screen, attrList);
	if (!vi)
		throw std::runtime_error("Failed to create double buffered glx visual");
	int glxMajorVersion, glxMinorVersion;
	glXQueryVersion(GLWin.display, &glxMajorVersion, &glxMinorVersion);

	// Create glx context.
	GLWin.context = glXCreateContext(GLWin.display, vi, 0, GL_TRUE);
	Colormap cmap = XCreateColormap(GLWin.display, RootWindow(GLWin.display, vi->screen), vi->visual, AllocNone);
	GLWin.attr.colormap = cmap;
	GLWin.attr.border_pixel = 0;

	// Create window.
	GLWin.attr.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask;
	GLWin.win = XCreateWindow(GLWin.display, RootWindow(GLWin.display, vi->screen), x, y, width, height, 0, vi->depth, InputOutput, vi->visual, CWBorderPixel | CWColormap | CWEventMask, &GLWin.attr);
	Atom wmDelete = XInternAtom(GLWin.display, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(GLWin.display, GLWin.win, &wmDelete, 1);
	XSetStandardProperties(GLWin.display, GLWin.win, title.c_str(), title.c_str(), None, NULL, 0, NULL);
	XMapRaised(GLWin.display, GLWin.win);

	XkbSetDetectableAutoRepeat(GLWin.display, 1, 0); //... a good window.

	// Connect the glx context to the window.
	Window winDummy;
	unsigned int borderDummy;
	glXMakeCurrent(GLWin.display, GLWin.win, GLWin.context);
	XGetGeometry(GLWin.display, GLWin.win, &winDummy, &GLWin.x, &GLWin.y, &GLWin.width, &GLWin.height, &borderDummy, &GLWin.depth);
	if (!glXIsDirect(GLWin.display, GLWin.context))
		throw std::runtime_error("No direct rendering possible");

	XFree(vi);
}

void PlatformWindow::destroyWindow()
{
	if (GLWin.context)
	{
		if (!glXMakeCurrent(GLWin.display, None, NULL))
			throw std::runtime_error("Could not release drawing context");
		glXDestroyContext(GLWin.display, GLWin.context);
		GLWin.context = NULL;
	}
	XCloseDisplay(GLWin.display);
}

void PlatformWindow::swapBuffers()
{
	glXSwapBuffers(GLWin.display, GLWin.win);
}

void PlatformWindow::processEvents()
{
	XEvent ev;
	while (XPending(GLWin.display) > 0)
	{
		XNextEvent(GLWin.display, &ev);
		linEventProc(ev);
	}
}

void PlatformWindow::resize(int width, int height)
{
	XWindowAttributes xwa;
	XGetWindowAttributes(GLWin.display, GLWin.win, &xwa);
	XMoveResizeWindow(GLWin.display, GLWin.win, xwa.x, xwa.y, width, height);
}

#endif

