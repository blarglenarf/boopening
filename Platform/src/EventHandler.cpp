#include "EventHandler.h"
#include "Win32/WinWindow.h"
#include "Linux/LinWindow.h"

using namespace Platform;

std::vector<Event> EventHandler::events;
std::vector<EventHandlerGroup> EventHandler::handlerGroups;


void EventHandlerGroup::handleAddQueue()
{
	if (addQueue.empty())
		return;
	for (size_t i = 0; i < addQueue.size(); i++)
		eventHandlers[addQueue[i].first].push_back(addQueue[i].second);
	addQueue.clear();
}

void EventHandlerGroup::handleRemoveQueue()
{
	if (removeQueue.empty())
		return;
	for (size_t i = 0; i < removeQueue.size(); i++)
	{
		auto &handlerType = eventHandlers[removeQueue[i].first];
		for (auto it = handlerType.begin(); it != handlerType.end(); )
		{
			if (it->id == removeQueue[i].second)
				it = handlerType.erase(it);
			else
				it++;
		}
	}
	removeQueue.clear();
}

size_t EventHandlerGroup::addHandler(Event::Type type, std::function<bool (Event &ev)> handler)
{
	addQueue.push_back({type, {maxHandlerID, handler}});
	return maxHandlerID++;
}

void EventHandlerGroup::removeHandler(Event::Type type, size_t id)
{
	removeQueue.push_back({type, id});
}


void EventHandler::processEvents()
{
	events.clear();
	
	Mouse::dx = 0;
	Mouse::dy = 0;
	Mouse::dScroll = 0;
	Mouse::clearPressed();
	Mouse::clearScrolled();
	Keyboard::clearPressed();

	PlatformWindow::processEvents();
}

bool EventHandler::handleEvents()
{
	processEvents();

	for (size_t i = 0; i < handlerGroups.size(); i++)
	{
		handlerGroups[i].handleAddQueue();
		handlerGroups[i].handleRemoveQueue();
	}

	// Performance is pretty critical, iterate the old fashioned way
	for (size_t i = 0; i < events.size(); i++)
	{
		auto &ev = events[i];
		if (ev.type == Event::Type::WINDOW_CLOSED)
			return true;

		bool handled = false;
		for (size_t i = 0; i < handlerGroups.size(); i++)
		{
			// TODO: something faster than this
			for (auto &handler : handlerGroups[i].eventHandlers[ev.type])
			{
				bool b = handler.func(ev);
				if (b)
					handled = true;
			}
			if (handled)
				break;
		}
	}

	return false;
}

EventHandlerGroup &EventHandler::addHandlerGroup()
{
	handlerGroups.push_back(EventHandlerGroup(handlerGroups.size()));
	return handlerGroups[handlerGroups.size() - 1];
}

void EventHandler::removeHandlerGroup(size_t pos)
{
	handlerGroups.erase(handlerGroups.begin() + pos);
	for (size_t i = pos; i < handlerGroups.size(); i++)
		handlerGroups[i].groupID = i;
}

