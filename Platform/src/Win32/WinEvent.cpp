#if _WIN32

#include "WinEvent.h"
#include "KeyTables.h"

#include "../Event.h"
#include "../EventHandler.h"
#include "../Window.h"
#include "../Mouse.h"
#include "../Keyboard.h"

using namespace Platform;

#define GETX(l) (int(l & 0xFFFF))
#define GETY(l) (Window::getHeight() - (int(l) >> 16))

static Keyboard::Key convertKey(WPARAM wParam)
{
	Keyboard::Key symbol = keymap(wParam);
	if (symbol == Keyboard::NONE)
	{
		char ch = (char) MapVirtualKey((UINT) wParam, MAPVK_VK_TO_CHAR);
		if (ch)
			symbol = chmap(ch);
	}

	if (symbol >= Keyboard::A && symbol <= Keyboard::Z)
	{
		// if shift isn't down, convert the key to lowercase
		if (!(GetKeyState(VK_SHIFT) & 0xff00))
			symbol = (Keyboard::Key)(symbol + Keyboard::a - Keyboard::A);
	}

	return symbol;
}


LRESULT CALLBACK wndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int pressed = 0;
	static int lastX = 0, lastY = 0;
	static int scroll = 0;
	int x = 0, y = 0;
	int actualScrollValue = 0;
	RECT rect;
	//PAINTSTRUCT ps;

	switch (message)
	{
		// FIXME: not sure if this is necessary
	/*case WM_PAINT:
		if (GetUpdateRect(hWnd, &rect, FALSE))
		{
			BeginPaint(hWnd, &ps);
			EndPaint(hWnd, &ps);
		}
		break;*/

	case WM_MOUSEMOVE:
		x = GETX(lParam);
		y = GETY(lParam);
		EventHandler::events.push_back(Event(Event::Type::MOUSE_MOVE, x, y, x - lastX, y - lastY));
		if (Mouse::getState(Mouse::LEFT) || Mouse::getState(Mouse::RIGHT) || Mouse::getState(Mouse::MIDDLE))
			EventHandler::events.push_back(Event(Event::Type::MOUSE_DRAG, x, y, x - lastX, y - lastY, Mouse::getState(Mouse::LEFT) ? Mouse::LEFT : Mouse::getState(Mouse::RIGHT) ? Mouse::RIGHT : Mouse::MIDDLE));
		Mouse::x = x;
		Mouse::y = y;
		Mouse::dx += x - lastX;
		Mouse::dy += y - lastY;
		lastX = x;
		lastY = y;
		break;

	case WM_LBUTTONDOWN:
		x = GETX(lParam);
		y = GETY(lParam);
		Mouse::addPressed(Mouse::LEFT);
		Mouse::setState(Mouse::LEFT, true);
		if (Mouse::isPressed(Mouse::LEFT))
			EventHandler::events.push_back(Event(Event::Type::MOUSE_PRESSED, x, y, x - lastX, y - lastY, Mouse::LEFT));
		EventHandler::events.push_back(Event(Event::Type::MOUSE_DOWN, x, y, x - lastX, y - lastY, Mouse::LEFT));
		break;

	case WM_LBUTTONUP:
		x = GETX(lParam);
		y = GETY(lParam);
		Mouse::setState(Mouse::LEFT, false);
		EventHandler::events.push_back(Event(Event::Type::MOUSE_UP, x, y, x - lastX, y - lastY, Mouse::LEFT));
		break;

	case WM_RBUTTONDOWN:
		x = GETX(lParam);
		y = GETY(lParam);
		Mouse::addPressed(Mouse::RIGHT);
		Mouse::setState(Mouse::RIGHT, true);
		if (Mouse::isPressed(Mouse::RIGHT))
			EventHandler::events.push_back(Event(Event::Type::MOUSE_PRESSED, x, y, x - lastX, y - lastY, Mouse::RIGHT));
		EventHandler::events.push_back(Event(Event::Type::MOUSE_DOWN, x, y, x - lastX, y - lastY, Mouse::RIGHT));
		break;

	case WM_RBUTTONUP:
		x = GETX(lParam);
		y = GETY(lParam);
		Mouse::setState(Mouse::RIGHT, false);
		EventHandler::events.push_back(Event(Event::Type::MOUSE_UP, x, y, x - lastX, y - lastY, Mouse::RIGHT));
		break;

	case WM_MBUTTONDOWN:
		x = GETX(lParam);
		y = GETY(lParam);
		Mouse::addPressed(Mouse::MIDDLE);
		Mouse::setState(Mouse::MIDDLE, true);
		if (Mouse::isPressed(Mouse::MIDDLE))
			EventHandler::events.push_back(Event(Event::Type::MOUSE_PRESSED, x, y, x - lastX, y - lastY, Mouse::MIDDLE));
		EventHandler::events.push_back(Event(Event::Type::MOUSE_DOWN, x, y, x - lastX, y - lastY, Mouse::MIDDLE));
		break;

	case WM_MBUTTONUP:
		x = GETX(lParam);
		y = GETY(lParam);
		Mouse::setState(Mouse::MIDDLE, false);
		EventHandler::events.push_back(Event(Event::Type::MOUSE_UP, x, y, x - lastX, y - lastY, Mouse::MIDDLE));
		break;

	case WM_MOUSEWHEEL:
		scroll += GET_WHEEL_DELTA_WPARAM(wParam);
		actualScrollValue = scroll / WHEEL_DELTA;
		scroll %= WHEEL_DELTA;
		Mouse::addScrolled(actualScrollValue);
		EventHandler::events.push_back(Event(Event::Type::MOUSE_WHEEL, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, Keyboard::NUM_STATES, actualScrollValue, nullptr));
		break;

		// FIXME: do these three cases cause memory leaks? Investigate!
	//case WM_SIZE:
	//	break;
	//case WM_SIZING:
	//	break;
	case WM_SIZE:
		if (wParam == SIZE_MAXIMIZED || wParam == SIZE_RESTORED)
		{
			GetClientRect(hWnd, &rect);
			EventHandler::events.push_back(Event(Event::Type::WINDOW_RESIZED, rect.right - rect.left, rect.bottom - rect.top));
		}
		break;

		// Only send one resize message, after resizing has finished
	case WM_EXITSIZEMOVE:
		GetClientRect(hWnd, &rect);
		EventHandler::events.push_back(Event(Event::Type::WINDOW_RESIZED, rect.right - rect.left, rect.bottom - rect.top));
		break;

	case WM_MOVE:
		EventHandler::events.push_back(Event(Event::Type::WINDOW_MOVED, LOWORD(lParam), HIWORD(lParam)));
		break;

	case WM_KEYDOWN:
		// only send key when the key is initially pushed and ignore repeats
		if (!(lParam & (1 << 30)))
		{
			Keyboard::addPressed(convertKey(wParam));
			Keyboard::setState(convertKey(wParam), true);
			EventHandler::events.push_back(Event(Event::Type::KEY_PRESSED, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, convertKey(wParam)));
		}
		EventHandler::events.push_back(Event(Event::Type::KEY_DOWN, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, convertKey(wParam)));
		break;

	case WM_KEYUP:
		Keyboard::setState(convertKey(wParam), false);
		EventHandler::events.push_back(Event(Event::Type::KEY_UP, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, convertKey(wParam)));
		break;

	case WM_SYSKEYDOWN:
		// only send key when the key is initially pushed and ignore repeats
		if (!(lParam & (1 << 30)))
		{
			Keyboard::addPressed((Keyboard::Key) wParam);
			Keyboard::setState((Keyboard::Key) wParam, true);
			EventHandler::events.push_back(Event(Event::Type::KEY_PRESSED, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, (Keyboard::Key) wParam));
		}
		EventHandler::events.push_back(Event(Event::Type::KEY_DOWN, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, (Keyboard::Key) wParam));
		break;

	case WM_SYSKEYUP:
		Keyboard::setState((Keyboard::Key) wParam, false);
		EventHandler::events.push_back(Event(Event::Type::KEY_UP, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy, Mouse::NUM_STATES, (Keyboard::Key) wParam));
		break;

	case WM_QUIT:
		EventHandler::events.push_back(Event(Event::Type::WINDOW_CLOSED, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy));
		PostQuitMessage(0); // FIXME: not sure if this is necessary
		exit(EXIT_SUCCESS);
		break;

	case WM_DESTROY:
		EventHandler::events.push_back(Event(Event::Type::WINDOW_CLOSED, Mouse::x, Mouse::y, Mouse::dx, Mouse::dy));
		PostQuitMessage(0); // FIXME: not sure if this is necessary
		exit(EXIT_SUCCESS);
		break;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}

	return DefWindowProc(hWnd, message, wParam, lParam);;
}

#endif

