#pragma once

#if _WIN32

#include <string>
#include <Windows.h>

namespace Platform
{
	class PlatformWindow
	{
	private:
		static HINSTANCE hInstance;
		static HWND hWnd;
		static HDC hDC;
		static HGLRC hRC;
		static HACCEL accelTable;

	private:
		PlatformWindow();

	public:
		static void createWindow(int x, int y, int width, int height);
		static void destroyWindow();
		static void swapBuffers();
		static void processEvents();
		static void resize(int width, int height);
	};
}

#endif

