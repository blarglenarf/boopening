#if _WIN32

#include "WinWindow.h"
#include "WinEvent.h"

#include <tchar.h>
#include <GL/GL.h>
#include <stdexcept>

using namespace Platform;

HINSTANCE PlatformWindow::hInstance;
HWND PlatformWindow::hWnd;
HDC PlatformWindow::hDC;
HGLRC PlatformWindow::hRC;
HACCEL PlatformWindow::accelTable;

std::wstring szWindowClass = _T("win32app");


void PlatformWindow::createWindow(int x, int y, int width, int height)
{
	hInstance = GetModuleHandle(NULL);;

	std::wstring wTitle = _T("Default Title");

	// Register the window class
	WNDCLASSEX wcex;
	wcex.cbSize = sizeof(WNDCLASSEX);
	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = wndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = NULL;// (HBRUSH) (COLOR_WINDOW + 1);
	wcex.lpszMenuName = 0;
	wcex.lpszClassName = szWindowClass.c_str();
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_APPLICATION));

	// Make sure it registered
	if (!RegisterClassEx(&wcex))
		throw std::runtime_error("Failed to register window class");

	// Create the window
	RECT rc = { x, y, width + x, height + y };

	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);
	hWnd = CreateWindow(szWindowClass.c_str(), wTitle.c_str(), WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
						rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, NULL);

	// Make sure it was created
	if (!IsWindow(hWnd))
		throw std::runtime_error("Failed to create window");

	ShowWindow(hWnd, SW_SHOW);
	UpdateWindow(hWnd);

	// Setup OpenGL related stuff, start with the pixel format
	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32; // Pretty sure this is the number of colour bits excluding alpha, but not entirely sure.
	pfd.cDepthBits = 24;
	pfd.cStencilBits = 8;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.iLayerType = PFD_MAIN_PLANE;

	// Create OpenGL context
	hDC = GetDC(hWnd);
	if (!hDC)
		throw std::runtime_error("Failed to get device context for intialising OpenGL");

	GLuint pixelFormat = ChoosePixelFormat(hDC, &pfd);
	if (!pixelFormat)
		throw std::runtime_error("Failed to create pixel format");
	if (!SetPixelFormat(hDC, pixelFormat, &pfd))
		throw std::runtime_error("Failed to set pixel format");

	hRC = wglCreateContext(hDC);
	if (!hRC)
		throw std::runtime_error("Failed to create OpenGL context");
	if (!wglMakeCurrent(hDC, hRC))
		throw std::runtime_error("Failed to activate OpenGL context");

	// Create the event acceleration table
	accelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDI_APPLICATION));
}

void PlatformWindow::destroyWindow()
{
	if (hRC)
	{
		wglMakeCurrent(0, 0);
		wglDeleteContext(hRC);
		hRC = 0;
	}

	ReleaseDC(hWnd, hDC);
	DestroyWindow(hWnd);
	UnregisterClass(szWindowClass.c_str(), hInstance);
}

void PlatformWindow::swapBuffers()
{
	SwapBuffers(hDC);
}

void PlatformWindow::processEvents()
{
	MSG msg;

	while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
	{
		if (!TranslateAccelerator(msg.hwnd, accelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}

void PlatformWindow::resize(int width, int height)
{
	RECT rect;
	GetWindowRect(hWnd, &rect);

	MoveWindow(hWnd, rect.left, rect.top, width, height, TRUE);
}

#endif

