#pragma once

#if _WIN32

#include <Windows.h>

#include "../Keyboard.h"

// From http://code.google.com/p/pyglet/source/browse/trunk/pyglet/libs/win32/winkey.py
// Thanks Alex!

namespace Platform
{
	Keyboard::Key keymap(WPARAM wParam)
	{
		switch(wParam)
		{
			// These case labels conflict with other VK cases.
#if 0
		case 'a': return Keyboard::a;
		case 'b': return Keyboard::b;
		case 'c': return Keyboard::c;
		case 'd': return Keyboard::d;
		case 'e': return Keyboard::e;
		case 'f': return Keyboard::f;
		case 'g': return Keyboard::g;
		case 'h': return Keyboard::h;
		case 'i': return Keyboard::i;
		case 'j': return Keyboard::j;
		case 'k': return Keyboard::k;
		case 'l': return Keyboard::l;
		case 'm': return Keyboard::m;
		case 'n': return Keyboard::n;
		case 'o': return Keyboard::o;
		case 'p': return Keyboard::p;
		case 'q': return Keyboard::q;
		case 'r': return Keyboard::r;
		case 's': return Keyboard::s;
		case 't': return Keyboard::t;
		case 'u': return Keyboard::u;
		case 'v': return Keyboard::v;
		case 'w': return Keyboard::w;
		case 'x': return Keyboard::x;
		case 'y': return Keyboard::y;
		case 'z': return Keyboard::z;
#endif

		case 'A': return Keyboard::A;
		case 'B': return Keyboard::B;
		case 'C': return Keyboard::C;
		case 'D': return Keyboard::D;
		case 'E': return Keyboard::E;
		case 'F': return Keyboard::F;
		case 'G': return Keyboard::G;
		case 'H': return Keyboard::H;
		case 'I': return Keyboard::I;
		case 'J': return Keyboard::J;
		case 'K': return Keyboard::K;
		case 'L': return Keyboard::L;
		case 'M': return Keyboard::M;
		case 'N': return Keyboard::N;
		case 'O': return Keyboard::O;
		case 'P': return Keyboard::P;
		case 'Q': return Keyboard::Q;
		case 'R': return Keyboard::R;
		case 'S': return Keyboard::S;
		case 'T': return Keyboard::T;
		case 'U': return Keyboard::U;
		case 'V': return Keyboard::V;
		case 'W': return Keyboard::W;
		case 'X': return Keyboard::X;
		case 'Y': return Keyboard::Y;
		case 'Z': return Keyboard::Z;
		case '0': return Keyboard::KEY_0;
		case '1': return Keyboard::KEY_1;
		case '2': return Keyboard::KEY_2;
		case '3': return Keyboard::KEY_3;
		case '4': return Keyboard::KEY_4;
		case '5': return Keyboard::KEY_5;
		case '6': return Keyboard::KEY_6;
		case '7': return Keyboard::KEY_7;
		case '8': return Keyboard::KEY_8;
		case '9': return Keyboard::KEY_9;
		case '\b': return Keyboard::BACKSPACE;

		// By experiment:
		case 0x14: return Keyboard::CAPSLOCK;
//		case 0x5d: return Keyboard::MENU;

//		case VK_CANCEL:   return Keyboard::CANCEL;
		case VK_TAB:      return Keyboard::TAB;
		case VK_RETURN:   return Keyboard::ENTER;
		case VK_SHIFT:    return Keyboard::LSHIFT;
		case VK_CONTROL:  return Keyboard::LCTRL;
		case VK_MENU:     return Keyboard::LALT;
		case VK_PAUSE:    return Keyboard::PAUSE;
		case VK_ESCAPE:   return Keyboard::ESCAPE;
		case VK_SPACE:    return Keyboard::SPACE;
		case VK_PRIOR:    return Keyboard::PAGEUP;
		case VK_NEXT:     return Keyboard::PAGEDOWN;
		case VK_END:      return Keyboard::END;
		case VK_HOME:     return Keyboard::HOME;
		case VK_LEFT:     return Keyboard::LEFT;
		case VK_UP:       return Keyboard::UP;
		case VK_RIGHT:    return Keyboard::RIGHT;
		case VK_DOWN:     return Keyboard::DOWN;
//		case VK_PRINT:    return Keyboard::PRINT;
//		case VK_INSERT:   return Keyboard::INSERT;
//		case VK_DELETE:   return Keyboard::DELETE;
//		case VK_HELP:     return Keyboard::HELP;
		case VK_LWIN:     return Keyboard::LWINDOWS;
		case VK_RWIN:     return Keyboard::RWINDOWS;
		case VK_NUMPAD0:  return Keyboard::NUMPAD_0;
		case VK_NUMPAD1:  return Keyboard::NUMPAD_1;
		case VK_NUMPAD2:  return Keyboard::NUMPAD_2;
		case VK_NUMPAD3:  return Keyboard::NUMPAD_3;
		case VK_NUMPAD4:  return Keyboard::NUMPAD_4;
		case VK_NUMPAD5:  return Keyboard::NUMPAD_5;
		case VK_NUMPAD6:  return Keyboard::NUMPAD_6;
		case VK_NUMPAD7:  return Keyboard::NUMPAD_7;
		case VK_NUMPAD8:  return Keyboard::NUMPAD_8;
		case VK_NUMPAD9:  return Keyboard::NUMPAD_9;
		case VK_MULTIPLY: return Keyboard::NUMPAD_MULTIPLY;
		case VK_ADD:      return Keyboard::NUMPAD_PLUS;
		case VK_SUBTRACT: return Keyboard::NUMPAD_MINUS;
		case VK_DECIMAL:  return Keyboard::NUMPAD_DECIMAL;
		case VK_DIVIDE:   return Keyboard::NUMPAD_DIVIDE;
		case VK_F1:       return Keyboard::F1;
		case VK_F2:       return Keyboard::F2;
		case VK_F3:       return Keyboard::F3;
		case VK_F4:       return Keyboard::F4;
		case VK_F5:       return Keyboard::F5;
		case VK_F6:       return Keyboard::F6;
		case VK_F7:       return Keyboard::F7;
		case VK_F8:       return Keyboard::F8;
		case VK_F9:       return Keyboard::F9;
		case VK_F10:      return Keyboard::F10;
		case VK_F11:      return Keyboard::F11;
		case VK_F12:      return Keyboard::F12;
		case VK_F13:      return Keyboard::F13;
		case VK_F14:      return Keyboard::F14;
		case VK_F15:      return Keyboard::F15;
		case VK_F16:      return Keyboard::F16;
//		case VK_NUMLOCK:  return Keyboard::NUMLOCK;
		case VK_SCROLL:   return Keyboard::SCROLLLOCK;
		case VK_LSHIFT:   return Keyboard::LSHIFT;
		case VK_RSHIFT:   return Keyboard::RSHIFT;
		case VK_LCONTROL: return Keyboard::LCTRL;
		case VK_RCONTROL: return Keyboard::RCTRL;
		case VK_LMENU:    return Keyboard::LALT;
		case VK_RMENU:    return Keyboard::RALT;
		}
		return Keyboard::NONE;
	}

	Keyboard::Key chmap(char in)
	{
		// Keys that must be translated via MapVirtualKey; as the virtual key code
		// is language and keyboard dependent.
		switch (in)
		{
			case '!': return Keyboard::EXCLAMATION;
			case '"': return Keyboard::DOUBLEQUOTE;
			case '#': return Keyboard::HASH;
			case '$': return Keyboard::DOLLAR;
			case '%': return Keyboard::PERCENT;
			case '&': return Keyboard::AMPERSAND;
			case '\'': return Keyboard::APOSTROPHE;
			case '(': return Keyboard::PARENLEFT;
			case ')': return Keyboard::PARENRIGHT;
			case '*': return Keyboard::ASTERISK;
			case '+': return Keyboard::PLUS;
			case ',': return Keyboard::COMMA;
			case '-': return Keyboard::MINUS;
			case '.': return Keyboard::PERIOD;
			case '/': return Keyboard::SLASH;
			case ':': return Keyboard::COLON;
			case ';': return Keyboard::SEMICOLON;
			case '<': return Keyboard::LESS;
			case '=': return Keyboard::EQUAL;
			case '>': return Keyboard::GREATER;
			case '?': return Keyboard::QUESTION;
			case '@': return Keyboard::AT;
			case '[': return Keyboard::BRACKETLEFT;
			case '\\': return Keyboard::BACKSLASH;
			case ']': return Keyboard::BRACKETRIGHT;
			case 0x5e: return Keyboard::ASCIICIRCUM;
			case '_': return Keyboard::UNDERSCORE;
	//	   case 0x60: return Keyboard::GRAVE;
			case '`': return Keyboard::QUOTELEFT;
			case '{': return Keyboard::BRACELEFT;
			case '|': return Keyboard::BAR;
			case '}': return Keyboard::BRACERIGHT;
			case '~': return Keyboard::ASCIITILDE;
		}
		return Keyboard::NONE;
	}
}

#endif

