#version 430

//uniform mat4 mvMatrix;
//uniform mat4 pMatrix;

out CatchData
{
	vec4 osPos;
} CatchOut;


//uniform float dt;

uniform vec3 catchPos;


void main()
{
	//float g = -0.3;

	//ballPos.y += g * dt;
	//if (ballPos.y < -1)
	//	ballPos.y = 1;

	// TODO: Peg related processing, ie animate.
	// TODO: Could do some peg related collision detection maybe?

	// TODO: Check if the ball has collided with the peg, if so, do something.

	CatchOut.osPos = vec4(catchPos, 1.0);
}

