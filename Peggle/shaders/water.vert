#version 430

out WaterData
{
	vec4 osPos[4];
	vec3 osNorm[4];
} WaterOut;

uniform int tess;
uniform float t;


struct SineFunction
{
	float amplitude;
	float phase;
	float periodX;
	float periodZ;
	float speed;
};

SineFunction sineFuncs[3];



float calcAngle(int i, float x, float z, float t)
{
	SineFunction f = sineFuncs[i];
	return f.periodX * x + f.periodZ * z + f.speed * t + f.phase;
}

float calcHeight(float x, float z, float t)
{
	float y = 0;
	for (int i = 0; i < 3; i++)
	{
		SineFunction f = sineFuncs[i];
		y += f.amplitude * sin(calcAngle(i, x, z, t));
	}
	return y;
}

vec3 calcNormal(float x, float z, float t)
{
	vec3 normal = vec3(0);
	for (int i = 0; i < 3; i++)
	{
		SineFunction f = sineFuncs[i];
		float angle = calcAngle(i, x, z, t);
		normal += normalize(vec3(f.amplitude * f.periodX * -cos(angle), 1, f.amplitude * f.periodZ * -cos(angle)));
	}
	return normalize(normal);
}



void main()
{
	sineFuncs[0] = SineFunction(0.2 / 5.0, 0.0, 0.17 * 10.0, 0.47 * 10.0, 7.0);
	sineFuncs[1] = SineFunction(0.3 / 5.0, 0.0, 0.15 * 10.0, 0.53 * 10.0, 0.7);
	sineFuncs[2] = SineFunction(0.4 / 5.0, 0.0, 0.05 * 10.0, 0.004 * 10.0, 1.0);

	// Calculate object space vertex positions based on gl_VertexID and tesselation.
	int x = gl_VertexID % tess;
	int z = gl_VertexID / tess;

	float width = 50;
	float depth = 50;

	float res = float(tess);

	// I actually need four vertices, but the remaining three are easy to calculate.
	float xStep = width / res;
	float zStep = depth / res;

	float osX = (float(x) / res) * width - (width / 2.0);
	float osZ = (float(z) / res) * depth - (depth / 2.0);

	vec3 osVerts[4];
	osVerts[0] = vec3(osX, 0, osZ);
	osVerts[1] = vec3(osX, 0, osZ + zStep);
	osVerts[2] = vec3(osX + xStep, 0, osZ);
	osVerts[3] = vec3(osX + xStep, 0, osZ + zStep);

	// Get height and normals.
	vec3 osNorms[4];
	for (int i = 0; i < 4; i++)
	{
		osVerts[i].y = calcHeight(osVerts[i].x, osVerts[i].z, t) - 1;
		osNorms[i] = calcNormal(osVerts[i].x, osVerts[i].z, t);
	}

	for (int i = 0; i < 4; i++)
	{
		WaterOut.osPos[i] = vec4(osVerts[i], 1);
		WaterOut.osNorm[i] = osNorms[i];
	}
}

