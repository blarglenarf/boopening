#version 130

in vec3 texCoord;

uniform samplerCube skybox;


out vec4 fragColor[2];


void main()
{
	fragColor[0] = texture(skybox, texCoord);
	fragColor[1] = vec4(0, 0, 0, 0);
}

