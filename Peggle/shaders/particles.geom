#version 430

layout(points) in;

layout(triangle_strip, max_vertices = 4) out;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

uniform ivec4 viewport;

in ParticleData
{
	//vec4 osPos;
	vec4 esVert;
	vec3 col;
	vec3 vel;
} ParticleIn[1];


#if 0
out VertexData
{
	flat vec3 osPos;
	flat vec3 esPos;
	flat float radius;
	flat vec3 col;
} VertexOut;


#define VERT(v) \
	esVert = vec4(verts[v] + esPos.xyz, 1); \
	csVert = pMatrix * esVert; \
	VertexOut.osPos = ParticleIn[0].osPos.xyz; \
	VertexOut.esPos = esPos.xyz; \
	VertexOut.radius = radius; \
	VertexOut.col = ParticleIn[0].col; \
	gl_Position = csVert; \
	EmitVertex();
#endif


out ParticleData
{
	vec2 texCoord;
	vec3 esFrag;
	vec3 color;
} VertexOut;


#define VERT(v) \
	esVert = m * vec4(verts[v].xyz, 1.0); \
	VertexOut.esFrag = esVert.xyz; \
	VertexOut.texCoord = texes[v]; \
	VertexOut.color = ParticleIn[0].col; \
	gl_Position = pMatrix * esVert; \
	EmitVertex();


mat4 getRot(vec3 vel)
{
	vec3 up = vec3(0, 0, 1);
	vec3 xAxis = normalize(cross(vel, up));
	vec3 yAxis = normalize(cross(xAxis, vel));

	mat4 m;

	m[0] = vec4(xAxis.x, xAxis.y, xAxis.z, 0);
	m[1] = vec4(vel.x, vel.y, vel.z, 0);
	m[2] = vec4(yAxis.x, yAxis.y, yAxis.z, 0);
	m[3] = vec4(ParticleIn[0].esVert.xyz, 1);

	return m;
}



void main()
{
	float size = 0.005;
	float stretch = 0.01;

	vec4 esVert;

	mat4 m = getRot(normalize(ParticleIn[0].vel));
	m = mvMatrix * m;

	vec4 verts[4];
	verts[0] = vec4(size, -size - stretch, 0, 1);
	verts[1] = vec4(-size, -size - stretch, 0, 1);
	verts[2] = vec4(-size, size + stretch, 0, 1);
	verts[3] = vec4(size, size + stretch, 0, 1);

	vec2 texes[4];
	texes[0] = vec2(1, 0);
	texes[1] = vec2(0, 0);
	texes[2] = vec2(0, 1);
	texes[3] = vec2(1, 1);

	VERT(1);
	VERT(2);
	VERT(0);
	VERT(3);
	EndPrimitive();

#if 0
	// TODO: How large should this be? Probably determined by data in the particle buffer.
	float radius = 0.01;
	float quadSize = radius * 2.5;
	vec4 esVert = vec4(0);
	vec4 csVert = vec4(0);

	// Position of the centre of the particle in eye-space, clip-space, screen-space.
	vec4 esPos = mvMatrix * ParticleIn[0].osPos;
	vec4 csPos = pMatrix * esPos;
	vec2 ssPos = ((vec2(csPos.xy / csPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);

	vec3 verts[4];
	verts[0] = vec3(quadSize, -quadSize, 0);
	verts[1] = vec3(-quadSize, -quadSize, 0);
	verts[2] = vec3(-quadSize, quadSize, 0);
	verts[3] = vec3(quadSize, quadSize, 0);

	VERT(2);
	VERT(1);
	VERT(3);
	VERT(0);
	EndPrimitive();
#endif
}

