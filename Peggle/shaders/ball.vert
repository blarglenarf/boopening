#version 430

//uniform mat4 mvMatrix;
//uniform mat4 pMatrix;

out BallData
{
	vec4 osPos;
} BallOut;


//uniform float dt;

uniform vec3 ballPos;


void main()
{
	//float g = -0.3;

	//ballPos.y += g * dt;
	//if (ballPos.y < -1)
	//	ballPos.y = 1;

	// TODO: Peg related processing, ie animate.
	// TODO: Could do some peg related collision detection maybe?

	// TODO: Check if the ball has collided with the peg, if so, do something.

	BallOut.osPos = vec4(ballPos, 1.0);
}

