#version 430

//uniform mat4 mvMatrix;
//uniform mat4 pMatrix;

coherent buffer ParticlePosData
{
	vec4 particlePosData[];
};

coherent buffer ParticleDirData
{
	vec2 particleDirData[];
};

readonly buffer RandomData
{
	vec2 randomData[];
};


uniform float color;
uniform vec3 pos;

uniform int offset;


#include "utils.glsl"


void main()
{
	int idx = offset + gl_VertexID;

	vec2 dir = randomData[gl_VertexID];

	particlePosData[idx] = vec4(pos, color);
	particleDirData[idx] = dir;
}

