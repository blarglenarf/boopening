#version 430

//uniform mat4 mvMatrix;
//uniform mat4 pMatrix;

buffer PegData
{
	vec4 pegData[];
};

out PegData
{
	vec4 osPos;
	vec3 col;
} PegOut;

//uniform float dt;
//uniform float g;

#include "utils.glsl"


void main()
{
	vec4 pegPos = pegData[gl_VertexID];
	vec4 col = floatToRGBA8(pegPos.w);

	//pegPos.y += g * dt;
	//if (pegPos.y < -1)
	//	pegPos.y = 1;

	pegData[gl_VertexID] = pegPos;

	// TODO: Peg related processing, ie animate.
	// TODO: Could do some peg related collision detection maybe?

	// TODO: Check if the ball has collided with the peg, if so, do something.

	pegPos.w = 1.0;
	PegOut.osPos = pegPos;
	PegOut.col = col.xyz;
}

