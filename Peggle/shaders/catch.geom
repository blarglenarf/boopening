#version 430

layout(points) in;

layout(triangle_strip, max_vertices = 4) out;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

uniform ivec4 viewport;

uniform float catchRadius;

in CatchData
{
	vec4 osPos;
} CatchIn[1];

out VertexData
{
	flat vec3 osPos;
	flat vec3 esPos;
	flat float radius;
} VertexOut;


#define VERT(v) \
	esVert = vec4(verts[v] + esPos.xyz, 1); \
	csVert = pMatrix * esVert; \
	VertexOut.osPos = CatchIn[0].osPos.xyz; \
	VertexOut.esPos = esPos.xyz; \
	VertexOut.radius = radius; \
	gl_Position = csVert; \
	EmitVertex();


void main()
{
	// TODO: How large should this be? Probably determined by data in the peg buffer.
	float radius = catchRadius;
	float quadSize = radius * 2.5;
	vec4 esVert = vec4(0);
	vec4 csVert = vec4(0);

	// Position of the centre of the peg in eye-space, clip-space, screen-space.
	vec4 esPos = mvMatrix * CatchIn[0].osPos;
	vec4 csPos = pMatrix * esPos;
	vec2 ssPos = ((vec2(csPos.xy / csPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);

	vec3 verts[4];
	verts[0] = vec3(quadSize, -quadSize, 0);
	verts[1] = vec3(-quadSize, -quadSize, 0);
	verts[2] = vec3(-quadSize, quadSize, 0);
	verts[3] = vec3(quadSize, quadSize, 0);

	VERT(2);
	VERT(1);
	VERT(3);
	VERT(0);
	EndPrimitive();
}

