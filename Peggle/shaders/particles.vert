#version 430

coherent buffer ParticlePosData
{
	vec4 particlePosData[];
};

coherent buffer ParticleDirData
{
	vec2 particleDirData[];
};

readonly buffer PegData
{
	vec4 pegData[];
};


out ParticleData
{
	vec4 esVert;
	vec3 col;
	vec3 vel;
} ParticleOut;


uniform mat4 mvMatrix;

uniform float dt;
uniform float g;

uniform int nPegs;

#include "utils.glsl"



int collisionResponse(inout vec3 pos, inout vec3 vel)
{
	// Collisions with arena edge.
	if (pos.x + 0.01 > 1)
	{
		vel = reflect(vel, vec3(-1, 0, 0));
		pos.x = 1 - 0.01;
		return -1;
	}
	else if (pos.x - 0.01 < -1)
	{
		vel = reflect(vel, vec3(1, 0, 0));
		pos.x = -1 + 0.01;
		return -1;
	}

	if (pos.y + 0.01 > 1)
	{
		vel = reflect(vel, vec3(0, -1, 0));
		pos.y = 1 - 0.01;
		return -1;
	}
	else if (pos.y - 0.01 < -1)
	{
		vel = reflect(vel, vec3(0, 1, 0));
		pos.y = -1 + 0.01;
		return -1;
	}


	for (int i = 0; i < nPegs; i++)
	{
		vec3 pegPos = pegData[i].xyz;
		float dist = distance(pegPos, pos);
		if (dist <= (0.01 + 0.04))
		{
			float m1 = 3, m2 = 10;
			vec3 u1 = vel, u2 = vec3(0, 0, 0);
			vec3 x1 = pos, x2 = pegPos;
			vec3 v1 = u1 - (((2 * m2) / (m1 + m2)) * (dot(u1 - u2, x1 - x2) / dot(x1 - x2, x1 - x2)) * (x1 - x2));
			vel = v1;

			// TODO: What about elastic collisions with polygonal surfaces?

			// Push the ball away from the peg slightly.
			while (dist <= (0.02 + 0.04))
			{
				pos += vel * 0.01;
				dist = distance(pegPos, pos);
			}

			return i;
		}
	}

	return -1;
}




void main()
{
	vec4 particlePos = particlePosData[gl_VertexID];
	vec4 col = floatToRGBA8(particlePos.w);

	vec2 particleDir = particleDirData[gl_VertexID];

	//particleDir.y += g * 0.1 * dt;

	// Collisions with pegs.
	vec3 dir = vec3(particleDir, 0);
	collisionResponse(particlePos.xyz, dir);

	particlePos.x += dir.x * dt * 0.5;
	particlePos.y += dir.y * dt * 0.5;

	//particleDirData[gl_VertexID] = particleDir;
	particlePosData[gl_VertexID] = particlePos;
	particleDirData[gl_VertexID] = dir.xy;

	//vec4 esVert = mvMatrix * vec4(particlePos.xyz, 1);
	vec4 esVert = vec4(particlePos.xyz, 1);

	//particlePos.w = 1.0;
	//ParticleOut.osPos = particlePos;
	ParticleOut.esVert = esVert;
	ParticleOut.col = col.xyz;
	ParticleOut.vel = dir * 0.5;
}

