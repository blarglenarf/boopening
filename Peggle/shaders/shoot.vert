#version 430

//uniform mat4 mvMatrix;
//uniform mat4 pMatrix;

out ShootData
{
	vec4 osPos;
} ShootOut;


uniform vec3 shooterPos;


void main()
{
	// Is this even necessary?
	ShootOut.osPos = vec4(shooterPos, 1.0);
}

