#version 130

uniform mat4 mvMatrix;
uniform mat4 pMatrix;


out vec3 texCoord;


void main()
{
	vec4 osVert = vec4(gl_Vertex.xyz, 1);

	//vec4 esVert = mvMatrix * osVert;
	//vec4 csVert = pMatrix * esVert;

	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

	texCoord = osVert.xyz;

	gl_Position = csVert;
}

