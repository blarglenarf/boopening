#version 430

layout(points) in;

layout(triangle_strip, max_vertices = 4) out;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;

in WaterData
{
	vec4 osPos[4];
	vec3 osNorm[4];
} WaterIn[1];


out FragData
{
	vec3 esFrag;
	vec3 norm;
} FragOut;


void main()
{
	for (int i = 0; i < 4; i++)
	{
		vec4 esPos = mvMatrix * WaterIn[0].osPos[i];
		vec4 csPos = pMatrix * esPos;
		FragOut.esFrag = esPos.xyz;
		FragOut.norm = nMatrix * WaterIn[0].osNorm[i];
		gl_Position = csPos;
		EmitVertex();
	}
	EndPrimitive();
}

