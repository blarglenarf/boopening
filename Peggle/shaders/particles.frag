#version 430


in ParticleData
{
	vec2 texCoord;
	vec3 esFrag;
	vec3 color;
} VertexIn;

uniform sampler2D particleTex;

uniform float particleTime;

#include "utils.glsl"

out vec4 fragColor[2];


void main()
{
	//vec4 lightColor = floatToRGBA8(lightColors[VertexIn.id]);
	vec4 lightColor = vec4(VertexIn.color, 1);
	vec4 texColor = texture2D(particleTex, VertexIn.texCoord);
	//vec4 texColor = vec4(0.5, 0, 0, 0.6);

	vec2 centreCoord = vec2(0.5, 0.5);
	float d = max(0.0, 0.75 - distance(centreCoord, VertexIn.texCoord));

	vec3 centreCol = vec3(1.0);

	vec3 col = texColor.xyz * lightColor.xyz + (centreCol * d);
	col = clamp(col, vec3(0), vec3(1));

	float a = particleTime / 5.0;
	a = min(a, texColor.w);

	vec4 color = vec4(col, a);

	fragColor[0] = color;
	fragColor[1] = vec4(0);
}


#if 0
in VertexData
{
	flat vec3 osPos;
	flat vec3 esPos;
	flat float radius;
	flat vec3 col;
} VertexIn;



uniform mat4 mvMatrix;
uniform mat3 nMatrix;
uniform mat4 invMvMatrix;
uniform mat4 invPMatrix;
uniform ivec4 viewport;

uniform float particleTime;

#include "utils.glsl"




out vec4 fragColor[2];


vec3 brdfLambert(vec3 material, vec3 lightCol, float dp)
{
	return material * dp * lightCol;
}

// see http://en.wikipedia.org/wiki/Schlick%27s_approximation
float fresnelSchlick(float r0, float vDotH)
{
	return r0 + (1.0 - r0) * pow(1.0 - vDotH, 5.0);
}

// see http://graphicrants.blogspot.de/2013/08/specular-brdf-reference.html
// see http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float geometricShadowingSchlickBeckmann(float nDotV, float k)
{
	return nDotV / (nDotV * (1.0 - k) + k);
}

// see http://graphicrants.blogspot.de/2013/08/specular-brdf-reference.html
// see http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float geometricShadowingSmith(float nDotL, float nDotV, float k)
{
	return geometricShadowingSchlickBeckmann(nDotL, k) * geometricShadowingSchlickBeckmann(nDotV, k);
}

vec3 brdfCookTorrence(vec3 material, vec3 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal, float roughness)
{
	// See https://github.com/McNopper/OpenGL/blob/master/Example32/shader/brdf.frag.glsl
	// basis, N, V, K
	// basis: tangent, bitangent, normal matrix, normal: normal vector, eye: eyeDir vector, k: roughness float

#if 0
	vec3 HtangentSpace = microfacetWeightedSampling(randomPoint);

	// Transform H to world space.
	vec3 H = basis * HtangentSpace;

	// Note: reflect takes incident vector.
	vec3 L = reflect(-V, H);

	float NdotL = dot(N, L);
	float NdotV = dot(N, V);
	float NdotH = dot(N, H);
#endif
	vec3 V = normalize(viewDir); // FIXME: I'm pretty sure this should be negative.
	vec3 H = normalize(reflect(normalize(-lightDir), normalize(normal)));
	vec3 L = normalize(lightDir);//reflect(-viewDir, H); // FIXME: Again, pretty sure this should be negative.

	float nDotL = dot(normal, L);
	float nDotV = dot(normal, V);
	float nDotH = dot(normal, H);

	// Lighted and visible?
	if (nDotL > 0 && nDotV > 0)
	{
		float vDotH = max(0, dot(V, H));

		// Fresnel.
		float refCoefficient = 1.0;
		float F = fresnelSchlick(refCoefficient, vDotH);

		// Geometric Shadowing.
		float G = geometricShadowingSmith(nDotL, nDotV, roughness);

		//
		// Lo = BRDF * L * NdotL / PDF
		//
		// BRDF is D * F * G / (4 * NdotL * NdotV).
		// PDF is D * NdotH / (4 * VdotH).
		// D and 4 * NdotL are canceled out, which results in this formula: Lo = color * L * F * G * VdotH / (NdotV * NdotH)		
		float colorFactor = F * G * vDotH / (nDotV * nDotH);

		// Note: Needed for robustness. With specific parameters, a NaN can be the result.
		if (isnan(colorFactor))
			return vec3(0);

		return material * lightColor * max(0, colorFactor);
		//return texture(u_panoramaTexture, panorama(L)).rgb * colorFactor;
	}

	return vec3(0);
}

vec3 brdfPhong(vec3 material, vec3 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal, float shininess)
{
	vec3 reflection = reflect(-lightDir, normal);
	float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
	//float nDotH = abs(dot(viewDir, normalize(reflection)));
	float intensity = pow(nDotH, shininess);
	return intensity * lightColor;
}

vec3 applyLighting(vec3 esFrag, vec3 normal, vec3 lightDir, vec3 material, vec3 lightColor)
{
	// TODO: Now apply some lighting to the sphere. Assume a light source very far away perhaps?
	// TODO: Probably need a light colour.
	//vec3 lightDir = (mvMatrix * vec4(1, 1, 1, 0)).xyz;
	//vec3 lightColor = vec3(1);

	// TODO: Need an incoming peg colour for ambient, diffuse, specular.
	vec3 ambient = vec3(material * 0.4);
	float cosTheta = dot(normal, lightDir);
	float dp = clamp(cosTheta, 0.0, 1.0);

	vec3 diffuse = brdfLambert(material, lightColor, dp);
	vec3 specular = vec3(0);
	if (dp > 0)
		specular = brdfCookTorrence(material, lightColor, lightDir, normalize(-esFrag), normal, 2);
		//specular = brdfPhong(material, lightColor, lightDir, normalize(-esFrag), normal, 64);

	return ambient + diffuse + specular;
}


void main()
{
	// Direction from the camera through the pixel in eye-space.
	vec3 dir = normalize(getEyeFromWindow(vec3(gl_FragCoord.xy, -30.0), viewport, invPMatrix).xyz);

	// Get front and back depths of the sphere.
	// TODO: Stretch this in the direction of movement. Maybe even add some motion blur.
	// TODO: Requires line-ellipsoid intersection instead of line-sphere.
	float frontDepth = 0, backDepth = 0;
	if (!sphereIntersection(VertexIn.esPos, VertexIn.radius, dir, frontDepth, backDepth))
	{
		discard;
		return;
	}

	// Normal vector for the sphere is the same as direction away from centre of the sphere.
	vec4 esFrag = getEyeFromWindow(vec3(gl_FragCoord.xy, -frontDepth), viewport, invPMatrix);
	vec4 osFrag = invMvMatrix * esFrag;

	vec3 osNorm = normalize(vec3(osFrag.xyz - VertexIn.osPos));
	vec3 esNorm = normalize(nMatrix * osNorm);

	//vec3 material = osFrag.xyz;
	//if (osFrag.x < 0.0 && osFrag.y < 0.0)
	//	material.yz = -osFrag.xy;
	//vec3 material = vec3(1.0, 0.0, 0.2);
	vec3 material = VertexIn.col;

	// Apply lighting from a single global light source, as well as all lit pegs.
	vec3 lightDir = (mvMatrix * vec4(1, 1, 1, 0)).xyz;

	// TODO: If the peg is lit, then it should be brighter.
	// TODO: Or alternatively, add a glow effect around the peg (requires rendering a slightly larger quad).
	// TODO: Do we want particles to be lit?
	vec3 col = applyLighting(esFrag.xyz, esNorm, lightDir, material, vec3(1)) * 0.5;

	// Also make the particle really bright.
#if 0
	col += applyLighting(esFrag.xyz, esNorm, vec3(0, 0, 1), material, material);
	vec4 lightCol = vec4(material, 1);
	vec3 esLightPos = (mvMatrix * vec4(VertexIn.osPos, 1.0)).xyz;

	// Attenuation.
	float dist = distance(esFrag.xyz, esLightPos.xyz);

	float sqAtt = 1.0 / (dist * 15 * dist * 15); // Upping the distance a bit since the game world is so small.
	sqAtt *= sqAtt;
	float lAtt = max(0, 1 - (dist * dist) / (0.6 * 0.6)); // Linear attenuation, not great.
	lAtt *= lAtt;

	float att = max(sqAtt, lAtt);

	col += applyLighting(esFrag.xyz, esNorm, normalize(esLightPos.xyz - esFrag.xyz), material, lightCol.xyz) * att;
#endif
	// Gradually reduce colour based on particleTime.
	float a = particleTime / 5.0;
	col = clamp(col, vec3(0), vec3(1));

#if 0
	// Check whether color is higher than given threshold.
	// sRGB values: 0.2126, 0.7152, 0.0722
	//vec3 upper = vec3(0.2126, 0.7152, 0.0722);
	// RGB values: 0.299, 0.587, 0.114
	vec3 upper = vec3(0.299, 0.587, 0.114);
	float brightness = dot(col.xyz, upper);

	// TODO: Pick based on brightness of the whole scene (not just the pixel).
	// Doing this after applying bloom.
	//color = toneMapColor(color, 1.0, 1.0);

	//float luminance = dot(luminanceVector, color.xyz);
	//luminance = max(0.0, luminance - brightPassThreshold);
	//color.xyz *= sign(luminance);

	// color.
	fragColor[0] = vec4(col, 1);

	// bright (can decide on a different brightness val I guess).
	if (brightness > 1)
		fragColor[1] = vec4(col, a);
	else
		fragColor[1] = vec4(0, 0, 0, 0);
#else
	fragColor[0] = vec4(col.xyz, a);
	fragColor[1] = vec4(0, 0, 0, 0);
#endif

	// TODO: We could now add these to an lfb for some cool oit with 3D spheres.

	// This is a linear depth value, but should be good enough.
	gl_FragDepth = -esFrag.z / 30.0;
}
#endif

