#version 430

in VertexData
{
	flat vec3 osPos;
	flat vec3 esPos;
	flat float radius;
} VertexIn;


uniform mat4 mvMatrix;
uniform mat3 nMatrix;
uniform mat4 invMvMatrix;
uniform mat4 invPMatrix;
uniform ivec4 viewport;

#include "utils.glsl"




out vec4 fragColor[2];


vec3 brdfLambert(vec3 material, vec3 lightCol, float dp)
{
	return material * dp * lightCol;
}

// see http://en.wikipedia.org/wiki/Schlick%27s_approximation
float fresnelSchlick(float r0, float vDotH)
{
	return r0 + (1.0 - r0) * pow(1.0 - vDotH, 5.0);
}

// see http://graphicrants.blogspot.de/2013/08/specular-brdf-reference.html
// see http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float geometricShadowingSchlickBeckmann(float nDotV, float k)
{
	return nDotV / (nDotV * (1.0 - k) + k);
}

// see http://graphicrants.blogspot.de/2013/08/specular-brdf-reference.html
// see http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float geometricShadowingSmith(float nDotL, float nDotV, float k)
{
	return geometricShadowingSchlickBeckmann(nDotL, k) * geometricShadowingSchlickBeckmann(nDotV, k);
}

vec3 brdfCookTorrence(vec3 material, vec3 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal, float roughness)
{
	// See https://github.com/McNopper/OpenGL/blob/master/Example32/shader/brdf.frag.glsl
	// basis, N, V, K
	// basis: tangent, bitangent, normal matrix, normal: normal vector, eye: eyeDir vector, k: roughness float

#if 0
	vec3 HtangentSpace = microfacetWeightedSampling(randomPoint);

	// Transform H to world space.
	vec3 H = basis * HtangentSpace;

	// Note: reflect takes incident vector.
	vec3 L = reflect(-V, H);

	float NdotL = dot(N, L);
	float NdotV = dot(N, V);
	float NdotH = dot(N, H);
#endif
	vec3 V = normalize(viewDir); // FIXME: I'm pretty sure this should be negative.
	vec3 H = normalize(reflect(normalize(-lightDir), normalize(normal)));
	vec3 L = normalize(lightDir);//reflect(-viewDir, H); // FIXME: Again, pretty sure this should be negative.

	float nDotL = dot(normal, L);
	float nDotV = dot(normal, V);
	float nDotH = dot(normal, H);

	// Lighted and visible?
	if (nDotL > 0 && nDotV > 0)
	{
		float vDotH = max(0, dot(V, H));

		// Fresnel.
		float refCoefficient = 1.0;
		float F = fresnelSchlick(refCoefficient, vDotH);

		// Geometric Shadowing.
		float G = geometricShadowingSmith(nDotL, nDotV, roughness);

		//
		// Lo = BRDF * L * NdotL / PDF
		//
		// BRDF is D * F * G / (4 * NdotL * NdotV).
		// PDF is D * NdotH / (4 * VdotH).
		// D and 4 * NdotL are canceled out, which results in this formula: Lo = color * L * F * G * VdotH / (NdotV * NdotH)		
		float colorFactor = F * G * vDotH / (nDotV * nDotH);

		// Note: Needed for robustness. With specific parameters, a NaN can be the result.
		if (isnan(colorFactor))
			return vec3(0);

		return material * lightColor * max(0, colorFactor);
		//return texture(u_panoramaTexture, panorama(L)).rgb * colorFactor;
	}

	return vec3(0);
}

vec3 brdfPhong(vec3 material, vec3 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal, float shininess)
{
	vec3 reflection = reflect(-lightDir, normal);
	float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
	//float nDotH = abs(dot(viewDir, normalize(reflection)));
	float intensity = pow(nDotH, shininess);
	return intensity * lightColor;
}

vec3 applyLighting(vec3 esFrag, vec3 normal, vec3 material)
{
	// TODO: Now apply some lighting to the sphere. Assume a light source very far away perhaps?
	// TODO: Probably need a light colour.
	vec3 lightDir = (mvMatrix * vec4(-1, 1, 1, 0)).xyz;
	vec3 lightColor = vec3(1);

	// TODO: Need an incoming peg colour for ambient, diffuse, specular.
	vec3 ambient = vec3(material * 0.4);
	float cosTheta = dot(normal, lightDir);
	float dp = clamp(cosTheta, 0.0, 1.0);

	vec3 diffuse = brdfLambert(material, lightColor, dp);
	vec3 specular = vec3(0);
	if (dp > 0)
		specular = brdfCookTorrence(material, lightColor, lightDir, normalize(-esFrag), normal, 2);
		//specular = brdfPhong(material, lightColor, lightDir, normalize(-esFrag), normal, 64);

	return ambient + diffuse + specular;
}


void main()
{
	// Direction from the camera through the pixel in eye-space.
	vec3 dir = normalize(getEyeFromWindow(vec3(gl_FragCoord.xy, -30.0), viewport, invPMatrix).xyz);

	// Get front and back depths of the sphere.
	float frontDepth = 0, backDepth = 0;
	if (!sphereIntersection(VertexIn.esPos, VertexIn.radius, dir, frontDepth, backDepth))
	{
		discard;
		return;
	}

	// Normal vector for the sphere is the same as direction away from centre of the sphere.
	vec4 esFrag = getEyeFromWindow(vec3(gl_FragCoord.xy, -frontDepth), viewport, invPMatrix);
	vec4 osFrag = invMvMatrix * esFrag;

	vec3 osNorm = normalize(vec3(osFrag.xyz - VertexIn.osPos));
	vec3 esNorm = normalize(nMatrix * osNorm);

	//vec3 material = osFrag.xyz;
	//if (osFrag.x < 0.0 && osFrag.y < 0.0)
	//	material.yz = -osFrag.xy;
	vec3 material = vec3(0.2, 0, 0.8);
	vec3 col = applyLighting(esFrag.xyz, esNorm, material);

	fragColor[0] = vec4(col.xyz, 1);
	fragColor[1] = vec4(0, 0, 0, 0);

	// TODO: We could now add these to an lfb for some cool oit with 3D spheres.

	// This is a linear depth value, but should be good enough.
	gl_FragDepth = -esFrag.z / 30.0;
}

