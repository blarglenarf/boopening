#version 430

// TODO: Apply cubemap reflections here.

in FragData
{
	vec3 esFrag;
	vec3 norm;
} FragIn;

buffer LightData
{
	vec4 lightData[];
};

uniform int nLights;


uniform mat4 mvMatrix;
uniform mat4 invMvMatrix;
uniform mat4 invPMatrix;
uniform ivec4 viewport;

uniform samplerCube skybox;

out vec4 fragColor[2];


#include "utils.glsl"



vec3 brdfLambert(vec3 material, vec3 lightCol, float dp)
{
	return material * dp * lightCol;
}

// see http://en.wikipedia.org/wiki/Schlick%27s_approximation
float fresnelSchlick(float r0, float vDotH)
{
	return r0 + (1.0 - r0) * pow(1.0 - vDotH, 5.0);
}

// see http://graphicrants.blogspot.de/2013/08/specular-brdf-reference.html
// see http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float geometricShadowingSchlickBeckmann(float nDotV, float k)
{
	return nDotV / (nDotV * (1.0 - k) + k);
}

// see http://graphicrants.blogspot.de/2013/08/specular-brdf-reference.html
// see http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
float geometricShadowingSmith(float nDotL, float nDotV, float k)
{
	return geometricShadowingSchlickBeckmann(nDotL, k) * geometricShadowingSchlickBeckmann(nDotV, k);
}

vec3 brdfCookTorrence(vec3 material, vec3 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal, float roughness)
{
	// See https://github.com/McNopper/OpenGL/blob/master/Example32/shader/brdf.frag.glsl
	// basis, N, V, K
	// basis: tangent, bitangent, normal matrix, normal: normal vector, eye: eyeDir vector, k: roughness float

#if 0
	vec3 HtangentSpace = microfacetWeightedSampling(randomPoint);

	// Transform H to world space.
	vec3 H = basis * HtangentSpace;

	// Note: reflect takes incident vector.
	vec3 L = reflect(-V, H);

	float NdotL = dot(N, L);
	float NdotV = dot(N, V);
	float NdotH = dot(N, H);
#endif
	vec3 V = normalize(viewDir); // FIXME: I'm pretty sure this should be negative.
	vec3 H = normalize(reflect(normalize(-lightDir), normalize(normal)));
	vec3 L = normalize(lightDir);//reflect(-viewDir, H); // FIXME: Again, pretty sure this should be negative.

	float nDotL = dot(normal, L);
	float nDotV = dot(normal, V);
	float nDotH = dot(normal, H);

	// Lighted and visible?
	if (nDotL > 0 && nDotV > 0)
	{
		float vDotH = max(0, dot(V, H));

		// Fresnel.
		float refCoefficient = 1.0;
		float F = fresnelSchlick(refCoefficient, vDotH);

		// Geometric Shadowing.
		float G = geometricShadowingSmith(nDotL, nDotV, roughness);

		//
		// Lo = BRDF * L * NdotL / PDF
		//
		// BRDF is D * F * G / (4 * NdotL * NdotV).
		// PDF is D * NdotH / (4 * VdotH).
		// D and 4 * NdotL are canceled out, which results in this formula: Lo = color * L * F * G * VdotH / (NdotV * NdotH)		
		float colorFactor = F * G * vDotH / (nDotV * nDotH);

		// Note: Needed for robustness. With specific parameters, a NaN can be the result.
		if (isnan(colorFactor))
			return vec3(0);

		return material * lightColor * max(0, colorFactor);
		//return texture(u_panoramaTexture, panorama(L)).rgb * colorFactor;
	}

	return vec3(0);
}

vec3 brdfPhong(vec3 material, vec3 lightColor, vec3 lightDir, vec3 viewDir, vec3 normal, float shininess)
{
	vec3 reflection = reflect(-lightDir, normal);
	float nDotH = max(dot(viewDir, normalize(reflection)), 0.0);
	//float nDotH = abs(dot(viewDir, normalize(reflection)));
	float intensity = pow(nDotH, shininess);
	return intensity * lightColor;
}

vec3 applyLighting(vec3 esFrag, vec3 normal, vec3 lightDir, vec3 material, vec3 lightColor)
{
	// TODO: Now apply some lighting to the sphere. Assume a light source very far away perhaps?
	// TODO: Probably need a light colour.
	//vec3 lightDir = (mvMatrix * vec4(1, 1, 1, 0)).xyz;
	//vec3 lightColor = vec3(1);

	// TODO: Need an incoming peg colour for ambient, diffuse, specular.
	vec3 ambient = vec3(material * 0.4);
	float cosTheta = dot(normal, lightDir);
	float dp = clamp(cosTheta, 0.0, 1.0);

	vec3 diffuse = brdfLambert(material, lightColor, dp);
	vec3 specular = vec3(0);
	if (dp > 0)
		specular = brdfCookTorrence(material, lightColor, lightDir, normalize(-esFrag), normal, 2);
		//specular = brdfPhong(material, lightColor, lightDir, normalize(-esFrag), normal, 64);

	return ambient + diffuse + specular;
}




void main()
{
	vec3 dir = normalize(getEyeFromWindow(vec3(gl_FragCoord.xy, -30.0), viewport, invPMatrix).xyz);

	vec3 esFrag = FragIn.esFrag;
	vec3 esNorm = FragIn.norm;

	// FIXME: What's the correct way to do this?
	//vec3 camDir = normalize((VertexIn.osPos.xyz + osFrag.xyz) - cameraPos.xyz);
	vec3 camDir = dir;
	vec3 skyRef = normalize(reflect(camDir, esNorm));
	vec3 osSkyRef = (invMvMatrix * vec4(skyRef, 0)).xyz;
	vec3 skyReflectCol = texture(skybox, osSkyRef).rgb;

	float ratio = 1.0 / 1.52;
	skyRef = normalize(refract(camDir, esNorm, ratio));
	osSkyRef = (invMvMatrix * vec4(skyRef, 0)).xyz;
	vec3 skyRefractCol = texture(skybox, osSkyRef).rgb;

	//vec3 material = osFrag.xyz;
	//if (osFrag.x < 0.0 && osFrag.y < 0.0)
	//	material.yz = -osFrag.xy;
	//vec3 material = vec3(1.0, 0.0, 0.2);

	vec3 material = vec3(0.1, 0.1, 0.4);
	material += skyReflectCol;
	material += skyRefractCol;
	material = clamp(material, vec3(0), vec3(1));

	// Apply lighting from a single global light source, as well as all lit pegs.
	vec3 lightDir = (mvMatrix * vec4(-1, 1, 1, 0)).xyz;

	// TODO: If the peg is lit, then it should be brighter.
	// TODO: Or alternatively, add a glow effect around the peg (requires rendering a slightly larger quad).
	vec3 col = applyLighting(esFrag.xyz, esNorm, lightDir, material, vec3(1)) * 0.5;

	for (int i = 0; i < nLights; i++)
	{
		vec4 lightCol = floatToRGBA8(lightData[i].w);
		//vec4 lightCol = vec4(1, 1, 1, 1);
		vec3 esLightPos = (mvMatrix * vec4(lightData[i].xyz, 1.0)).xyz;

		// Attenuation.
		float dist = distance(esFrag.xyz, esLightPos.xyz);

		float sqAtt = 1.0 / (dist * dist); // Upping the distance a bit since the game world is so small.
		sqAtt *= sqAtt;
		float lAtt = max(0, 1 - (dist * dist) / (0.6 * 0.6)); // Linear attenuation, not great.
		lAtt *= lAtt;

		float att = max(sqAtt, lAtt);

		col += applyLighting(esFrag.xyz, esNorm, normalize(esLightPos.xyz - esFrag.xyz), material, lightCol.xyz) * att;
	}

	//vec3 col = vec3(0, 0, 1);

	fragColor[0] = vec4(col.xyz, 1);
	fragColor[1] = vec4(0, 0, 0, 1);
}

