#include "Controls.h"

#include "../../../Renderer/src/GL.h"

bool DebugControls::wireframeFlag;
bool DebugControls::cullFlag;


void DebugControls::toggleWireframe()
{
	if (wireframeFlag)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void DebugControls::toggleCulling()
{
	if (cullFlag)
		glEnable(GL_CULL_FACE);
	else
		glDisable(GL_CULL_FACE);
}

