#pragma once

#include "../../Math/src/MathCommon.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/Profiler.h"
#include "../../Renderer/src/Lighting/Bloom.h"

class App
{
public:
	enum GameMode
	{
		CAMERA,
		GAME
	};

private:
	float gravity;

	std::vector<Math::vec4> pegs;
	std::vector<bool> hitFlags;
	size_t nPegs;

	std::vector<Math::vec4> lights;
	std::vector<int> hitPegs;

	size_t nParticles;

	Math::vec3 shootPos;
	Math::vec3 shootDir;
	float shootSpeed;

	Math::vec3 ballPos;
	Math::vec3 ballVel;
	float ballRadius;

	Math::vec3 catchPos;
	Math::vec3 catchDir;
	float catchRadius;

	// One shader to handle each aspect of the game.
	Rendering::Shader pegSphereShader;
	Rendering::Shader pegPolyShader;

	Rendering::Shader shootShader;
	Rendering::Shader ballShader;
	Rendering::Shader catchShader;
	Rendering::Shader particleShader;
	Rendering::Shader particleInitShader;
	Rendering::Shader waterShader;
	Rendering::Shader cubemapShader;

	// Cubemap for the skybox.
	Rendering::TextureCubeMap cubemap;

	// A buffer to store the positions (vec4) of the pegs.
	Rendering::StorageBuffer pegBuffer;

	// And another buffer for lights.
	Rendering::StorageBuffer lightBuffer;

	// And again for the particle system.
	Rendering::StorageBuffer particlePosBuffer; // vec4: pos, colour.
	Rendering::StorageBuffer particleDirBuffer; // vec2: dir.
	Rendering::Texture2D particleTex;

	// Who doesn't like bloom?
	Rendering::Bloom bloom;

	Rendering::Profiler profiler;

	int ballsRemaining;
	int score;

	float dt;
	float t;

	int nParts;
	float particleTime;

	bool hudFlag;
	bool cheatFlag;

	int partsPerLight;
	std::vector<Math::vec2> randoms;
	Rendering::StorageBuffer randomBuffer;

	GameMode mode;

private:
	void drawPegs();
	void drawShooter();
	void drawBall();
	void drawTrajectory();
	void drawCatcher();
	void drawArena();
	void drawParticles();
	void drawWater();

	void killHitPegs();

	void drawHud();
	void drawSkybox();

	int collisionResponse(Math::vec3 &pos, Math::vec3 &vel);

	void spawnLight(int idx);

public:
	App() : gravity(-0.3f), nPegs(50), nParticles(0), shootSpeed(0.5f), ballRadius(0.02f), catchRadius(0.3f), ballsRemaining(5), score(0), dt(0), t(0), nParts(0), particleTime(0), hudFlag(false), cheatFlag(false), partsPerLight(1000), mode(CAMERA) {}
	~App() {}

	void init();

	void reloadShaders();

	void render();
	void update(float dt);

	void setShooter(float x, float y);
	void shootBall();

	void toggleHud() { hudFlag = !hudFlag; }
	void toggleCheat() { cheatFlag = !cheatFlag; }

	void enableCameraMode() { mode = CAMERA; }
	void enableGameMode() { mode = GAME; }
	GameMode getMode() const { return mode; }
};


