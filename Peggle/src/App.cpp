#include "App.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Resources/src/ResourcesCommon.h"
#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Math/src/Ray.h"

using namespace Math;
using namespace Rendering;


// TODO: Polygonal pegs. What kind of polygon? Upside down diamond maybe?
// TODO: Make sure particle spawning works on laptop.
// TODO: Figure out where laptop performance loss is coming from.

// TODO: Better looking boolm.

// TODO: Maybe have the shooter be a hemisphere with a cannon thingy.

// TODO: Reset.
// TODO: Ball loses energy on collisions?
// TODO: Ball comes to a rest on local minimums?

// TODO: Transparency.
// TODO: Circular arena.

// TODO: Unable to shoot new ball when pegs are dying.

// TODO: Physics in shaders.
// TODO: Collision in shaders.
// TODO: Path in shaders.
// TODO: Load arena type and pegs from a file.
// TODO: Grid for storing pegs.

// TODO: Shadows.
// TODO: Moving pegs.


#define BLOOM 0


Font testFont;
float tpf = 0;

static void initFont()
{
	static bool fontInitFlag = false;
	if (fontInitFlag)
		return;
	fontInitFlag = true;

	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");
}

static void printFramerate(float total)
{
	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);

	float smoothing = 0.9f; // Larger => more smoothing.
	tpf = (tpf * smoothing) + (total * (1.0f - smoothing));
	float t = round(total);
#if 0
	testFont.renderFromAtlas("Hybrid Lighting\nTime per frame(ms): " + Utils::toString(t) +
		"\nFrames per second (fps): " + Utils::toString(1000.0 / t) +
		"\nLights: " + Utils::toString(lightColors.size()), 10, 10, 0, 0, 1);
#else
	testFont.renderFromAtlas("Time per frame (ms): " + Utils::toString(t) +
		"\nFrames per second (fps): " + Utils::toString(1000.0 / t), 10, 10, 0, 1, 1, 1);
#endif

	glPopAttrib();
}

static void setUniforms(Shader &shader)
{
	auto &camera = Renderer::instance().getActiveCamera();

	auto mvMat = camera.getInverse();
	auto pMat = camera.getProjection();
	auto nMat = camera.getTransform().getMat3().transpose();
	auto invMvMat = camera.getTransform();
	auto invPMat = camera.getInverseProj();
	auto view = camera.getViewport();

	shader.setUniform("mvMatrix", mvMat);
	shader.setUniform("pMatrix", pMat);
	shader.setUniform("nMatrix", nMat);
	shader.setUniform("invMvMatrix", invMvMat);
	shader.setUniform("invPMatrix", invPMat);
	shader.setUniform("viewport", view);
}




void App::drawPegs()
{
	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);
	glDisable(GL_CULL_FACE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	pegSphereShader.bind();
	cubemap.bind();
	setUniforms(pegSphereShader);
	pegSphereShader.setUniform("PegData", &pegBuffer);
	if (!lights.empty())
		pegSphereShader.setUniform("LightData", &lightBuffer);
	pegSphereShader.setUniform("nLights", (int) lights.size());
	pegSphereShader.setUniform("skybox", cubemap);
	pegSphereShader.setUniform("dt", dt);
	//pegSphereShader.setUniform("g", gravity);
	glDrawArrays(GL_POINTS, 0, (GLsizei) nPegs);
	cubemap.unbind();
	pegSphereShader.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glPopAttrib();
}

void App::drawShooter()
{
	// Draw the shooter thingy. Seems like a waste to have a whole draw call just for this.
	// TODO: At the moment the cone rotates around its tip, may want to have it rotate around the base instead.
	shootShader.bind();
	setUniforms(shootShader);
	shootShader.setUniform("shooterPos", shootPos);
	shootShader.setUniform("shooterDir", shootDir);
	glDrawArrays(GL_POINTS, 0, 1);
	shootShader.unbind();
}

void App::drawBall()
{
	// Draw the ball.
	// TODO: Maybe check against world bounds rather than a hard-coded -y value.
	if (ballPos.y > -1)
	{
		ballShader.bind();
		setUniforms(ballShader);
		ballShader.setUniform("ballPos", ballPos);
		ballShader.setUniform("ballRadius", ballRadius);
		glDrawArrays(GL_POINTS, 0, 1);
		ballShader.unbind();
	}
}

void App::drawTrajectory()
{
	if (!pegBuffer.isGenerated())
		return;

	//vec4 *pegPositions = (vec4*) pegBuffer.read();

	// TODO: Draw the trajectory. Not really sure how this would work in a shader tbh.
	// TODO: For now just render this normally until I decide what to do.
	vec3 trajPos = shootPos;
	vec3 trajVel = shootDir * shootSpeed;

	if (ballPos.y > -1)
	{
		trajPos = ballPos;
		trajVel = ballVel;
	}

	int nCollisions = 0;

	glBegin(GL_LINE_STRIP);
	glColor3f(1, 1, 1);
	for (int canary = 1000; canary >= 0; canary--)
	{
		if (trajPos.y < -1)
		{
			trajPos.y = -1;
			glVertex3fv(&trajPos.x);
			break;
		}

		// Collisions with pegs.
		if (collisionResponse(trajPos, trajVel) >= 0)
			nCollisions++;

		// Hit enough pegs? Then stop.
		if (!cheatFlag && nCollisions >= 2)
			break;

		// Collisions with arena edge.
		if (trajPos.x + ballRadius > 1)
		{
			trajVel.reflect(vec3(1, 0, 0));
			trajPos.x = 1 - ballRadius;
		}
		else if (trajPos.x - ballRadius < -1)
		{
			trajVel.reflect(vec3(-1, 0, 0));
			trajPos.x = -1 + ballRadius;
		}

		glVertex3fv(&trajPos.x);

		float dt = 0.016f;
		trajVel.y += gravity * dt;
		trajPos += trajVel * dt;
	}
	glEnd();
}

void App::drawCatcher()
{
	catchShader.bind();
	setUniforms(catchShader);
	catchShader.setUniform("catchPos", catchPos);
	catchShader.setUniform("catchRadius", catchRadius);
	glDrawArrays(GL_POINTS, 0, 1);
	catchShader.unbind();
}

void App::drawArena()
{
	// TODO: It'd be cool if this was a continuous particle system.

	glPushAttrib(GL_CURRENT_BIT);
	glColor3f(0, 1, 1);

	glBegin(GL_LINES);
	glVertex3f(-1, 1, 0);
	glVertex3f(-1, -1, 0);
	glVertex3f(1, 1, 0);
	glVertex3f(1, -1, 0);
	glEnd();

	glPopAttrib();
}

void App::drawParticles()
{
	if (particleTime <= 0)
	{
		nParts = 0;
		return;
	}

	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);
	glDisable(GL_CULL_FACE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	particleShader.bind();
	particleTex.bind();

	setUniforms(particleShader);
	particleShader.setUniform("ParticlePosData", &particlePosBuffer);
	particleShader.setUniform("ParticleDirData", &particleDirBuffer);
	particleShader.setUniform("PegData", &pegBuffer);
	particleShader.setUniform("dt", dt);
	particleShader.setUniform("g", gravity);
	particleShader.setUniform("nPegs", (int) nPegs);
	particleShader.setUniform("particleTime", particleTime);
	particleShader.setUniform("particleTex", &particleTex);

	glDrawArrays(GL_POINTS, 0, nParts);

	particleTex.unbind();
	particleShader.unbind();

	glPopAttrib();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
}

void App::drawWater()
{
	// What tesselation?
	int tess = 1000;

	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	waterShader.bind();
	cubemap.bind();

	setUniforms(waterShader);
	if (!lights.empty())
		waterShader.setUniform("LightData", &lightBuffer);
	waterShader.setUniform("nLights", (int) lights.size());
	waterShader.setUniform("t", t);
	waterShader.setUniform("tess", tess);
	waterShader.setUniform("skybox", cubemap);

	glDrawArrays(GL_POINTS, 0, tess * tess);

	cubemap.unbind();
	waterShader.unbind();

	glPopAttrib();
}

void App::killHitPegs()
{
	score += (int) lights.size();
	nParts = (int) lights.size() * partsPerLight;

	particlePosBuffer.create(nullptr, nParts * sizeof(vec4));
	particleDirBuffer.create(nullptr, nParts * sizeof(vec2));

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Go through all the lights and spawn a particle system at those locations.
	glEnable(GL_RASTERIZER_DISCARD);
	particleInitShader.bind();

	particleInitShader.setUniform("ParticlePosData", &particlePosBuffer);
	particleInitShader.setUniform("ParticleDirData", &particleDirBuffer);
	particleInitShader.setUniform("RandomData", &randomBuffer);

	// Just initialise the data in a vertex shader. Hopefully it doesn't take too long.
	// One draw call per light may be too much, could do them all at once?
	int offset = 0;
	for (auto &lightData : lights)
	{
		// Position is xyz, colour is w.
		particleInitShader.setUniform("pos", vec3(lightData.xyz));
		particleInitShader.setUniform("color", lightData.w);
		particleInitShader.setUniform("offset", offset);

		glDrawArrays(GL_POINTS, 0, partsPerLight);

		// TODO: Not 100% sure if this is necessary or not, try it on the laptop.
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		offset += partsPerLight;
	}

	particleInitShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Keep them alive for 5 seconds maybe?
	particleTime = 5;

	// No more lights.
	lights.clear();

	// Remove these from the pegs array.
	int nRemoved = 0;
	for (size_t i = 0; i < pegs.size(); i++)
	{
		for (size_t j = 0; j < hitPegs.size(); j++)
		{
			int idx = hitPegs[j] - nRemoved;
			if (idx == (int) i)
			{
				pegs.erase(pegs.begin() + idx);
				i--;
				nRemoved++;
				break;
			}
		}
	}

	// hitFlags should now all be false.
	hitFlags.clear();
	hitFlags.resize(pegs.size(), false);

	// Clear the hitPegs.
	hitPegs.clear();

	// Update the peg buffer.
	nPegs = pegs.size();
	pegBuffer.bufferData(&pegs[0].x, sizeof(vec4) * nPegs);
}

void App::drawHud()
{
	if (!hudFlag)
		return;

	testFont.renderFromAtlas("Balls remaining: " + Utils::toString(ballsRemaining) +
		"\nScore: " + Utils::toString(score), 10, 50, 0, 1, 1, 1);
}

void App::drawSkybox()
{
	// Could generate the cube in a geometry shader instead I suppose...
	static Mesh cube = Mesh::getCube();

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_DEPTH_BUFFER_BIT);

	glDisable(GL_CULL_FACE);

	glEnable(GL_DEPTH_TEST);
	glDepthMask(GL_FALSE);

	glEnable(GL_TEXTURE_2D);

	// No translations...
	auto mv = camera.getInverse();
	mv.t[3] = vec4(0, 0, 0, 1);

	cubemapShader.bind();
	cubemap.bind();
	cubemapShader.setUniform("mvMatrix", mv);
	cubemapShader.setUniform("pMatrix", camera.getProjection());
	cubemapShader.setUniform("skybox", &cubemap);

	cube.renderImmediate();

	cubemap.unbind();
	cubemapShader.unbind();

	glPopAttrib();
}

int App::collisionResponse(Math::vec3 &pos, Math::vec3 &vel)
{
	for (size_t i = 0; i < nPegs; i++)
	{
		vec3 pegPos = pegs[i].xyz;
		float dist = pegPos.distance(pos);
		if (dist <= (0.02f + 0.04f))
		{
			//vec3 norm = (ballPos - pegPos).normalize();
			//vec3 ballDir = ballVel.unit();

			//float ballSpeed = ballVel.len();
			//ballDir.reflect(norm);
			//ballVel = ballDir * ballSpeed;

			// 1D elastic collisions.
			// where u1 = initial ball velocity, u2 = initial peg velocity.
			// v1 = new ball velocity, v2 = new peg velocity.
			// v1 = ((m1 - m2) / (m1 + m2)) * u1 + ((2 * m2) / (m1 + m2)) * u2;
			// v2 = ((2 * m1) / (m1 + m2)) * u1 + ((m2 - m1) / (m1 + m2)) * u2;
			// If both masses are the same, v1 = u2, v2 = u1

			// 2D/3D elastic collisions (with no angles).
			// where x1 = ball centre, x2 = peg centre
			// v1 = u1 - (((2 * m2) / (m1 + m2)) * ((u1 - u2).dot(x1 - x2) / (x1 - x2).lenSq()) * (x1 - x2));
			// v2 = u2 - (((2 * m1) / (m1 + m2)) * ((u2 - u1).dot(x2 - x1) / (x2 - x1).lenSq()) * (x2 - x1));
			float m1 = 3, m2 = 10;
			vec3 u1 = vel, u2 = vec3(0, 0, 0);
			vec3 x1 = pos, x2 = pegPos;
			vec3 v1 = u1 - (((2 * m2) / (m1 + m2)) * ((u1 - u2).dot(x1 - x2) / (x1 - x2).lenSq()) * (x1 - x2));
			vel = v1;

			// TODO: What about elastic collisions with polygonal surfaces?

			// Push the ball away from the peg slightly.
			while (dist <= (0.02f + 0.04f))
			{
				pos += vel * 0.01f;
				dist = pegPos.distance(pos);
			}

			return (int) i;
		}
	}

	return -1;
}

void App::spawnLight(int idx)
{
	vec4 light = pegs[idx];

	lights.push_back(light);
	hitPegs.push_back(idx);

	lightBuffer.create(&lights[0].x, sizeof(vec4) * lights.size());
}





void App::init()
{
	initFont();

	// Initialise peg positions/types.
	pegBuffer.release();

	// Initialise the shooter.
	shootPos = vec3(0, 1.25, 0);
	shootDir = vec3(0, -1, 0);

	catchPos = vec3(0, -1.1, 0);
	catchDir = vec3(1, 0, 0);

	// Just give the ballPos a really low position to start with.
	ballPos.y = -1000;

	// TODO: Actual peg positions should probably be loaded from a file, for now just do it randomly.
	// TODO: Decide how to handle different peg types as well.
	// TODO: No two pegs should be overlapping.
	//std::vector<vec4> pegPositions(nPegs);
	pegs.resize(nPegs);
	hitFlags.resize(nPegs, false);
	for (size_t i = 0; i < nPegs; i++)
	{
		pegs[i].x = Utils::getRand(-1.0f, 1.0f);
		pegs[i].y = Utils::getRand(-1.0f, 1.0f);

		// Keep everything 2D for now.
		//pegPositions[i].z = Utils::getRand(-1.0f, 1.0f);

		// Can store a colour in the 4th variable.
		// Maybe just make the pegs either red, green or blue.
	#if 0
		int r = Utils::getRand(0, 3);
		vec4 col = vec4(r == 0 ? 1.0f : 0.0f, r == 1 ? 1.0f : 0.0f, r == 2 ? 1.0f : 0.0f, 1.0f);
		float w = Utils::rgba8ToFloat(col);
	#else
		// Or a more random colour?
		float r = Utils::getRand(0.0f, 1.0f);
		float g = Utils::getRand(0.0f, 1.0f);
		float b = Utils::getRand(0.0f, 1.0f);
		float w = Utils::rgba8ToFloat(vec4(r, g, b, 1.0f));
	#endif
		pegs[i].w = w;
	}

	pegBuffer.create(&pegs[0], sizeof(vec4) * nPegs);

	// Skybox cubemap.
	static Image images[6];

	// Needs to match the order of the GL_TEXTURE_CUBE_MAP faces.
	Resources::ImageLoader::load(&images[0], "images/skybox/right.jpg");
	Resources::ImageLoader::load(&images[1], "images/skybox/left.jpg");
	Resources::ImageLoader::load(&images[2], "images/skybox/bottom.jpg");
	Resources::ImageLoader::load(&images[3], "images/skybox/top.jpg");
	Resources::ImageLoader::load(&images[4], "images/skybox/front.jpg");
	Resources::ImageLoader::load(&images[5], "images/skybox/back.jpg");

	cubemap.generate();
	for (int i = 0; i < 6; i++)
		cubemap.bufferData(images[i].getFormat(), i, images[i].getData(), images[i].getDimensions().x, images[i].getDimensions().y);

	reloadShaders();

	Renderer::instance().getActiveCamera().setZoom(2);

	static Image particleImg;
	Resources::ImageLoader::load(&particleImg, "images/particle.png");
	particleTex.create(particleImg.getData(), particleImg.getFormat(), particleImg.getDimensions().x, particleImg.getDimensions().y);

	// Some random particle directions.
	if (randoms.empty())
	{
		randoms.resize(partsPerLight);
		for (size_t i = 0; i < randoms.size(); i++)
		{
			randoms[i].x = Utils::getRand(-1.0f, 1.0f);
			randoms[i].y = Utils::getRand(-1.0f, 1.0f);
			randoms[i].normalize();
			float randSpeed = Utils::getRand(0.05f, 0.2f);
			randoms[i] *= randSpeed;
		}
		randomBuffer.create(&randoms[0].x, sizeof(vec2) * partsPerLight);
	}
}

void App::reloadShaders()
{
	std::cout << "Reloading shaders\n";

	profiler.clear();

	bloom.reloadShaders();

	// Pegs shader.
	auto pegSphereVert = ShaderSourceCache::getShader("pegSphereVert").loadFromFile("shaders/pegSphere.vert");
	auto pegSphereGeom = ShaderSourceCache::getShader("pegSphereGeom").loadFromFile("shaders/pegSphere.geom");
	auto pegSphereFrag = ShaderSourceCache::getShader("pegSphereFrag").loadFromFile("shaders/pegSphere.frag");
	pegSphereShader.release();
	pegSphereShader.create(&pegSphereVert, &pegSphereFrag, &pegSphereGeom);

	auto pegPolyVert = ShaderSourceCache::getShader("pegPolyVert").loadFromFile("shaders/pegPoly.vert");
	auto pegPolyGeom = ShaderSourceCache::getShader("pegPolyGeom").loadFromFile("shaders/pegPoly.geom");
	auto pegPolyFrag = ShaderSourceCache::getShader("pegPolyFrag").loadFromFile("shaders/pegPoly.frag");
	pegPolyShader.release();
	pegPolyShader.create(&pegPolyVert, &pegPolyFrag, &pegPolyGeom);

	// Shooter shader.
	auto shootVert = ShaderSourceCache::getShader("shootVert").loadFromFile("shaders/shoot.vert");
	auto shootGeom = ShaderSourceCache::getShader("shootGeom").loadFromFile("shaders/shoot.geom");
	auto shootFrag = ShaderSourceCache::getShader("shootFrag").loadFromFile("shaders/shoot.frag");
	shootShader.release();
	shootShader.create(&shootVert, &shootFrag, &shootGeom);

	// Ball shader.
	auto ballVert = ShaderSourceCache::getShader("ballVert").loadFromFile("shaders/ball.vert");
	auto ballGeom = ShaderSourceCache::getShader("ballGeom").loadFromFile("shaders/ball.geom");
	auto ballFrag = ShaderSourceCache::getShader("ballFrag").loadFromFile("shaders/ball.frag");
	ballShader.release();
	ballShader.create(&ballVert, &ballFrag, &ballGeom);

	// Trajectory shader.
	// TODO: Would be cool if this was a curve made of particles.
#if 0
	auto trajVert = ShaderSourceCache::getShader("trajVert").loadFromFile("shaders/traj.vert");
	auto trajGeom = ShaderSourceCache::getShader("trajGeom").loadFromFile("shaders/traj.geom");
	auto trajFrag = ShaderSourceCache::getShader("trajFrag").loadFromFile("shaders/traj.frag");
	trajShader.release();
	trajShader.create(&trajVert, &trajFrag, &trajGeom);
#endif

	// Catcher shader.
	auto catchVert = ShaderSourceCache::getShader("catchVert").loadFromFile("shaders/catch.vert");
	auto catchGeom = ShaderSourceCache::getShader("catchGeom").loadFromFile("shaders/catch.geom");
	auto catchFrag = ShaderSourceCache::getShader("catchFrag").loadFromFile("shaders/catch.frag");
	catchShader.release();
	catchShader.create(&catchVert, &catchFrag, &catchGeom);

	// Particles.
	auto partVert = ShaderSourceCache::getShader("partVert").loadFromFile("shaders/particles.vert");
	auto partGeom = ShaderSourceCache::getShader("partGeom").loadFromFile("shaders/particles.geom");
	auto partFrag = ShaderSourceCache::getShader("partFrag").loadFromFile("shaders/particles.frag");
	particleShader.release();
	particleShader.create(&partVert, &partFrag, &partGeom);

	// Creating the particles.
	auto partInitVert = ShaderSourceCache::getShader("partInitVert").loadFromFile("shaders/particleInit.vert");
	particleInitShader.release();
	particleInitShader.create(&partInitVert);

	// Drawing the water.
	auto waterVert = ShaderSourceCache::getShader("waterVert").loadFromFile("shaders/water.vert");
	auto waterGeom = ShaderSourceCache::getShader("waterGeom").loadFromFile("shaders/water.geom");
	auto waterFrag = ShaderSourceCache::getShader("waterFrag").loadFromFile("shaders/water.frag");
	waterShader.release();
	waterShader.create(&waterVert, &waterFrag, &waterGeom);

	// Drawing the cubemap.
	auto cubemapVert = ShaderSourceCache::getShader("cubemapVert").loadFromFile("shaders/cubemap.vert");
	auto cubemapFrag = ShaderSourceCache::getShader("cubemapFrag").loadFromFile("shaders/cubemap.frag");
	cubemapShader.release();
	cubemapShader.create(&cubemapVert, &cubemapFrag);
}

void App::render()
{
	if (hudFlag)
	{
		profiler.begin();
		profiler.start("total");
	}

	auto &camera = Renderer::instance().getActiveCamera();

#if BLOOM
	bloom.resize(camera.getWidth(), camera.getHeight());
#endif

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);

#if BLOOM
	bloom.beginCapture();
#endif

	drawSkybox();

	drawWater();

	drawPegs();
	drawShooter();
	drawBall();
	drawCatcher();

	drawParticles();

	// Anything else?
	glDisable(GL_DEPTH_TEST);
	drawTrajectory();
	drawArena();

	Renderer::instance().drawAxes();

#if BLOOM
	bloom.endCapture();

	// Blur the bright texture and composite.
	bloom.applyBlur();
	bloom.composite();

	//bloom.debugRenderColor();
	//bloom.debugRenderBright();
	//bloom.debugRenderBlur(3);
#endif

	drawHud();

	glPopAttrib();

	// Print the framerate.
	if (hudFlag)
	{
		profiler.time("total");
		float total = profiler.getTime("total");
		printFramerate(total);
	}
}

void App::update(float dt)
{
	// Only update at 60fps.
	if (this->dt >= 0.016f)
		this->dt = 0;

	this->dt += dt;
	if (this->dt >= 0.016f)
		this->dt = 0.016f;
	else
		return;

	t += this->dt;
	if (particleTime > 0)
		particleTime -= this->dt;

	//if (this->dt <= 0)
	//	return;

	// Update position of the ball using velocity/acceleration.
	ballVel.y += gravity * this->dt;
	ballPos += ballVel * this->dt;

	// Update the position of the catcher.
	catchPos += catchDir * this->dt * 0.2f;
	if (catchPos.x + catchRadius > 1 || catchPos.x - catchRadius < -1)
	{
		catchPos.x = clamp(catchPos.x, -1.0f, 1.0f);
		catchDir = -catchDir;
	}

	// Collisions between ball and pegs. This only follows the path if dt is 0.016 (since that's the dt we use for path drawing).
	// TODO: This, but in the vertex shader.
	int col = collisionResponse(ballPos, ballVel);
	if (col >= 0 && !hitFlags[col])
	{
		spawnLight(col);
		hitFlags[col] = true;
	}

	// If the ball hits the edge of the arena, reverse direction.
	if (ballPos.x + ballRadius > 1)
	{
		ballVel.reflect(vec3(-1, 0, 0));
		ballPos.x = 1 - ballRadius;
	}
	else if (ballPos.x - ballRadius < -1)
	{
		ballVel.reflect(vec3(1, 0, 0));
		ballPos.x = -1 + ballRadius;
	}

	// If the ball hits the bottom, kill the hit pegs and spawn a bunch of particles.
	if (ballPos.y <= -1 && ballPos.y > -2)
	{
		// Give the ball a really low position, this is how the game knows it's ready to fire again.
		ballPos.y = -1000;

		killHitPegs();

		bool caughtFlag = false;
		if (ballPos.x - ballRadius < catchPos.x + catchRadius && ballPos.x + ballRadius > catchPos.x - catchRadius)
			caughtFlag = true;

		if (!caughtFlag)
		{
			ballsRemaining--;
			std::cout << "Ball not caught! ";
		}
		else
			std::cout << "Ball caught! ";
		std::cout << "Balls remaining: " << ballsRemaining << "\n";
	}
}

void App::setShooter(float x, float y)
{
	// Aim the shooter at the x,y cursor position.
	auto &camera = Renderer::instance().getActiveCamera();
	auto ray = Math::Ray::screenToRay((int) x, (int) y, camera.getInverse(), camera.getProjection(), camera.getViewport(), camera.getNear(), camera.getFar());

	// What value t gives z of 0?
	float t = -ray.origin.z / ray.dir.z;
	auto hitPos = ray.origin + (t * ray.dir);

	// Set hitPos.z to 0, because it might just be an infinitessimally small number.
	hitPos.z = 0;

	// Shoot direction is dir from shooter to hitPos.
	shootDir = (hitPos - shootPos).normalize();
}

void App::shootBall()
{
	if (ballPos.y > -1)
		return;

	ballPos = shootPos;
	ballVel = shootDir * shootSpeed;
}

