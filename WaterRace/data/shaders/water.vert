#version 450

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

void main()
{
	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

	gl_Position = csVert;
}

