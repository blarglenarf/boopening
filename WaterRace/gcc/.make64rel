####################################################################
# child makefile, for different builds of a specific project
####################################################################


####################################################################
#### Modify any of these settings to suit your specific project ####
####################################################################

# Platform path (64 or 32 bit)
PLATFORM_PATH := x64

# Version path (Debug or Release)
VERSION_PATH := Release

# Path of folder to place all build files/folders
BUILD_PATH := .build

# Release or debug?
CFLAGS += $(RLS_CFLAGS)

# 32 or 64 bit?
CFLAGS += $(64_CFLAGS)

# Binary
BIN := $(BIN)64

# Extra libs
LDFLAGS += -lPlatform64 -lUtils64





###############################################################
#### You shouldn't need to modify anything below this line ####
###############################################################

# Full Object path
OBJ_PATH := $(BUILD_PATH)/$(PLATFORM_PATH)/$(VERSION_PATH)

# Temporary folders created for the object files, mimicking structure of source files
DIRS := $(addprefix $(OBJ_PATH)/, $(filter-out ./, $(dir $(OBJECTS))))

# Add object file path, now that temporary folder info has been created
OBJECTS := $(OBJECTS:%.o=$(OBJ_PATH)/%.o)

# Dependency files
DEPS := $(OBJECTS:.o=.d)


# Standard compilation command, taking into account different build settings
.PHONY: all
all: dirs $(BIN_PATH)/$(BIN)

# Link command for the exe, will compile object files first if necessary
$(BIN_PATH)/$(BIN) : $(OBJECTS)
	@echo linking $(BIN_PATH)/$(BIN)
	$(CMD_PREFIX)$(LD) $(CMD_OPT) $(BIN_PATH)/$(BIN) $(OBJECTS) $(LIBS) $(LDFLAGS)

# Removes all build files
.PHONY: clean
clean:
	@echo "Removing dependencies"
	$(CMD_PREFIX)$(RM) $(DEPS)
	@echo "Removing object files"
	$(CMD_PREFIX)$(RM) $(OBJECTS)
ifneq ($(strip $(DIRS)),)
	@echo "Removing folders"
	$(CMD_PREFIX)rmdir $(DIRS)
endif

# All folders that need to be created for the object files
.PHONY: dirs
dirs:
	@mkdir -p $(BUILD_PATH)
	@mkdir -p $(BUILD_PATH)/$(PLATFORM_PATH)
	@mkdir -p $(BUILD_PATH)/$(PLATFORM_PATH)/$(VERSION_PATH)
ifneq ($(strip $(DIRS)),)
	@echo "Creating folders"
	@mkdir -p $(DIRS)
endif

# Add dependency files, if they exist
-include $(DEPS)

# Source file rules
$(OBJECTS): $(OBJ_PATH)/%.o: $(SRC_PATH)/%.$(SRC_EXT)
	@echo "Compiling: $< -> $@"
	$(CMD_PREFIX)$(CC) $(INCLUDES) $(CFLAGS) -MP -MMD -c $< -o $@
