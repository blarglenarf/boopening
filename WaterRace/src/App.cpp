#include "App.h"

#include "MathCommon.h"
#include "ResourcesCommon.h"

using namespace Rendering;
using namespace Math;
using namespace Resources;

void App::init()
{
	glEnable(GL_TEXTURE_2D);
	glColor3f(1, 1, 1);
	
	auto vert = ShaderSource::getPhongVertShader();
	auto frag = ShaderSource::getPhongFragShader();
	shader.create(&vert, &frag);

	auto waterVert = ResourceLoader::instance().getShaderSource("waterV");
	auto waterFrag = ResourceLoader::instance().getShaderSource("waterF");
	waterShader.create(waterVert, waterFrag);

	auto cube = Mesh::getSphere();
	auto grid = Mesh::getGrid();
	auto quad = Mesh::getSquare();

	vaoCube.create(&cube);
	vaoGrid.create(&grid);
	vaoQuad.create(&quad);

	Renderer::instance().getActiveCamera().updateZoom(20);
	Renderer::instance().getActiveCamera().updateRotation(30, -30);
}

void App::render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	auto &camera = Renderer::instance().getActiveCamera();
	camera.upload();

	glViewport(0, 0, 512, 512);
	
	// TODO: not get the uniform locations every frame
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().transpose()); // Transpose of inverse of modelview, also TODO: use a mat3 for this

	shader.setUniform("ambientMat", Math::vec3(0.2f, 0.0f, 0.0f));
	shader.setUniform("diffuseMat", Math::vec3(0.5f, 0.0f, 0.0f));
	shader.setUniform("specularMat", Math::vec3(1, 1, 1));

	vaoCube.render();
	shader.unbind();

	waterShader.bind();
	waterShader.setUniform("mvMatrix", camera.getInverse());
	waterShader.setUniform("pMatrix", camera.getProjection());
	//waterShader.setUniform("nMatrix", camera.getTransform().transpose());

	vaoGrid.render();
	waterShader.unbind();
	
	Renderer::drawAxes();
}

void App::update(float dt)
{
	(void) (dt);
}
