#include <cstdlib>

#include "Controls.h"
#include "../App.h"

#include "RendererCommon.h"
#include "PlatformCommon.h"
#include "ResourcesCommon.h"

#define UNUSED(x) (void)(x)

using namespace Rendering;
using namespace Platform;
using namespace Resources;


static void render(App &app)
{
	Renderer::instance().getActiveCamera().setViewport(0, 0, Window::getWidth(), Window::getHeight());
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	Renderer::instance().getActiveCamera().upload();

	app.render();
}

static void update(App &app, float dt)
{
	Renderer::instance().getActiveCamera().update(dt);
	app.update(dt);
}

static bool mouseDrag(Event &ev)
{
	if (Mouse::isDown(Mouse::LEFT))
		Renderer::instance().getActiveCamera().updateRotation(ev.dx, ev.dy);
	if (Mouse::isDown(Mouse::RIGHT))
		Renderer::instance().getActiveCamera().updateZoom(ev.dy);
	return false;
}

static bool keyPress(Event &ev)
{
	switch (ev.key)
	{
	case Keyboard::ESCAPE:
	case Keyboard::q:
		exit(EXIT_SUCCESS);
		break;

	case Keyboard::p:
		DebugControls::wireframeFlag = !DebugControls::wireframeFlag;
		DebugControls::toggleWireframe();
		break;

	case Keyboard::c:
		DebugControls::cullFlag = !DebugControls::cullFlag;
		DebugControls::toggleCulling();
		break;

	default:
		break;
	}

	return false;
}


int main(int argc, char **argv)
{
	UNUSED(argc);
	UNUSED(argv);

	Window::createWindow(512, 512);
	EventHandler::addHandler(Event::Type::KEY_PRESSED, &keyPress);
	EventHandler::addHandler(Event::Type::MOUSE_DRAG, &mouseDrag);

	App app;
	auto &cam = Renderer::instance().addCamera("main", Camera(Camera::Type::PERSP));

	Renderer::instance().init();
	app.init();

	bool finished = false;
	while (!finished)
	{
		float dt = Window::getDeltaTime();

		// First approach for event handling, using callback functions provided above.
		finished = Window::handleEvents();
#if 0
		// Alternative approach, loop through the events and handle each in the switch statement.
		Window::processEvents();
		for (auto &ev : EventHandler::events)
		{
			switch (ev.type)
			{
			case Event::Type::KEY_PRESSED:
				keyPress(ev);
				break;
			case Event::Type::MOUSE_DRAG:
				mouseDrag(ev);
				break;
			default:
				break;
			}
		}
#endif
#if 0
		// Yet another alternative approach, just ask the keyboard and mouse if something has happened.
		Window::processEvents();
		if (Keyboard::isPressed(Keyboard::ESCAPE) || Keyboard::isPressed(Keyboard::q))
			exit(EXIT_SUCCESS);
		if (Keyboard::isPressed(Keyboard::p))
		{
			DebugControls::wireframeFlag = !DebugControls::wireframeFlag;
			DebugControls::toggleWireframe();
		}
		if (Keyboard::isPressed(Keyboard::c))
		{
			DebugControls::cullFlag = !DebugControls::cullFlag;
			DebugControls::toggleCulling();
		}
		if (Mouse::isDown(Mouse::LEFT))
			Renderer::instance().getActiveCamera().updateRotation(Mouse::dx, Mouse::dy);
		if (Mouse::isDown(Mouse::RIGHT))
			Renderer::instance().getActiveCamera().updateZoom(Mouse::dy);
#endif
		update(app, dt);
		render(app);

		Window::swapBuffers();
	}

	Window::destroyWindow();

	return EXIT_SUCCESS;
}
