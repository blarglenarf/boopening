#pragma once

#include "RendererCommon.h"

class App
{
private:
	Rendering::VertexArrayObject vaoCube, vaoGrid, vaoQuad;
	Rendering::Shader shader, waterShader;

public:
	App() {}
	~App() {}

	void init();

	void render();
	void update(float dt);
};
