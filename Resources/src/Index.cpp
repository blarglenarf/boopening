#include "Index.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

using namespace Resources;

std::string Index::getCommonPath()
{
	return "../Data/";
}

std::string Index::getFilePath(const std::string &filename)
{
	std::string path = filename;

	if (!Utils::File::exists(path))
	{
		path = Index::getCommonPath() + path;

		// If there are a bunch of '..' in the path, then get rid of them if they have preceding folders.
		auto tokens = Utils::split(path, "/\\");
		std::string newPath = "";
		int back = 0;
		for (int i = (int) tokens.size() - 1; i > 0; i--)
		{
			if (tokens[i] == "..")
				back++;
			else
			{
				int end = tokens[0] == ".." ? 1 : 0;
				while (back > 0 && i > end)
				{
					i--;
					back--;
				}
				if (newPath == "")
					newPath = tokens[i];
				else
					newPath = tokens[i] + "/" + newPath;
			}
		}
		if (tokens[0] == "..")
			newPath = "../" + newPath;
		//std::cout << path << " ... " << newPath << "\n";

		path = newPath;

		if (!Utils::File::exists(path))
		{
			std::cout << "Failed to locate path for file " << filename << "\n";
			return filename;
		}
	}

	return path;
}

