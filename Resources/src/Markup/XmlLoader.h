#pragma once

#include <string>
#include <vector>
#include <map>

#include "pugixml/pugixml.hpp"

namespace Rendering
{
	class Scene;
}

namespace Resources
{
	class XmlData
	{
	public:
		struct XmlDataIterator
		{
			std::vector<XmlData>::iterator it;
			XmlDataIterator(std::vector<XmlData>::iterator it) : it(it) {}
			const XmlDataIterator &operator ++ () { it++; return *this; }
			bool operator != (const XmlDataIterator &iter) { return iter.it != it; }
			XmlData &operator * () { return *it; }
		};

	public:
		std::map<std::string, std::string> attrs;
		std::vector<XmlData> childElements;
		std::string type;
		std::string text;

	public:
		XmlData() {}
		XmlData(pugi::xml_node &parent) { populate(parent); }
		~XmlData() {}

		void populate(pugi::xml_node &parent);

		std::string &operator [] (const std::string &name);

		XmlDataIterator begin() { return XmlDataIterator(childElements.begin()); }
		XmlDataIterator end() { return XmlDataIterator(childElements.end()); }
	};

	class XmlLoader
	{
	public:
		static bool load(XmlData *data, const std::string &filename);

		static bool loadScenes(std::vector<Rendering::Scene> &scenes, const std::string &filename);
	};
}

