#include "SceneLoader.h"
#include "XmlLoader.h"
#include "../Index.h"
#include "../../../Utils/src/StringUtils.h"

using namespace Resources;

bool SceneLoader::load(std::vector<Rendering::Scene> &scenes, const std::string &filename)
{
	auto ext = Utils::getExtension(filename);
	auto path = Index::getFilePath(filename);

	if (ext == "xml")
		return XmlLoader::loadScenes(scenes, path);

	return false;
}

