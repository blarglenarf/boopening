#pragma once

#include <string>
#include <vector>

namespace Rendering
{
	class Scene;
}

namespace Resources
{
	class SceneLoader
	{
	public:
		static bool load(std::vector<Rendering::Scene> &scenes, const std::string &filename);
	};
}

