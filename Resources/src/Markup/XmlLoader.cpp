#include "XmlLoader.h"
#include "../Index.h"

#include "../../../Renderer/src/RenderResource/Scene.h"
#include "../../../Utils/src/StringUtils.h"

#include <iostream>

using namespace Resources;
using namespace Rendering;

void XmlData::populate(pugi::xml_node &parent)
{
	childElements.clear();
	attrs.clear();

	text = parent.child_value();
	type = Utils::tolower(parent.name());

	for (pugi::xml_attribute attr = parent.first_attribute(); attr; attr = attr.next_attribute())
		attrs[Utils::tolower(attr.name())] = Utils::tolower(attr.value());

	for (pugi::xml_node child = parent.first_child(); child; child = child.next_sibling())
		childElements.push_back(XmlData(child));
}

std::string &XmlData::operator [] (const std::string &name)
{
	static std::string nope = "";
	auto a = attrs.find(Utils::tolower(name));
	if (a == attrs.end())
		return nope;
	return a->second;
}


bool XmlLoader::load(XmlData *data, const std::string &filename)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(filename.c_str());
	if (!result)
	{
		std::cout << "Error loading file " << filename << ": " << result.description() << "\n";
		return false;
	}

	pugi::xml_node parent = doc.first_child();
	data->populate(parent);
	//auto a = xmlFiles.find(indexTag);
	//if (a != xmlFiles.end())
	//	throw std::runtime_error("Error, " + indexTag + " already exists in file " + filename);
	//xmlFiles[indexTag].init(parent);
	return true;
}

bool XmlLoader::loadScenes(std::vector<Rendering::Scene> &scenes, const std::string &filename)
{
	XmlData data;
	bool loadFlag = load(&data, filename);
	if (!loadFlag)
		return false;

	for (auto &sceneData : data)
	{
		if (sceneData.type != "scene")
			continue;
		std::string name = sceneData["name"];
		scenes.push_back(Scene());
		Scene &scene = scenes[scenes.size() - 1];

		std::vector<Scene::SceneObject> sceneObjects;

		for (auto &sceneChild : sceneData)
		{
			if (sceneChild.type == "mesh")
			{
				sceneObjects.push_back(Scene::SceneObject());
				Scene::SceneObject &obj = sceneObjects[sceneObjects.size() - 1];

				obj.meshName = sceneChild["name"];
				obj.file = sceneChild["file"];
				std::string bake = sceneChild["bake"];
				if (!bake.empty())
					obj.bakeFlag = Utils::tobool(bake);

				std::vector<std::string> posToks = Utils::split(sceneChild["pos"], ", \t");
				std::vector<std::string> rotToks = Utils::split(sceneChild["rot"], ", \t");
				std::vector<std::string> scaleToks = Utils::split(sceneChild["scale"], ", \t");
				std::vector<std::string> scaleMinToks = Utils::split(sceneChild["scalemin"], ", \t");
				std::vector<std::string> scaleMaxToks = Utils::split(sceneChild["scalemax"], ", \t");

				if (!posToks.empty())
					obj.pos = Math::vec3(std::stof(posToks[0]), std::stof(posToks[1]), std::stof(posToks[2]));
				if (!rotToks.empty())
					obj.rot = Math::vec3(std::stof(rotToks[0]), std::stof(rotToks[1]), std::stof(rotToks[2]));
				if (!scaleToks.empty())
					obj.scale = Math::vec3(std::stof(scaleToks[0]), std::stof(scaleToks[1]), std::stof(scaleToks[2]));
				if (!scaleMinToks.empty())
					obj.scaleMin = Math::vec3(std::stof(scaleMinToks[0]), std::stof(scaleMinToks[1]), std::stof(scaleMinToks[2]));
				if (!scaleMaxToks.empty())
					obj.scaleMax = Math::vec3(std::stof(scaleMaxToks[0]), std::stof(scaleMaxToks[1]), std::stof(scaleMaxToks[2]));
			}
			else if (sceneChild.type == "camera")
			{
				//std::string name = sceneChild["name"];
				//std::string type = sceneChild["type"];
				//std::string pos = sceneChild["pos"];
				//std::string rot = sceneChild["rot"];
				//std::string zoom = sceneChild["zoom"];
			}
			else if (sceneChild.type == "light")
			{
				//std::string name = sceneChild["name"];
				//std::string pos = sceneChild["pos"];
				//std::string dir = sceneChild["dir"];
			}
		}

		scene.load(name, std::move(sceneObjects));
	}

	return true;
}

