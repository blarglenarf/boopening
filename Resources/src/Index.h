#pragma once

#include <string>

namespace Resources
{
	class Index
	{
	public:
		static std::string getCommonPath();

		static std::string getFilePath(const std::string &filename);
	};
}

