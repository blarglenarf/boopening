#include "ImageLoader.h"
#include "../Index.h"
#include "../../../Renderer/src/RenderResource/Image.h"
#include "SOIL2/SOIL2.h"
#include "../../../Utils/src/StringUtils.h"

#include <cstring>
#include <map>

using namespace Resources;

bool ImageLoader::load(Rendering::Image *img, const std::string &filename)
{
	auto path = Index::getFilePath(filename);
	int width, height, channels;

	// TODO: modify stbi so it uses new [] instead.
	// SOIL malloc's the data, whereas the Image expects it to have been new'd.
	unsigned char *tmpData = SOIL_load_image(path.c_str(), &width, &height, &channels, SOIL_LOAD_AUTO);
	if (!tmpData)
		return false;

	unsigned char *data = new unsigned char[width * height * channels];
	memcpy(data, tmpData, width * height * channels);
	free(tmpData);

	img->load(data, width * height * channels, channels, Math::ivec2(width, height));

	// TODO: figure out if the image *really* needs to be flipped or not
	img->flip();

	return true;
}

bool ImageLoader::save(Rendering::Image *img, const std::string &filename)
{
	static std::map<std::string, int> imgTypes;
	if (imgTypes.empty())
	{
		imgTypes["tga"] = SOIL_SAVE_TYPE_TGA;
		imgTypes["bmp"] = SOIL_SAVE_TYPE_BMP;
		imgTypes["png"] = SOIL_SAVE_TYPE_PNG;
		imgTypes["dds"] = SOIL_SAVE_TYPE_DDS;
		imgTypes["jpg"] = SOIL_SAVE_TYPE_JPG;
		imgTypes["jpeg"] = SOIL_SAVE_TYPE_JPG;
	}

	auto ext = Utils::getExtension(filename);
	if (imgTypes.find(ext) == imgTypes.end())
		return false;

	auto imgType = imgTypes[ext];

	// TODO: may need to flip the image?

	auto size = img->getDimensions();
	auto channels = img->getChannels();
	auto *data = img->getData();

	int res = SOIL_save_image(filename.c_str(), imgType, size.x, size.y, channels, data);
	if (res == 0)
		return false;

	return true;
}

