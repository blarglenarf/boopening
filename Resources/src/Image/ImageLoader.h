#pragma once

#include <string>

namespace Rendering
{
	class Image;
}

namespace Resources
{
	class ImageLoader
	{
	public:
		static bool load(Rendering::Image *img, const std::string &filename);
		static bool save(Rendering::Image *img, const std::string &filename);
	};
}
