#include "Database.h"
#include "../Index.h"
#if 0
#include "sqlite/sqlite3.h"

#include <iostream>

using namespace Resources;


static int callback(void *rs, int argc, char **argv, char **colName)
{
	ResultSet *result = (ResultSet*) rs;
	Result *currResult = nullptr;

	for (int i = 0; i < argc; i++)
	{
		//printf("%s = %s\n", colName[i], argv[i] ? argv[i] : "NULL");
		//std::cout << i << ": " << colName[i] << ": " << (argv[i] ? argv[i] : "NULL") << "\n";

		int idx = result->getColumnIndex(colName[i]);
		if (idx == -1)
			idx = result->addColumn(colName[i]);
		if (idx == 0)
		{
			result->getResults().push_back(Result());
			currResult = &result->getResults()[result->getResults().size() - 1];
		}

		if (!currResult)
			std::cout << "Error, unable to process query result.\n";
		else
			currResult->setValue(idx, argv[i] ? argv[i] : "NULL");
	}

	return 0;
}

void Result::setValue(int idx, const std::string &val)
{
	if (idx == (int) values.size())
		values.push_back(val);
	else if (idx > (int) values.size())
	{
		values.resize(idx);
		values[idx] = val;
	}
	else
		values[idx] = val;
}

const std::string &Result::getStr(int idx)
{
	static std::string err = "";
	if (idx >= (int) values.size())
		return err;
	return values[idx];
}

int Result::getInt(int idx)
{
	if (idx >= (int) values.size())
		return 0;
	return std::stoi(values[idx]);
}

float Result::getFloat(int idx)
{
	if (idx >= (int) values.size())
		return 0;
	return std::stof(values[idx]);
}

double Result::getDouble(int idx)
{
	if (idx >= (int) values.size())
		return 0;
	return std::stod(values[idx]);
}

unsigned int Result::getUInt(int idx)
{
	if (idx >= (int) values.size())
		return 0;
	return (unsigned int) std::stoul(values[idx]);
}


int ResultSet::getColumnIndex(const std::string &column)
{
	for (size_t i = 0; i < columns.size(); i++)
		if (columns[i] == column)
			return (int) i;
	return -1;
}

int ResultSet::addColumn(const std::string &column)
{
	columns.push_back(column);
	return (int) columns.size() - 1;
}


Database::Database() : db(nullptr)
{
	// TODO: May be necessary to set the sqlite3 tmp folder on windows before opening a database: https://sqlite.org/c3ref/temp_directory.html
	openMemory();
}

Database::~Database()
{
	close();
}

bool Database::open(const std::string &filename)
{
	auto path = Index::getFilePath(filename);

	close();
	std::string dbName = "file:";
	dbName += path;
	int rc = sqlite3_open(dbName.c_str(), &db);
	if (rc)
	{
		sqlite3_close(db);
		std::cout << "Failed to open database " << filename << "\n";
		return false;
	}
	return true;
}

bool Database::openMemory()
{
	close();
	int rc = sqlite3_open(":memory:", &db);
	if (rc)
	{
		sqlite3_close(db);
		std::cout << "Failed to open in-memory database. This should never happen.\n";
		return false;
	}
	return true;
}

void Database::close()
{
	if (db)
		sqlite3_close(db);
	db = nullptr;
}

ResultSet Database::runQuery(const std::string &query)
{
	ResultSet result;

	if (!db)
	{
		std::cout << "Database not open, cannot run query: " << query << "\n";
		return result;
	}

	char *errMsg = nullptr;

	int rc = sqlite3_exec(db, query.c_str(), callback, &result, &errMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL error: " << errMsg << "\n";
		sqlite3_free(errMsg);
	}

	return result;
}
#endif

