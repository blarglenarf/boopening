#pragma once
#if 0
#include <string>
#include <vector>

struct sqlite3;

namespace Resources
{
	class Result
	{
	private:
		std::vector<std::string> values;

	public:
		void setValue(int idx, const std::string &val);

		size_t size() { return values.size(); }

		const std::string &getStr(int idx);
		int getInt(int idx);
		float getFloat(int idx);
		double getDouble(int idx);
		unsigned int getUInt(int idx);

		std::vector<std::string>::iterator begin() { return values.begin(); }
		std::vector<std::string>::iterator end() { return values.end(); }

		std::vector<std::string> &getValues() { return values; }
	};

	class ResultSet
	{
	private:
		std::vector<std::string> columns;
		std::vector<Result> results;

	public:
		int getColumnIndex(const std::string &column);
		int addColumn(const std::string &column);

		std::vector<Result>::iterator begin() { return results.begin(); }
		std::vector<Result>::iterator end() { return results.end(); }

		std::vector<std::string> &getColumns() { return columns; }
		std::vector<Result> &getResults() { return results; }
	};

	class Database
	{
	private:
		sqlite3 *db;

	public:
		Database();
		~Database();

		bool open(const std::string &filename);
		bool openMemory();
		void close();

		ResultSet runQuery(const std::string &query);

		// TODO: More specific querying functions.
		// TODO: Prepared statement type query functions, or whatever they're called.
	};
}
#endif

