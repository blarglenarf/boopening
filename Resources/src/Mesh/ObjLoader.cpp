#include "ObjLoader.h"
#include "../Image/ImageLoader.h"
#include "../../../Utils/src/File.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Renderer/src/RenderResource/Mesh.h"
#include "../../../Renderer/src/RenderResource/Material.h"
#include "../../../Renderer/src/RenderResource/RenderResourceCache.h"
#include "../../../Math/src/MathCommon.h"

#include <set>
#include <iostream>

using namespace Resources;
using namespace Math;

int ObjLoader::maxIndex = 0;
int ObjLoader::nVertices = 0;

int ObjLoader::getIndex(std::vector<std::list<TableVert>> &vertTable, int vertPos, int texPos, int normPos)
{
	if (vertPos < 0)
		return -1;

	for (auto iter = vertTable[vertPos].begin(); iter != vertTable[vertPos].end(); iter++)
	{
		if (iter->texPos == texPos && iter->normPos == normPos)
			return iter->index;
	}

	vertTable[vertPos].push_back(TableVert(texPos, normPos, maxIndex++));

	return maxIndex - 1;
}

void ObjLoader::addVertex(std::vector<std::list<TableVert>> &vertTable, std::vector<unsigned int> &indices, std::vector<Rendering::Vertex> &vertices, std::string &token, std::vector<Math::vec3> &positions, std::vector<Math::vec2> &texes, std::vector<Math::vec3> &normals)
{
	// TODO: Offsets may start from 1, or from 0. Handle this.
	// TODO: make this go faster
	std::vector<std::string> toks = Utils::split(token, "/");
	int nSlashes = Utils::count(token, '/');

	int vertPos = std::stoi(toks[0]) - 1;
	int texPos = -1;
	int normPos = -1;

	bool loadTex = false, loadNorm = false;

	if (toks.size() > 1 && nSlashes == 1)
	{
		texPos = std::stoi(toks[1]) - 1;
		loadTex = true;
	}
	if (toks.size() == 2 && nSlashes == 2)
	{
		normPos = std::stoi(toks[1]) - 1;
		loadNorm = true;
	}
	if (toks.size() == 3) 
	{
		texPos = std::stoi(toks[1]) - 1;
		normPos = std::stoi(toks[2]) - 1;
		loadTex = true;
		loadNorm = true;
	}

	if (vertPos < 0)
		vertPos = (int) positions.size() + vertPos + 1;
	if (loadTex && texPos < 0)
		texPos = (int) texes.size() + texPos + 1;
	if (loadNorm && normPos < 0)
		normPos = (int) normals.size() + normPos + 1;

	int currMaxIndex = maxIndex;
	int index = getIndex(vertTable, vertPos, texPos, normPos);
	if (index < 0)
		return;

	vertices[index].pos = positions[vertPos];
	if (texPos != -1 && texPos < (int) texes.size()) vertices[index].tex = texes[texPos];
	if (normPos != -1 && normPos < (int) normals.size()) vertices[index].norm = normals[normPos];

	indices.push_back(index);

	// No. of vertices won't necessarily increase...
	if (currMaxIndex != maxIndex)
		nVertices++;
}

void ObjLoader::loadMaterials(const std::string &path, const std::string &filename, std::map<std::string, Rendering::Material*> &materialMap, std::map<std::string, Rendering::Material*> &duplicates)
{
	Utils::File file(path + filename);
	int lineNo = 0;
	std::string currMatName;
	Rendering::Material *mat = nullptr;

	try
	{
		for (auto &line : file)
		{
			lineNo++;

			std::vector<std::string> toks = Utils::split(line, " \t");
			if (toks.empty())
				continue;

			if (toks[0] == "newmtl")
			{
				Rendering::Material *newMat = nullptr;
				Rendering::MaterialCache::getMaterial(filename + toks[1], newMat);
				materialMap[toks[1]] = newMat;
				mat = newMat;
				currMatName = toks[1];
			}

			if (!mat)
				continue;

			if (toks[0] == "Ka")
				mat->setAmbientColor(vec4(std::stof(toks[1]), std::stof(toks[2]), std::stof(toks[3]), 1.0));
			if (toks[0] == "Kd")
				mat->setDiffuseColor(vec4(std::stof(toks[1]), std::stof(toks[2]), std::stof(toks[3]), 1.0));
			if (toks[0] == "Ks")
				mat->setSpecularColor(vec4(std::stof(toks[1]), std::stof(toks[2]), std::stof(toks[3]), 1.0));
			if (toks[0] == "Ns")
				mat->setShininess(std::stof(toks[1]));
			// map_Ka --> ambient texture map.
			// map_Kd --> diffuse texture map.
			// map_Ks --> specular texture map.
			if (toks[0] == "map_Ka" || toks[0] == "map_Kd")
			{
				Rendering::Image *img = nullptr;
				bool addFlag = true;
				if (!Rendering::ImageCache::getImage(path + toks[1], img))
					if(!ImageLoader::load(img, path + toks[1]))
					{
						std::cout << "Failed to load texture " << path << toks[1] << " when loading material " << currMatName << " from " << filename << "\n";
						addFlag = false;
					}
				if (addFlag)
					mat->setDiffuseImg(img);
			}
			// TODO: d --> opacity.
			// TODO: Tr --> transparency (inverse of d, Tr = 1 - d).
			// TODO: Implement the illum codes.
			// TODO: Add normal map (either map_bump, or bump, or disp).
		}
	}
	catch (...)
	{
		std::cout << "Failed to read data from mtl file " << filename << " at line number " << lineNo << "\n";
		return;
	}

	// Determine duplicate materials (materials with same properties but different names).
	// TODO: Use an approach that isn't O(n^2).
	for (auto it : materialMap)
	{
		auto *mat = it.second;

		// If this has already been marked as a duplicate, then keep going.
		if (duplicates.find(it.first) != duplicates.end())
			continue;
		duplicates[it.first] = nullptr;

		for (auto jt : materialMap)
		{
			// Can't be a duplicate of yourself.
			if (it.first == jt.first)
				continue;

			auto *checkMat = jt.second;
			if (mat->getAmbientColor() == checkMat->getAmbientColor() && mat->getDiffuseColor() == checkMat->getDiffuseColor() &&
				mat->getSpecularColor() == checkMat->getSpecularColor() && mat->getShininess() == checkMat->getShininess() &&
				mat->getDiffuseImg() == checkMat->getDiffuseImg() && mat->getNormalImg() == checkMat->getNormalImg())
			{
				// Both materials are exactly the same (except for their names), mark as a duplicate and record the original.
				duplicates[jt.first] = mat;
				//std::cout << "Duplicate found; original: " << mat->getName() << ", duplicate: " << checkMat->getName() << "\n";
			}
		}
	}
}


bool ObjLoader::load(Rendering::Mesh *mesh, const std::string &filename)
{
	// FIXME: this is slooooooooooooow... make it go faster :)
	if (!mesh)
		return false;
	mesh->clear();

	int nVerts = 0;
	int nTexs = 0;
	int nNorms = 0;
	size_t nFaces = 0;
	int lineNo = 0;

	Utils::File file(filename);
	std::string path = Utils::getFilePath(filename);
	std::map<std::string, Rendering::Material*> materialMap;

	// If a material is a duplicate, this will have its original.
	std::map<std::string, Rendering::Material*> duplicateMaterials;

	// Some obj files are stupid enough to list the same mtllib multiple times...
	std::set<std::string> loadedMtlLibs;

	try
	{
		for (auto &line : file)
		{
			lineNo++;

			// Need to load up the necessary material info for later.
			if (line[0] == 'm')
			{
				std::vector<std::string> toks = Utils::split(line, " \t");
				if (toks[0] == "mtllib" && loadedMtlLibs.find(toks[1]) == loadedMtlLibs.end())
				{
					loadMaterials(path, toks[1], materialMap, duplicateMaterials);
					loadedMtlLibs.insert(toks[1]);
				}
			}

			if (line == "")
				continue;

			if (line[0] == 'v')
			{
				if (line[1] == 't') nTexs++;
				else if (line[1] == 'n') nNorms++;
				else nVerts++;
			}

			if (line[0] == 'f') nFaces++;
		}
	}
	catch (...)
	{
		std::cout << "Failed to read data from obj file " << filename << " at line number " << lineNo << "\n";
		return false;
	}

	std::vector<Math::vec3> verts;
	verts.reserve(nVerts);

	std::vector<Math::vec3> norms;
	norms.reserve(nNorms);

	std::vector<Math::vec2> texs;
	texs.reserve(nTexs);

	std::vector<unsigned int> indices;
	indices.reserve(nFaces * 4);

	std::vector<std::list<TableVert>> vertTable(nVerts);

	std::vector<Rendering::Vertex> vertices(nFaces * 4);

	maxIndex = 0;
	nVertices = 0;
	lineNo = 0;

	nFaces = 0;

	Math::vec3 min, max;

	size_t prevNFaces = 0;
	Rendering::Material *mat = nullptr;

	// Sort meshes by material, to reduce facesets and hence bind/unbind calls.
	struct Faceset { size_t start; size_t end; Faceset(size_t start = 0, size_t end = 0) : start(start), end(end) {} };
	std::map<Rendering::Material*, std::vector<Faceset>> materialInstances;
	std::vector<Faceset> noMaterialInstances;

	try
	{
		for (auto &line : file)
		{
			lineNo++;

			if (line == "")
				continue;

			std::vector<std::string> toks = Utils::split(line, " \t");
			if (toks.empty())
				continue;

			// Need to make a MaterialInstance whenever a usemtl tag is found, can then sort faces based on materials to reduce draw calls.
			if (toks[0] == "usemtl")
			{
				if (mat && prevNFaces < nFaces)
					materialInstances[mat].push_back(Faceset(prevNFaces * 3, nFaces * 3));
				else if (!mat && prevNFaces < nFaces)
					noMaterialInstances.push_back(Faceset(prevNFaces * 3, nFaces * 3));
				auto it = materialMap.find(toks[1]);
				if (it == materialMap.end())
				{
					std::cout << "Unable to find material " << toks[1] << " in file " << filename << " on line " << lineNo << "\n";
					mat = nullptr;
				}
				else
				{
					// Make sure the material isn't a duplicate, if it is, then use the original.
					if (duplicateMaterials[it->first])
						mat = duplicateMaterials[it->first];
					else
						mat = it->second;
				}
				prevNFaces = nFaces;
			}

			if (toks[0] == "v")
			{
				Math::vec3 v(std::stof(toks[1]), std::stof(toks[2]), std::stof(toks[3]));
				Rendering::Mesh::updateMinMax(v, min, max);
				verts.push_back(v);
			}
			if (toks[0] == "vn") norms.push_back(Math::vec3(std::stof(toks[1]), std::stof(toks[2]), std::stof(toks[3])));
			if (toks[0] == "vt") texs.push_back(Math::vec2(std::stof(toks[1]), std::stof(toks[2])));

			if (toks[0] == "f")
			{
				for (int i = 1; i < 4; i++)
				{
					addVertex(vertTable, indices, vertices, toks[i], verts, texs, norms);
				}
				nFaces++;

				if (toks.size() > 4)
				{
					for (int i = 4; i < (int) toks.size(); i++)
					{
						addVertex(vertTable, indices, vertices, toks[1], verts, texs, norms);
						addVertex(vertTable, indices, vertices, toks[i - 1], verts, texs, norms);
						addVertex(vertTable, indices, vertices, toks[i], verts, texs, norms);
						nFaces++;
					}
				}
			}
		}
	}
	catch (...)
	{
		std::cout << "Failed to load data from obj file " << filename << " at line number " << lineNo << "\n";
		return false;
	}

	// Now apply the last material to the last remaining faces.
	if (mat && prevNFaces < nFaces)
		materialInstances[mat].push_back(Faceset(prevNFaces * 3, nFaces * 3));
	else if (!mat && prevNFaces < nFaces)
		noMaterialInstances.push_back(Faceset(prevNFaces * 3, nFaces * 3));

	vertices.resize(nVertices);
	indices.resize(nFaces * 3);

	Rendering::Material *defaultMat = nullptr;
	if (!Rendering::MaterialCache::getMaterial("default", defaultMat))
	{
		defaultMat->setAmbientColor(vec4(0.3, 0.3, 0.3, 1.0));
		defaultMat->setDiffuseColor(vec4(0.4, 0.4, 0.4, 1.0));
		defaultMat->setSpecularColor(vec4(0.6, 0.6, 0.6, 1.0));
		defaultMat->setShininess(128);
	}
	if (!noMaterialInstances.empty())
		mesh->addMaterial(defaultMat);

	// If there are no duplicates, then no sorting is needed.
	bool dupFlag = false;
	for (auto it : materialInstances)
		if (it.second.size() > 1)
		{
			dupFlag = true;
			break;
		}
	if (noMaterialInstances.size() > 1)
		dupFlag = true;

	// No duplicates, just apply the materials to the facesets as needed.
	if (!dupFlag)
	{
		for (auto it : materialInstances)
		{
			mesh->addMaterial(it.first);
			mesh->useMaterial(it.first, it.second[0].start, it.second[0].end);
		}
		if (!noMaterialInstances.empty())
			mesh->useMaterial(defaultMat, noMaterialInstances[0].start, noMaterialInstances[0].end);
		mesh->load(std::move(vertices), std::move(indices), min, max);
	}

	// There were duplicates, sort facesets based on material.
	else
	{
		// TODO: Could arrange vertices/indices in sequential order to improve coherence, not sure what performance improvement there'd be.
		//std::vector<Rendering::Vertex> newVertices(vertices.size());
		//size_t vertOffset = 0

		std::vector<unsigned int> newIndices(indices.size());
		size_t indexOffset = 0;

		for (auto it : materialInstances)
		{
			mesh->addMaterial(it.first);
			size_t matStart = indexOffset;

			for (auto &faceset : it.second)
			{
				for (size_t i = 0; i < faceset.end - faceset.start; i++)
					newIndices[indexOffset + i] = indices[faceset.start + i];
				indexOffset += (faceset.end - faceset.start);
			}
			//std::cout << "Using material: " << it.first->getName() << "\n";
			mesh->useMaterial(it.first, matStart, indexOffset);
		}

		size_t matStart = indexOffset;
		for (auto &faceset : noMaterialInstances)
		{
			for (size_t i = 0; i < faceset.end - faceset.start; i++)
				newIndices[indexOffset + i] = indices[faceset.start + i];
			indexOffset += (faceset.end - faceset.start);
		}
		mesh->useMaterial(defaultMat, matStart, indexOffset);
		mesh->load(std::move(vertices), std::move(newIndices), min, max);
	}

	// TODO: If we already have normals then we may not need to generate them.
	mesh->generateNormals();
	mesh->generateTangents();

	return true;
}

