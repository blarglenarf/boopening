#include "MeshLoader.h"
#include "ObjLoader.h"
#include "ThreeDSLoader.h"
#include "CTMLoader.h"
#include "../Index.h"
#include "../../../Utils/src/StringUtils.h"

using namespace Resources;

bool MeshLoader::load(Rendering::Mesh *mesh, const std::string &filename)
{
	auto ext = Utils::getExtension(filename);
	auto path = Index::getFilePath(filename);

	if (ext == "obj")
		return ObjLoader::load(mesh, path);
	else if (ext == "3ds")
		return ThreeDSLoader::load(mesh, path);
	else if (ext == "ctm")
		return CTMLoader::load(mesh, path);

	return false;
}

