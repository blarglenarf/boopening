#include "ThreeDSLoader.h"
#include "lib3ds/lib3ds.h"
#include "../Image/ImageLoader.h"
#include "../../../Renderer/src/RenderResource/Mesh.h"
#include "../../../Renderer/src/RenderResource/Material.h"
#include "../../../Renderer/src/RenderResource/Image.h"
#include "../../../Renderer/src/RenderResource/RenderResourceCache.h"
#include "../../../Math/src/MathCommon.h"
#include "../../../Utils/src/StringUtils.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <cassert>
#include <cstring>

using namespace Resources;
using namespace Math;

struct MeshNode
{
	Lib3dsNode *node;
	Math::mat4 matrix;
	MeshNode(Lib3dsNode* n, Math::mat4 m = mat4::getIdentity())
	{
		node = n;
		matrix = m;
	}
};

bool ThreeDSLoader::load(Rendering::Mesh *mesh, const std::string &filename)
{
	if (!mesh)
		return false;
	mesh->clear();

	std::string path = Utils::getFilePath(filename);

	Lib3dsFile *f = lib3ds_file_open(filename.c_str());
	if (!f)
	{
		std::cout << "Failed to open 3ds file " << filename << "\n";
		return false;
	}

	size_t vertexOffset = 0;
	size_t indexOffset = 0;
	size_t nVertices = 0;
	size_t nIndices = 0;
	//bool hasTexCoords = false;

	// Extract all instances.
	typedef std::pair<std::string, mat4> Instance;
	std::vector<Instance> instances;
	std::set<std::string> isInstanced;
	std::vector<MeshNode> nodes;
	for (Lib3dsNode *p = f->nodes; p != nullptr; p = p->next)
		nodes.push_back(MeshNode(p, mat4(p->matrix))); // Add root.
	while (nodes.size() > 0)
	{
		// Get next.
		MeshNode n = nodes.back();
		nodes.pop_back();

		// Add children.
		for (Lib3dsNode *p = n.node->childs; p != nullptr; p = p->next)
			nodes.push_back(MeshNode(p, n.matrix * mat4(p->matrix)));

		// If mesh instance, extract.
		if (n.node->type == LIB3DS_NODE_MESH_INSTANCE)
		{
			Lib3dsMeshInstanceNode *inst = (Lib3dsMeshInstanceNode*) n.node;
			std::cout << "\n" << (*(vec4*) inst->pos) << "\n" << (*(vec4*) inst->rot) << "\n" << (*(vec4*) inst->scl) << "\n" << (*(vec4*) inst->pivot) << "\n";
			instances.push_back(Instance(n.node->name, n.matrix * getTranslate(-*(vec3*) inst->pivot)));
			isInstanced.insert(n.node->name);
		}
	}

	std::map<std::string, Lib3dsMesh*> meshByName;

	// Perhaps some meshes were not referenced in the node hierarchy. chuck them in too :)
	for (int i = 0; i < f->nmeshes; i++)
	{
		if (isInstanced.find(f->meshes[i]->name) == isInstanced.end())
		{
			//printf("3ds mesh %s UNINSTANCED\n", f->meshes[i]->name);
			instances.push_back(Instance(f->meshes[i]->name, mat4::getIdentity()));
		}
		assert(meshByName.find(f->meshes[i]->name) == meshByName.end()); // I'd assume the mesh name must be unique.
		meshByName[f->meshes[i]->name] = f->meshes[i];
	}

	// Count num verts/indices.
	for (std::vector<Instance>::iterator it = instances.begin(); it != instances.end(); ++it)
	{
		std::map<std::string, Lib3dsMesh*>::iterator found;
		found = meshByName.find(it->first);
		if (found == meshByName.end())
		{
			printf("missing mesh %s\n", (*it).first.c_str());
			continue;
		}
		Lib3dsMesh* m = found->second;
		nVertices += m->nvertices;
		nIndices += m->nfaces * 3;
		//hasTexCoords = hasTexCoords || (m->texcos != nullptr);
	}

	// Extract materials.
	//FIXME: Currently, only one material per mesh is supported - the material of the first face.
	std::map<std::string, Rendering::Material*> materialMap;
	for (int i = 0; i < f->nmaterials; i++)
	{
		Lib3dsTextureMap &texture = f->materials[i]->texture1_map;
		Lib3dsTextureMap &normalmap = f->materials[i]->bump_map;
		Rendering::Material *mat = nullptr;
		Rendering::Image *img = nullptr;
		if (!Rendering::MaterialCache::getMaterial(Utils::getBaseFilename(std::string(filename)) + f->materials[i]->name, mat))
		{
			mat->setAmbientColor(vec4(f->materials[i]->ambient[0], f->materials[i]->ambient[1], f->materials[i]->ambient[2], 1.0));
			mat->setDiffuseColor(vec4(f->materials[i]->diffuse[0], f->materials[i]->diffuse[1], f->materials[i]->diffuse[2], 1.0));
			mat->setSpecularColor(vec4(f->materials[i]->specular[0], f->materials[i]->specular[1], f->materials[i]->specular[2], 1.0));
			mat->setShininess(f->materials[i]->shininess);

			//printf("%s -> %s\n", f->materials[i]->name, (path + "/" + texture.name).c_str());
			if (strlen(texture.name) != 0)
			{
				bool addFlag = true;
				if (!Rendering::ImageCache::getImage(path + texture.name, img))
					if(!ImageLoader::load(img, path + texture.name))
					{
						std::cout << "Failed to load texture " << path + texture.name << " when loading material " << f->materials[i]->name << " from " << filename << "\n";
						addFlag = false;
					}
				if (addFlag)
					mat->setDiffuseImg(img);
				//mat->imgColour.load((path + texture.name).c_str());
				//mat->imgColour->anisotropy = 4.0f;
			}
			if (strlen(normalmap.name) != 0)
			{
				bool addFlag = true;
				if (!Rendering::ImageCache::getImage(path + texture.name, img))
					if(!ImageLoader::load(img, path + texture.name))
					{
						std::cout << "Failed to load normal map " << path + texture.name << " when loading material " << f->materials[i]->name << " from " << filename << "\n";
						addFlag = false;
					}
				if (addFlag)
					mat->setNormalImg(img);
				//mat->imgNormal.load((path + normalmap.name).c_str());
				//mat->imgNormal->anisotropy = 4.0f;
			}
			else
			{
				// Assume normal map is basename + dd.
				std::string bn = Utils::getBaseFilename(texture.name);
				std::string assumedNormalsImage = path + bn + "dd.png";
				bool addFlag = true;
				if (Utils::fileExists(assumedNormalsImage))
				{
					if (!Rendering::ImageCache::getImage(assumedNormalsImage, img))
						if(!ImageLoader::load(img, assumedNormalsImage))
						{
							std::cout << "Failed to load normal map " << assumedNormalsImage << " when loading material " << f->materials[i]->name << " from " << filename << "\n";
							addFlag = false;
						}
					if (addFlag)
						mat->setNormalImg(img);
				}
			#if 0
				std::string bn = basefilename(texture.name);
				std::string assumedNormalsImage = path + bn + "dd.png";
				if (fileExists(assumedNormalsImage.c_str()))
					mat->imgNormal.load(assumedNormalsImage.c_str());
				//mat->imgNormal->anisotropy = 4.0f;
				//if (mat->imgNormal.texture)
				//	printf("%s\n", (path + bn + "dd.png").c_str());
			#endif
			}
		}
		materialMap[f->materials[i]->name] = mat;
		mesh->addMaterial(mat);
	}

	// Sort meshes by material, to reduce facesets and hence bind/unbind calls.
	typedef std::map<int, std::vector<int>> MaterialInstances;
	MaterialInstances materialInstances;
	for (size_t i = 0; i < instances.size(); i++)
	{
		std::map<std::string, Lib3dsMesh*>::iterator found;
		found = meshByName.find(instances[i].first);
		if (found == meshByName.end())
			continue;
		Lib3dsMesh *m = found->second;

		int material = -1;
		if (m->nfaces > 0)
			material = m->faces[0].material;

		// Append mesh to material instance list. Create if one doesn't exist for this material. FIXME: is this even necessary?
		if (materialInstances.find(material) == materialInstances.end())
			materialInstances[material] = std::vector<int>();
		materialInstances[material].push_back((int) i);
	}

	mesh->resizeVertices(nVertices);
	mesh->resizeIndices(nIndices);

#if 0
	mesh.sub[VERTICES] = new float[mesh.numVertices*3];
	if (hasTexCoords)
		mesh.sub[TEXCOORDS] = new float[mesh.numVertices*2];
	mesh.dataIndices = new unsigned int[mesh.numIndices];
#endif
	
	vec3 min, max;
	size_t lastMaterialIndex = 0;
	for (MaterialInstances::iterator it = materialInstances.begin(); it != materialInstances.end(); it++)
	{
		for (size_t i = 0; i < it->second.size(); ++i)
		{
			Instance &inst = instances[it->second[i]];
			std::map<std::string, Lib3dsMesh*>::iterator found;
			found = meshByName.find(inst.first);
			if (found == meshByName.end())
				continue;
			Lib3dsMesh *m = found->second;
			//printf("3ds mesh %s\n", m->name);
			assert(m->vertices);

			// Swivel x/y/z.
			mat4 mat;
			mat.t[1][0] = 1.0;
			mat.t[0][2] = 1.0;
			mat.t[2][1] = 1.0;
			mat *= mat4(m->matrix);
			mat *= inst.second;
			mat *= mat4(m->matrix).getInverse();
			//mat *= mat44::translate(m->matrix[3][0], m->matrix[3][1], m->matrix[3][2]);
			//PRINTMAT44(mat44(m->matrix));

			//printf("\tverts=%i\n", m->nvertices);
			auto *vertices = mesh->getVertices();
			for (int v = 0; v < m->nvertices; v++)
			{
				auto &vertex = vertices[vertexOffset + v];
				vertex.pos = (mat * vec4(m->vertices[v][0], m->vertices[v][1], m->vertices[v][2], 1.0f)).xyz;
				Rendering::Mesh::updateMinMax(vertex.pos, min, max);

				if (m->texcos)
					vertex.tex = vec2(m->texcos[v][0], m->texcos[v][1]);
			#if 0
				*(vec3f*)&mesh.sub[VERTICES][(vertexOffset+v)*3] = mat * vec4f(m->vertices[v][0], m->vertices[v][1], m->vertices[v][2], 1.0f);
				if (m->texcos)
				{
					mesh.sub[TEXCOORDS][(vertexOffset+v)*2+0] = m->texcos[v][0];
					mesh.sub[TEXCOORDS][(vertexOffset+v)*2+1] = m->texcos[v][1];
				}
				else if (hasTexCoords)
				{
					mesh.sub[TEXCOORDS][(vertexOffset+v)*2+0] = 0.0f;
					mesh.sub[TEXCOORDS][(vertexOffset+v)*2+1] = 0.0f;
				}
			#endif
			}

			auto *indices = mesh->getIndices();
			for (int n = 0; n < m->nfaces; n++)
			{
				indices[(indexOffset + n * 3) + 0] = (unsigned int) vertexOffset + m->faces[n].index[0];
				indices[(indexOffset + n * 3) + 1] = (unsigned int) vertexOffset + m->faces[n].index[1];
				indices[(indexOffset + n * 3) + 2] = (unsigned int) vertexOffset + m->faces[n].index[2];
			}
			vertexOffset += m->nvertices;
			indexOffset += m->nfaces * 3;
		}

		// Add a faceset for the material group.
		if (it->first >= 0 && indexOffset > lastMaterialIndex)
		{
			mesh->useMaterial(materialMap[f->materials[it->first]->name], lastMaterialIndex, indexOffset);
			//mesh.useMaterial(lastMaterialIndex, indexOffset, f->materials[it->first]->name);
			//printf("%s: %i->%i\n", f->materials[it->first]->name, lastMaterialIndex, indexOffset);
			lastMaterialIndex = indexOffset;
		}
	}
	//printf("Grouped to %i facesets\n", (int)materialInstances.size());

	lib3ds_file_free(f);

	mesh->setMin(min);
	mesh->setMax(max);
	mesh->generateNormals();
	mesh->generateTangents();
	//mesh.numPolygons = mesh.numIndices / 3;

	return true;
}

