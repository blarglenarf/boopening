#include "CTMLoader.h"
#include "../../../Renderer/src/RenderResource/Mesh.h"
#include "../../../Renderer/src/RenderResource/Material.h"
#include "../../../Renderer/src/RenderResource/RenderResourceCache.h"
#include "../../../Math/src/Vector/VectorCommon.h"
#include "openctm/openctm.h"

#include <iostream>
#include <cstring>

using namespace Resources;
using namespace Math;

bool CTMLoader::load(Rendering::Mesh *mesh, const std::string &filename)
{
	if (!mesh)
		return false;
	mesh->clear();

	//http://openctm.sourceforge.net/media/DevelopersManual.pdf
	CTMcontext context;

	// Create a new importer context
	context = ctmNewContext(CTM_IMPORT);

	// Load the OpenCTM file
	ctmLoad(context, filename.c_str());
	CTMenum err = ctmGetError(context);
	if(err != CTM_NONE)
	{
		std::cout << "Failed to open ctm file " << filename << "\n";
		ctmFreeContext(context);
		return false;
	}

	// Access the mesh data
	unsigned int numVertices = ctmGetInteger(context, CTM_VERTEX_COUNT);
	unsigned int norms = ctmGetInteger(context, CTM_HAS_NORMALS);
	unsigned int texcs = ctmGetInteger(context, CTM_UV_MAP_COUNT);
	//unsigned int other = ctmGetInteger(context, CTM_ATTRIB_MAP_COUNT);
	unsigned int numPolygons = ctmGetInteger(context, CTM_TRIANGLE_COUNT);
	unsigned int numIndices = 3 * numPolygons;

	float *vertFloats = nullptr, *normFloats = nullptr, *texFloats = nullptr;
	vertFloats = (float*) ctmGetFloatArray(context, CTM_VERTICES);
	if (norms)
		normFloats = (float*) ctmGetFloatArray(context, CTM_NORMALS);
	if (texcs)
		texFloats = (float*) ctmGetFloatArray(context, CTM_UV_MAP_1);
	unsigned int *indUints = (unsigned int*) ctmGetIntegerArray(context, CTM_INDICES);

	std::vector<Rendering::Vertex> vertices(numVertices);
	std::vector<unsigned int> indices(numIndices);

	// TODO: copy this over a littler better.
	for (size_t i = 0; i < numIndices; i++)
		indices[i] = indUints[i];

	vec3 min, max;

	for (size_t i = 0; i < numVertices; i++)
	{
		vertices[i].pos.x = vertFloats[i * 3];
		vertices[i].pos.y = vertFloats[i * 3 + 1];
		vertices[i].pos.z = vertFloats[i * 3 + 2];
		Rendering::Mesh::updateMinMax(vertices[i].pos, min, max);

		if (i < norms)
		{
			vertices[i].norm.x = normFloats[i * 3];
			vertices[i].norm.y = normFloats[i * 3 + 1];
			vertices[i].norm.z = normFloats[i * 3 + 2];
		}

		if (i < texcs)
		{
			vertices[i].tex.x = texFloats[i * 2];
			vertices[i].tex.y = texFloats[i * 2 + 1];
		}
	}

	mesh->load(std::move(vertices), std::move(indices), min, max);

	//if (!norms)
	//{
		//printf("Generating normals\n");
		mesh->generateNormals();
		mesh->generateTangents();
	//}

	// Free the context
	ctmFreeContext(context);

	Rendering::Material *defaultMat = nullptr;
	if (!Rendering::MaterialCache::getMaterial("default", defaultMat))
	{
		defaultMat->setAmbientColor(vec4(0.3, 0.3, 0.3, 1.0));
		defaultMat->setDiffuseColor(vec4(0.4, 0.4, 0.4, 1.0));
		defaultMat->setSpecularColor(vec4(0.6, 0.6, 0.6, 1.0));
		defaultMat->setShininess(128);
	}
	mesh->useMaterial(defaultMat, 0, indices.size());

	return true;
}

