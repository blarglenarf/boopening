#pragma once

#include <string>

namespace Rendering
{
	class Mesh;
}

namespace Resources
{
	class CTMLoader
	{
	public:
		static bool load(Rendering::Mesh *mesh, const std::string &filename);
	};
}

