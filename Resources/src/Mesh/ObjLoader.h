#pragma once

#include "../../../Renderer/src/Vertex.h"
#include "../../../Math/src/Vector/VectorCommon.h"

#include <vector>
#include <list>
#include <map>

namespace Rendering
{
	class Mesh;
	class Material;
}

namespace Resources
{
	class ObjLoader
	{
	private:
		// Struct used internally by the obj loader to store a chaining hash table of vertices.
		struct TableVert
		{
			int texPos;
			int normPos;
			int index;

			TableVert(int texPos = -1, int normPos = -1, int index = -1) : texPos(texPos), normPos(normPos), index(index) {}
		};

		// Maximum index value to an element in the vertex array.
		static int maxIndex;

		static int nVertices;

	private:
		// Adds a new vertex to the vertex array, storing it in the position returned by getIndex.
		static void addVertex(std::vector<std::list<TableVert>> &vertTable, std::vector<unsigned int> &indices, std::vector<Rendering::Vertex> &vertices, std::string &token, std::vector<Math::vec3> &positions, std::vector<Math::vec2> &texes, std::vector<Math::vec3> &normals);

		// Gets the location of a vertex from the vertTable if it exists, otherwise it adds the new vertex and increments maxIndex.
		static int getIndex(std::vector<std::list<TableVert>> &vertTable, int vertPos, int texPos, int normPos);

		static void loadMaterials(const std::string &path, const std::string &filename, std::map<std::string, Rendering::Material*> &materialMap, std::map<std::string, Rendering::Material*> &duplicates);

	public:
		static bool load(Rendering::Mesh *mesh, const std::string &filename);
	};
}

