#pragma once

#include <string>

namespace Rendering
{
	class Mesh;
}

namespace Resources
{
	class MeshLoader
	{
	public:
		static bool load(Rendering::Mesh *mesh, const std::string &filename);
	};
}

