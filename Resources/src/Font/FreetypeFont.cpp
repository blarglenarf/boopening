#include "FreetypeFont.h"

#include <iostream>

using namespace Resources;


static void calcCursorPos(Math::vec2 offset, float &dist, int penX, int penY, int fontSize, int linePadding, Math::ivec3 &cursorPos, int charPos, bool lastLine)
{
	// Pick closest pos, on the selected line.
	int linePos = (int) (offset.y / (float) (fontSize + linePadding));
	int currPos = (int) (penY / (float) (fontSize + linePadding));

	if (linePos != currPos && !lastLine)
		return;
	else if (lastLine && linePos < currPos)
		return;

	// FIXME: This isn't 100% correct.
	float d = offset.distance(Math::vec2(penX, penY + fontSize / 2.0f));

	if (d < dist)
	{
		dist = d;
		cursorPos.x = penX;
		cursorPos.y = penY;
		cursorPos.z = (int) charPos;
	}
}


FreetypeFont::Glyph::~Glyph()
{
	if (glyph)
		FT_Done_Glyph(glyph);
}

// TODO: Different render modes.
FT_BitmapGlyph FreetypeFont::Glyph::convertToBitmap()
{
	FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, 0, 1);
	return (FT_BitmapGlyph) glyph;
}


FreetypeFont::~FreetypeFont()
{
	FT_Done_Face(face);
}

void FreetypeFont::setSize(int size)
{
	if (size == fontSize)
		return;

	fontSize = size;
	auto error = FT_Set_Char_Size(face, 0, fontSize * 64, 90, 90);
	if (error)
		std::cout << "Font error: Unable to set character size for font " << name << "\n";

	error = FT_Set_Pixel_Sizes(face, 0, fontSize);
	if (error)
		std::cout << "Font error: Unable to set pixel size for font " << name << "\n";
}

void FreetypeFont::setGlyphSlot()
{
	glyphSlot = face->glyph;
	kerningFlag = FT_HAS_KERNING(face);
}

void FreetypeFont::getGlyphs(const std::string &str, int maxWidth, Math::ivec2 &dimensions, std::vector<Glyph> &glyphs, int linePadding)
{
	Math::ivec3 cursorPos(-1, -1, 0);

	stepGlyphs(str, maxWidth, dimensions, cursorPos, glyphs, linePadding, -1, -1, -1, true);
}

void FreetypeFont::getGlyph(char c, Math::ivec2 &dimensions, Glyph &glyph)
{
	auto glyphIndex = FT_Get_Char_Index(face, c);

	FT_Load_Glyph(face, glyphIndex, FT_LOAD_DEFAULT);

	glyph.x = 0;
	glyph.y = 0;

	FT_Get_Glyph(face->glyph, &glyph.glyph);

	dimensions.x = (glyphSlot->advance.x >> 6); // FIXME: May need a +1.
	dimensions.y = (fontSize / 3) + fontSize + 1; // FIXME: Such a fuckin hack. Figure out proper baseline distance.
}

// TODO: Implement some kind of centering/justify/right aligned layout. Atm everything is left aligned.
void FreetypeFont::stepGlyphs(const std::string &str, int maxWidth, Math::ivec2 &dimensions, Math::ivec3 &cursorPos, std::vector<Glyph> &glyphs, int linePadding, int offsetX, int offsetY, int offsetZ, bool storeGlyphs)
{
	Math::vec2 offset(offsetX, offsetY);
	float dist = 100000000;

	if (storeGlyphs)
		glyphs.resize(str.length());

	FT_UInt previous = 0;
	int penX = 0, penY = 0;
	size_t startWordPos = 0;
	int wordLen = 0;
	int nSpecials = 0;

	int width = 0;
	int height = 0;

	for (size_t i = 0; i < str.length(); i++)
	{
		wordLen++; // TODO: Word length needs to be based on character width and positioning.

		if (offsetZ == (int) i)
		{
			cursorPos.x = penX;
			cursorPos.y = penY;
			cursorPos.z = (int) i;
		}

		// TODO: What if only one of these is -1, even though it makes no sense.
		if (offsetX != -1 && offsetY != -1 && offsetZ == -1)
			calcCursorPos(offset, dist, penX, penY, fontSize, linePadding, cursorPos, (int) i, false);

		// If we have a newline, then go to the next line.
		if (str[i] == '\n')
		{
			penY += fontSize;
			penY += linePadding;
			penX = 0;
			previous = 0;
		}

		// If we have gone over the max. width, push the word to the next line (unless the word is too long for the line, then put it on its own line).
		if (maxWidth != 0 && (penX + fontSize >= maxWidth && wordLen <= maxWidth))
		{
			penY += fontSize;
			penY += linePadding;
			penX = 0;
			previous = 0;
			i = startWordPos;
		}

		auto glyphIndex = FT_Get_Char_Index(face, str[i]);

		if (kerningFlag && previous && glyphIndex)
		{
			FT_Vector delta;
			FT_Get_Kerning(face, previous, glyphIndex, FT_KERNING_DEFAULT, &delta);
			penX += delta.x >> 6;
		}

		FT_Load_Glyph(face, glyphIndex, FT_LOAD_DEFAULT);

		if (str[i] == '\n' || str[i] == ' ' || str[i] == '\t' || str[i] == '\r')
		{
			startWordPos = i + 1;
			wordLen = 0;
			nSpecials++;
		}
		else if (storeGlyphs)
		{
			int pos = (int) i - nSpecials;
			glyphs[pos].x = penX;
			glyphs[pos].y = penY;

			FT_Get_Glyph(face->glyph, &glyphs[pos].glyph);
		}

		if (str[i] != '\n' && str[i] != '\r')
			penX += glyphSlot->advance.x >> 6;

		if (penX > width)
			width = penX;
		if (penY > height)
			height = penY;

		previous = glyphIndex;
	}

	if (storeGlyphs)
		glyphs.resize((int) glyphs.size() - nSpecials);

	if (offsetX != -1 && offsetY != -1 && offsetZ == -1)
	{
		calcCursorPos(offset, dist, penX, penY, fontSize, linePadding, cursorPos, (int) str.length(), true);
	}
	else if (offsetZ >= (int) str.length())
	{
		cursorPos.x = penX;
		cursorPos.y = penY;
		cursorPos.z = (int) str.length();
	}

	dimensions.x = width; // FIXME: May need a +1.
	dimensions.y = height + (fontSize / 3) + fontSize + 1; // FIXME: Such a fuckin hack. Figure out proper baseline distance.
}

Math::ivec2 FreetypeFont::getDimensions(const std::string &str, int maxWidth, int linePadding)
{
	Math::ivec2 dimensions;	
	Math::ivec3 cursorPos(-1, -1, 0);
	std::vector<Glyph> glyphs;

	stepGlyphs(str, maxWidth, dimensions, cursorPos, glyphs, linePadding, -1, -1);

	return dimensions;
}

Math::ivec3 FreetypeFont::getCursorPos(const std::string &str, int offsetX, int offsetY, int charPos, int maxWidth, int linePadding)
{
	Math::ivec2 dimensions;
	Math::ivec3 cursorPos;
	std::vector<Glyph> glyphs;

	stepGlyphs(str, maxWidth, dimensions, cursorPos, glyphs, linePadding, offsetX, offsetY, charPos);

	return cursorPos;
}

