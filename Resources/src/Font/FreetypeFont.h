#pragma once

#include "../../../Math/src/Vector/Vec2.h"
#include "../../../Math/src/Vector/Vec3.h"

#include <string>
#include <vector>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

namespace Resources
{
	class FreetypeFont
	{
	public:
		struct Glyph
		{
		public:
			FT_Glyph glyph;
			int x;
			int y;

		public:
			Glyph() : glyph(0), x(0), y(0) {}
			~Glyph();
			FT_BitmapGlyph convertToBitmap();
		};

	private:
		std::string name;

		FT_Face face;
		FT_GlyphSlot glyphSlot;
		FT_Bool kerningFlag;

		int fontSize;

	public:
		FreetypeFont(const std::string &name = "") : name(name), fontSize(0) {}
		~FreetypeFont();

		void setName(const std::string &name) { this->name = name; }
		const std::string &getName() const { return name; }

		void setSize(int size);
		int getSize() const { return fontSize; }

		FT_Face &getFace() { return face; }
		void setGlyphSlot();

		void getGlyphs(const std::string &str, int maxWidth, Math::ivec2 &dimensions, std::vector<Glyph> &glyphs, int linePadding = 0);
		void getGlyph(char c, Math::ivec2 &dimensions, Glyph &glyph);

		// offsetX/Y are for determining closest cursor position, offsetZ gives cursor position at that char index.
		void stepGlyphs(const std::string &str, int maxWidth, Math::ivec2 &dimensions, Math::ivec3 &cursorPos, std::vector<Glyph> &glyphs, int linePadding = 0, int offsetX = -1, int offsetY = -1, int offsetZ = -1, bool storeGlyphs = false);

		Math::ivec2 getDimensions(const std::string &str, int maxWidth = 0, int linePadding = 0);
		Math::ivec3 getCursorPos(const std::string &str, int offsetX, int offsetY, int charPos = -1, int maxWidth = 0, int linePadding = 0);
	};
}

