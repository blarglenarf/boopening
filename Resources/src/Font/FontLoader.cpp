#include "FontLoader.h"
#include "FreetypeFont.h"
#include "../Index.h"
#include "../../../Renderer/src/RenderResource/Font.h"
#include "../../../Renderer/src/RenderResource/RenderResourceCache.h"

#include <iostream>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H

using namespace Resources;

bool FontLoader::load(Rendering::Font *renderFont, const std::string &filename)
{
	auto path = Index::getFilePath(filename);

	static FT_Library library;
	static bool initFlag = false;
	if (!initFlag)
	{
		FT_Error error = FT_Init_FreeType(&library);
		if (error)
		{
			std::cout << "Failed to initialise freetype font library\n";
			return false;
		}
		initFlag = true;
	}

	FreetypeFont *font = new FreetypeFont(path);
	auto error = FT_New_Face(library, path.c_str(), 0, &font->getFace());
	if (error == FT_Err_Unknown_File_Format)
	{
		std::cout << "Invalid format in font file " << filename << "\n";
		delete font;
		return false;
	}
	else if (error)
	{
		std::cout << "Unable to open or read font file " << filename << "\n";
		delete font;
		return false;
	}
	font->setGlyphSlot();

	renderFont->setFreetypeFont(font);
	Rendering::FontCache::addFont(renderFont);

	// TODO: This should be called when the program exits (does this mean it's not necessary?).
	//FT_Done_FreeType(library);

	return true;
}

