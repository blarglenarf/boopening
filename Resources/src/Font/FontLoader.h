#pragma once

#include <string>

namespace Rendering
{
	class Font;
}

namespace Resources
{
	class FontLoader
	{
	public:
		static bool load(Rendering::Font *font, const std::string &filename);
	};
}

