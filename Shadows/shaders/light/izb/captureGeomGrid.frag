#version 430

#define MAX_EYE_Z 30.0
#define CLUSTERS 32
#define GRID_CELL_SIZE 16.0

#import "cluster"

#include "../../utils.glsl"

#define OPAQUE 0

#if !OPAQUE
	CLUSTER_DEC(geom, uint);
#endif

in VertexData
{
	vec3 esFrag;
} VertexIn;

uniform uint startIndex;

out vec4 fragColor;


void main()
{
	float depth = -VertexIn.esFrag.z;

	float x = gl_FragCoord.x;// / float(GRID_CELL_SIZE);
	float y = gl_FragCoord.y;// / float(GRID_CELL_SIZE);

	// TODO: Pick the four points of the grid cell.

	// TODO: Clamp them inside the triangle.

	// TODO: Raycast to find eye-space depths of these points.

	// TODO: Min/max depth gives clusters to add the id to.

	// TODO: Add id to the grid cell if it's not already there.
	int pixel = CLUSTER_GET_SIZE(geom).x * int(y) + int(x);

	// TODO: Need the depth range of the triangle for this tile.

	int cluster = CLUSTER_GET(depth, MAX_EYE_Z);
	int clusterIndex = CLUSTER_GET_INDEX(geom, vec2(x, y), cluster);

	CLUSTER_ADD_DATA(geom, startIndex + uint(gl_PrimitiveID), clusterIndex);

	fragColor = vec4(0.0);
}

