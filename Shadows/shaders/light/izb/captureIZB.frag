#version 430

#define USE_G_BUFFER 0
#define GRID_CELL_SIZE 1

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#import "lfb"

#include "../../utils.glsl"

// FIXME: Argh I need a full vec4? WTF is this?
LFB_DEC(izb, vec4);


#if USE_G_BUFFER
	uniform ivec4 viewport;
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;
	uniform mat4 invPMatrix;

	// Uniforms for the g-buffer textures.
	uniform sampler2D depthTex;
	uniform sampler2D normalTex;
	uniform sampler2D materialTex;
	uniform sampler2D polyIDTex;
#else
	uniform MaterialBlock
	{
		vec4 ambient;
		vec4 diffuse;
		vec4 specular;
		float shininess;
	} Material;

	in VertexData
	{
		vec3 normal;
		vec3 esFrag;
		vec2 texCoord;
	} VertexIn;

	uniform ivec4 viewport;
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;
	uniform mat4 invPMatrix;

	uniform bool texFlag;
	uniform sampler2D diffuseTex;
	uniform sampler2D normalTex;
#endif

uniform mat4 invMvMatrix;
uniform mat4 lsMvMatrix;
uniform mat4 lsPMatrix;

uniform ivec2 size;


void main()
{
#if USE_G_BUFFER
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	if (depth == 0)
	{
		discard;
		return;
	}

	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	//vec3 esFrag = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).xyz;

	//vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	//vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	//color = getAmbient(material, 0.2);
#else
	vec3 esFrag = VertexIn.esFrag;
	//vec3 normal = VertexIn.normal;
	//color = getAmbient(0.2);
#endif

	// Get esFrag into light space.
	vec4 osFrag = invMvMatrix * vec4(esFrag, 1.0);
	//osFrag = vec4(VertexIn.osFrag, 1.0);
	vec4 lEsFrag = lsMvMatrix * osFrag;

	// TODO: Could alternatively use the mapping function given in the original paper (may give a better distribution?).
	// Then figure out where in the Irregular Z Buffer grid it should be stored (light screen space).
	vec4 lCsFrag = lsPMatrix * lEsFrag;
	vec2 lNdcFrag = lCsFrag.xy / lCsFrag.w;
	vec2 lsFrag = vec2((lNdcFrag.x * 0.5 + 0.5) * float(viewport.z), (lNdcFrag.y * 0.5 + 0.5) * float(viewport.w));

	lsFrag /= float(GRID_CELL_SIZE);

	//vec3 viewDir = normalize(-VertexIn.esFrag);
	//vec3 normal = normalize(VertexIn.normal);

	//vec4 color = vec4(1, 0, 0, 0.2);
	//vec2 izbCoord = gl_FragCoord.xy / float(GRID_CELL_SIZE);
	//LFB_ADD_DATA(izb, izbCoord, vec2(rgba8ToFloat(color), -esFrag.z));

	// Finally store the light's eye-space value in the list (could store anything I guess).
	// TODO: Start with a 2D grid, but later we can switch to 3D (3D will mean fewer fragments to step through later).
	// FIXME: Apparently I'm storing multiples of the same value in the buffer. How is this even possible?
	LFB_ADD_DATA(izb, lsFrag, vec4(lEsFrag.x, lEsFrag.y, lEsFrag.z, 1.0));

	discard;
}

