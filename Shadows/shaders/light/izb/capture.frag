#version 430

#define GRID_CELL_SIZE 1
#define OPAQUE 0
#define USE_G_BUFFER 0

#define CAST_SHADOWS 0

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "lfb"
#import "cluster"

#include "../../utils.glsl"


#if !OPAQUE
	LFB_DEC(lfb, vec2);
#endif

// Light grid.
CLUSTER_DEC(cluster, uint);


#if USE_G_BUFFER
	uniform ivec4 viewport;
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;
	uniform mat4 invPMatrix;
	uniform uint nSphereLights;

	// Uniforms for the g-buffer textures.
	uniform sampler2D depthTex;
	uniform sampler2D normalTex;
	uniform sampler2D materialTex;
	uniform sampler2D polyIDTex;
#else
	uniform MaterialBlock
	{
		vec4 ambient;
		vec4 diffuse;
		vec4 specular;
		float shininess;
	} Material;

	in VertexData
	{
		vec3 normal;
		vec3 esFrag;
		vec3 osFrag;
		vec2 texCoord;
	} VertexIn;

	uniform ivec4 viewport;
	uniform uint nSphereLights;
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;

	uniform bool texFlag;
	uniform sampler2D diffuseTex;
	uniform sampler2D normalTex;

	uniform uint startIndex;
#endif

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

readonly buffer LightZBuffer
{
	float lightZBuffer[];
};

#include "../../light/light.glsl"

out vec4 fragColor;
vec4 workColor;

uniform uint nIndices;

//uniform int maxLevel;



void main()
{
	vec4 color = vec4(0);
	fragColor = color;
	workColor = color;

#if OPAQUE
	color.w = 1.0;
#endif

#if USE_G_BUFFER
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	//vec3 esFrag = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).xyz;

	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	color = getAmbient(material, 0.2);
#else
	vec3 esFrag = VertexIn.esFrag;
	vec3 normal = VertexIn.normal;
	color = getAmbient(0.2);
	#if OPAQUE
		if (color.w == 0)
		{
			discard;
			return;
		}
	#endif
#endif

	// Check light z buffer to see if we're in shadow.
	int lightZIndex = viewport.z * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	float lightZ = lightZBuffer[lightZIndex];
	if (lightZ < 0) // FIXME: Needs to be checked against actual fragment depth in light space.
	{
		fragColor = color;
		return;
	}

	normal = normalize(normal);

	vec3 viewDir = normalize(-esFrag);

	// Convert fragment into cluster space.
	int cluster = CLUSTER_GET(-esFrag.z, MAX_EYE_Z);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Get fragIndex in cluster space.
	int fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, cluster);

	// Apply lighting for all lights at given cluster.
	for (CLUSTER_ITER_BEGIN(cluster, fragIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
	{
		//continue;
		uint id = CLUSTER_GET_DATA(cluster);

		// Sphere.
		if (id < nSphereLights)
		{
			vec4 osLightPos = vec4(sphereLightPositions[id].xyz, 1.0);
			vec4 esLightPos = mvMatrix * osLightPos;
			float lightRadius = sphereLightRadii[id];
			float dist = distance(esLightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			// TODO: Screen space coords are absolutely not necessary, use world space instead.
			// Need light pos in screen space to figure out what grid cell it's in.
			vec4 csLightPos = pMatrix * esLightPos;
			vec2 ssLightPos = ((vec2(csLightPos.xy / csLightPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);
			ivec2 lightGridCoord = ivec2(ssLightPos.x / float(GRID_CELL_SIZE), ssLightPos.y / float(GRID_CELL_SIZE));

			// Need to check grid cells between fragment and light, test geometry to see if it occludes.
			// TODO: Something other than true/false for soft shadows.
		#if CAST_SHADOWS
			//if (checkGridCells(VertexIn.osFrag, osLightPos.xyz, VertexIn.esFrag, esLightPos.xyz, gridCoord, lightGridCoord))
			//{
			//	continue;
			//}
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
		#if USE_G_BUFFER
			color += sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, lightRadius, dist);
		#else
			color += sphereLight(lightColor, esLightPos, esFrag, normal, viewDir, lightRadius, dist);
		#endif
		}

		// Cone.
		else
		{
			vec4 osLightPos = vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			vec4 esLightPos = mvMatrix * osLightPos;
			float lightRadius = coneLightDist[id - nSphereLights];
			float dist = distance(esLightPos.xyz, esFrag);

			// TODO: Add an early out to check if fragment is outside spotlight direction/aperture.
			if (dist > lightRadius)
			{
				continue;
			}

			// TODO: Screen space coords are absolutely not necessary, use world space instead.
			// Need light pos in screen space to figure out what grid cell it's in.
			vec4 csLightPos = pMatrix * esLightPos;
			vec2 ssLightPos = ((vec2(csLightPos.xy / csLightPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);
			ivec2 lightGridCoord = ivec2(ssLightPos / GRID_CELL_SIZE);

			// Need to check grid cells between fragment and light, test geometry to see if it occludes.
			// TODO: Something other than true/false for soft shadows.
		#if CAST_SHADOWS
			//if (checkGridCells(VertexIn.osFrag, osLightPos.xyz, VertexIn.esFrag, esLightPos.xyz, gridCoord, lightGridCoord))
			//{
			//	continue;
			//}
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
		#if USE_G_BUFFER
			color += coneLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		#else
			color += coneLight(lightColor, esLightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		#endif
		}
	}

#if LIGHT_COUNT_DEBUG && 0
	uint count = LIGHT_COUNT_AT(fragIndex);
	float dc = float(count) / 64.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

#if !OPAQUE
	LFB_ADD_DATA(lfb, gl_FragCoord.xy, vec2(rgba8ToFloat(color), -esFrag.z));
#endif

#if OPAQUE
	//fragColor = workColor;
	fragColor = color;
#else
	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
#endif
}

