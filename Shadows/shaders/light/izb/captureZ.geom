#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VertexData
{
	vec3 esFrag;
} VertexIn[3];

out TriangleData
{
	vec3 esFrag;
	flat vec3 vertA;
	flat vec3 vertB;
	flat vec3 vertC;
} VertexOut;


void main()
{
	for (int i = 0; i < 3; i++)
	{
		gl_Position = gl_in[i].gl_Position;
		VertexOut.esFrag = VertexIn[i].esFrag;
		VertexOut.vertA = VertexIn[0].esFrag;
		VertexOut.vertB = VertexIn[1].esFrag;
		VertexOut.vertC = VertexIn[2].esFrag;
		EmitVertex();
	}
	EndPrimitive();
}

