#version 430

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

uniform mat4 lsMvMatrix;
uniform mat4 lsPMatrix;

out VertexData
{
	vec3 esFrag;
} VertexOut;

void main()
{
	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = lsMvMatrix * osVert;
	vec4 csVert = lsPMatrix * esVert;

	VertexOut.esFrag = esVert.xyz;

	gl_Position = csVert;
}

