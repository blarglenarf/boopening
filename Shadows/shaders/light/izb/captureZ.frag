#version 430

#define GRID_CELL_SIZE 1

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
// TODO: If you're planning to write depths, then this line needs to go.
//layout(early_fragment_tests) in;

#import "lfb"

#include "../../utils.glsl"

// FIXME: Argh I need a full vec4? WTF is this?
LFB_DEC(izb, vec4);

in TriangleData
{
	vec3 esFrag;
	flat vec3 vertA;
	flat vec3 vertB;
	flat vec3 vertC;
} VertexIn;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat4 invPMatrix;

uniform uint startIndex;

// TODO: Read this data from the light grid instead.
uniform vec3 lightPos;

uniform mat4 invMvMatrix;
uniform mat4 lsMvMatrix;
uniform mat4 lsPMatrix;
uniform mat4 lsInvMvMatrix;

coherent buffer LightZBuffer
{
	float lightZBuffer[];
};

uniform ivec2 size;

out vec4 fragColor;


bool checkTriangle(vec3 o, vec3 d, out float z)
{
	z = 0;

	// Line intersection test with triangle given by id.
	// Code from http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/ and https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm

	// Triangle.
	vec3 v0 = VertexIn.vertA;
	vec3 v1 = VertexIn.vertB;
	vec3 v2 = VertexIn.vertC;

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.0001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	float u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	float v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * dot(e2, q);

	// Intersection.
	if (t > 0.00001 && t < 1)
	{
		vec3 zPos = d * t;
		z = zPos.z;
		return true;
	}

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}

vec2 pixelFromIZB(vec4 izbFrag)
{
	vec4 osFrag = lsInvMvMatrix * izbFrag;
	vec4 esFrag = mvMatrix * osFrag;
	vec4 csFrag = pMatrix * esFrag;
	vec2 ndcFrag = csFrag.xy / csFrag.w;
	vec2 ssFrag = vec2((ndcFrag.x * 0.5 + 0.5) * float(viewport.z), (ndcFrag.y * 0.5 + 0.5) * float(viewport.w));
	return ssFrag;
}



void main()
{
	vec2 izbPos = gl_FragCoord.xy / float(GRID_CELL_SIZE);
	int izbIndex = LFB_GET_SIZE(izb).x * int(izbPos.y) + int(izbPos.x);

	// For each light space fragment, compute position based on values in the irregular z buffer.
	// FIXME: Some lists are far too long. Need to be shortened somehow (ideally less than 50 per cell)
	// FIXME: Could just store at full res rather than reduced res.
	//int canary = 50;
	for (LFB_ITER_BEGIN(izb, izbIndex); LFB_ITER_CHECK(izb); LFB_ITER_INC(izb))
	{
		//if (canary < 0)
		//	break;
		//canary--;

		// Determine position in the izb, and read fragments from it.
		vec4 izbFrag = LFB_GET_DATA(izb);
		izbFrag.w = 1;

		// TODO: Convert back up to full-screen fragment. If it's not at gl_FragCoord then ignore it.

		// Cast ray from light source to fragment, perform line-triangle intersection test with geometry and get intersection point.
		// FIXME: LightPos needs to be in light space, so 0,0,0?
		float z = 0;
		vec3 d = izbFrag.xyz - vec3(0, 0, 0);
		if (!checkTriangle(vec3(0, 0, 0), d, z))
			continue;

		// FIXME: Apparently there's never an intersection.

		// Calculate camera's screen space xy position for the light space fragment.
		vec2 pixel = pixelFromIZB(izbFrag);

		// TODO: Save min light space depth to screen-space pixel position (need to be an atomicMin on a float, no idea how to do that).
		// TODO: Actually it's an atomicMax, since eye-space z is negative.
		// TODO: Could convert to a 32-bit int and use that instead.
		// TODO: Alternatively, write to gl_FragDepth and save to a depth texture (actually can't be done, since it would need a texture per light).
		int pixelID = viewport.z * int(pixel.y) + int(pixel.x);
		lightZBuffer[pixelID] = -1;
	}

	//discard;
	fragColor = vec4(1, 0, 0, 1);
}

