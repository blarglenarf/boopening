#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VertexData
{
	vec3 esFrag;
} VertexIn[3];

out VertexData
{
	vec3 esFrag;
} VertexOut;


void main()
{
	for (int i = 0; i < gl_in.length(); i++)
	{
		gl_Position = gl_in[i].gl_Position;
		VertexOut.esFrag = VertexIn[i].esFrag;

		EmitVertex();
	}
}

