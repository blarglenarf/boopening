#version 430

#define GRID_CELL_SIZE 1
#define OPAQUE 0
#define USE_G_BUFFER 0
#define USE_MASK 0

#define CAST_SHADOWS 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "lfb"
#import "cluster"

#include "../../utils.glsl"

#include "../../voxel/voxelMask.glsl"

#if !OPAQUE
	LFB_DEC(lfb, vec2);
#endif

// Light grid.
CLUSTER_DEC(cluster, uint);

// Voxel grid data.
buffer GridMasks
{
	uint gridMasks[];
};


buffer VoxelNodeData
{
	uint voxelNodeData[];
};

#if USE_G_BUFFER
	uniform ivec4 viewport;
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;
	uniform mat4 invPMatrix;
	uniform uint nSphereLights;

	// Uniforms for the g-buffer textures.
	uniform sampler2D depthTex;
	uniform sampler2D normalTex;
	uniform sampler2D materialTex;
	uniform sampler2D polyIDTex;
#else
	uniform MaterialBlock
	{
		vec4 ambient;
		vec4 diffuse;
		vec4 specular;
		float shininess;
	} Material;

	in VertexData
	{
		vec3 normal;
		vec3 esFrag;
		vec3 osFrag;
		vec2 texCoord;
	} VertexIn;

	uniform ivec4 viewport;
	uniform uint nSphereLights;
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;

	uniform bool texFlag;
	uniform sampler2D diffuseTex;
	uniform sampler2D normalTex;

	uniform uint startIndex;
#endif

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

#include "../../light/light.glsl"

out vec4 fragColor;
vec4 workColor;

uniform int size;
uniform int nLevels;

uniform uint nIndices;

//uniform int maxLevel;

uniform vec3 wsMin;
uniform vec3 wsMax;



bool checkTriangle(uint id, vec3 o, vec3 d)
{
	// TODO: Return something other than true/false for soft shadows.

	// Line intersection test with triangle given by id.
	// Code from http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/ and https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm

	// Triangle.
	// Convert triangle to eye space so this works properly.
	// FIXME: Reading these is really slow. Find another way...
	vec3 v0 = vertexData[indexData[id * 3 + 0]].xyz;
	vec3 v1 = vertexData[indexData[id * 3 + 1]].xyz;
	vec3 v2 = vertexData[indexData[id * 3 + 2]].xyz;

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.0001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	float u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	float v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * dot(e2, q);

	// Intersection.
	if (t > 0.00001 && t < 1)
		return true;

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}

bool traverseNodes(uvec3 pos)
{
	// Find start node.
	uvec3 maxPos = uvec3(size, size, size);
	uvec3 midPos = maxPos / 2;
	uvec3 minPos = uvec3(0, 0, 0);

	int currIndex = 0;
	int currLevel = 1;

	// What side of the split on x/y/z are we on? Adjust index accordingly.
	if (pos.y >= midPos.y)
	{
		currIndex += 4;
		minPos.y = midPos.y;
	}
	else
		maxPos.y = midPos.y;

	if (pos.x >= midPos.x)
	{
		currIndex += 2;
		minPos.x = midPos.x;
	}
	else
		maxPos.x = midPos.x;

	if (pos.z >= midPos.z)
	{
		currIndex += 1;
		minPos.z = midPos.z;
	}
	else
		maxPos.z = midPos.z;
	midPos = minPos + ((maxPos - minPos) / 2);

	// Follow nodes until we hit null or marked for creation (or we hit the end).
	while (voxelNodeData[currIndex] != 0 && voxelNodeData[currIndex] != 1 && currLevel < nLevels)
	{
		currIndex = int(voxelNodeData[currIndex]);

		if (pos.y >= midPos.y)
		{
			currIndex += 4;
			minPos.y = midPos.y;
		}
		else
			maxPos.y = midPos.y;

		if (pos.x >= midPos.x)
		{
			currIndex += 2;
			minPos.x = midPos.x;
		}
		else
			maxPos.x = midPos.x;

		if (pos.z >= midPos.z)
		{
			currIndex += 1;
			minPos.z = midPos.z;
		}
		else
			maxPos.z = midPos.z;
		midPos = minPos + ((maxPos - minPos) / 2);
		currLevel++;
	}

	// Note: If currLevel != maxLevel, then we didn't reach the end of the tree.
	return currLevel == nLevels;
}

#if USE_MASK
// Here's where you throw in the "smarter" top-down voxel grid traversal.
// Remember, we always start at level 4...
//  32      64    128      256      512        1024
//  4       5      6        7        8          9
// 1024   8192   65536   524288   4194304    33554432
// TODO: Starting at level 4 means we do need a dda of some kind, probably.
// TODO: Or we simply traverse to level 4 irrespective of wether there is geometry or not. <-- Start with this approach.
// TODO: Or we build a sparse svo for level 0,1,2,3.


#if 0

vec3 calcMid(vec3 a, vec3 b)
{
	return vec3(a.x + ((b.x - a.x) / 2.0), a.y + ((b.y - a.y) / 2.0), a.z + ((b.z - a.z) / 2.0));
}

bool isPastVoxel(vec3 aabbMin, vec3 aabbMax, vec3 lMin, vec3 lMax)
{
	// Need to ensure that intersection point isn't past the start/end points.
	// FIXME: Find a faster way of doing this, preferably without if statements.
	if (lMin.x > aabbMax.x || lMin.y > aabbMax.y || lMin.z > aabbMax.z || lMax.x < aabbMin.x || lMax.y < aabbMin.y || lMax.z < aabbMin.z)
		return true;
	return false;
}

bool rayIntersection(vec3 aabbMin, vec3 aabbMax, vec3 origin, vec3 inv)
{
	// X component.
	float t1 = (aabbMin.x - origin.x) * inv.x;
	float t2 = (aabbMax.x - origin.x) * inv.x;
	float tmin = min(t1, t2);
	float tmax = max(t1, t2);

	// Y component.
	t1 = (aabbMin.y - origin.y) * inv.y;
	t2 = (aabbMax.y - origin.y) * inv.y;
	tmin = max(tmin, min(t1, t2));
	tmax = min(tmax, max(t1, t2));

	// Z component.
	t1 = (aabbMin.z - origin.z) * inv.z;
	t2 = (aabbMax.z - origin.z) * inv.z;
	tmin = max(tmin, min(t1, t2));
	tmax = min(tmax, max(t1, t2));

	return tmax > max(tmin, 0.0);
}

void getMinMidMax(int i, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, inout vec3 currMin, inout vec3 currMid, inout vec3 currMax)
{
	switch (i)
	{
	case 0:
		currMin = aabbMin;
		currMax = aabbMid;
		currMid = calcMid(currMin, currMax);
		break;
	case 1:
		currMin = vec3(aabbMin.x, aabbMin.y, aabbMid.z);
		currMax = vec3(aabbMid.x, aabbMid.y, aabbMax.z);
		currMid = calcMid(currMin, currMax);
		break;
	case 2:
		currMin = vec3(aabbMid.x, aabbMin.y, aabbMin.z);
		currMax = vec3(aabbMax.x, aabbMid.y, aabbMid.z);
		currMid = calcMid(currMin, currMax);
		break;
	case 3:
		currMin = vec3(aabbMid.x, aabbMin.y, aabbMid.z);
		currMax = vec3(aabbMax.x, aabbMid.y, aabbMax.z);
		currMid = calcMid(currMin, currMax);
		break;
	case 4:
		currMin = vec3(aabbMin.x, aabbMid.y, aabbMin.z);
		currMax = vec3(aabbMid.x, aabbMax.y, aabbMid.z);
		currMid = calcMid(currMin, currMax);
		break;
	case 5:
		currMin = vec3(aabbMin.x, aabbMid.y, aabbMid.z);
		currMax = vec3(aabbMid.x, aabbMax.y, aabbMax.z);
		currMid = calcMid(currMin, currMax);
		break;
	case 6:
		currMin = vec3(aabbMid.x, aabbMid.y, aabbMin.z);
		currMax = vec3(aabbMax.x, aabbMax.y, aabbMid.z);
		currMid = calcMid(currMin, currMax);
		break;
	case 7:
		currMin = vec3(aabbMid.x, aabbMid.y, aabbMid.z);
		currMax = vec3(aabbMax.x, aabbMax.y, aabbMax.z);
		currMid = calcMid(currMin, currMax);
		break;
	}
}

bool isInVoxel(vec3 p, vec3 aabbMin, vec3 aabbMax)
{
	return (p.x <= aabbMax.x && p.y <= aabbMax.y && p.z <= aabbMax.z && p.x >= aabbMin.x && p.y >= aabbMin.y && p.z >= aabbMin.z);
}


// FIXME: Shader cannot compile with this many levels. Need an iterative approach that doesn't require a bunch of separate functions.

// FIXME: This is here so I don't have a recursive function, need to find a better way of doing this.
// TODO: hash define all of this stuff so that we don't have a bunch of unnecessary functions lying around.
#if 0
bool checkLevel9(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv, lMin, lMax) && VOXEL_GET(gridMask, index + i) != 0)
		{
		#if 0
			if (nLevels > 10)
				found = checkLevel10((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, dInv, lMin, lMax);
			else
				found = true;
			if (found)
				return true;
		#else
			return true;
		#endif
		}
	}
	return false;
}

bool checkLevel8(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv, lMin, lMax) && VOXEL_GET(gridMask, index + i) != 0)
		{
			if (nLevels > 9)
				found = checkLevel9((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, dInv, lMin, lMax);
			else
				found = true;
			if (found)
				return true;
		}
	}
	return false;
}

bool checkLevel7(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv, lMin, lMax) && VOXEL_GET(gridMask, index + i) != 0)
		{
			if (nLevels > 8)
				found = checkLevel8((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, dInv, lMin, lMax);
			else
				found = true;
			if (found)
				return true;
		}
	}
	return false;
}
#endif
bool checkLevel6(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 e, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv) && VOXEL_GET(gridMask, index + i) != 0)
		{
			if (isPastVoxel(currMin, currMax, lMin, lMax))
				continue;
			//if (nLevels > 7)
			//	found = checkLevel7((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, e, dInv, lMin, lMax);
			//else
			//{
			//	// If the destination fragment is in this voxel, return false.
			//	if (isInVoxel(e, currMin, currMax)
			//		found = false;
			//	else
			//		found = true;
			//}
			//return true;
			
			// If the destination fragment is in this voxel, return false.
			if (isInVoxel(e, currMin, currMax))
				return false;
			else
				return true;
		}
	}
	return false;
}

bool checkLevel5(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 e, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv))
		{
			if (isPastVoxel(currMin, currMax, lMin, lMax))
				continue;
			if (VOXEL_GET(gridMask, index + i) == 0)
				continue;
			if (nLevels > 6)
				found = checkLevel6((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, e, dInv, lMin, lMax);
			else
			{
				// If the destination fragment is in this voxel, return false.
				if (isInVoxel(e, currMin, currMax))
					found = false;
				else
					found = true;
			}
			if (found)
				return true;
		}
	}
	return false;
}

bool checkLevel4(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 e, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv))
		{
			if (isPastVoxel(currMin, currMax, lMin, lMax))
				continue;
			if (VOXEL_GET(gridMask, index + i) == 0)
				continue;
			if (nLevels > 5)
				found = checkLevel5((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, e, dInv, lMin, lMax);
			else
			{
				// If the destination fragment is in this voxel, return false.
				if (isInVoxel(e, currMin, currMax))
					found = false;
				else
					found = true;
			}
			if (found)
				return true;
		}
	}
	return false;
}

bool checkLevel3(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 e, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv))
		{
			if (isPastVoxel(currMin, currMax, lMin, lMax))
				continue;
			if (VOXEL_GET(gridMask, index + i) == 0)
				continue;
			if (nLevels > 4)
				found = checkLevel4((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, e, dInv, lMin, lMax);
			else
			{
				// If the destination fragment is in this voxel, return false.
				if (isInVoxel(e, currMin, currMax))
					found = false;
				else
					found = true;
			}
			if (found)
				return true;
		}
	}
	return false;
}

bool checkLevel2(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 e, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv))
		{
			if (isPastVoxel(currMin, currMax, lMin, lMax))
				continue;
			if (VOXEL_GET(gridMask, index + i) == 0)
				continue;
			if (nLevels > 3)
				found = checkLevel3((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, e, dInv, lMin, lMax);
			else
			{
				// If the destination fragment is in this voxel, return false.
				if (isInVoxel(e, currMin, currMax))
					found = false;
				else
					found = true;
			}
			if (found)
				return true;
		}
	}
	return false;
}

bool checkLevel1(int index, int lvl, vec3 aabbMin, vec3 aabbMid, vec3 aabbMax, vec3 o, vec3 e, vec3 dInv, vec3 lMin, vec3 lMax)
{
	vec3 currMin = aabbMin;
	vec3 currMid = aabbMid;
	vec3 currMax = aabbMax;
	bool found = false;

	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv))
		{
			if (isPastVoxel(currMin, currMax, lMin, lMax))
				continue;
			if (VOXEL_GET(gridMask, index + i) == 0)
				continue;
			if (nLevels > 2)
				found = checkLevel2((index + i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, e, dInv, lMin, lMax);
			else
			{
				// If the destination fragment is in this voxel, return false.
				if (isInVoxel(e, currMin, currMax))
					found = false;
				else
					found = true;
			}
			if (found)
				return true;
		}
	}
	return false;
}

bool traverseGrid(vec3 o, vec3 e)
{
	int lvl = 0;
	vec3 d = normalize(e - o);
	vec3 dInv = 1.0 / d;
	vec3 lMin = vec3(min(o.x, e.x), min(o.y, e.y), min(o.z, e.z));
	vec3 lMax = vec3(max(o.x, e.x), max(o.y, e.y), max(o.z, e.z));

	// Root aabb.
	vec3 aabbMin = vec3(0.0);
	vec3 aabbMax = vec3(size);
	vec3 aabbMid = aabbMax / 2;

	vec3 currMin = aabbMin;
	vec3 currMax = aabbMax;
	vec3 currMid = aabbMid;

	bool found = false;

	// Start by going through the 8 children of the root node.
	for (int i = 0; i < 8; i++)
	{
		workColor.x += 0.01;
		getMinMidMax(i, aabbMin, aabbMid, aabbMax, currMin, currMid, currMax);

		if (rayIntersection(currMin, currMax, o, dInv))
		{
			if (isPastVoxel(currMin, currMax, lMin, lMax))
				continue;
			if (VOXEL_GET(gridMask, i) == 0)
				continue;
			// Go to this node's children and repeat the process.
			if (nLevels > 0)
				found = checkLevel1((i + 1) * 8, lvl + 1, currMin, currMid, currMax, o, e, dInv, lMin, lMax);
			else
			{
				// If the destination fragment is in this voxel, return false.
				if (isInVoxel(e, currMin, currMax))
					found = false;
				else
					found = true;
			}
			if (found)
				return true;
		}
	}

	return false;
}

#else


uint a = 0;

// FIXME: For whatever reason, bit 0 is Z and bit 2 is X (swapped around).
uint firstNode(float tx0, float ty0, float tz0, float txm, float tym, float tzm)
{
	uint answer = 0;
	if (tx0 > ty0)
	{
		if (tx0 > tz0)
		{
			// Plane YZ.
			if (tym < tx0)
				answer |= 2; // Bit 1 (2) --> Y.
			if (tzm < tx0)
				answer |= 1; // Bit 2 (4) --> Z.
			return answer;
		}
	}
	else
	{
		if (ty0 > tz0)
		{
			// Plane XZ.
			if (txm < ty0)
				answer |= 4; // Bit 0 (1) --> X.
			if (tzm < ty0)
				answer |= 1; // Bit 2 (4) --> Z.
			return answer;
		}
	}
	// Plane XY.
	if (txm < tz0)
		answer |= 4; // Bit 0 (1) --> X.
	if (tym < tz0)
		answer |= 2; // Bit 1 (2) --> Y.
	return answer;
}

	#if 0
	def test_first_node(self, tx0, ty0, tz0, txm, tym, tzm):
		answer = 0
		if tx0 > ty0:
			if tx0 > tz0:
				//# Plane YZ.
				if tym < tx0:
					answer |= 2 //# Bit 1 (2) --> Y.
				if tzm < tx0:
					answer |= 1 //# Bit 2 (4) --> Z.
				return answer
		else:
			if ty0 > tz0:
				//# Plane XZ.
				if txm < ty0:
					answer |= 4 //# Bit 0 (1) --> X.
				if tzm < ty0:
					answer |= 1 //# Bit 2 (4) --> Z.
				return answer
		//# Plane XY.
		if txm < tz0:
			answer |= 4 //# Bit 0 (1) --> X.
		if tym < tz0:
			answer |= 2 //# Bit 1 (2) --> Y.
		return answer
	#endif


uint newNode(float txm, uint x, float tym, uint y, float tzm, uint z)
{
	if (txm < tym)
	{
		if (txm < tzm)
			return x; // YZ plane.
	}
	else
	{
		if (tym < tzm)
			return y; // XZ plane.
	}
	return z; // XY plane.
}

	#if 0
	def test_new_node(self, txm, x, tym, y, tzm, z):
		if txm < tym:
			if txm < tzm:
				return x //# YZ plane.
		else:
			if tym < tzm:
				return y //# XZ plane.
		return z //# XY plane.
	#endif


// FIXME: Compress down to a vec2 if possible.
struct StackVal
{
	float tx0, ty0, tz0, tx1, ty1, tz1;
	uint currLevel, index;
};

// FIXME: Major occupancy issues caused here! Need to compress the stack, or get rid of it somehow, or find ways of reducing the number of push operations.
#define STACK_SIZE 8
uint stackEnd = 0;
StackVal stack[STACK_SIZE];

void addToStack(float tx0, float ty0, float tz0, float tx1, float ty1, float tz1, uint currLevel, uint index)
{
	// TODO: Some kind of error handling here.
	if (stackEnd >= STACK_SIZE)
	{
		workColor.rgb = vec3(1, 0, 1);
		return;
	}

	stack[stackEnd].tx0 = tx0;
	stack[stackEnd].ty0 = ty0;
	stack[stackEnd].tz0 = tz0;
	stack[stackEnd].tx1 = tx1;
	stack[stackEnd].ty1 = ty1;
	stack[stackEnd].tz1 = tz1;
	stack[stackEnd].currLevel = currLevel;
	stack[stackEnd].index = index;
	stackEnd++;
}


bool checkSubtree(float tx0, float ty0, float tz0, float tx1, float ty1, float tz1, uint currLevel, uint index)
{
	// First value onto the stack.
	addToStack(tx0, ty0, tz0, tx1, ty1, tz1, currLevel, index);

	while (stackEnd > 0)
	{
		workColor.x += 0.01;

		// Grab the top of the stack.
		stackEnd--;
		tx0 = stack[stackEnd].tx0;
		ty0 = stack[stackEnd].ty0;
		tz0 = stack[stackEnd].tz0;
		tx1 = stack[stackEnd].tx1;
		ty1 = stack[stackEnd].ty1;
		tz1 = stack[stackEnd].tz1;
		currLevel = stack[stackEnd].currLevel;
		index = stack[stackEnd].index;

		if (tx1 < 0 || ty1 < 0 || tz1 < 0)
			continue;

		// If it's a terminal node, and the node exists, then we need to exit.
		// TODO: As an alternative, if this function was called and curr_lvl == self.n_levels, then we know the parent was a leaf node that existed, so the raycast ends there.
		// TODO: Problem with this approach is that it requires an extra unnecessary level on the stack.
		if (currLevel == nLevels)
		{
			// uint parentNode = (index + nodeVal) / 8 - 1;
			// TODO: Make sure the parent node isn't a voxel containing the geometry being lit.
			return true;
		}

		float txm = 0.5 * (tx0 + tx1);
		float tym = 0.5 * (ty0 + ty1);
		float tzm = 0.5 * (tz0 + tz1);

		// We pick the first node on this level that we want to enter.
		// Remember that curr_node needs to be x-ord with self.a to account for rays with neg dir.
		uint currNode = firstNode(tx0, ty0, tz0, txm, tym, tzm);
		uint currIndex = index + (currNode ^ a);

		// Now we check the rest of the nodes by first checking the node's children, then checking the next sibling until all siblings are checked.
		int canary = 0;
		while (canary++ < 4096)
		{
			workColor.x += 0.01;
			uint val = VOXEL_GET(gridMask, currIndex);
			uint childIndex = (currIndex + 1) * 8;

			switch (currNode)
			{
			case 0:
				if (val != 0)
					addToStack(tx0, ty0, tz0, txm, tym, tzm, currLevel + 1, childIndex);
				currNode = newNode(txm, 4, tym, 2, tzm, 1);
				break;
			case 1:
				if (val != 0)
					addToStack(tx0, ty0, tzm, txm, tym, tz1, currLevel + 1, childIndex);
				currNode = newNode(txm, 5, tym, 3, tz1, 8);
				break;
			case 2:
				if (val != 0)
					addToStack(tx0, tym, tz0, txm, ty1, tzm, currLevel + 1, childIndex);
				currNode = newNode(txm, 6, ty1, 8, tzm, 3);
				break;
			case 3:
				if (val != 0)
					addToStack(tx0, tym, tzm, txm, ty1, tz1, currLevel + 1, childIndex);
				currNode = newNode(txm, 7, ty1, 8, tz1, 8);
				break;
			case 4:
				if (val != 0)
					addToStack(txm, ty0, tz0, tx1, tym, tzm, currLevel + 1, childIndex);
				currNode = newNode(tx1, 8, tym, 6, tzm, 5);
				break;
			case 5:
				if (val != 0)
					addToStack(txm, ty0, tzm, tx1, tym, tz1, currLevel + 1, childIndex);
				currNode = newNode(tx1, 8, tym, 7, tz1, 8);
				break;
			case 6:
				if (val != 0)
					addToStack(txm, tym, tz0, tx1, ty1, tzm, currLevel + 1, childIndex);
				currNode = newNode(tx1, 8, ty1, 8, tzm, 7);
				break;
			case 7:
				if (val != 0)
					addToStack(txm, tym, tzm, tx1, ty1, tz1, currLevel + 1, childIndex);
				currNode = 8;
				break;
			case 8:
				canary = 9001;
				break;
			}
			currIndex = index + (currNode ^ a);
		}
	}
	return false;
}


	#if 0
	def test_proc_subtree_iterative(self, aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, node_val, curr_lvl, index):
		//# Stack of values that need to be checked.
		max_stack = 1
		stack = []
		stack.append((aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, node_val, curr_lvl, index))

		while len(stack) > 0:
			if len(stack) > max_stack:
				max_stack = len(stack)
			//# We've just entered a subtree from the level above.
			aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, node_val, curr_lvl, index = stack.pop()

			if tx1 < 0 or ty1 < 0 or tz1 < 0:
				continue

			//# If it's a terminal node, and the node exists, then we need to exit.
			//# TODO: This particular node may not actually be the node in this subtree that contains the geometry.
			//#if curr_lvl == self.n_levels - 1:
			//#	if self.nodes[index + node_val] == True:
			//#		curr_min, curr_mid, curr_max = get_aabb(node_val, aabb_min, aabb_mid, aabb_max)
			//#		self.test_print_node(curr_min, curr_max, index + node_val, curr_lvl)
			//#		return True
			//#	else:
			//#		return False
			//# TODO: As an alternative, if this function was called and curr_lvl == self.n_levels, then we know the parent was a leaf node that existed, so the raycast ends there.
			//# TODO: Problem with this approach is that it requires an extra unnecessary level on the stack.
			if curr_lvl == self.n_levels:
				parent_node = (index + node_val) / 8 - 1
				self.test_print_node(aabb_min, aabb_max, parent_node, curr_lvl - 1)
				for i in xrange(len(self.vals)):
					if self.vals[i].x == aabb_min.x and self.vals[i].y == aabb_min.y and self.vals[i].z == aabb_min.z:
						print 'node exists in vals'
				print 'max_stack', max_stack
				return True

			txm = 0.5 * (tx0 + tx1)
			tym = 0.5 * (ty0 + ty1)
			tzm = 0.5 * (tz0 + tz1)

			//# We pick the first node on this level that we want to enter.
			//# Remember that curr_node needs to be x-ord with self.a to account for rays with neg dir.
			curr_node = self.test_first_node(tx0, ty0, tz0, txm, tym, tzm)
			curr_index = index + (curr_node ^ self.a)

			//# Calculate the aabb based on the first node.
			curr_min, curr_mid, curr_max = get_aabb(curr_node ^ self.a, aabb_min, aabb_mid, aabb_max)
			self.test_print_node(curr_min, curr_max, curr_index, curr_lvl)

			//# Now we check the rest of the nodes by first checking the node's children, then checking the next sibling until all siblings are checked.
			while True:
				self.n_ops += 1
				self.n_reads += 1
				child_index = (curr_index + 1) * 8
				if curr_node == 0:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, tx0, ty0, tz0, txm, tym, tzm, self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(txm, 4, tym, 2, tzm, 1)
				elif curr_node == 1:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, tx0, ty0, tzm, txm, tym, tz1, 1 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(txm, 5, tym, 3, tz1, 8)
				elif curr_node == 2:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, tx0, tym, tz0, txm, ty1, tzm, 2 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(txm, 6, ty1, 8, tzm, 3)
				elif curr_node == 3:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, tx0, tym, tzm, txm, ty1, tz1, 3 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(txm, 7, ty1, 8, tz1, 8)
				elif curr_node == 4:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, txm, ty0, tz0, tx1, tym, tzm, 4 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(tx1, 8, tym, 6, tzm, 5)
				elif curr_node == 5:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, txm, ty0, tzm, tx1, tym, tz1, 5 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(tx1, 8, tym, 7, tz1, 8)
				elif curr_node == 6:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, txm, tym, tz0, tx1, ty1, tzm, 6 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(tx1, 8, ty1, 8, tzm, 7)
				elif curr_node == 7:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, txm, tym, tzm, tx1, ty1, tz1, 7 ^ self.a, curr_lvl + 1, child_index))
					curr_node = 8
				elif curr_node == 8:
					break
				//# Remember that curr_node needs to be x-ord with self.a to account for rays with neg dir.
				curr_min, curr_mid, curr_max = get_aabb(curr_node ^ self.a, aabb_min, aabb_mid, aabb_max)
				curr_index = index + (curr_node ^ self.a)
				self.test_print_node(curr_min, curr_max, curr_index, curr_lvl)
		print 'max_stack', max_stack
		return False
	#endif



bool traverseGrid(vec3 o, vec3 e)
{
	// Swap x/y, since the algorithm assumes an ordering of ZYX, whereas our format is ZXY.
	float f = o.x;
	o.x = o.y; o.y = f;
	f = e.x;
	e.x = e.y; e.y = f;

	vec3 dir = normalize(e - o);

	// Fixes for rays with negative direction.
	if (dir.x < 0)
	{
		o.x = float(size) - o.x;
		dir.x = -dir.x;
		a |= 4;
	}
	if (dir.y < 0)
	{
		o.y = float(size) - o.y;
		dir.y = -dir.y;
		a |= 2;
	}
	if (dir.z < 0)
	{
		o.z = float(size) - o.z;
		dir.z = -dir.z;
		a |= 1;
	}

	float divX = 1.0 / dir.x;
	float divY = 1.0 / dir.y;
	float divZ = 1.0 / dir.z;

	float tx0 = (0 - o.x) * divX;
	float tx1 = (float(size) - o.x) * divX;
	float ty0 = (0 - o.y) * divY;
	float ty1 = (float(size) - o.y) * divY;
	float tz0 = (0 - o.z) * divZ;
	float tz1 = (float(size) - o.z) * divZ;

	bool found = false;
	if (max(max(tx0, ty0), tz0) < min(min(tx1, ty1), tz1))
		found = checkSubtree(tx0, ty0, tz0, tx1, ty1, tz1, 0, 0);
	return found;
}

	#if 0
	def test_ray_traversal(self, start_pos, end_pos):
		print 'Starting smart traversal'
		self.n_ops = 0
		self.n_reads = 0

		//# Swap x/y, since the algorithm assumes an ordering of ZYX, whereas our format is ZXY.
		start_pos.x, start_pos.y = start_pos.y, start_pos.x
		end_pos.x, end_pos.y = end_pos.y, end_pos.x

		self.a = 0
		ray_dir = Point(end_pos.x - start_pos.x, end_pos.y - start_pos.y, end_pos.z - start_pos.z)
		l = math.sqrt(ray_dir.x * ray_dir.x + ray_dir.y * ray_dir.y + ray_dir.z * ray_dir.z)
		ray_dir.x /= l
		ray_dir.y /= l
		ray_dir.z /= l

		//# Fixes for rays with negative direction.
		if ray_dir.x < 0:
			start_pos.x = self.size - start_pos.x
			ray_dir.x = -ray_dir.x
			self.a |= 4
		if ray_dir.y < 0:
			start_pos.y = self.size - start_pos.y
			ray_dir.y = -ray_dir.y
			self.a |= 2
		if ray_dir.z < 0:
			start_pos.z = self.size - start_pos.z
			ray_dir.z = -ray_dir.z
			self.a |= 1

		div_x = 1.0 / ray_dir.x
		div_y = 1.0 / ray_dir.y
		div_z = 1.0 / ray_dir.z

		tx0 = (0 - start_pos.x) * div_x
		tx1 = (self.size - start_pos.x) * div_x
		ty0 = (0 - start_pos.y) * div_y
		ty1 = (self.size - start_pos.y) * div_y
		tz0 = (0 - start_pos.z) * div_z
		tz1 = (self.size - start_pos.z) * div_z

		aabb_min = Point(0, 0, 0)
		aabb_max = Point(self.size, self.size, self.size)
		aabb_mid = Point(self.size / 2, self.size / 2, self.size / 2)

		found = False
		if max(max(tx0, ty0), tz0) < min(min(tx1, ty1), tz1):
			//#found = self.test_proc_subtree_recursive(aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, 0, 0, 0)
			found = self.test_proc_subtree_iterative(aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, 0, 0, 0)
		if found == True:
			print 'Found!'
		else:
			print 'No leaf node found!'
	#endif


#endif

#endif

#if USE_MASK
bool checkVoxel(ivec3 pos, int level)
{
	// Since this is a full octree, you could just go straight to the correct node.
	ivec3 maxPos = ivec3(size, size, size);
	ivec3 midPos = maxPos / 2;
	ivec3 minPos = ivec3(0, 0, 0);
	uint currIndex = 0;

	for (int i = 0; i <= level; i++)
	{
		workColor.x += 0.01;

		if (pos.y >= midPos.y)
		{
			currIndex += 4;
			minPos.y = midPos.y;
		}
		else
			maxPos.y = midPos.y;
		if (pos.x >= midPos.x)
		{
			currIndex += 2;
			minPos.x = midPos.x;
		}
		else
			maxPos.x = midPos.x;
		if (pos.z >= midPos.z)
		{
			currIndex += 1;
			minPos.z = midPos.z;
		}
		else
			maxPos.z = midPos.z;
		midPos = minPos + ((maxPos - minPos) / 2);
		if (i < level)
		{
			if (VOXEL_GET(gridMask, currIndex) == 0)
				return false;
			currIndex = (currIndex + 1) * 8;
		}
	}
#if 1
	return VOXEL_GET(gridMask, currIndex) != 0;
#else
	return gridMasks[currIndex] != 0;
#endif

}
#else
bool checkVoxel(ivec3 pos)
{
	return traverseNodes(uvec3(pos.x, pos.y, pos.z));
}
#endif

// Example code modified from slightly incorrect http://stackoverflow.com/questions/16505905/walk-a-line-between-two-points-in-a-3d-voxel-space-visiting-all-cells
bool checkGridCells(vec3 osFrag, vec3 osLight, vec3 esFrag, vec3 esLight, ivec2 ssFrag, ivec2 ssLight)
{
#if USE_G_BUFFER
	uint polyID = uint(texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x);
#else
	uint polyID = startIndex / 3 + uint(gl_PrimitiveID);
#endif

#if 0
	// This is here to just check all triangles for testing purposes. It's also slooooooooooooow.
	vec3 o = osFrag;
	vec3 e = osLight;
	vec3 d = e - o; // If we don't normalize this, then we know t values > 1 are past the end point.

	for (uint i = 0; i < nIndices / 3; i++)
	{
		if (i == polyID)
		{
			continue;
		}
		else if (checkTriangle(i, o, d))
		{
			return true;
		}
	}
	return false;
#else
	// TODO: Completely replace all of this with something that checks the voxel grid.
	// TODO: Which means no more of this dda stuff.

	// Ray origin/direction.
	vec3 o = osLight;
	vec3 e = osFrag;
	vec3 d = e - o; // If we don't normalize this, then we know t values > 1 are past the end point.

	// Start grid cell and end grid cell.
	vec3 startF = (o - wsMin) * vec3(size) / (wsMax - wsMin);
	vec3 endF = (e - wsMin) * vec3(size) / (wsMax - wsMin);

	return traverseGrid(startF, endF);

	ivec3 start = ivec3(int(startF.x), int(startF.y), int(startF.z));
	ivec3 end = ivec3(int(endF.x), int(endF.y), int(endF.z));

	ivec3 s = ivec3(end.x > start.x ? 1 : end.x < start.x ? -1 : 0, end.y > start.y ? 1 : end.y < start.y ? -1 : 0, end.z > start.z ? 1 : end.z < start.z ? -1 : 0);
	ivec3 g = start;

	// Planes for each axis that we will cross.
	ivec3 gp = ivec3(start.x + (end.x > start.x ? 1 : 0), start.y + (end.y > start.y ? 1 : 0), start.z + (end.z > start.z ? 1 : 0));

	// Only used for multiplying up the error margins.
	ivec3 v = ivec3(end.x == start.x ? 1 : end.x - start.x, end.y == start.y ? 1 : end.y - start.y, end.z == start.z ? 1 : end.z - start.z);

	// Error is normalized to v.x * v.y * v.z so we only have to multiply up.
	int vxvy = v.x * v.y;
	int vxvz = v.x * v.z;
	int vyvz = v.y * v.z;

	// Error from the next plane accumulators, scaled up by vx * vy * vz.
	//ivec3 err = ivec3((gp.x - g.x) * vyvz, (gp.y - g.y) * vxvz, (gp.z - g.z) * vxvy);
	ivec3 err = ivec3(gp.x - g.x, gp.y - g.y, gp.z - g.z);
	ivec3 derr = ivec3(s.x * vyvz, s.y * vxvz, s.z * vxvy);

	int canary = 1024;
	for (int i = 0; i < canary; i++)
	{
		// FIXME: Since we're doing this at high res, ignore the end grid cell.
		if (distance(g, end) <= 1)
			break;
		if (g.x == end.x && g.y == end.y && g.z == end.z)
			break;

		//workColor.x += 0.01;

		// Visit g.
	#if USE_MASK
		if (checkVoxel(g, nLevels - 1))
	#else
		if (checkVoxel(g))
	#endif
		{
			// TODO: Get triangle data for this voxel, and raycast against it.
			// For now just see if it works at high res and what the performance is like.
			return true;
		}
	#if 0
		int gridIndex = CLUSTER_GET_INDEX(cluster, ivec2(g.x, g.y), g.z);
		if (checkTriangles(gridIndex, o, d, polyID))
			return true;
	#endif

		// Reached the end.
		if (g.x == end.x && g.y == end.y && g.z == end.z)
			break;

		// Which plane do we cross first.
		ivec3 r = ivec3(abs(err.x), abs(err.y), abs(err.z));

		if (s.x != 0 && (s.y == 0 || r.x < r.y) && (s.z == 0 || r.x < r.z))
		{
			g.x += s.x;
			err.x += derr.x;
		}
		else if (s.y != 0 && (s.z == 0 || r.y < r.z))
		{
			g.y += s.y;
			err.y += derr.y;
		}
		else if (s.z != 0)
		{
			g.z += s.z;
			err.z += derr.z;
		}
	}

	// TODO: Return something other than true/false for soft shadows.

	return false;
#endif
}


void main()
{
	vec4 color = vec4(0);
	fragColor = color;
	workColor = color;

#if OPAQUE
	color.w = 1.0;
#endif

#if USE_G_BUFFER
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	color = getAmbient(material, 0.2);
#else
	vec3 esFrag = VertexIn.esFrag;
	vec3 normal = VertexIn.normal;
	color = getAmbient(0.2);
	#if OPAQUE
		if (color.w == 0)
		{
			discard;
			return;
		}
	#endif
#endif

	// Need fragment in world space for shadows to work properly... or do I?

	vec3 viewDir = normalize(-esFrag);

	// Convert fragment into cluster space.
	int cluster = CLUSTER_GET(-esFrag.z, MAX_EYE_Z);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Get fragIndex in cluster space.
	int fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, cluster);

	// Apply lighting for all lights at given cluster.
	for (CLUSTER_ITER_BEGIN(cluster, fragIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
	{
		//continue;
		uint id = CLUSTER_GET_DATA(cluster);

		// Sphere.
		if (id < nSphereLights)
		{
			vec4 osLightPos = vec4(sphereLightPositions[id].xyz, 1.0);
			vec4 esLightPos = mvMatrix * osLightPos;
			float lightRadius = sphereLightRadii[id];
			float dist = distance(esLightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			// TODO: Screen space coords are absolutely not necessary, use world space instead.
			// Need light pos in screen space to figure out what grid cell it's in.
			vec4 csLightPos = pMatrix * esLightPos;
			vec2 ssLightPos = ((vec2(csLightPos.xy / csLightPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);
			ivec2 lightGridCoord = ivec2(ssLightPos.x / float(GRID_CELL_SIZE), ssLightPos.y / float(GRID_CELL_SIZE));

			// Need to check grid cells between fragment and light, test geometry to see if it occludes.
			// TODO: Something other than true/false for soft shadows.
		#if CAST_SHADOWS
			if (checkGridCells(VertexIn.osFrag, osLightPos.xyz, VertexIn.esFrag, esLightPos.xyz, gridCoord, lightGridCoord))
			{
				continue;
			}
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
		#if USE_G_BUFFER
			color += sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, lightRadius, dist);
		#else
			color += sphereLight(lightColor, esLightPos, esFrag, normal, viewDir, lightRadius, dist);
		#endif
		}

		// Cone.
		else
		{
			vec4 osLightPos = vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			vec4 esLightPos = mvMatrix * osLightPos;
			float lightRadius = coneLightDist[id - nSphereLights];
			float dist = distance(esLightPos.xyz, esFrag);

			// TODO: Add an early out to check if fragment is outside spotlight direction/aperture.
			if (dist > lightRadius)
			{
				continue;
			}

			// TODO: Screen space coords are absolutely not necessary, use world space instead.
			// Need light pos in screen space to figure out what grid cell it's in.
			vec4 csLightPos = pMatrix * esLightPos;
			vec2 ssLightPos = ((vec2(csLightPos.xy / csLightPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);
			ivec2 lightGridCoord = ivec2(ssLightPos / GRID_CELL_SIZE);

			// Need to check grid cells between fragment and light, test geometry to see if it occludes.
			// TODO: Something other than true/false for soft shadows.
		#if CAST_SHADOWS
			if (checkGridCells(VertexIn.osFrag, osLightPos.xyz, VertexIn.esFrag, esLightPos.xyz, gridCoord, lightGridCoord))
			{
				//continue;
			}
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
		#if USE_G_BUFFER
			color += coneLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		#else
			color += coneLight(lightColor, esLightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		#endif
		}
	}

#if LIGHT_COUNT_DEBUG && 0
	uint count = LIGHT_COUNT_AT(fragIndex);
	float dc = float(count) / 64.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

#if !OPAQUE
	LFB_ADD_DATA(lfb, gl_FragCoord.xy, vec2(rgba8ToFloat(color), -esFrag.z));
#endif

#if OPAQUE
	fragColor = workColor;
	//fragColor = color;
#else
	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
#endif
}

