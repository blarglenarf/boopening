#version 430

#define GRID_CELL_SIZE 1
#define OPAQUE 0
#define USE_G_BUFFER 0

#define CAST_SHADOWS 1

// Just in case the link list lfb is currently being used.
#define USE_ATOMIC_COUNTER 0

// At the moment, just assuming near eye-z is 0 and far is 30.
#define MAX_EYE_Z 30.0

#import "lfb"
#import "cluster"

#include "../../utils.glsl"

#if !OPAQUE
	LFB_DEC(lfb, vec2);
#endif

// Light grid.
CLUSTER_DEC(cluster, uint);

// Geometry grid.
CLUSTER_DEC(geom, uint);

#if USE_G_BUFFER
	uniform ivec4 viewport;
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;
	uniform mat4 invPMatrix;
	uniform uint nSphereLights;

	// Uniforms for the g-buffer textures.
	uniform sampler2D depthTex;
	uniform sampler2D normalTex;
	uniform sampler2D materialTex;
	uniform sampler2D polyIDTex;
#else
	uniform MaterialBlock
	{
		vec4 ambient;
		vec4 diffuse;
		vec4 specular;
		float shininess;
	} Material;

	in VertexData
	{
		vec3 normal;
		vec3 esFrag;
		vec3 osFrag;
		vec2 texCoord;
	} VertexIn;

	uniform ivec4 viewport;
	uniform uint nSphereLights;
	uniform mat4 mvMatrix;
	uniform mat4 pMatrix;

	uniform bool texFlag;
	uniform sampler2D diffuseTex;
	uniform sampler2D normalTex;

	uniform uint startIndex;
#endif

readonly buffer SphereLightPositions
{
	vec4 sphereLightPositions[];
};

readonly buffer SphereLightRadii
{
	float sphereLightRadii[];
};

readonly buffer ConeLightDist
{
	float coneLightDist[];
};

readonly buffer ConeLightDir
{
	vec4 coneLightDir[];
};

readonly buffer ConeLightPositions
{
	vec4 coneLightPositions[];
};

readonly buffer LightColors
{
	float lightColors[];
};

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer VertexData
{
	vec4 vertexData[];
};

#include "../../light/light.glsl"

out vec4 fragColor;

uniform uint nIndices;


bool checkTriangle(uint id, vec3 o, vec3 d)
{
	// TODO: Return something other than true/false for soft shadows.

	// Line intersection test with triangle given by id.
	// Code from http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/ and https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm

	// Triangle.
	// Convert triangle to eye space so this works properly.
	// FIXME: Reading these is really slow. Find another way...
	vec3 v0 = vertexData[indexData[id * 3 + 0]].xyz;
	vec3 v1 = vertexData[indexData[id * 3 + 1]].xyz;
	vec3 v2 = vertexData[indexData[id * 3 + 2]].xyz;

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.0001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	float u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	float v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * dot(e2, q);

	// Intersection.
	if (t > 0.00001 && t < 1)
		return true;

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}

bool checkTriangles(int gridIndex, vec3 o, vec3 d, uint polyID)
{
	// TODO: Return something other than true/false for soft shadows.

	// Check all triangles in grid cell gridIndex.
	for (CLUSTER_ITER_BEGIN(geom, gridIndex); CLUSTER_ITER_CHECK(geom); CLUSTER_ITER_INC(geom))
	{
		uint id = CLUSTER_GET_DATA(geom);

		// Obviously don't check the polygon that the fragment came from, that'd be silly...
		if (id == polyID)
		{
			continue;
		}

		if (checkTriangle(id, o, d))
		{
			return true;
		}
	}

	return false;
}

// Example code modified from slightly incorrect http://stackoverflow.com/questions/16505905/walk-a-line-between-two-points-in-a-3d-voxel-space-visiting-all-cells
bool checkGridCells(vec3 osFrag, vec3 osLight, vec3 esFrag, vec3 esLight, ivec2 ssFrag, ivec2 ssLight)
{
#if USE_G_BUFFER
	uint polyID = uint(texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x);
#else
	uint polyID = startIndex + uint(gl_PrimitiveID);
#endif

	return false;
#if 1
	// This is here to just check all triangles for testing purposes. It's also slooooooooooooow.
	vec3 o = osFrag;
	vec3 e = osLight;
	vec3 d = e - o; // If we don't normalize this, then we know t values > 1 are past the end point.

	for (uint i = 0; i < nIndices / 3; i++)
	{
		if (i == polyID)
		{
			continue;
		}
		else if (checkTriangle(i, o, d))
		{
			return true;
		}
	}
	return false;
#else
	// Ray origin/direction.
	vec3 o = osFrag;
	vec3 e = osLight;
	vec3 d = e - o; // If we don't normalize this, then we know t values > 1 are past the end point.

	// Start grid cell (from fragment) and end grid cell (light).
	ivec3 start = ivec3(ssFrag.x, ssFrag.y, CLUSTER_GET(-esFrag.z, MAX_EYE_Z));
	ivec3 end = ivec3(ssLight.x, ssLight.y, CLUSTER_GET(-esLight.z, MAX_EYE_Z));

	// It's possible that these are outside the grid, in which case they need to be capped.
	start.x = clamp(start.x, 0, CLUSTER_GET_SIZE(geom).x - 1);
	start.y = clamp(start.y, 0, CLUSTER_GET_SIZE(geom).y - 1);
	end.x = clamp(end.x, 0, CLUSTER_GET_SIZE(geom).x - 1);
	end.y = clamp(end.y, 0, CLUSTER_GET_SIZE(geom).y - 1);

	ivec3 s = ivec3(end.x > start.x ? 1 : end.x < start.x ? -1 : 0, end.y > start.y ? 1 : end.y < start.y ? -1 : 0, end.z > start.z ? 1 : end.z < start.z ? -1 : 0);
	ivec3 g = start;

	// Planes for each axis that we will cross.
	ivec3 gp = ivec3(start.x + (end.x > start.x ? 1 : 0), start.y + (end.y > start.y ? 1 : 0), start.z + (end.z > start.z ? 1 : 0));

	// Only used for multiplying up the error margins.
	ivec3 v = ivec3(end.x == start.x ? 1 : end.x - start.x, end.y == start.y ? 1 : end.y - start.y, end.z == start.z ? 1 : end.z - start.z);

	// Error is normalized to v.x * v.y * v.z so we only have to multiply up.
	int vxvy = v.x * v.y;
	int vxvz = v.x * v.z;
	int vyvz = v.y * v.z;

	// Error from the next plane accumulators, scaled up by vx * vy * vz.
	//ivec3 err = ivec3((gp.x - g.x) * vyvz, (gp.y - g.y) * vxvz, (gp.z - g.z) * vxvy);
	ivec3 err = ivec3(gp.x - g.x, gp.y - g.y, gp.z - g.z);
	ivec3 derr = ivec3(s.x * vyvz, s.y * vxvz, s.z * vxvy);

	int canary = 512;
	for (int i = 0; i < canary; i++)
	{
		// Visit g.
		int gridIndex = CLUSTER_GET_INDEX(cluster, ivec2(g.x, g.y), g.z);
		if (checkTriangles(gridIndex, o, d, polyID))
			return true;

		// Reached the end.
		if (g.x == end.x && g.y == end.y && g.z == end.z)
			break;

		// Which plane do we cross first.
		ivec3 r = ivec3(abs(err.x), abs(err.y), abs(err.z));

		if (s.x != 0 && (s.y == 0 || r.x < r.y) && (s.z == 0 || r.x < r.z))
		{
			g.x += s.x;
			err.x += derr.x;
		}
		else if (s.y != 0 && (s.z == 0 || r.y < r.z))
		{
			g.y += s.y;
			err.y += derr.y;
		}
		else if (s.z != 0)
		{
			g.z += s.z;
			err.z += derr.z;
		}
	}

	// TODO: Return something other than true/false for soft shadows.

	return false;
#endif
}


void main()
{
	vec4 color = vec4(0);

#if OPAQUE
	color.w = 1.0;
#endif

#if USE_G_BUFFER
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	color = getAmbient(material, 0.2);
#else
	vec3 esFrag = VertexIn.esFrag;
	vec3 normal = VertexIn.normal;
	color = getAmbient(0.2);
	#if OPAQUE
		if (color.w == 0)
		{
			discard;
			return;
		}
	#endif
#endif

	// Need fragment in world space for shadows to work properly... or do I?

	vec3 viewDir = normalize(-esFrag);

	// Convert fragment into cluster space.
	int cluster = CLUSTER_GET(-esFrag.z, MAX_EYE_Z);

	// Composite light volumes using grid.
	ivec2 gridCoord = ivec2(gl_FragCoord.xy / GRID_CELL_SIZE);

	// Get fragIndex in cluster space.
	int fragIndex = CLUSTER_GET_INDEX(cluster, gridCoord, cluster);

	// Apply lighting for all lights at given cluster.
	for (CLUSTER_ITER_BEGIN(cluster, fragIndex); CLUSTER_ITER_CHECK(cluster); CLUSTER_ITER_INC(cluster))
	{
		uint id = CLUSTER_GET_DATA(cluster);

		// Sphere.
		if (id < nSphereLights)
		{
			vec4 osLightPos = vec4(sphereLightPositions[id].xyz, 1.0);
			vec4 esLightPos = mvMatrix * osLightPos;
			float lightRadius = sphereLightRadii[id];
			float dist = distance(esLightPos.xyz, esFrag);

			if (dist > lightRadius)
			{
				continue;
			}

			// Need light pos in screen space to figure out what grid cell it's in.
			vec4 csLightPos = pMatrix * esLightPos;
			vec2 ssLightPos = ((vec2(csLightPos.xy / csLightPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);
			ivec2 lightGridCoord = ivec2(ssLightPos.x / float(GRID_CELL_SIZE), ssLightPos.y / float(GRID_CELL_SIZE));

			// Need to check grid cells between fragment and light, test geometry to see if it occludes.
			// TODO: Something other than true/false for soft shadows.
		#if CAST_SHADOWS
			if (checkGridCells(VertexIn.osFrag, osLightPos.xyz, VertexIn.esFrag, esLightPos.xyz, gridCoord, lightGridCoord))
			{
				continue;
			}
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
		#if USE_G_BUFFER
			color += sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, lightRadius, dist);
		#else
			color += sphereLight(lightColor, esLightPos, esFrag, normal, viewDir, lightRadius, dist);
		#endif
		}

		// Cone.
		else
		{
			vec4 osLightPos = vec4(coneLightPositions[id - nSphereLights].xyz, 1.0);
			vec4 esLightPos = mvMatrix * osLightPos;
			float lightRadius = coneLightDist[id - nSphereLights];
			float dist = distance(esLightPos.xyz, esFrag);

			// TODO: Add an early out to check if fragment is outside spotlight direction/aperture.
			if (dist > lightRadius)
			{
				continue;
			}

			// Need light pos in screen space to figure out what grid cell it's in.
			vec4 csLightPos = pMatrix * esLightPos;
			vec2 ssLightPos = ((vec2(csLightPos.xy / csLightPos.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);
			ivec2 lightGridCoord = ivec2(ssLightPos / GRID_CELL_SIZE);

			// Need to check grid cells between fragment and light, test geometry to see if it occludes.
			// TODO: Something other than true/false for soft shadows.
		#if CAST_SHADOWS
			if (checkGridCells(VertexIn.osFrag, osLightPos.xyz, VertexIn.esFrag, esLightPos.xyz, gridCoord, lightGridCoord))
			{
				continue;
			}
		#endif

			vec4 lightColor = floatToRGBA8(lightColors[id]);
			vec4 coneDir = mvMatrix * vec4(coneLightDir[id - nSphereLights].xyz, 0.0);
			float aperture = coneLightDir[id - nSphereLights].w;
		#if USE_G_BUFFER
			color += coneLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		#else
			color += coneLight(lightColor, esLightPos, esFrag, normal, viewDir, coneDir.xyz, aperture, lightRadius, dist);
		#endif
		}
	}

#if LIGHT_COUNT_DEBUG && 0
	uint count = LIGHT_COUNT_AT(fragIndex);
	float dc = float(count) / 64.0;
	dc = sqrt(dc);
	color.rgb = mix(vec3(avg(color.rgb)), heat(dc), 0.25);
#endif

#if !OPAQUE
	LFB_ADD_DATA(lfb, gl_FragCoord.xy, vec2(rgba8ToFloat(color), -esFrag.z));
#endif

#if OPAQUE
	fragColor = color;
#else
	// This is here to prevent false rendering which seems to happen during capture sometimes...
	fragColor = vec4(0.0);
#endif
}

