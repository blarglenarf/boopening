#version 450

#define USE_G_BUFFER 1

#define LFB_FRAG_TYPE vec4

#import "lfb"

#include "../utils.glsl"

#include "../light/light.glsl"


LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);

// Do we want to index by tiles? If so then include tiles.glsl as well.

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;

// Camera uniforms for fragment transforms.
uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

uniform mat4 lightMvMatrix;
uniform mat4 lightPMatrix;

uniform int width;
uniform int height;

uniform vec3 lightPos;


#if 0
readonly buffer OcclusionBuffer
{
	bool occlusion[];
};
#endif

out vec4 fragColor;


void main()
{
	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	//uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	fragColor = getAmbient(material, 0.2) * 0.5;

	if (depth == 0)
		return;

	// TODO: Check the occlusion buffer to see if we're in shadow or not.
	//int pixel = int(viewport.z) * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	//if (occlusion[pixel])
	//	return;

	vec4 osPos = invMvMatrix * vec4(esFrag, 1);
	vec3 osNorm = normalize((invMvMatrix * vec4(normal, 0)).xyz);

	// Get fragment position in light space.
	vec4 lightEsFrag = lightMvMatrix * osPos;
	vec4 lightCsFrag = lightPMatrix * lightEsFrag;
	vec2 lightNdcFrag = lightCsFrag.xy / lightCsFrag.w;
	vec2 izbSize = vec2(float(width), float(height));
	vec2 lightFragPos = vec2((lightNdcFrag.x * 0.5 + 0.5) * izbSize.x, (lightNdcFrag.y * 0.5 + 0.5) * izbSize.y);
	int lightPixel = int(lightFragPos.x) + int(lightFragPos.y) * width;

	uint count = LFB_COUNT_AT(LFB_NAME, lightPixel);

	vec3 viewDir = normalize(-esFrag);
	vec4 esLightPos = mvMatrix * vec4(lightPos, 1.0);

	vec4 lightColor = vec4(1);
	float dist = distance(esLightPos.xyz, esFrag);

	float att = 0;
	vec4 directColor = sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, 50, dist, true, att);

	fragColor += directColor;

	float dc = float(count) / 1024.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
}

