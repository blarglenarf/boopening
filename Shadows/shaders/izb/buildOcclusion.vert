#version 450

layout(location = 0) in vec3 vertex;

uniform mat4 lightMvMatrix;
uniform mat4 lightPMatrix;

out VertexData
{
	vec3 lightEsVert;
} VertexOut;

void main()
{
	// Transform geometry using light's camera.
	vec4 esVert = lightMvMatrix * vec4(vertex, 1.0);
	vec4 csVert = lightPMatrix * esVert;
	VertexOut.lightEsVert = esVert.xyz;
	gl_Position = csVert;
}

