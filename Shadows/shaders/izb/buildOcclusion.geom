#version 450

layout(triangles) in;

layout(triangle_strip, max_vertices = 3) out;

in VertexData
{
	vec3 lightEsVert;
} VertexIn[3];


out FragData
{
	flat vec3 lightEsVert0;
	flat vec3 lightEsVert1;
	flat vec3 lightEsVert2;
} FragOut; // Hehehehe.


void main()
{
	for (int i = 0; i < 3; i++)
	{
		FragOut.lightEsVert0 = VertexIn[0].lightEsVert;
		FragOut.lightEsVert1 = VertexIn[1].lightEsVert;
		FragOut.lightEsVert2 = VertexIn[2].lightEsVert;
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();
	}
	EndPrimitive();
}

