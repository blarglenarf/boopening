#version 450

#define GET_COUNTS 1

#define LFB_FRAG_TYPE vec4

#if !GET_COUNTS
	#import "lfb"
#endif

#include "../utils.glsl"

// Do we want to index by tiles? If so then include tiles.glsl as well.

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;

// Camera uniforms for fragment transforms.
uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat4 invPMatrix;


uniform mat4 invMvMatrix;
uniform mat4 lightMvMatrix;
uniform mat4 lightPMatrix;

uniform int width;
uniform int height;

#if GET_COUNTS
	coherent buffer Offsets
	{
		uint offsets[];
	};
#else
	LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);
#endif



void main()
{
	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	if (depth == 0)
	{
		discard;
		return;
	}

	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth; // Don't think this is necessary. Not sure if we want eye-space depths or not.

	//vec3 esFrag = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).xyz;

	//vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	//vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	//color = getAmbient(material, 0.2);

	// Get esFrag into light space.
	vec4 osFrag = invMvMatrix * vec4(esFrag, 1.0);
	vec4 lightEsFrag = lightMvMatrix * osFrag;

	// TODO: Could alternatively use the mapping function given in the original paper (may give a better distribution?).
	// Then figure out where in the Irregular Z Buffer grid it should be stored (light screen space).
	vec4 lightCsFrag = lightPMatrix * lightEsFrag;
	vec2 lightNdcFrag = lightCsFrag.xy / lightCsFrag.w;

	vec2 izbSize = vec2(float(width), float(height));
	vec2 lightFragPos = vec2((lightNdcFrag.x * 0.5 + 0.5) * izbSize.x, (lightNdcFrag.y * 0.5 + 0.5) * izbSize.y);

	// Finally store the light's eye-space value in the list (could store anything I guess).
	// TODO: This assumes we're not indexing by tile. Which we probably don't want to do anyway.
	int pixel = int(lightFragPos.x) + int(lightFragPos.y) * int(width);
#if GET_COUNTS
	atomicAdd(offsets[pixel], 1);
#else
	// Not sure if we want to negate the z value or not.
	// TODO: Do we want to store the full fragment position? I think we kinda do.
	//LFB_ADD_DATA(LFB_NAME, pixel, lightEsFrag.z);
	LFB_ADD_DATA(LFB_NAME, pixel, osFrag);
#endif

	discard;
}

