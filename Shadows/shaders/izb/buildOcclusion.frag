#version 450

#define LFB_FRAG_TYPE vec4

in FragData
{
	flat vec3 lightEsVert0;
	flat vec3 lightEsVert1;
	flat vec3 lightEsVert2;
} FragIn;


#include "../utils.glsl"


#import "lfb"

LFB_DEC(LFB_NAME, LFB_FRAG_TYPE);


coherent buffer OcclusionBuffer
{
	bool occlusion[];
};


uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform int width;
uniform int height;

uniform mat4 lightMvMatrix;
uniform mat4 lightPMatrix;

uniform mat4 lightInvMvMatrix;

uniform vec3 lightPos;
uniform vec3 lightDir;


// TODO: In the fragment shader, raycast from the light through the pixel to each fragment in the pixel's list.

// TODO: Set occlusion value to 1 if the light goes through the triangle (maybe using an atomicOr operation).

bool checkTriangle(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 d)
{
	// TODO: Possible to get a colour value here?

	// Line intersection test with triangle given by id.
	// Code from
	// http://www.lighthouse3d.com/tutorials/maths/ray-triangle-intersection/
	// and
	// https://en.wikipedia.org/wiki/M%C3%B6ller%E2%80%93Trumbore_intersection_algorithm

	// Triangle.
	// Convert triangle to eye space so this works properly.
	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	// FIXME: The calculation itself is also pretty slow...
	vec3 h = cross(d, e2);
	float a = dot(e1, h);

	if (a > -0.00001 && a < 0.00001)
		return false;

	float f = 1.0 / a;
	vec3 s = o - v0;
	float u = f * dot(s, h);

	if (u < 0.0 || u > 1.0)
		return false;

	vec3 q = cross(s, e1);
	float v = f * dot(d, q);

	if (v < 0.0 || u + v > 1.0)
		return false;

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * dot(e2, q);

	// Intersection.
	if (t > 0.01)// && t < 0.99)
	{
		// TODO: Compute actual intersection pos.
		//hitPos = o + d * t;
		return true;
	}

	// Line intersection but not ray intersection.
	else
		return false;

	return false;
}


void main()
{
	// If I do this in object-space, then I can use any arbitrary projections I like for the light's frustum.

	vec4 v0 = lightInvMvMatrix * vec4(FragIn.lightEsVert0, 1.0);
	vec4 v1 = lightInvMvMatrix * vec4(FragIn.lightEsVert1, 1.0);
	vec4 v2 = lightInvMvMatrix * vec4(FragIn.lightEsVert2, 1.0);

	// TODO: If using perspective projection: lightPos, if using orthographic: lightDir.
	vec3 o = lightPos;
	vec3 d = lightDir;

	// Repeat for all fragments in the list.
	int pixel = LFB_GET_SIZE(LFB_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	for (LFB_ITER_BEGIN(LFB_NAME, pixel); LFB_ITER_CHECK(LFB_NAME); LFB_ITER_INC(LFB_NAME))
	{
		//vec4 lightEsFrag = LFB_GET_DATA(LFB_NAME);
		//if (FragIn.lightEsFrag.z - 0.01 < lightEsFrag.z)
		//	continue;

		vec4 osFrag = LFB_GET_DATA(LFB_NAME);

		//vec4 osFrag = lightInvMvMatrix * lightEsFrag;
		vec4 esFrag = mvMatrix * osFrag;
		vec4 csFrag = pMatrix * esFrag;
		vec2 ssFrag = getScreenFromClip(csFrag, vec2(width, height));
		int ssPixel = width * int(ssFrag.y) + int(ssFrag.x);

		if (occlusion[ssPixel] == true)
			continue;

		// Casting from fragment to the light is better IMO, but not 100% necessary.
		// TODO: Value of d depends on osFrag and lightPos if using a perspective projection.
		bool inShadow = checkTriangle(v0.xyz, v1.xyz, v2.xyz, osFrag.xyz, -d);
		if (inShadow)
		{
			occlusion[ssPixel] = true;
		}
	}

	discard;
}

