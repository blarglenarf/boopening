#version 450

#define USE_G_BUFFER 1

#include "../utils.glsl"


uniform vec3 mainLightPos;

uniform ivec4 viewport;
uniform mat4 mvMatrix;
uniform mat4 invPMatrix;
uniform mat4 invMvMatrix;

// Uniforms for the g-buffer textures.
uniform sampler2D depthTex;
uniform sampler2D normalTex;
uniform sampler2D materialTex;
uniform sampler2D polyIDTex;
uniform sampler2D positionTex;


#include "../light/light.glsl"

out vec4 fragColor;



void main()
{
	fragColor = vec4(0);

	float depth = texelFetch(depthTex, ivec2(gl_FragCoord.xy), 0).x;
	vec3 esFrag = getEyeFromWindow(vec3(gl_FragCoord.x, gl_FragCoord.y, depth), viewport, invPMatrix).xyz;
	esFrag.z = depth;
	vec3 normal = texelFetch(normalTex, ivec2(gl_FragCoord.xy), 0).xyz;
	vec4 material = texelFetch(materialTex, ivec2(gl_FragCoord.xy), 0);
	uint selfIndex = uint(texelFetch(polyIDTex, ivec2(gl_FragCoord.xy), 0).x);
	fragColor = getAmbient(material, 0.2) * 0.5;

	if (depth == 0)
		return;

	// Are we in shadow?
	vec4 osPos = invMvMatrix * vec4(esFrag, 1);
	vec4 osLight = vec4(mainLightPos.xyz, 1);

	vec3 viewDir = normalize(-esFrag);
	vec4 esLightPos = mvMatrix * osLight;

	vec4 lightColor = vec4(1);
	float dist = distance(esLightPos.xyz, esFrag);

	float att = 0;
	vec4 directColor = sphereLight(material.xyz, lightColor, esLightPos, esFrag, normal, viewDir, 50, dist, true, att);

	fragColor += directColor;

#if 0
	float dc = float(count) / 128.0;
	dc = sqrt(dc);
	fragColor.rgb = mix(vec3(avg(fragColor.rgb)), heat(dc), 0.25);
#endif
}

