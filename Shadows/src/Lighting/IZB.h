#pragma once

#include "Lighting.h"

#include "../../../Renderer/src/Lighting/GBuffer.h"
#include "../../../Renderer/src/RenderObject/Shader.h"
#include "../../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../../Renderer/src/RenderObject/Texture.h"
#include "../../../Renderer/src/LFB/LFB.h"
#include "../../../Renderer/src/Camera.h"

#include <vector>

class IZB : public Lighting
{
private:
	Rendering::GBuffer gBuffer;

	// Can just use one IZB to cover the scene.
	Rendering::LFB izb;

	// Or cascade multiple IZBs of different resolutions.
	std::vector<Rendering::LFB> izbCascades;

	Rendering::Shader countIZBShader;
	Rendering::Shader buildIZBShader;

	Rendering::FrameBuffer buildOcclusionFbo;
	Rendering::RenderBuffer buildOcclusionDepth;

	std::vector<Rendering::FrameBuffer*> fboCascades;
	std::vector<Rendering::RenderBuffer*> depthCascades;

	Rendering::StorageBuffer occlusionBuffer;
	Rendering::Shader buildOcclusionShader;

	Rendering::Shader renderShadowShader;

	Rendering::Shader renderCountShader;

	Rendering::StorageBuffer minBuffer;
	Rendering::StorageBuffer maxBuffer;
	Rendering::Shader minmaxFirstShader;
	Rendering::Shader minmaxShader;

	int minmaxSize;
	Math::vec3 lightMin;
	Math::vec3 lightMax;

private:
	void captureGBuffer(App *app);

	void buildIZB(App *app);
	void buildIZBCascades(App* app);

	void buildOcclusionBuffer(App *app);
	void renderGeometry(App *app);

	void fitLightToFrustum(App *app);

	void debugRenderLight(App *app);
	void debugRenderCounts(App *app);

public:
	IZB() : Lighting(), minmaxSize(0) {}
	virtual ~IZB() { for (auto *a : fboCascades) delete a; for (auto *a : depthCascades) delete a; }

	virtual void init(App *app) override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLighting(App *app) override;
	virtual void useScene(App *app) override { (void) (app); }

	virtual void reloadShaders(App *app) override;
	virtual void saveData(App *app) override;
};

