#include "Hybrid.h"

#if 0

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"

#include <string>

#define MAX_FRAGS 512
#define VOXEL_RES 128
#define CLUSTER_CULL 1
#define PROFILE_MEMORY 1
#define USE_MASK 1
#define RENDER_VOXELS 1
#define VOXEL_OUTLINES 1
#define LINEARISED 0 // Can't actually build a linearised geometry grid yet...

using namespace Rendering;
using namespace Math;


void Hybrid::countLightClusters(App *app)
{
	app->getProfiler().start("Light Grid Count");

	lightGrid.beginCountFrags();

	countLightGridShader.bind();

	app->setLightUniforms(&countLightGridShader);
	app->setCameraUniforms(&countLightGridShader);
	countLightGridShader.setUniform("size", ivec2(width, height));

#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&countLightGridShader);
#endif

	lightGrid.setCountUniforms(&countLightGridShader);

	// Render all the lights.
	countLightGridShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	countLightGridShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	countLightGridShader.unbind();

	app->getProfiler().time("Light Grid Count");

	app->getProfiler().start("Light Grid Prefix");

	lightGrid.endCountFrags();

	app->getProfiler().time("Light Grid Prefix");
}

void Hybrid::createLightClusters(App *app)
{
	lightGrid.beginComposite();
	createLightGridShader.bind();
	lightGrid.setUniforms(&createLightGridShader);

#if CLUSTER_CULL
	app->getDepthMask().setUniforms(&createLightGridShader);
#endif

	app->setLightUniforms(&createLightGridShader);
	app->setCameraUniforms(&createLightGridShader);
	createLightGridShader.setUniform("size", ivec2(width, height));

	// Render all the lights.
	createLightGridShader.setUniform("indexOffset", (unsigned int) 0);
	app->getVaoSphereLights().render();
	createLightGridShader.setUniform("indexOffset", (unsigned int) app->getNSphereLights());
	app->getVaoConeLights().render();

	createLightGridShader.unbind();
	lightGrid.endComposite();
}

void Hybrid::buildLightGrid(App *app)
{
	// Capture lights as billboarded quads, and raycast for front/back depths, then add to clusters.
	glPushAttrib(GL_ENABLE_BIT);

	// Disable if using billboarded quads, enable if using light outlines.
	glDisable(GL_CULL_FACE);
	//glEnable(GL_CULL_FACE);

	// Resize based on grid size.
	app->setTmpViewport(width, height);

	if (lightGrid.getType() == Cluster::LINEARIZED)
		countLightClusters(app);

	// Now store the ids in their clusters.
	app->getProfiler().start("Light Grid");

	// This can only repeat if using link lists.
	lightGrid.beginCapture();
	createLightClusters(app);
	if (lightGrid.endCapture())
	{
		lightGrid.beginCapture();
		createLightClusters(app);
		if (lightGrid.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	app->getProfiler().time("Light Grid");

	// Restore original size.
	app->restoreViewport();

	glPopAttrib();
}

void Hybrid::createVoxelMask(App *app)
{
	//static bool createFlag = false;

	//if (createFlag)
	//	return;
	//createFlag = true;

	// Extra cameras not needed if swizzling vertex components and using conservative rasterization.
	float scale = (float) size / ((float) wsMax - (float) wsMin);
#if 0
	auto &voxelCamX = Renderer::instance().getCamera("voxelX");
	if (voxelCamX.getType() != Camera::Type::ORTHO)
	{
		voxelCamX.setType(Camera::Type::ORTHO);
		voxelCamX.setDistance(0, size);
		voxelCamX.setPos({-wsMin, wsMin, -wsMin});
		voxelCamX.setEulerRot({0, 1.570796, 0});
		voxelCamX.setZoom(scale);
		voxelCamX.setViewport(0, 0, size, size);
		voxelCamX.update(0.1f);
	}
	auto &voxelCamY = Renderer::instance().getCamera("voxelY");
	if (voxelCamY.getType() != Camera::Type::ORTHO)
	{
		voxelCamY.setType(Camera::Type::ORTHO);
		voxelCamY.setDistance(0, size);
		voxelCamY.setPos({wsMin, -wsMin, -wsMin});
		voxelCamY.setEulerRot({-1.570796, 0, 0});
		voxelCamY.setZoom(scale);
		voxelCamY.setViewport(0, 0, size, size);
		voxelCamY.update(0.1f);
	}
#endif
	auto &voxelCamZ = Renderer::instance().getCamera("voxelZ");
	if (voxelCamZ.getType() != Camera::Type::ORTHO)
	{
		voxelCamZ.setType(Camera::Type::ORTHO);
		voxelCamZ.setDistance(0, size);
		voxelCamZ.setPos({wsMin, wsMin, -wsMin});
		voxelCamZ.setZoom(scale);
		voxelCamZ.setViewport(0, 0, size, size);
		voxelCamZ.update(0.1f);
	}

	static Texture2D colorBuffer;
	static RenderBuffer depthBuffer;
	static FrameBuffer fbo(1024, 1024);
	if (!fbo.isGenerated())
		fbo.create(GL_RGBA, &colorBuffer, &depthBuffer, true);

	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
	}

	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);

	app->getProfiler().start("Voxel Mask");

	voxelMask.beginCreateMask();
	auto &shader = voxelMask.getMaskShader();

	// Needs to render to a large enough buffer.
	fbo.bind();

	// Render geometry.
#if 0
	voxelCamX.uploadViewport();
	voxelCamX.setUniforms(&shader);
	app->getGpuMesh().render(false);

	voxelCamY.uploadViewport();
	voxelCamY.setUniforms(&shader);
	app->getGpuMesh().render(false);
#endif
	voxelCamZ.uploadViewport();
	voxelCamZ.setUniforms(&shader);

	app->getGpuMesh().render(false);
	
	voxelMask.endCreateMask();

	fbo.unbind();

	app->getProfiler().time("Voxel Mask");

	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);

	if (opaqueFlag == 1)
		glPopAttrib();

	Renderer::instance().getActiveCamera().uploadViewport();
}

void Hybrid::renderVoxelMask(App *app)
{
	static size_t nVerts = 0;

	bool updateFlag = false;
	if (nVerts != app->getGpuMesh().getVao().getVertexBuffer()->getNVerts())
	{
		nVerts = app->getGpuMesh().getVao().getVertexBuffer()->getNVerts();
		updateFlag = true;
	}

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	Renderer::instance().getActiveCamera().upload();
	voxelMask.debugRender(voxelMask.getNLevels() - 1, VOXEL_OUTLINES == 1, updateFlag);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void Hybrid::createVoxelTree(App *app)
{
	// Extra cameras not needed if swizzling vertex components and using conservative rasterization.
	float scale = (float) size / ((float) wsMax - (float) wsMin);
	auto &voxelCamZ = Renderer::instance().getCamera("voxelZ");
	if (voxelCamZ.getType() != Camera::Type::ORTHO)
	{
		voxelCamZ.setType(Camera::Type::ORTHO);
		voxelCamZ.setDistance(0, (float) size);
		voxelCamZ.setPos({wsMin, wsMin, -wsMin});
		voxelCamZ.setZoom(scale);
		voxelCamZ.setViewport(0, 0, size, size);
		voxelCamZ.update(0.1f);
	}

	static Texture2D colorBuffer;
	static RenderBuffer depthBuffer;
	static FrameBuffer fbo(1024, 1024);
	if (!fbo.isGenerated())
		fbo.create(GL_RGBA, &colorBuffer, &depthBuffer, true);

	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
	}

	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);

	app->getProfiler().start("Voxel Tree");

	voxelTree.beginCreateTree();
	auto &shader = voxelTree.getCreateShader();

	// Needs to render to a large enough buffer.
	fbo.bind();

	// Render geometry.
	voxelCamZ.uploadViewport();
	voxelCamZ.setUniforms(&shader);

	app->getGpuMesh().render(false);

	// If not enough voxels were allocated, we need to re-render.
	if (voxelTree.endCreateTree())
	{
		std::cout << "Resizing voxel tree\n";

		voxelTree.beginCreateTree();
		voxelCamZ.uploadViewport();
		voxelCamZ.setUniforms(&shader);
		app->getGpuMesh().render(false);

		// There should always be enough voxels at this point.
		if (voxelTree.endCreateTree())
			std::cout << "Something very wrong just happened\n";
	}

	fbo.unbind();

	app->getProfiler().time("Voxel Tree");

	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);

	if (opaqueFlag == 1)
		glPopAttrib();

	Renderer::instance().getActiveCamera().uploadViewport();
}

void Hybrid::renderVoxelTree(App *app)
{
	static size_t nVerts = 0;

	bool updateFlag = false;
	if (nVerts != app->getGpuMesh().getVao().getVertexBuffer()->getNVerts())
	{
		nVerts = app->getGpuMesh().getVao().getVertexBuffer()->getNVerts();
		updateFlag = true;
	}

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	Renderer::instance().getActiveCamera().upload();
	voxelTree.debugRender(voxelTree.getNLevels() - 1, VOXEL_OUTLINES == 1, updateFlag);
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void Hybrid::renderGeometry(App *app)
{
	// TODO: Render full-res geometry (either deferred or forward), applying lighting from light grid.
	// TODO: For each light, ray-cast from fragment to light source, check geometry in the geometry grid for intersections.

	// TODO: implement me!
	if (gBufferFlag)
	{
		app->getProfiler().start("Light Deferred");

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		captureGeometryShader.bind();
		gBuffer.bindTextures();
		gBuffer.setUniforms(&captureGeometryShader);
		app->setCameraUniforms(&captureGeometryShader);
		app->setLightUniforms(&captureGeometryShader);
		lightGrid.setUniforms(&captureGeometryShader, false);
		//captureGeometryShader.setUniform("nIndices", app->getNIndices());
		//captureGeometryShader.setUniform("IndexData", &app->getIndexData());
		//captureGeometryShader.setUniform("VertexData", &app->getVertexData());

		Renderer::instance().drawQuad();

		gBuffer.unbindTextures();
		captureGeometryShader.unbind();

		app->getProfiler().time("Light Deferred");
	}
	else
	{
	#if USE_MASK
		app->captureGeometry(&captureGeometryShader, &lightGrid, nullptr, &voxelMask, true, true);
	#else
		app->captureGeometry(&captureGeometryShader, &lightGrid, &voxelTree, nullptr, true, true);
	#endif
	}
}


Hybrid::Hybrid(int opaqueFlag) : Lighting(opaqueFlag), gBufferFlag(false), width(0), height(0), wsMin(-10), wsMax(10), size(VOXEL_RES)
{
}

void Hybrid::init()
{
	auto lightGridCountVert = ShaderSourceCache::getShader("countLightGridVert").loadFromFile("shaders/light/countLightRay.vert");
	auto lightGridCountGeom = ShaderSourceCache::getShader("countLightGridGeom").loadFromFile("shaders/light/countLightRay.geom");
	auto lightGridCountFrag = ShaderSourceCache::getShader("countLightGridFrag").loadFromFile("shaders/light/countLightRay.frag");
	lightGridCountFrag.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	lightGridCountFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	lightGridCountFrag.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	lightGridCountFrag.setDefine("USE_CLUSTER", "1");
	lightGridCountFrag.setDefine("ATOMIC_ADD", "1");
	countLightGridShader.create(&lightGridCountVert, &lightGridCountFrag, &lightGridCountGeom);
}

void Hybrid::render(App *app)
{
	if (gBufferFlag && opaqueFlag != 1)
		return;

	auto &camera = Renderer::instance().getActiveCamera();

	if (gBufferFlag)
		gBuffer.resize(camera.getWidth(), camera.getHeight());

	// Re-allocate cluster data if the camera has resized.
	if (width != ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE) || height != ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE))
	{
		width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
		height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);
		lightGrid.resize(width, height);

	#if CLUSTER_CULL
		app->getDepthMask().resize(width, height);
	#endif
	}

	if (gBufferFlag)
	{
		gBuffer.beginCapture();
		app->captureGeometry(&gBuffer.getCaptureShader(), nullptr, nullptr, nullptr, false);
		gBuffer.endCapture();
	}

#if CLUSTER_CULL
	// Create masks to determine which clusters are active.
	if (gBufferFlag)
		app->createDepthMask(gBuffer.getDepthTexture());
	else
		app->createDepthMask();
#endif

	buildLightGrid(app);

#if USE_MASK
	createVoxelMask(app);
#else
	createVoxelTree(app);
#endif

	if (opaqueFlag == 1)
	{
		glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

#if !RENDER_VOXELS
	renderGeometry(app);
#else
	renderGeometry(app);
	#if USE_MASK
		renderVoxelMask(app);
	#else
		renderVoxelTree(app);
	#endif
#endif

#if USE_MASK
	voxelMask.debugRenderBox();
#else
	voxelTree.debugRenderBox();
#endif

	if (opaqueFlag == 1)
		glPopAttrib();
	else
		// Sort and composite as normal.
		app->sortGeometry();
}

void Hybrid::update(float dt)
{
	(void) (dt);
}

void Hybrid::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (type);

	auto &camera = Renderer::instance().getActiveCamera();
	width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

#if LINEARISED
	lightGrid.setType(Cluster::LINEARIZED, "cluster", CLUSTERS, Cluster::UINT, true);
#else
	lightGrid.setType(Cluster::LINK_LIST, "cluster", CLUSTERS, Cluster::UINT, true);
#endif

	lightGrid.setProfiler(&app->getProfiler());
	if (gBufferFlag)
		gBuffer.setProfiler(&app->getProfiler());

	// Shader for capturing light grid.
	auto vertLight = ShaderSourceCache::getShader("lightGridVert").loadFromFile("shaders/light/captureLightRay.vert");
	auto geomLight = ShaderSourceCache::getShader("lightGridGeom").loadFromFile("shaders/light/captureLightRay.geom");
	auto fragLight = ShaderSourceCache::getShader("lightGridFrag").loadFromFile("shaders/light/captureLightRay.frag");
	fragLight.setDefine("CLUSTER_CULL", Utils::toString(CLUSTER_CULL));
	fragLight.setDefine("MASK_SIZE", Utils::toString(MASK_SIZE));
	fragLight.setDefine("MAX_EYE_Z", Utils::toString(MAX_EYE_Z));
	fragLight.setDefine("USE_CLUSTER", "1");
	fragLight.setDefine("ATOMIC_ADD", "1");

	createLightGridShader.release();
	createLightGridShader.create(&vertLight, &fragLight, &geomLight);

	// Shader for capturing geometry, applies lighting during capture.
	auto &geomCapVert = ShaderSourceCache::getShader("hybridCapVert").loadFromFile("shaders/light/hybrid/capture.vert");
	auto &geomCapFrag = ShaderSourceCache::getShader("hybridCapFrag").loadFromFile("shaders/light/hybrid/capture.frag");
	geomCapFrag.setDefine("GRID_CELL_SIZE", Utils::toString(GRID_CELL_SIZE));
	geomCapFrag.setDefine("CLUSTERS", Utils::toString(CLUSTERS));
	geomCapFrag.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	geomCapFrag.setDefine("USE_MASK", Utils::toString(USE_MASK));
	if (gBufferFlag)
	{
		geomCapVert.setDefine("USE_G_BUFFER", "1");
		geomCapFrag.setDefine("USE_G_BUFFER", "1");
	}

	captureGeometryShader.release();
	captureGeometryShader.create(&geomCapVert, &geomCapFrag);

	lightGrid.resize(width, height);

#if USE_MASK
	voxelMask.setProfiler(&app->getProfiler());
	voxelMask.regen(size);
	voxelMask.setWsMin(Math::vec3(wsMin, wsMin, wsMin));
	voxelMask.setWsMax(Math::vec3(wsMax, wsMax, wsMax));
#else
	voxelTree.setProfiler(&app->getProfiler());
	voxelTree.regen(size);
	voxelTree.setWsMin(Math::vec3(wsMin, wsMin, wsMin));
	voxelTree.setWsMax(Math::vec3(wsMax, wsMax, wsMax));
#endif
	//voxelMask.resize((int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE), (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE));
}

void Hybrid::saveLFB(App *app)
{
	(void) (app);

	auto s = lightGrid.getStr();
	Utils::File f1("lightGrid.txt");
	f1.write(s);

#if USE_MASK
	s = voxelMask.getStr();
#else
	s = voxelTree.getStr();
#endif
	Utils::File f2("voxelGrid.txt");
	f2.write(s);

	//app->saveGeometryLFB();
}

#endif

