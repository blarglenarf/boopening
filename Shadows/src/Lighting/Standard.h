#pragma once

#include "Lighting.h"

#include "../../../Renderer/src/Lighting/GBuffer.h"
#include "../../../Renderer/src/RenderObject/Shader.h"

class Standard : public Lighting
{
private:
	Rendering::Shader basicShader;
	Rendering::GBuffer gBuffer;

private:
	void captureGeometry(App *app);
	void renderGBuffer(App *app);

public:
	Standard() : Lighting() {}
	virtual ~Standard() {}

	virtual void init(App *app) override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLighting(App *app) override;
	virtual void useScene(App *app) override { (void) (app); }

	virtual void reloadShaders(App *app) override;
	virtual void saveData(App *app) override { (void) (app); }
};

