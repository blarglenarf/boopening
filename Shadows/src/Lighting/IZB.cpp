#include "IZB.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"

#include <string>
#include <iostream>


// TODO: Can we extend this to reflections as well as just shadows?

// TODO: Store list of triangles affecting each light-space pixel I guess.
// TODO: Don't actually need all the triangles affecting the pixel, fully occluded ones can be ignored (how to do this?).
// TODO: Does this affect performance? If so, by how much?
// TODO: What about triangle id's but sorted by depth, captured from the light's perspective...

// TODO: Load balancing can be fixed either by:
// TODO: Cascaded IZB's.
// TODO: A BMAish approach.
// TODO: Maybe even some kind of combination of these?
// TODO: May even mean that we don't need to minmax the visible frustum.


// If cascading, then this is the minimum resolution IZB.
#define IZB_WIDTH 512
#define IZB_HEIGHT 512

// TODO: Decided to skip cascading for now since it's not working properly...
#define IZB_CASCADE 1
#define N_CASCADES 1

// TODO: Implement me!
// TODO: This kind of still requires cascading, it's just a bit more fine-grained.
// TODO: Also it definitely requires each cascaded IZB to have greater resolution.
#define IZB_BMA 1

#define DEBUG_LIGHT 0
#define DEBUG_COUNTS 0

using namespace Rendering;

static void izbPass(Camera &camera, Camera &lightCam, LFB &izb, GBuffer &gBuffer, Shader &shader, bool countFlag)
{
	shader.bind();

	gBuffer.bindTextures();
	gBuffer.setUniforms(&shader);
	if (countFlag)
		izb.setCountUniforms(&shader);
	else
		izb.setUniforms(&shader);

	shader.setUniform("viewport", camera.getViewport());
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("invMvMatrix", camera.getTransform());
	shader.setUniform("invPMatrix", camera.getInverseProj());

	shader.setUniform("lightMvMatrix", lightCam.getInverse());
	shader.setUniform("lightPMatrix", lightCam.getProjection());

	shader.setUniform("width", IZB_WIDTH);
	shader.setUniform("height", IZB_HEIGHT);

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();

	shader.unbind();
}


void IZB::captureGBuffer(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	auto &shader = gBuffer.getCaptureShader();

	// Capture data into the g-buffer.
	gBuffer.beginCapture();
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	//gBuffer.setUniforms(&shader);
	app->getGpuMesh().render();

	shader.unbind();
	gBuffer.endCapture();
}

void IZB::buildIZB(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	auto &lightCam = app->getLightCam();

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);

	app->getProfiler().start("build izb");

	// Count fragments if necessary.
	if (izb.getType() != LFB::LINK_LIST)
	{
		izb.beginCountFrags();
		izbPass(camera, lightCam, izb, gBuffer, countIZBShader, true);
		izb.endCountFrags();
	}

	// Store fragments.
	izb.beginCapture();

	izbPass(camera, lightCam, izb, gBuffer, buildIZBShader, false);

	// Again, if we didn't allocate enough memory.
	if (izb.endCapture())
	{
		izb.beginCapture();

		izbPass(camera, lightCam, izb, gBuffer, buildIZBShader, false);

		if (izb.endCapture())
			std::cout << "Something very wrong happened\n";
	}

	app->getProfiler().time("build izb");

	glPopAttrib();
}

void IZB::buildIZBCascades(App* app)
{
	// TODO: First just get the per-pixel counts at the lowest resolution.
	// TODO: Then set the different levels of the stencil buffer based on the counts.
	// TODO: Then build the different izb levels based on stencil buffer and counts.
	// TODO: But won't we need a different resolution of stencil buffer for each izb?
	// TODO: How do I handle that? A different fbo and stencil buffer for each izb cascade?
	// TODO: Non-linear rasterization sounds a whole lot easier than this.

	auto &camera = Renderer::instance().getActiveCamera();
	auto &lightCam = app->getLightCam();

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);

	app->getProfiler().start("build izb");

	for (int i = 0; i < N_CASCADES; i++)
	{
		auto &izb = izbCascades[i];

		// Count fragments if necessary.
		if (izb.getType() != LFB::LINK_LIST)
		{
			izb.beginCountFrags();
			izbPass(camera, lightCam, izb, gBuffer, countIZBShader, true);
			izb.endCountFrags();
		}

		// Store fragments.
		izb.beginCapture();

		izbPass(camera, lightCam, izb, gBuffer, buildIZBShader, false);

		// Again, if we didn't allocate enough memory.
		if (izb.endCapture())
		{
			izb.beginCapture();

			izbPass(camera, lightCam, izb, gBuffer, buildIZBShader, false);

			if (izb.endCapture())
				std::cout << "Something very wrong happened\n";
		}
	}

	app->getProfiler().time("build izb");

	glPopAttrib();
}

void IZB::buildOcclusionBuffer(App *app)
{
	static Shader zeroShader;
	if (!zeroShader.isGenerated())
	{
		auto zeroVert = ShaderSourceCache::getShader("zeroVert").loadFromFile("shaders/zero.vert");
		zeroVert.replace("DATA_TYPE", "bool"); // Is bool the right data type? Is it just 1 byte or 4?
		zeroShader.create(&zeroVert);
	}

	auto &camera = Renderer::instance().getActiveCamera();
	auto &lightCam = app->getLightCam();

	if (!occlusionBuffer.isGenerated() || width != camera.getWidth() || height != camera.getHeight())
	{
		width = camera.getWidth();
		height = camera.getHeight();
		occlusionBuffer.create(nullptr, (size_t) width * (size_t) height * 4); // 1 byte per pixel (either shadowed, or not shadowed).
	}

	app->getProfiler().start("build occlusion");

	buildOcclusionFbo.bind();

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	// First zero the occlusion buffer.
	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("Data", &occlusionBuffer);
	glDrawArrays(GL_POINTS, 0, width * height);
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Then conservatively rasterize geometry from light's perspective and store occlusion for each pixel (yes/no).
	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);

#if IZB_CASCADE && 0
	// TODO: We need stencil buffers to be set so that we don't rasterize pixels unnecessarily.
	// TODO: Need increasingly large framebuffer objects to handle this.
	int amt = 2;
	for (int i = 0; i < N_CASCADES; i++)
	{
		auto &izb = izbCascades[i];

		izb.beginComposite();
		buildOcclusionShader.bind();
		izb.setUniforms(&buildOcclusionShader);
		buildOcclusionShader.setUniform("OcclusionBuffer", &occlusionBuffer);
		buildOcclusionShader.setUniform("mvMatrix", camera.getInverse());
		buildOcclusionShader.setUniform("pMatrix", camera.getProjection());
		buildOcclusionShader.setUniform("width", width);
		buildOcclusionShader.setUniform("height", height);

		buildOcclusionShader.setUniform("lightMvMatrix", lightCam.getInverse());
		buildOcclusionShader.setUniform("lightPMatrix", lightCam.getProjection());
		buildOcclusionShader.setUniform("lightInvMvMatrix", lightCam.getTransform());

		buildOcclusionShader.setUniform("lightPos", app->getLightPos());
		buildOcclusionShader.setUniform("lightDir", app->getLightDir());

		app->setTmpViewport(IZB_WIDTH * amt, IZB_HEIGHT * amt);
		app->getGpuMesh().render(false);
		app->restoreViewport();

		buildOcclusionShader.unbind();
		izb.endComposite();
		amt *= 2;
	}
#else

	izb.beginComposite();
	buildOcclusionShader.bind();
	izb.setUniforms(&buildOcclusionShader);
	buildOcclusionShader.setUniform("OcclusionBuffer", &occlusionBuffer);
	buildOcclusionShader.setUniform("mvMatrix", camera.getInverse());
	buildOcclusionShader.setUniform("pMatrix", camera.getProjection());
	buildOcclusionShader.setUniform("width", width);
	buildOcclusionShader.setUniform("height", height);

	buildOcclusionShader.setUniform("lightMvMatrix", lightCam.getInverse());
	buildOcclusionShader.setUniform("lightPMatrix", lightCam.getProjection());
	buildOcclusionShader.setUniform("lightInvMvMatrix", lightCam.getTransform());

	buildOcclusionShader.setUniform("lightPos", app->getLightPos());
	buildOcclusionShader.setUniform("lightDir", app->getLightDir());

	app->setTmpViewport(IZB_WIDTH, IZB_HEIGHT);
	app->getGpuMesh().render(false);
	app->restoreViewport();

	buildOcclusionShader.unbind();
	izb.endComposite();
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);

	glPopAttrib();

	buildOcclusionFbo.unbind();

	app->getProfiler().time("build occlusion");
}

void IZB::renderGeometry(App *app)
{
	(void) (app);

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);

	renderShadowShader.bind();
	gBuffer.bindTextures();

	gBuffer.setUniforms(&renderShadowShader);
	renderShadowShader.setUniform("OcclusionBuffer", &occlusionBuffer);
	renderShadowShader.setUniform("lightPos", app->getLightPos());

	renderShadowShader.setUniform("viewport", camera.getViewport());
	renderShadowShader.setUniform("mvMatrix", camera.getInverse());
	renderShadowShader.setUniform("pMatrix", camera.getProjection());
	renderShadowShader.setUniform("invPMatrix", camera.getInverseProj());
	renderShadowShader.setUniform("invMvMatrix", camera.getTransform());

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();
	renderShadowShader.unbind();

	glPopAttrib();
}

void IZB::fitLightToFrustum(App *app)
{
	// TODO: I think CUDA also has its own minmax reduction stuff. Might be worth comparing to see how fast theirs is.

	static Shader zeroShader;
	if (!zeroShader.isGenerated())
	{
		auto zeroMinmax = ShaderSourceCache::getShader("zeroMinmax").loadFromFile("shaders/zero.vert");
		zeroMinmax.replace("DATA_TYPE", "vec4");
		zeroShader.create(&zeroMinmax);
	}

	static StorageBuffer answerBuffer;
	if (!answerBuffer.isGenerated())
		answerBuffer.create(nullptr, 2 * sizeof(float) * 4);

	auto &camera = Renderer::instance().getActiveCamera();
	auto &lightCam = app->getLightCam();

	int gBufferSize = camera.getWidth() * camera.getHeight();
	int bufferSize = (int) ceil((float) gBufferSize / 32.0f);

	if (bufferSize != minmaxSize)
	{
		minBuffer.create(nullptr, bufferSize * sizeof(float) * 4);
		maxBuffer.create(nullptr, bufferSize * sizeof(float) * 4);
		minmaxSize = bufferSize;
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	}

	app->getProfiler().start("minmax reduction");

	// Zero the min and max buffers.
	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("Data", &minBuffer);
	glDrawArrays(GL_POINTS, 0, minmaxSize);
	zeroShader.setUniform("Data", &maxBuffer);
	glDrawArrays(GL_POINTS, 0, minmaxSize);
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Almost all of the time comes from this initial pass. Not sure if it's due to the memory read or write.
	// TODO: Rendering a quad for more coherent texture reads may be a better idea.

	// Now do the initial minmax reduction pass using data from the g-buffer.
	gBuffer.getDepthTexture()->bind();

	minmaxFirstShader.bind();
	minmaxFirstShader.setUniform("MinBuffer", &minBuffer);
	minmaxFirstShader.setUniform("MaxBuffer", &maxBuffer);

	minmaxFirstShader.setUniform("invPMatrix", camera.getInverseProj());
	minmaxFirstShader.setUniform("invMvMatrix", camera.getTransform());
	minmaxFirstShader.setUniform("lightMvMatrix", lightCam.getInverse());
	minmaxFirstShader.setUniform("viewport", camera.getViewport());

	minmaxFirstShader.setUniform("depthTex", gBuffer.getDepthTexture());

	minmaxFirstShader.setUniform("width", camera.getWidth());
	minmaxFirstShader.setUniform("height", camera.getHeight());

	// Each thread reads 32 pixels and writes the min/max from the group.
	glDispatchCompute(minmaxSize, 1, 1);

	gBuffer.getDepthTexture()->unbind();

	minmaxFirstShader.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

#if 1
	// TODO: Might be overkill to use the nvidia intrinsics in this way. Could just read and compare as above.
	// This step is so fast it may as well be free.
	// Keep going until there's no data left to compare.
	minmaxShader.bind();
	minmaxShader.setUniform("MinBuffer", &minBuffer);
	minmaxShader.setUniform("MaxBuffer", &maxBuffer);
	minmaxShader.setUniform("AnswerBuffer", &answerBuffer);

	int prevSize = minmaxSize;
	int newSize = (int) ceil((float) prevSize / 32.0f);

	while (prevSize > 1)
	{
		minmaxShader.setUniform("prevSize", prevSize);

		glDispatchCompute(prevSize, 1, 1);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		prevSize = newSize;
		newSize = (int) ceil((float) prevSize / 32.0f);
	}

	minmaxShader.unbind();

	//minmax is occasionally 0. What's up with that?
	Math::vec4 *minmaxAnswer = (Math::vec4*) answerBuffer.read();
	Math::vec3 minVal = minmaxAnswer[0].xyz;
	Math::vec3 maxVal = minmaxAnswer[1].xyz;
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	if (minVal != Math::vec3(0.0f, 0.0f, 0.0f))
		lightMin = minVal;
	if (maxVal != Math::vec3(0.0f, 0.0f, 0.0f))
		lightMax = maxVal;

	// Now update the light projection to fit the range of visible geometry. May want a tighter bounds than this.
	lightCam.setOrtho(lightMin.x, lightMax.x, lightMin.y, lightMax.y);
	lightCam.setDistance(0, 30);
	lightCam.update(0.1f);

	// Default galleon answer -- min: -4.79296,-3.8027,-16.6909; max: 4.50187,6.30789,-11.1568
	//std::cout << "min: " << minVal << "; max: " << maxVal << "\n";
#endif

#if 0
	// Read the data back and see if it makes sense.
	Math::vec4 *minVals = (Math::vec4*) minBuffer.read();
	Math::vec4 *maxVals = (Math::vec4*) maxBuffer.read();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	Math::vec3 minVal(1000.0f, 1000.0f, 1000.0f), maxVal(-1000.0f, -1000.0f, -1000.0f);
	for (int i = 0; i < minmaxSize; i++)
	{
		if (minVals[i] != Math::vec3(0.0f, 0.0f, 0.0f))
		{
			if (minVals[i].x < minVal.x) minVal.x = minVals[i].x;
			if (minVals[i].y < minVal.y) minVal.y = minVals[i].y;
			if (minVals[i].z < minVal.z) minVal.z = minVals[i].z;
		}

		if (maxVals[i] != Math::vec3(0.0f, 0.0f, 0.0f))
		{
			if (maxVals[i].x > maxVal.x) maxVal.x = maxVals[i].x;
			if (maxVals[i].y > maxVal.y) maxVal.y = maxVals[i].y;
			if (maxVals[i].z > maxVal.z) maxVal.z = maxVals[i].z;
		}
	}

	std::cout << "min: " << minVal << "; max: " << maxVal << "\n";
#endif

	app->getProfiler().time("minmax reduction");

#if 0
	// First, try fitting the light's camera to the view frustum.

	// Coords are in NDC.
	std::vector<Math::vec4> corners(8);
	Math::vec4 frontBotLeft(-1, -1, 1, 1);
	Math::vec4 frontTopLeft(-1,  1, 1, 1);
	Math::vec4 frontBotRight(1, -1, 1, 1);
	Math::vec4 frontTopRight(1,  1, 1, 1);

	Math::vec4 backBotLeft(-1, -1, -1, 1);
	Math::vec4 backTopLeft(-1,  1, -1, 1);
	Math::vec4 backBotRight(1, -1, -1, 1);
	Math::vec4 backTopRight(1,  1, -1, 1);

	corners[0] = frontBotLeft;
	corners[1] = frontTopLeft;
	corners[2] = frontBotRight;
	corners[3] = frontTopRight;
	corners[4] = backBotLeft;
	corners[5] = backTopLeft;
	corners[6] = backBotRight;
	corners[7] = backTopRight;

	std::vector<Math::vec4> esCorners(8);
	for (int i = 0; i < 8; i++)
	{
		esCorners[i] = camera.getInverseProj() * corners[i];
		esCorners[i].xyz /= esCorners[i].w;
		esCorners[i].w = 1.0f;
	}

	std::vector<Math::vec4> osCorners(8);
	for (int i = 0; i < 8; i++)
		osCorners[i] = camera.getTransform() * esCorners[i];

	// TODO: This gives a world-space frustum that is really large. The light need not encompass all of it.
	// TODO: How to get the intersection of this frustum with the light's view...
	// TODO: What coords do I get if I transform this using the light's modelview matrix?
	std::vector<Math::vec4> lsCorners(8);
	for (int i = 0; i < 8; i++)
		lsCorners[i] = lightCam.getInverse() * osCorners[i];

	Math::vec3 min(1000.0f, 1000.0f, 1000.0f), max(-1000.0f, -1000.0f, -1000.0f);
	for (int i = 0; i < 8; i++)
	{
		if (lsCorners[i].x < min.x) min.x = lsCorners[i].x;
		if (lsCorners[i].x > max.x) max.x = lsCorners[i].x;
		if (lsCorners[i].y < min.y) min.y = lsCorners[i].y;
		if (lsCorners[i].y > max.y) max.y = lsCorners[i].y;
		if (lsCorners[i].z < min.z) min.z = lsCorners[i].z;
		if (lsCorners[i].z > max.z) max.z = lsCorners[i].z;
	}
#endif
}

void IZB::debugRenderLight(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	// TODO: If we're cascaing, then render some boxes to show exactly which area of the scene each light covers.

	// Draw geometry on the left as normal.
	app->setTmpViewport(512, 512);
	app->drawMesh(camera, false);

	// Then draw geometry on the right from the camera.
	auto &lightCam = app->getLightCam();
	lightCam.setViewport(512, 0, 1024, 512);
	lightCam.uploadViewport();
	//lightCam.setZoom(0.75f);
	lightCam.update(0.1f);
	app->drawMesh(lightCam, false);

	lightCam.setViewport(0, 0, IZB_WIDTH, IZB_HEIGHT);
	lightCam.uploadViewport();
	lightCam.update(0.1f);

	app->restoreViewport();
}

void IZB::debugRenderCounts(App *app)
{
#if IZB_CASCADE && 0
	(void) (app);
#else
	auto &camera = Renderer::instance().getActiveCamera();
	auto &lightCam = app->getLightCam();

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);

	renderCountShader.bind();
	gBuffer.bindTextures();

	izb.beginComposite();

	izb.setUniforms(&renderCountShader);
	gBuffer.setUniforms(&renderCountShader);
	//renderCountShader.setUniform("OcclusionBuffer", &occlusionBuffer);
	renderCountShader.setUniform("lightPos", app->getLightPos());

	renderCountShader.setUniform("viewport", camera.getViewport());
	renderCountShader.setUniform("mvMatrix", camera.getInverse());
	renderCountShader.setUniform("pMatrix", camera.getProjection());
	renderCountShader.setUniform("invPMatrix", camera.getInverseProj());
	renderCountShader.setUniform("invMvMatrix", camera.getTransform());

	renderCountShader.setUniform("lightMvMatrix", lightCam.getInverse());
	renderCountShader.setUniform("lightPMatrix", lightCam.getProjection());
	renderCountShader.setUniform("width", IZB_WIDTH);
	renderCountShader.setUniform("height", IZB_HEIGHT);

	Renderer::instance().drawQuad();

	izb.endComposite();

	gBuffer.unbindTextures();
	renderCountShader.unbind();

	glPopAttrib();
#endif
}


void IZB::init(App *app)
{
	(void) (app);

#if IZB_CASCADE
	izbCascades.resize(N_CASCADES);
	for (int i = 0; i < N_CASCADES; i++)
	{
		izbCascades[i].release();
		izbCascades[i].setType(LFB::LINEARIZED, "izb");
		izbCascades[i].setDataType(LFB::VEC4);
		izbCascades[i].setMaxFrags(512);
		izbCascades[i].setProfiler(&app->getProfiler());
	}
#endif

	izb.release();
	izb.setType(LFB::LINEARIZED, "izb");
	izb.setDataType(LFB::VEC4);
	izb.setMaxFrags(512);
	izb.setProfiler(&app->getProfiler());

	auto &lightCam = app->getLightCam();
	lightCam.setViewport(0, 0, IZB_WIDTH, IZB_HEIGHT);
	lightCam.update(0.1f);

	// May want some separate fbo's to handle the different izb sizes.
	buildOcclusionFbo.setSize(IZB_WIDTH, IZB_HEIGHT);
	buildOcclusionFbo.create(&buildOcclusionDepth, true);

#if IZB_CASCADE
	int amt = 2;
	fboCascades.resize(N_CASCADES);
	depthCascades.resize(N_CASCADES);
	for (int i = 0; i < N_CASCADES; i++)
	{
		fboCascades[i] = new Rendering::FrameBuffer();
		depthCascades[i] = new Rendering::RenderBuffer();

		fboCascades[i]->setSize(IZB_WIDTH * amt, IZB_HEIGHT * amt);
		fboCascades[i]->create(depthCascades[i], true);
		amt *= 2;
	}
#endif
}

void IZB::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	gBuffer.resize(camera.getWidth(), camera.getHeight());

	// Not sure if we need to resize here or not.
#if IZB_CASCADE
	int amt = 2;
	for (int i = 0; i < N_CASCADES; i++)
	{
		izbCascades[i].resize(IZB_WIDTH * amt, IZB_HEIGHT * amt);
		amt *= 2;
	}
#endif

	izb.resize(IZB_WIDTH, IZB_HEIGHT);

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Step 1: Capture the g-buffer.
	captureGBuffer(app);

	// Step 1.5: How big should the light's view be?
	fitLightToFrustum(app);

#if DEBUG_LIGHT
	debugRenderLight(app);
	glPopAttrib();
	return;
#endif

	// Step 2: Store g-buffer pixels in light-space lfb.
#if IZB_CASCADE
	buildIZBCascades(app);
#else
	buildIZB(app);
#endif

#if DEBUG_COUNTS
	debugRenderCounts(app);
	glPopAttrib();
	return;
#endif

	// Step 3: Render geometry from light's perspective and check to see if izb data is occluded.
	buildOcclusionBuffer(app);

	// Now render g-buffer as normal, applying calculated shadow stuff.
	renderGeometry(app);

	glPopAttrib();
}

void IZB::update(float dt)
{
	(void) (dt);
}

void IZB::useLighting(App *app)
{
	app->getProfiler().clear();

	gBuffer.setProfiler(&app->getProfiler());

	reloadShaders(app);

	// Linked list or linearized?
	// We just store depths, nothing special.
}

void IZB::reloadShaders(App *app)
{
	(void) (app);

	width = 0;
	height = 0;

	// Building the IZB.
	auto countIZBVert = ShaderSourceCache::getShader("countIZBVert").loadFromFile("shaders/izb/buildIZB.vert");
	auto countIZBFrag = ShaderSourceCache::getShader("countIZBFrag").loadFromFile("shaders/izb/buildIZB.frag");
	countIZBFrag.setDefine("LFB_FRAG_TYPE", "vec4");
	countIZBFrag.setDefine("GET_COUNTS", "1");
	countIZBFrag.replace("LFB_NAME", "izb");
	countIZBShader.release();
	countIZBShader.create(&countIZBVert, &countIZBFrag);

	auto buildIZBVert = ShaderSourceCache::getShader("buildIZBVert").loadFromFile("shaders/izb/buildIZB.vert");
	auto buildIZBFrag = ShaderSourceCache::getShader("buildIZBFrag").loadFromFile("shaders/izb/buildIZB.frag");
	buildIZBFrag.setDefine("LFB_FRAG_TYPE", "vec4");
	buildIZBFrag.setDefine("GET_COUNTS", "0");
	buildIZBFrag.replace("LFB_NAME", "izb");
	buildIZBShader.release();
	buildIZBShader.create(&buildIZBVert, &buildIZBFrag);

	// Building the occlusion buffer.
	auto buildOccVert = ShaderSourceCache::getShader("buildOccVert").loadFromFile("shaders/izb/buildOcclusion.vert");
	auto buildOccGeom = ShaderSourceCache::getShader("buildOffGeom").loadFromFile("shaders/izb/buildOcclusion.geom");
	auto buildOccFrag = ShaderSourceCache::getShader("buildOccFrag").loadFromFile("shaders/izb/buildOcclusion.frag");
	buildOccFrag.setDefine("LFB_FRAG_TYPE", "vec4");
	buildOccFrag.replace("LFB_NAME", "izb");
	buildOcclusionShader.release();
	buildOcclusionShader.create(&buildOccVert, &buildOccFrag, &buildOccGeom);

	// Rendering shadowed geometry using the occlusion buffer.
	auto renderShadowVert = ShaderSourceCache::getShader("renderShadowVert").loadFromFile("shaders/izb/renderShadows.vert");
	auto renderShadowFrag = ShaderSourceCache::getShader("renderShadowFrag").loadFromFile("shaders/izb/renderShadows.frag");
	renderShadowShader.release();
	renderShadowShader.create(&renderShadowVert, &renderShadowFrag);

	// Min/max reduction of values in the g-buffer. So we can get a tighter shadow map bounds.
	auto minmaxFirstComp = ShaderSourceCache::getShader("minmaxFirstComp").loadFromFile("shaders/izb/minmax.comp");
	minmaxFirstComp.setDefine("FIRST_PASS", "1");
	minmaxFirstShader.release();
	minmaxFirstShader.createCompute(&minmaxFirstComp);

	auto minmaxComp = ShaderSourceCache::getShader("minmaxComp").loadFromFile("shaders/izb/minmax.comp");
	minmaxComp.setDefine("FIRST_PASS", "0");
	minmaxShader.release();
	minmaxShader.createCompute(&minmaxComp);

	// Rendering IZB counts per-pixel, for debugging.
	auto renderCountsVert = ShaderSourceCache::getShader("renderCountsVert").loadFromFile("shaders/izb/renderCounts.vert");
	auto renderCountsFrag = ShaderSourceCache::getShader("renderCountsFrag").loadFromFile("shaders/izb/renderCounts.frag");
	renderCountsFrag.setDefine("LFB_FRAG_TYPE", "vec4");
	renderCountsFrag.replace("LFB_NAME", "izb");
	renderCountShader.release();
	renderCountShader.create(&renderCountsVert, &renderCountsFrag);
}

void IZB::saveData(App *app)
{
	(void) (app);
	Utils::File f("data.txt");
	auto str = izb.getStr(true);
	f.write(str);
	f.close();
}

