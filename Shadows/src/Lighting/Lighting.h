#pragma once

class App;

class Lighting
{
public:
	enum Type
	{
		STANDARD,
		IZB
	};

protected:
	int width;
	int height;

public:
	Lighting() : width(0), height(0) {}
	virtual ~Lighting() {}

	virtual void init(App *app) = 0;

	virtual void render(App *app) = 0;
	virtual void update(float dt) = 0;

	virtual void useLighting(App *app) = 0;
	virtual void useScene(App *app) { (void) (app); }

	virtual void reloadShaders(App *app) = 0;
	virtual void saveData(App *app) { (void) (app); }
};

