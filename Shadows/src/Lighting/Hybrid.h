#pragma once

#if 0
#include "Lighting.h"

#include "../../../Renderer/src/LFB/Cluster.h"
#include "../../../Renderer/src/Lighting/GBuffer.h"
#include "../../../Renderer/src/Voxel/VoxelTree.h"
#include "../../../Renderer/src/Voxel/GridMask.h"

class Hybrid : public Lighting
{
private:
	Rendering::Shader captureGeometryShader;

	Rendering::Cluster lightGrid;
	Rendering::VoxelTree voxelTree;
	Rendering::GridMask voxelMask;

	// For the deferred version.
	Rendering::GBuffer gBuffer;
	bool gBufferFlag;

	// For constructing and storing the clusters.
	int width;
	int height;

	Rendering::Shader countLightGridShader;
	Rendering::Shader createLightGridShader;

	float wsMin;
	float wsMax;
	int size;

private:
	void countLightClusters(App *app);
	void createLightClusters(App *app);

	void buildLightGrid(App *app);

	void createVoxelMask(App *app);
	void renderVoxelMask(App *app);
	void createVoxelTree(App *app);
	void renderVoxelTree(App *app);

	void renderGeometry(App *app);

public:
	Hybrid(int opaqueFlag = 0);
	virtual ~Hybrid() {}

	virtual void init() override;

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};
#endif

