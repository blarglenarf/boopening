#include "Standard.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

using namespace Rendering;


void Standard::captureGeometry(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	auto &shader = gBuffer.getCaptureShader();

	// Capture data into the g-buffer.
	gBuffer.beginCapture();
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	shader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	//gBuffer.setUniforms(&shader);
	app->getGpuMesh().render();

	shader.unbind();
	gBuffer.endCapture();
}

void Standard::renderGBuffer(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	basicShader.bind();

	gBuffer.bindTextures();
	gBuffer.setUniforms(&basicShader);

	basicShader.setUniform("mainLightPos", app->getLightPos());
	basicShader.setUniform("mvMatrix", camera.getInverse());
	basicShader.setUniform("pMatrix", camera.getProjection());
	basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	basicShader.setUniform("viewport", camera.getViewport());
	basicShader.setUniform("invPMatrix", camera.getInverseProj());
	basicShader.setUniform("invMvMatrix", camera.getTransform());

	Renderer::instance().drawQuad();

	gBuffer.unbindTextures();

	basicShader.unbind();
}


void Standard::init(App *app)
{
	(void) (app);
}

void Standard::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();

	gBuffer.resize(camera.getWidth(), camera.getHeight());

	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	captureGeometry(app);
	renderGBuffer(app);

	glPopAttrib();
}

void Standard::update(float dt)
{
	(void) (dt);
}

void Standard::useLighting(App *app)
{
	reloadShaders(app);
}

void Standard::reloadShaders(App *app)
{
	(void) (app);

	auto basicVert = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/standard/basic.vert");
	auto basicFrag = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/standard/basic.frag");
	basicShader.release();
	basicShader.create(&basicVert, &basicFrag);
}

