#version 430

readonly buffer PolyData
{
	uint polyData[];
};

out PrimitiveData
{
	flat uint id;
} VertexOut;


void main()
{
	uint primID = polyData[gl_VertexID];
	//uint primID = gl_VertexID;
	VertexOut.id = primID;
}

