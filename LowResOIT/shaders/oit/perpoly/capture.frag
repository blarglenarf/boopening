#version 430

#import "lfb"

#include "../../utils.glsl"

struct FragData
{
	uint id;
	float depth;
};

// TODO: Replace the vec2 type with FragData...
LFB_DEC(perPoly, vec2);

in PolyData
{
	vec3 esFrag;
	flat vec3 esNorm;
	flat vec3 p0;
	flat vec3 p1;
	flat vec3 p2;
	flat int id;
} VertexIn;

uniform mat4 invPMatrix;
uniform ivec4 viewport;

uniform uint startIndex;

out vec4 fragColor;


// http://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
float si(vec2 p0, vec2 p1, vec2 p2)
{
	return (p0.x - p2.x) * (p1.y - p2.y) - (p1.x - p2.x) * (p0.y - p2.y);
}

bool pointInTriangle(vec2 pt, vec2 p0, vec2 p1, vec2 p2)
{
	bool b1 = si(pt, p0, p1) < 0.0;
	bool b2 = si(pt, p1, p2) < 0.0;
	bool b3 = si(pt, p2, p0) < 0.0;

	return ((b1 == b2) && (b2 == b3));
}


void main()
{
	vec4 color = vec4(0);

	float z = -VertexIn.esFrag.z;
	//float z = 0.0;

	// Make sure the depth value actually lies on the polygon and isn't from the extruded part.
	vec2 pixMin = gl_FragCoord.xy - vec2(0.5, 0.5);
	vec2 pixMax = gl_FragCoord.xy + vec2(0.5, 0.5);

	bool b0 = (VertexIn.p0.x > pixMin.x && VertexIn.p0.x < pixMax.x && VertexIn.p0.y > pixMin.y && VertexIn.p0.y < pixMax.y);
	bool b1 = (VertexIn.p1.x > pixMin.x && VertexIn.p1.x < pixMax.x && VertexIn.p1.y > pixMin.y && VertexIn.p1.y < pixMax.y);
	bool b2 = (VertexIn.p2.x > pixMin.x && VertexIn.p2.x < pixMax.x && VertexIn.p2.y > pixMin.y && VertexIn.p2.y < pixMax.y);
	bool b3 = false;
	float d = 0;

	// TODO: Double-check that all of this works correctly...
	// Case 1: Triangle is fully inside pixel, or at least one point is inside.
#if 0
	bool inside = b0 && b1 && b2;
	if (b0)
		z = max(z, -VertexIn.p0.z);
	if (b1)
		z = max(z, -VertexIn.p1.z);
	if (b2)
		z = max(z, -VertexIn.p2.z);
#endif
#if 0
	// Case 2: Corners of pixel are covered by triangle (and triangle wasn't fully inside pixel).
	if (!inside)
	{
		b0 = pointInTriangle(pixMin, VertexIn.p0.xy, VertexIn.p1.xy, VertexIn.p2.xy);
		b1 = pointInTriangle(pixMin + vec2(0, 1), VertexIn.p0.xy, VertexIn.p1.xy, VertexIn.p2.xy);
		b2 = pointInTriangle(pixMin + vec2(1, 0), VertexIn.p0.xy, VertexIn.p1.xy, VertexIn.p2.xy);
		b3 = pointInTriangle(pixMax, VertexIn.p0.xy, VertexIn.p1.xy, VertexIn.p2.xy);

		// Line-triangle intersection test at corner to give depth value.
		if (b0)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(pixMin, -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
		if (b1)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(pixMin + vec2(0, 1), -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
		if (b2)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(pixMin + vec2(1, 0), -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
		if (b3)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(pixMax, -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
	}
#endif
#if 0
	// Case 3: Triangle intersects pixel edges (and triangle wasn't fully inside pixel, and didn't cover pixel).
	if (!inside && !(b0 && b1 && b2 && b3))
	{
		// For each triangle edge, calculate two y values given x at left/right pixel bounds.
		float m0 = (VertexIn.p0.y - VertexIn.p1.y) / (VertexIn.p0.x - VertexIn.p1.x);
		float m1 = (VertexIn.p0.y - VertexIn.p2.y) / (VertexIn.p0.x - VertexIn.p2.y);
		float m2 = (VertexIn.p1.y - VertexIn.p2.y) / (VertexIn.p1.x - VertexIn.p2.x);

		float c0 = -m0 * VertexIn.p0.x + VertexIn.p0.y;
		float c1 = -m1 * VertexIn.p0.x + VertexIn.p0.y;
		float c2 = -m2 * VertexIn.p1.x + VertexIn.p0.y;

		float leftX = pixMin.x, rightX = pixMax.x;

		// We have the three line equations, now get the 6 y values (3 for left/right x).
		float leftY0 = m0 * leftX + c0, rightY0 = m0 * rightX + c0;
		float leftY1 = m1 * leftX + c1, rightY1 = m1 * rightX + c1;
		float leftY2 = m2 * leftX + c2, rightY2 = m2 * rightX + c2;

		// Determine which points lie on the pixel.
		b0 = !(leftY0 < pixMin.y || leftY0 > pixMax.y);
		b1 = !(rightY0 < pixMin.y || rightY0 > pixMax.y);
		b2 = !(leftY1 < pixMin.y || leftY1 > pixMax.y);
		b3 = !(rightY1 < pixMin.y || rightY1 > pixMax.y);
		bool b4 = !(leftY2 < pixMin.y || leftY2 > pixMax.y);
		bool b5 = !(rightY2 < pixMin.y || rightY2 > pixMax.y);

		d = 0;
		if (b0)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(leftX, leftY0, -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
		if (b1)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(rightX, rightY0, -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
		if (b2)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(leftX, leftY1, -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
		if (b3)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(rightX, rightY1, -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
		if (b4)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(leftX, leftY2, -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
		if (b5)
		{
			vec3 esRay = normalize(getEyeFromWindow(vec3(rightX, rightY2, -30.0), viewport, invPMatrix).xyz);
			planeIntersection(VertexIn.esFrag, VertexIn.esNorm, esRay, vec3(0, 0, 0), d);
			z = max(z, -((esRay * d).z));
		}
	}
#endif
	//uint id = uint(gl_PrimitiveID) + uint(startIndex);
	uint id = uint(VertexIn.id) + uint(startIndex);
	uint pixelIndex = uint(gl_FragCoord.y) * viewport.z + uint(gl_FragCoord.x);

	// Per-polygon list of pixels with depths.
	LFB_ADD_DATA(perPoly, vec2(id, 0), vec2(pixelIndex, z));

	// Now we should have the min z-value for the triangle in this pixel.
#if 0
	//uint id = uint(gl_PrimitiveID) + uint(startIndex);//VertexIn.id;
	uint id = uint(VertexIn.id) + uint(startIndex);
	uint currFrag = atomicCounterIncrement(FragListCount);
	if (currFrag < fragListAlloc)
	{
		FragData d;
		d.id = id;
		d.depth = -VertexIn.esFrag.z;
		fragListData[currFrag] = d;//vec2(VertexIn.id, -VertexIn.esFrag);
	}

	uint pixel = uint(gl_FragCoord.y) * viewport.z + uint(gl_FragCoord.x);
	atomicAdd(pixelCount[VertexIn.id], 1U);
#endif

	fragColor = color;
}

