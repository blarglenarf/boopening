#version 430

// TODO: We don't actually care about lighting or colour.
layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoord;

buffer EsPositions
{
	vec4 esPositions[];
};

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;

out VertexData
{
	vec3 esFrag;
	vec4 csVert;
} VertexOut;

void main()
{
	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

	esPositions[gl_VertexID] = esVert;

    VertexOut.esFrag = esVert.xyz;
    VertexOut.csVert = csVert;

	gl_Position = csVert;
}

