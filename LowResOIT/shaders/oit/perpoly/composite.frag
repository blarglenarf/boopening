#version 430

#extension GL_NV_gpu_shader5 : enable

// TODO: Ignore material uniform buffer and smaplers, switch to array of materials.
readonly buffer MatAmbient
{
	vec4 matAmbient[];
};

readonly buffer MatDiffuse
{
	vec4 matDiffuse[];
};

readonly buffer MatSpecular
{
	vec4 matSpecular[];
};

readonly buffer MatShininess
{
	float matShininess[];
};

readonly buffer MatTexFlag
{
	bool matTexFlag[];
};

uniform sampler2DArray diffuseTex;
uniform sampler2DArray normalTex;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
	flat uint matIndex;
} VertexIn;

uniform vec3 lightDir;

#if 0
uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;
#endif

out vec4 fragColor;


void main()
{
	vec3 normal = normalize(VertexIn.normal);
	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 lightDirN = normalize(lightDir);

	float cosTheta = clamp(dot(normal, lightDirN), 0.0, 1.0);

	//vec3 texColor = texture2D(diffuseTex, VertexIn.texCoord).xyz;
	vec3 texColor = texture2DArray(diffuseTex, vec3(VertexIn.texCoord, VertexIn.matIndex)).xyz;

	//vec3 ambient = Material.ambient.xyz * 0.2;
	vec3 ambient = matAmbient[VertexIn.matIndex].xyz * 0.2;
	//vec3 ambient = vec3(0.2, 0.2, 0.2);
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float dp = max(cosTheta, 0.0);

	//if (texFlag)
	//	ambient = texColor * 0.2;
	if (matTexFlag[VertexIn.matIndex])
		ambient = texColor * 0.2;

	if (dp > 0.0)
	{
		//if (!texFlag)
		//	diffuse = Material.diffuse.xyz * dp;
		//else
		//	diffuse = texColor * dp;
		if (!matTexFlag[VertexIn.matIndex])
			diffuse = matDiffuse[VertexIn.matIndex].xyz * dp;
		else
			diffuse = texColor * dp;
		//diffuse = vec3(0.6, 0.6, 0.6) * dp;
		vec3 reflection = normalize(reflect(-lightDirN, normal));
		float nDotH = max(dot(viewDir, reflection), 0.0);
		//float intensity = pow(nDotH, Material.shininess);
		float intensity = pow(nDotH, matShininess[VertexIn.matIndex]);
		//float intensity = pow(nDotH, 128.0);
		//specular = Material.specular.xyz * intensity;
		specular = matSpecular[VertexIn.matIndex].xyz * intensity;
		//specular = vec3(1.0) * intensity;
	}

	vec4 color = vec4(ambient + diffuse + specular, 1.0);
	if (gl_FrontFacing)
		fragColor = color;
	else
		fragColor = vec4(1, 0, 0, 1);
}

