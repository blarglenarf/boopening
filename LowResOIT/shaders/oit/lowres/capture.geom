#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in VertexData
{
	vec3 esFrag;
	vec4 csVert;
} VertexIn[3];

out PolyData
{
	vec3 esFrag;
	flat vec3 esNorm;
	flat vec3 p0;
	flat vec3 p1;
	flat vec3 p2;
	flat int id;
} VertexOut;

uniform ivec4 viewport;

void main()
{
	// Calculate screen-space triangle extents.
	vec4 cs0 = VertexIn[0].csVert;
	vec4 cs1 = VertexIn[1].csVert;
	vec4 cs2 = VertexIn[2].csVert;

	vec2 ss0 = ((vec2(cs0.xy / cs0.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);
	vec2 ss1 = ((vec2(cs1.xy / cs1.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);
	vec2 ss2 = ((vec2(cs2.xy / cs2.w) + vec2(1.0, 1.0)) / 2.0) * vec2(viewport.z, viewport.w);

	vec3 p0 = vec3(ss0.x, ss0.y, VertexIn[0].esFrag.z);
	vec3 p1 = vec3(ss1.x, ss1.y, VertexIn[1].esFrag.z);
	vec3 p2 = vec3(ss2.x, ss2.y, VertexIn[2].esFrag.z);

	for (int i = 0; i < gl_in.length(); i++)
	{
		//if (i == 2)
		gl_PrimitiveID = gl_PrimitiveIDIn;
		gl_Position = gl_in[i].gl_Position;
		VertexOut.esNorm = normalize(cross(VertexIn[0].esFrag - VertexIn[1].esFrag, VertexIn[0].esFrag - VertexIn[2].esFrag));
		VertexOut.esFrag = VertexIn[i].esFrag;
		VertexOut.id = gl_PrimitiveIDIn;
		VertexOut.p0 = p0;
		VertexOut.p1 = p1;
		VertexOut.p2 = p2;
		EmitVertex();
	}
	EndPrimitive();
}

