#version 430

struct FragData
{
	uint id;
	float depth;
};

readonly buffer FragListData
{
	FragData fragListData[];
};

coherent buffer PolyFlags
{
	uint polyFlags[];
};

out PrimitiveData
{
	flat bool exists;
	flat uint id;
} VertexOut;


void main()
{
	uint primID = fragListData[gl_VertexID].id;
	//uint primID = gl_VertexID;
	// TODO: Get rid of duplicates so that this is not needed!
	// TODO: Probably best to do that just after sorting.
	uint exists = atomicCompSwap(polyFlags[primID], 0U, 1U);

	if (exists == 0U)
	{
		VertexOut.exists = true;
		VertexOut.id = primID;
	}
	else
	{
		VertexOut.exists = false;
		VertexOut.id = 0;
	}
}

