#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform layout(binding = 0) atomic_uint TriListCount;
uniform layout(binding = 1) atomic_uint TessCount;

struct PolyKey
{
	uint id;
	float z;
};

in VertexData
{
	vec4 esVert;
	vec3 normal;
	vec2 texCoord;
	flat uint index;
	flat uint matIndex;
} VertexIn[3];

out FragData
{
	vec3 esFrag;
	vec3 normal;
} VertexOut;

// TODO: Save data in these buffers.
coherent buffer TriIndices
{
	PolyKey triIndices[];
};

coherent buffer TransPositions
{
	vec4 transPositions[];
};

coherent buffer TransNormals
{
	vec4 transNormals[];
};

coherent buffer TransTexCoords
{
	vec2 transTexCoords[];
};

coherent buffer TransMatIndices
{
	uint transMatIndices[];
};

uniform ivec4 viewport;

uniform uint triListAlloc;

void main()
{
	// Calculate screen-space triangle extents.
	vec4 cs0 = gl_in[0].gl_Position;
	vec4 cs1 = gl_in[1].gl_Position;
	vec4 cs2 = gl_in[2].gl_Position;

	vec2 ss0 = ((vec2(cs0.xy / cs0.w) + vec2(1.0, 1.0)) / 2.0) * viewport.zw + viewport.xy;
	vec2 ss1 = ((vec2(cs1.xy / cs1.w) + vec2(1.0, 1.0)) / 2.0) * viewport.zw + viewport.xy;
	vec2 ss2 = ((vec2(cs2.xy / cs2.w) + vec2(1.0, 1.0)) / 2.0) * viewport.zw + viewport.xy;

	vec2 ssMin = vec2(min(min(ss0.x, ss1.x), ss2.x), min(min(ss0.y, ss1.y), ss2.y));
	vec2 ssMax = vec2(max(max(ss0.x, ss1.x), ss2.x), max(max(ss0.y, ss1.y), ss2.y));

	float zMin = min(min(VertexIn[0].esVert.z, VertexIn[1].esVert.z), VertexIn[2].esVert.z);
	float zMax = max(max(VertexIn[0].esVert.z, VertexIn[1].esVert.z), VertexIn[2].esVert.z);

	// If the triangle is outside the viewport then ignore it (remember eye-space z is negative).
	if (ssMax.x < 0 || ssMin.x > viewport.z || ssMax.y < 0 || ssMin.y > viewport.w || zMax < -30 || zMin > 0)
		return;

	// TODO: If the triangle is sub-pixel size or would not be visible, ignore it.

	// TODO: Decide based on these screen space extents (area) if the triangle should be subdivided.
	// TODO: If subdividing here is too slow, this triangle can be marked for subdivision and subdivided in a tessellation shader.

	// Heron's formula to find triangle area, can be optimised so the sqrt is not required for length.
#if 0
	// Screen-space version.
	float a = length(ss1 - ss0);
	float b = length(ss2 - ss0);
	float c = length(ss2 - ss1);
#else
	// Eye-space version.
	float a = length(VertexIn[1].esVert.xyz - VertexIn[0].esVert.xyz);
	float b = length(VertexIn[2].esVert.xyz - VertexIn[0].esVert.xyz);
	float c = length(VertexIn[2].esVert.xyz - VertexIn[1].esVert.xyz);
#endif
	float s = (a + b + c) / 2.0;
	float area = sqrt(s * (s - a) * (s - b) * (s - c));

	// Decide if tessellation is necessary or not.
#if 0
	if (area > 0.005)
	{
		uint currTess = atomicCounterIncrement(TessCount);
		if (currTess < tessListAlloc)
		{
			tessIndices[currTess] = currTess;
			for (int i = 0; i < 3; i++)
			{
				tessPositions[currTess * 3 + i] = VertexIn[i].esVert;
				tessPositions[currTess * 3 + i] = vec4(VertexIn[i].normal, 0);
				tessTexCoords[currTess * 3 + i] = VertexIn[i].texCoord;
				tessMatIndices[currTess * 3 + i] = VertexIn[i].matIndex;
			}
		}
	}
	else
	{
#endif
		// Make sure there's enough allocated memory.
		uint currTri = atomicCounterIncrement(TriListCount);
		if (currTri < triListAlloc)
		{
			// Save the created triangle data necessary for sorting.
			triIndices[currTri].id = currTri;
			triIndices[currTri].z = -zMin; // Negated since the sort expects positive values.

			// TODO: Figure out how to save less data.
			for (int i = 0; i < 3; i++)
			{
				transPositions[currTri * 3 + i] = VertexIn[i].esVert;
				transNormals[currTri * 3 + i] = vec4(VertexIn[i].normal, 0);
				transTexCoords[currTri * 3 + i] = VertexIn[i].texCoord;
				transMatIndices[currTri * 3 + i] = VertexIn[i].matIndex;
			}

		#if 0
			// For debugging purposes, render this data to make sure everything is fine and see what the performance is like.
			gl_Position = cs0;
			VertexOut.esFrag = VertexIn[0].esVert.xyz;
			VertexOut.normal = VertexIn[0].normal;
			EmitVertex();

			gl_Position = cs1;
			VertexOut.esFrag = VertexIn[1].esVert.xyz;
			VertexOut.normal = VertexIn[1].normal;
			EmitVertex();

			gl_Position = cs2;
			VertexOut.esFrag = VertexIn[2].esVert.xyz;
			VertexOut.normal = VertexIn[2].normal;
			EmitVertex();

			EndPrimitive();
		#endif
		}
#if 0
	}
#endif
}

