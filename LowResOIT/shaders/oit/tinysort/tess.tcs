#version 430

#define TESS_AREA 0.1

layout (vertices = 3) out;

in VertexData
{
	vec3 esFrag;
	vec4 csVert;
} VertexIn[];

out VertexData
{
	vec3 esFrag;
	vec4 csVert;
} VertexOut[];

float getTessLevel(float d1, float d2)
{
	float avg = (d1 + d2) / 2.0;

	if (avg <= 2)
		return 10.0;
	else if (avg <= 5)
		return 7.0;
	else
		return 3.0;
}

void main()
{
	VertexOut[gl_InvocationID].esFrag = VertexIn[gl_InvocationID].esFrag;
	VertexOut[gl_InvocationID].csVert = VertexIn[gl_InvocationID].csVert;

	float e0 = distance(VertexIn[1].esFrag, VertexIn[2].esFrag);
	float e1 = distance(VertexIn[0].esFrag, VertexIn[2].esFrag);
	float e2 = distance(VertexIn[0].esFrag, VertexIn[1].esFrag);

	float d0 = e0 / TESS_AREA;
	float d1 = e1 / TESS_AREA;
	float d2 = e2 / TESS_AREA;

	// Heron's formula to find triangle area, can be optimised so the sqrt is not required for length.
	float a = length(VertexIn[1].esFrag - VertexIn[0].esFrag);
	float b = length(VertexIn[2].esFrag - VertexIn[0].esFrag);
	float c = length(VertexIn[2].esFrag - VertexIn[1].esFrag);
	float s = (a + b + c) / 2.0;
	float area = sqrt(s * (s - a) * (s - b) * (s - c));
	float amt = area / TESS_AREA;

	// Tessellate based on triangle size (edge length I guess).
	gl_TessLevelOuter[0] = d0;//getTessLevel(e1, e2);
	gl_TessLevelOuter[1] = d1;//getTessLevel(e2, e0);
	gl_TessLevelOuter[2] = d2;//getTessLevel(e0, e1);
	gl_TessLevelInner[0] = amt; // Inner triangle amount should be based on triangle size.
}

#if 0
#version 410 core                                                                               
                                                                                                
// define the number of CPs in the output patch                                                 
layout (vertices = 3) out;                                                                      
                                                                                                
uniform vec3 gEyeWorldPos;                                                                      
                                                                                                
// attributes of the input CPs                                                                  
in vec3 WorldPos_CS_in[];                                                                       
in vec2 TexCoord_CS_in[];                                                                       
in vec3 Normal_CS_in[];                                                                         
                                                                                                
// attributes of the output CPs                                                                 
out vec3 WorldPos_ES_in[];                                                                      
out vec2 TexCoord_ES_in[];                                                                      
out vec3 Normal_ES_in[];                                                                        
                                                                                                
void main()                                                                                     
{                                                                                               
    // Set the control points of the output patch                                               
    TexCoord_ES_in[gl_InvocationID] = TexCoord_CS_in[gl_InvocationID];                          
    Normal_ES_in[gl_InvocationID]   = Normal_CS_in[gl_InvocationID];                            
    WorldPos_ES_in[gl_InvocationID] = WorldPos_CS_in[gl_InvocationID];                          
                                                                                                
    // Calculate the distance from the camera to the three control points                       
    float EyeToVertexDistance0 = distance(gEyeWorldPos, WorldPos_ES_in[0]);                     
    float EyeToVertexDistance1 = distance(gEyeWorldPos, WorldPos_ES_in[1]);                     
    float EyeToVertexDistance2 = distance(gEyeWorldPos, WorldPos_ES_in[2]);                     
                                                                                                
    // Calculate the tessellation levels                                                        
    gl_TessLevelOuter[0] = GetTessLevel(EyeToVertexDistance1, EyeToVertexDistance2);            
    gl_TessLevelOuter[1] = GetTessLevel(EyeToVertexDistance2, EyeToVertexDistance0);            
    gl_TessLevelOuter[2] = GetTessLevel(EyeToVertexDistance0, EyeToVertexDistance1);            
    gl_TessLevelInner[0] = gl_TessLevelOuter[2];                                                
}
#endif

