#version 430

struct PolyKey
{
	uint id;
	float z;
};

readonly buffer TriIndices
{
	PolyKey polyIDs[];
};

out PrimitiveData
{
	flat uint id;
} VertexOut;


void main()
{
	VertexOut.id = polyIDs[gl_VertexID].id;
}

