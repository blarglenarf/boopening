#version 430

// TODO: Just render all this stuff normally with vertex attributes, no need for separate buffers.
readonly buffer Indices
{
	uint indices[];
};

readonly buffer Positions
{
	vec4 positions[];
};

readonly buffer Normals
{
	vec4 normals[];
};

readonly buffer TexCoords
{
	vec2 texCoords[];
};

readonly buffer MatIndices
{
	uint matIndices[];
};


out VertexData
{
	vec4 esVert;
	vec3 normal;
	vec2 texCoord;
	flat uint index;
	flat uint matIndex;
} VertexOut;


uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;


void main()
{
	uint index = indices[gl_VertexID];

	vec4 osVert = vec4(positions[index].xyz, 1);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

	vec3 normal = nMatrix * normalize(normals[index].xyz);
	vec2 texCoord = texCoords[index];
	uint matIndex = matIndices[index];

	VertexOut.esVert = esVert;
	VertexOut.normal = normalize(normal);
	VertexOut.texCoord = texCoord;
	VertexOut.index = index;
	VertexOut.matIndex = matIndex;

	gl_Position = csVert;
}

