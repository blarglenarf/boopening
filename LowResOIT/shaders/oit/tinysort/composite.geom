#version 430

layout(points) in;
layout(triangle_strip, max_vertices = 3) out;

readonly buffer Positions
{
	vec4 positions[];
};

readonly buffer Normals
{
	vec4 normals[];
};

readonly buffer TexCoords
{
	vec2 texCoords[];
};

readonly buffer MatIndices
{
	uint matIndices[];
};

in PrimitiveData
{
	flat uint id;
} VertexIn[1];

out VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
	flat uint matIndex;
} VertexOut;


uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;


void main()
{
	// Take into accout materials.
	for (uint i = 0; i < 3; i++)
	{
		uint index = VertexIn[0].id * 3 + i;

		vec4 esPos = positions[index];
		vec4 norm = normals[index];
		vec2 texCoord = texCoords[index];

		//vec4 esPos = mvMatrix * vec4(pos.xyz, 1.0);
		vec4 csPos = pMatrix * esPos;

		gl_Position = csPos;
		VertexOut.normal = normalize(norm.xyz);
		VertexOut.esFrag = esPos.xyz;
		VertexOut.texCoord = texCoord;
		VertexOut.matIndex = matIndices[index];

		EmitVertex();
	}
	EndPrimitive();
}

