#version 430

#extension GL_NV_gpu_shader5 : enable

// TODO: Ignore material uniform buffer and samplers, switch to array of materials.

in FragData
{
	vec3 esFrag;
	vec3 normal;
} VertexIn;

uniform vec3 lightDir;

out vec4 fragColor;


void main()
{
	vec3 normal = normalize(VertexIn.normal);
	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 lightDirN = normalize(lightDir);

	float cosTheta = clamp(dot(normal, lightDirN), 0.0, 1.0);

	//vec3 texColor = texture2DArray(diffuseTex, vec3(VertexIn.texCoord, VertexIn.matIndex)).xyz;

	//vec3 ambient = matAmbient[VertexIn.matIndex].xyz * 0.2;
	vec3 ambient = vec3(0.2, 0.2, 0.2);
	vec3 diffuse = vec3(0.0);
	vec3 specular = vec3(0.0);

	float dp = max(cosTheta, 0.0);

	//if (matTexFlag[VertexIn.matIndex])
	//	ambient = texColor * 0.2;

	if (dp > 0.0)
	{
		//if (!matTexFlag[VertexIn.matIndex])
		//	diffuse = matDiffuse[VertexIn.matIndex].xyz * dp;
		//else
		//	diffuse = texColor * dp;
		diffuse = vec3(0.6, 0.6, 0.6) * dp;
		vec3 reflection = normalize(reflect(-lightDirN, normal));
		float nDotH = max(dot(viewDir, reflection), 0.0);
		//float intensity = pow(nDotH, matShininess[VertexIn.matIndex]);
		float intensity = pow(nDotH, 128.0);
		//specular = matSpecular[VertexIn.matIndex].xyz * intensity;
		specular = vec3(1.0) * intensity;
	}

	vec4 color = vec4(ambient + diffuse + specular, 1.0);
	fragColor = color;
}

