#version 430

layout(points) in;
layout(triangle_strip, max_vertices = 3) out;

readonly buffer IndexData
{
	uint indexData[];
};

readonly buffer Positions
{
	vec4 positions[];
};

readonly buffer Normals
{
	vec4 normals[];
};

readonly buffer TexCoords
{
	vec2 texCoords[];
};

readonly buffer MatIndices
{
	uint matIndices[];
};

in PrimitiveData
{
	flat bool exists;
	flat uint id;
} VertexIn[1];

out VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
	flat uint matIndex;
} VertexOut;


uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix;


void main()
{
	if (VertexIn[0].exists == true)
	{
		// Take into accout materials.
		for (uint i = 0; i < 3; i++)
		{
			uint index = indexData[VertexIn[0].id * 3 + i];

			vec4 pos = positions[index];
			vec4 norm = normals[index];
			vec2 texCoord = texCoords[index];

			vec4 esPos = mvMatrix * vec4(pos.xyz, 1.0);
			vec4 csPos = pMatrix * esPos;

			gl_Position = csPos;
			VertexOut.normal = nMatrix * normalize(norm.xyz);
			VertexOut.esFrag = esPos.xyz;
			VertexOut.texCoord = texCoord;
			VertexOut.matIndex = matIndices[VertexIn[0].id];

			EmitVertex();
		}
		EndPrimitive();
	}
#if 0
	else
	{
		// TODO: Primitive has already been rendered, don't render it again.
		for (uint i = 0; i < 3; i++)
		{
			gl_Position = vec4(0, 0, 0, 0);
			VertexOut.exists = false;
			EmitVertex();
		}
		EndPrimitive();
	}
#endif
}

