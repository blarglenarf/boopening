#version 430

struct FragData
{
	uint id;
	float depth;
};

readonly buffer FragListData
{
	FragData fragListData[];
};

coherent buffer PolyFlags
{
	uint polyFlags[];
};

readonly buffer IndexData
{
	uint indexData[];
};

#if 0
readonly buffer VertexData
{
	// TODO: A bunch of vertex data goes here...
};
#endif


void main()
{
	uint primID = fragListData[gl_VertexID].id;
	uint exists = atomicCompSwap(polyFlags[primID], 0U, 1U);

	if (exists == 0U)
	{
		// TODO: This goes in the geometry shader.
		// TODO: Assemble the primitive by grabbing vertices from these locations.
		// TODO: Then transform the vertices as normal, and send the result to the fragment shader.
		uint i0 = indexData[primID * 3 + 0];
		uint i1 = indexData[primID * 3 + 1];
		uint i2 = indexData[primID * 3 + 2];
	}

	// TODO: If the primitive has already been drawn, pass a flag to the geometry shader telling it to assemble nothing.
	// TODO: Then pass a flag to the fragment shader telling it to draw nothing.
}

