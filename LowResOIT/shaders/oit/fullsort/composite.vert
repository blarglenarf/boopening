#version 430

coherent buffer PolyIDs
{
	uint polyIDs[];
};

out PrimitiveData
{
	flat bool exists;
	flat uint id;
} VertexOut;


void main()
{
	uint primID = polyIDs[gl_VertexID];

	VertexOut.exists = true;
	VertexOut.id = primID;
}

