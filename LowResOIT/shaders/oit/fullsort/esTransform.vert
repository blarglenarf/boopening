#version 430

readonly buffer Positions
{
	vec4 osPositions[];
};

buffer EsPositions
{
	vec4 esPositions[];
};

uniform mat4 mvMatrix;


void main()
{
	vec4 osPos = vec4(osPositions[gl_VertexID].xyz, 1);
	vec4 esPos = mvMatrix * osPos;
	esPositions[gl_VertexID] = esPos;
}

