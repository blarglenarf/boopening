#version 430

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

#define MAX_DEPTH 1

buffer Grid
{
	uint grid[];
};

uniform ivec2 size;

//out vec4 fragColor;


void main()
{
	//fragColor = vec4(0.0);

	ivec2 gridCoord = ivec2(gl_FragCoord.x, gl_FragCoord.y);

	// Iterate over grid cells from front to back, computing visibility as you go.
	float vis = 1.0;
	for (int z = 0; z < MAX_DEPTH; z++)
	{
		int cell = gridCoord.y * size.x * MAX_DEPTH + gridCoord.x * MAX_DEPTH + z;
		if (grid[cell] != 0)
		{
			float alpha = uintBitsToFloat(grid[cell]);
			vis *= (1.0 - alpha);
			grid[cell] = floatBitsToUint(vis);
		}
	}

	discard;
}

