#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define MAX_DEPTH 1

#import "lfb"

#include "../../utils.glsl"

#define GRID_TILE_SIZE 1

LFB_DEC(lfb, vec2);

LFB_DEC(tri, vec2);

uniform ivec2 size;


void main()
{
	float vis = 1;

	// Loop through the deep image, and alter color based on visibility value and alpha.
	ivec2 gridCoord = ivec2(gl_FragCoord.x, gl_FragCoord.y);
	int tile = size.x * gridCoord.y + gridCoord.x;

	for (LFB_ITER_BEGIN(lfb, tile); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
	{
		vec2 f = LFB_GET_DATA(lfb);
		uint val = floatBitsToUint(f.x);
		uint fragID = val >> 8U;
		vis = float(val & 0xFFU) / 255.0;

		// TODO: Write x/y coord to first value and visibility to second.
		// TODO: Could have one value per fragment, 12 bit x, 12 bit y, 8 bit visibility (try it if everything else works).
		uint pos = gridCoord.x << 16U | gridCoord.y;
		LFB_ADD_DATA(tri, uvec2(fragID, 0), vec2(uintBitsToFloat(pos), vis));
	}
}

