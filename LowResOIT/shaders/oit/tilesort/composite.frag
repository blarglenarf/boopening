#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define USE_GRID 0
#define PER_TRIANGLE 0
#define MAX_DEPTH 1

#if !USE_GRID
#import "lfb"
#endif

#include "../../utils.glsl"

#define GRID_TILE_SIZE 1
#define OPAQUE 0

#if !OPAQUE && !USE_GRID
	#if PER_TRIANGLE
		LFB_DEC(tri, vec2);
	#else
		LFB_DEC(lfb, vec2);
	#endif
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

uniform uint startIndex;
uniform ivec2 size;

#if USE_GRID
	buffer Grid
	{
		uint grid[];
	};
#endif

#include "../../light/light.glsl"


out vec4 fragColor;


void main()
{
	vec4 color = vec4(0);
	float vis = 1;

	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	color = getAmbient(0.2);
#if OPAQUE
	if (color.w == 0)
	{
		discard;
		return;
	}
	color.w = 1.0;
#endif

	color += directionLight(viewDir, normal);

#if !OPAQUE
	#if USE_GRID
		// Just throw the alpha value in the grid (if the cell is empty).
		ivec2 gridCoord = ivec2(gl_FragCoord.x / float(GRID_TILE_SIZE), gl_FragCoord.y / float(GRID_TILE_SIZE));
		int z = max(0, min(MAX_DEPTH - 1, int(-VertexIn.esFrag.z / (30.0 / float(MAX_DEPTH - 1.0)))));
		int cell = gridCoord.y * size.x * MAX_DEPTH + gridCoord.x * MAX_DEPTH + z;
		vis = uintBitsToFloat(grid[cell]);
		fragColor.rgb = color.rgb * vis * 0.5;
	#else
		// Loop through the deep image, and alter color based on visibility value and alpha.
		ivec2 gridCoord = ivec2(gl_FragCoord.x / float(GRID_TILE_SIZE), gl_FragCoord.y / float(GRID_TILE_SIZE));
		int tile = size.x * gridCoord.y + gridCoord.x;
		uint id = startIndex / 3 + uint(gl_PrimitiveID);

		// TODO: Find a faster way of doing this.
		#if PER_TRIANGLE
			float dist = 10000.0;
			for (LFB_ITER_BEGIN(tri, id); LFB_ITER_CHECK(tri); LFB_ITER_INC(tri))
			{
				vec2 f = LFB_GET_DATA(tri);
				uint val = floatBitsToUint(f.x);
				uint x = val >> 16U;
				uint y = val & 0xFFFU;
				float newDist = distance(ivec2(x, y), gridCoord);
				if (newDist < dist)
				{
					vis = f.y;
					dist = newDist;
				}

				// TODO: Always keep the vis value for the fragment closest to gridCoord.
				if (x == gridCoord.x && y == gridCoord.y)
				{
					fragColor.rgb = color.rgb * vis * 0.5;
					return;
				}
			}
		#else
			for (LFB_ITER_BEGIN(lfb, tile); LFB_ITER_CHECK(lfb); LFB_ITER_INC(lfb))
			{
				vec2 f = LFB_GET_DATA(lfb);
				uint val = floatBitsToUint(f.x);
				uint fragID = val >> 8U;
				vis = float(val & 0xFFU) / 255.0;

				//fragColor.rgb = color.rgb * vis * 0.5;

				if (fragID == id)
				{
					fragColor.rgb = color.rgb * vis * 0.5;
					return;
				}
				if (f.y > -VertexIn.esFrag.z)
				{
					// We're now looking at visibility behind of the fragment's position.
					// TODO: Something to account for this.
					fragColor.rgb = color.rgb * vis * 0.5;
					//fragColor = vec4(0);
					return;
				}
			}
		#endif

		// TODO: If we made it here, it means we didn't find anything :(
		fragColor.rgb = color.rgb * vis * 0.5;
		//fragColor = vec4(0);
	#endif
#else
	fragColor = color;
#endif
}

