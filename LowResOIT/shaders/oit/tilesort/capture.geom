// In case I want to try conservative rasterization... why I do.

#version 430

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;
uniform mat3 nMatrix; // Don't use this, we need the normal in world space.

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn[3];

out FragData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
	flat vec3 aabbMin;
	flat vec3 aabbMax;
} VertexOut;


void updateAABB(vec3 val, inout vec3 aabbMin, inout vec3 aabbMax)
{
	aabbMin.x = min(aabbMin.x, val.x);
	aabbMin.y = min(aabbMin.y, val.y);
	aabbMin.z = min(aabbMin.z, val.z);
	aabbMax.x = max(aabbMax.x, val.x);
	aabbMax.y = max(aabbMax.y, val.y);
	aabbMax.z = max(aabbMax.z, val.z);
}


void main()
{
	// FIXME: I think this still has to be done in world space, but not 100% sure...
	vec3 aabbMin = VertexIn[0].esFrag;
	vec3 aabbMax = VertexIn[0].esFrag;
	for (int i = 0; i < gl_in.length(); i++)
		updateAABB(VertexIn[i].esFrag, aabbMin, aabbMax);

	for (int i = 0; i < gl_in.length(); i++)
	{
		VertexOut.esFrag = VertexIn[i].esFrag;
		VertexOut.normal = VertexIn[i].normal;
		VertexOut.texCoord = VertexIn[i].texCoord;
		VertexOut.aabbMin = aabbMin;
		VertexOut.aabbMax = aabbMax;

		gl_Position = gl_Position[i];
		EmitVertex();
	}
	EndPrimitive();
}

