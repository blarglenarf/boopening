#version 430

// Added this for polygon sorted OIT, not sure if this will break other stuff or not...
layout(early_fragment_tests) in;

#define GRID_TILE_SIZE 1
#define MAX_DEPTH 1
#define USE_GRID 0

#if !USE_GRID
#import "lfb"
#endif

#include "../../utils.glsl"

#define OPAQUE 0

#if !OPAQUE && !USE_GRID
	LFB_DEC(lfb, vec2);
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;

uniform uint startIndex;

#if USE_GRID
	coherent buffer Grid
	{
		uint grid[];
	};

	uniform ivec2 size;
#endif

#include "../../light/light.glsl"

#if OPAQUE
	out vec4 fragColor;
#endif


void main()
{
	vec4 color = vec4(0);

	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	color = getAmbient(0.2);
#if OPAQUE
	if (color.w == 0)
	{
		discard;
		return;
	}
	color.w = 1.0;
#endif

	color += directionLight(viewDir, normal);
	//color.w = 0.99;

#if !OPAQUE
	#if USE_GRID
		// Need an average of alpha values (this is actually not correct for getting proper averages...)
		// FIXME: To get a proper running average, you need a count, which you divide by after accumulating.
		float alpha = color.w;
		ivec2 gridCoord = ivec2(gl_FragCoord.x / float(GRID_TILE_SIZE), gl_FragCoord.y / float(GRID_TILE_SIZE));
		int z = max(0, min(MAX_DEPTH - 1, int(-VertexIn.esFrag.z / (30.0 / float(MAX_DEPTH - 1.0)))));
		int cell = gridCoord.y * size.x * MAX_DEPTH + gridCoord.x * MAX_DEPTH + z;
		for (int canary = 0; canary < 64; canary++)
		{
			uint currVal = grid[cell];
			if (currVal == 0)
			{
				uint oldVal = atomicCompSwap(grid[cell], 0U, floatBitsToUint(alpha));
				if (oldVal == 0)
					break;
				currVal = grid[cell];
			}
			float avg = (uintBitsToFloat(currVal) + alpha) / 2.0;
			uint oldVal = atomicCompSwap(grid[cell], currVal, floatBitsToUint(avg));
			if (oldVal == currVal)
				break;
		}
		discard;
	#else
		// Pack triangle id into first 24 bits, alpha into last 8 bits.
		uint id = startIndex / 3 + uint(gl_PrimitiveID);
		uint alpha = uint(clamp(color.w * 255.0, 0, 255));
		uint val = (id << 8U) | alpha;
		int pixel = LFB_GET_SIZE(lfb).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
		LFB_ADD_DATA(lfb, pixel, vec2(uintBitsToFloat(val), -VertexIn.esFrag.z));
		discard;
	#endif
#else
	fragColor = color;
#endif
}

