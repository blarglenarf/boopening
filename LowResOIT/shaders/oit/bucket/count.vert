#version 430

layout(location = 0) in vec3 vertex;

uniform mat4 mvMatrix;
uniform mat4 pMatrix;

out VertexData
{
	vec3 esFrag;
} VertexOut;

void main()
{
	vec4 osVert = vec4(vertex, 1.0);
	vec4 esVert = mvMatrix * osVert;
	vec4 csVert = pMatrix * esVert;

    VertexOut.esFrag = esVert.xyz;

	gl_Position = csVert;
}

