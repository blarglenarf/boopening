#version 430

#define MAX_EYE_Z 30.0
#define CLUSTERS 32

#import "cluster"

#include "../../utils.glsl"

#define OPAQUE 0

#if !OPAQUE
	CLUSTER_DEC(geom, vec2);
#endif

uniform MaterialBlock
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
} Material;

in VertexData
{
	vec3 normal;
	vec3 esFrag;
	vec2 texCoord;
} VertexIn;

coherent buffer ClusterCounts
{
	uint clusterCounts[];
};

uniform mat4 mvMatrix;

uniform bool texFlag;
uniform sampler2D diffuseTex;
uniform sampler2D normalTex;


#include "../../light/light.glsl"

out vec4 fragColor;


void main()
{
	vec4 color = vec4(0);

	vec3 viewDir = normalize(-VertexIn.esFrag);
	vec3 normal = normalize(VertexIn.normal);

	color = getAmbient(0.2);
	#if OPAQUE
		if (color.w == 0)
		{
			discard;
			return;
		}
		color.w = 1.0;
	#endif

	color += directionLight(viewDir, normal);

	#if !OPAQUE
		int cluster = CLUSTER_GET(-VertexIn.esFrag.z, MAX_EYE_Z);
		int clusterIndex = CLUSTER_GET_INDEX(geom, gl_FragCoord.xy, cluster);
		CLUSTER_ADD_DATA(geom, vec2(rgba8ToFloat(color), -VertexIn.esFrag.z), clusterIndex);
		atomicAdd(clusterCounts[clusterIndex], 1);
	#endif

	fragColor = color;
}

