#version 430

#define MAX_EYE_Z 30.0
#define CLUSTERS 32

buffer ClusterCounts
{
	uint clusterCounts[];
};

buffer PixelCounts
{
	uint pixelCounts[];
};

uniform ivec2 size;

out vec4 fragColor;


void main()
{
	int pixelIndex = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	uint pixelCount = 0;

	for (int cluster = 0; cluster < CLUSTERS; cluster++)
	{
		int clusterIndex = int(gl_FragCoord.y) * size.x * CLUSTERS + int(gl_FragCoord.x) * CLUSTERS + cluster;
		uint clusterCount = clusterCounts[clusterIndex];
		if (pixelCount < clusterCount)
			pixelCount = clusterCount;
	}

	pixelCounts[pixelIndex] = pixelCount;

	fragColor = vec4(0.0);
}

