#version 430

#define MAX_EYE_Z 30.0
#define CLUSTERS 32

buffer Offsets
{
	uint offsets[];
};

buffer PixelCounts
{
	uint pixelCounts[];
};

in VertexData
{
	vec3 esFrag;
} VertexIn;

uniform ivec2 size;

out vec4 fragColor;


void main()
{
	int cluster = min(CLUSTERS - 1, int(-VertexIn.esFrag.z / (MAX_EYE_Z / float(CLUSTERS - 1.0))));
	int index = int(gl_FragCoord.y) * size.x * CLUSTERS + int(gl_FragCoord.x) * CLUSTERS + cluster;

	atomicAdd(offsets[index], 1);

	fragColor = vec4(0.0);
}

