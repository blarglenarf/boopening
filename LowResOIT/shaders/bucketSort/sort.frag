#version 430

// Needed to cull before writing to global memory.
layout(early_fragment_tests) in;

#define CLUSTERS 32
#define MAX_EYE_Z 30.0

out vec4 fragColor;

#import "cluster"

#include "../utils.glsl"

#define CLUSTER_FRAG_TYPE vec2
#define CLUSTER_FRAG_DEPTH(frag) frag.y

// To use different deep images, call replace(CLUSTER_NAME, ...).
CLUSTER_DEC(CLUSTER_NAME, CLUSTER_FRAG_TYPE);

#define COMPOSITE 0
#define COMPOSITE_LOCAL 0

#define MAX_FRAGS 0
#define MAX_FRAGS_OVERRIDE 0

#if MAX_FRAGS_OVERRIDE != 0
	#define _MAX_FRAGS MAX_FRAGS_OVERRIDE
#else
	#define _MAX_FRAGS MAX_FRAGS
#endif

CLUSTER_FRAG_TYPE frags[_MAX_FRAGS];

#define CLUSTER_GET_FRAG(CLUSTER_NAME) CLUSTER_GET_DATA(CLUSTER_NAME)
#define CLUSTER_PUT_FRAG(CLUSTER_NAME, frag) CLUSTER_WRITE_DATA(CLUSTER_NAME, frag)


#include "sort.glsl"


void main()
{
	fragColor = vec4(0.0);

	int pixel = CLUSTER_GET_SIZE(CLUSTER_NAME).x * int(gl_FragCoord.y) + int(gl_FragCoord.x);

	// Need to iterate through the clusters one at a time (if cluster is empty, skip it).
	for (int cluster = CLUSTERS - 1; cluster >= 0; cluster--)
	{
		int clusterIndex = CLUSTER_GET_INDEX(CLUSTER_NAME, gl_FragCoord.xy, cluster);

		// Load into local memory and sort. This will also write to global memory.
		int fragCount = sort(clusterIndex);

		// Iterate uses cluster index, not pixel.
	#if COMPOSITE
		// Read from global memory and composite.
		for (CLUSTER_ITER_BEGIN(CLUSTER_NAME, clusterIndex); CLUSTER_ITER_CHECK(CLUSTER_NAME); CLUSTER_ITER_INC(CLUSTER_NAME))
		{
			CLUSTER_FRAG_TYPE f = CLUSTER_GET_FRAG(CLUSTER_NAME);
			vec4 col = floatToRGBA8(f.x);
			fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		}
	#endif

	#if COMPOSITE_LOCAL
		#if _MAX_FRAGS <= 32 // Dirty hack to account for rbs not sorting in place.
		for (int i = fragCount - 1; i >= 0; i--)
		{
			CLUSTER_FRAG_TYPE f = frags[i];
			vec4 col = floatToRGBA8(f.x);
			fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);
		}
		#endif
	#endif
	}
}

