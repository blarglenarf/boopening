#version 430

#include "../utils.glsl"

buffer PixelCounts
{
	uint pixelCounts[];
};

uniform ivec2 size;
uniform int interval;

#define DEBUG 0

#if DEBUG
	out vec4 fragColor;
#endif

void main()
{
	int pixel = size.x * int(gl_FragCoord.y) + int(gl_FragCoord.x);
	int fragCount = int(pixelCounts[pixel]);

#if DEBUG
	if (fragCount == 0)
		fragColor = vec4(0.5, 0.0, 0.0, 1.0);
	else if (fragCount <= interval)
		fragColor = vec4(debugColLog(fragCount), 1.0);
	else
		fragColor = vec4(debugColLog(fragCount), 1.0);
#else
	if (fragCount <= interval)
		discard;
#endif
}

