#pragma once
#if 0
#include <vector>

struct PolyKey
{
	float id;
	float z;

	//PolyKey() : id(0), z(0) {}
};

// Note: the opengl buffer has to be bound before calling this function.
void createPerPolyKeysCudaResource(unsigned int polyKeysBuffer, unsigned int headPtrsBuffer, unsigned int nextPtrsBuffer, unsigned int dataBuffer);
void destroyPerPolyKeysCudaResource();

// Note: the opengl buffer has to be bound before calling this function.
void sortPerPolyKeysThrust(size_t nPolyKeys);
#endif

