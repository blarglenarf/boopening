#pragma once

#include "Oit.h"

class FullSort : public Oit
{
private:
	Rendering::Shader esTransformShader;
	Rendering::Shader compositeShader;
	Rendering::Shader zeroShader;

#if 0
	// Array of voxel positions.
	unsigned int fragListAlloc;
	unsigned int totalFrags;
	Rendering::AtomicBuffer fragListCount;
	Rendering::StorageBuffer fragListData;
#endif
	unsigned int nPolys;
	unsigned int nVerts;
	Rendering::StorageBuffer polyBuffer;
	Rendering::StorageBuffer esPositionBuffer;

	Rendering::StorageBuffer indexData;
	Rendering::StorageBuffer positionBuffer;
	Rendering::StorageBuffer normalBuffer;
	Rendering::StorageBuffer texCoordBuffer;

	Rendering::StorageBuffer matIndexBuffer;
	Rendering::StorageBuffer ambientBuffer;
	Rendering::StorageBuffer diffuseBuffer;
	Rendering::StorageBuffer specularBuffer;
	Rendering::StorageBuffer shininessBuffer;
	Rendering::StorageBuffer texFlagBuffer;
	Rendering::Texture2DArray diffuseTextures;

	//Rendering::StorageBuffer pixelCountBuffer;

	int width;
	int height;

private:
	void buildMaterialBuffers(App *app);
	void buildVertexBuffers(App *app);

	void resize(App *app, int width, int height);
	void beginCapture(App *app);
	bool endCapture(App *app);

	void capture(App *app);
	void sort(App *app);

	void drawStencil();
	void drawGrid();

public:
	FullSort(int opaqueFlag = 0) : Oit(opaqueFlag), nPolys(0), nVerts(0), width(0), height(0) {}
	virtual ~FullSort();

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

