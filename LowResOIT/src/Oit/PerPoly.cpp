#include "PerPoly.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Renderer/src/LFB/LFBList.h"
#include "../../../Utils/src/File.h"

#include "PerPolySort.h"

#include <string>
#include <stdlib.h>

#define FRAG_OVERALLOCATE 1.2
#define FRAG_UNDERALLOCATE 0.5

#define CPU_SORT 1

using namespace Rendering;
using namespace Math;


static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer PolyFlags\
	{\
		uint polyFlags[];\
	};\
	\
	void main()\
	{\
		polyFlags[gl_VertexID] = 0;\
	}\
	";
}

#if CPU_SORT
static float sign(vec3 p1, vec3 p2, vec3 p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

static bool pointInTriangle(vec3 pt, vec3 v1, vec3 v2, vec3 v3)
{
	bool b1 = sign(pt, v1, v2) < 0.0f;
	bool b2 = sign(pt, v2, v3) < 0.0f;
	bool b3 = sign(pt, v3, v1) < 0.0f;

	return ((b1 == b2) && (b2 == b3));
}

static vec3 triangleRayIntersection(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 end)
{
	// If we don't normalise this, then we know t values > 1 will be past the end point.
	vec3 d = (end - o);

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	vec3 h = d.cross(e2);
	float a = e1.dot(h);

	if (a > -0.00001 && a < 0.0001)
	{
		std::cout << "1: This should never happen!\n";
		return vec3(0, 0, 0);
	}

	float f = 1.0f / a;
	vec3 s = o - v0;
	float u = f * s.dot(h);

	if (u < 0.0 || u > 1.0)
	{
		std::cout << "2: This should never happen!\n";
		return vec3(0, 0, 0);
	}

	vec3 q = s.cross(e1);
	float v = f * d.dot(q);

	if (v < 0.0 || u + v > 1.0)
	{
		std::cout << "3: This should never happen!\n";
		return vec3(0, 0, 0);
	}

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * e2.dot(q);
	vec3 pos = o + (d * t);
	return pos;
}
#endif


void PerPoly::buildMaterialBuffers(App *app)
{
	auto &mesh = app->getMesh();

	// Create buffers of material buffers with appropriate properties.
	auto &materials = mesh.getMaterials();
	std::map<Material*, unsigned int> matMap;
	for (size_t i = 0; i < materials.size(); i++)
		matMap[materials[i]] = i;

	ambientBuffer.release();
	diffuseBuffer.release();
	specularBuffer.release();
	shininessBuffer.release();
	texFlagBuffer.release();

	std::vector<vec4> matAmbient(materials.size());
	std::vector<vec4> matDiffuse(materials.size());
	std::vector<vec4> matSpecular(materials.size());
	std::vector<float> matShininess(materials.size());
	//std::vector<bool> matTexFlag(materials.size()); // Apparently the compiler doesn't like me using a vector of bools.
	bool *matTexFlag = new bool[materials.size()];

	for (size_t i = 0; i < materials.size(); i++)
	{
		matAmbient[i] = materials[i]->getAmbientColor();
		matDiffuse[i] = materials[i]->getDiffuseColor();
		matSpecular[i] = materials[i]->getSpecularColor();
		matShininess[i] = materials[i]->getShininess();
		//matTexFlag[i] = (materials[i]->getDiffuseImg() != nullptr);
		matTexFlag[i] = false;
	}

	ambientBuffer.create(&matAmbient[0], materials.size() * sizeof(vec4));
	diffuseBuffer.create(&matDiffuse[0], materials.size() * sizeof(vec4));
	specularBuffer.create(&matSpecular[0], materials.size() * sizeof(vec4));
	shininessBuffer.create(&matShininess[0], materials.size() * sizeof(float));
	texFlagBuffer.create(&matTexFlag[0], materials.size() * sizeof(bool));

	// TODO: Create a 2D texture array with a texture for each material.
	// TODO: Need a map with different texture array formats, which needs to be accessed by the GPU.
#if 0
	diffuseTextures.release();
	diffuseTextures.create(GL_RGB, 1024, 1024, materials.size());
	diffuseTextures.bind();
	for (size_t i = 0; i < materials.size(); i++)
	{
		if (materials[i]->getDiffuseImg() != nullptr)
			diffuseTextures.bufferData(materials[i]->getDiffuseImg()->getData(), i);
	}
	diffuseTextures.unbind();
#endif
	//diffuseTextures.create(GL_RGBA, size_t width, size_t height, size_t layers);

	// Each polygon needs a material id assigned to it.
	auto &faceSets = mesh.getFacesets();
	std::vector<unsigned int> matIndices(mesh.getNumIndices() / 3, 0);

	for (size_t i = 0; i < faceSets.size(); i++)
	{
		auto start = faceSets[i].start;
		auto end = faceSets[i].end;
		unsigned int id = 0;
		if (faceSets[i].mat != nullptr)
			id = matMap[faceSets[i].mat];

		for (unsigned int j = start / 3; j < end / 3; j++)
			matIndices[j] = id;
	}

	matIndexBuffer.release();
	matIndexBuffer.create(&matIndices[0], matIndices.size() * sizeof(unsigned int));

	delete [] matTexFlag;
}

void PerPoly::buildVertexBuffers(App *app)
{
	auto &mesh = app->getMesh();

	buildMaterialBuffers(app);

	indexData.release();

	auto nIndices = mesh.getNumIndices();
	auto *indices = mesh.getIndices();
	indexData.create(&indices[0], nIndices * sizeof(unsigned int));

	// Vertex Data needs to be in an appropriate format.
	positionBuffer.release();
	normalBuffer.release();
	texCoordBuffer.release();

	auto nVertices = mesh.getNumVertices();
	auto *vertices = mesh.getVertices();

	std::vector<vec4> positions(nVertices);
	std::vector<vec4> normals(nVertices);
	std::vector<vec4> texCoords(nVertices);

	for (size_t i = 0; i < nVertices; i++)
	{
		auto &vertex = vertices[i];
		positions[i] = vec4(vertex.pos.x, vertex.pos.y, vertex.pos.z, 1.0f);
		normals[i] = vec4(vertex.norm.x, vertex.norm.y, vertex.norm.z, 0.0f);
		texCoords[i] = vec2(vertex.tex.x, vertex.tex.y);
	}

	positionBuffer.create(&positions[0], positions.size() * sizeof(vec4));
	normalBuffer.create(&normals[0], normals.size() * sizeof(vec4));
	texCoordBuffer.create(&texCoords[0], texCoords.size() * sizeof(vec2));

	esPositionBuffer.create(nullptr, positions.size() * sizeof(vec4));
}

void PerPoly::capture(App *app)
{
	app->getProfiler().start("Capture");

	auto &camera = Renderer::instance().getActiveCamera();
	
	width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	app->setTmpViewport(width, height);

	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);

	polyLFB.beginCapture();
	captureShader.bind();
	captureShader.setUniform("EsPositions", &esPositionBuffer);
	polyLFB.setUniforms(&captureShader);
	app->setCameraUniforms(&captureShader);
	app->getGpuMesh().getVao().render();
	captureShader.unbind();
	if (polyLFB.endCapture())
	{
		std::cout << "resizing\n";
		polyLFB.beginCapture();
		captureShader.bind();
		captureShader.setUniform("EsPositions", &esPositionBuffer);
		polyLFB.setUniforms(&captureShader);
		app->setCameraUniforms(&captureShader);
		app->getGpuMesh().getVao().render();
		captureShader.unbind();
		if (polyLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	//app->captureGeometryLFB();
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	app->restoreViewport();

	glPopAttrib();

	app->getProfiler().time("Capture");
}

#if CPU_SORT
struct PolyKey
{
	float id;
	float z;
};

static vec3 o(0, 0, 0);
static vec4 *esPositions = nullptr;
static unsigned int *indices = nullptr;

static unsigned int *headPtrs = nullptr;
static unsigned int *nextPtrs = nullptr;
static PolyKey *data = nullptr;

static int cmpfnc(const void *a, const void *b)
{
	// This will need to check all the fragments for both polygons to determine the order.
	unsigned int ua = *((unsigned int*) a);
	unsigned int ub = *((unsigned int*) b);
	float zA = ua;
	float zB = ub;

	//return zA < zB ? -1 : 1;

	unsigned int nA = headPtrs[ua];
	unsigned int nB = headPtrs[ub];

	if (nA == 0 && nB == 0)
		return 0;
	else if (nA == 0)
		return -1;
	else if (nB == 0)
		return 1;

	//return data[nA].z < data[nB].z ? 1 : -1;

	// TODO: If both lists are sorted, we can step through simultaneously.
	unsigned int nodeA = headPtrs[ua];
	while (nodeA != 0)
	{
		PolyKey &dataA = data[nodeA];
		zA = dataA.z;

		vec3 av[3] = { esPositions[indices[ua * 3 + 0]].xyz, esPositions[indices[ua * 3 + 1]].xyz, esPositions[indices[ua * 3 + 2]].xyz };
		vec3 aMin(std::min(std::min(av[0].x, av[1].x), av[2].x), std::min(std::min(av[0].y, av[1].y), av[2].y), std::min(std::min(av[0].z, av[1].z), av[2].z));
		vec3 aMax(std::max(std::max(av[0].x, av[1].x), av[2].x), std::max(std::max(av[0].y, av[1].y), av[2].y), std::max(std::max(av[0].z, av[1].z), av[2].z));

		unsigned int nodeB = headPtrs[ub];
		while (nodeB != 0)
		{
			PolyKey &dataB = data[nodeB];

			// Instead of just comparing depth values, raycast through both triangles to determine which is in front.
			vec3 bv[3] = { esPositions[indices[ub * 3 + 0]].xyz, esPositions[indices[ub * 3 + 1]].xyz, esPositions[indices[ub * 3 + 2]].xyz };
			vec3 bMin(std::min(std::min(bv[0].x, bv[1].x), bv[2].x), std::min(std::min(bv[0].y, bv[1].y), bv[2].y), std::min(std::min(bv[0].z, bv[1].z), bv[2].z));
			vec3 bMax(std::max(std::max(bv[0].x, bv[1].x), bv[2].x), std::max(std::max(bv[0].y, bv[1].y), bv[2].y), std::max(std::max(bv[0].z, bv[1].z), bv[2].z));

			zB = dataB.z;
			bool intersect = false;
			if (dataA.id == dataB.id)
				intersect = true;

			// TODO: If there was an intersection, we need to do some checking.
			if (intersect)
			{
				// Check for overlap of the bounding boxes.
				if (aMax.x < bMin.x || aMax.y < bMin.y || aMax.z < bMin.z || bMax.x < aMin.x || bMax.y < aMin.y || bMax.z < aMin.z)
					return aMin.z < bMin.z ? -1 : 1; // Furthest extent of 'a' is further away than furthest extent of 'b'.
			}

			// 1. Any vertex from either triangle is inside the other triangle (raycast for depths of each triangle).
		#if 1
			for (int i = 0; i < 3; i++)
			{
				if (pointInTriangle(av[i], bv[0], bv[1], bv[2]))
				{
					vec3 bPos = triangleRayIntersection(bv[0], bv[1], bv[2], o, av[i]);
					return av[i].z < bPos.z ? 1 : -1;
				}
				if (pointInTriangle(bv[i], av[0], av[2], av[2]))
				{
					vec3 aPos = triangleRayIntersection(av[0], av[1], av[2], o, bv[i]);
					return aPos.z < bv[i].z ? 1 : -1;
				}
			}
		#endif

			nodeB = nextPtrs[nodeB];
			//std::cout << "b: " << nodeB << "\n";
		}
		//std::cout << "a: " << nodeA << "\n";

		nodeA = nextPtrs[nodeA];
	}

	return zA < zB ? 1 : -1;
}
#endif

void PerPoly::sort(App *app)
{
	static bool initFlag = false;

	app->getProfiler().start("Sort");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	LFBList *list = (LFBList*) polyLFB.getBase();

	// Testing this on the CPU to make sure I'm not going crazy.
#if CPU_SORT
	(void) (initFlag);

	unsigned int *polyData = (unsigned int*) polySortBuffer.read();
	headPtrs = (unsigned int*) list->getHeadPtrs()->read();
	nextPtrs = (unsigned int*) list->getNextPtrs()->read();
	data = (PolyKey*) list->getData()->read();

	esPositions = (vec4*) esPositionBuffer.read();
	indices = (unsigned int*) indexData.read();

	//std::cout << "sorting!\n";
	qsort(&polyData[0], nPolys, sizeof(unsigned int), cmpfnc);
	//std::cout << "sorted!\n";

	polySortBuffer.bufferData(&polyData[0], nPolys * sizeof(unsigned int));
#else
	if (!initFlag)
	{
		destroyPerPolyKeysCudaResource();
		polySortBuffer.bind();
		list->getHeadPtrs()->bind();
		list->getNextPtrs()->bind();
		list->getData()->bind();
		createPerPolyKeysCudaResource(polySortBuffer.getObject(), list->getHeadPtrs()->getObject(), list->getNextPtrs()->getObject(), list->getData()->getObject());
		sortPerPolyKeysThrust(nPolys);
		polySortBuffer.unbind();
		list->getHeadPtrs()->unbind();
		list->getNextPtrs()->unbind();
		list->getData()->unbind();

		initFlag = true;
	}
	else
	{
		polySortBuffer.bind();
		list->getHeadPtrs()->bind();
		list->getNextPtrs()->bind();
		list->getData()->bind();
		sortPerPolyKeysThrust(nPolys);
		polySortBuffer.unbind();
		list->getHeadPtrs()->unbind();
		list->getNextPtrs()->unbind();
		list->getData()->unbind();
	}
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().time("Sort");
}

void PerPoly::drawGrid()
{
	glPushAttrib(GL_TRANSFORM_BIT | GL_POLYGON_BIT | GL_ENABLE_BIT | GL_CURRENT_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glColor3f(1, 0, 0);

	float xStep = 2.0f / (float) width;
	float yStep = 2.0f / (float) height;

	glBegin(GL_LINES);

	for (float x = -1.0f; x <= 1.0f; x += xStep)
	{
		glVertex3f(x, -1, 0);
		glVertex3f(x, 1, 0);
	}
	for (float y = -1.0f; y <= 1.0f; y += yStep)
	{
		glVertex3f(-1, y, 0);
		glVertex3f(1, y, 0);
	}

	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
}


PerPoly::~PerPoly()
{
#if !CPU_SORT
	destroyPerPolyKeysCudaResource();
#endif
}

void PerPoly::render(App *app)
{
	//glEnable(GL_CULL_FACE);
	//glFrontFace(GL_CW);
	capture(app);
	sort(app);

	// TODO: Instead of trying to pull the data in the right order, we can just populate an index buffer based on the polygon ids.

	// Now render geometry in sorted polygon order.
	app->getProfiler().start("Render");

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// TODO: Bind the 2d texture array.
	// FIXME: This is slooooooooow.
	//diffuseTextures.bind();

	compositeShader.bind();
	// TODO: Send the material data.
	//compositeShader.setUniform("diffuseTex", &diffuseTextures);
	compositeShader.setUniform("MatIndices", &matIndexBuffer);
	compositeShader.setUniform("MatAmbient", &ambientBuffer);
	compositeShader.setUniform("MatDiffuse", &diffuseBuffer);
	compositeShader.setUniform("MatSpecular", &specularBuffer);
	compositeShader.setUniform("MatShininess", &shininessBuffer);
	compositeShader.setUniform("MatTexFlag", &texFlagBuffer);
	// Send all the other vertex data.
	//compositeShader.setUniform("FragListData", &newBuffer);
	//compositeShader.setUniform("FragListData", &fragListData);
	//compositeShader.setUniform("PolyFlags", &polyFlagBuffer);
	compositeShader.setUniform("PolyData", &polySortBuffer);
	compositeShader.setUniform("IndexData", &indexData);
	compositeShader.setUniform("Positions", &positionBuffer);
	compositeShader.setUniform("Normals", &normalBuffer);
	compositeShader.setUniform("TexCoords", &texCoordBuffer);
	// Finally send the camera data.
	compositeShader.setUniform("mvMatrix", camera.getInverse());
	compositeShader.setUniform("pMatrix", camera.getProjection());
	compositeShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	compositeShader.setUniform("lightDir", vec3(0, 0, 1));
	glDrawArrays(GL_POINTS, 0, nPolys);
	compositeShader.unbind();

	//diffuseTextures.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glPopAttrib();

	app->getProfiler().time("Render");

	//drawGrid();
}

void PerPoly::update(float dt)
{
	(void) (dt);
}

void PerPoly::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	nPolys = (unsigned int) app->getGpuMesh().getVao().getIndexBuffer()->getNIndices() / 3;
	std::vector<unsigned int> polyIDs(nPolys);
	for (size_t i = 0; i < nPolys; i++)
		polyIDs[i] = i;
	polySortBuffer.create(&polyIDs[0], nPolys * sizeof(unsigned int));
	//polyFlagBuffer.create(nullptr, nPolys * sizeof(unsigned int));
	//app->getProfiler().setBufferSize("polyFlags", nPolys * sizeof(unsigned int));
	//pixelCountBuffer.create(nullptr, nPolys * sizeof(unsigned int));

	auto zeroVert = ShaderSource("zeroFlags", getZeroShaderSrc());
	zeroShader.release();
	zeroShader.create(&zeroVert);

	if (opaqueFlag == 0)
	{
		polyLFB.setType(LFB::LINK_LIST, "perPoly");
		polyLFB.setMaxFrags(512);
		polyLFB.resize((int) nPolys, 1); // One list per polygon.

		auto vertCap = ShaderSourceCache::getShader("lowCapVert").loadFromFile("shaders/oit/perpoly/capture.vert");
		auto geomCap = ShaderSourceCache::getShader("lowCapGeom").loadFromFile("shaders/oit/perpoly/capture.geom");
		auto fragCap = ShaderSourceCache::getShader("lowCapFrag").loadFromFile("shaders/oit/perpoly/capture.frag");

		captureShader.release();
		captureShader.create(&vertCap, &fragCap, &geomCap);

		auto vertComp = ShaderSourceCache::getShader("lowCompVert").loadFromFile("shaders/oit/perpoly/composite.vert");
		auto geomComp = ShaderSourceCache::getShader("lowCompGeom").loadFromFile("shaders/oit/perpoly/composite.geom");
		auto fragComp = ShaderSourceCache::getShader("lowCompFrag").loadFromFile("shaders/oit/perpoly/composite.frag");

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp, &geomComp);
	}

	// Create the index and vertex data buffers.
	buildVertexBuffers(app);
}

void PerPoly::saveLFB(App *app)
{
	(void) (app);
	//auto s = polyLFB.getStrSorted();
	auto s = polyLFB.getStr();
	Utils::File f("lfb.txt");
	f.write(s);
#if 0
	unsigned int *counts = (unsigned int*) pixelCountBuffer.read();
	std::stringstream ss;
	unsigned int max = 0;
	for (unsigned int i = 0; i < nPolys; i++)
		if (counts[i] > max)
			max = counts[i];
	ss << "max: " << max << "\n";
	for (unsigned int i = 0; i < nPolys; i++)
		ss << i << ": " << counts[i] << "\n";
	Utils::File f("counts.txt");
	f.write(ss.str());
	//app->saveGeometryLFB();
#endif
}

