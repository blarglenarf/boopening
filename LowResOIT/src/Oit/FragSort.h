#pragma once

#include <vector>

struct Frag
{
	float color;
	float z;
};

// This one is slow since it has to copy data to/from the GPU.
void sortFragsThrust(std::vector<Frag> &frags);

// Note: the opengl buffer has to be bound before calling this function.
void createFragsCudaResource(unsigned int fragsBuffer);
void destroyFragsCudaResource();

// Note: the opengl buffer has to be bound before calling this function.
void sortFragsThrust(unsigned int *offsetData, unsigned int nOffsets);

