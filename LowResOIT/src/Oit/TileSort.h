#pragma once

#include "Oit.h"

#include "../../../Renderer/src/LFB/BMA.h"

class TileSort : public Oit
{
private:
	Rendering::Shader captureShader;
	Rendering::Shader compositeShader;
	Rendering::Shader gridShader;
	Rendering::Shader zeroShader;

	Rendering::Shader perTriangleShader;

	Rendering::StorageBuffer grid;

	Rendering::BMA alphaBMA;
	Rendering::LFB perTriangleLFB;

	unsigned int nPolys;
	unsigned int nVerts;

	int width;
	int height;
	int depth;

private:
	void resize(App *app, int width, int height);

	void capture(App *app);
	void sort(App *app);
	void composite(App *app);

	void drawGrid();

public:
	TileSort(int opaqueFlag = 0) : Oit(opaqueFlag), nPolys(0), nVerts(0), width(0), height(0), depth(0) {}
	virtual ~TileSort();

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

