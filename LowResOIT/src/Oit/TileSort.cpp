#include "TileSort.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Renderer/src/LFB/LFBLinear.h"
#include "../../../Utils/src/File.h"

#include "FragSort.h"

#include <string>

#define VERT_OVERALLOCATE 1.2
#define VERT_UNDERALLOCATE 0.5

#define CPU_SORT 0
#define GRID_TILE_SIZE 2
#define USE_GRID 0 // The grid looks like shit...
#define PER_TRIANGLE 0
#define CONSERVATIVE_RASTER 0
#define MAX_DEPTH 64

using namespace Rendering;
using namespace Math;

// TODO: For this silly tiled idea to work:
// DONE: Conservatively raster at low res.
// DONE: Store alpha and depth.
// TODO: Get rid of all the other colour calculation in the capture.frag shader.
// DONE: During sort, save cumulative product of inverse alpha as the sort progresses.
// DONE: Raster geometry at full res, fragment reads alpha from its list and composites according to the alpha value.
// DONE: Use triangle id's instead of testing based on depths.
// DONE: Composite using low-res deep image.
// TODO: Try storing per-triangle lists of visibility values, and composite using those. <-- TODAY
// TODO: If this doesn't improve composite time, then we abandon this approach. <-- TODAY
// TODO: If this does work, then how do you know which visibility value to use?
// TODO: Ideally need high resolution at silhouette edges, and lower resolution everywhere else. <-- HOW/WHEN?
// Ignore the below coverage mask stuff, I'm pretty sure it won't work.
// TODO: Instead of rasterizing conservatively at low-res, you need a coverage mask.
// TODO: How the fuck do I store the coverage mask?
// TODO: Either do some kind of sub-pixel sampling at low-res, or render at high res and find the correct mask in each case.
// TODO: Instead of sorting low-res pixels, can compute visibility as needed (requires iterating through all fragments).
// TODO: But how do I avoid reading the entire tile's list...?
// TODO: Need some way of mapping {fragment,id} to a tile's {alpha,id}, although that only works with linearised...
// TODO: Per-triangle lists would probably solve that, but I have no idea how to sort and pre-compute those.
// TODO: Possible solution is yet another pass to build per-triangle lists of {alpha,pos}, sounds slow.
// TODO: Some kind of hashing function perhaps?
// TODO: For now, forget about performance and just see if the general idea works or not.

// TODO: Raster triangle depths at full res (or low-res conservative).
// TODO: Store a list of ints per triangle, each int is a per tile coverage mask (probably not necessary, just alpha and depth).
// TODO: Masks are size 4x8 pixels.
// TODO: Along with each mask, store depth (not quite sure how to handle colour).
// TODO: Place per-triangle masks into per-tile lists (might be too slow, instead just conservatively raster at low res).
// TODO: May be no point storing masks per-triangle, storing mask per-tile right from the start may be just as much work.
// TODO: Possible solution - initial pass to allocate correct number of per-tile masks, and second pass to populate them.
// TODO: Could store an alpha value per mask, which is then computed during sorting.
// TODO: Sort per-tile masks by depth.
// TODO: If we have per-tile alpha, then we re-rasterize at full res, looking up the correct alpha value in the list.
// TODO: Composite result to full-res pixels.
// TODO: For compositing to work, need to decide how to handle colour.
// TODO: Start with just a single colour value per tile.
// TODO: Could re-rasterize to compute colours, with depth sorting having already pre-computed order.
// TODO: Colour either needs to be 32 ints per tile or a list (preferably a list).
// TODO: A single depth value per tile may be insufficient, in which case per-tile masks need to be checked for overlaps.


static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer Data\
	{\
		uint data[];\
	};\
	\
	void main()\
	{\
		data[gl_VertexID] = 0;\
	}\
	";
}

#if CPU_SORT
static vec4 *esPositions = nullptr;
static unsigned int *indices = nullptr;

static size_t pCount = 0, lCount = 0, fCount = 0;

#if 0
static float det(vec3 u, vec3 v)
{
	return (u.x * v.y) - (u.y * v.x);
}

static bool pointInTriangle(vec3 pt, vec3 v0, vec3 u1, vec3 u2)
{
	vec3 v1 = u1 - v0;
	vec3 v2 = u2 - v0;

	float a = (det(pt, v2) - det(v0, v2)) / det(v1, v2);
	float b = -((det(pt, v1) - det(v0, v1)) / det(v1, v2));

	return a > 0 && b > 0 && a + b < 1;
}
#else
static float sign(vec3 p1, vec3 p2, vec3 p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

static bool pointInTriangle(vec3 pt, vec3 v1, vec3 v2, vec3 v3)
{
	bool b1 = sign(pt, v1, v2) < 0.0f;
	bool b2 = sign(pt, v2, v3) < 0.0f;
	bool b3 = sign(pt, v3, v1) < 0.0f;

	return ((b1 == b2) && (b2 == b3));
}
#endif

// FIXME: Figure out why this is failing in cases where it should not fail...
static vec3 triangleRayIntersection(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 end)
{
	// If we don't normalise this, then we know t values > 1 will be past the end point.
	vec3 d = (end - o);

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	vec3 h = d.cross(e2);
	float a = e1.dot(h);

	if (a > -0.00001 && a < 0.0001)
	{
		//std::cout << "1: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	float f = 1.0f / a;
	vec3 s = o - v0;
	float u = f * s.dot(h);

	if (u < 0.0 || u > 1.0)
	{
		//std::cout << "2: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	vec3 q = s.cross(e1);
	float v = f * d.dot(q);

	if (v < 0.0 || u + v > 1.0)
	{
		//std::cout << "3: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * e2.dot(q);
	vec3 pos = o + (d * t);
	return pos;
}

// NOTE: Remember that eye-space z is negative.
static int cmpfnc(const void *va, const void *vb)
{
	static vec3 o(0, 0, 0);

	unsigned int a = *((unsigned int*) va);
	unsigned int b = *((unsigned int*) vb);

	// Reading this data takes a *very* long time in debug mode, probably not an issue I guess.
	vec3 av[3] = { esPositions[indices[a * 3 + 0]].xyz, esPositions[indices[a * 3 + 1]].xyz, esPositions[indices[a * 3 + 2]].xyz };
	vec3 bv[3] = { esPositions[indices[b * 3 + 0]].xyz, esPositions[indices[b * 3 + 1]].xyz, esPositions[indices[b * 3 + 2]].xyz };

	// Min/max for each triangle.
	vec3 aMin(std::min(std::min(av[0].x, av[1].x), av[2].x), std::min(std::min(av[0].y, av[1].y), av[2].y), std::min(std::min(av[0].z, av[1].z), av[2].z));
	vec3 aMax(std::max(std::max(av[0].x, av[1].x), av[2].x), std::max(std::max(av[0].y, av[1].y), av[2].y), std::max(std::max(av[0].z, av[1].z), av[2].z));

	vec3 bMin(std::min(std::min(bv[0].x, bv[1].x), bv[2].x), std::min(std::min(bv[0].y, bv[1].y), bv[2].y), std::min(std::min(bv[0].z, bv[1].z), bv[2].z));
	vec3 bMax(std::max(std::max(bv[0].x, bv[1].x), bv[2].x), std::max(std::max(bv[0].y, bv[1].y), bv[2].y), std::max(std::max(bv[0].z, bv[1].z), bv[2].z));

	// Check for overlap of the bounding boxes.
	if (aMax.x < bMin.x || aMax.y < bMin.y || aMax.z < bMin.z || bMax.x < aMin.x || bMax.y < aMin.y || bMax.z < aMin.z)
		return aMin.z < bMin.z ? -1 : 1; // Furthest extent of 'a' is further away than furthest extent of 'b'.

#if 1
	// 1. Any vertex from either triangle is inside the other triangle (raycast for depths of each triangle).
	for (int i = 0; i < 3; i++)
	{
		if (pointInTriangle(av[i], bv[0], bv[1], bv[2]))
		{
			pCount++;
			vec3 bPos = triangleRayIntersection(bv[0], bv[1], bv[2], o, av[i]);
			return av[i].z < bPos.z ? -1 : 1;
		}
		if (pointInTriangle(bv[i], av[0], av[2], av[2]))
		{
			pCount++;
			vec3 aPos = triangleRayIntersection(av[0], av[1], av[2], o, bv[i]);
			return aPos.z < bv[i].z ? -1 : 1;
		}
	}

	// TODO: 2. No vertex overlap, but there is triangle edge overlap (requires line-segment intersection tests).
	lCount++;
#endif
	// Above cases don't exist, so no overlap (just sort according to z).
	return aMin.z < bMin.z ? -1 : 1; // Furthest extent of 'a' is further away than furthest extent of 'b'.
}
#endif


void TileSort::resize(App *app, int width, int height)
{
	// TODO: Make sure these sizes are correct.
	(void) (app);

	if (this->width == (int) ceil((float) width / (float) GRID_TILE_SIZE) && this->height == (int) ceil((float) height / (float) GRID_TILE_SIZE))
		return;

	this->width = (int) ceil((float) width / (float) GRID_TILE_SIZE);
	this->height = (int) ceil((float) height / (float) GRID_TILE_SIZE);

#if USE_GRID
	// TODO: Only need one byte per grid cell, not one float.
	grid.create(nullptr, this->width * this->height * depth * sizeof(unsigned int));
#endif
}

void TileSort::capture(App *app)
{
	app->getProfiler().start("Capture");
#if USE_GRID
	auto &camera = Renderer::instance().getActiveCamera();

	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("Data", &grid);
	glDrawArrays(GL_POINTS, 0, width * height * depth);
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	captureShader.bind();
	captureShader.setUniform("Grid", &grid);
	captureShader.setUniform("size", ivec2(width, height));
	captureShader.setUniform("mvMatrix", camera.getInverse());
	captureShader.setUniform("pMatrix", camera.getProjection());
	captureShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	app->getGpuMesh().render();
	captureShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
#else
	#if CONSERVATIVE_RASTER
		glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
	#endif
	app->setTmpViewport(width, height);
	app->captureGeometryLFB(&captureShader);
	app->restoreViewport();
	#if CONSERVATIVE_RASTER
		glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
	#endif
#endif
	app->getProfiler().time("Capture");
}

void TileSort::sort(App *app)
{
#if USE_GRID
	app->getProfiler().start("Grid");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Go through grid from front to back, accumulating visibility as you go.
	app->setTmpViewport(width, height);
	gridShader.bind();
	gridShader.setUniform("Grid", &grid);
	gridShader.setUniform("size", ivec2(width, height));
	Renderer::instance().drawQuad();
	gridShader.unbind();
	app->restoreViewport();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().time("Grid");
#else
	app->getProfiler().start("Sort");

	// TODO: Need to do your own sorting now instead of relying on App.cpp.
	// TODO: Bind all the stuff in perTriangleLFB to the custom sort shaders (sort BMA stuff).
	// TODO: Write per-triangle visibility data to perTriangleLFB.

	// FIXME: For now, sort the data, then create the per-triangle lists just to see if it works.
	// FIXME: If it works, then create the lists during sorting.
	app->setTmpViewport(width, height);

	alphaBMA.createMask(&app->getGeometryLfb());
	alphaBMA.sort(&app->getGeometryLfb());

	app->getProfiler().time("Sort");

	// TODO: If doing grid-based stuff, build the grid here.
	// TODO: If the grid turns out to be any good, then build it during sorting.

	#if PER_TRIANGLE
		glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

		app->getProfiler().start("Triangles");
		perTriangleLFB.beginCapture();

		perTriangleShader.bind();
		perTriangleShader.setUniform("size", ivec2(width, height));
		perTriangleLFB.setUniforms(&perTriangleShader);
		app->getGeometryLfb().setUniforms(&perTriangleShader, false);
		Renderer::instance().drawQuad();
		perTriangleShader.unbind();

		if (perTriangleLFB.endCapture())
		{
			perTriangleLFB.beginCapture();
			perTriangleShader.bind();
			perTriangleShader.setUniform("size", ivec2(width, height));
			perTriangleLFB.setUniforms(&perTriangleShader);
			app->getGeometryLfb().setUniforms(&perTriangleShader, false);
			Renderer::instance().drawQuad();
			perTriangleShader.unbind();
			if (perTriangleLFB.endCapture())
				std::cout << "Something very wrong happened\n";
		}
		app->getProfiler().time("Triangles");

		glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
		glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	#endif

	app->restoreViewport();
#endif
}

void TileSort::composite(App *app)
{
	// Alpha blend according to visibility, which is available in the deep image.
	// TODO: Now, instead of using the low-res deep image, use data in perTriangleLFB (hopefully it'll be faster...).
	app->getProfiler().start("Composite");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_COLOR_BUFFER_BIT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	compositeShader.bind();

#if USE_GRID
	compositeShader.setUniform("Grid", &grid);
#else
	#if PER_TRIANGLE
		perTriangleLFB.setUniforms(&compositeShader, false);
	#else
		app->getGeometryLfb().setUniforms(&compositeShader, false);
	#endif
#endif
	compositeShader.setUniform("size", ivec2(width, height));
	compositeShader.setUniform("mvMatrix", camera.getInverse());
	compositeShader.setUniform("pMatrix", camera.getProjection());
	compositeShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	app->getGpuMesh().render();
	compositeShader.unbind();

	glPopAttrib();

	app->getProfiler().time("Composite");
}

void TileSort::drawGrid()
{
	glPushAttrib(GL_TRANSFORM_BIT | GL_POLYGON_BIT | GL_ENABLE_BIT | GL_CURRENT_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glColor3f(1, 0, 0);

	float xStep = 2.0f / (float) width;
	float yStep = 2.0f / (float) height;

	glBegin(GL_LINES);

	for (float x = -1.0f; x <= 1.0f; x += xStep)
	{
		glVertex3f(x, -1, 0);
		glVertex3f(x, 1, 0);
	}
	for (float y = -1.0f; y <= 1.0f; y += yStep)
	{
		glVertex3f(-1, y, 0);
		glVertex3f(1, y, 0);
	}

	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
}


TileSort::~TileSort()
{
	//destroyFragsCudaResource();
}

void TileSort::render(App *app)
{
	auto &camera = Renderer::instance().getActiveCamera();
	resize(app, camera.getWidth(), camera.getHeight());
#if 0
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	auto &shader = Renderer::instance().getBasicShader();
	shader.bind();
	shader.setUniform("mvMatrix", camera.getInverse());
	shader.setUniform("pMatrix", camera.getProjection());
	//app->getGpuMesh().render();
	app->getMesh().renderImmediate();
	shader.unbind();
	return;
#endif
	capture(app);
	sort(app);
	composite(app);
}

void TileSort::update(float dt)
{
	(void) (dt);
}

void TileSort::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
		auto vertCapture = ShaderSourceCache::getShader("capTileVert").loadFromFile("shaders/oit/tilesort/capture.vert");
		//auto geomCapture = ShaderSourceCache::getShader("capTileGeom").loadFromFile("shaders/oit/tilesort/capture.geom");
		auto fragCapture = ShaderSourceCache::getShader("capTileFrag").loadFromFile("shaders/oit/tilesort/capture.frag");
		fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
		fragCapture.setDefine("USE_GRID", Utils::toString(USE_GRID));
		fragCapture.setDefine("GRID_TILE_SIZE", Utils::toString(GRID_TILE_SIZE));
		fragCapture.setDefine("MAX_DEPTH", Utils::toString(MAX_DEPTH));

		captureShader.release();
		captureShader.create(&vertCapture, &fragCapture);

		auto vertComp = ShaderSourceCache::getShader("compTileVert").loadFromFile("shaders/oit/tilesort/composite.vert");
		auto fragComp = ShaderSourceCache::getShader("compTileFrag").loadFromFile("shaders/oit/tilesort/composite.frag");
		fragComp.setDefine("OPAQUE", Utils::toString(opaqueFlag));
		fragComp.setDefine("USE_GRID", Utils::toString(USE_GRID));
		fragComp.setDefine("GRID_TILE_SIZE", Utils::toString(GRID_TILE_SIZE));
		fragComp.setDefine("MAX_DEPTH", Utils::toString(MAX_DEPTH));
		fragComp.setDefine("PER_TRIANGLE", Utils::toString(PER_TRIANGLE));

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp);
	}

	nPolys = (unsigned int) app->getGpuMesh().getVao().getIndexBuffer()->getNIndices() / 3;
	nVerts = (unsigned int) app->getGpuMesh().getVao().getVertexBuffer()->getNVerts();

	auto zeroVert = ShaderSource("zeroFlags", getZeroShaderSrc());
	zeroShader.release();
	zeroShader.create(&zeroVert);

#if USE_GRID
	depth = MAX_DEPTH;
	grid.release();

	auto gridVertSrc = ShaderSourceCache::getShader("gridVertVis").loadFromFile("shaders/oit/tilesort/grid.vert");
	auto gridFragSrc = ShaderSourceCache::getShader("gridFragVis").loadFromFile("shaders/oit/tilesort/grid.frag");
	gridFragSrc.setDefine("MAX_DEPTH", Utils::toString(MAX_DEPTH));
	gridShader.release();
	gridShader.create(&gridVertSrc, &gridFragSrc);
#endif

#if PER_TRIANGLE
	perTriangleLFB.setType(LFB::LINK_LIST, "tri");
	perTriangleLFB.setMaxFrags(512); // Not really necessary, since we won't be sorting the data.
	perTriangleLFB.setProfiler(&app->getProfiler());
	perTriangleLFB.resize(nPolys, 1);

	auto triVertSrc = ShaderSourceCache::getShader("triVert").loadFromFile("shaders/oit/tilesort/perTriangle.vert");
	auto triFragSrc = ShaderSourceCache::getShader("triFrag").loadFromFile("shaders/oit/tilesort/perTriangle.frag");
	perTriangleShader.release();
	perTriangleShader.create(&triVertSrc, &triFragSrc);
#endif

	alphaBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
	alphaBMA.createShaders(&app->getGeometryLfb(), "shaders/alphaSort/sort.vert", "shaders/alphaSort/sort.frag", {{"COMPOSITE_LOCAL", "1"}, {"PER_TRIANGLE", Utils::toString(PER_TRIANGLE)}});
}

void TileSort::saveLFB(App *app)
{
#if PER_TRIANGLE
	(void) (app);
	auto s = perTriangleLFB.getStr();
	Utils::File f("lfb.txt");
	f.write(s);
#else
	app->saveGeometryLFB();
#endif
}

