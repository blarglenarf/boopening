#include "PerPolySort.h"
#if 0
#include "../../../Renderer/src/GL.h"

#include <cuda_runtime.h>
#include <cuda.h>
#include <cudaGL.h>
#include <cuda_gl_interop.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/sort.h>

#include <iostream>

// Need a bunch of resources for the per-polygon fragment lists.
cudaGraphicsResource *keysResource = NULL;
cudaGraphicsResource *headPtrsResource = NULL;
cudaGraphicsResource *nextPtrsResource = NULL;
cudaGraphicsResource *dataResource = NULL;

unsigned int *keysDevPtr = NULL;
unsigned int *headPtrsDevPtr = NULL;
unsigned int *nextPtrsDevPtr = NULL;
PolyKey *dataDevPtr = NULL;

size_t size = 0;

// TODO: Need to find a way to get this comparison struct to see the data in the other arrays.
struct PolyKeyComp
{
	unsigned int *headPtrs;
	unsigned int *nextPtrs;
	PolyKey *data;

	PolyKeyComp(unsigned int *headPtrs, unsigned int *nextPtrs, PolyKey *data) : headPtrs(headPtrs), nextPtrs(nextPtrs), data(data) {}

	__host__ __device__
	bool operator()(const unsigned int a, const unsigned int b)
	{
		// This will need to check all the fragments for both polygons to determine the order.
		float zA = a;
		float zB = b;

		// TODO: If both lists are sorted, we can step through simultaneously.
		unsigned int nodeA = headPtrs[a];
		while (nodeA != 0)
		{
			PolyKey dataA = data[nodeA];
			zA = dataA.z;

			unsigned int nodeB = headPtrs[b];
			while (nodeB != 0)
			{
				PolyKey dataB = data[nodeB];
				zB = dataB.z;
				if (dataA.id == dataB.id)
					return zA > zB;
				nodeB = nextPtrs[nodeB];
			}

			nodeA = nextPtrs[nodeA];
		}
		return zA > zB;
	}
};


void createPerPolyKeysCudaResource(unsigned int polyKeysBuffer, unsigned int headPtrsBuffer, unsigned int nextPtrsBuffer, unsigned int dataBuffer)
{
	cudaGraphicsGLRegisterBuffer(&keysResource, polyKeysBuffer, cudaGraphicsMapFlagsNone);
	cudaGraphicsGLRegisterBuffer(&headPtrsResource, headPtrsBuffer, cudaGraphicsMapFlagsNone);
	cudaGraphicsGLRegisterBuffer(&nextPtrsResource, nextPtrsBuffer, cudaGraphicsMapFlagsNone);
	cudaGraphicsGLRegisterBuffer(&dataResource, dataBuffer, cudaGraphicsMapFlagsNone);
}

void destroyPerPolyKeysCudaResource()
{
	if (keysResource != NULL)
		cudaGraphicsUnregisterResource(keysResource);
	if (headPtrsResource != NULL)
		cudaGraphicsUnregisterResource(headPtrsResource);
	if (nextPtrsResource != NULL)
		cudaGraphicsUnregisterResource(nextPtrsResource);
	if (dataResource != NULL)
		cudaGraphicsUnregisterResource(dataResource);
	keysResource = NULL;
	headPtrsResource = NULL;
	nextPtrsResource = NULL;
	dataResource = NULL;
}

void sortPerPolyKeysThrust(size_t nPolyKeys)
{
	// FIXME: Probably shouldn't do this here...
	//cudaGLSetGLDevice(0);
	cudaGraphicsMapResources(1, &keysResource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**) &keysDevPtr, &size, keysResource);
	cudaGraphicsMapResources(1, &headPtrsResource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**) &headPtrsDevPtr, &size, headPtrsResource);
	cudaGraphicsMapResources(1, &nextPtrsResource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**) &nextPtrsDevPtr, &size, nextPtrsResource);
	cudaGraphicsMapResources(1, &dataResource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**) &dataDevPtr, &size, dataResource);

	if (keysDevPtr != NULL && headPtrsDevPtr != NULL && nextPtrsDevPtr != NULL && dataDevPtr != NULL)
	{
		thrust::device_ptr<unsigned int> deviceKeys = thrust::device_pointer_cast(keysDevPtr);
		thrust::device_ptr<unsigned int> deviceHeadPtrs = thrust::device_pointer_cast(headPtrsDevPtr);
		thrust::device_ptr<unsigned int> deviceNextPtrs = thrust::device_pointer_cast(nextPtrsDevPtr);
		thrust::device_ptr<PolyKey> deviceData = thrust::device_pointer_cast(dataDevPtr);

		unsigned int *rawHead = thrust::raw_pointer_cast(deviceHeadPtrs.get());
		unsigned int *rawNext = thrust::raw_pointer_cast(deviceNextPtrs.get());
		PolyKey *rawData = thrust::raw_pointer_cast(deviceData.get());

		thrust::sort(deviceKeys, deviceKeys + nPolyKeys, PolyKeyComp(rawHead, rawNext, rawData));
	}

	cudaGraphicsUnmapResources(1, &keysResource, NULL);
	cudaGraphicsUnmapResources(1, &headPtrsResource, NULL);
	cudaGraphicsUnmapResources(1, &nextPtrsResource, NULL);
	cudaGraphicsUnmapResources(1, &dataResource, NULL);
}
#endif

