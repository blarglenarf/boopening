#include "FullSort.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"

#include "PolySort.h"

#include <string>

#define FRAG_OVERALLOCATE 1.2
#define FRAG_UNDERALLOCATE 0.5

#define CPU_SORT 1

using namespace Rendering;
using namespace Math;


static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer Data\
	{\
		uint data[];\
	};\
	\
	void main()\
	{\
		data[gl_VertexID] = 0;\
	}\
	";
}

#if CPU_SORT
static vec4 *esPositions = nullptr;
static unsigned int *indices = nullptr;

static size_t pCount = 0, lCount = 0, fCount = 0;

#if 0
static float det(vec3 u, vec3 v)
{
	return (u.x * v.y) - (u.y * v.x);
}

static bool pointInTriangle(vec3 pt, vec3 v0, vec3 u1, vec3 u2)
{
	vec3 v1 = u1 - v0;
	vec3 v2 = u2 - v0;

	float a = (det(pt, v2) - det(v0, v2)) / det(v1, v2);
	float b = -((det(pt, v1) - det(v0, v1)) / det(v1, v2));

	return a > 0 && b > 0 && a + b < 1;
}
#else
static float sign(vec3 p1, vec3 p2, vec3 p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

static bool pointInTriangle(vec3 pt, vec3 v1, vec3 v2, vec3 v3)
{
	bool b1 = sign(pt, v1, v2) < 0.0f;
	bool b2 = sign(pt, v2, v3) < 0.0f;
	bool b3 = sign(pt, v3, v1) < 0.0f;

	return ((b1 == b2) && (b2 == b3));
}
#endif

// FIXME: Figure out why this is failing in cases where it should not fail...
static vec3 triangleRayIntersection(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 end)
{
	// If we don't normalise this, then we know t values > 1 will be past the end point.
	vec3 d = (end - o);

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	vec3 h = d.cross(e2);
	float a = e1.dot(h);

	if (a > -0.00001 && a < 0.0001)
	{
		//std::cout << "1: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	float f = 1.0f / a;
	vec3 s = o - v0;
	float u = f * s.dot(h);

	if (u < 0.0 || u > 1.0)
	{
		//std::cout << "2: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	vec3 q = s.cross(e1);
	float v = f * d.dot(q);

	if (v < 0.0 || u + v > 1.0)
	{
		//std::cout << "3: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * e2.dot(q);
	vec3 pos = o + (d * t);
	return pos;
}

// NOTE: Remember that eye-space z is negative.
static int cmpfnc(const void *va, const void *vb)
{
	static vec3 o(0, 0, 0);

	unsigned int a = *((unsigned int*) va);
	unsigned int b = *((unsigned int*) vb);

	// Reading this data takes a *very* long time in debug mode, probably not an issue I guess.
	vec3 av[3] = { esPositions[indices[a * 3 + 0]].xyz, esPositions[indices[a * 3 + 1]].xyz, esPositions[indices[a * 3 + 2]].xyz };
	vec3 bv[3] = { esPositions[indices[b * 3 + 0]].xyz, esPositions[indices[b * 3 + 1]].xyz, esPositions[indices[b * 3 + 2]].xyz };

	// Min/max for each triangle.
	vec3 aMin(std::min(std::min(av[0].x, av[1].x), av[2].x), std::min(std::min(av[0].y, av[1].y), av[2].y), std::min(std::min(av[0].z, av[1].z), av[2].z));
	vec3 aMax(std::max(std::max(av[0].x, av[1].x), av[2].x), std::max(std::max(av[0].y, av[1].y), av[2].y), std::max(std::max(av[0].z, av[1].z), av[2].z));

	vec3 bMin(std::min(std::min(bv[0].x, bv[1].x), bv[2].x), std::min(std::min(bv[0].y, bv[1].y), bv[2].y), std::min(std::min(bv[0].z, bv[1].z), bv[2].z));
	vec3 bMax(std::max(std::max(bv[0].x, bv[1].x), bv[2].x), std::max(std::max(bv[0].y, bv[1].y), bv[2].y), std::max(std::max(bv[0].z, bv[1].z), bv[2].z));

	// Check for overlap of the bounding boxes.
	if (aMax.x < bMin.x || aMax.y < bMin.y || aMax.z < bMin.z || bMax.x < aMin.x || bMax.y < aMin.y || bMax.z < aMin.z)
		return aMin.z < bMin.z ? -1 : 1; // Furthest extent of 'a' is further away than furthest extent of 'b'.

#if 1
	// 1. Any vertex from either triangle is inside the other triangle (raycast for depths of each triangle).
	for (int i = 0; i < 3; i++)
	{
		if (pointInTriangle(av[i], bv[0], bv[1], bv[2]))
		{
			pCount++;
			vec3 bPos = triangleRayIntersection(bv[0], bv[1], bv[2], o, av[i]);
			return av[i].z < bPos.z ? -1 : 1;
		}
		if (pointInTriangle(bv[i], av[0], av[2], av[2]))
		{
			pCount++;
			vec3 aPos = triangleRayIntersection(av[0], av[1], av[2], o, bv[i]);
			return aPos.z < bv[i].z ? -1 : 1;
		}
	}

	// TODO: 2. No vertex overlap, but there is triangle edge overlap (requires line-segment intersection tests).
	lCount++;
#endif
	// Above cases don't exist, so no overlap (just sort according to z).
	return aMin.z < bMin.z ? -1 : 1; // Furthest extent of 'a' is further away than furthest extent of 'b'.
}
#endif

void FullSort::buildMaterialBuffers(App *app)
{
	auto &mesh = app->getMesh();

	// Create buffers of material buffers with appropriate properties.
	auto &materials = mesh.getMaterials();
	std::map<Material*, unsigned int> matMap;
	for (size_t i = 0; i < materials.size(); i++)
		matMap[materials[i]] = i;

	ambientBuffer.release();
	diffuseBuffer.release();
	specularBuffer.release();
	shininessBuffer.release();
	texFlagBuffer.release();

	std::vector<vec4> matAmbient(materials.size());
	std::vector<vec4> matDiffuse(materials.size());
	std::vector<vec4> matSpecular(materials.size());
	std::vector<float> matShininess(materials.size());
	//std::vector<bool> matTexFlag(materials.size()); // Apparently the compiler doesn't like me using a vector of bools.
	bool *matTexFlag = new bool[materials.size()];

	for (size_t i = 0; i < materials.size(); i++)
	{
		matAmbient[i] = materials[i]->getAmbientColor();
		matDiffuse[i] = materials[i]->getDiffuseColor();
		matSpecular[i] = materials[i]->getSpecularColor();
		matShininess[i] = materials[i]->getShininess();
		//matTexFlag[i] = (materials[i]->getDiffuseImg() != nullptr);
		matTexFlag[i] = false;
	}

	ambientBuffer.create(&matAmbient[0], materials.size() * sizeof(vec4));
	diffuseBuffer.create(&matDiffuse[0], materials.size() * sizeof(vec4));
	specularBuffer.create(&matSpecular[0], materials.size() * sizeof(vec4));
	shininessBuffer.create(&matShininess[0], materials.size() * sizeof(float));
	texFlagBuffer.create(&matTexFlag[0], materials.size() * sizeof(bool));

	// TODO: Create a 2D texture array with a texture for each material.
	// TODO: Need a map with different texture array formats, which needs to be accessed by the GPU.
#if 0
	diffuseTextures.release();
	diffuseTextures.create(GL_RGB, 1024, 1024, materials.size());
	diffuseTextures.bind();
	for (size_t i = 0; i < materials.size(); i++)
	{
		if (materials[i]->getDiffuseImg() != nullptr)
			diffuseTextures.bufferData(materials[i]->getDiffuseImg()->getData(), i);
	}
	diffuseTextures.unbind();
#endif
	//diffuseTextures.create(GL_RGBA, size_t width, size_t height, size_t layers);

	// Each polygon needs a material id assigned to it.
	auto &faceSets = mesh.getFacesets();
	std::vector<unsigned int> matIndices(mesh.getNumIndices() / 3, 0);

	for (size_t i = 0; i < faceSets.size(); i++)
	{
		auto start = faceSets[i].start;
		auto end = faceSets[i].end;
		unsigned int id = 0;
		if (faceSets[i].mat != nullptr)
			id = matMap[faceSets[i].mat];

		for (unsigned int j = start / 3; j < end / 3; j++)
			matIndices[j] = id;
	}

	matIndexBuffer.release();
	matIndexBuffer.create(&matIndices[0], matIndices.size() * sizeof(unsigned int));

	delete [] matTexFlag;
}

void FullSort::buildVertexBuffers(App *app)
{
	auto &mesh = app->getMesh();

	buildMaterialBuffers(app);

	indexData.release();

	auto nIndices = mesh.getNumIndices();
	auto *indices = mesh.getIndices();
	indexData.create(&indices[0], nIndices * sizeof(unsigned int));

	// Vertex Data needs to be in an appropriate format.
	positionBuffer.release();
	normalBuffer.release();
	texCoordBuffer.release();

	auto nVertices = mesh.getNumVertices();
	auto *vertices = mesh.getVertices();

	std::vector<vec4> positions(nVertices);
	std::vector<vec4> normals(nVertices);
	std::vector<vec4> texCoords(nVertices);

	for (size_t i = 0; i < nVertices; i++)
	{
		auto &vertex = vertices[i];
		positions[i] = vec4(vertex.pos.x, vertex.pos.y, vertex.pos.z, 1.0f);
		normals[i] = vec4(vertex.norm.x, vertex.norm.y, vertex.norm.z, 0.0f);
		texCoords[i] = vec2(vertex.tex.x, vertex.tex.y);
	}

	positionBuffer.create(&positions[0], positions.size() * sizeof(vec4));
	normalBuffer.create(&normals[0], normals.size() * sizeof(vec4));
	texCoordBuffer.create(&texCoords[0], texCoords.size() * sizeof(vec2));

}

void FullSort::resize(App *app, int width, int height)
{
	(void) (app);

	if (this->width == width && this->height == height)
		return;

	this->width = width;
	this->height = height;
}

void FullSort::beginCapture(App *app)
{
#if 0
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	zeroShader.bind();
	//zeroShader.setUniform("PolyFlags", &polyFlagBuffer);
	glDrawArrays(GL_POINTS, 0, nPolys);
	zeroShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glDisable(GL_RASTERIZER_DISCARD);
#endif
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	(void) (app);

	//captureShader.bind();

	//app->setCameraUniforms(&captureShader);
}

bool FullSort::endCapture(App *app)
{
	(void) (app);

	//captureShader.unbind();

	return false;
}

void FullSort::capture(App *app)
{
	// Transform geometry into eye-space and save to an esPosBuffer.
	// TODO: Could use rasterizer to discard polygons that aren't visible.
	app->getProfiler().start("Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glEnable(GL_RASTERIZER_DISCARD);

	esTransformShader.bind();
	esTransformShader.setUniform("Positions", &positionBuffer);
	esTransformShader.setUniform("EsPositions", &esPositionBuffer);
	esTransformShader.setUniform("mvMatrix", camera.getInverse());
	glDrawArrays(GL_POINTS, 0, nVerts);
	esTransformShader.unbind();

	glDisable(GL_RASTERIZER_DISCARD);

	app->getProfiler().time("Capture");
}

void FullSort::sort(App *app)
{
	(void) (app);

	app->getProfiler().start("Sort");
#if CPU_SORT
	pCount = 0;
	lCount = 0;
	fCount = 0;

	// TODO: For now, just use std::sort/qsort, move the sorting to cuda after it's working.
	StorageBuffer tmpBuffer;

	// For the sort we need:
	// * polyIDs - the thing actually being sorted.
	// * esPositions - eye-space geometry.
	// * indices - connectivity of eye-space geometry.
	tmpBuffer.create(nullptr, polyBuffer.getSize());
	polyBuffer.copy(&tmpBuffer);
	unsigned int *polyIDs = (unsigned int*) tmpBuffer.read();

	tmpBuffer.release();
	tmpBuffer.create(nullptr, esPositionBuffer.getSize());
	esPositionBuffer.copy(&tmpBuffer);
	esPositions = (vec4*) tmpBuffer.read();

	tmpBuffer.release();
	tmpBuffer.create(nullptr, indexData.getSize());
	indexData.copy(&tmpBuffer);
	indices = (unsigned int*) tmpBuffer.read();

	qsort(&polyIDs[0], nPolys, sizeof(unsigned int), cmpfnc);
	std::cout << pCount << ", " << lCount << ", " << fCount << "\n";

	// Put the sorted polyIDs back in the polyBuffer.
	polyBuffer.bufferData(&polyIDs[0], nPolys * sizeof(unsigned int));
#else
	// TODO: Cuda implementation (or some kind of compute shader implementation) here.
#endif
	app->getProfiler().time("Sort");
}

void FullSort::drawStencil()
{
	// See which pixels were ok and which ones weren't (render stencil buffer).
	glPushAttrib(GL_ENABLE_BIT | GL_STENCIL_BUFFER_BIT);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	glStencilFunc(GL_EQUAL, 1, 1);

	glColor3f(1, 0, 0);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	Renderer::instance().drawQuad();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
}

void FullSort::drawGrid()
{
	glPushAttrib(GL_TRANSFORM_BIT | GL_POLYGON_BIT | GL_ENABLE_BIT | GL_CURRENT_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glColor3f(1, 0, 0);

	float xStep = 2.0f / (float) width;
	float yStep = 2.0f / (float) height;

	glBegin(GL_LINES);

	for (float x = -1.0f; x <= 1.0f; x += xStep)
	{
		glVertex3f(x, -1, 0);
		glVertex3f(x, 1, 0);
	}
	for (float y = -1.0f; y <= 1.0f; y += yStep)
	{
		glVertex3f(-1, y, 0);
		glVertex3f(1, y, 0);
	}

	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
}


FullSort::~FullSort()
{
#if !CPU_SORT
	destroyPolyKeysCudaResource();
#endif
}

void FullSort::render(App *app)
{
	capture(app);
	sort(app);

	app->getProfiler().start("Index Buffer");
#if 0
	// FIXME: For now just remove the duplicate polygon ids, and see if that removes flickering.
	StorageBuffer tmpBuffer;
	tmpBuffer.create(nullptr, fragListData.getSize());
	fragListData.copy(&tmpBuffer);

	PolyKey *fragData = (PolyKey*) tmpBuffer.read();
	std::vector<PolyKey> newPolys(totalFrags, {0, 0});
	std::vector<PolyKey> polyFlags(nPolys, {0, 0});
	size_t last = 0;
	for (unsigned int i = 0; i < totalFrags; i++)
	{
		auto &frag = fragData[i];
		auto id = frag.id;
		auto z = frag.z;
		// Add new frag.
		if (polyFlags[id].z == 0)
		{
			newPolys[last++] = frag;
			polyFlags[id].z = z;
			polyFlags[id].id = last - 1;
		}
		// Depth is greater, replace.
		else if (z > polyFlags[id].z)
		{
			polyFlags[id].z = z;
			newPolys[polyFlags[id].id].z = z;
		}
	}
	StorageBuffer newBuffer;
	newBuffer.create(&newPolys[0], last * sizeof(PolyKey));
#endif

	// Now render geometry in sorted polygon order.
	app->getProfiler().time("Index Buffer");

	app->getProfiler().start("Render");

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_STENCIL_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	glClear(GL_STENCIL_BUFFER_BIT);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_REPLACE, GL_KEEP);
	glStencilFunc(GL_ALWAYS, 1, 1);

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// TODO: Bind the 2d texture array.
	// FIXME: This is slooooooooow.
	//diffuseTextures.bind();

	compositeShader.bind();
	// TODO: Send the material data.
	//compositeShader.setUniform("diffuseTex", &diffuseTextures);
	compositeShader.setUniform("MatIndices", &matIndexBuffer);
	compositeShader.setUniform("MatAmbient", &ambientBuffer);
	compositeShader.setUniform("MatDiffuse", &diffuseBuffer);
	compositeShader.setUniform("MatSpecular", &specularBuffer);
	compositeShader.setUniform("MatShininess", &shininessBuffer);
	compositeShader.setUniform("MatTexFlag", &texFlagBuffer);
	// Send all the other vertex data.
	compositeShader.setUniform("PolyIDs", &polyBuffer);
	compositeShader.setUniform("IndexData", &indexData);
	compositeShader.setUniform("Positions", &positionBuffer);
	compositeShader.setUniform("Normals", &normalBuffer);
	compositeShader.setUniform("TexCoords", &texCoordBuffer);
	// Finally send the camera data.
	compositeShader.setUniform("mvMatrix", camera.getInverse());
	compositeShader.setUniform("pMatrix", camera.getProjection());
	compositeShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	compositeShader.setUniform("lightDir", vec3(0, 0, 1));
	glDrawArrays(GL_POINTS, 0, nPolys);
	//glDrawArrays(GL_POINTS, 0, totalFrags);
	compositeShader.unbind();

	//diffuseTextures.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glPopAttrib();

	app->getProfiler().time("Render");

	//drawGrid();
	drawStencil();
}

void FullSort::update(float dt)
{
	(void) (dt);
}

void FullSort::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
		auto vertES = ShaderSourceCache::getShader("esTransform").loadFromFile("shaders/oit/fullsort/esTransform.vert");
		esTransformShader.release();
		esTransformShader.create(&vertES);

		auto vertComp = ShaderSourceCache::getShader("lowCompVert").loadFromFile("shaders/oit/fullsort/composite.vert");
		auto geomComp = ShaderSourceCache::getShader("lowCompGeom").loadFromFile("shaders/oit/fullsort/composite.geom");
		auto fragComp = ShaderSourceCache::getShader("lowCompFrag").loadFromFile("shaders/oit/fullsort/composite.frag");

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp, &geomComp);
	}

	nPolys = (unsigned int) app->getGpuMesh().getVao().getIndexBuffer()->getNIndices() / 3;
	nVerts = (unsigned int) app->getGpuMesh().getVao().getVertexBuffer()->getNVerts();
	std::vector<unsigned int> polyIDs(nPolys);
	for (size_t i = 0; i < nPolys; i++)
		polyIDs[i] = i;
	polyBuffer.create(&polyIDs[0], nPolys * sizeof(unsigned int));
	esPositionBuffer.create(nullptr, nVerts * sizeof(vec4));
	app->getProfiler().setBufferSize("polyIDs", nPolys * sizeof(unsigned int));
	app->getProfiler().setBufferSize("esVerts", nVerts * sizeof(vec4));

	auto zeroVert = ShaderSource("zeroFlags", getZeroShaderSrc());
	zeroShader.release();
	zeroShader.create(&zeroVert);

	// Create the index and vertex data buffers.
	buildVertexBuffers(app);
}

void FullSort::saveLFB(App *app)
{
	(void) (app);
#if 0
	unsigned int *counts = (unsigned int*) pixelCountBuffer.read();
	std::stringstream ss;
	unsigned int max = 0;
	for (unsigned int i = 0; i < nPolys; i++)
		if (counts[i] > max)
			max = counts[i];
	ss << "max: " << max << "\n";
	for (unsigned int i = 0; i < nPolys; i++)
		ss << i << ": " << counts[i] << "\n";
	Utils::File f("counts.txt");
	f.write(ss.str());
	//app->saveGeometryLFB();
#endif
}

