#pragma once

#include "Oit.h"

class TinySort : public Oit
{
private:
	Rendering::Shader preTransformShader;
	Rendering::Shader compositeShader;
	Rendering::Shader zeroShader;

	unsigned int triListAlloc;
	unsigned int totalTris;
	Rendering::AtomicBuffer triListCount;

	Rendering::StorageBuffer triBuffer;
	Rendering::StorageBuffer transPositionBuffer;
	Rendering::StorageBuffer transNormalBuffer;
	Rendering::StorageBuffer transTexCoordBuffer;
	Rendering::StorageBuffer transMatIndexBuffer;

#if 0
	// Array of voxel positions.
	unsigned int fragListAlloc;
	unsigned int totalFrags;
	Rendering::AtomicBuffer fragListCount;
	Rendering::StorageBuffer fragListData;
#endif
	unsigned int nPolys;
	unsigned int nVerts;

	Rendering::StorageBuffer indexBuffer;
	Rendering::StorageBuffer positionBuffer;
	Rendering::StorageBuffer normalBuffer;
	Rendering::StorageBuffer texCoordBuffer;

	Rendering::StorageBuffer matIndexBuffer;
	Rendering::StorageBuffer ambientBuffer;
	Rendering::StorageBuffer diffuseBuffer;
	Rendering::StorageBuffer specularBuffer;
	Rendering::StorageBuffer shininessBuffer;
	Rendering::StorageBuffer texFlagBuffer;
	Rendering::Texture2DArray diffuseTextures;

	//Rendering::StorageBuffer pixelCountBuffer;

	int width;
	int height;

private:
	void buildMaterialBuffers(App *app);
	void buildVertexBuffers(App *app);

	void resizeTransformBuffers(App *app);
	bool preTransform(App *app);

	void resize(App *app, int width, int height);
	void beginCapture(App *app);
	bool endCapture(App *app);

	void capture(App *app);
	void sort(App *app);

	void renderBadPixels(App *app);

	void drawStencil();
	void drawGrid();
	void drawTessellation();

public:
	TinySort(int opaqueFlag = 0) : Oit(opaqueFlag), triListAlloc(0), totalTris(0), nPolys(0), nVerts(0), width(0), height(0) {}
	virtual ~TinySort();

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

