#pragma once

#include "Oit.h"

class LowRes : public Oit
{
private:
	Rendering::Shader captureShader;
	Rendering::Shader compositeShader;
	Rendering::Shader zeroShader;

	// Array of voxel positions.
	unsigned int fragListAlloc;
	unsigned int totalFrags;
	Rendering::AtomicBuffer fragListCount;
	Rendering::StorageBuffer fragListData;

	unsigned int nPolys;
	Rendering::StorageBuffer polyFlagBuffer;

	Rendering::StorageBuffer indexData;
	Rendering::StorageBuffer positionBuffer;
	Rendering::StorageBuffer normalBuffer;
	Rendering::StorageBuffer texCoordBuffer;

	Rendering::StorageBuffer matIndexBuffer;
	Rendering::StorageBuffer ambientBuffer;
	Rendering::StorageBuffer diffuseBuffer;
	Rendering::StorageBuffer specularBuffer;
	Rendering::StorageBuffer shininessBuffer;
	Rendering::StorageBuffer texFlagBuffer;
	Rendering::Texture2DArray diffuseTextures;

	Rendering::StorageBuffer pixelCountBuffer;

	int width;
	int height;

private:
	void buildMaterialBuffers(App *app);
	void buildVertexBuffers(App *app);

	void resize(App *app, int width, int height);
	void beginCapture(App *app);
	bool endCapture(App *app);

	void capture(App *app);
	void sort(App *app);

	void drawGrid();
	void drawStencil();

public:
	LowRes(int opaqueFlag = 0) : Oit(opaqueFlag), fragListAlloc(0), totalFrags(0), width(0), height(0) {}
	virtual ~LowRes();

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

