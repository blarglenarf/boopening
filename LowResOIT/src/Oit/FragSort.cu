#include "FragSort.h"

#include "../../../Renderer/src/GL.h"

#include <cuda_runtime.h>
#include <cuda.h>
#include <cudaGL.h>
#include <cuda_gl_interop.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/sort.h>

struct FragComp
{
	__host__ __device__
	bool operator()(const Frag &a, const Frag &b)
	{
		return b.z < a.z;
	}
};

cudaGraphicsResource *fragResource = NULL;
Frag *fragDevPtr = NULL;
size_t fragSize = 0;

void sortFragsThrust(std::vector<Frag> &frags)
{
	// Copy the frags to the GPU.
	thrust::device_vector<Frag> deviceFrags(frags.size());
	thrust::copy(frags.begin(), frags.end(), deviceFrags.begin());

	thrust::sort(deviceFrags.begin(), deviceFrags.end(), FragComp());

	// Get the frags back from the GPU.
	thrust::copy(deviceFrags.begin(), deviceFrags.end(), frags.begin());
}

void createFragsCudaResource(unsigned int fragBuffer)
{
	cudaGraphicsGLRegisterBuffer(&fragResource, fragBuffer, cudaGraphicsMapFlagsNone);
}

void destroyFragsCudaResource()
{
	if (fragResource != NULL)
		cudaGraphicsUnregisterResource(fragResource);
	fragResource = NULL;
}

void sortFragsThrust(unsigned int *offsetData, unsigned int nOffsets)
{
	// FIXME: Probably shouldn't do this here...
	//cudaGLSetGLDevice(0);
	cudaGraphicsMapResources(1, &fragResource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**) &fragDevPtr, &fragSize, fragResource);
	if (fragDevPtr != NULL)
	{
		// Iterate over each pixel, using the per-pixel offsets to determine list sizes.
		thrust::device_ptr<Frag> deviceFrags = thrust::device_pointer_cast(fragDevPtr);
		unsigned int start = 0;
		for (unsigned int i = 0; i <= nOffsets; i++)
		{
			unsigned int end = offsetData[i];
			thrust::sort(deviceFrags + start, deviceFrags + end, FragComp());
			start = end;
		}
	}
	cudaGraphicsUnmapResources(1, &fragResource, NULL);
}

