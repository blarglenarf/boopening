#include "PolySort.h"

#include "../../../Renderer/src/GL.h"

#include <cuda_runtime.h>
#include <cuda.h>
#include <cudaGL.h>
#include <cuda_gl_interop.h>
#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/sort.h>

// TODO: Instead of just comparing depth values, perform a ray intersection test on the two polygons.
// TODO: If this turns out to be too slow, rasterize at low res to get coarse pixel intersections.
// TODO: Can use prime number multiplication to determine which pixels a polygon covers (maybe?).
// TODO: Could also write a min/max z value per polygon for a coarse z intersection test.
struct PolyKeyComp
{
	__host__ __device__
	bool operator()(const PolyKey &a, const PolyKey &b)
	{
		return b.z < a.z;
	}
};

cudaGraphicsResource *resource = NULL;
PolyKey *devPtr = NULL;
size_t size = 0;

void sortPolyKeysThrust(std::vector<PolyKey> &polyKeys)
{
	// Copy the polyKeys to the GPU.
	thrust::device_vector<PolyKey> deviceKeys(polyKeys.size());
	thrust::copy(polyKeys.begin(), polyKeys.end(), deviceKeys.begin());

	thrust::sort(deviceKeys.begin(), deviceKeys.end(), PolyKeyComp());

	// Get the polyKeys back from the GPU.
	thrust::copy(deviceKeys.begin(), deviceKeys.end(), polyKeys.begin());
}

void createPolyKeysCudaResource(unsigned int polyKeysBuffer)
{
	cudaGraphicsGLRegisterBuffer(&resource, polyKeysBuffer, cudaGraphicsMapFlagsNone);
}

void destroyPolyKeysCudaResource()
{
	if (resource != NULL)
		cudaGraphicsUnregisterResource(resource);
	resource = NULL;
}

void sortPolyKeysThrust(size_t nPolyKeys)
{
	// FIXME: Probably shouldn't do this here...
	//cudaGLSetGLDevice(0);
	cudaGraphicsMapResources(1, &resource, NULL);
	cudaGraphicsResourceGetMappedPointer((void**) &devPtr, &size, resource);
	if (devPtr != NULL)
	{
		thrust::device_ptr<PolyKey> deviceKeys = thrust::device_pointer_cast(devPtr);
		thrust::sort(deviceKeys, deviceKeys + nPolyKeys, PolyKeyComp());
	}
	cudaGraphicsUnmapResources(1, &resource, NULL);
}

