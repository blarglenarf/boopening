#pragma once

#include "Oit.h"
#include "../../../Renderer/src/LFB/Cluster.h"

class Bucket : public Oit
{
private:
	struct BmaInterval
	{
		int start;
		int end;
		Rendering::Shader *shader;
		BmaInterval(int start = 0, int end = 0, Rendering::Shader *shader = nullptr) : start(start), end(end), shader(shader) {}
		~BmaInterval() { delete shader; }
	};

	std::vector<BmaInterval> intervals;

	Rendering::Shader bmaMaskShader;

private:
	Rendering::Cluster geometryCluster;

	Rendering::Shader zeroShader;
	Rendering::Shader pixelCountShader;
	Rendering::Shader countShader;
	Rendering::Shader captureShader;
	Rendering::Shader compositeShader;

	Rendering::StorageBuffer clusterCounts;
	Rendering::StorageBuffer pixelCounts;

private:
	void createBmaShaders();
	void createBmaMask();
	void bmaSort();

	void zeroClusterCounts(App *app);
	void captureGeometryCluster(App *app);
	void calcPixelCounts(App *app);
	void sortGeometryCluster(App *app);
	void compositeGeometryCluster();

public:
	Bucket(int opaqueFlag = 0) : Oit(opaqueFlag) {}
	virtual ~Bucket() {}

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

