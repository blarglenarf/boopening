#include "Bucket.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"
#include "../../../Utils/src/UtilsGeneral.h"

#include <string>

#define COMPOSITE_LOCAL 1
#define MAX_FRAGS 32
#define DEBUG_INTERVALS 0

// TODO:
// Instead of using an lfb, use a cluster.
// Need a different sort for clusters.
// Sort will need a thread per pixel, but only needs to sort a cluster at a time.
// Means BMA only needs the max frag count from the clusters per pixel, not the max frag count of the whole pixel.
// Might mean faster sorting on the power plant.
// Might even mean faster capture time?

using namespace Rendering;


static std::string getZeroShaderSrc()
{
	return "\
	#version 430\n\
	\
	buffer Data\
	{\
		uint data[];\
	};\
	\
	void main()\
	{\
		data[gl_VertexID] = 0;\
	}\
	";
}


void Bucket::createBmaShaders()
{
	auto bmaVert = ShaderSourceCache::getShader("bmaMaskVertBucket").loadFromFile("shaders/bucketSort/bmaMask.vert");
	auto bmaFrag = ShaderSourceCache::getShader("bmaMaskFragBucket").loadFromFile("shaders/bucketSort/bmaMask.frag");
	bmaFrag.replace("CLUSTER_NAME", geometryCluster.getName());
	bmaFrag.setDefine("DEBUG", Utils::toString(DEBUG_INTERVALS));
	bmaFrag.setDefine("COMPOSITE_LOCAL", Utils::toString(COMPOSITE_LOCAL));
	bmaMaskShader.create(&bmaVert, &bmaFrag);

	int maxBmaInterval = 1 << Utils::ceilLog2(geometryCluster.getMaxFrags());
	int prev = 0;

	intervals.clear();

	std::vector<int> splits;
	for (int interval = 8; interval <= maxBmaInterval; interval *= 2)
		splits.push_back(interval);
	std::sort(splits.begin(), splits.end());

	intervals.resize(splits.size());

	std::cout << "Trying to compile shaders\n";
	for (size_t i = 0; i < splits.size(); i++)
	{
		int intervalEnd = splits[i];
		intervals[i].start = prev;
		intervals[i].end = intervalEnd;
		intervals[i].shader = new Shader();
		auto &bmaVert = ShaderSourceCache::getShader("bmaVert" + Utils::toString(intervalEnd) + geometryCluster.getName()).loadFromFile("shaders/bucketSort/sort.vert");
		auto &bmaFrag = ShaderSourceCache::getShader("bmaFrag" + Utils::toString(intervalEnd) + geometryCluster.getName()).loadFromFile("shaders/bucketSort/sort.frag");
		bmaFrag.setDefine("MAX_FRAGS", Utils::toString(geometryCluster.getMaxFrags()));
		bmaFrag.setDefine("MAX_FRAGS_OVERRIDE", Utils::toString(intervalEnd));
		bmaFrag.setDefine("COMPOSITE_LOCAL", Utils::toString(COMPOSITE_LOCAL));
		bmaFrag.replace("CLUSTER_NAME", geometryCluster.getName());
		intervals[i].shader->create(&bmaVert, &bmaFrag);
		prev = intervalEnd;
		std::cout << "Another shader compiled\n";
	}
	std::cout << "Shaders compiled!\n";
}

void Bucket::createBmaMask()
{
#if !DEBUG_INTERVALS
	glClear(GL_STENCIL_BUFFER_BIT);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_REPLACE, GL_REPLACE);
#endif

	auto &camera = Renderer::instance().getActiveCamera();

	bmaMaskShader.bind();
	//lfb->setUniforms(maskShader);
	//geometryCluster.setBmaUniforms(maskShader);
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		bmaMaskShader.setUniform("interval", intervals[i].start);
		bmaMaskShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
		bmaMaskShader.setUniform("PixelCounts", &pixelCounts);
		glStencilFunc(GL_GREATER, 1<<i, 0xFF);
		Rendering::Renderer::instance().drawQuad();
	}
	bmaMaskShader.unbind();

#if !DEBUG_INTERVALS
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glDisable(GL_STENCIL_TEST);
#endif
}

void Bucket::bmaSort()
{
#if !DEBUG_INTERVALS
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	for (int i = (int) intervals.size() - 1; i >= 0; i--)
	{
		glStencilFunc(GL_EQUAL, 1<<i, 0xFF);
		intervals[i].shader->bind();
		geometryCluster.setBmaUniforms(intervals[i].shader);
		Rendering::Renderer::instance().drawQuad();
		intervals[i].shader->unbind();
	}
	glDisable(GL_STENCIL_TEST);
#endif
}

void Bucket::zeroClusterCounts(App *app)
{
	app->getProfiler().start("Zero Counts");

	if (!zeroShader.isGenerated())
	{
		auto zeroSrc = ShaderSource("zeroCounts", getZeroShaderSrc());
		zeroShader.create(&zeroSrc);
	}

	auto &camera = Renderer::instance().getActiveCamera();

	if ((int) geometryCluster.getWidth() != camera.getWidth() || (int) geometryCluster.getHeight() != camera.getHeight())
	{
		clusterCounts.create(nullptr, camera.getWidth() * camera.getHeight() * CLUSTERS * sizeof(unsigned int));
		pixelCounts.create(nullptr, camera.getWidth() * camera.getHeight() * sizeof(unsigned int));
	}

	glEnable(GL_RASTERIZER_DISCARD);
	zeroShader.bind();
	zeroShader.setUniform("Data", &clusterCounts);
	glDrawArrays(GL_POINTS, 0, camera.getWidth() * camera.getHeight() * CLUSTERS);
	zeroShader.unbind();
	glDisable(GL_RASTERIZER_DISCARD);

	app->getProfiler().time("Zero Counts");
}

void Bucket::captureGeometryCluster(App *app)
{
	app->getProfiler().start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	if (opaqueFlag == 0)
		geometryCluster.resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && geometryCluster.getType() == Cluster::LINEARIZED)
	{
		// Count the number of frags for capture.
		geometryCluster.beginCountFrags();
		countShader.bind();
		geometryCluster.setCountUniforms(&countShader);
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		app->getGpuMesh().render(false);
		countShader.unbind();
		geometryCluster.endCountFrags();
	}

	if (opaqueFlag == 0)
		geometryCluster.beginCapture();

	captureShader.bind();

	captureShader.setUniform("mvMatrix", camera.getInverse());
	captureShader.setUniform("pMatrix", camera.getProjection());
	captureShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	captureShader.setUniform("ClusterCounts", &clusterCounts);

	if (opaqueFlag == 0)
		geometryCluster.setUniforms(&captureShader);

	// Capture fragments into the lfb.
	app->getGpuMesh().render();
	captureShader.unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && geometryCluster.endCapture())
	{
		geometryCluster.beginCapture();
		captureShader.bind();
		captureShader.setUniform("mvMatrix", camera.getInverse());
		captureShader.setUniform("pMatrix", camera.getProjection());
		captureShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		captureShader.setUniform("ClusterCounts", &clusterCounts);
		geometryCluster.setUniforms(&captureShader);
		app->getGpuMesh().render();
		captureShader.unbind();

		if (geometryCluster.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	// TODO: Profile memory usage perhaps?

	app->getProfiler().time("Geometry Capture");
}

void Bucket::calcPixelCounts(App *app)
{
	app->getProfiler().start("Pixel Counts");

	if (!pixelCountShader.isGenerated())
	{
		auto countVertSrc = ShaderSourceCache::getShader("pixelCountVert").loadFromFile("shaders/oit/bucket/pixelCount.vert");
		auto countFragSrc = ShaderSourceCache::getShader("pixelCountFrag").loadFromFile("shaders/oit/bucket/pixelCount.frag");
		pixelCountShader.create(&countVertSrc, &countFragSrc);
	}

	auto &camera = Renderer::instance().getActiveCamera();

	pixelCountShader.bind();
	pixelCountShader.setUniform("ClusterCounts", &clusterCounts);
	pixelCountShader.setUniform("PixelCounts", &pixelCounts);
	pixelCountShader.setUniform("size", Math::ivec2(camera.getWidth(), camera.getHeight()));
	Renderer::instance().drawQuad();
	pixelCountShader.unbind();

	app->getProfiler().time("Pixel Counts");
}

void Bucket::sortGeometryCluster(App *app)
{
	// TODO: Implement me!
	if (opaqueFlag == 1)
		return;

	app->getProfiler().start("Geometry Sort");

	// Sort using bma+rbs, and write the result back into the lfb.

	createBmaMask();
	bmaSort();
#if 0
	// Sort and composite using bma.
	if (composite)
	{
		geometryLocalBMA.createMask(&geometryCluster);
		geometryLocalBMA.sort(&geometryCluster);
	}
	else
	{
		geometryBMA.createMask(&geometryCluster);
		geometryBMA.sort(&geometryCluster);
	}
#endif

	app->getProfiler().time("Geometry Sort");
}

void Bucket::compositeGeometryCluster()
{
	// TODO: Implement me!
}

void Bucket::render(App *app)
{	
	auto &camera = Renderer::instance().getActiveCamera();
	int width = (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE);
	int height = (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE);

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	app->setTmpViewport(width, height);

	zeroClusterCounts(app);
	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);
	captureGeometryCluster(app);
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);
	calcPixelCounts(app);

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#if COMPOSITE_LOCAL
	sortGeometryCluster(app);
#else
	// Read the result back from the lfb and composite.
	sortGeometryCluster(app);
	app->getProfiler().start("Composite");
	compositeShader.bind();
	compositeGeometryCluster();
	compositeShader.unbind();
	app->getProfiler().time("Composite");
#endif

	app->restoreViewport();

	glPopAttrib();
}

void Bucket::update(float dt)
{
	(void) (dt);
}

void Bucket::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	// Init the geometry cluster.
	geometryCluster.release();
	geometryCluster.setType(Cluster::LINK_LIST, "geom", CLUSTERS, Cluster::VEC2, true, MAX_FRAGS, 1);
	geometryCluster.setMaxFrags(MAX_FRAGS);

	// Init the capture shader.
	auto vertCapture = ShaderSourceCache::getShader("captureBucketVert").loadFromFile("shaders/oit/bucket/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureBucketFrag").loadFromFile("shaders/oit/bucket/capture.frag");
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));
	captureShader.release();
	captureShader.create(&vertCapture, &fragCapture);

	if (opaqueFlag == 0)
	{
		// Need a count cluster shader (could have a default one like the regular lfb).
		auto vertCount = ShaderSourceCache::getShader("countBucketVert").loadFromFile("shaders/oit/bucket/count.vert");
		auto fragCount = ShaderSourceCache::getShader("countBucketFrag").loadFromFile("shaders/oit/bucket/count.frag");
		countShader.release();
		countShader.create(&vertCount, &fragCount);

		auto vertComp = ShaderSourceCache::getShader("compBucketVert").loadFromFile("shaders/oit/bucket/composite.vert");
		auto fragComp = ShaderSourceCache::getShader("compBucketFrag").loadFromFile("shaders/oit/bucket/composite.frag");
		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp);

		// A BMA that uses the PixelCounts.
		createBmaShaders();
#if 0
		// BMA shaders that write to global memory.
		geometryBMA.createMaskShader("geom", "shaders/bucketSort/bmaMask.vert", "shaders/bucketSort/bmaMask.frag");
		geometryBMA.createShaders(&geometryCluster, "shaders/bucketSort/sort.vert", "shaders/bucketSort/sort.frag", {{"COMPOSITE_LOCAL", "0"}});

		// BMA shaders that sort and composite.
		geometryLocalBMA.createMaskShader("geom", "shaders/bucketSort/bmaMask.vert", "shaders/bucketSort/bmaMask.frag");
		geometryLocalBMA.createShaders(&geometryCluster, "shaders/bucketSort/sort.vert", "shaders/bucketSort/sort.frag", {{"COMPOSITE_LOCAL", "1"}});
#endif
	}
}

void Bucket::saveLFB(App *app)
{
	(void) (app);

	if (opaqueFlag == 1)
		return;
	auto s = geometryCluster.getStr();
	Utils::File f("cluster.txt");
	f.write(s);
}

