#include "Standard.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"

#include <string>

#define COMPOSITE_LOCAL 1

using namespace Rendering;

void Standard::render(App *app)
{
	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}
	
	app->captureGeometryLFB();

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

#if COMPOSITE_LOCAL
	app->sortGeometryLFB();
#else
	// Read the result back from the lfb and composite.
	app->sortGeometryLFB(false);
	app->getProfiler().start("Composite");
	compositeShader.bind();
	app->getGeometryLfb().composite(&compositeShader);
	compositeShader.unbind();
	app->getProfiler().time("Composite");
#endif

	glPopAttrib();
}

void Standard::update(float dt)
{
	(void) (dt);
}

void Standard::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
		auto vertComp = ShaderSourceCache::getShader("compBruteVert").loadFromFile("shaders/oit/standard/composite.vert");
		auto fragComp = ShaderSourceCache::getShader("compBruteFrag").loadFromFile("shaders/oit/standard/composite.frag");

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp);
	}
}

void Standard::saveLFB(App *app)
{
	app->saveGeometryLFB();
}

