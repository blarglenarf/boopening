#include "LowRes.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"

#include "PolySort.h"

#include <string>

#define FRAG_OVERALLOCATE 1.2
#define FRAG_UNDERALLOCATE 0.5

using namespace Rendering;
using namespace Math;


static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer PolyFlags\
	{\
		uint polyFlags[];\
	};\
	\
	void main()\
	{\
		polyFlags[gl_VertexID] = 0;\
	}\
	";
}


void LowRes::buildMaterialBuffers(App *app)
{
	auto &mesh = app->getMesh();

	// Create buffers of material buffers with appropriate properties.
	auto &materials = mesh.getMaterials();
	std::map<Material*, unsigned int> matMap;
	for (size_t i = 0; i < materials.size(); i++)
		matMap[materials[i]] = i;

	ambientBuffer.release();
	diffuseBuffer.release();
	specularBuffer.release();
	shininessBuffer.release();
	texFlagBuffer.release();

	std::vector<vec4> matAmbient(materials.size());
	std::vector<vec4> matDiffuse(materials.size());
	std::vector<vec4> matSpecular(materials.size());
	std::vector<float> matShininess(materials.size());
	//std::vector<bool> matTexFlag(materials.size()); // Apparently the compiler doesn't like me using a vector of bools.
	bool *matTexFlag = new bool[materials.size()];

	for (size_t i = 0; i < materials.size(); i++)
	{
		matAmbient[i] = materials[i]->getAmbientColor();
		matDiffuse[i] = materials[i]->getDiffuseColor();
		matSpecular[i] = materials[i]->getSpecularColor();
		matShininess[i] = materials[i]->getShininess();
		//matTexFlag[i] = (materials[i]->getDiffuseImg() != nullptr);
		matTexFlag[i] = false;
	}

	ambientBuffer.create(&matAmbient[0], materials.size() * sizeof(vec4));
	diffuseBuffer.create(&matDiffuse[0], materials.size() * sizeof(vec4));
	specularBuffer.create(&matSpecular[0], materials.size() * sizeof(vec4));
	shininessBuffer.create(&matShininess[0], materials.size() * sizeof(float));
	texFlagBuffer.create(&matTexFlag[0], materials.size() * sizeof(bool));

	// TODO: Create a 2D texture array with a texture for each material.
	// TODO: Need a map with different texture array formats, which needs to be accessed by the GPU.
#if 0
	diffuseTextures.release();
	diffuseTextures.create(GL_RGB, 1024, 1024, materials.size());
	diffuseTextures.bind();
	for (size_t i = 0; i < materials.size(); i++)
	{
		if (materials[i]->getDiffuseImg() != nullptr)
			diffuseTextures.bufferData(materials[i]->getDiffuseImg()->getData(), i);
	}
	diffuseTextures.unbind();
#endif
	//diffuseTextures.create(GL_RGBA, size_t width, size_t height, size_t layers);

	// Each polygon needs a material id assigned to it.
	auto &faceSets = mesh.getFacesets();
	std::vector<unsigned int> matIndices(mesh.getNumIndices() / 3, 0);

	for (size_t i = 0; i < faceSets.size(); i++)
	{
		auto start = faceSets[i].start;
		auto end = faceSets[i].end;
		unsigned int id = 0;
		if (faceSets[i].mat != nullptr)
			id = matMap[faceSets[i].mat];

		for (unsigned int j = start / 3; j < end / 3; j++)
			matIndices[j] = id;
	}

	matIndexBuffer.release();
	matIndexBuffer.create(&matIndices[0], matIndices.size() * sizeof(unsigned int));

	delete [] matTexFlag;
}

void LowRes::buildVertexBuffers(App *app)
{
	auto &mesh = app->getMesh();

	buildMaterialBuffers(app);

	indexData.release();

	auto nIndices = mesh.getNumIndices();
	auto *indices = mesh.getIndices();
	indexData.create(&indices[0], nIndices * sizeof(unsigned int));

	// Vertex Data needs to be in an appropriate format.
	positionBuffer.release();
	normalBuffer.release();
	texCoordBuffer.release();

	auto nVertices = mesh.getNumVertices();
	auto *vertices = mesh.getVertices();

	std::vector<vec4> positions(nVertices);
	std::vector<vec4> normals(nVertices);
	std::vector<vec4> texCoords(nVertices);

	for (size_t i = 0; i < nVertices; i++)
	{
		auto &vertex = vertices[i];
		positions[i] = vec4(vertex.pos.x, vertex.pos.y, vertex.pos.z, 1.0f);
		normals[i] = vec4(vertex.norm.x, vertex.norm.y, vertex.norm.z, 0.0f);
		texCoords[i] = vec2(vertex.tex.x, vertex.tex.y);
	}

	positionBuffer.create(&positions[0], positions.size() * sizeof(vec4));
	normalBuffer.create(&normals[0], normals.size() * sizeof(vec4));
	texCoordBuffer.create(&texCoords[0], texCoords.size() * sizeof(vec2));

}

void LowRes::resize(App *app, int width, int height)
{
	if (this->width == width && this->height == height)
		return;

	this->width = width;
	this->height = height;

	totalFrags = 0;

	if (!fragListCount.isGenerated())
	{
		unsigned int zero = 0;
		fragListCount.create(&zero, sizeof(zero));
	}

	// For now just allocate 5 * resolution frags, this will dynamically resize later.
	if (fragListAlloc == 0)
		fragListAlloc = width * height * 5;

	if (!fragListData.isGenerated())
		fragListData.create(nullptr, fragListAlloc * sizeof(float) * 2);

	app->getProfiler().setBufferSize("fragData", fragListAlloc * sizeof(float) * 2);
}

void LowRes::beginCapture(App *app)
{
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	zeroShader.bind();
	//zeroShader.setUniform("PolyFlags", &polyFlagBuffer);
	zeroShader.setUniform("PolyFlags", &pixelCountBuffer);
	glDrawArrays(GL_POINTS, 0, nPolys);
	zeroShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	captureShader.bind();

	captureShader.setUniform("FragListCount", &fragListCount);
	captureShader.setUniform("FragListData", &fragListData);
	captureShader.setUniform("fragListAlloc", fragListAlloc);
	captureShader.setUniform("PixelCount", &pixelCountBuffer);
	app->setCameraUniforms(&captureShader);
}

bool LowRes::endCapture(App *app)
{
	captureShader.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	// It's possible that not enough (or far too many) voxels were allocated, if so, reallocate.
	// FIXME: This read may not actually be the best function to use here...
	totalFrags = *((unsigned int*) fragListCount.read());
	//std::cout << "total frags: " << totalFrags << "\n";

	bool resizeFlag = false;
	if (totalFrags > fragListAlloc || totalFrags < fragListAlloc * FRAG_UNDERALLOCATE)
	{
		fragListAlloc = (unsigned int) (totalFrags * FRAG_OVERALLOCATE);
		resizeFlag = true;
	}

	if (resizeFlag)
	{
		fragListData.bufferData(nullptr, fragListAlloc * sizeof(float) * 2);
		app->getProfiler().setBufferSize("fragData", fragListAlloc * sizeof(float) * 2);
	}

	unsigned int zero = 0;
	fragListCount.bufferData(&zero, sizeof(zero));

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);

	return resizeFlag;
}

void LowRes::capture(App *app)
{
	app->getProfiler().start("Capture");

	auto &camera = Renderer::instance().getActiveCamera();
	resize(app, (int) ceil((float) camera.getWidth() / (float) GRID_CELL_SIZE), (int) ceil((float) camera.getHeight() / (float) GRID_CELL_SIZE));

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	if (opaqueFlag == 1)
	{
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
	}

	app->setTmpViewport(width, height);

	glEnable(GL_CONSERVATIVE_RASTERIZATION_NV);

	// TODO: Capture everything to one big list.
	beginCapture(app);
	//app->getGpuMesh().render();
	app->getGpuMesh().getVao().render();
	if (endCapture(app))
	{
		std::cout << "Resizing\n";
		beginCapture(app);
		//app->getGpuMesh().render();
		app->getGpuMesh().getVao().render();
		endCapture(app);
	}

	//app->captureGeometryLFB();
	glDisable(GL_CONSERVATIVE_RASTERIZATION_NV);

	if (opaqueFlag)
	{
		glPopAttrib();
		return;
	}

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	
	app->restoreViewport();

	glPopAttrib();

	app->getProfiler().time("Capture");
}

void LowRes::sort(App *app)
{
	static unsigned int prevTotalFrags = 0;
#if 0
	StorageBuffer tmpBuffer;
	tmpBuffer.create(nullptr, fragListData.getSize());
	fragListData.copy(&tmpBuffer);

	PolyKey *fragData = (PolyKey*) tmpBuffer.read();
	for (unsigned int i = 0; i < totalFrags; i++)
		std::cout << fragData[i].id << "; ";
	std::cout << "\n" << totalFrags << "\n" << nPolys << "\n";
	exit(0);
#endif
	app->getProfiler().start("Sort");

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

#if 0
	if (totalFrags != prevTotalFrags)
	{
		destroyPolyKeysCudaResource();
		fragListData.bind();
		createPolyKeysCudaResource(fragListData.getObject());
		sortPolyKeysThrust(totalFrags);
		fragListData.unbind();

		prevTotalFrags = totalFrags;
	}
	else
	{
		fragListData.bind();
		sortPolyKeysThrust(totalFrags);
		fragListData.unbind();
	}
#else
	if (prevTotalFrags == 0)
	{
	#if 0
		destroyPolyKeysCudaResource();
		fragListData.bind();
		createPolyKeysCudaResource(fragListData.getObject());
		sortPolyKeysThrust(totalFrags);
		fragListData.unbind();
	#endif
		prevTotalFrags = 1;
	}
	else
	{
	#if 0
		fragListData.bind();
		sortPolyKeysThrust(totalFrags);
		fragListData.unbind();
	#endif
	}
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	app->getProfiler().time("Sort");
#if 0
	StorageBuffer tmpBuffer;
	tmpBuffer.create(nullptr, fragListData.getSize());
	fragListData.copy(&tmpBuffer);

	PolyKey *fragData = (PolyKey*) tmpBuffer.read();
	for (unsigned int i = 0; i < totalFrags; i++)
		std::cout << fragData[i].id << "; ";
	std::cout << "\n";
#endif
}

void LowRes::drawGrid()
{
	glPushAttrib(GL_TRANSFORM_BIT | GL_POLYGON_BIT | GL_ENABLE_BIT | GL_CURRENT_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glColor3f(1, 0, 0);

	float xStep = 2.0f / (float) width;
	float yStep = 2.0f / (float) height;

	glBegin(GL_LINES);

	for (float x = -1.0f; x <= 1.0f; x += xStep)
	{
		glVertex3f(x, -1, 0);
		glVertex3f(x, 1, 0);
	}
	for (float y = -1.0f; y <= 1.0f; y += yStep)
	{
		glVertex3f(-1, y, 0);
		glVertex3f(1, y, 0);
	}

	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
}

void LowRes::drawStencil()
{
	// See which pixels were ok and which ones weren't (render stencil buffer).
	glPushAttrib(GL_ENABLE_BIT | GL_STENCIL_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	glStencilFunc(GL_EQUAL, 1, 1);

	glColor3f(1, 0, 0);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	Renderer::instance().drawQuad();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
}


LowRes::~LowRes()
{
#if 0
	destroyPolyKeysCudaResource();
#endif
}

void LowRes::render(App *app)
{
	capture(app);
	sort(app);

	app->getProfiler().start("Index Buffer");
#if 0
	// FIXME: For now just remove the duplicate polygon ids, and see if that removes flickering.
	StorageBuffer tmpBuffer;
	tmpBuffer.create(nullptr, fragListData.getSize());
	fragListData.copy(&tmpBuffer);

	PolyKey *fragData = (PolyKey*) tmpBuffer.read();
	std::vector<PolyKey> newPolys(totalFrags, {0, 0});
	std::vector<PolyKey> polyFlags(nPolys, {0, 0});
	size_t last = 0;
	for (unsigned int i = 0; i < totalFrags; i++)
	{
		auto &frag = fragData[i];
		auto id = frag.id;
		auto z = frag.z;
		// Add new frag.
		if (polyFlags[id].z == 0)
		{
			newPolys[last++] = frag;
			polyFlags[id].z = z;
			polyFlags[id].id = last - 1;
		}
		// Depth is greater, replace.
		else if (z > polyFlags[id].z)
		{
			polyFlags[id].z = z;
			newPolys[polyFlags[id].id].z = z;
		}
	}
	StorageBuffer newBuffer;
	newBuffer.create(&newPolys[0], last * sizeof(PolyKey));
#endif
	// TODO: As an alternative to using polygon flags, remove duplicate polygon ids (pretty much a requirement).
	// Zero the polygon flag buffer.
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	zeroShader.bind();
	zeroShader.setUniform("PolyFlags", &polyFlagBuffer);
	glDrawArrays(GL_POINTS, 0, nPolys);
	zeroShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// TODO: Instead of trying to pull the data in the right order, we can just populate an index buffer based on the polygon ids.

	glDisable(GL_RASTERIZER_DISCARD);

	// Now render geometry in sorted polygon order.
	app->getProfiler().time("Index Buffer");

	app->getProfiler().start("Render");

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_STENCIL_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Setup the stencil mask so that any pixel with a failed depth test is marked.
	// TODO: One possibility is to setup the stencil mask first, and then render geometry (probably the best approach).
	glClear(GL_STENCIL_BUFFER_BIT);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_REPLACE, GL_KEEP);
	glStencilFunc(GL_ALWAYS, 1, 1);

	//glDisable(GL_DEPTH_TEST);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// TODO: Bind the 2d texture array.
	// FIXME: This is slooooooooow.
	//diffuseTextures.bind();

	compositeShader.bind();
	// TODO: Send the material data.
	//compositeShader.setUniform("diffuseTex", &diffuseTextures);
	compositeShader.setUniform("MatIndices", &matIndexBuffer);
	compositeShader.setUniform("MatAmbient", &ambientBuffer);
	compositeShader.setUniform("MatDiffuse", &diffuseBuffer);
	compositeShader.setUniform("MatSpecular", &specularBuffer);
	compositeShader.setUniform("MatShininess", &shininessBuffer);
	compositeShader.setUniform("MatTexFlag", &texFlagBuffer);
	// Send all the other vertex data.
	//compositeShader.setUniform("FragListData", &newBuffer);
	compositeShader.setUniform("FragListData", &fragListData);
	compositeShader.setUniform("PolyFlags", &polyFlagBuffer);
	compositeShader.setUniform("IndexData", &indexData);
	compositeShader.setUniform("Positions", &positionBuffer);
	compositeShader.setUniform("Normals", &normalBuffer);
	compositeShader.setUniform("TexCoords", &texCoordBuffer);
	// Finally send the camera data.
	compositeShader.setUniform("mvMatrix", camera.getInverse());
	compositeShader.setUniform("pMatrix", camera.getProjection());
	compositeShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	compositeShader.setUniform("lightDir", vec3(0, 0, 1));
	glDrawArrays(GL_POINTS, 0, totalFrags);
	compositeShader.unbind();

	//diffuseTextures.unbind();

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glPopAttrib();

	app->getProfiler().time("Render");

	//drawGrid();
	drawStencil();
}

void LowRes::update(float dt)
{
	(void) (dt);
}

void LowRes::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
		auto vertCap = ShaderSourceCache::getShader("lowCapVert").loadFromFile("shaders/oit/lowres/capture.vert");
		auto geomCap = ShaderSourceCache::getShader("lowCapGeom").loadFromFile("shaders/oit/lowres/capture.geom");
		auto fragCap = ShaderSourceCache::getShader("lowCapFrag").loadFromFile("shaders/oit/lowres/capture.frag");

		captureShader.release();
		captureShader.create(&vertCap, &fragCap, &geomCap);

		auto vertComp = ShaderSourceCache::getShader("lowCompVert").loadFromFile("shaders/oit/lowres/composite.vert");
		auto geomComp = ShaderSourceCache::getShader("lowCompGeom").loadFromFile("shaders/oit/lowres/composite.geom");
		auto fragComp = ShaderSourceCache::getShader("lowCompFrag").loadFromFile("shaders/oit/lowres/composite.frag");

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp, &geomComp);
	}

	nPolys = (unsigned int) app->getGpuMesh().getVao().getIndexBuffer()->getNIndices() / 3;
	polyFlagBuffer.create(nullptr, nPolys * sizeof(unsigned int));
	app->getProfiler().setBufferSize("polyFlags", nPolys * sizeof(unsigned int));
	pixelCountBuffer.create(nullptr, nPolys * sizeof(unsigned int));

	auto zeroVert = ShaderSource("zeroFlags", getZeroShaderSrc());
	zeroShader.release();
	zeroShader.create(&zeroVert);

	// Create the index and vertex data buffers.
	buildVertexBuffers(app);
}

void LowRes::saveLFB(App *app)
{
	(void) (app);

	unsigned int *counts = (unsigned int*) pixelCountBuffer.read();
	std::stringstream ss;
	unsigned int max = 0;
	for (unsigned int i = 0; i < nPolys; i++)
		if (counts[i] > max)
			max = counts[i];
	ss << "max: " << max << "\n";
	for (unsigned int i = 0; i < nPolys; i++)
		ss << i << ": " << counts[i] << "\n";
	Utils::File f("counts.txt");
	f.write(ss.str());
	//app->saveGeometryLFB();
}

