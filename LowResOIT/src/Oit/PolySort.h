#pragma once

#include <vector>

struct PolyKey
{
	unsigned int id;
	float z;

	//PolyKey() : id(0), z(0) {}
};

// This one is slow since it has to copy data to/from the GPU.
void sortPolyKeysThrust(std::vector<PolyKey> &polyKeys);

// Note: the opengl buffer has to be bound before calling this function.
void createPolyKeysCudaResource(unsigned int polyKeysBuffer);
void destroyPolyKeysCudaResource();

// Note: the opengl buffer has to be bound before calling this function.
void sortPolyKeysThrust(size_t nPolyKeys);

