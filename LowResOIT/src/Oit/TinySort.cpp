#include "TinySort.h"

#include "../App.h"
#include "../../../Renderer/src/RendererCommon.h"
#include "../../../Utils/src/File.h"

#include "PolySort.h"

#include <string>

#define VERT_OVERALLOCATE 1.2
#define VERT_UNDERALLOCATE 0.5

#define CPU_SORT 0

using namespace Rendering;
using namespace Math;


static std::string getZeroShaderSrc()
{
	return "\
	#version 450\n\
	\
	buffer Data\
	{\
		uint data[];\
	};\
	\
	void main()\
	{\
		data[gl_VertexID] = 0;\
	}\
	";
}

#if CPU_SORT
static vec4 *esPositions = nullptr;
static unsigned int *indices = nullptr;

static size_t pCount = 0, lCount = 0, fCount = 0;

#if 0
static float det(vec3 u, vec3 v)
{
	return (u.x * v.y) - (u.y * v.x);
}

static bool pointInTriangle(vec3 pt, vec3 v0, vec3 u1, vec3 u2)
{
	vec3 v1 = u1 - v0;
	vec3 v2 = u2 - v0;

	float a = (det(pt, v2) - det(v0, v2)) / det(v1, v2);
	float b = -((det(pt, v1) - det(v0, v1)) / det(v1, v2));

	return a > 0 && b > 0 && a + b < 1;
}
#else
static float sign(vec3 p1, vec3 p2, vec3 p3)
{
	return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

static bool pointInTriangle(vec3 pt, vec3 v1, vec3 v2, vec3 v3)
{
	bool b1 = sign(pt, v1, v2) < 0.0f;
	bool b2 = sign(pt, v2, v3) < 0.0f;
	bool b3 = sign(pt, v3, v1) < 0.0f;

	return ((b1 == b2) && (b2 == b3));
}
#endif

// FIXME: Figure out why this is failing in cases where it should not fail...
static vec3 triangleRayIntersection(vec3 v0, vec3 v1, vec3 v2, vec3 o, vec3 end)
{
	// If we don't normalise this, then we know t values > 1 will be past the end point.
	vec3 d = (end - o);

	vec3 e1 = v1 - v0;
	vec3 e2 = v2 - v0;

	vec3 h = d.cross(e2);
	float a = e1.dot(h);

	if (a > -0.00001 && a < 0.0001)
	{
		//std::cout << "1: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	float f = 1.0f / a;
	vec3 s = o - v0;
	float u = f * s.dot(h);

	if (u < 0.0 || u > 1.0)
	{
		//std::cout << "2: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	vec3 q = s.cross(e1);
	float v = f * d.dot(q);

	if (v < 0.0 || u + v > 1.0)
	{
		//std::cout << "3: This should never happen!\n";
		fCount++;
		return vec3(0, 0, 0);
	}

	// Now can compute t to find out where the intersection point is on the line.
	float t = f * e2.dot(q);
	vec3 pos = o + (d * t);
	return pos;
}

// NOTE: Remember that eye-space z is negative.
static int cmpfnc(const void *va, const void *vb)
{
	static vec3 o(0, 0, 0);

	unsigned int a = *((unsigned int*) va);
	unsigned int b = *((unsigned int*) vb);

	// Reading this data takes a *very* long time in debug mode, probably not an issue I guess.
	vec3 av[3] = { esPositions[indices[a * 3 + 0]].xyz, esPositions[indices[a * 3 + 1]].xyz, esPositions[indices[a * 3 + 2]].xyz };
	vec3 bv[3] = { esPositions[indices[b * 3 + 0]].xyz, esPositions[indices[b * 3 + 1]].xyz, esPositions[indices[b * 3 + 2]].xyz };

	// Min/max for each triangle.
	vec3 aMin(std::min(std::min(av[0].x, av[1].x), av[2].x), std::min(std::min(av[0].y, av[1].y), av[2].y), std::min(std::min(av[0].z, av[1].z), av[2].z));
	vec3 aMax(std::max(std::max(av[0].x, av[1].x), av[2].x), std::max(std::max(av[0].y, av[1].y), av[2].y), std::max(std::max(av[0].z, av[1].z), av[2].z));

	vec3 bMin(std::min(std::min(bv[0].x, bv[1].x), bv[2].x), std::min(std::min(bv[0].y, bv[1].y), bv[2].y), std::min(std::min(bv[0].z, bv[1].z), bv[2].z));
	vec3 bMax(std::max(std::max(bv[0].x, bv[1].x), bv[2].x), std::max(std::max(bv[0].y, bv[1].y), bv[2].y), std::max(std::max(bv[0].z, bv[1].z), bv[2].z));

	// Check for overlap of the bounding boxes.
	if (aMax.x < bMin.x || aMax.y < bMin.y || aMax.z < bMin.z || bMax.x < aMin.x || bMax.y < aMin.y || bMax.z < aMin.z)
		return aMin.z < bMin.z ? -1 : 1; // Furthest extent of 'a' is further away than furthest extent of 'b'.

#if 1
	// 1. Any vertex from either triangle is inside the other triangle (raycast for depths of each triangle).
	for (int i = 0; i < 3; i++)
	{
		if (pointInTriangle(av[i], bv[0], bv[1], bv[2]))
		{
			pCount++;
			vec3 bPos = triangleRayIntersection(bv[0], bv[1], bv[2], o, av[i]);
			return av[i].z < bPos.z ? -1 : 1;
		}
		if (pointInTriangle(bv[i], av[0], av[2], av[2]))
		{
			pCount++;
			vec3 aPos = triangleRayIntersection(av[0], av[1], av[2], o, bv[i]);
			return aPos.z < bv[i].z ? -1 : 1;
		}
	}

	// TODO: 2. No vertex overlap, but there is triangle edge overlap (requires line-segment intersection tests).
	lCount++;
#endif
	// Above cases don't exist, so no overlap (just sort according to z).
	return aMin.z < bMin.z ? -1 : 1; // Furthest extent of 'a' is further away than furthest extent of 'b'.
}
#endif

void TinySort::buildMaterialBuffers(App *app)
{
	auto &mesh = app->getMesh();

	// Create buffers of material buffers with appropriate properties.
	auto &materials = mesh.getMaterials();
	std::map<Material*, unsigned int> matMap;
	for (size_t i = 0; i < materials.size(); i++)
		matMap[materials[i]] = i;

	ambientBuffer.release();
	diffuseBuffer.release();
	specularBuffer.release();
	shininessBuffer.release();
	texFlagBuffer.release();

	std::vector<vec4> matAmbient(materials.size());
	std::vector<vec4> matDiffuse(materials.size());
	std::vector<vec4> matSpecular(materials.size());
	std::vector<float> matShininess(materials.size());
	//std::vector<bool> matTexFlag(materials.size()); // Apparently the compiler doesn't like me using a vector of bools.
	bool *matTexFlag = new bool[materials.size()];

	for (size_t i = 0; i < materials.size(); i++)
	{
		matAmbient[i] = materials[i]->getAmbientColor();
		matDiffuse[i] = materials[i]->getDiffuseColor();
		matSpecular[i] = materials[i]->getSpecularColor();
		matShininess[i] = materials[i]->getShininess();
		//matTexFlag[i] = (materials[i]->getDiffuseImg() != nullptr);
		matTexFlag[i] = false;
	}

	ambientBuffer.create(&matAmbient[0], materials.size() * sizeof(vec4));
	diffuseBuffer.create(&matDiffuse[0], materials.size() * sizeof(vec4));
	specularBuffer.create(&matSpecular[0], materials.size() * sizeof(vec4));
	shininessBuffer.create(&matShininess[0], materials.size() * sizeof(float));
	texFlagBuffer.create(&matTexFlag[0], materials.size() * sizeof(bool));

	// TODO: Create a 2D texture array with a texture for each material.
	// TODO: Need a map with different texture array formats, which needs to be accessed by the GPU.
#if 0
	diffuseTextures.release();
	diffuseTextures.create(GL_RGB, 1024, 1024, materials.size());
	diffuseTextures.bind();
	for (size_t i = 0; i < materials.size(); i++)
	{
		if (materials[i]->getDiffuseImg() != nullptr)
			diffuseTextures.bufferData(materials[i]->getDiffuseImg()->getData(), i);
	}
	diffuseTextures.unbind();
#endif
	//diffuseTextures.create(GL_RGBA, size_t width, size_t height, size_t layers);

	// Each polygon needs a material id assigned to it.
	auto &faceSets = mesh.getFacesets();
	std::vector<unsigned int> matIndices(mesh.getNumIndices() / 3, 0);

	for (size_t i = 0; i < faceSets.size(); i++)
	{
		auto start = faceSets[i].start;
		auto end = faceSets[i].end;
		unsigned int id = 0;
		if (faceSets[i].mat != nullptr)
			id = matMap[faceSets[i].mat];

		for (unsigned int j = start / 3; j < end / 3; j++)
			matIndices[j] = id;
	}

	matIndexBuffer.release();
	matIndexBuffer.create(&matIndices[0], matIndices.size() * sizeof(unsigned int));

	delete [] matTexFlag;
}

void TinySort::buildVertexBuffers(App *app)
{
	auto &mesh = app->getMesh();

	buildMaterialBuffers(app);

	indexBuffer.release();

	auto nIndices = mesh.getNumIndices();
	auto *indices = mesh.getIndices();
	indexBuffer.create(&indices[0], nIndices * sizeof(unsigned int));

	// Vertex Data needs to be in an appropriate format.
	positionBuffer.release();
	normalBuffer.release();
	texCoordBuffer.release();

	auto nVertices = mesh.getNumVertices();
	auto *vertices = mesh.getVertices();

	std::vector<vec4> positions(nVertices);
	std::vector<vec4> normals(nVertices);
	std::vector<vec4> texCoords(nVertices);

	for (size_t i = 0; i < nVertices; i++)
	{
		auto &vertex = vertices[i];
		positions[i] = vec4(vertex.pos.x, vertex.pos.y, vertex.pos.z, 1.0f);
		normals[i] = vec4(vertex.norm.x, vertex.norm.y, vertex.norm.z, 0.0f);
		texCoords[i] = vec2(vertex.tex.x, vertex.tex.y);
	}

	positionBuffer.create(&positions[0], positions.size() * sizeof(vec4));
	normalBuffer.create(&normals[0], normals.size() * sizeof(vec4));
	texCoordBuffer.create(&texCoords[0], texCoords.size() * sizeof(vec2));
}

void TinySort::resizeTransformBuffers(App *app)
{
	if (triListAlloc == 0)
		triListAlloc = nPolys * 2;

	// One id per triangle.
	triBuffer.create(nullptr, triListAlloc * sizeof(PolyKey));

	// Three values per triangle.
	size_t vertAlloc = triListAlloc * 3;
	transPositionBuffer.create(nullptr, vertAlloc * sizeof(vec4));
	transNormalBuffer.create(nullptr, vertAlloc * sizeof(vec4));
	transTexCoordBuffer.create(nullptr, vertAlloc * sizeof(vec2));
	transMatIndexBuffer.create(nullptr, vertAlloc * sizeof(unsigned int));

	// Set buffer sizes in the profiler.
	app->getProfiler().setBufferSize("triBuffer", triListAlloc * sizeof(PolyKey));
	app->getProfiler().setBufferSize("posBuffer", vertAlloc * sizeof(vec4));
	app->getProfiler().setBufferSize("normBuffer", vertAlloc * sizeof(vec4));
	app->getProfiler().setBufferSize("texCoordBuffer", vertAlloc * sizeof(vec2));
	app->getProfiler().setBufferSize("matIndexBuffer", vertAlloc * sizeof(unsigned int));
}

bool TinySort::preTransform(App *app)
{
	// TODO: Don't use separate buffers for all the input data, vertex attributes in a vao will do fine.
	// TODO: Alternatively, we can feed in data that was subdivided in the previous frame to make future subdivision faster.
	// TODO: Although if we do that, there needs to be a way to account for missing geometry.
	// TODO: Alternative is to have a single vertex struct for sorting and rendering, then no id is necessary.
	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	glPatchParameteri(GL_PATCH_VERTICES, 3);

	unsigned int zero = 0;
	triListCount.bufferData(&zero, sizeof(zero));

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glEnable(GL_RASTERIZER_DISCARD);

	preTransformShader.bind();
	// Atomic counter needed for saving tessellated geometry.
	preTransformShader.setUniform("TriListCount", &triListCount);
	// Camera uniforms.
	app->setCameraUniforms(&preTransformShader); // TODO: Figure out which uniforms are actually needed and only set those.
	// Initial scene geometry data.
	preTransformShader.setUniform("Indices", &indexBuffer);
	preTransformShader.setUniform("Positions", &positionBuffer);
	preTransformShader.setUniform("Normals", &normalBuffer);
	preTransformShader.setUniform("TexCoords", &texCoordBuffer);
	preTransformShader.setUniform("MatIndices", &matIndexBuffer);
	preTransformShader.setUniform("lightDir", vec3(0, 0, 1)); // TODO: This is just here to debug the triangle rendering.
	// Geometry buffers for saving tessellated geometry.
	preTransformShader.setUniform("triListAlloc", triListAlloc);
	preTransformShader.setUniform("TriIndices", &triBuffer);
	preTransformShader.setUniform("TransPositions", &transPositionBuffer);
	preTransformShader.setUniform("TransNormals", &transNormalBuffer);
	preTransformShader.setUniform("TransTexCoords", &transTexCoordBuffer);
	preTransformShader.setUniform("TransMatIndices", &transMatIndexBuffer);
	// 3 verts per triangle, incoming data is pulled from the geometry storage buffers.
	glDrawArrays(GL_PATCHES, 0, nPolys * 3);
	preTransformShader.unbind();

	glDisable(GL_RASTERIZER_DISCARD);

	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	glPopAttrib();

	totalTris = *((unsigned int*) triListCount.read());

	bool resizeFlag = false;
	//if (totalTris > triListAlloc || totalTris < triListAlloc * VERT_UNDERALLOCATE)
	if (totalTris > triListAlloc)
	{
		triListAlloc = (unsigned int) (totalTris * VERT_OVERALLOCATE);
		resizeTransformBuffers(app);
		resizeFlag = true;
	}

	return resizeFlag;
}

void TinySort::resize(App *app, int width, int height)
{
	// TODO: Function probably not necessary.
	(void) (app);

	if (this->width == width && this->height == height)
		return;

	this->width = width;
	this->height = height;
}

void TinySort::beginCapture(App *app)
{
	// TODO: Function probably not necessary.
#if 0
	glEnable(GL_RASTERIZER_DISCARD);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	zeroShader.bind();
	//zeroShader.setUniform("PolyFlags", &polyFlagBuffer);
	glDrawArrays(GL_POINTS, 0, nPolys);
	zeroShader.unbind();
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
	glDisable(GL_RASTERIZER_DISCARD);
#endif
	glMemoryBarrier(GL_ATOMIC_COUNTER_BARRIER_BIT);
	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	(void) (app);

	//app->setCameraUniforms(&captureShader);
}

bool TinySort::endCapture(App *app)
{
	// TODO: Function probably not necessary.
	(void) (app);

	//captureShader.unbind();

	return false;
}

void TinySort::capture(App *app)
{
	// Create vertex buffers that will be needed for sorting.
	app->getProfiler().start("Capture");

	if (preTransform(app))
	{
		std::cout << "Resizing vertex data\n";
		if (preTransform(app))
			std::cout << "Something went really really wrong\n";
	}

	app->getProfiler().time("Capture");
}

void TinySort::sort(App *app)
{
	(void) (app);

	app->getProfiler().start("Sort");
#if CPU_SORT
	pCount = 0;
	lCount = 0;
	fCount = 0;

	#if 0
		// TODO: For now, just use std::sort/qsort, move the sorting to cuda after it's working.
		StorageBuffer tmpBuffer;

		// For the sort we need:
		// * polyIDs - the thing actually being sorted.
		// * esPositions - eye-space geometry.
		// * indices - connectivity of eye-space geometry.
		tmpBuffer.create(nullptr, triBuffer.getSize());
		triBuffer.copy(&tmpBuffer);
		unsigned int *polyIDs = (unsigned int*) tmpBuffer.read();

		tmpBuffer.release();
		tmpBuffer.create(nullptr, transPositionBuffer.getSize());
		transPositionBuffer.copy(&tmpBuffer);
		esPositions = (vec4*) tmpBuffer.read();

		tmpBuffer.release();
		tmpBuffer.create(nullptr, indexBuffer.getSize());
		indexBuffer.copy(&tmpBuffer);
		indices = (unsigned int*) tmpBuffer.read();

		qsort(&polyIDs[0], nPolys, sizeof(unsigned int), cmpfnc);
		std::cout << pCount << ", " << lCount << ", " << fCount << "\n";

		// Put the sorted polyIDs back in the triBuffer.
		triBuffer.bufferData(&polyIDs[0], nPolys * sizeof(unsigned int));
	#endif
#else
	// Cuda implementation here.
	static bool initFlag = false;

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	if (!initFlag)
	{
	#if 0
		destroyPolyKeysCudaResource();
		triBuffer.bind();
		createPolyKeysCudaResource(triBuffer.getObject());
		sortPolyKeysThrust(totalTris);
		triBuffer.unbind();
	#endif
		initFlag = true;
	}
	else
	{
	#if 0
		triBuffer.bind();
		sortPolyKeysThrust(totalTris);
		triBuffer.unbind();
	#endif
	}

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
#endif
	app->getProfiler().time("Sort");
}

void TinySort::renderBadPixels(App *app)
{
	// TODO: Need to enable early stencil test.
	// Now perform per-pixel oit for all pixels that failed.
	glPushAttrib(GL_ENABLE_BIT | GL_STENCIL_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	glStencilFunc(GL_EQUAL, 1, 1);

	app->captureGeometryLFB();
	app->sortGeometryLFB();

	glPopAttrib();
}

void TinySort::drawStencil()
{
	// See which pixels were ok and which ones weren't (render stencil buffer).
	glPushAttrib(GL_ENABLE_BIT | GL_STENCIL_BUFFER_BIT);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
	glStencilFunc(GL_EQUAL, 1, 1);

	glColor3f(1, 0, 0);
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	Renderer::instance().drawQuad();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
}

void TinySort::drawGrid()
{
	glPushAttrib(GL_TRANSFORM_BIT | GL_POLYGON_BIT | GL_ENABLE_BIT | GL_CURRENT_BIT);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glColor3f(1, 0, 0);

	float xStep = 2.0f / (float) width;
	float yStep = 2.0f / (float) height;

	glBegin(GL_LINES);

	for (float x = -1.0f; x <= 1.0f; x += xStep)
	{
		glVertex3f(x, -1, 0);
		glVertex3f(x, 1, 0);
	}
	for (float y = -1.0f; y <= 1.0f; y += yStep)
	{
		glVertex3f(-1, y, 0);
		glVertex3f(1, y, 0);
	}

	glEnd();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glPopAttrib();
}

void TinySort::drawTessellation()
{
	glPatchParameteri(GL_PATCH_VERTICES, 3);

	// Just try some basic tessellation for now and see what happens.
	static std::vector<vec4> positions = { vec4(-0.5f, -1, 0, 1), vec4(0, 1, 0, 1), vec4(0.5f, -1, 0, 1) };
	static VertexBuffer vbo(GL_PATCHES);
	static VertexFormat fmt({ {VertexAttrib::POSITION, 0, 4, GL_FLOAT} });
	if (!vbo.isGenerated())
		vbo.create(fmt, &positions[0].x, positions.size() * sizeof(vec4), sizeof(vec4));

	static Shader teShader;
	if (!teShader.isGenerated())
	{
		auto v = ShaderSourceCache::getShader("tessVert").loadFromFile("shaders/oit/tinysort/tess.vert");
		auto f = ShaderSourceCache::getShader("tessFrag").loadFromFile("shaders/oit/tinysort/tess.frag");
		auto tcs = ShaderSourceCache::getShader("tessTcs").loadFromFile("shaders/oit/tinysort/tess.tcs");
		auto tes = ShaderSourceCache::getShader("tessTes").loadFromFile("shaders/oit/tinysort/tess.tes");
		teShader.release();
		teShader.create(&v, &f, nullptr, &tcs, &tes);
	}

	mat4 m = mat4::getIdentity();

	glPushAttrib(GL_ENABLE_BIT | GL_POLYGON_BIT);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	vbo.enableAttribs(fmt);

	teShader.bind();
	teShader.setUniform("mvMatrix", m);
	teShader.setUniform("pMatrix", m);
	vbo.render();
	teShader.unbind();

	vbo.disableAttribs(fmt);

	glPopAttrib();
}


TinySort::~TinySort()
{
#if 0
	destroyPolyKeysCudaResource();
#endif
}

void TinySort::render(App *app)
{
	capture(app);
	sort(app);

#if 0
	StorageBuffer tmpBuffer;
	tmpBuffer.create(nullptr, triBuffer.getSize());
	triBuffer.copy(&tmpBuffer);
	PolyKey *polyIDs = (PolyKey*) tmpBuffer.read();
	for (size_t i = 0; i < totalTris; i++)
		std::cout << polyIDs[i].id << "," << polyIDs[i].z << "; ";
	std::cout << "\n";
	exit(0);
#endif

	glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);

	// Now render geometry in sorted polygon order.
	app->getProfiler().start("Render");

	auto &camera = Renderer::instance().getActiveCamera();

	glPushAttrib(GL_ENABLE_BIT | GL_STENCIL_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Setup the stencil mask so that any pixel with a failed depth test is marked.
	// TODO: One possibility is to setup the stencil mask first, and then render geometry (probably the best approach).
	glClear(GL_STENCIL_BUFFER_BIT);
	glEnable(GL_STENCIL_TEST);
	glStencilOp(GL_KEEP, GL_REPLACE, GL_KEEP);
	glStencilFunc(GL_ALWAYS, 1, 1);

	// Depth testing needs to be enabled so we can mark failed pixels.
	//glDisable(GL_DEPTH_TEST);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL); // FIXME: Still doesn't seem to be sufficient.
	glDisable(GL_CULL_FACE); // FIXME: May want back face culling enabled, not sure.
	//glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// TODO: Bind the 2d texture array.
	// FIXME: This is slooooooooow.
	//diffuseTextures.bind();

	compositeShader.bind();
	// TODO: Send the material data.
	//compositeShader.setUniform("diffuseTex", &diffuseTextures);
	compositeShader.setUniform("MatIndices", &transMatIndexBuffer);
	compositeShader.setUniform("MatAmbient", &ambientBuffer);
	compositeShader.setUniform("MatDiffuse", &diffuseBuffer);
	compositeShader.setUniform("MatSpecular", &specularBuffer);
	compositeShader.setUniform("MatShininess", &shininessBuffer);
	compositeShader.setUniform("MatTexFlag", &texFlagBuffer);
	// Send all the other vertex data.
	compositeShader.setUniform("TriIndices", &triBuffer);
	compositeShader.setUniform("Positions", &transPositionBuffer);
	compositeShader.setUniform("Normals", &transNormalBuffer);
	compositeShader.setUniform("TexCoords", &transTexCoordBuffer);
	// Finally send the camera data.
	compositeShader.setUniform("mvMatrix", camera.getInverse());
	compositeShader.setUniform("pMatrix", camera.getProjection());
	compositeShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	compositeShader.setUniform("lightDir", vec3(0, 0, 1));
	glDrawArrays(GL_POINTS, 0, totalTris);
	compositeShader.unbind();

	//diffuseTextures.unbind();

	glPopAttrib();

	app->getProfiler().time("Render");

	//glPushAttrib(GL_ENABLE_BIT);
	//glEnable(GL_CULL_FACE);
	//renderBadPixels(app);
	//glPopAttrib();

	drawStencil();
}

void TinySort::update(float dt)
{
	(void) (dt);
}

void TinySort::useLFB(App *app, Rendering::LFB::LFBType type)
{
	(void) (app);
	(void) (type);

	if (opaqueFlag == 0)
	{
		auto vertPre = ShaderSourceCache::getShader("preTransVert").loadFromFile("shaders/oit/tinysort/preTransform.vert");
		auto geomPre = ShaderSourceCache::getShader("preTransGeom").loadFromFile("shaders/oit/tinysort/preTransform.geom");
		auto fragPre = ShaderSourceCache::getShader("preTransFrag").loadFromFile("shaders/oit/tinysort/preTransform.frag");
		auto tcsPre = ShaderSourceCache::getShader("preTransTcs").loadFromFile("shaders/oit/tinysort/preTransform.tcs");
		auto tesPre = ShaderSourceCache::getShader("preTransTes").loadFromFile("shaders/oit/tinysort/preTransform.tes");

		preTransformShader.release();
		preTransformShader.create(&vertPre, &fragPre, &geomPre, &tcsPre, &tesPre);

		auto vertComp = ShaderSourceCache::getShader("lowCompVert").loadFromFile("shaders/oit/tinysort/composite.vert");
		auto geomComp = ShaderSourceCache::getShader("lowCompGeom").loadFromFile("shaders/oit/tinysort/composite.geom");
		auto fragComp = ShaderSourceCache::getShader("lowCompFrag").loadFromFile("shaders/oit/tinysort/composite.frag");

		compositeShader.release();
		compositeShader.create(&vertComp, &fragComp, &geomComp);
	}

	nPolys = (unsigned int) app->getGpuMesh().getVao().getIndexBuffer()->getNIndices() / 3;
	nVerts = (unsigned int) app->getGpuMesh().getVao().getVertexBuffer()->getNVerts();

	auto zeroVert = ShaderSource("zeroFlags", getZeroShaderSrc());
	zeroShader.release();
	zeroShader.create(&zeroVert);

	// Create the index and vertex data buffers.
	buildVertexBuffers(app);

	unsigned int zero = 0;
	triListCount.release();
	triListCount.create(&zero, sizeof(zero));

	resizeTransformBuffers(app);
}

void TinySort::saveLFB(App *app)
{
	(void) (app);
#if 0
	unsigned int *counts = (unsigned int*) pixelCountBuffer.read();
	std::stringstream ss;
	unsigned int max = 0;
	for (unsigned int i = 0; i < nPolys; i++)
		if (counts[i] > max)
			max = counts[i];
	ss << "max: " << max << "\n";
	for (unsigned int i = 0; i < nPolys; i++)
		ss << i << ": " << counts[i] << "\n";
	Utils::File f("counts.txt");
	f.write(ss.str());
	//app->saveGeometryLFB();
#endif
}

