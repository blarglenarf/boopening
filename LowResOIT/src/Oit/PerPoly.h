#pragma once

#include "Oit.h"

class PerPoly : public Oit
{
private:
	Rendering::Shader captureShader;
	Rendering::Shader compositeShader;
	Rendering::Shader zeroShader;

	Rendering::LFB polyLFB;

	unsigned int nPolys;
	Rendering::StorageBuffer polySortBuffer;
	Rendering::StorageBuffer esPositionBuffer;

	Rendering::StorageBuffer indexData;
	Rendering::StorageBuffer positionBuffer;
	Rendering::StorageBuffer normalBuffer;
	Rendering::StorageBuffer texCoordBuffer;

	Rendering::StorageBuffer matIndexBuffer;
	Rendering::StorageBuffer ambientBuffer;
	Rendering::StorageBuffer diffuseBuffer;
	Rendering::StorageBuffer specularBuffer;
	Rendering::StorageBuffer shininessBuffer;
	Rendering::StorageBuffer texFlagBuffer;
	Rendering::Texture2DArray diffuseTextures;

	int width;
	int height;

private:
	void buildMaterialBuffers(App *app);
	void buildVertexBuffers(App *app);

	void capture(App *app);
	void sort(App *app);

	void drawGrid();

public:
	PerPoly(int opaqueFlag = 0) : Oit(opaqueFlag), width(0), height(0) {}
	virtual ~PerPoly();

	virtual void init() override {}

	virtual void render(App *app) override;
	virtual void update(float dt) override;

	virtual void useLFB(App *app, Rendering::LFB::LFBType type) override;

	virtual void saveLFB(App *app) override;
};

