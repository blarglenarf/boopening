#include "App.h"

#include "Oit/Oit.h"
#include "Oit/Standard.h"
#include "Oit/Bucket.h"
#include "Oit/LowRes.h"
#include "Oit/PerPoly.h"
#include "Oit/FullSort.h"
#include "Oit/TinySort.h"
#include "Oit/TileSort.h"

#include "../../Renderer/src/RendererCommon.h"
#include "../../Math/src/MathCommon.h"
#include "../../Platform/src/PlatformCommon.h"
#include "../../Resources/src/ResourcesCommon.h"

#include "../../Utils/src/Random.h"
#include "../../Utils/src/UtilsGeneral.h"
#include "../../Utils/src/File.h"
#include "../../Utils/src/StringUtils.h"

#include <sstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <ctime>
#include <iomanip>


// TODO:
//
// Tesselated Sorting:
// DONE: Save triangles to a storage buffer (just one large list I guess).
// DONE: Sort the new triangle index buffer data, and render based on that.
// DONE: Triangles that are too small (sub-pixel) can be ignored.
// DONE: Tesselate incoming geometry so that triangles too large are decomposed to a given resolution size.
// DONE: Sort tesselated triangles based on their min z eye-space z values (start with sorting on the CPU).
// DONE: Mark pixels that failed so they can be sorted normally using per-pixel OIT.
// TODO: Try combining the grid approach with the sorting/visibility approach.
// TODO: Make sure that only failed pixels are being captured in the fixing pass (measure LFB memory usage perhaps).
// TODO: Try building the triangle list in two passes, so tessellation is only performed on a subset of triangles (may be no difference).
// TODO: Consider using transform feedback instead of manually saving in the geometry shader.
// TODO: Sort using line-triangle intersection test between triangle pairs.
//
// Polygon Sorting:
// DONE: First transform geometry into eye-space and put that in a storage buffer.
// DONE: Start by just sorting all the triangles in the scene, and perform a line-triangle intersection test between each triangle pair during sort.
// TODO: If that turns out to be too slow for all triangles, capture a list of triangle id's for all visible geometry and sort that.
// TODO: If it's still too slow, determine coarse xy intersections with low-res rasterization, either as per-polygon lists or with prime numbers.
// TODO: If it's still too slow, determine coarse z intersections by comparing min/max eye-space depths.
// TODO: Now fix any artifacts by marking incorrect pixels and doing per-pixel OIT on these.
//
// Bucketed OIT:
// Need a version of BMA specifically for clustered geometry.
// Instead of a fragment list per pixel, use 32 lists per pixel (gridded up).
// For BMA, could just use the max grid cell count per pixel, or use 32 different stencil masks (maybe).
// Sort per grid cell.
// Either save back to global memory and composite, or composite during sort (requires threads to wait until previous grid cells are finished).
//
// Better Bucketed OIT:
// Get per-pixel counts first.
// Grid divisions based on per-pixel counts instead of just 32 per pixel.
//
// Low-Res Bucketed OIT:
// Render scene at low resolution using conservative rasterization.
// Use 3d gridded approach to keep list sizes small I guess.
// Save polygon ids per tile?
// Sort them by depth (but how? now they cover a depth range not discrete values).
// Re-render scene per tile at full res in polygon depth order (but what about cross-over points?).
// Don't save or sort, just composite.
// Will almost certainly have artifacts where polygons cross over.
//
//
// Powerplant view:
// type: PERSPECTIVE
// pos: -4.9,3.85,0
// euler rot: -0.0261794,1.56207,0
// quat rot: -0.00921509, 0.703955, -0.00929585, 0.710124
// zoom: 7.22657
// fov/near/far: 1.309, 0.01, 30
// viewport: 0,0,1920,1016


using namespace Math;
using namespace Rendering;

// TODO: Get rid of this!
std::string currOitStr;
Animation anim;
Font testFont;
float tpf = 0;


// TODO: Keep it simple for now, add more frags later.
#define MAX_FRAGS 512
#define USE_BMA 1



App::App() : currLFB(1), currMesh(0), opaqueFlag(0), oitType(Oit::TILE_SORT), currOit(nullptr), width(0), height(0), tmpWidth(0), tmpHeight(0)
{
}

void App::init()
{
	auto &camera = Renderer::instance().getActiveCamera();

#if 0
	#if 1
		camera.setZoom(6.68847f);
		camera.setEulerRot(vec3(-0.209439f, 0, 0));
	#else
		camera.setZoom(6.97035);
		camera.setEulerRot(vec3(-0.0523594,0.855212,0));
	#endif
#endif

	auto &interpolator = anim.addInterpolator(const_cast<vec3*>(&camera.getEulerRot()), 10, vec3(-0.0523594,0.855212,0));
	interpolator.addKeyFrame(0, vec3(-0.0523594,0.855212,0));
	interpolator.addKeyFrame(10, vec3(-0.061086,-0.715585,0));

#if 0
	anim.setFilename("animations/anim.jpg");
#endif

	// TODO: Get rid of this!
	testFont.setName("testFont");
	Resources::FontLoader::load(&testFont, "fonts/ttf-bitstream-vera-1.10/VeraMono.ttf");
	testFont.setSize(18);
	testFont.storeInAtlas("abcdefghijklmnopqrstuvwxyzABCDEFGHIJLKMNOPQRSTUVWXYZ0123456789`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?");

	int maxLayers = 0;
	glGetIntegerv(GL_MAX_ARRAY_TEXTURE_LAYERS, &maxLayers);
	std::cout << "Max texture layers: " << maxLayers << "\n";

	loadNextMesh();
	useLFB();

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glColor3f(1, 1, 1);

	auto vertBasic = ShaderSourceCache::getShader("basicVert").loadFromFile("shaders/basic.vert");
	auto geomBasic = ShaderSourceCache::getShader("basicGeom").loadFromFile("shaders/basic.geom");
	auto fragBasic = ShaderSourceCache::getShader("basicFrag").loadFromFile("shaders/basic.frag");
	basicShader.create(&vertBasic, &fragBasic, &geomBasic);

	//std::cout << basicShader.getBinary() << "\n";
}

void App::setTmpViewport(int width, int height)
{
	auto &camera = Renderer::instance().getActiveCamera();

	tmpWidth = camera.getWidth();
	tmpHeight = camera.getHeight();

	camera.setViewport(0, 0, width, height);
	camera.uploadViewport();
	camera.update(0);
}

void App::restoreViewport()
{
	auto &camera = Renderer::instance().getActiveCamera();

	camera.setViewport(0, 0, tmpWidth, tmpHeight);
	camera.uploadViewport();
	camera.update(0);
}

void App::setCameraUniforms(Shader *shader)
{
	auto &camera = Renderer::instance().getActiveCamera();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
	shader->setUniform("viewport", camera.getViewport());
	shader->setUniform("invPMatrix", camera.getInverseProj());
}

void App::captureGeometryLFB(Shader *shader)
{
	if (!shader)
		shader = &captureGeometryShader;

	profiler.start("Geometry Capture");

	auto &camera = Renderer::instance().getActiveCamera();

	if (opaqueFlag == 0)
		geometryLFB.resize(camera.getWidth(), camera.getHeight());

	if (opaqueFlag == 0 && geometryLFB.getType() == LFB::LINEARIZED)
	{
		// Count the number of frags for capture.
		auto &countShader = geometryLFB.beginCountFrags();
		countShader.bind();
		geometryLFB.setCountUniforms(&countShader);
		countShader.setUniform("mvMatrix", camera.getInverse());
		countShader.setUniform("pMatrix", camera.getProjection());
		gpuMesh.render(false);
		countShader.unbind();
		geometryLFB.endCountFrags();
	}

	if (opaqueFlag == 0)
		geometryLFB.beginCapture();

	shader->bind();

	shader->setUniform("mvMatrix", camera.getInverse());
	shader->setUniform("pMatrix", camera.getProjection());
	shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());

	if (opaqueFlag == 0)
		geometryLFB.setUniforms(shader);

	// Capture fragments into the lfb.
	gpuMesh.render();
	shader->unbind();

	// Sometimes may need to re-render if the lfb allocated too much/too little.
	if (opaqueFlag == 0 && geometryLFB.endCapture())
	{
		geometryLFB.beginCapture();
		shader->bind();
		shader->setUniform("mvMatrix", camera.getInverse());
		shader->setUniform("pMatrix", camera.getProjection());
		shader->setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		geometryLFB.setUniforms(shader);
		gpuMesh.render();
		shader->unbind();

		if (geometryLFB.endCapture())
			std::cout << "Something very very wrong just happened...\n";
	}

	// TODO: Profile memory usage perhaps?

	profiler.time("Geometry Capture");
}

void App::sortGeometryLFB(bool composite)
{
	if (opaqueFlag == 1)
		return;

	profiler.start("Geometry Sort");

	// Sort using bma+rbs, and write the result back into the lfb.
#if USE_BMA
	// Sort and composite using bma.
	if (composite)
	{
		geometryLocalBMA.createMask(&geometryLFB);
		geometryLocalBMA.sort(&geometryLFB);
	}
	else
	{
		geometryBMA.createMask(&geometryLFB);
		geometryBMA.sort(&geometryLFB);
	}
#else
	// Sort and composite normally.
	if (composite)
	{
		sortGeometryLocalShader.bind();
		geometryLFB.composite(&sortGeometryLocalShader); // TODO: change this to a sort method.
		sortGeometryLocalShader.unbind();
	}
	else
	{
		sortGeometryShader.bind();
		geometryLFB.composite(&sortGeometryShader); // TODO: change this to a sort method.
		sortGeometryShader.unbind();
	}
#endif

	profiler.time("Geometry Sort");
}

void App::render()
{
	static float fpsUpdate = 0;

#if 0
	testFont.blit();
	return;
#endif

	profiler.begin();

	profiler.start("Total");

	if (currOit)
	{
		currOit->render(this);
	}
	else
	{
		auto &camera = Renderer::instance().getActiveCamera();

		// Opaque Rendering.
		glPushAttrib(GL_ENABLE_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		basicShader.bind();
		basicShader.setUniform("mvMatrix", camera.getInverse());
		basicShader.setUniform("pMatrix", camera.getProjection());
		basicShader.setUniform("nMatrix", camera.getTransform().getMat3().transpose());
		basicShader.setUniform("lightDir", vec3(0, 0, 1));
		gpuMesh.render();
		basicShader.unbind();

		glPopAttrib();
	}

	profiler.time("Total");

#if 1
	float total = profiler.getTime("Total");
	float smoothing = 0.9f; // Larger => more smoothing.
	tpf = (tpf * smoothing) + (total * (1.0f - smoothing));
	float t = round(total);

	testFont.renderFromAtlas("Technique: " + currOitStr +
		"\nTime per frame (ms): " + Utils::toString(t) +
		"\nFrames per second (fps): " + Utils::toString(1000.0 / t), 10, 10, 0, 0, 1);
#endif

	float dt = Platform::Window::getDeltaTime();
	fpsUpdate += dt;
	if (fpsUpdate > 2)
	{
		profiler.print();
		fpsUpdate = 0;
	}

#if 1
	Renderer::instance().drawAxes();
#endif
}

void App::update(float dt)
{
	if (currOit)
		currOit->update(dt);

	if (anim.isAnimating())
	{
		anim.update(dt);
		Renderer::instance().getActiveCamera().setDirty();
	}
}

void App::runBenchmarks()
{
}

void App::useMesh()
{
	static std::vector<std::string> meshes = { "meshes/quad.obj", "meshes/sponza/sponza.3ds", "meshes/galleon.obj", "meshes/teapot/teapot.obj", "meshes/hairball.ctm", "meshes/powerplant/powerplant.ctm", };

	if (currMesh > (int) meshes.size() - 1)
		currMesh = 0;
	else if (currMesh < 0)
		currMesh = (int) meshes.size() - 1;

	gpuMesh.release();

	std::cout << "Loading mesh: " << meshes[currMesh] << "\n";

	mesh.clear();
	if (Resources::MeshLoader::load(&mesh, meshes[currMesh]))
	{
		if (meshes[currMesh] == "meshes/powerplant/powerplant.ctm")
		{
			mesh.scale(vec3(0.0001f, 0.0001f, 0.0001f));
			mesh.translate(vec3(7.5, -10, -6)); // Move the mesh so it's reasonably well centered.
		}
		else if (meshes[currMesh] == "meshes/hairball.ctm")
			mesh.scale(vec3(-10, -10, -10), vec3(10, 10, 10));
		else if (meshes[currMesh] == "meshes/sponza/sponza.3ds")
			mesh.scale(vec3(-10, -5, -10), vec3(10.0f, 5.0f, 10.0f));
		else if (meshes[currMesh] == "meshes/galleon.obj")
			mesh.scale(vec3(0.2, 0.2, 0.2));
		else if (meshes[currMesh] == "meshes/teapot/teapot.obj")
			mesh.scale(vec3(2.0, 2.0, 2.0));
		//else if (meshes[currMesh] == "meshes/quad.obj")
		//	mesh.scale(vec3(10, 10, 10));
		gpuMesh.create(&mesh);
	}

	useLFB();
}

void App::useLFB()
{
	profiler.clear();

#if USE_LINK_PAGES
	static std::vector<LFB::LFBType> types = { LFB::LINK_PAGES, LFB::LINK_LIST, LFB::LINEARIZED };
	static std::vector<std::string> typeStrs = { "Link Pages", "Link List", "Linearized" };
#else
	#if 1
		static std::vector<LFB::LFBType> types = { LFB::LINEARIZED, LFB::LINK_LIST };
		static std::vector<std::string> typeStrs = { "Linearized", "Link List" };
	#else
		static std::vector<LFB::LFBType> types = { LFB::LINK_LIST, LFB::LINEARIZED };
		static std::vector<std::string> typeStrs = { "Link List", "Linearized" };
	#endif
#endif

	//geometryLFB.release();

	delete currOit;
	currOit = nullptr;

	StorageBuffer::clearBufferGroup();
	UniformBuffer::clearBufferGroup();
	AtomicBuffer::clearBufferGroup();
	Texture::clearTextureGroup();

	if (currLFB > (int) types.size() - 1)
		currLFB = 0;
	else if (currLFB < 0)
		currLFB = (int) types.size() - 1;
	LFB::LFBType type = types[currLFB];

	std::cout << "Using LFB: " << typeStrs[currLFB] << "\n";
	currOitStr = "Standard";

	if (opaqueFlag == 0)
	{
		geometryLFB.setType(type, "lfb");
		geometryLFB.setMaxFrags(MAX_FRAGS);
		geometryLFB.setProfiler(&profiler);
	}

	auto vertCapture = ShaderSourceCache::getShader("captureVert").loadFromFile("shaders/oit/capture.vert");
	auto fragCapture = ShaderSourceCache::getShader("captureFrag").loadFromFile("shaders/oit/capture.frag");
	fragCapture.setDefine("OPAQUE", Utils::toString(opaqueFlag));

	captureGeometryShader.release();
	captureGeometryShader.create(&vertCapture, &fragCapture);

	if (opaqueFlag == 0)
	{
		// Sorts and writes to global memory.
		auto vertSort = ShaderSourceCache::getShader("sortVert").loadFromFile("shaders/sort/sort.vert");
		auto fragSort = ShaderSourceCache::getShader("sortFrag").loadFromFile("shaders/sort/sort.frag");
		fragSort.setDefine("MAX_FRAGS", Utils::toString(geometryLFB.getMaxFrags()));
		fragSort.setDefine("COMPOSITE_LOCAL", "0");
		fragSort.replace("LFB_NAME", "lfb");

		sortGeometryShader.release();
		sortGeometryShader.create(&vertSort, &fragSort);

		// Sorts and composites.
		auto vertLocalSort = ShaderSourceCache::getShader("sortLocalVert").loadFromFile("shaders/sort/sort.vert");
		auto fragLocalSort = ShaderSourceCache::getShader("sortLocalFrag").loadFromFile("shaders/sort/sort.frag");
		fragLocalSort.setDefine("MAX_FRAGS", Utils::toString(geometryLFB.getMaxFrags()));
		fragLocalSort.setDefine("COMPOSITE_LOCAL", "1");
		fragLocalSort.replace("LFB_NAME", "lfb");

		sortGeometryLocalShader.release();
		sortGeometryLocalShader.create(&vertLocalSort, &fragLocalSort);

		// BMA shaders that write to global memory.
		geometryBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
		geometryBMA.createShaders(&geometryLFB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "0"}});

		// BMA shaders that sort and composite.
		geometryLocalBMA.createMaskShader("lfb", "shaders/sort/bmaMask.vert", "shaders/sort/bmaMask.frag");
		geometryLocalBMA.createShaders(&geometryLFB, "shaders/sort/sort.vert", "shaders/sort/sort.frag", {{"COMPOSITE_LOCAL", "1"}});
	}

	switch (oitType)
	{
	case Oit::STANDARD:
		currOit = new Standard(opaqueFlag);
		break;

	case Oit::BUCKET:
		currOit = new Bucket(opaqueFlag);
		break;

	case Oit::LOW_RES:
		currOit = new LowRes(opaqueFlag);
		break;

	case Oit::PER_POLY:
		currOit = new PerPoly(opaqueFlag);
		break;

	case Oit::FULL_SORT:
		currOit = new FullSort(opaqueFlag);
		break;

	case Oit::TINY_SORT:
		currOit = new TinySort(opaqueFlag);
		break;

	case Oit::TILE_SORT:
		currOit = new TileSort(opaqueFlag);
		break;

	default:
		currOit = new Standard(opaqueFlag);
		break;
	}

	if (currOit)
	{
		currOit->init();
		currOit->useLFB(this, type);
	}
}

void App::toggleTransparency()
{
	opaqueFlag = opaqueFlag == 0 ? 1 : 0;
	useLFB();
}

void App::loadNextMesh()
{
	currMesh++;
	useMesh();
}

void App::loadPrevMesh()
{
	currMesh--;
	useMesh();
}

void App::useNextLFB()
{
	currLFB++;
	useLFB();
}

void App::usePrevLFB()
{
	currLFB--;
	useLFB();
}

void App::playAnim()
{
	anim.play();
}

void App::saveGeometryLFB()
{
	if (opaqueFlag == 1)
		return;
	//auto s = geometryLFB.getStrSorted();
	auto s = geometryLFB.getStr();
	Utils::File f("lfb.txt");
	f.write(s);
}

void App::saveLFBData()
{
	currOit->saveLFB(this);
}

