#pragma once

#define GRID_CELL_SIZE 16
#define CLUSTERS 32
#define MAX_EYE_Z 30.0
#define MASK_SIZE (CLUSTERS / 32)

#include "Oit/Oit.h"

#include "../../Renderer/src/LFB/LFB.h"
#include "../../Renderer/src/LFB/Cluster.h"
#include "../../Renderer/src/LFB/BMA.h"
#include "../../Renderer/src/RenderObject/GPUBuffer.h"
#include "../../Renderer/src/RenderObject/Shader.h"
#include "../../Renderer/src/RenderObject/Texture.h"
#include "../../Renderer/src/RenderResource/Mesh.h"
#include "../../Renderer/src/Profiler.h"

#include "../../Math/src/Vector/Vec4.h"

#include <vector>

class App
{
private:
	int currLFB;
	int currMesh;

	int opaqueFlag;

	Oit::OitType oitType;
	Oit *currOit;

	Rendering::LFB geometryLFB;
	Rendering::BMA geometryBMA;
	Rendering::BMA geometryLocalBMA;

	Rendering::Shader basicShader;
	Rendering::Shader sortGeometryShader;
	Rendering::Shader sortGeometryLocalShader;
	Rendering::Shader captureGeometryShader;

	Rendering::Mesh mesh;
	Rendering::GPUMesh gpuMesh;

	Rendering::Profiler profiler;

	int width;
	int height;

	int tmpWidth;
	int tmpHeight;

public:
	App();
	~App() {}

	void init();

	void setTmpViewport(int width, int height);
	void restoreViewport();

	void setCameraUniforms(Rendering::Shader *shader);

	void captureGeometryLFB(Rendering::Shader *shader = nullptr);
	void sortGeometryLFB(bool composite = true);

	void render();
	void update(float dt);

	void runBenchmarks();

	void useMesh();
	void useLFB();

	void toggleTransparency();

	void loadNextMesh();
	void loadPrevMesh();

	void useNextLFB();
	void usePrevLFB();

	void playAnim();

	void saveGeometryLFB();
	void saveLFBData();

	Rendering::LFB &getGeometryLfb() { return geometryLFB; }
	Rendering::Mesh &getMesh() { return mesh; }
	Rendering::GPUMesh &getGpuMesh() { return gpuMesh; }
	Rendering::Shader &getBasicShader() { return basicShader; }

	Rendering::Profiler &getProfiler() { return profiler; }
};

