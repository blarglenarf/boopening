import sys
import random


# TODO: All the stuff that's necessary for an interleaved lfb.
# DONE: Create a bunch of per-pixel lists.
# DONE: Get per-pixel counts.
# DONE: Split into groups of n (start with 2 I guess, but move up to 32).
# DONE: Get list size per group (per-group counts).
# DONE: Convert per-pixel counts and per-group counts to offsets.
# DONE: Could probably get per-group offsets based on the per-pixel offsets.
# DONE: Need to know per-group offsets.
# DONE: Once that's done, interleaving data should be easy if pixel counts are all the same.
# DONE: Allow iterating through per-pixel lists using the interleaved data.
# DONE: Allow random list sizes and calculate min size per list.
# DONE: Interleave data per-group by min list size.
# DONE: Now have lists be different sizes.
# DONE: Write the rest of the data non-interleaved.
# DONE: Read the data per-pixel in-order and make sure it's correct.
# DONE: Come up with a way that doesn't require an extra buffer/pass.
# DONE: GroupOffsets may not be necessary, we can probably just get these from the offsets array.
# TODO: Don't use complete offsets, just have groupOffsets and counts.


def genList(size):
	l = [random.randint(0, 10) for i in range(size)]
	#l = [i for i in range(size)]
	return l


def genLists(nLists = 4):
	lists = []
	for i in xrange(nLists):
		#size = 4
		size = random.randint(4, 32)
		lists += [genList(size)]
	return lists


def getPerPixelCounts(lists):
	counts = [0] * len(lists)
	for i in xrange(len(lists)):
		counts[i] = len(lists[i])
	return counts


def getPerGroupCounts(counts, groupSize = 2):
	groupCounts = [0] * (len(counts) / groupSize)
	for i in xrange(len(counts) / groupSize):
		start = i * groupSize
		count = counts[start]
		for j in xrange(groupSize):
			index = start + j
			if counts[index] < count:
				count = counts[index]
		groupCounts[i] = count
	return groupCounts


def getMinCounts(counts, groupSize = 2, blockSize = 2):
	minGroupCounts = [1024] * (len(counts) / groupSize)
	for i in xrange(len(counts) / groupSize):
		for j in xrange(groupSize):
			index = i * groupSize + j
			#if counts[index] < minGroupCounts[i]:
			#	minGroupCounts[i] = counts[index]
			# Needs to be rounded down to the nearest blockSize.
			rounded = counts[index] - (counts[index] % blockSize)
			if rounded < minGroupCounts[i]:
				minGroupCounts[i] = rounded
	return minGroupCounts


def getPerPixelOffsets(counts):
	offsets = [0] * len(counts)
	currCount = 0
	for i in xrange(len(counts)):
		offsets[i] = currCount
		currCount += counts[i]
	return offsets


def getPerPixelAdjustedOffsets(counts, minCounts, groupOffsets, groupSize = 2):
	offsets = [0] * len(counts)
	for i in xrange(len(minCounts)):
		currCount = 0
		for j in xrange(groupSize):
			index = i * groupSize + j
			offsets[index] = currCount
			pixCount = counts[index] - minCounts[i]
			currCount += pixCount
	return offsets


def calcAdjustedOffsets(adjustedOffsets, counts, minCounts, offsets, groupOffsets, groupSize = 2):
	print 'expected:', adjustedOffsets
	print 'got:',
	for pixel in xrange(len(offsets)):
		run = pixel % groupSize
		group = pixel / groupSize
		#groupOffset = groupOffsets[group]
		groupOffset = offsets[group * groupSize]
		minCount = minCounts[group]
		minFrags = minCount * groupSize
		startOffset = groupOffset + minFrags
		print 'run:', run, 'group:', group, 'groupOffset:', groupOffset, 'minCount:', minCount, 'minFrags:', minFrags, 'startOffset:', startOffset, 'offset:', offsets[pixel], 'expected:', adjustedOffsets[pixel],
		print 'maybe?', offsets[pixel] - groupOffset - minCount * run
	print ''


def getPerGroupOffsets(offsets, groupSize = 2):
	groupOffsets = [0] * (len(offsets) / groupSize)
	for i in xrange(len(groupOffsets)):
		groupOffsets[i] = offsets[i * groupSize]
	return groupOffsets


def getInterleavedData(lists, counts, offsets, adjustedOffsets, minCounts, groupOffsets, groupSize = 2, blockSize = 2):
	newCounts = [0] * len(counts)
	nFrags = 0
	for c in counts:
		nFrags += c
	fragData = [0] * nFrags
	for pixel in xrange(len(lists)):
		for fragIndex in xrange(len(lists[pixel])):
			fragment = lists[pixel][fragIndex]
			run = pixel % groupSize
			group = pixel / groupSize
			#groupOffset = groupOffsets[group]
			groupOffset = offsets[group * groupSize]
			minCount = minCounts[group]
			# For now, just don't write any data that is past the minCount.
			# In glsl, atomically increment the count, then do the check (eg, if count < minCount).
			currCount = newCounts[pixel]
			newCounts[pixel] += 1
			if currCount < minCount:
				# Interleave if possible (based on blockSize).
				#index = groupOffset + run + groupSize * currCount
				blockCount = currCount / blockSize
				index = groupOffset + blockCount * groupSize * blockSize + run * blockSize + currCount % blockSize
				fragData[index] = fragment
			else:
				# Don't interleave this data, instead just write the value at the appropriate location.
				adjustedOffset = offsets[pixel] - groupOffset - minCount * run
				#adjustedOffset = adjustedOffsets[pixel]
				index = groupOffset + minCount * groupSize + adjustedOffset + (currCount - minCount)
				fragData[index] = fragment
	return fragData


def traversePixelData(pixel, fragData, counts, offsets, adjustedOffsets, minCounts, groupOffsets, groupSize = 2, blockSize = 2):
	run = pixel % groupSize
	group = pixel / groupSize
	#groupOffset = groupOffsets[group]
	groupOffset = offsets[group * groupSize]
	minCount = minCounts[group]
	#offset = adjustedOffsets[pixel]
	fragList = []
	#print pixel, offset, ':',
	for i in xrange(counts[pixel]):
		index = 0
		if i < minCount:
			#index = groupOffset + run + groupSize * i
			blockCount = i / blockSize
			index = groupOffset + blockCount * groupSize * blockSize + run * blockSize + i % blockSize
		else:
			adjustedOffset = offsets[pixel] - minCount * run
			#adjustedOffset = adjustedOffsets[pixel]
			index = minCount * groupSize + adjustedOffset + i - minCount
		#	print '\n', groupOffset, minCount * groupSize, offset, i - minCount + run, minCount,
		#	index = minCount * groupSize + offset + i - minCount
		#	print index
		fragment = fragData[index]
		fragList += [fragment]
		#print fragment,
	#print ''
	return fragList




groupSize = 32
blockSize = 2
nLists = 64

lists = genLists(nLists)

# For testing...
#lists = [[7, 4, 1, 2], [10, 1, 1, 1, 2, 10, 4], [6, 3, 2, 1, 6, 10, 1], [2, 1, 8, 6, 9, 0, 10]]
#lists = [[5, 6, 3, 8, 9], [0, 5, 8, 7], [9, 7, 4, 10, 4, 10, 9, 1], [7, 10, 0, 1, 1, 9, 0, 10]]
#lists = [[3, 3, 8, 3, 8, 2], [0, 9, 6, 2, 10, 6], [10, 0, 7, 7, 1], [8, 1, 8, 1]]
#lists = [[5, 6, 3, 8, 9], [0, 5, 8, 7], [9, 7, 4, 10, 4, 10, 9, 1], [7, 10, 0, 1, 1, 9]]

# Moar testing.
#lists = [[4, 6, 2, 0], [1, 8, 9, 0, 10], [9, 9, 1, 1, 8], [2, 5, 1, 10, 1, 0, 0, 7], [5, 3, 0, 5, 0, 0, 0], [9, 6, 7, 2, 8], [0, 10, 0, 4, 8], [1, 9, 2, 2]]

#lists = [[3, 6, 5, 1], [10, 1, 0, 5]]

print lists

counts = getPerPixelCounts(lists)
#groupCounts = getPerGroupCounts(counts, groupSize)
minCounts = getMinCounts(counts, groupSize, blockSize)

offsets = getPerPixelOffsets(counts)
groupOffsets = getPerGroupOffsets(offsets, groupSize)

adjustedOffsets = getPerPixelAdjustedOffsets(counts, minCounts, groupOffsets, groupSize)

#calcAdjustedOffsets(adjustedOffsets, counts, minCounts, offsets, groupOffsets, groupSize)

fragData = getInterleavedData(lists, counts, offsets, adjustedOffsets, minCounts, groupOffsets, groupSize, blockSize)


success = True
for i in xrange(len(lists)):
	fragList = traversePixelData(i, fragData, counts, offsets, adjustedOffsets, minCounts, groupOffsets, groupSize, blockSize)
	if fragList != lists[i]:
		success = False
		print 'Failed!'
		print 'expected:', lists[i]
		print 'got:', fragList
if success:
	print 'Passed!'


#print lists
#print 'groupCounts:', groupCounts

#print 'groupSize:', groupSize
#print 'counts:', counts
#print 'minCounts:', minCounts
#print 'offsets:', offsets
#print 'adjustedOffsets:', adjustedOffsets
#print 'groupOffsets:', groupOffsets
#print fragData

