def apply_lighting(frag, lights, minLight, maxLight):
	print 'Applying lighting to frag ', frag, ' from lights: ',
	for i in range(minLight, maxLight):
		lightID = lights[i]
		print lightID,
	print ''

def apply_constant_lighting(frag, lights, minLight, maxLight):
	print 'Applying lighting to frag ', frag, ' from lights: ',
	for i in range(minLight, maxLight):
		lightFrag, lightID = lights[i]
		print lightID,
	print ''



def test_constant_stepping(frags, lights, reverse):
	nFrags = len(frags)
	nLights = len(lights)
	lightSize = 0.75

	# Set in order from 0 to nLights, same for frags.
	minLight = 0
	maxLight = 0

	for i in range(nFrags):
		currFrag = i

		# Increase max light if necessary.
		if maxLight < nLights:
			lightFrag, lightID = lights[maxLight]
			if reverse == True:
				while maxLight < nLights and frags[currFrag] - lightFrag <= lightSize:
					maxLight += 1
					if maxLight < nLights:
						lightFrag, lightID = lights[maxLight]
			else:
				while maxLight < nLights and lightFrag - frags[currFrag] <= lightSize:
					maxLight += 1
					if maxLight < nLights:
						lightFrag, lightID = lights[maxLight]

		# Increase min light if necessary.
		if minLight < nLights:
			lightFrag, lightID = lights[minLight]
			if reverse == True:
				while minLight < nLights and lightFrag - frags[currFrag] > lightSize:
					minLight += 1
					if minLight < nLights:
						lightFrag, lightID = lights[minLight]
			else:
				while minLight < nLights and frags[currFrag] - lightFrag > lightSize:
					minLight += 1
					if minLight < nLights:
						lightFrag, lightID = lights[minLight]

		apply_constant_lighting(frags[currFrag], lights, minLight, maxLight)



def test_light_stepping(frags, lights, reverse):
	nFrags = len(frags)
	nLights = len(lights)

	# Step in order from 0 to nLights, same for frags.
	if reverse == True:
		#currLight = nLights - 1
		currLight = 0
		currLights = [-1] * (len(lights) / 2)
		minLight = 0
		maxLight = 0

		for i in range(nFrags):
			#currFrag = nFrags - i - 1
			currFrag = i
			# Increase max light if necessary
			lightFrag, lightID = lights[maxLight]
			if currLight < nLights:
				lightFrag, lightID = lights[currLight]

				while currLight < nLights and frags[currFrag] < lightFrag:
					# Increment currLight and either add or delete it from currLights.
					found = False
					m = maxLight
					for j in range(m):
						if currLights[j] == lightID:
							found = True
							# TODO: remove the light, keeping currLights in sorted order.
							for k in range(j, m - 1):
								currLights[k] = currLights[k + 1]
							maxLight -= 1
							break
					# Binary search to find the correct light id.
					"""lo = 0
					hi = maxLight
					while lo <= hi:
						mid = (lo + hi) / 2
						if currLights[mid] > lightID:
							hi = mid - 1
						elif currLights[mid] < lightID:
							lo = mid + 1
						else:
							found = True
							# Remove the light, keeping currLights in sorted order.
							for j in range(mid, maxLight - 1):
								currLights[j] = currLights[j + 1]
							maxLight -= 1
							break"""
					# Insertion sort the lightID (or not).
					if found == False:
						currLights[maxLight] = lightID
						"""j = maxLight - 1
						while j >= 0 and currLights[j] > lightID:
							currLights[j + 1] = currLights[j]
							j -= 1
						currLights[j + 1] = lightID"""
						maxLight += 1
					currLight += 1

					if currLight < nLights:
						lightFrag, lightID = lights[currLight]
			apply_lighting(frags[currFrag], currLights, minLight, maxLight)

	else:
		currLight = nLights - 1
		currLights = [-1] * (len(lights) / 2)
		minLight = 0
		maxLight = 0

		for i in range(nFrags):
			currFrag = nFrags - i - 1
			if currLight >= 0:
				lightFrag, lightID = lights[currLight]
				while currLight >= 0 and frags[currFrag] < lightFrag:
					found = False
					for j in range(len(currLights)):
						if currLights[j] == lightID:
							found = True
							for k in range(j, len(currLights) - 1):
								currLights[k] = currLights[k + 1]
							maxLight -= 1
							break
					# Insertion sort the lightID.
					if found == False:
						#currLights[maxLight] = lightID
						#maxLight += 1
						j = maxLight - 1
						while j >= 0 and currLights[j] > lightID:
							currLights[j + 1] = currLights[j]
							j -= 1
						currLights[j + 1] = lightID
						maxLight += 1
					currLight -= 1
					lightFrag, lightID = lights[currLight]
			apply_lighting(frags[currFrag], currLights, minLight, maxLight)



def test_in_place_stepping(frags, lights, reverse):
	nFrags = len(frags)
	nLights = len(lights)

	# Step in order from 0 to nLights, same for frags.
	if reverse == True:
		#currLight = nLights - 1
		minLight = 0
		maxLight = 0

		for i in range(nFrags):
			#currFrag = nFrags - i - 1
			currFrag = i

			# Increase max light if necessary
			maxLightFrag, maxLightID = lights[maxLight]
			while maxLight < nLights and maxLightFrag > frags[currFrag]:
				maxLight += 1
				if maxLight < nLights:
					maxLightFrag, maxLightID = lights[maxLight]

			# How do I decide when to increase min light?

			apply_lighting(frags[currFrag], lights, minLight, maxLight)



def test_non_overlapping(constant = False):
	print 'Testing non-overlapping'
	if constant == True:
		# Reverse Sorted Order.
		print 'Reverse'
		test_constant_stepping([2.6, 2.4, 1.9, 1.7, 1.4, 1.3, 1.2, 1.0, 0.7, 0.5], [(2.6, 1), (0.4, 0)], True)
		# Sorted Order.
		print '\nIn-order'
		test_constant_stepping([0.5, 0.7, 1.0, 1.2, 1.3, 1.4, 1.7, 1.9, 2.4, 2.6], [(0.4, 0), (2.6, 1)], False)
	else:
		print 'Reverse'
		#test_in_place_stepping([2.6, 2.4, 1.9, 1.7, 1.4, 1.3, 1.2, 1.0, 0.7, 0.5], [(2.7, 1), (1.8, 1), (1.1, 0), (0.4, 0)], True)
		test_light_stepping([2.6, 2.4, 1.9, 1.7, 1.4, 1.3, 1.2, 1.0, 0.7, 0.5], [(2.7, 1), (1.8, 1), (1.1, 0), (0.4, 0)], True)
		# Sorted Order.
		print '\nIn-order'
		test_light_stepping([0.5, 0.7, 1.0, 1.2, 1.3, 1.4, 1.7, 1.9, 2.4, 2.6], [(0.4, 0), (1.1, 0), (1.8, 1), (2.7, 1)], False)
	print ''


def test_overlapping(constant = False):
	print 'Testing overlapping'
	if constant == True:
		# Reverse Sorted Order.
		print 'Reverse'
		test_constant_stepping([2.6, 2.4, 1.9, 1.7, 1.4, 1.3, 1.2, 1.0, 0.7, 0.5], [(2.7, 2), (2.5, 1), (2.1, 0), (1.1, 2), (0.6, 1), (0.4, 0)], True)
		# Sorted Order.
		print '\nIn-order'
		test_constant_stepping([0.5, 0.7, 1.0, 1.2, 1.3, 1.4, 1.7, 1.9, 2.4, 2.6], [(0.4, 0), (0.6, 1), (1.1, 2), (2.1, 0), (2.5, 1), (2.7, 2)], False)
	else:
		print 'Reverse'
		#test_in_place_stepping([2.6, 2.4, 1.9, 1.7, 1.4, 1.3, 1.2, 1.0, 0.7, 0.5], [(2.7, 2), (2.5, 1), (2.1, 0), (1.1, 2), (0.6, 1), (0.4, 0)], True)
		test_light_stepping([2.6, 2.4, 1.9, 1.7, 1.4, 1.3, 1.2, 1.0, 0.7, 0.5], [(2.7, 2), (2.5, 1), (2.1, 0), (1.1, 2), (0.6, 1), (0.4, 0)], True)
		# Sorted Order.
		print '\nIn-order'
		test_light_stepping([0.5, 0.7, 1.0, 1.2, 1.3, 1.4, 1.7, 1.9, 2.4, 2.6], [(0.4, 0), (0.6, 1), (1.1, 2), (2.1, 0), (2.5, 1), (2.7, 2)], False)
	print ''


test_non_overlapping()
test_overlapping()

