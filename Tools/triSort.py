import math

class vec3:
	def __init__(self, x = 0, y = 0, z = 0):
		self.x, self.y, self.z = x, y, z

	def __sub__(self, other):
		return vec3(self.x - other.x, self.y - other.y, self.z - other.z)


def cross(a, b):
	c = vec3()
	c.x = a.y * b.z - a.z * b.y
	c.y = a.z * b.x - a.x * b.z
	c.z = a.x * b.y - a.y * b.x
	return c

def dot(a, b):
	return a.x * b.x + a.y * b.y + a.z * b.z




"""
Triangles *tris;
std::vector<vec3> centers;
for(i = 0;i < num_tris;i++) {
    vec3 v;
    v.x = (tris[i].vec[0].x + tris[i].vec[1].x + tris[i].vec[2].x) / 3.0;
    v.y = (tris[i].vec[0].y + tris[i].vec[1].y + tris[i].vec[2].y) / 3.0;
    v.z = (tris[i].vec[0].z + tris[i].vec[1].z + tris[i].vec[2].z) / 3.0;
    centers.push_back(v-camera_vec);
}

for(i = 0;i < centers.size();i++) {
    for(j = i+1;j < centers.size();j++) {
        float dist1, dist2;
        dist1 = dot_product(centers[i], centers[i]);
        dist2 = dot_product(centers[j], centers[j]);
        if(dist1 < dist2) {
            swap(tris[i], tris[j]);
            swap(centers[i], centers[j]);
        }
    }
}
"""

"""
def proj(u1, u2, u3, v1, v2, v3):
	n = cross(u2 - u1, u3 - u1)
	d1 = dot(n, v1)
	d2 = dot(n, v2)
	d3 = dot(n, v3)
	print d1, d2, d3
	print d3 - d2 + d1
"""


# This is just distance from camera to the centre, which is wrong...
def proj(u1, u2, u3, v1, v2, v3):
	cu = vec3()
	cu.x = (u1.x + u2.x + u3.x) / 3.0
	cu.y = (u1.y + u2.y + u3.y) / 3.0
	cu.z = (u1.z + u2.z + u3.z) / 3.0
	cv = vec3()
	cv.x = (v1.x + v2.x + v3.x) / 3.0
	cv.y = (v1.y + v2.y + v3.y) / 3.0
	cv.z = (v1.z + v2.z + v3.z) / 3.0
	d1 = dot(cu, cu)
	d2 = dot(cv, cv)
	print d1, d2


def test_proj():
	u1 = vec3(-1, 1, -1)
	u2 = vec3(1, 1, -1)
	u3 = vec3(0, -1, -1)

	v1 = vec3(-1, 1, -2)
	v2 = vec3(1, 1, -2)
	v3 = vec3(0, -1, -2)

	proj(u1, u2, u3, v1, v2, v3)



test_proj()
