def oddSort(n):
	s = ''
	for i in xrange(n):
		if i + 2 >= n:
			break
		if i % 2 == 0:
			continue
		s += 'CSWAP(' + str(i) + ',' + str(i + 2) + ');'
	return s

def evenSort(n):
	s = ''
	for i in xrange(n):
		if i + 2 >= n:
			break
		if i % 2 != 0:
			continue
		s += 'CSWAP(' + str(i) + ',' + str(i + 2) + ');'
	return s

def genSort(n):
	print 'odd:'
	print oddSort(n)
	print '\n'
	print 'even:'
	print evenSort(n)


def oddevenSort(n):
	s = ''
	for i in range(0, n-1, 2):
		s += 'CSWAP(' + str(i) + ',' + str(i + 1) + ');'
	s += '\n'
	for i in range(1, n-1, 2):
		s += 'CSWAP(' + str(i) + ',' + str(i + 1) + ');'
	s += '\n'
	return s

print oddevenSort(128)


#genSort(128)

