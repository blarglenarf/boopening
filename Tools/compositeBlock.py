from math import *
from random import *


class frag:
	def __init__(self, val = 0, alpha = 0):
		self.val = val
		self.alpha = alpha
	def __repr__(self):
		return str(self.val) + ', ' + str(self.alpha)
	def __str__(self):
		return str(self.val) + ', ' + str(self.alpha)

def createRandomList(size, s = 0):
	seed(s)
	#l = [frag(uniform(0, 1), 0.3) for i in xrange(size)]
	#l = [frag(1.0, 0.5), frag(0.75, 0.5), frag(0.5, 0.5), frag(0.5, 0.5), frag(0.25, 0.5), frag(0.0, 0.5)]
	#l = [frag(1.0, 0.5), frag(0.5, 0.5), frag(0.5, 0.5), frag(0.0, 0.5)]
	l = [frag(1.0, 0.3), frag(0.9, 0.3), frag(0.8, 0.3), frag(0.7, 0.3), frag(0.6, 0.3), frag(0.5, 0.3), frag(0.4, 0.3), frag(0.3, 0.3)]
	return l


def mix(x, y, a):
	return x * (1.0 - a) + y * a

def mixAlpha(x, y):
	return 1.0 - (1.0 - x) * (1.0 - y)



def compositeStepwise(l):
	val = 0.0
	for i in xrange(len(l)):
		idx = len(l) - i - 1
		val = mix(val, l[idx].val, l[idx].alpha)
		print val
	return val



# Note: nBlocks will always be 32 in practice, so there will always be 5 pairwise merges after each block has been composited.
def compositeBlockwise(l, blockSize = 2, nBlocks = 4):
	# TODO: Support for 5 merges of 32 blocks. And merge in the same way that you would in glsl!
	# Note: composite needs to happen from the front using this approach.
	blocks = [frag(0.0, 0.0)] * nBlocks
	print 'blocks'
	for block in xrange(nBlocks):
		currVis = 1.0
		currColor = 0.0
		blockAlpha = 0.0
		for i in xrange(blockSize):
			idx = block * blockSize + i

			# TODO: Handle incomplete/empty blocks.
			if idx >= len(l):
				continue

			a = l[idx].alpha
			f = l[idx].val * a

			currColor += f * currVis
			currVis *= (1.0 - a)
			blockAlpha = mixAlpha(blockAlpha, a)
		blocks[block] = frag(currColor, blockAlpha)
		print blockAlpha, currColor

	# First set of pairwise block merges:
	moreBlocks = [frag(0.0, 0.0)] * (nBlocks / 2)
	print 'first merge'
	for i in xrange(nBlocks / 2):
		idx1 = i * 2
		idx2 = i * 2 + 1

		# TODO: Handle non-existant blocks.
		if (idx2 >= nBlocks):
			pass

		a = mixAlpha(blocks[idx1].alpha, blocks[idx2].alpha)
		f = blocks[idx1].val + blocks[idx2].val * (1.0 - blocks[idx1].alpha)
		moreBlocks[i] = frag(f, a)
		print a, f

	# Second set of pariwise block merges:
	print 'second merge'
	for i in xrange(nBlocks / 2 / 2):
		idx1 = i * 2
		idx2 = i * 2 + 1

		# TODO: Handle non-existant blocks.
		if (idx2 >= nBlocks / 2):
			pass

		a = mixAlpha(moreBlocks[idx1].alpha, moreBlocks[idx2].alpha)
		f = moreBlocks[idx1].val + moreBlocks[idx2].val * (1.0 - moreBlocks[idx1].alpha)
		print a, f


	# Second colour needs to be pre-multiplied by alpha.
	#v1 = mix(1.0, 0.5 * (1.0 - 0.5), 0.5)
	#v2 = mix(0.5, 0.0 * (1.0 - 0.5), 0.5)

	# or pre-multiplying both values by alpha [(0.5+0.25*(1-0.5), 1-(1-0.5)*(1-0.5))], [(0.25+0.0*0.5, 1-(1-0.5)*(1-0.5))]:
	#v1 = 1.0 * 0.5 + 0.5 * 0.5 * (1.0 - 0.5)
	#v2 = 0.5 * 0.5 + 0.0 * 0.5 * (1.0 - 0.5)

	print 'manual'

	# Alpha values for each fragment.
	a1 = l[0].alpha
	a2 = l[1].alpha
	a3 = l[2].alpha
	a4 = l[3].alpha
	a5 = l[4].alpha
	a6 = l[5].alpha

	# Pre-multiplied alpha values.
	f1 = l[0].val * a1
	f2 = l[1].val * a2
	f3 = l[2].val * a3
	f4 = l[3].val * a4
	f5 = l[4].val * a5
	f6 = l[5].val * a6

	# For Merging 4 blocks of 2 (seems to work):
	a7 = l[6].alpha
	a8 = l[7].alpha

	f7 = l[6].val * a7
	f8 = l[7].val * a8

	# Values for each of the 4 blocks.
	fb1 = f1 + f2 * (1.0 - a1)
	fb2 = f3 + f4 * (1.0 - a3)
	fb3 = f5 + f6 * (1.0 - a5)
	fb4 = f7 + f8 * (1.0 - a7)

	# Alphas for each of the 4 blocks.
	ab1 = mixAlpha(a1, a2)
	ab2 = mixAlpha(a3, a4)
	ab3 = mixAlpha(a5, a6)
	ab4 = mixAlpha(a7, a8)

	print 'blocks'
	print ab1, fb1
	print ab2, fb2
	print ab3, fb3
	print ab4, fb4

	# Now combine the 4 blocks to make 2.
	fb5 = fb1 + fb2 * (1.0 - ab1)
	fb6 = fb3 + fb4 * (1.0 - ab3)
	ab5 = mixAlpha(ab1, ab2)
	ab6 = mixAlpha(ab3, ab4)

	print 'first merge'
	print ab5, fb5
	print ab6, fb6

	# Now combine the 2 blocks to get the final value.
	fb7 = fb5 + fb6 * (1.0 - ab5)
	ab7 = mixAlpha(ab5, ab6)

	print 'second merge'

	print ab7, fb7
	return fb7



	# For merging 2 blocks of 3 (seems to be correct):
	v1 = f1 + f2 * (1.0 - a1) + f3 * (1.0 - a1) * (1.0 - a2)
	v2 = f4 + f5 * (1.0 - a4) + f6 * (1.0 - a4) * (1.0 - a5)

	ab1 = mixAlpha(a1, mixAlpha(a2, a3))
	ab2 = mixAlpha(a4, mixAlpha(a5, a6))

	ab3 = mixAlpha(ab1, ab2)

	print v1, v2
	print ab1, ab2, ab3

	val = v1 + v2 * (1.0 - ab1)
	print val
	return val


	# For merging 3 blocks of 2 (seems to be correct):
	v1 = f1 + f2 * (1.0 - a1)
	v2 = f3 + f4 * (1.0 - a3)
	v3 = f5 + f6 * (1.0 - a4)

	# Alpha for each block.
	ab1 = mixAlpha(a1, a2)
	ab2 = mixAlpha(a3, a4)
	ab3 = mixAlpha(a5, a6)

	# Combined alpha of the first two blocks.
	ab4 = mixAlpha(ab1, ab2)

	print v1, v2, v3
	print ab1, ab2, ab3, ab4

	# Merged value (using alpha of the first block, converted to a visibility value).
	val = v1 + v2 * (1.0 - ab1)
	val = val + v3 * (1.0 - ab4)

	print val
	return val


	# Blocks of size 3:
	#vis1 = (1.0 - 0.5)
	#vis2 = (1.0 - vis1)
	#v1 = 1.0 * 0.5 + 0.75 * 0.5 * vis1 + 0.5 * 0.5 * vis2
	#v2 = 0.5 * 0.5 + 0.25 * 0.5 * vis1 + 0.0 * 0.5 * vis2

	#a1 = mixAlpha(0.5, 0.5)
	#a1 = mixAlpha(a1, 0.5)
	#a2 = mixAlpha(0.5, 0.5)
	#a2 = mixAlpha(a2, 0.5)

	#print v1, v2

	#val = v1 + v2 * (1.0 - a1)

	#return val

	#v1 = mix(l[3].val, l[2].val, 0.5)
	#v2 = mix(l[1].val, l[0].val, 0.5)

	#print a1, a2
	#print v1, v2

	#a3 = mixAlpha(a1, a2)
	#print a3

	#v3 = mix(v1, v2, a3)
	#v3 = mix(0.25, 0.75, 1.19869)
	#print v3

	#return v3
	#for i in xrange(len(l)):
	#	idx = len(l) - i - 1
	#	alpha = mixAlpha(alpha, l[idx].alpha)
	#return alpha

	# so for  [(c1, a1)],[(c2, a2)] you get col = c1 + c2 * a1 and alpha = 1 - (1 - a1) * (1 - a2)




l = createRandomList(8)
print l

print 'stepwise'
val = compositeStepwise(l)
print 'final:', val

print 'blockwise'
val = compositeBlockwise(l)
print 'final:', val

