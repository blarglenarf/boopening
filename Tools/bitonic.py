from math import *
from random import *
import threading


def createRandomList(size, s = 0):
	seed(s)
	l = [uniform(0, 10) for i in xrange(size)]
	#candidate = [(d, j) for j, d in enumerate(l)]
	#candidate_sorted = [j for f, j in sorted(candidate)]
	#return l, candidate_sorted
	return l



def checkSorted(a, s):
	if a == s:
		print 'sorted!'
	else:
		print 'not sorted!'



def kernel(a, p, q):
	d = 1 << (p - q)

	for i in xrange(len(a)):
		up = ((i >> p) & 2) == 0

		if (i & d) == 0:
			b = a[i] > a[i | d]
			print up, i, i | d, ';',
			if b == up:
				t = a[i]
				a[i] = a[i | d]
				a[i | d] = t



# TODO: Currently only works with power2 size lists, needs to work with any size list.
def bitonicSort(a):
	logn = int(log(len(a), 2))

	for i in xrange(logn):
		for j in xrange(i + 1):
			kernel(a, i, j)



def bitonicSortSingle(a):
	# Doesn't matter what values are here, comparison operations will be the same.
	logn = int(log(len(a), 2))

	for p in xrange(logn):
		for q in xrange(p + 1):
			d = 1 << (p - q)

			for i in xrange(len(a)):
				up = ((i >> p) & 2) == 0
				if (i & d) == 0:
					if up:
						print i, i | d, ',',
						if a[i] > a[i | d]:
							a[i], a[i | d] = a[i | d], a[i]
					else:
						print i | d, i, ',',
						if a[i | d] > a[i]:
							a[i | d], a[i] = a[i], a[i | d]
			print ''
	return a



class BitonicThread(threading.Thread):

	def __init__(self, a, i, p, d):
		threading.Thread.__init__(self)
		self.a = a
		self.i = i
		self.p = p
		self.d = d

	# TODO: This is going to use a sort network, does that mean a massive branch at the start based on thread id?
	def run(self):
		i, p, d = self.i, self.p, self.d
		# Doesn't matter what values are here, comparison operations will be the same.
		up = ((i >> p) & 2) == 0
		if (i & d) == 0: # Doesn't seem to be necessary?
			if up:
				if self.a[i] > self.a[i | d]:
					self.a[i], self.a[i | d] = self.a[i | d], self.a[i]
			else:
				if self.a[i | d] > self.a[i]:
					self.a[i | d], self.a[i] = self.a[i], self.a[i | d]



def bitonicSortParallel(a):
	# TODO: To make this fast in glsl we can't synchronise like this, all threads must start and keep running until the list is sorted.
	# Ideally, the number of threads needed is equal to the size of the list, but we're limited to 32 (we'll never sort fewer than 32).
	logn = int(log(len(a), 2))

	for p in xrange(logn):
		for q in xrange(p + 1):
			d = 1 << (p - q)

			# TODO: Spawn 32 threads, dole out work to each of them (yes I know that's silly in python, this is for testing).
			threads = [BitonicThread(a, i, p, d) for i in xrange(len(a))]
			for t in threads:
				t.start()
			for t in threads:
				t.join()
	return a





def bitonicSortShared(a):
	len_a = len(a)
	logn = int(log(len_a, 2))
	threads = 8 # Threads should not be > len_a / 2
	valsPerThread = len_a / threads
	swapsPerThread = valsPerThread / 2

	for p in xrange(logn):
		for q in xrange(p + 1):
			d = 1 << (p - q)

			for t in xrange(threads):
				for v in xrange(swapsPerThread):
					s = t * swapsPerThread + v
					i = s * 2 - s % d
					i_d = i + d
					up = ((i >> p) & 2) == 0
					if up:
						print i, i_d, ',',
						if a[i] > a[i_d]:
							a[i], a[i_d] = a[i_d], a[i]
					else:
						print i_d, i, ',',
						if a[i_d] > a[i]:
							a[i_d], a[i] = a[i], a[i_d]
			print ''
	return a



# TODO: Different possibilities:
# * Unroll the whole thing, put into a massive switch statement at the start based on thread id.
# * Very simple unrolling, just if statements for r1 and r2.
# * Partial unrolling.

def unrollBitonicParallel():
	# TODO: Have this print an unrolled version of itself for different vals of logn, MAX_REGISTERS and thread
	len_a = 1024
	logn = int(log(len_a, 2))
	N_THREADS = 32
	MAX_REGISTERS = len_a / N_THREADS
	#thread = 0

	prev_s = ''

	# One possible way to unroll...
	# getSwapThread({0..16}) --> i = thread * MAX_REGISTERS + {0..16}

	for thread in xrange(N_THREADS):
		s = ''
		for p in xrange(logn):
			for q in xrange(p + 1):
				s += '(' + str(p) + ',' + str(q) + ');'
		s += '\n\n'
		#s += '\nthread ' + str(thread) + '\n'
		for p in xrange(logn):
			for q in xrange(p + 1):
				# TODO: Focus on unrolling this part specifically.
				for j in xrange(MAX_REGISTERS):
					i = thread * MAX_REGISTERS + j
					d = 1 << (p - q)
					swapTarget = i | d
					up = ((i >> p) & 2) == 0
					if (i & d) != 0:
						swapTarget = i - d
						up = not up
					swapRegister = swapTarget % MAX_REGISTERS
					swapThread = swapTarget / MAX_REGISTERS

					# TODO: Crucial operation.
					# This first part seems to be the same for all threads at least, but a function is needed to return the actual thread.
					if swapThread == thread:
						s += 'swapVals[' + str(j) + '] = vals[' + str(swapRegister) + '];\n'
					else:
						s += 'swapThread = getSwapThread(thread, ' + str(p) + ', ' + str(q) + ', ' + str(j) + ');\n'
						s += 'swapVals[' + str(j) + '] = shuffleNV(vals[' + str(swapRegister) + '], swapThread, N_THREADS);\n'

				# This part is much easier to unroll, ignore it.
				for j in xrange(MAX_REGISTERS):
					s += 'up = getUp(thread, ' + str(p) + ', ' + str(q) + ', ' + str(j) + ');\n'
					s += 'swapVal(' + str(j) + ', up);\n'
					#i = thread * MAX_REGISTERS + j
					#d = 1 << (p - q)
					#up = ((i >> p) & 2) == 0
					#if (i & d) != 0:
					#	up = not up

					# Crucial operation.
					#if up:
						#if vals[j] > swapVals[j]:
						#	vals[j] = swapVals[j]
						#s += 'if (vals[' + str(j) + '] > swapVals[' + str(j) + ']) vals[' + str(j) + '] = swapVals[' + str(j) + '];'
						#s += 'swapUp(' + str(j) + ');'
					#	pass
					#else:
						#if swapVals[j] > vals[j]:
						#	vals[j] = swapVals[j]
						#s += 'if (swapVals[' + str(j) + '] > vals[' + str(j) + ']) vals[' + str(j) + '] = swapVals[' + str(j) + '];'
						#s += 'swapDown(' + str(j) + ');'
					#	pass
				s += '\n'
		if prev_s == '':
			prev_s = s
		if s != prev_s:
			print 'Different!'
			print prev_s
			print s
			return
	print s
	#print 'Same!'




# TODO: Instead of a linear series of if statements, use a binary series so we can have log(n) ops instead of n.
def unrollBitonicParallelPart():
	s = ''
	for i in xrange(0, 16):
		for j in xrange(0, 16):
			s += 'SWAP(' + str(i) + ',' + str(j) + ');'
		s += '\n'
	print s
	return
	added = [(0,0)]
	print 'SWAP(0,0);'
	print '#if MAX_REGISTERS > 1'
	print '\t',
	for i in xrange(2):
		for j in xrange(2):
			if (i, j) not in added:
				print 'SWAP(' + str(i) + ',' + str(j) + ');',
				added += [(i, j)]
	print '\n#endif'
	print '#if MAX_REGISTERS > 2'
	print '\t',
	for i in xrange(4):
		for j in xrange(4):
			if (i, j) not in added:
				print 'SWAP(' + str(i) + ',' + str(j) + ');',
				added += [(i, j)]
	print '\n#endif'
	print '#if MAX_REGISTERS > 4'
	print '\t',
	for i in xrange(8):
		for j in xrange(8):
			if (i, j) not in added:
				print 'SWAP(' + str(i) + ',' + str(j) + ');',
				added += [(i, j)]
	print '\n#endif'
	print '#if MAX_REGISTERS > 8'
	print '\t',
	for i in xrange(16):
		for j in xrange(16):
			if (i, j) not in added:
				print 'SWAP(' + str(i) + ',' + str(j) + ');',
				added += [(i, j)]
	print '\n#endif'





a = createRandomList(16, 0)
s = sorted(a)


#unrollBitonicParallel()
#unrollBitonicParallelPart()

a = bitonicSortShared(a)
#print a
checkSorted(a, s)

#a = bitonicSortParallel(a)
a = bitonicSortSingle(a)
#print a
checkSorted(a, s)

