from random import *


class Frag:
	def __init__(self, x = 0, y = 0):
		self.x = x
		self.y = y



def getRandomList(val, size = 12, sortFlag = True):
	print 'listSize:', size
	l = [uniform(0, 10) for i in range(size)]
	if sortFlag:
		l = sorted(l, reverse=True)
	lVal = []
	for i in xrange(size):
		lVal += [Frag(val, l[i])]
	return lVal



def printFragList(l):
	for a in l:
		print '(', a.x, a.y, ')',
	print ''



def getFragKey(item):
	return item.y



def compareLists(listA, listB, mergeList):
	actual = listA + listB
	actual = sorted(actual, reverse=True, key=getFragKey)
	#printFragList(actual)
	#printFragList(mergeList)
	if actual != mergeList:
		print 'Not merged!'
	else:
		print 'Merged!'



# TODO: Implement a version that reads the whole list to local memory (It'll probably be slow af).
def testEntireMerge(randA, randB, blockSize = 4):
	# Easy enough I guess, just assume that all of randA and all of randB are in local memory, then repeat all the stuff below.
	pass



def testKMerge(randA, randB):
	pass





# Merging two lists (hopefully) using registers.
def testRegisterMerge(randA, randB, blockSize = 4):
	print 'register blockwise merge'
	ops, reads = 0, 0
	newList = []
	blockAData = [Frag(0, 0)] * blockSize
	blockBData = [Frag(0, 0)] * blockSize

	currRandAPos, currRandBPos = 0, 0

	blockASize, blockBSize = 0, 0
	blockAPos, blockBPos = 0, 0

	while currRandAPos < len(randA) or currRandBPos < len(randB):
		# Load the next four fragments from each (or until we reach the end).
		if blockASize == 0:
			#print 'loading block A'
			n = 0
			if 0 + currRandAPos < len(randA):
				blockAData[0] = randA[currRandAPos + 0]
				n = 1
			if 1 + currRandAPos < len(randA):
				blockAData[1] = randA[currRandAPos + 1]
				n = 2
			if 2 + currRandAPos < len(randA):
				blockAData[2] = randA[currRandAPos + 2]
				n = 3
			if 3 + currRandAPos < len(randA):
				blockAData[3] = randA[currRandAPos + 3]
				n = 4
			blockASize = n
			currRandAPos += n
			ops += n
			reads += n
		if blockBSize == 0:
			#print 'loading block b'
			n = 0
			if 0 + currRandBPos < len(randB):
				blockBData[0] = randB[currRandBPos + 0]
				n = 1
			if 1 + currRandBPos < len(randB):
				blockBData[1] = randB[currRandBPos + 1]
				n = 2
			if 2 + currRandBPos < len(randB):
				blockBData[2] = randB[currRandBPos + 2]
				n = 3
			if 3 + currRandBPos < len(randB):
				blockBData[3] = randB[currRandBPos + 3]
				n = 4
			blockBSize = n
			currRandBPos += n
			ops += n
			reads += n
		if blockASize == 0 or blockBSize == 0:
			break
		# Iterate over these and composite until one list is empty.
		#currVal = Frag(0, 0)
		#print 'merging'
		for i in xrange(blockSize * 2):
			fA, fB = Frag(0, 0), Frag(0, 0) # TODO: This but without the fA/fB vals.
			if blockAPos < blockASize and blockBPos < blockBSize:
				ops += 1
				if blockAPos == 0:
					fA = blockAData[0]
				elif blockAPos == 1:
					fA = blockAData[1]
				elif blockAPos == 2:
					fA = blockAData[2]
				elif blockAPos == 3:
					fA = blockAData[3]
				if blockBPos == 0:
					fB = blockBData[0]
				elif blockBPos == 1:
					fB = blockBData[1]
				elif blockBPos == 2:
					fB = blockBData[2]
				elif blockBPos == 3:
					fB = blockBData[3]
				if fA.y >= fB.y:
					blockAPos += 1
					newList += [fA]
				else:
					blockBPos += 1
					newList += [fB]
		if blockAPos == blockASize:
			blockAPos, blockASize = 0, 0
			#print 'reset block A'
		if blockBPos == blockBSize:
			blockBPos, blockBSize = 0, 0
			#print 'reset block B'

	# Composite any fragments left in the blocks.
	if blockAPos < blockASize:
		if 0 < blockASize and 0 >= blockAPos:
			currVal = blockAData[0]
			newList += [currVal]
			ops += 1
		if 1 < blockASize and 1 >= blockAPos:
			currVal = blockAData[1]
			newList += [currVal]
			ops += 1
		if 2 < blockASize and 2 >= blockAPos:
			currVal = blockAData[2]
			newList += [currVal]
			ops += 1
		if 3 < blockASize and 3 >= blockAPos:
			currVal = blockAData[3]
			newList += [currVal]
			ops += 1
	if blockBPos < blockBSize:
		if 0 < blockBSize and 0 >= blockBPos:
			currVal = blockBData[0]
			newList += [currVal]
			ops += 1
		if 1 < blockBSize and 1 >= blockBPos:
			currVal = blockBData[1]
			newList += [currVal]
			ops += 1
		if 2 < blockBSize and 2 >= blockBPos:
			currVal = blockBData[2]
			newList += [currVal]
			ops += 1
		if 3 < blockBSize and 3 >= blockBPos:
			currVal = blockBData[3]
			newList += [currVal]
			ops += 1
	# If any list still has fragments in it, composite them.
	while currRandAPos < len(randA):
		ops += 1
		reads += 1
		currVal = randA[currRandAPos]
		currRandAPos += 1
		#print currVal.x, currVal.y
		newList += [currVal]
	while currRandBPos < len(randB):
		ops += 1
		reads += 1
		currVal = randB[currRandBPos]
		currRandBPos += 1
		#print currVal.x, currVal.y
		newList += [currVal]
	#print blockAPos, blockASize
	#print blockBPos, blockBSize
	#print currRandAPos, currRandBPos
	print 'expected:', len(randA) + len(randB)
	print 'ops:', ops, 'reads:', reads
	return newList



def testMerge(randA, randB, blockSize = 4):
	print 'blockwise merge'
	ops, reads = 0, 0
	newList = []
	blockAData = [Frag(0, 0)] * blockSize
	blockBData = [Frag(0, 0)] * blockSize

	currRandAPos, currRandBPos = 0, 0

	blockASize, blockBSize = 0, 0
	blockAPos, blockBPos = 0, 0

	while currRandAPos < len(randA) or currRandBPos < len(randB):
		# Load the next four fragments from each (or until we reach the end).
		if blockASize == 0:
			#print 'loading block A'
			for i in xrange(blockSize):
				if currRandAPos == len(randA):
					break
				reads += 1
				ops += 1
				blockAData[i] = randA[currRandAPos]
				blockASize += 1
				currRandAPos += 1
		if blockBSize == 0:
			#print 'loading block b'
			for i in xrange(blockSize):
				if currRandBPos == len(randB):
					break
				reads += 1
				ops += 1
				blockBData[i] = randB[currRandBPos]
				blockBSize += 1
				currRandBPos += 1
		if blockASize == 0 or blockBSize == 0:
			break
		# Iterate over these and composite until one list is empty.
		currVal = Frag(0, 0)
		if blockASize > 0 and blockBSize > 0:
			#print 'merging'
			while blockAPos < blockASize and blockBPos < blockBSize:
				ops += 1
				if blockAData[blockAPos].y >= blockBData[blockBPos].y:
					currVal = blockAData[blockAPos]
					blockAPos += 1
				else:
					currVal = blockBData[blockBPos]
					blockBPos += 1
				#print currVal.x, currVal.y
				newList += [currVal]
		if blockAPos == blockASize:
			blockAPos, blockASize = 0, 0
			#print 'reset block A'
		if blockBPos == blockBSize:
			blockBPos, blockBSize = 0, 0
			#print 'reset block B'

	# Composite any fragments left in the blocks.
	while blockAPos < blockASize:
		ops += 1
		currVal = blockAData[blockAPos]
		blockAPos += 1
		#print currVal.x, currVal.y
		newList += [currVal]
	while blockBPos < blockBSize:
		ops += 1
		currVal = blockBData[blockBPos]
		blockBPos += 1
		#print currVal.x, currVal.y
		newList += [currVal]

	# If any list still has fragments in it, composite them.
	while currRandAPos < len(randA):
		ops += 1
		reads += 1
		currVal = randA[currRandAPos]
		currRandAPos += 1
		#print currVal.x, currVal.y
		newList += [currVal]
	while currRandBPos < len(randB):
		ops += 1
		reads += 1
		currVal = randB[currRandBPos]
		currRandBPos += 1
		#print currVal.x, currVal.y
		newList += [currVal]
	#print blockAPos, blockASize
	#print blockBPos, blockBSize
	#print currRandAPos, currRandBPos
	print 'expected:', len(randA) + len(randB)
	print 'ops:', ops, 'reads:', reads
	return newList



def testStep(randA, randB):
	print 'stepwise merge'
	# Need to read from both lfb's simultaneously and composite from them in order.
	ops, reads = 0, 0
	newList = []

	aFlag, bFlag = True, True
	currRandAPos, currRandBPos = 0, 0
	currA, currB = Frag(0, 0), Frag(0, 0)

	while currRandAPos < len(randA) or currRandBPos < len(randB):
		if aFlag and currRandAPos < len(randA):
			ops += 1
			reads += 1
			currA = randA[currRandAPos]
			currRandAPos += 1
			aFlag = False
		if bFlag and currRandBPos < len(randB):
			ops += 1
			reads += 1
			currB = randB[currRandBPos]
			currRandBPos += 1
			bFlag = False
		currVal = Frag(0, 0)
		if currA.y >= currB.y:
			ops += 1
			currVal = currA
			currA = Frag(0, 0)
			aFlag = True
		else:
			ops += 1
			currVal = currB
			currB = Frag(0, 0)
			bFlag = True
		#print currVal.x, currVal.y
		newList += [currVal]
	# Write remaining value if necessary.
	if not aFlag:
		#print currA.x, currA.y
		newList += [currA]
	if not bFlag:
		#print currB.x, currB.y
		newList += [currB]
	print 'expected:', len(randA) + len(randB)
	print 'ops:', ops, 'reads:', reads
	return newList




randA = getRandomList(1, randint(0, 1000))
randB = getRandomList(2, randint(0, 1000))

#randA = getRandomList(1, 8)
#randB = getRandomList(2, 8)


#printFragList(randA)
#printFragList(randB)


newListA = testMerge(randA, randB)
newListB = testStep(randA, randB)
newListC = testRegisterMerge(randA, randB)
#newListD = testRegisterMergesort(randA, randB)

compareLists(randA, randB, newListA)
compareLists(randA, randB, newListB)
compareLists(randA, randB, newListC)
#compareLists(randA, randB, newListD)

