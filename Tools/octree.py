from random import *
import math


def create_random_list(size, s = 0):
	seed(s)
	l = [uniform(0, 1024) for i in range(size)]
	candidate = [(d, j) for j, d in enumerate(l)]
	candidate_sorted = [j for f, j in sorted(candidate)]
	return l#, candidate_sorted




class Point:

	def __init__(self, x, y, z):
		self.x, self.y, self.z = x, y, z


def calc_mid(a, b):
	return Point(a.x + ((b.x - a.x) / 2), a.y + ((b.y - a.y) / 2), a.z + ((b.z - a.z) / 2))

def distance(a, b):
	p = Point(b.x - a.x, b.y - a.y, b.z - a.z)
	return math.sqrt(p.x * p.x + p.y * p.y + p.z * p.z)

def get_aabb(i, aabb_min, aabb_mid, aabb_max):
	curr_min, curr_mid, curr_max = aabb_min, aabb_mid, aabb_max
	if i == 0:
		curr_min, curr_max = aabb_min, aabb_mid
		curr_mid = calc_mid(curr_min, curr_max)
	elif i == 1:
		curr_min = Point(aabb_min.x, aabb_min.y, aabb_mid.z)
		curr_max = Point(aabb_mid.x, aabb_mid.y, aabb_max.z)
		curr_mid = calc_mid(curr_min, curr_max)
	elif i == 2:
		curr_min = Point(aabb_mid.x, aabb_min.y, aabb_min.z)
		curr_max = Point(aabb_max.x, aabb_mid.y, aabb_mid.z)
		curr_mid = calc_mid(curr_min, curr_max)
	elif i == 3:
		curr_min = Point(aabb_mid.x, aabb_min.y, aabb_mid.z)
		curr_max = Point(aabb_max.x, aabb_mid.y, aabb_max.z)
		curr_mid = calc_mid(curr_min, curr_max)
	elif i == 4:
		curr_min = Point(aabb_min.x, aabb_mid.y, aabb_min.z)
		curr_max = Point(aabb_mid.x, aabb_max.y, aabb_mid.z)
		curr_mid = calc_mid(curr_min, curr_max)
	elif i == 5:
		curr_min = Point(aabb_min.x, aabb_mid.y, aabb_mid.z)
		curr_max = Point(aabb_mid.x, aabb_max.y, aabb_max.z)
		curr_mid = calc_mid(curr_min, curr_max)
	elif i == 6:
		curr_min = Point(aabb_mid.x, aabb_mid.y, aabb_min.z)
		curr_max = Point(aabb_max.x, aabb_max.y, aabb_mid.z)
		curr_mid = calc_mid(curr_min, curr_max)
	elif i == 7:
		curr_min = Point(aabb_mid.x, aabb_mid.y, aabb_mid.z)
		curr_max = Point(aabb_max.x, aabb_max.y, aabb_max.z)
		curr_mid = calc_mid(curr_min, curr_max)
	return curr_min, curr_mid, curr_max


# From: https://tavianator.com/fast-branchless-raybounding-box-intersections-part-2-nans/
"""bool intersection(box b, ray r)
{
	double t1 = (b.min[0] - r.origin[0]) * r.dir_inv[0];
	double t2 = (b.max[0] - r.origin[0]) * r.dir_inv[0];

	double tmin = min(t1, t2);
	double tmax = max(t1, t2);

	for (int i = 1; i < 3; ++i)
	{
		t1 = (b.min[i] - r.origin[i]) * r.dir_inv[i];
		t2 = (b.max[i] - r.origin[i]) * r.dir_inv[i];

		tmin = max(tmin, min(t1, t2));
		tmax = min(tmax, max(t1, t2));
	}

	return tmax > max(tmin, 0.0);
}"""
def ray_intersection(aabb_min, aabb_max, origin, inv, l_min, l_max):
	# Need to ensure that intersection point isn't past the start/end points.
	# FIXME: Find a faster way of doing this, preferably without if statements.
	if l_min.x > aabb_max.x or l_min.y > aabb_max.y or l_min.z > aabb_max.z or l_max.x < aabb_min.x or l_max.y < aabb_min.y or l_max.z < aabb_min.z:
		return False

	# X component.
	t1 = (aabb_min.x - origin.x) * inv.x
	t2 = (aabb_max.x - origin.x) * inv.x
	tmin = min(t1, t2)
	tmax = max(t1, t2)

	# Y component.
	t1 = (aabb_min.y - origin.y) * inv.y
	t2 = (aabb_max.y - origin.y) * inv.y
	tmin = max(tmin, min(t1, t2))
	tmax = min(tmax, max(t1, t2))

	# Z component.
	t1 = (aabb_min.z - origin.z) * inv.z
	t2 = (aabb_max.z - origin.z) * inv.z
	tmin = max(tmin, min(t1, t2))
	tmax = min(tmax, max(t1, t2))

	return tmax > max(tmin, 0.0)



# TODO: Modify this to support a non-sparse octree, using bits (stored in bytes per node).
class Octree:

	def __init__(self):
		# Make a point in each root node for now.
		# First value is NULL.
		#self.vals = [Point(0, 0, 0), Point(10, 10, 10), Point(530, 10, 10), Point(10, 10, 530), Point(530, 10, 530), Point(10, 530, 10), Point(530, 530, 10), Point(10, 530, 530), Point(530, 530, 530), Point(530, 530, 530)]
		#self.vals = [Point(0, 0, 0), Point(530, 530, 530), Point(530, 530, 530)]

		# It should be 9 levels for 512x512x512, if I'm implementing this correctly...
		self.size = 128
		self.n_levels = int(math.log(self.size, 2))
		self.nodes = [0] * 2000000
		self.next = [0] * 2000000
		self.allocated = 1 # GLSL Astomic counter.

		self.n_ops = 0
		self.n_reads = 0

		# TODO: Need more vals than this for proper testing.
		n_vals = 10000
		seed(10)
		rand_x = [randint(0, self.size) for i in range(n_vals)]
		rand_y = [randint(0, self.size) for i in range(n_vals)]
		rand_z = [randint(0, self.size) for i in range(n_vals)]
		self.vals = [Point(0, 0, 0)] * n_vals
		for i in xrange(n_vals):
			self.vals[i] = Point(rand_x[i], rand_y[i], rand_z[i])
		#	print self.vals[i].x, self.vals[i].y, self.vals[i].z, ';',
		#print ''

	"""
	def traverse_nodes_sparse(self, pos):
		# Find start node.
		max_x, max_y, max_z = self.size, self.size, self.size
		mid_x, mid_y, mid_z = self.size / 2, self.size / 2, self.size / 2
		min_x, min_y, min_z = 0, 0, 0
		curr_index = 0
		curr_level = 1

		if pos.y >= mid_y:
			curr_index += 4
			min_y = mid_y
		else:
			max_y = mid_y
		if pos.x >= mid_x:
			curr_index += 2
			min_x = mid_x
		else:
			max_x = mid_x
		if pos.z >= mid_z:
			curr_index += 1
			min_z = mid_z
		else:
			max_z = mid_z
		mid_x, mid_y, mid_z = min_x + ((max_x - min_x) / 2), min_y + ((max_y - min_y) / 2), min_z + ((max_z - min_z) / 2)

		# Follow nodes until we hit null (or marked for creation).
		while self.nodes[curr_index] != 0 and self.nodes[curr_index] != 1 and curr_level < self.n_levels:
			# Need to know which of the 8 children to follow.
			curr_index = self.nodes[curr_index]
			if pos.y >= mid_y:
				curr_index += 4
				min_y = mid_y
			else:
				max_y = mid_y
			if pos.x >= mid_x:
				curr_index += 2
				min_x = mid_x
			else:
				max_x = mid_x
			if pos.z >= mid_z:
				curr_index += 1
				min_z = mid_z
			else:
				max_z = mid_z
			mid_x, mid_y, mid_z = min_x + ((max_x - min_x) / 2), min_y + ((max_y - min_y) / 2), min_z + ((max_z - min_z) / 2)
			curr_level += 1

		# If curr_level != max_level, then we failed to reach the end of the tree.
		#print curr_level

		# Final position is aabb between min and max.
		aabb_min = Point(min_x, min_y, min_z)
		aabb_max = Point(max_x, max_y, max_z) # May want to subtract 1 from each of these, not sure.
		#print curr_index
		return curr_index, aabb_min, aabb_max, curr_level
	"""
	"""
	def traverse_list_sparse(self, index):
		# FIXME: Linked list appears to contain incorrect values for some nodes.
		head = self.nodes[index]
		curr_node = self.nodes[head]
		while curr_node != 0:
			print self.vals[curr_node].x, self.vals[curr_node].y, self.vals[curr_node].z
			curr_node = self.next[curr_node]
	"""
	"""
	def flag_node_sparse(self, index):
		# Traverse the tree until we find an empty node surrounding the given position.
		# Remember that the first value is NULL.
		pos = self.vals[index + 1]
		node_index, aabb_min, aabb_max, level = self.traverse_nodes_sparse(pos)
		#print aabb_min.x, aabb_min.y, aabb_min.z, aabb_max.x, aabb_max.y, aabb_max.z

		# Mark the node as needing to be created.
		self.nodes[node_index] = 1
	"""
	"""
	def create_node_sparse(self, index, prev_allocated):
		node_pos = prev_allocated + index

		# If node isn't marked, ignore it.
		if self.nodes[node_pos] != 1:
			return

		# Node is marked, make a new node.
		new_node = self.allocated
		self.allocated += 1 # GLSL atomic counter increment.
		self.nodes[node_pos] = new_node * 8
	"""
	"""
	def create_leaf_sparse(self, index):
		# Remember that first value is NULL.
		pos = self.vals[index + 1]

		# Traverse tree to find position.
		node_index, aabb_min, aabb_max, level = self.traverse_nodes_sparse(pos)

		# Leaf node at that position becomes the head ptr of a linked list.
		node_index = self.nodes[node_index]
		#print node_index, self.nodes[node_index]

		# The next ptrs work the same as the lfb.
		curr_head = self.nodes[node_index] # GLSL atomicExchange.
		self.nodes[node_index] = index + 1 # GLSL atomicExchange.
		self.next[index + 1] = curr_head
	"""
	"""
	def build_tree_sparse(self):
		# TODO: Probably works for all levels, but needs more testing.
		# Need to fill all levels of the octree.
		prev_allocated = 0
		for level in xrange(self.n_levels):
			# First, mark nodes as needing to be created.
			for i in xrange(len(self.vals) - 1):
				self.flag_node_sparse(i)

			# Spawn a thread for each node at the current level and create new nodes for each marked node.
			for i in xrange(self.allocated * 8 - prev_allocated):
				self.create_node_sparse(i, prev_allocated)

			prev_allocated = self.allocated - 1

		# Now create the leaf nodes (list of values per node).
		for i in xrange(len(self.vals) - 1):
			self.create_leaf_sparse(i)

		#print self.next
		#print self.nodes
	"""
	"""
	def find_nodes_sparse(self, pos, print_flag = True):
		node_index, aabb_min, aabb_max, level = self.traverse_nodes_sparse(pos)
		if level != self.n_levels:
			if print_flag:
				print 'No nodes found at', pos.x, pos.y, pos.z
			return False
		else:
			self.traverse_list_sparse(node_index)
			return True
	"""
	"""
	def check_children_nodes_sparse(self, aabb_min, aabb_mid, aabb_max, o, d_inv, l_min, l_max, index, lvl, n_ops, n_reads):
		curr_min, curr_max, curr_mid = aabb_min, aabb_max, aabb_mid

		# FIXME: Looks like this isn't working correctly, not sure why.
		# FIXME: For whatever reason, the dda stuff reports more hits than this does under some conditions.
		# FIXME: Actually it looks like that's an artifact of digitising the path, whereas this will only give intersections with the actual ray.

		# TODO: What is an iterative push/pop operation?
		# push is curr_index = self.nodes[index + i].
		# pop is what? The structure is sparse so it can't be calculated... I think...
		# could just use a non-sparse structure to fix this.
		# or I could *gasp* write a different function for each level of the tree.

		# TODO: This feels like it could be arranged in glsl to use only registers.
		# TODO: If registers aren't possible, then restrict this to iterating only over nodes that the ray will intersect, instead of all 8.
		for i in xrange(8):
			# TODO: A switch statement might be nice right about now.
			# If there's an intersection, and the node exists, check its children.
			if i == 0:
				curr_min, curr_max = aabb_min, aabb_mid
				curr_mid = calc_mid(curr_min, curr_max)
			elif i == 1:
				curr_min = Point(aabb_min.x, aabb_min.y, aabb_mid.z)
				curr_max = Point(aabb_mid.x, aabb_mid.y, aabb_max.z)
				curr_mid = calc_mid(curr_min, curr_max)
			elif i == 2:
				curr_min = Point(aabb_mid.x, aabb_min.y, aabb_min.z)
				curr_max = Point(aabb_max.x, aabb_mid.y, aabb_mid.z)
				curr_mid = calc_mid(curr_min, curr_max)
			elif i == 3:
				curr_min = Point(aabb_mid.x, aabb_min.y, aabb_mid.z)
				curr_max = Point(aabb_max.x, aabb_mid.y, aabb_max.z)
				curr_mid = calc_mid(curr_min, curr_max)
			elif i == 4:
				curr_min = Point(aabb_min.x, aabb_mid.y, aabb_min.z)
				curr_max = Point(aabb_mid.x, aabb_max.y, aabb_mid.z)
				curr_mid = calc_mid(curr_min, curr_max)
			elif i == 5:
				curr_min = Point(aabb_min.x, aabb_mid.y, aabb_mid.z)
				curr_max = Point(aabb_mid.x, aabb_max.y, aabb_max.z)
				curr_mid = calc_mid(curr_min, curr_max)
			elif i == 6:
				curr_min = Point(aabb_mid.x, aabb_mid.y, aabb_min.z)
				curr_max = Point(aabb_max.x, aabb_max.y, aabb_mid.z)
				curr_mid = calc_mid(curr_min, curr_max)
			elif i == 7:
				curr_min = Point(aabb_mid.x, aabb_mid.y, aabb_mid.z)
				curr_max = Point(aabb_max.x, aabb_max.y, aabb_max.z)
				curr_mid = calc_mid(curr_min, curr_max)
			n_ops += 1

			# Go to this node's children and repeat the process.
			if ray_intersection(curr_min, curr_max, o, d_inv, l_min, l_max):
				if lvl == self.n_levels - 1:
					print 'found!'
					print 'index', index + i, 'level', lvl, 'node', self.nodes[index + i]
					print curr_min.x, curr_min.y, curr_min.z
					print curr_max.x, curr_max.y, curr_max.z
					print 'n_ops', n_ops, 'n_reads', n_reads
					return True
				n_reads += 1
				if self.nodes[index + i] == 0:
					continue
				print 'index', index + i, 'level', lvl, 'node', self.nodes[index + i]
				print curr_min.x, curr_min.y, curr_min.z
				print curr_max.x, curr_max.y, curr_max.z
				# TODO: This is a push operation.
				found = self.check_children_nodes_sparse(curr_min, curr_mid, curr_max, o, d_inv, l_min, l_max, self.nodes[index + i], lvl + 1, n_ops, n_reads)
				if found == True:
					return True
			# TODO: If we get to here and i == 7, this is a pop operation.
		return False
	"""
	"""
	def traverse_grid_sparse(self, start_pos, end_pos):
		# TODO: Doing this without a stack is possible, with a bunch of nested if statements drilling down to the appropriate level.
		print start_pos.x, start_pos.y, start_pos.z
		print end_pos.x, end_pos.y, end_pos.z

		# Ray origin/direction.
		o = start_pos
		e = end_pos
		d = Point(end_pos.x - start_pos.x, end_pos.y - start_pos.y, end_pos.z - start_pos.z) # If we don't normalize this, then we know t values > 1 are past the end point.
		l_min = Point(min(o.x, e.x), min(o.y, e.y), min(o.z, e.z))
		l_max = Point(max(o.x, e.x), max(o.y, e.y), max(o.z, e.z))

		# Normalising anyway, and getting inverse.
		l = math.sqrt(d.x * d.x + d.y * d.y + d.z * d.z)
		d.x /= l
		d.y /= l
		d.z /= l
		d_inv = Point(1.0 / d.x, 1.0 / d.y, 1.0 / d.z)

		# Root aabb.
		aabb_min = Point(0, 0, 0)
		aabb_max = Point(self.size, self.size, self.size)
		aabb_mid = Point(self.size / 2, self.size / 2, self.size / 2)

		# Check intersection with each node.
		# TODO: I guess that could be done with a separate function for each level in glsl, although it won't be pretty.
		# TODO: Nested functions may not be necessary, all we're really pushing is the index, and we can keep track of that without a stack, I think...
		if self.check_children_nodes_sparse(aabb_min, aabb_mid, aabb_max, o, d_inv, l_min, l_max, 0, 0, 0, 0) == True:
			print 'Found leaf node!'
		else:
			print 'No leaf node found!'
	"""

	def check_children_nodes_full(self, aabb_min, aabb_mid, aabb_max, o, d_inv, l_min, l_max, index, lvl):
		for i in xrange(8):
			# If there's an intersection, and the node exists, check its children.
			curr_min, curr_mid, curr_max = get_aabb(i, aabb_min, aabb_mid, aabb_max)
			self.n_ops += 1

			# Go to this node's children and repeat the process.
			if ray_intersection(curr_min, curr_max, o, d_inv, l_min, l_max):
				if lvl == self.n_levels - 1 and self.nodes[index + i] != 0:
					print 'found!'
					print 'index', index + i, 'level', lvl, 'node', self.nodes[index + i]
					print curr_min.x, curr_min.y, curr_min.z
					print curr_max.x, curr_max.y, curr_max.z
					for i in xrange(len(self.vals)):
						if self.vals[i].x == curr_min.x and self.vals[i].y == curr_min.y and self.vals[i].z == curr_min.z:
							print 'node exists in vals'
					return True
				self.n_reads += 1
				if self.nodes[index + i] == 0:
					continue
				print 'index', index + i, 'level', lvl, 'node', self.nodes[index + i]
				print curr_min.x, curr_min.y, curr_min.z
				print curr_max.x, curr_max.y, curr_max.z
				# TODO: This is a push operation.
				found = self.check_children_nodes_full(curr_min, curr_mid, curr_max, o, d_inv, l_min, l_max, (index + i + 1) * 8, lvl + 1)
				if found == True:
					return True
			# TODO: If we get to here and i == 7, this is a pop operation.
		return False


	def traverse_grid_full(self, start_pos, end_pos):
		print 'starting brute force traversal'
		self.n_ops = 0
		self.n_reads = 0
		#print start_pos.x, start_pos.y, start_pos.z
		#print end_pos.x, end_pos.y, end_pos.z

		# Ray origin/direction.
		o = start_pos
		e = end_pos
		d = Point(end_pos.x - start_pos.x, end_pos.y - start_pos.y, end_pos.z - start_pos.z) # If we don't normalize this, then we know t values > 1 are past the end point.
		l_min = Point(min(o.x, e.x), min(o.y, e.y), min(o.z, e.z))
		l_max = Point(max(o.x, e.x), max(o.y, e.y), max(o.z, e.z))

		# Normalising anyway, and getting inverse.
		l = math.sqrt(d.x * d.x + d.y * d.y + d.z * d.z)
		d.x /= l
		d.y /= l
		d.z /= l
		d_inv = Point(1.0 / d.x, 1.0 / d.y, 1.0 / d.z)

		# Root aabb.
		aabb_min = Point(0, 0, 0)
		aabb_max = Point(self.size, self.size, self.size)
		aabb_mid = Point(self.size / 2, self.size / 2, self.size / 2)

		# Check intersection with each node.
		# TODO: I guess that could be done with a separate function for each level in glsl, although it won't be pretty.
		if self.check_children_nodes_full(aabb_min, aabb_mid, aabb_max, o, d_inv, l_min, l_max, 0, 0) == True:
			print 'Found leaf node!'
		else:
			print 'No leaf node found!'


	def find_nodes_full(self, pos, print_flag = True):
		# Since this is a full octree, you could just go straight to the correct node, but screw that.
		max_x, max_y, max_z = self.size, self.size, self.size
		mid_x, mid_y, mid_z = self.size / 2, self.size / 2, self.size / 2
		min_x, min_y, min_z = 0, 0, 0
		curr_index = 0

		for level in xrange(self.n_levels):
			self.n_ops += 1
			if pos.y >= mid_y:
				curr_index += 4
				min_y = mid_y
			else:
				max_y = mid_y
			if pos.x >= mid_x:
				curr_index += 2
				min_x = mid_x
			else:
				max_x = mid_x
			if pos.z >= mid_z:
				curr_index += 1
				min_z = mid_z
			else:
				max_z = mid_z
			mid_x, mid_y, mid_z = min_x + ((max_x - min_x) / 2), min_y + ((max_y - min_y) / 2), min_z + ((max_z - min_z) / 2)
			if level < self.n_levels - 1:
				curr_index = (curr_index + 1) * 8
		self.n_reads += 1
		if self.nodes[curr_index] != 0:
			if print_flag:
				print 'Found!'
			return True
		if print_flag:
			print 'No nodes found at', pos.x, pos.y, pos.z
		return False


	def build_tree_full(self):
		# Allocate nodes for all levels of the tree and set them to zero.
		# If there are a lot of levels, this could use up *a lot* of memory.
		max_size = 0
		for i in xrange(self.n_levels):
			max_size += 8 ** (i + 1)
		if len(self.nodes) != max_size:
			self.nodes = [0] * max_size

		for i in xrange(len(self.vals)):
			pos = self.vals[i]

			# Find start node.
			max_x, max_y, max_z = self.size, self.size, self.size
			mid_x, mid_y, mid_z = self.size / 2, self.size / 2, self.size / 2
			min_x, min_y, min_z = 0, 0, 0
			curr_index = 0

			# Fill all affected nodes in all levels.
			for level in xrange(self.n_levels):
				if pos.y >= mid_y:
					curr_index += 4
					min_y = mid_y
				else:
					max_y = mid_y
				if pos.x >= mid_x:
					curr_index += 2
					min_x = mid_x
				else:
					max_x = mid_x
				if pos.z >= mid_z:
					curr_index += 1
					min_z = mid_z
				else:
					max_z = mid_z
				mid_x, mid_y, mid_z = min_x + ((max_x - min_x) / 2), min_y + ((max_y - min_y) / 2), min_z + ((max_z - min_z) / 2)
				self.nodes[curr_index] = 1
				curr_index = (curr_index + 1) * 8


	def print_dda_path(self, start_pos, end_pos, sparse = True):
		self.n_ops = 0
		self.n_reads = 0

		# FIXME: I suspect that this dda is actually incorrect, or at least misses some data. That's quite bad.
		print 'Starting dda traversal'
		# Test function to print all leaf nodes along the path, to confirm that intersections are correctly found.
		# Ray origin/direction.
		o = start_pos
		e = end_pos
		d = Point(end_pos.x - start_pos.x, end_pos.y - start_pos.y, end_pos.z - start_pos.z) # If we don't normalize this, then we know t values > 1 are past the end point.

		s = Point(1 if e.x > o.x else -1 if e.x < o.x else 0, 1 if e.y > o.y else -1 if e.y < o.y else 0, 1 if e.z > o.z else -1 if e.z < o.z else 0)
		g = o

		# Planes for each axis that we will cross.
		gp = Point(o.x + (1 if e.x > o.x else 0), o.y + (1 if e.y > o.y else 0), o.z + (1 if e.z > o.z else 0))

		# Only used for multiplying up the error margins.
		v = Point(1 if e.x == o.x else e.x - o.x, 1 if e.y == o.y else e.y - o.y, 1 if e.z == o.z else e.z - o.z)

		# Error is normalized to v.x * v.y * v.z so we only have to multiply up.
		vxvy = v.x * v.y
		vxvz = v.x * v.z
		vyvz = v.y * v.z

		# Error from the next plane accumulators, scaled up by vx * vy * vz.
		#err = Point((gp.x - g.x) * vyvz, (gp.y - g.y) * vxvz, (gp.z - g.z) * vxvy)
		err = Point(gp.x - g.x, gp.y - g.y, gp.z - g.z)
		derr = Point(s.x * vyvz, s.y * vxvz, s.z * vxvy)

		canary = 4096
		found = False
		for i in xrange(canary):
			self.n_ops += 1
			# FIXME: Since we're doing this at high res, ignore the end grid cell.
			if distance(g, e) <= 1:
				break
			if g.x == e.x and g.y == e.y and g.z == e.z:
				break

			print g.x, g.y, g.z, ';',

			# Visit g.
			if sparse == True:
				if self.find_nodes_sparse(g, False):
					print '\nNode found at', g.x, g.y, g.z
					found = True
					#return True
			else:
				if self.find_nodes_full(g, False):
					print '\nNode found at', g.x, g.y, g.z
					found = True
					#return True

			# Reached the end.
			if g.x == e.x and g.y == e.y and g.z == e.z:
				break

			# Which plane do we cross first?
			r = Point(abs(err.x), abs(err.y), abs(err.z))

			if s.x != 0 and (s.y == 0 or r.x < r.y) and (s.z == 0 or r.x < r.z):
				g.x += s.x
				err.x += derr.x
			elif s.y != 0 and (s.z == 0 or r.y < r.z):
				g.y += s.y
				err.y += derr.y
			elif s.z != 0:
				g.z += s.z
				err.z += derr.z
		return found


	"""
	# From https://stackoverflow.com/questions/10228690/ray-octree-intersection-algorithms
	# Note: For whatever reason, bit 0 is Z and bit 2 is X.
	int first_node(double tx0, double ty0, double tz0, double txm, double tym, double tzm)
	{
		unsigned char answer = 0;   // initialize to 00000000
		// select the entry plane and set bits
		if(tx0 > ty0)
		{
			if(tx0 > tz0)
			{ // PLANE YZ
				if(tym < tx0) answer|=2;    // set bit at position 1
				if(tzm < tx0) answer|=1;    // set bit at position 0
				return (int) answer;
			}
		}
		else
		{
			if(ty0 > tz0)
			{ // PLANE XZ
				if(txm < ty0) answer|=4;    // set bit at position 2
				if(tzm < ty0) answer|=1;    // set bit at position 0
				return (int) answer;
			}
		}
		// PLANE XY
		if(txm < tz0) answer|=4;    // set bit at position 2
		if(tym < tz0) answer|=2;    // set bit at position 1
		return (int) answer;
	}
	"""
	# FIXME: For whatever reason, bit 0 is Z and bit 2 is X (swapped around).
	def test_first_node(self, tx0, ty0, tz0, txm, tym, tzm):
		answer = 0
		if tx0 > ty0:
			if tx0 > tz0:
				# Plane YZ.
				if tym < tx0:
					answer |= 2 # Bit 1 (2) --> Y.
				if tzm < tx0:
					answer |= 1 # Bit 2 (4) --> Z.
				return answer
		else:
			if ty0 > tz0:
				# Plane XZ.
				if txm < ty0:
					answer |= 4 # Bit 0 (1) --> X.
				if tzm < ty0:
					answer |= 1 # Bit 2 (4) --> Z.
				return answer
		# Plane XY.
		if txm < tz0:
			answer |= 4 # Bit 0 (1) --> X.
		if tym < tz0:
			answer |= 2 # Bit 1 (2) --> Y.
		return answer


	"""
	int new_node(double txm, int x, double tym, int y, double tzm, int z)
	{
		if(txm < tym)
		{
			if(txm < tzm) {return x;}  // YZ plane
		}
		else
		{
			if(tym < tzm) {return y;} // XZ plane
		}
		return z; // XY plane;
	}
	"""
	def test_new_node(self, txm, x, tym, y, tzm, z):
		if txm < tym:
			if txm < tzm:
				return x # YZ plane.
		else:
			if tym < tzm:
				return y # XZ plane.
		return z # XY plane.


	def test_print_node(self, curr_min, curr_max, index, lvl):
		print 'index', index, 'level', lvl, 'node', self.nodes[index]
		print curr_min.x, curr_min.y, curr_min.z
		print curr_max.x, curr_max.y, curr_max.z

	"""
	void proc_subtree (double tx0, double ty0, double tz0, double tx1, double ty1, double tz1, Node* node)
	{
		float txm, tym, tzm;
		int currNode;

		if(tx1 < 0 || ty1 < 0 || tz1 < 0) return;
		if(node->terminal)
		{
			cout << "Reached leaf node " << node->debug_ID << endl;
			return;
		}
		else
		{
			cout << "Reached node " << node->debug_ID << endl;
		}

		txm = 0.5*(tx0 + tx1);
		tym = 0.5*(ty0 + ty1);
		tzm = 0.5*(tz0 + tz1);

		currNode = first_node(tx0,ty0,tz0,txm,tym,tzm);
		do
		{
			switch (currNode)
			{
			case 0:
				proc_subtree(tx0,ty0,tz0,txm,tym,tzm,node->children[a]);
				currNode = new_node(txm,4,tym,2,tzm,1);
				break;
			case 1:
				proc_subtree(tx0,ty0,tzm,txm,tym,tz1,node->children[1^a]);
				currNode = new_node(txm,5,tym,3,tz1,8);
				break;
			case 2:
				proc_subtree(tx0,tym,tz0,txm,ty1,tzm,node->children[2^a]);
				currNode = new_node(txm,6,ty1,8,tzm,3);
				break;
			case 3:
				proc_subtree(tx0,tym,tzm,txm,ty1,tz1,node->children[3^a]);
				currNode = new_node(txm,7,ty1,8,tz1,8);
				break;
			case 4:
				proc_subtree(txm,ty0,tz0,tx1,tym,tzm,node->children[4^a]);
				currNode = new_node(tx1,8,tym,6,tzm,5);
				break;
			case 5:
				proc_subtree(txm,ty0,tzm,tx1,tym,tz1,node->children[5^a]);
				currNode = new_node(tx1,8,tym,7,tz1,8);
				break;
			case 6:
				proc_subtree(txm,tym,tz0,tx1,ty1,tzm,node->children[6^a]);
				currNode = new_node(tx1,8,ty1,8,tzm,7);
				break;
			case 7:
				proc_subtree(txm,tym,tzm,tx1,ty1,tz1,node->children[7^a]);
				currNode = 8;
				break;
			}
		} while (currNode < 8);
	}
	"""
	# Some of these values aren't actually necessary except for debugging, like the aabb stuff.
	def test_proc_subtree_recursive(self, aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, node_val, curr_lvl, index):
		# We've just entered a subtree from the level above.
		if tx1 < 0 or ty1 < 0 or tz1 < 0:
			return False

		# If it's a terminal node, and the node exists, then we need to exit.
		# TODO: This particular node may not actually be the node in this subtree that contains the geometry.
		#if curr_lvl == self.n_levels - 1:
		#	if self.nodes[index + node_val] == True:
		#		curr_min, curr_mid, curr_max = get_aabb(node_val, aabb_min, aabb_mid, aabb_max)
		#		self.test_print_node(curr_min, curr_max, index + node_val, curr_lvl)
		#		return True
		#	else:
		#		return False
		# TODO: As an alternative, if this function was called and curr_lvl == self.n_levels, then we know the parent was a leaf node that existed, so the raycast ends there.
		# TODO: Problem with this approach is that it requires an extra unnecessary level on the stack.
		if curr_lvl == self.n_levels:
			parent_node = (index + node_val) / 8 - 1
			self.test_print_node(aabb_min, aabb_max, parent_node, curr_lvl - 1)
			for i in xrange(len(self.vals)):
				if self.vals[i].x == aabb_min.x and self.vals[i].y == aabb_min.y and self.vals[i].z == aabb_min.z:
					print 'node exists in vals'
			return True

		txm = 0.5 * (tx0 + tx1)
		tym = 0.5 * (ty0 + ty1)
		tzm = 0.5 * (tz0 + tz1)

		# We pick the first node on this level that we want to enter.
		# Remember that curr_node needs to be x-ord with self.a to account for rays with neg dir.
		curr_node = self.test_first_node(tx0, ty0, tz0, txm, tym, tzm)
		curr_index = index + (curr_node ^ self.a)

		# Calculate the aabb based on the first node.
		curr_min, curr_mid, curr_max = get_aabb(curr_node ^ self.a, aabb_min, aabb_mid, aabb_max)
		self.test_print_node(curr_min, curr_max, curr_index, curr_lvl)

		# Now we check the rest of the nodes by first checking the node's children, then checking the next sibling until all siblings are checked.
		while True:
			self.n_ops += 1
			self.n_reads += 1
			child_index = (curr_index + 1) * 8
			val = self.nodes[curr_index]
			if curr_node == 0:
				if val != 0 and self.test_proc_subtree_recursive(curr_min, curr_mid, curr_max, tx0, ty0, tz0, txm, tym, tzm, self.a, curr_lvl + 1, child_index) == True:
					return True
				curr_node = self.test_new_node(txm, 4, tym, 2, tzm, 1)
			elif curr_node == 1:
				if val != 0 and self.test_proc_subtree_recursive(curr_min, curr_mid, curr_max, tx0, ty0, tzm, txm, tym, tz1, 1 ^ self.a, curr_lvl + 1, child_index) == True:
					return True
				curr_node = self.test_new_node(txm, 5, tym, 3, tz1, 8)
			elif curr_node == 2:
				if val != 0 and self.test_proc_subtree_recursive(curr_min, curr_mid, curr_max, tx0, tym, tz0, txm, ty1, tzm, 2 ^ self.a, curr_lvl + 1, child_index) == True:
					return True
				curr_node = self.test_new_node(txm, 6, ty1, 8, tzm, 3)
			elif curr_node == 3:
				if val != 0 and self.test_proc_subtree_recursive(curr_min, curr_mid, curr_max, tx0, tym, tzm, txm, ty1, tz1, 3 ^ self.a, curr_lvl + 1, child_index) == True:
					return True
				curr_node = self.test_new_node(txm, 7, ty1, 8, tz1, 8)
			elif curr_node == 4:
				if val != 0 and self.test_proc_subtree_recursive(curr_min, curr_mid, curr_max, txm, ty0, tz0, tx1, tym, tzm, 4 ^ self.a, curr_lvl + 1, child_index) == True:
					return True
				curr_node = self.test_new_node(tx1, 8, tym, 6, tzm, 5)
			elif curr_node == 5:
				if val != 0 and self.test_proc_subtree_recursive(curr_min, curr_mid, curr_max, txm, ty0, tzm, tx1, tym, tz1, 5 ^ self.a, curr_lvl + 1, child_index) == True:
					return True
				curr_node = self.test_new_node(tx1, 8, tym, 7, tz1, 8)
			elif curr_node == 6:
				if val != 0 and self.test_proc_subtree_recursive(curr_min, curr_mid, curr_max, txm, tym, tz0, tx1, ty1, tzm, 6 ^ self.a, curr_lvl + 1, child_index) == True:
					return True
				curr_node = self.test_new_node(tx1, 8, ty1, 8, tzm, 7)
			elif curr_node == 7:
				if val != 0 and self.test_proc_subtree_recursive(curr_min, curr_mid, curr_max, txm, tym, tzm, tx1, ty1, tz1, 7 ^ self.a, curr_lvl + 1, child_index) == True:
					return True
				curr_node = 8
			elif curr_node == 8:
				break
			# Remember that curr_node needs to be x-ord with self.a to account for rays with neg dir.
			curr_min, curr_mid, curr_max = get_aabb(curr_node ^ self.a, aabb_min, aabb_mid, aabb_max)
			curr_index = index + (curr_node ^ self.a)
			self.test_print_node(curr_min, curr_max, curr_index, curr_lvl)
		return False


	# Some of these values aren't actually necessary except for debugging, like the aabb stuff.
	# FIXME: tx0, ty0, tz0, tx1, ty1, tz1... can these be packed or compressed?
	# FIXME: node_val doesn't need to be stored in the stack either.
	# FIXME: Can curr_lvl be inferred somehow?
	# FIXME: Can index be inferred somehow?
	def test_proc_subtree_iterative(self, aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, node_val, curr_lvl, index):
		# Stack of values that need to be checked.
		max_stack = 1
		stack = []
		stack.append((aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, node_val, curr_lvl, index))

		while len(stack) > 0:
			if len(stack) > max_stack:
				max_stack = len(stack)
			# We've just entered a subtree from the level above.
			aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, node_val, curr_lvl, index = stack.pop()

			if tx1 < 0 or ty1 < 0 or tz1 < 0:
				continue

			# If it's a terminal node, and the node exists, then we need to exit.
			# TODO: This particular node may not actually be the node in this subtree that contains the geometry.
			#if curr_lvl == self.n_levels - 1:
			#	if self.nodes[index + node_val] == True:
			#		curr_min, curr_mid, curr_max = get_aabb(node_val, aabb_min, aabb_mid, aabb_max)
			#		self.test_print_node(curr_min, curr_max, index + node_val, curr_lvl)
			#		return True
			#	else:
			#		return False
			# TODO: As an alternative, if this function was called and curr_lvl == self.n_levels, then we know the parent was a leaf node that existed, so the raycast ends there.
			# TODO: Problem with this approach is that it requires an extra unnecessary level on the stack.
			if curr_lvl == self.n_levels:
				parent_node = (index + node_val) / 8 - 1
				self.test_print_node(aabb_min, aabb_max, parent_node, curr_lvl - 1)
				for i in xrange(len(self.vals)):
					if self.vals[i].x == aabb_min.x and self.vals[i].y == aabb_min.y and self.vals[i].z == aabb_min.z:
						print 'node exists in vals'
				print 'max_stack', max_stack
				return True

			txm = 0.5 * (tx0 + tx1)
			tym = 0.5 * (ty0 + ty1)
			tzm = 0.5 * (tz0 + tz1)

			# We pick the first node on this level that we want to enter.
			# Remember that curr_node needs to be x-ord with self.a to account for rays with neg dir.
			curr_node = self.test_first_node(tx0, ty0, tz0, txm, tym, tzm)
			curr_index = index + (curr_node ^ self.a)

			# Calculate the aabb based on the first node.
			curr_min, curr_mid, curr_max = get_aabb(curr_node ^ self.a, aabb_min, aabb_mid, aabb_max)
			self.test_print_node(curr_min, curr_max, curr_index, curr_lvl)
			print tx0, txm, tx1, ty0, tym, ty1, tz0, tzm, tz1

			# Now we check the rest of the nodes by first checking the node's children, then checking the next sibling until all siblings are checked.
			while True:
				self.n_ops += 1
				self.n_reads += 1
				child_index = (curr_index + 1) * 8
				if curr_node == 0:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, tx0, ty0, tz0, txm, tym, tzm, self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(txm, 4, tym, 2, tzm, 1)
				elif curr_node == 1:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, tx0, ty0, tzm, txm, tym, tz1, 1 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(txm, 5, tym, 3, tz1, 8)
				elif curr_node == 2:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, tx0, tym, tz0, txm, ty1, tzm, 2 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(txm, 6, ty1, 8, tzm, 3)
				elif curr_node == 3:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, tx0, tym, tzm, txm, ty1, tz1, 3 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(txm, 7, ty1, 8, tz1, 8)
				elif curr_node == 4:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, txm, ty0, tz0, tx1, tym, tzm, 4 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(tx1, 8, tym, 6, tzm, 5)
				elif curr_node == 5:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, txm, ty0, tzm, tx1, tym, tz1, 5 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(tx1, 8, tym, 7, tz1, 8)
				elif curr_node == 6:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, txm, tym, tz0, tx1, ty1, tzm, 6 ^ self.a, curr_lvl + 1, child_index))
					curr_node = self.test_new_node(tx1, 8, ty1, 8, tzm, 7)
				elif curr_node == 7:
					if self.nodes[curr_index] != 0:
						stack.append((curr_min, curr_mid, curr_max, txm, tym, tzm, tx1, ty1, tz1, 7 ^ self.a, curr_lvl + 1, child_index))
					curr_node = 8
				elif curr_node == 8:
					break
				# Remember that curr_node needs to be x-ord with self.a to account for rays with neg dir.
				curr_min, curr_mid, curr_max = get_aabb(curr_node ^ self.a, aabb_min, aabb_mid, aabb_max)
				curr_index = index + (curr_node ^ self.a)
				self.test_print_node(curr_min, curr_max, curr_index, curr_lvl)
		print 'max_stack', max_stack
		return False


	"""
	void ray_octree_traversal(Octree* octree, Ray ray)
	{
		a = 0;

		// fixes for rays with negative direction
		if(ray.direction[0] < 0)
		{
			ray.origin[0] = octree->size[0] - ray.origin[0];
			ray.direction[0] = - ray.direction[0];
			a |= 4 ; //bitwise OR (latest bits are XYZ)
		}
		if(ray.direction[1] < 0)
		{
			ray.origin[1] = octree->size[1] - ray.origin[1];
			ray.direction[1] = - ray.direction[1];
			a |= 2 ; 
		}
		if(ray.direction[2] < 0)
		{
			ray.origin[2] = octree->size[2] - ray.origin[2];
			ray.direction[2] = - ray.direction[2];
			a |= 1 ; 
		}

		double divx = 1 / ray.direction[0]; // IEEE stability fix
		double divy = 1 / ray.direction[1];
		double divz = 1 / ray.direction[2];

		double tx0 = (octree->min[0] - ray.origin[0]) * divx;
		double tx1 = (octree->max[0] - ray.origin[0]) * divx;
		double ty0 = (octree->min[1] - ray.origin[1]) * divy;
		double ty1 = (octree->max[1] - ray.origin[1]) * divy;
		double tz0 = (octree->min[2] - ray.origin[2]) * divz;
		double tz1 = (octree->max[2] - ray.origin[2]) * divz;

		if(max(max(tx0,ty0),tz0) < min(min(tx1,ty1),tz1))
		{
			proc_subtree(tx0,ty0,tz0,tx1,ty1,tz1,octree->root);
		}
	}
	"""
	def test_ray_traversal(self, start_pos, end_pos):
		print 'Starting smart traversal'
		self.n_ops = 0
		self.n_reads = 0

		# Swap x/y, since the algorithm assumes an ordering of ZYX, whereas our format is ZXY.
		start_pos.x, start_pos.y = start_pos.y, start_pos.x
		end_pos.x, end_pos.y = end_pos.y, end_pos.x

		self.a = 0
		ray_dir = Point(end_pos.x - start_pos.x, end_pos.y - start_pos.y, end_pos.z - start_pos.z)
		l = math.sqrt(ray_dir.x * ray_dir.x + ray_dir.y * ray_dir.y + ray_dir.z * ray_dir.z)
		ray_dir.x /= l
		ray_dir.y /= l
		ray_dir.z /= l

		# Fixes for rays with negative direction.
		if ray_dir.x < 0:
			start_pos.x = self.size - start_pos.x
			ray_dir.x = -ray_dir.x
			self.a |= 4
		if ray_dir.y < 0:
			start_pos.y = self.size - start_pos.y
			ray_dir.y = -ray_dir.y
			self.a |= 2
		if ray_dir.z < 0:
			start_pos.z = self.size - start_pos.z
			ray_dir.z = -ray_dir.z
			self.a |= 1

		div_x = 1.0 / ray_dir.x
		div_y = 1.0 / ray_dir.y
		div_z = 1.0 / ray_dir.z

		tx0 = (0 - start_pos.x) * div_x
		tx1 = (self.size - start_pos.x) * div_x
		ty0 = (0 - start_pos.y) * div_y
		ty1 = (self.size - start_pos.y) * div_y
		tz0 = (0 - start_pos.z) * div_z
		tz1 = (self.size - start_pos.z) * div_z

		aabb_min = Point(0, 0, 0)
		aabb_max = Point(self.size, self.size, self.size)
		aabb_mid = Point(self.size / 2, self.size / 2, self.size / 2)

		found = False
		if max(max(tx0, ty0), tz0) < min(min(tx1, ty1), tz1):
			#found = self.test_proc_subtree_recursive(aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, 0, 0, 0)
			found = self.test_proc_subtree_iterative(aabb_min, aabb_mid, aabb_max, tx0, ty0, tz0, tx1, ty1, tz1, 0, 0, 0)
		if found == True:
			print 'Found!'
		else:
			print 'No leaf node found!'





def test_octree():
	octree = Octree()
	octree.build_tree_full()
	print 'nlevels:', octree.n_levels

	octree.find_nodes_full(Point(100, 100, 100))
	#octree.build_tree_sparse()
	#octree.find_nodes_sparse(Point(100, 100, 100))
	seed(10)

	#start_pos = Point(randint(0, octree.size), randint(0, octree.size), randint(0, octree.size))
	#end_pos = Point(randint(0, octree.size), randint(0, octree.size), randint(0, octree.size))
	start_pos = Point(20, 50, 10)
	end_pos = Point(80, 100, 50)

	print 'start', start_pos.x, start_pos.y, start_pos.z
	print 'end', end_pos.x, end_pos.y, end_pos.z

	#octree.traverse_grid_sparse(start_pos, end_pos)
	#octree.print_dda_path(start_pos, end_pos, True)

	# FIXME: These disagree with the dda, but that's not necessarily a bad thing.
	octree.traverse_grid_full(start_pos, end_pos)
	print 'n_ops', octree.n_ops, 'n_reads', octree.n_reads, '\n'

	# TODO: Implement this with a stack to see how it'll work.
	octree.test_ray_traversal(start_pos, end_pos)
	print 'n_ops', octree.n_ops, 'n_reads', octree.n_reads, '\n'

	octree.print_dda_path(start_pos, end_pos, False)
	print 'n_ops', octree.n_ops, 'n_reads', octree.n_reads, '\n'



test_octree()

