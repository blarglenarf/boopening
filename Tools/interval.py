# CENTERED INTERVAL TREE
#
# Given a set of n intervals on the number line, we want to construct a data structure so that we can efficiently retrieve
# all intervals overlapping another interval or point.
#
# We start by taking the entire range of all the intervals and dividing it in half at x_center (in practice, x_center should
# be picked to keep the tree relatively balanced). This gives three sets of intervals, those completely to the left of x_center
# which we'll call S_left, those completely to the right of x_center which we'll call S_right, and those overlapping x_center which we'll call S_center.
#
# The intervals in S_left and S_right are recursively divided in the same manner until there are no intervals left.
#
# The intervals in S_center that overlap the center point are stored in a separate data structure linked to the node in the
# interval tree. This data structure consists of two lists, one containing all the intervals sorted by their beginning points,
# and another containing all the intervals sorted by their ending points.
#
#
# The result is a ternary tree with each node storing:
#
# * A center point
# * A pointer to another node containing all intervals completely to the left of the center point
# * A pointer to another node containing all intervals completely to the right of the center point
# * All intervals overlapping the center point sorted by their beginning point
# * All intervals overlapping the center point sorted by their ending point

class CenteredNode:

	def __init__(self, lo, hi):
		self.lo, self.hi = lo, hi
		self.centre = (lo + hi) / 2.0 # Only using positive numbers here anyway.
		self.min_overlap = 0
		self.max_overlap = 0
		self.left = None
		self.right = None


	def print_vals(self):
		print 'lo, hi, centre, min_overlap, max_overlap: ', self.lo, self.hi, self.centre, self.min_overlap, self.max_overlap
		if self.left != None:
			print 'left',
			self.left.print_vals()
		if self.right != None:
			print 'right',
			self.right.print_vals()



class CenteredArrayVal:

	def __init__(self, lo, hi, min_interval, max_interval):
		self.lo, self.hi = lo, hi
		self.min_overlap, self.max_overlap = 0, 0
		self.min_interval, self.max_interval = min_interval, max_interval # TODO: These are only needed during construction, not traversal.


	def print_vals(self):
		centre = (self.lo + self.hi) / 2.0
		print 'lo, hi, centre, min_overlap, max_overlap, min_interval, max_interval: ', self.lo, self.hi, centre, self.min_overlap, self.max_overlap, self.min_interval, self.max_interval



class CenteredIntervalTree:

	def __init__(self, intervals, reverse_order = False):
		self.reverse_order = reverse_order
		self.create_tree(intervals) # TODO: have this handle reverse order intervals.
		self.create_array(intervals)


	# First the array implementation (the one needed for glsl).
	def create_array(self, intervals):
		self.array = [None] * len(intervals)

		# Root at 0 is median.
		min_interval, max_interval = 0, len(intervals) - 1
		curr_loc = 0
		done = False

		# Start by adding the root and finding its overlaps.
		self.add_array_interval(intervals, min_interval, max_interval, curr_loc)

		# Keep looping until there are no more intervals remaining.
		while not done:
			# TODO: Figure out a better terminating condition...
			if curr_loc >= len(intervals):
				done = True
				break

			# If the current interval is empty, continue to the next one.
			if self.array[curr_loc] is None:
				curr_loc += 1
				continue

			# If the current interval has any children, continue to the next interval.
			if self.array[curr_loc * 2 + 1] is not None or self.array[curr_loc * 2 + 2] is not None:
				curr_loc += 1
				continue

			parent = self.array[curr_loc]

			# Start with the left child.
			if self.reverse_order:
				left_min_interval, left_max_interval = parent.max_overlap + 1, parent.max_interval
			else:
				left_min_interval, left_max_interval = parent.min_interval, parent.min_overlap - 1

			# Then the right.
			if self.reverse_order:
				right_min_interval, right_max_interval = parent.min_interval, parent.min_overlap - 1
			else:
				right_min_interval, right_max_interval = parent.max_overlap + 1, parent.max_interval

			# Only add them if the intervals make sense.
			left_add, right_add = False, False
			if left_min_interval <= left_max_interval:
				self.add_array_interval(intervals, left_min_interval, left_max_interval, curr_loc * 2 + 1)
				left_add = True
			if right_min_interval <= right_max_interval:
				self.add_array_interval(intervals, right_min_interval, right_max_interval, curr_loc * 2 + 2)
				right_add = True

			# TODO: Test this a bit more, or prove that it's ok for a terminating condition.
			# If we didn't add anything, then we can quit.
			if not left_add and not right_add:
				done = True
				break

			# Now go to the next inerval.
			curr_loc += 1


	def add_array_interval(self, intervals, min_interval, max_interval, curr_loc):
		median = (min_interval + max_interval) / 2
		if self.reverse_order:
			hi, lo = intervals[median]
		else:
			lo, hi = intervals[median]
		self.array[curr_loc] = CenteredArrayVal(lo, hi, min_interval, max_interval)

		# Set the overlapping elements of the current interval.
		self.set_overlapping(intervals, median, min_interval, max_interval, curr_loc)


	def set_overlapping(self, intervals, median, min_interval, max_interval, curr_loc):
		# Loop through intervals to the left to find overlapping ones.
		if self.reverse_order:
			hi, lo = intervals[median]
		else:
			lo, hi = intervals[median]
		min_i = median - 1
		max_i = median + 1
		while min_i >= min_interval:
			if self.reverse_order:
				new_max, new_min = intervals[min_i]
			else:
				new_min, new_max = intervals[min_i]
			# Still overlapping?
			if self.reverse_order and new_min > hi:
				break
			elif not self.reverse_order and new_max < lo:
				break
			min_i -= 1
		while max_i <= max_interval:
			if self.reverse_order:
				new_max, new_min = intervals[max_i]
			else:
				new_min, new_max = intervals[max_i]
			# Still overlapping?
			if self.reverse_order and new_max < lo:
				break
			elif not self.reverse_order and new_min > hi:
				break
			max_i += 1
		self.array[curr_loc].min_overlap = min_i + 1
		self.array[curr_loc].max_overlap = max_i - 1


	def find_intervals_array(self, intervals, val):
		curr_loc = 0
		done = False
		while not done:
			if self.array[curr_loc] is None:
				done = True
				break
			curr = self.array[curr_loc]
			if val < curr.lo:
				# Go left, unless left is too small, then check the overlaps here.
				new_loc = curr_loc * 2 + 1
				if self.array[new_loc].hi >= val:
					# It's not too small, proceed.
					curr_loc = new_loc
					continue
			elif val > curr.hi:
				# Go right, unless right is too large, then check the overlaps here.
				new_loc = curr_loc * 2 + 2
				if self.array[new_loc].lo <= val:
					# It's not too large, proceed.
					curr_loc = new_loc
					continue

			# We're here, check this and all its overlaps.
			for i in range(curr.min_overlap, curr.max_overlap + 1):
				lo, hi = intervals[i]
				if val >= lo and val <= hi:
					print 'loc, interval, lo, hi: ', curr_loc, i, lo, hi
			done = True


	# Then the node implementation for testing and comparison.
	def create_tree(self, intervals):
		self.root = self.create_node(intervals, 0, len(intervals) - 1)


	def create_node(self, intervals, min_interval, max_interval):
		median = (min_interval + max_interval) / 2
		lo, hi = intervals[median]
		node = CenteredNode(lo, hi)

		# Loop through intervals to the left of the median and right of the median to find the overlapping ones.
		min_i = median - 1
		while min_i >= min_interval:
			new_min, new_max = intervals[min_i]
			if new_max < lo:
				node.left = self.create_node(intervals, min_interval, min_i)
				break
			min_i -= 1
		max_i = median + 1
		while max_i <= max_interval:
			new_min, new_max = intervals[max_i]
			if new_min > hi:
				node.right = self.create_node(intervals, max_i, max_interval)
				break
			max_i += 1

		# Record intervals that overlap this one.
		node.min_overlap = min_i + 1
		node.max_overlap = max_i - 1
		return node


	def print_tree(self):
		if self.root is None:
			return
		print 'root',
		self.root.print_vals()

		for i in range(0, len(self.array)):
			if self.array[i] is not None:
				self.array[i].print_vals()



# Do I really need to implement the augmented version?
class AugmentedNode:

	def __init__(self, parent, lo, hi):
		self.parent = parent
		self.lo, self.hi = lo, hi
		self.left, self.right = None, None
		self.min_val, self.max_val = lo, hi


	def print_vals(self):
		print 'lo, hi, min_val, max_val: ', self.lo, self.hi, self.min_val, self.max_val
		if self.left is not None:
			print 'left',
			self.left.print_vals()
		if self.right is not None:
			print 'right',
			self.right.print_vals()


class AugmentedArrayVal:

	def __init__(self, lo, hi, min_interval, max_interval):
		self.lo, self.hi = lo, hi
		self.min_interval, self.max_interval = min_interval, max_interval
		self.min_val, self.max_val = lo, hi


	def print_vals(self):
		print 'lo, hi, min_val, max_val, min_interval, max_interval: ', self.lo, self.hi, self.min_val, self.max_val, self.min_interval, self.max_interval



class AgumentedIntervalTree:

	def __init__(self, intervals):
		self.create_tree(intervals)
		self.create_array(intervals)


	# Iterative array implementation for glsl.
	def create_array(self, intervals):
		self.array = [None] * len(intervals) * 2

		# Root at 0 is median.
		min_interval, max_interval = 0, len(intervals) - 1
		curr_loc = 0
		done = False

		# Add the root node first.
		self.add_array_interval(intervals, min_interval, max_interval, curr_loc)

		# TODO: add these nodes properly (one median at a time).
		while not done:
			# TODO: Figure out a better terminating condition...
			if curr_loc > len(intervals):
				done = True
				break

			# If the current interval is empty, continue to the next one.
			if self.array[curr_loc] is None:
				curr_loc += 1
				continue

			# If the current interval has any children, continue to the next interval.
			if self.array[curr_loc * 2 + 1] is not None or self.array[curr_loc * 2 + 2] is not None:
				curr_loc += 1
				continue

			parent = self.array[curr_loc]

			# Start with the left child.
			left_min_interval, left_max_interval = parent.min_interval, (parent.min_interval + parent.max_interval) / 2 - 1

			# Then the right.
			right_min_interval, right_max_interval = (parent.min_interval + parent.max_interval) / 2 + 1, parent.max_interval

			# Only add the children if the intervals make sense.
			left_add, right_add = False, False
			if left_min_interval <= left_max_interval:
				self.add_array_interval(intervals, left_min_interval, left_max_interval, curr_loc * 2 + 1)
				left_add = True
			if right_min_interval <= right_max_interval:
				self.add_array_interval(intervals, right_min_interval, right_max_interval, curr_loc * 2 + 2)
				right_add = True

			# Now go to the next inerval.
			curr_loc += 1


	def add_array_interval(self, intervals, min_interval, max_interval, curr_loc):
		# Add the interval to the array.
		median = (min_interval + max_interval) / 2
		lo, hi = intervals[median]
		self.array[curr_loc] = AugmentedArrayVal(lo, hi, min_interval, max_interval)

		# Go through parent nodes and update their min/max vals.
		parent = (curr_loc - 1) / 2
		while parent >= 0:
			if self.array[parent].min_val > lo:
				self.array[parent].min_val = lo
			if self.array[parent].max_val < hi:
				self.array[parent].max_val = hi
			parent = (parent - 1) / 2


	def find_intervals_array(self, val):
		prev_loc = -1
		curr_loc = 0
		done = False
		while not done:
			if self.array[curr_loc] is None:
				done = True
				break
			curr = self.array[curr_loc]

			# Are matches actually possible?
			if val < curr.min_val or val > curr.max_val:
				done = True
				break

			if val >= curr.lo and val <= curr.hi:
				# We're here.
				print 'loc, lo, hi: ', curr_loc, curr.lo, curr.hi



	# Node implementation for testing and comparison.
	def create_tree(self, intervals):
		self.root = self.create_node(None, intervals, 0, len(intervals) - 1)


	def create_node(self, parent, intervals, min_interval, max_interval):
		# Can't create a node if the intervals are invalid.
		if max_interval < min_interval:
			return None

		median = (min_interval + max_interval) / 2
		lo, hi = intervals[median]
		node = AugmentedNode(parent, lo, hi)

		# Walk back up the tree and change the min/max values.
		parent = node.parent
		while parent is not None:
			if parent.min_val > node.lo:
				parent.min_val = node.lo
			if parent.max_val < node.hi:
				parent.max_val = node.hi
			parent = parent.parent

		# Left node is between min and median - 1.
		node.left = self.create_node(node, intervals, min_interval, median - 1)

		# Right node is between median + 1 and max.
		node.right = self.create_node(node, intervals, median + 1, max_interval)

		return node

	def print_tree(self):
		if self.root is None:
			return
		print 'root',
		self.root.print_vals()

		for i in range(0, len(self.array)):
			if self.array[i] is not None:
				self.array[i].print_vals()



def gen_rand_intervals():
	# TODO: just a whole lot of random numbers I guess?
	# TODO: for each number generated between 0 and 30, generate a second number which is between -3 and +3 of the original.
	# TODO: add those two numbers as a new interval.
	# TODO: compare the difference between sorting and not sorting these intervals (I think if the intervals aren't sorted, then the tree must be augmented).
	pass



def test_tree():
	print 'Testing'
	# TODO: test this with larger set of intervals...

	# In-order.
	intervals = [(0.4, 1.1), (0.6, 2.1), (1.9, 2.7), (2.0, 2.5), (2.9, 4.0), (3.1, 3.7), (3.5, 4.1), (4.2, 4.8), (4.5, 5.0)]

	# Reverse order (the order they they'll be saved in glsl, note that they'll be read back in reverse order).
	intervals = [(5.0, 4.5), (4.8, 4.2), (4.1, 3.5), (4.0, 2.9), (3.7, 3.1), (2.7, 1.9), (2.5, 2.0), (2.1, 0.6), (1.1, 0.4)]

	# Actual order (in-order should work for this one too, test to find out). FIXME: might not be the actual order, need to test to find out!!!
	intervals = [(0.4, 1.1), (0.6, 2.1), (2.0, 2.5), (1.9, 2.7), (3.1, 3.7), (2.9, 4.0), (3.5, 4.1), (4.2, 4.8), (4.5, 5.0)]

	# List needs to be sorted (I'll just assume that was already done).
	# We also need to step through to find the intervals (I'll just assume this was already done too).

	# Insert the intervals into the tree, using the median as the root.
	print 'intervals: ', intervals
	print 'centered'
	tree = CenteredIntervalTree(intervals, False)
	tree.print_tree()

	print 'augmented'
	augment = AgumentedIntervalTree(intervals)
	augment.print_tree()

	# Now find some stuff.
	print 'find centered'
	tree.find_intervals_array(intervals, 2.0)

	print 'find augmented'
	augment.find_intervals_array(2.0)


test_tree()
