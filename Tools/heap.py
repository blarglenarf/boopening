from random import *



def create_random_list(size, s = 0):
	seed(s)
	l = [uniform(0, 10) for i in range(size)]
	candidate = [(d, j) for j, d in enumerate(l)]
	candidate_sorted = [j for f, j in sorted(candidate)]
	return l#, candidate_sorted



# TODO: Merge blocks using the heap.
# TODO: Non-recursion.
# TODO: Loop unrolling.
class BinaryHeap:

	def __init__(self):
		self.maxFrags = 256
		self.maxRegisters = 16
		self.blockSize = 16
		self.mergeSize = self.maxFrags / self.blockSize
		self.next = [0] * self.mergeSize
		self.data = create_random_list(self.maxFrags)
		self.sortedData = [0] * self.maxFrags
		self.heap = [0] * self.maxRegisters
		self.endPos = 1

	def heapAdd(self, left, right):
		if right == 0:
			return
		b = self.heap[left - 1] < self.heap[right - 1]
		if b:
			tmp = self.heap[left - 1]
			self.heap[left - 1] = self.heap[right - 1]
			self.heap[right - 1] = tmp
			self.heapAdd(right, right >> 1)

	def heapRemove(self, pos):
		left = pos * 2
		right = pos * 2 + 1
		tmp = self.heap[pos - 1]
		if left < self.endPos and right < self.endPos:
			if self.heap[left - 1] < self.heap[pos - 1] or self.heap[right - 1] < self.heap[pos - 1]:
				if self.heap[left - 1] < self.heap[right - 1]:
					self.heap[pos - 1] = self.heap[left - 1]
					self.heap[left - 1] = tmp
					self.heapRemove(left)
				else:
					self.heap[pos - 1] = self.heap[right - 1]
					self.heap[right - 1] = tmp
					self.heapRemove(right)
		elif left < self.endPos and self.heap[left - 1] < self.heap[pos - 1]:
			self.heap[pos - 1] = self.heap[left - 1]
			self.heap[left - 1] = tmp
			self.heapRemove(left)
		elif right < self.endPos and self.heap[right - 1] < self.heap[pos - 1]:
			self.heap[pos - 1] = self.heap[right - 1]
			self.heap[right - 1] = self.heap[pos - 1]
			self.heapRemove(right)


	def empty(self):
		return self.endPos == 1

	def top(self):
		return self.heap[0]

	def push(self, i):
		self.heap[self.endPos - 1] = i
		self.heapAdd(self.endPos, self.endPos >> 1)
		self.endPos += 1

	def pop(self):
		if self.endPos == 1:
			return
		i = self.heap[0]
		self.endPos -= 1
		self.heap[0] = self.heap[self.endPos - 1]
		if self.endPos < self.mergeSize:
			self.heap[self.endPos] = 0
		self.heapRemove(1)
		return i

	def sortData(self):
		block = [0] * self.blockSize

		# Sort each individual block.
		for i in range(0, len(self.data), self.blockSize):
			for j in range(0, self.blockSize):
				block[j] = self.data[i + j]
			sortedBlock = sorted(block)
			for j in range(0, self.blockSize):
				self.data[i + j] = sortedBlock[j]

		# Merge counters.
		for i in range(self.mergeSize):
			self.next[i] = (i + 1) * self.maxRegisters - 1
			self.heap[i] = self.data[self.next[i]]

		# TODO: Merge blocks using a heap.
		k = self.maxFrags - 1
		for i in range(self.maxFrags):
			n = 0
			f = 0
			for j in range(0, self.mergeSize):
				if self.next[j] >= j * self.maxRegisters:
					if self.heap[j] > f:
						f = self.heap[j]
						n = j
			#for j in range(0, self.mergeSize):
			#	if n == j:
			#		if self.next[j] - 1 >= j * self.maxRegisters:
			#			self.next[j] -= 1
			#			self.heap[j] = self.data[self.next[j]]
			self.next[n] -= 1
			if self.next[n] >= n * self.maxRegisters:
				self.heap[n] = self.data[self.next[n]]
			self.sortedData[k] = f
			k -= 1





class RegisterBinaryHeap:

	def __init__(self):
		self.maxFrags = 512
		self.maxRegisters = 32
		self.blockSize = 32
		self.mergeSize = self.maxFrags / self.blockSize
		self.next = [0] * self.mergeSize
		self.indices = [0] * self.maxRegisters
		self.heap = [0] * self.maxRegisters
		self.data = create_random_list(self.maxFrags) # Not needed in the glsl version.
		self.sortedData = [0] * self.maxFrags # Not needed in the glsl version.

	def downHeapBlocks(self, n, k):
		index, value = 0, 0

		# For loop selection, auto unrolling.
		for i in range(self.maxRegisters):
			if i == k:
				index = self.indices[i]
				value = self.heap[i]
				break

		# This cannot be unrolled!!!!!!
		while k + k + 1 < n:
			i = k + k + 1

			# Unrolled just in case.
			for j in range(self.maxRegisters):
				if j == i and j + 1 < n and self.heap[j] < self.heap[j + 1]:
					i += 1
					break
			#
			#if i + 1 < n and self.heap[i] , self.heap[i + 1]:
			#	i += 1

			# Unrolled just in case.
			tmpIndex, tmpVal = 0, 0
			fin = False
			for j in range(self.maxRegisters):
				if j == i:
					if value >= self.heap[j]:
						fin = True
						break
					tmpIndex = self.indices[j]
					tmpVal = self.heap[j]
			if fin == True:
				break
			#
			#tmpIndex, tmpVal = self.indices[i], self.heap[i]
			#if value >= self.heap[i]:
			#	break

			# For loop selection, auto unrolling.
			for j in range(self.maxRegisters):
				if j == k:
					self.heap[j] = tmpVal
					self.indices[j] = tmpIndex
					break
			k = i

		# For loop selection, auto unrolling.
		for i in range(self.maxRegisters):
			if i == k:
				self.heap[i] = value
				self.indices[i] = index
				break


	def assignHead(self, k):
		self.heap[0], self.indices[0] = self.heap[k], self.indices[k]


	def mergeHeapBlocks(self, n):
		# Initialise.
		# I'm sure glsl can at least unroll this right?
		for i in range(self.mergeSize):
			# TODO: Also check to make sure it's less than merge count.
			self.next[i] = (i + 1) * self.blockSize - 1
			self.heap[i] = self.data[self.next[i]]
			self.indices[i] = i

		# Heapify.
		# TODO: Probably use merge count instead.
		k = self.mergeSize / 2 - 1
		while k >= 0:
			self.downHeapBlocks(self.mergeSize, k)
			k -= 1

		# Repeated min finding using heap.
		# TODO: Probably use merge count instead.
		s = self.mergeSize
		for i in range(n):
			# Delete min and heapify.
			self.sortedData[i] = self.heap[0]

			# For loop selection, auto unrolling.
			for j in range(self.maxRegisters):
				if j == self.indices[0]:
					self.next[j] -= 1
					if self.next[j] >= j * self.blockSize:
						self.heap[0] = self.data[self.next[j]]
					else:
						s -= 1
						for k in range(self.mergeSize):
							if k == s:
								self.heap[0] = self.heap[k]
								self.indices[0] = self.indices[k]
								break
					break
			#
			#j = self.indices[0]
			#self.next[j] -= 1
			#if self.next[j] >= j * self.blockSize:
			#	self.heap[0] = self.data[self.next[j]]
			#else:
			#	self.mergeSize -= 1
			#	self.heap[0] = self.heap[self.mergeSize]
			#	self.indices[0] = self.indices[self.mergeSize]

			if s == 0:
				break
			self.downHeapBlocks(s, 0)


	def sortData(self):
		block = [0] * self.blockSize

		# Sort each individual block.
		for i in range(0, len(self.data), self.blockSize):
			for j in range(0, self.blockSize):
				block[j] = self.data[i + j]
			sortedBlock = sorted(block)
			for j in range(0, self.blockSize):
				self.data[i + j] = sortedBlock[j]

		# Merge blocks using a heap.
		self.mergeHeapBlocks(self.maxFrags)
		#print ''


"""
# FIXME: This heap sort code is buggy as hell!
class RegisterBinaryHeap:

	def __init__(self):
		self.maxFrags = 256
		self.maxRegisters = 16
		self.blockSize = 16
		self.mergeSize = self.maxFrags / self.blockSize
		self.next = [0] * self.mergeSize
		self.end = [0] * self.mergeSize
		self.blocks = [0] * self.maxRegisters
		self.heap = [0] * self.maxRegisters
		self.data = create_random_list(self.maxFrags)
		self.sortedData = [0] * self.maxFrags


	def downHeapBlocksRegistersUnrolled(self, n, k):
		i, v = 0, 0
		if k == 0:
			i = self.blocks[0]
			v = self.heap[0]
		elif k == 1:
			i = self.blocks[1]
			v = self.heap[1]
		elif k == 2:
			i = self.blocks[2]
			v = self.heap[2]
		elif k == 3:
			i = self.blocks[3]
			v = self.heap[3]
		elif k == 4:
			i = self.blocks[4]
			v = self.heap[4]
		elif k == 5:
			i = self.blocks[5]
			v = self.heap[5]
		elif k == 6:
			i = self.blocks[6]
			v = self.heap[6]
		elif k == 7:
			i = self.blocks[7]
			v = self.heap[7]
		elif k == 8:
			i = self.blocks[8]
			v = self.heap[8]
		elif k == 9:
			i = self.blocks[9]
			v = self.heap[9]
		elif k == 10:
			i = self.blocks[10]
			v = self.heap[10]
		elif k == 11:
			i = self.blocks[11]
			v = self.heap[11]
		elif k == 12:
			i = self.blocks[12]
			v = self.heap[12]
		elif k == 13:
			i = self.blocks[13]
			v = self.heap[13]
		elif k == 14:
			i = self.blocks[14]
			v = self.heap[14]
		elif k == 15:
			i = self.blocks[15]
			v = self.heap[15]

		hj, hjp1 = 0, 0
		bj, bjp1 = 0, 0
		j = 0
		while k + k + 1 < n:
			j = k + k + 1
			if j == 1:
				hj, bj = self.heap[1], self.blocks[1]
				hjp1, bjp1 = self.heap[2], self.blocks[2]
			elif j == 2:
				hj, bj = self.heap[2], self.blocks[2]
				hjp1, bjp1 = self.heap[3], self.blocks[3]
			elif j == 3:
				hj, bj = self.heap[3], self.blocks[3]
				hjp1, bjp1 = self.heap[4], self.blocks[4]
			elif j == 4:
				hj, bj = self.heap[4], self.blocks[4]
				hjp1, bjp1 = self.heap[5], self.blocks[5]
			elif j == 5:
				hj, bj = self.heap[5], self.blocks[5]
				hjp1, bjp1 = self.heap[6], self.blocks[6]
			elif j == 6:
				hj, bj = self.heap[6], self.blocks[6]
				hjp1, bjp1 = self.heap[7], self.blocks[7]
			elif j == 7:
				hj, bj = self.heap[7], self.blocks[7]
				hjp1, bjp1 = self.heap[8], self.blocks[8]
			elif j == 8:
				hj, bj = self.heap[8], self.blocks[8]
				hjp1, bjp1 = self.heap[9], self.blocks[9]
			elif j == 9:
				hj, bj = self.heap[9], self.blocks[9]
				hjp1, bjp1 = self.heap[10], self.blocks[10]
			elif j == 10:
				hj, bj = self.heap[10], self.blocks[10]
				hjp1, bjp1 = self.heap[11], self.blocks[11]
			elif j == 11:
				hj, bj = self.heap[11], self.blocks[11]
				hjp1, bjp1 = self.heap[12], self.blocks[12]
			elif j == 12:
				hj, bj = self.heap[12], self.blocks[12]
				hjp1, bjp1 = self.heap[13], self.blocks[13]
			elif j == 13:
				hj, bj = self.heap[13], self.blocks[13]
				hjp1, bjp1 = self.heap[14], self.blocks[14]
			elif j == 14:
				hj, bj = self.heap[14], self.blocks[14]
				hjp1, bjp1 = self.heap[15], self.blocks[15]
			elif j == 15:
				hj, bj = self.heap[15], self.blocks[15]

			if j + 1 < n and hj > hjp1:
				j += 1
				hj = hjp1
				bj = bjp1
			if v <= hj:
				break

			if k == 0:
				self.heap[0], self.blocks[0] = hj, bj
			elif k == 1:
				self.heap[1], self.blocks[1] = hj, bj
			elif k == 2:
				self.heap[2], self.blocks[2] = hj, bj
			elif k == 3:
				self.heap[3], self.blocks[3] = hj, bj
			elif k == 4:
				self.heap[4], self.blocks[4] = hj, bj
			elif k == 5:
				self.heap[5], self.blocks[5] = hj, bj
			elif k == 6:
				self.heap[6], self.blocks[6] = hj, bj
			elif k == 7:
				self.heap[7], self.blocks[7] = hj, bj
			elif k == 8:
				self.heap[8], self.blocks[8] = hj, bj
			elif k == 9:
				self.heap[9], self.blocks[9] = hj, bj
			elif k == 10:
				self.heap[10], self.blocks[10] = hj, bj
			elif k == 11:
				self.heap[11], self.blocks[11] = hj, bj
			elif k == 12:
				self.heap[12], self.blocks[12] = hj, bj
			elif k == 13:
				self.heap[13], self.blocks[13] = hj, bj
			elif k == 14:
				self.heap[14], self.blocks[14] = hj, bj
			elif k == 15:
				self.heap[15], self.blocks[15] = hj, bj
			k = j

		if k == 0:
			self.heap[0], self.blocks[0] = v, i
		elif k == 1:
			self.heap[1], self.blocks[1] = v, i
		elif k == 2:
			self.heap[2], self.blocks[2] = v, i
		elif k == 3:
			self.heap[3], self.blocks[3] = v, i
		elif k == 4:
			self.heap[4], self.blocks[4] = v, i
		elif k == 5:
			self.heap[5], self.blocks[5] = v, i
		elif k == 6:
			self.heap[6], self.blocks[6] = v, i
		elif k == 7:
			self.heap[7], self.blocks[7] = v, i
		elif k == 8:
			self.heap[8], self.blocks[8] = v, i
		elif k == 9:
			self.heap[9], self.blocks[9] = v, i
		elif k == 10:
			self.heap[10], self.blocks[10] = v, i
		elif k == 11:
			self.heap[11], self.blocks[11] = v, i
		elif k == 12:
			self.heap[12], self.blocks[12] = v, i
		elif k == 13:
			self.heap[13], self.blocks[13] = v, i
		elif k == 14:
			self.heap[14], self.blocks[14] = v, i
		elif k == 15:
			self.heap[15], self.blocks[15] = v, i


	def mergeBlocksHeapRegistersUnrolled(self, n):
		# Starts and ends of each block.
		for i in range(self.mergeSize):
			self.next[i] = i * self.blockSize
			self.end[i] = (i + 1) * self.blockSize - 1

		# Heap data.
		for i in range(self.maxRegisters):
			self.heap[i] = self.data[self.next[i]]
			self.blocks[i] = i

		# Initialise heap.
		k = self.mergeSize / 2 - 1
		while k >= 0:
			self.downHeapBlocksRegistersUnrolled(self.mergeSize, k)
			k -= 1

		# Repeated min finding using heap.
		for j in range(self.maxFrags):
			# Delete min and heapify.
			self.sortedData[j] = self.heap[0]
			if self.blocks[0] == 0 and self.next[0] + 1 <= self.end[0]:
				self.next[0] += 1
				self.heap[0] = self.data[self.next[0]]
			elif self.blocks[0] == 1 and self.next[1] + 1 <= self.end[1]:
				self.next[1] += 1
				self.heap[0] = self.data[self.next[1]]
			elif self.blocks[0] == 2 and self.next[2] + 1 <= self.end[2]:
				self.next[2] += 1
				self.heap[0] = self.data[self.next[2]]
			elif self.blocks[0] == 3 and self.next[3] + 1 <= self.end[3]:
				self.next[3] += 1
				self.heap[0] = self.data[self.next[3]]
			elif self.blocks[0] == 4 and self.next[4] + 1 <= self.end[4]:
				self.next[4] += 1
				self.heap[0] = self.data[self.next[4]]
			elif self.blocks[0] == 5 and self.next[5] + 1 <= self.end[5]:
				self.next[5] += 1
				self.heap[0] = self.data[self.next[5]]
			elif self.blocks[0] == 6 and self.next[6] + 1 <= self.end[6]:
				self.next[6] += 1
				self.heap[0] = self.data[self.next[6]]
			elif self.blocks[0] == 7 and self.next[7] + 1 <= self.end[7]:
				self.next[7] += 1
				self.heap[0] = self.data[self.next[7]]
			elif self.blocks[0] == 8 and self.next[8] + 1 <= self.end[8]:
				self.next[8] += 1
				self.heap[0] = self.data[self.next[8]]
			elif self.blocks[0] == 9 and self.next[9] + 1 <= self.end[9]:
				self.next[9] += 1
				self.heap[0] = self.data[self.next[9]]
			elif self.blocks[0] == 10 and self.next[10] + 1 <= self.end[10]:
				self.next[10] += 1
				self.heap[0] = self.data[self.next[10]]
			elif self.blocks[0] == 11 and self.next[11] + 1 <= self.end[11]:
				self.next[11] += 1
				self.heap[0] = self.data[self.next[11]]
			elif self.blocks[0] == 12 and self.next[12] + 1 <= self.end[12]:
				self.next[12] += 1
				self.heap[0] = self.data[self.next[12]]
			elif self.blocks[0] == 13 and self.next[13] + 1 <= self.end[13]:
				self.next[13] += 1
				self.heap[0] = self.data[self.next[13]]
			elif self.blocks[0] == 14 and self.next[14] + 1 <= self.end[14]:
				self.next[14] += 1
				self.heap[0] = self.data[self.next[14]]
			elif self.blocks[0] == 15 and self.next[15] + 1 <= self.end[15]:
				self.next[15] += 1
				self.heap[0] = self.data[self.next[15]]
			else:
				self.mergeSize -= 1
				if self.mergeSize == 1:
					self.heap[0], self.blocks[0] = self.heap[1], self.blocks[1]
				elif self.mergeSize == 2:
					self.heap[0], self.blocks[0] = self.heap[2], self.blocks[2]
				elif self.mergeSize == 3:
					self.heap[0], self.blocks[0] = self.heap[3], self.blocks[3]
				elif self.mergeSize == 4:
					self.heap[0], self.blocks[0] = self.heap[4], self.blocks[4]
				elif self.mergeSize == 5:
					self.heap[0], self.blocks[0] = self.heap[5], self.blocks[5]
				elif self.mergeSize == 6:
					self.heap[0], self.blocks[0] = self.heap[6], self.blocks[6]
				elif self.mergeSize == 7:
					self.heap[0], self.blocks[0] = self.heap[7], self.blocks[7]
				elif self.mergeSize == 8:
					self.heap[0], self.blocks[0] = self.heap[8], self.blocks[8]
				elif self.mergeSize == 9:
					self.heap[0], self.blocks[0] = self.heap[9], self.blocks[9]
				elif self.mergeSize == 10:
					self.heap[0], self.blocks[0] = self.heap[10], self.blocks[10]
				elif self.mergeSize == 11:
					self.heap[0], self.blocks[0] = self.heap[11], self.blocks[11]
				elif self.mergeSize == 12:
					self.heap[0], self.blocks[0] = self.heap[12], self.blocks[12]
				elif self.mergeSize == 13:
					self.heap[0], self.blocks[0] = self.heap[13], self.blocks[13]
				elif self.mergeSize == 14:
					self.heap[0], self.blocks[0] = self.heap[14], self.blocks[14]
				elif self.mergeSize == 15:
					self.heap[0], self.blocks[0] = self.heap[15], self.blocks[15]
			if self.mergeSize == 0:
				break
		self.downHeapBlocksRegistersUnrolled(self.mergeSize, 0)


	def sortData(self):
		block = [0] * self.blockSize

		# Sort each individual block.
		for i in range(0, len(self.data), self.blockSize):
			for j in range(0, self.blockSize):
				block[j] = self.data[i + j]
			sortedBlock = sorted(block)
			for j in range(0, self.blockSize):
				self.data[i + j] = sortedBlock[j]

		# Merge blocks using a heap.
		self.mergeBlocksHeapRegistersUnrolled(self.maxFrags)
		#print ''
"""




def test_heap_class():
	heap = BinaryHeap()
	heap.sortData()

	if heap.sortedData != sorted(heap.data):
		print 'not sorted!'
	else:
		print 'sorted!'

	print heap.sortedData


def test_heap_registers():
	heap = RegisterBinaryHeap()
	heap.sortData()

	if heap.sortedData != sorted(heap.data, reverse=True):
		print 'not sorted!'
	else:
		print 'sorted!'

	print heap.sortedData



test_heap_registers()

