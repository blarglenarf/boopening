import sys

def removeDups(filename):
	infile = open(filename, 'r')
	outfile = open('out.txt', 'w')

	maxfrags = 0
	filestr = ''
	for line in infile:
		outstr = ''

		# Ignore max frags line.
		if line.startswith('Max frags'):
			#outstr = line
			pass
		else:
			line = line.replace(' ', '')
			line = line.replace('\n', '')

			# Ignore start x,x: tag.
			mainToks = line.split(':')
			outstr += mainToks[0] + ': '
			
			# Put remaining x,x; tokens into a list.
			if len(mainToks) > 1:
				otherToks = mainToks[1].split(';')

				# Remove duplicates from the list.
				otherToks = list(set(otherToks))
				if len(otherToks) > maxfrags:
					maxfrags = len(otherToks)

				# Write resulting line back to new file.
				for tok in otherToks:
					if tok != '':
						outstr += tok + '; '
		filestr += outstr + '\n'
	outfile.write('Max frags: ' + str(maxfrags))
	outfile.write(filestr)
	infile.close()
	outfile.close()



if __name__ == '__main__':
	if len(sys.argv) < 2:
		print 'usage: python removeDups.py filename'
	else:
		filename = sys.argv[1]
		removeDups(filename)

