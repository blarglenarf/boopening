import random


# Pre-computed bvh, branching factor of 32
THREADS_PER_CLUSTER = 32

bvh = [
	-5.09436, 19.053,
	-5.09436, 3.51627, 0.245502, 8.89335, 5.15035, 13.5131, 10.1347, 19.053, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
	-5.09436, -2.06307, -4.5022, -1.73594, -4.84634, -1.13879, -4.48753, -0.739388, -4.23887, -0.586733, -3.01059, -1.54179, -3.66364, -0.506412, -3.79591, 0.13456, -3.74892, 0.241391, -3.18528, -0.105343, -3.37479, 0.402661, -2.92985, 0.281418, -3.12399, 0.712693, -2.93191, 0.990472, -1.69886, 0.149545, -2.17721, 0.911218, -1.986, 1.06752, -2.29665, 1.59699, -2.04165, 1.82354, -1.96338, 1.78967, -1.15859, 1.41241, -1.271, 1.93896, -1.19236, 2.04417, -0.96844, 2.18759, -0.819041, 2.36177, -0.957204, 2.63974, -0.320797, 2.42653, -0.077123, 2.4813, 0.451244, 2.50603, -0.363486, 3.51627, -0.0303305, 3.42726, 1.01463, 2.87439, 
	0.245502, 3.74594, 0.274823, 4.11395, 1.47901, 3.35424, 1.33098, 3.64609, 1.38815, 3.90534, 1.0542, 4.75101, 1.54526, 4.5592, 1.17579, 5.07101, 1.87256, 4.59437, 1.64621, 5.34251, 1.71644, 5.64447, 1.79551, 5.79384, 2.78984, 5.20073, 2.66737, 5.62628, 2.75918, 5.77164, 2.52928, 6.40452, 2.70725, 6.27847, 3.62673, 5.9232, 3.39426, 6.36852, 3.43335, 6.59656, 4.28858, 6.0566, 3.64602, 7.12472, 3.59097, 7.54988, 4.00162, 7.50035, 4.24212, 7.54225, 4.44804, 7.59359, 4.26563, 8.05188, 4.59727, 7.99748, 4.65563, 8.2086, 5.59666, 7.82307, 5.24123, 8.21797, 5.10127, 8.89335, 
	5.15035, 9.03572, 5.38068, 9.09759, 5.76602, 9.02183, 5.84068, 9.28815, 7.17932, 8.27384, 6.83038, 8.9449, 6.1705, 9.77033, 7.35176, 9.20121, 6.83796, 9.90909, 6.61563, 10.5122, 7.44364, 9.97088, 7.9458, 9.8402, 7.48667, 10.5901, 7.30905, 11.1325, 7.41939, 11.129, 7.83018, 11.0249, 7.66957, 11.5926, 8.82493, 10.7457, 8.97491, 10.8871, 8.0391, 12.0334, 9.11336, 11.2012, 9.27568, 11.2661, 8.56223, 12.4792, 8.75686, 12.5363, 8.95793, 12.6636, 9.97793, 11.843, 9.41805, 12.5209, 9.85654, 12.5464, 9.75727, 12.7581, 10.3035, 12.5686, 9.80188, 13.5131, 11.0144, 12.425, 
	10.1347, 13.667, 10.4325, 13.5407, 10.1874, 14.1106, 10.9729, 13.4475, 10.8496, 14.0924, 10.576, 14.5259, 11.5295, 14.0153, 11.0519, 14.9936, 12.2767, 13.8895, 12.7609, 13.7033, 11.6019, 14.9384, 11.7252, 15.1709, 12.4479, 14.7577, 12.3091, 15.552, 12.727, 15.3711, 12.1766, 16.0633, 13.4213, 15.2349, 12.6118, 16.4301, 14.0806, 15.2333, 13.1429, 16.3608, 13.1457, 16.8012, 13.4864, 16.6189, 13.719, 16.6148, 13.5088, 17.1108, 13.574, 17.2936, 14.1416, 17.133, 13.9456, 17.4994, 14.5899, 17.5748, 14.6695, 17.7622, 14.4683, 18.4067, 15.6079, 17.5882, 15.1736, 19.053
]


bvhParents = 33
bvhSize = 161


def intersectsAABB(vMin, vMax, aabbMin, aabbMax):
	if vMax < aabbMin or vMin > aabbMax:
		return False
	return True



def traverseBVH(clusterAabbMin, clusterAabbMax, left):
	parent = 0

	# Root node has only 1, rest have THREADS_PER_CLUSTER.
	nodesInBlock = 1

	while parent < bvhSize:

		# Left traversal, start at the first node, and sweep right, jump into the first intersecting node.
		if left == True:
			# If no bounding boxes in this group intersect, then there isn't an intersection.
			found = False
			child = 0

			for i in range(nodesInBlock):
				aabbMin = bvh[(parent + i) * 2]
				aabbMax = bvh[(parent + i) * 2 + 1]

				if intersectsAABB(clusterAabbMin, clusterAabbMax, aabbMin, aabbMax) == True:
					# Depth first traversal.
					found = True
					child = (parent + i) * THREADS_PER_CLUSTER + 1

					# If child exists, traverse into it.
					if child < bvhSize:
						parent = child
						break

					# Otherwise it doesn't exist, and we've found the bounding volume.
					else:
						return parent + i

			# If we checked all nodes and found no intersection, exit.
			if found == False:
				return -1

		# Right traversal, start at the last node, and sweep left, jump into the first intersecting node.
		else:
			# If no bounding boxes in this group intersect, then there isn't an intersection.
			found = False
			child = 0

			for i in range(nodesInBlock - 1, -1, -1):
				aabbMin = bvh[(parent + i) * 2]
				aabbMax = bvh[(parent + i) * 2 + 1]

				# If there are only zeroes, then the aabb isn't populated. Go to the next one.
				if aabbMin == 0 and aabbMax == 0:
					continue

				if intersectsAABB(clusterAabbMin, clusterAabbMax, aabbMin, aabbMax):
					# Depth first traversal.
					found = True
					child = (parent + i) * THREADS_PER_CLUSTER + 1

					# If child exists, traverse into it.
					if child < bvhSize:
						parent = child
						break

					# Otherwise it doesn't exist, and we've found the bounding volume.
					else:
						return parent + i

			# If we checked all nodes and found no intersection, exit.
			if found == False:
				return -1

		nodesInBlock = THREADS_PER_CLUSTER

	# This shouldn't happen.
	return -1


def test(clusterAabbMin, clusterAabbMax):
	# Traverse the bvh to find the first intersecting aabb.
	leftParent = traverseBVH(clusterAabbMin, clusterAabbMax, True)
	rightParent = traverseBVH(clusterAabbMin, clusterAabbMax, False)
	if leftParent == -1 or rightParent == -1:
		return -1, -1

	# Get the block associated with the parent.
	if THREADS_PER_CLUSTER * leftParent + 1 < bvhSize or THREADS_PER_CLUSTER * rightParent + 1 < bvhSize:
		return -1, -1

	#leftBlock = (THREADS_PER_CLUSTER * leftParent + 1) - bvhParents
	#rightBlock = (THREADS_PER_CLUSTER * rightParent + 1) - bvhParents
	#start = leftBlock
	#end = rightBlock + THREADS_PER_CLUSTER
	start = (leftParent - bvhParents) * THREADS_PER_CLUSTER
	end = (rightParent - bvhParents) * THREADS_PER_CLUSTER + THREADS_PER_CLUSTER
	return leftParent, rightParent, start, end



def getRand(a, b):
	return random.uniform(a, b)


print bvh
print bvhParents
print bvhSize

a = getRand(-5, 19)
b = getRand(-5, 19)

minV = min(a, b)
maxV = max(a, b)

a, b, start, end = test(minV, maxV)

print minV, maxV, a, b, start, end

#for i in range(a, b + 1):
#	print bvh[i * 2], bvh[i * 2 + 1]





