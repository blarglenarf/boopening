from math import *
from random import *

def create_random_list(size, s = 0):
	seed(s)
	l = [uniform(0, 10) for i in range(size)]
	candidate = [(d, j) for j, d in enumerate(l)]
	candidate_sorted = [j for f, j in sorted(candidate)]
	return l, candidate_sorted




def sort_insert(frags, min_pos, size):
	# Sort in place using insertion sort.
	for i in range(min_pos + 1, min_pos + size):
		f = frags[i]
		j = i - 1
		while j >= min_pos and frags[j] > f:
			frags[j + 1] = frags[j]
			j -= 1
		frags[j + 1] = f



def sort_rbs(frags):
	print frags
	MAX_REGISTERS = 8
	MAX_FRAGS = 16
	MERGE_SIZE = MAX_FRAGS / MAX_REGISTERS
	NEXT_SIZE = 2

	registers = [0] * MAX_REGISTERS * NEXT_SIZE
	next = [0] * MERGE_SIZE
	next_curr = [0] * MERGE_SIZE

	# Sort blocks in registers.
	fragCount = len(frags)
	mergeCount = 0

	for i in xrange(0, fragCount, MAX_REGISTERS):
		sort_insert(frags, i, min(fragCount - i, MAX_REGISTERS))
		mergeCount += 1

	# Instead of just one next value per block, have 2 or 4 per block that are all read at once.
	# Load frag section.
	print frags
	for i in xrange(MERGE_SIZE):
		if i < MERGE_SIZE and i < mergeCount:
			next[i] = min(fragCount, (i + 1) * MAX_REGISTERS) - 1
			for j in xrange(NEXT_SIZE):
				registers[i * NEXT_SIZE + j] = frags[next[i] - j]
	print registers
	print next

	# Merge blocks.
	out = [0] * fragCount
	for x in xrange(fragCount):
		n = 0
		f = 0
		# Check frag section.
		for i in xrange(MERGE_SIZE):
			if next[i] >= i * MAX_REGISTERS:
				if registers[i * NEXT_SIZE + next_curr[i]] > f:
					f = registers[i * NEXT_SIZE + next_curr[i]]
					n = i
		# Put frag section.
		for i in xrange(MERGE_SIZE):
			if n == i:
				# FIXME: There may not be enough data in this block...
				next_curr[i] += 1
				next[i] -= 1
				if next_curr[i] >= NEXT_SIZE and next[i] >= i * MAX_REGISTERS:
					next_curr[i] = 0
					for j in xrange(NEXT_SIZE):
						registers[i * NEXT_SIZE + j] = frags[next[i] - j]
		out[fragCount - 1 - x] = f
	for i in xrange(fragCount):
		frags[i] = out[i]
	print ''




def test_sort_insert():
	l, c = create_random_list(8, 0)
	sort_insert(l, 0, len(l))
	if l != sorted(l):
		print 'failed to sort:', l
	else:
		print 'sorted:', l



def test_sort_rbs():
	l, c = create_random_list(11, 0)
	s_l = sorted(l)
	sort_rbs(l)
	if l != s_l:
		print 'failed to sort:', l
	else:
		print 'sorted:', l




#test_sort_insert()
test_sort_rbs()

