from random import *
from math import *


def bitonicSortUnrollable(size):
	# Doesn't matter what values are here, comparison operations will be the same.
	a = [uniform(0, 10) for i in range(size)]
	logn = int(log(len(a), 2))

	s = ''
	s += '\t#define IF_CSWAP_B(i, j) if (LFB_FRAG_DEPTH(frags[i]) > LFB_FRAG_DEPTH(frags[j])) {SWAP_FRAGS(frags[i], frags[j]);}\n\n\t'

	for p in xrange(logn):
		for q in xrange(p + 1):
			d = 1 << (p - q)

			for i in xrange(len(a)):
				up = ((i >> p) & 2) == 0
				if (i & d) == 0: # FIXME: Not 100% sure if this is necessary or not...
					if up:
						s += 'IF_CSWAP_B(' + str(i) + ',' + str(i | d) + ');'
					else:
						s += 'IF_CSWAP_B(' + str(i | d) + ',' + str(i) + ');'
			s += '\n\t'
	s += '\n\t#undef IF_CSWAP_B\n'
	return s





# TODO: Can remove a bunch of if statements by checking if _MAX_FRAGS and LFB_BLOCK_SIZE is large enought.
def genUnrolledRegisterSort():
	s = '// BEGIN GENERATED\n\n'

	s += '#if SORT_REGISTERS\n\n'

	s += 'int sortRegisters(int fragIndex)\n'
	s += '{\n'

	# First load frags into registers.
	s += '\tint fragCount = 0;\n'
	s += '\tLFB_FRAG_TYPE tmp;\n\n'

	s += '\t#define LOAD_FRAG(i) if (LFB_ITER_CHECK(LFB_NAME)) { fragCount = i + 1; frags[i] = LFB_GET_FRAG(LFB_NAME); LFB_ITER_INC(LFB_NAME); }\n\n'

	s += '\tLFB_ITER_BEGIN(LFB_NAME, fragIndex);\n\t'
	for i in xrange(8):
		s += 'LOAD_FRAG(' + str(i) + ');'

	s += '\n\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n\t\t'
	for i in xrange(8, 16):
		s += 'LOAD_FRAG(' + str(i) + ');'
	s += '\n\t#endif\n'
	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n\t\t'
	for i in xrange(16, 32):
		s += 'LOAD_FRAG(' + str(i) + ');'
	s += '\n\t#endif\n'

	# If we have up to 64 frags, then we need to use a bitonic sort and we need to populate all the frags.
	# TODO: Maybe try a bitonic sort that doesn't populate all the frags.
	s += '\t#undef LOAD_FRAG\n'
	s += '\t#define LOAD_FRAG(i) if (LFB_ITER_CHECK(LFB_NAME)) { fragCount = i + 1; frags[i] = LFB_GET_FRAG(LFB_NAME); LFB_ITER_INC(LFB_NAME); } else { frags[i] = vec2(0, 9999); }\n\n'
	s += '\t#if _MAX_FRAGS > 32 && LFB_BLOCK_SIZE > 32\n\t\t'
	for i in xrange(32, 64):
		s += 'LOAD_FRAG(' + str(i) + ');'
	s += '\n\t#endif\n'

	s += '\n\t#undef LOAD_FRAG\n\n'

	# Now perform the sort.
	s += '#if IS_SORT_NETWORK_FREE // IS_SORT_NETWORK_FREE.\n'
	s += '\t// Sort in registers.\n'
	s += '\t#define SWAP_FRAGS(a, b) {tmp = a; a = b; b = tmp;}\n'
	s += '\t#define IF_CSWAP_I(i, j) if (LFB_FRAG_DEPTH(frags[i]) > LFB_FRAG_DEPTH(frags[j])) {SWAP_FRAGS(frags[i], frags[j]);\n'
	for i in xrange(1, 8):
		s += '\tif (fragCount > ' + str(i) + ') {'
		for j in xrange(i, 0, -1):
			s += 'IF_CSWAP_I(' + str(j - 1) + ',' + str(j) + ')'
		for j in xrange(i, 0, -1):
			s += '}'
		s += '\n'

	s += '\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n'
	for i in xrange(8, 16):
		s += '\t\tif (fragCount > ' + str(i) + ') {'
		for j in xrange(i, 0, -1):
			s += 'IF_CSWAP_I(' + str(j - 1) + ',' + str(j) + ')'
		for j in xrange(i, 0, -1):
			s += '}'
		s += '\n'
	s += '\t#endif\n'

	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n'
	for i in xrange(16, 32):
		s += '\t\tif (fragCount > ' + str(i) + ') {'
		for j in xrange(i, 0, -1):
			s += 'IF_CSWAP_I(' + str(j - 1) + ',' + str(j) + ')'
		for j in xrange(i, 0, -1):
			s += '}'
		s += '\n'
	s += '\t#endif\n'

	s += '\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n\t\t'
	for i in xrange(8, 16):
		s += '}'
	s += '\n\t#endif\n'
	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n\t\t'
	for i in xrange(16, 32):
		s += '}'
	s += '\n\t#endif\n'
	s += '\t'
	for i in xrange(7):
		s += '}'
	s += '\n\t#undef IF_CSWAP_I\n'

	# Insertion sort fails for 64 frags. Need to use a bitonic sort instead.
	s += '\n\t// Bitonic sort for 32 <= fragCount <= 64.\n'
	s += '\t#if _MAX_FRAGS > 32 && LFB_BLOCK_SIZE > 32\n'
	s += bitonicSortUnrollable(64)
	#for i in xrange(32, 64):
	#	s += '\t\tif (fragCount > ' + str(i) + ') {'
	#	for j in xrange(i, 0, -1):
	#		s += 'IF_CSWAP_I(' + str(j - 1) + ',' + str(j) + ')'
	#	for j in xrange(i, 0, -1):
	#		s += '}'
	#	s += '\n'
	s += '\t#endif\n'

	s += '\n\t#undef SWAP_FRAGS\n'

	s += '\n#endif // IS_SORT_NETWORK_FREE.\n\n'

	# Finally, blend.
	s += '\t#define BLEND(i) if (fragCount >= i) {vec4 col = floatToRGBA8(frags[i].x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);}\n'
	s += '\t#if _MAX_FRAGS > 32 && LFB_BLOCK_SIZE > 32\n\t\t'
	for i in xrange(64, 32, -1):
		s += 'BLEND(' + str(i - 1) + ');'
	s += '\n\t#endif\n'
	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n\t\t'
	for i in xrange(32, 16, -1):
		s += 'BLEND(' + str(i - 1) + ');'
	s += '\n\t#endif\n'
	s += '\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n\t\t'
	for i in xrange(16, 8, -1):
		s += 'BLEND(' + str(i - 1) + ');'
	s += '\n\t#endif\n\t'
	for i in xrange(8, 0, -1):
		s += 'BLEND(' + str(i - 1) + ');'

	s += '\n\t#undef BLEND\n'

	# In case we want to write the result back to global memory.
	s += '\n#if GLOBAL_WRITE\n'
	s += '\tLFB_ITER_BEGIN(LFB_NAME, fragIndex);\n'
	s += '\t#define PUT_FRAG(i) { {LFB_PUT_FRAG(LFB_NAME, frags[i]);} {LFB_ITER_INC(LFB_NAME);} }\n'
	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n\t\t'
	for i in xrange(32, 16, -1):
		s += 'PUT_FRAG(' + str(i - 1) + ');'
	s += '\n\t#endif\n'
	s += '\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n\t\t'
	for i in xrange(16, 8, -1):
		s += 'PUT_FRAG(' + str(i - 1) + ');'
	s += '\n\t#endif\n\t'
	for i in xrange(8, 0, -1):
		s += 'PUT_FRAG(' + str(i - 1) + ');'
	s += '\n\t#undef PUT_FRAG\n'
	s += '#endif\n'

	s += '\n\treturn fragCount;\n'
	s += '}\n'
	s += '#endif\n'

	s += '\n// END GENERATED\n'

	print s



# TODO: See what happens if we instead load from global memory rather than local memory.
def genUnrolledRbsBlock():
	s = '// BEGIN GENERATED\n\n'

	#s += '#if SORT_REGISTERS\n\n'

	s += 'void registerSortRange(int offset, int count)\n'
	s += '{\n'

	# First load frags into registers.
	#s += '\tint fragCount = 0;\n'
	s += '\tLFB_FRAG_TYPE tmp;\n\n'

	#s += '\t#define LOAD_FRAG(i) if (LFB_ITER_CHECK(LFB_NAME)) { fragCount = i + 1; frags[i] = LFB_GET_FRAG(LFB_NAME); LFB_ITER_INC(LFB_NAME); }\n\n'
	s += '\t#define LOAD_FRAG(i) if (i < count && offset + i < _MAX_FRAGS) { registers[i] = frags[offset + i]; } else { registers[i] = vec2(0, 9999); }\n\n\t'

	#s += '\tLFB_ITER_BEGIN(LFB_NAME, fragIndex);\n\t'
	for i in xrange(8):
		s += 'LOAD_FRAG(' + str(i) + ');'

	s += '\n\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n\t\t'
	for i in xrange(8, 16):
		s += 'LOAD_FRAG(' + str(i) + ');'
	s += '\n\t#endif\n'
	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n\t\t'
	for i in xrange(16, 32):
		s += 'LOAD_FRAG(' + str(i) + ');'
	s += '\n\t#endif\n'

	s += '\n\t#undef LOAD_FRAG\n\n'

	# Now perform the sort.
	s += '#if IS_SORT_NETWORK_FREE // IS_SORT_NETWORK_FREE.\n'
	s += '\t// Sort in registers.\n'
	s += '\t#define SWAP_FRAGS(a, b) {tmp = a; a = b; b = tmp;}\n'
	s += '\t#define IF_CSWAP_I(i, j) if (LFB_FRAG_DEPTH(registers[i]) > LFB_FRAG_DEPTH(registers[j])) {SWAP_FRAGS(registers[i], registers[j]);\n'
	for i in xrange(1, 8):
		s += '\tif (count > ' + str(i) + ') {'
		for j in xrange(i, 0, -1):
			s += 'IF_CSWAP_I(' + str(j - 1) + ',' + str(j) + ')'
		for j in xrange(i, 0, -1):
			s += '}'
		s += '\n'

	s += '\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n'
	for i in xrange(8, 16):
		s += '\t\tif (count > ' + str(i) + ') {'
		for j in xrange(i, 0, -1):
			s += 'IF_CSWAP_I(' + str(j - 1) + ',' + str(j) + ')'
		for j in xrange(i, 0, -1):
			s += '}'
		s += '\n'
	s += '\t#endif\n'

	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n'
	for i in xrange(16, 32):
		s += '\t\tif (count > ' + str(i) + ') {'
		for j in xrange(i, 0, -1):
			s += 'IF_CSWAP_I(' + str(j - 1) + ',' + str(j) + ')'
		for j in xrange(i, 0, -1):
			s += '}'
		s += '\n'
	s += '\t#endif\n'

	s += '\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n\t\t'
	for i in xrange(8, 16):
		s += '}'
	s += '\n\t#endif\n'
	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n\t\t'
	for i in xrange(16, 32):
		s += '}'
	s += '\n\t#endif\n'
	s += '\t'
	for i in xrange(7):
		s += '}'

	s += '\n\t#undef IF_CSWAP_I\n'
	s += '\n\t#undef SWAP_FRAGS\n'

	s += '\n#endif // IS_SORT_NETWORK_FREE.\n\n'

	# Copy back to local array.
	#s += '\t#define PUT_FRAG(i) if (fragCount >= i) {vec4 col = floatToRGBA8(frags[i].x); fragColor.rgb = mix(fragColor.rgb, col.rgb, col.a);}\n'
	s += '\t#define PUT_FRAG(i) if (i < count && offset + i < _MAX_FRAGS) { frags[offset + i] = registers[i]; }\n\t'
	for i in xrange(8):
		s += 'PUT_FRAG(' + str(i) + ');'
	s += '\n\t#if _MAX_FRAGS > 8 && LFB_BLOCK_SIZE > 8\n\t\t'
	for i in xrange(8, 16):
		s += 'PUT_FRAG(' + str(i) + ');'
	s += '\n\t#endif\n\t'
	s += '\t#if _MAX_FRAGS > 16 && LFB_BLOCK_SIZE > 16\n\t\t'
	for i in xrange(16, 32):
		s += 'PUT_FRAG(' + str(i - 1) + ');'
	s += '\n\t#endif\n'

	s += '\n\t#undef PUT_FRAG\n'
	s += '}\n'

	s += '\n// END GENERATED\n'

	print s




def genUnrolledRbsMerge():
	s = ''
	for i in xrange(128, 256):
		s += 'PUT_FRAG(' + str(i) + ')'
	print s




genUnrolledRegisterSort()
#genUnrolledRbsMerge()


