import random
import math


def sign(x):
	if x > 0:
		return 1
	if x < 0:
		return -1
	return 0


def frac0(x):
	return x - math.floor(x)


def frac1(x):
	return 1 - x + math.floor(x)


def traverse(start_x, start_y, start_z, end_x, end_y, end_z):
	# Correct voxel traversal algorithm:
	# https://stackoverflow.com/questions/12367071/how-do-i-initialize-the-t-variables-in-a-fast-voxel-traversal-algorithm-for-ray

	# TODO: Traverse through x and y, get the z range for each x,y cell.

	ss_x, ss_y, ss_z = int(start_x), int(start_y), start_z

	dd_x, dd_y, dd_z = end_x - start_x, end_y - start_y, end_z - start_z
	s_x, s_y, s_z = sign(dd_x), sign(dd_y), sign(dd_z)

	td_x, td_y, td_z = 0, 0, 0
	tMax_x, tMax_y, tMax_z = 0, 0, 0

	if dd_x != 0:
		td_x = min(s_x / float(dd_x), 10000000.0)
	else:
		td_x = 10000000.0
	if dd_x > 0:
		tMax_x = td_x * frac1(start_x)
	else:
		tMax_x = td_x * frac0(start_x)

	if dd_y != 0:
		td_y = min(s_y / float(dd_y), 10000000.0)
	else:
		td_y = 10000000.0
	if dd_y > 0:
		tMax_y = td_y * frac1(start_y)
	else:
		tMax_y = td_y * frac0(start_y)

	if dd_z != 0:
		td_z = min(s_z / float(dd_z), 10000000.0)
	else:
		td_z = 10000000.0
	if dd_z > 0:
		tMax_z = td_z * frac1(start_z)
	else:
		tMax_z = td_z * frac0(start_z)

	print 'dd:', dd_x, dd_y, dd_z
	print 's:', s_x, s_y, s_z
	print 'td:', td_x, td_y, td_z
	print 'tMax:', tMax_x, tMax_y, tMax_z

	while True:
		if tMax_x > 1 and tMax_y > 1:# and tMax_z > 1:
			break
		print ss_x, ss_y, ',', #, ss_z, ',',

		min_z, max_z = ss_z, ss_z
		tm = min(tMax_x, tMax_y)
		if tMax_z <= tm:
			amt = math.ceil((tm - tMax_z) / td_z)
			tMax_z += td_z * amt
			ss_z += s_z * amt
		#while tMax_z <= tm:
		#	tMax_z += td_z
		#	ss_z += s_z

		#if tMax_x < tMax_y:
		#	while tMax_z <= tMax_x:
		#		tMax_z += td_z
		#		ss_z += s_z
		#else:
		#	while tMax_z <= tMax_y:
		#		tMax_z += td_z
		#		ss_z += s_z
		max_z = ss_z
		print math.floor(min_z), "-", math.ceil(max_z)

		if tMax_x < tMax_y:
			#if tMax_x < tMax_z:
				#print 'a',
				tMax_x += td_x
				ss_x += s_x
			#else:
			#	print 'b',
			#	tMax_z += td_z
			#	ss_z += s_z
		else:
			#if tMax_y < tMax_z:
				#print 'c',
				tMax_y += td_y
				ss_y += s_y
			#else:
			#	print 'd',
			#	tMax_z += td_z
			#	ss_z += s_z


def test_voxel_traverse(s = 0):
	random.seed(s)
	s_x, s_y, s_z = int(random.uniform(0, 10)), int(random.uniform(0, 10)), random.uniform(0, 30)
	e_x, e_y, e_z = int(random.uniform(0, 10)), int(random.uniform(0, 10)), random.uniform(0, 30)
	print 'start:', s_x, s_y, s_z
	print 'end:', e_x, e_y, e_z
	traverse(s_x, s_y, s_z, e_x, e_y, e_z)


# Example for 5,4,5 - 2,8,8
# 5,4: 5-5
# 4,4: 5-5
# 4,5: 5-6
# 3,5: 6-6
# 3,6: 6-7
# 2,6: 7-7
# 2,7: 7-8
# 2,8: 8-8
test_voxel_traverse(10)

