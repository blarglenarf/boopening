import math

# 1D R-TREE as described at http://lin-ear-th-inking.blogspot.com.au/2007/06/packed-1-dimensional-r-tree.html









def mid(i):
	lo, hi = i
	return (lo + hi) / 2.0


def log2(i):
	return int(math.ceil(math.log(i) / math.log(2.0)))

def next_pow2(x):
	return 1 << (x - 1).bit_length()



def sort_intervals(intervals):
	# Sort in place using insertion sort.
	for i in range(1, len(intervals)):
		f = intervals[i]
		m = mid(f)
		j = i - 1
		while j >= 0 and mid(intervals[j]) > f:
			intervals[j + 1] = intervals[j]
			j -= 1
		intervals[j + 1] = f


intervals = [(0.4, 1.1), (0.6, 2.1), (1.9, 2.7), (2.0, 2.5), (2.9, 4.0), (3.1, 3.7), (3.5, 4.1), (4.2, 4.8), (4.5, 5.0)]

sort_intervals(intervals)

size = next_pow2(len(intervals)) - 1

print size
"""
print curr_amt

numLevels = 1 + log2(curr_amt)

print numLevels

size = 0

for i in range(0, numLevels):
	curr_amt = math.ceil(curr_amt / 2.0)
	size += curr_amt

print size
"""
print intervals

