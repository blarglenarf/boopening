import os

from tempfile import mkstemp
from shutil import move
from os import remove, close

#def replace(file_path, pattern, subst):
#	#Create temp file
#	fh, abs_path = mkstemp()
#	with open(abs_path,'w') as new_file:
#		with open(file_path) as old_file:
#			for line in old_file:
#				new_file.write(line.replace(pattern, subst))
#	close(fh)
#	#Remove original file
#	remove(file_path)
#	#Move new file
#	move(abs_path, file_path)

# ShaderSource::createFromFile(str1, str2)
# ShaderSourceCache::getShader(str1).load(str2)

searchStr = 'ShaderSource::createFromFile'
replaceStr = 'ShaderSourceCache::getShader'

f = []

# Uncomment me if you dare!!!
#for (dirpath, dirnames, filenames) in os.walk('../'):
#	f.extend(dirpath + '/' + name for name in filenames if (name.endswith('.cpp') or name.endswith('.c') or name.endswith('.h')) and name != 'ShaderSource.cpp' and searchStr in open(dirpath + '/' + name).read())

#print f

# Risky...
for filepath in f:
	print filepath
	fh, abs_path = mkstemp()
	with open(abs_path, 'w') as new_file:
		with open(filepath) as old_file:
			for line in old_file:
				if searchStr in line:
					startLoc = line.find(searchStr)
					argOneLoc = line.find('(', startLoc) + 1
					argTwoLoc = line.find(',', argOneLoc)
					endLoc = line.find(')', argTwoLoc)
					argOne = line[argOneLoc : argTwoLoc]
					argTwo = line[argTwoLoc + 2 : endLoc]
					origStr = searchStr + '(' + argOne + ', ' + argTwo + ')'
					newStr = replaceStr + '(' + argOne + ').loadFromFile(' + argTwo + ')'
					new_file.write(line.replace(origStr, newStr))
				else:
					new_file.write(line)
	close(fh)
	remove(filepath)
	move(abs_path, filepath)

print 'Fin'
