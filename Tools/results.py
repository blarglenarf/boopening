import csv
import re
import sys
import operator

#
# Example input format:
#
# NVIDIA Corporation GeForce GTX 1060 6GB/PCIe/SSE2 4.5.0 NVIDIA 367.44
# Test,Cluster Count (ms),Cluster Mask (ms),Cluster compact (ms),Cluster prefix (ms),Create Frustums (ms),Geometry Capture (ms),Light Capture (ms),Light Cluster (ms),Light Depth Clear (ms),Light Draw (ms),Light Deferred (ms),Total (ms),clusterData (mb),clusterHeadPtrs (mb),clusterIndices (mb),clusterMasks (mb),clusterNextPtrs (mb),clusterOffsets (mb),clusterCounts (mb),lfbCounts (mb),lfbData (mb),lfbHeadPtrs (mb),lfbNextPtrs (mb),lightDepths (mb),g-buffer (mb),
# Opaque Linear Deferred Shading 4096 lights,0,0,0,0,0,0.630948,0,0,0,0,1.49578,2.18865,0,0,0,0,0,0,0,0,0,0,0,0,17.5565,
#
#
# Example output format:
#
# Thing being measured
# Lights,Technique1,Technique2,...
# 4096,2.1,5.2,...
#
# Don't forget to add a total (mb)
#


if __name__ == '__main__':

	if len(sys.argv) != 4:
		print 'Usage: python results.py input.csv output.csv desired_key'
		sys.exit()

	input_file = open(sys.argv[1])
	output_file = open(sys.argv[2], 'w')
	desired_key = sys.argv[3]

	# TODO: Separate tables for opaque and transparent link list/linear.

	# Grab the first row so it can be put into the new csv file, and ignore it in the csv reading.
	platform = input_file.readline()
	reader = csv.DictReader(input_file)

	output_dict = {}
	output_csv = platform

	for row in reader:
		d = {}
		pat = re.compile('^[^(]*')
		total_mem = 0
		technique = ''
		lights = ''

		for key, val in row.items():
			if key == '':
				continue

			# If the key ends in (mb), add its value to total memory usage.
			if key.endswith('(mb)'):
				total_mem += float(val)

			# Strip out stuff in brackets if necessary.
			#key = pat.match(key).group(0)

			# If key is the technique, extract name and number of lights.
			# eg: Opaque Linear Deferred Shading 4096 lights.
			if key == 'Test':
				t = val.split()
				technique = ' '.join(t[2:-2])
				lights = t[-2]
				d['Technique'] = technique
				d['Lights'] = int(lights)
			else:
				d[key] = val

		# Add an extra d value, counting total data usage in mb.
		d['totalMem (mb)'] = total_mem

		# Now that we have data in a more easily accessible format, extract the stuff that we need.
		for key in d:
			if key.startswith(desired_key):
				if d['Lights'] not in output_dict:
					output_dict[d['Lights']] = {}
				output_dict[d['Lights']][d['Technique']] = d[key]

	# Lights in ascending order.
	sorted_lights = sorted(output_dict.items(), key=operator.itemgetter(0))

	# First print a row containing all the techniques.
	output_csv += 'Lights,'
	for key, val in sorted_lights:
		for technique in val:
			output_csv += technique + ','
		output_csv += '\n'
		break

	# Now add useful csv output.
	for key, val in sorted_lights:
		output_csv += str(key) + ','
		for technique in val:
			output_csv += str(output_dict[key][technique]) + ','
		output_csv += '\n'

	# Write csv output.
	input_file.close()
	output_file.write(output_csv)
	output_file.close()

