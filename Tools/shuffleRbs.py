from math import *
from random import *

def create_random_list(size, s = 0):
	seed(s)
	l = [int(uniform(0, 10)) for i in range(size)]
	candidate = [(d, j) for j, d in enumerate(l)]
	candidate_sorted = [j for f, j in sorted(candidate)]
	return l, candidate_sorted




def sort_insert(frags, min_pos, size):
	# Sort in place using insertion sort.
	for i in range(min_pos + 1, min_pos + size):
		f = frags[i]
		j = i - 1
		while j >= min_pos and frags[j] > f:
			frags[j + 1] = frags[j]
			j -= 1
		frags[j + 1] = f



def sort_rbs_shuffle(frags):
	print frags
	N_THREADS = 8
	MAX_FRAGS = 16
	MAX_REGISTERS = MAX_FRAGS / N_THREADS
	counts = [0] * N_THREADS
	next = [0] * N_THREADS

	# Sort blocks in registers.
	fragCount = len(frags)

	# Poorly simulated multi-threaded approach.
	for thread in xrange(N_THREADS):
		# First need counts/next for each thread.
		offset = thread * MAX_REGISTERS
		counts[thread] = MAX_REGISTERS
		if fragCount <= offset:
			counts[thread] = 0
		elif fragCount < (thread + 1) * MAX_REGISTERS:
			counts[thread] = (thread + 1) * MAX_REGISTERS - fragCount
		next[thread] = counts[thread] - 1

		# Then insertion sort the block for this thread.
		if counts[thread] > 0:
			sort_insert(frags, offset, counts[thread])

	# Now that all blocks are sorted, do the butterfly merge.
	out = [0] * fragCount
	outPos = 0
	for i in xrange(fragCount):
		# Get max value from all blocks.
		currVal = 0
		currThread = 0
		for thread in xrange(N_THREADS):
			maxVal = 0
			if next[thread] >= 0:
				maxVal = frags[thread * MAX_REGISTERS + next[thread]]
			if maxVal > currVal:
				currVal = maxVal
				# TODO: Need a way for different threads to read this value.
				currThread = thread

		# This is where the composite would happen.
		out[outPos] = currVal
		outPos += 1

		if next[currThread] >= 0:
			next[currThread] -= 1

	for i in xrange(len(frags)):
		frags[len(frags) - i - 1] = out[i]
	print ''




def test_sort_insert():
	l, c = create_random_list(8, 0)
	sort_insert(l, 0, len(l))
	if l != sorted(l):
		print 'failed to sort:', l
	else:
		print 'sorted:', l



def test_sort_rbs():
	l, c = create_random_list(11, 0)
	s_l = sorted(l)
	sort_rbs_shuffle(l)
	if l != s_l:
		print 'failed to sort:'
		print l
		print s_l
	else:
		print 'sorted:', l


def test_block_sizes(pow2, fragCount, threads):
	registers = pow2 / threads
	for thread in xrange(threads):
		offset = thread * registers
		count = registers
		if fragCount <= offset:
			count = 0
		elif fragCount < (thread + 1) * registers:
			count = fragCount % registers
		print offset, count, count - 1


test_block_sizes(128, 73, 32)

#test_sort_insert()
test_sort_rbs()

