import math
from random import *

def create_random_list(size = 120, s = 0):
	seed(s)
	l1 = [int(uniform(0, size)) for i in range(1000)]
	l2 = [int(uniform(0, size)) for i in range(1000)]
	out = []
	for i in xrange(size):
		if l1[i] != l2[i]:
			out += [(l1[i], l2[i])]
			out += [(l2[i], l1[i])]
	return out


def check_prime(n):
	if n <= 1:
		return False
	elif n <= 3:
		return True
	elif n % 2 == 0 or n % 3 == 0:
		return False
	i = 5
	while i * i <= n:
		if n % i == 0 or n % (i + 2) == 0:
			return False
		i += 6
	return True


def get_prime_factors(n):
	i = 2
	factors = []
	while i * i <= n:
		if n % i:
			i += 1
		else:
			n //= i
			factors.append(i)
	if n > 1:
		factors.append(n)
	return factors



# TODO: Apparently this has some bugs in it, somewhere. Find them and kill them.
def gen_primes(limit = 1000):
	# set of wheel "hit" positions for a 2/3/5 wheel rolled twice as per the Atkin algorithm
	s = [1,7,11,13,17,19,23,29,31,37,41,43,47,49,53,59]
	is_prime = [False] * (limit + 1)

	# Initialize the sieve with enough wheels to include limit:
	#for w in range(limit / 60 + 1):
	#	for x in s:
	#		is_prime[60 * w + x] = False # Not really necessary unless using a sparse data structure.

	# Put in candidate primes:
	#   integers which have an odd number of
	#   representations by certain quadratic forms.
	for x in xrange(1, int(math.ceil(math.sqrt(limit))) + 1):
		y_sub = 1
		for y in xrange(1, int(math.ceil(math.sqrt(limit))) + 1):
			# Algorithm step 3.1 (all x, odd y):
			if y % 2 != 0:
				n = 4 * x * x + y * y
				if n <= limit:
					t = [1,13,17,29,37,41,49,53]
					if n % 60 in t:
						is_prime[n] = not is_prime[n]
			# Algorithm step 3.2 (odd x, even y):
			if x % 2 != 0 and y % 2 == 0:
				n = 3 * x * x + y * y
				if n <= limit:
					t = [7,19,31,43]
					if n % 60 in t:
						is_prime[n] = not is_prime[n]
			# Algorithm step 3.3:
			if x > 1 and y == x - y_sub:
				n = 2 * x * x - y * y
				if n <= limit:
					t = [11,23,47,59]
					if n % 60 in t:
						is_prime[n] = not is_prime[n]
			y_sub += 2

	# Eliminate composites by sieving, only for those occurrences on the wheel:
	fin = False
	for x in s:
		for w in xrange(limit * limit + 1):
			n = 60 * w + x
			if n < 7:
				continue
			if n * n > limit:
				fin = True
				break
			if is_prime[n]:
				# n is prime, omit multiples of its square; this is sufficient, because square-free composites can't get on this list.
				finfin = False
				for xx in s:
					for ww in xrange(limit * limit + 1):
						c = n * n * (60 * ww + xx)
						if c > limit:
							finfin = True
							break
						is_prime[c] = False
					if finfin:
						break
		if fin:
			break

	# one sweep to produce a sequential list of primes up to limit: output 2, 3, 5.
	prime_list = []
	fin = False
	for w in xrange(limit + 1):
		if len(prime_list) > 300000:
			break
		for x in s:
			n = 60 * w + x
			if n > limit:
				fin = True
				break
			if is_prime[n]:
				if check_prime(n): # TODO: Ideally this should not be necessary, if I wasn't a bad programmer.
					prime_list += [n]
		if fin:
			break

	#print prime_list
	print len(prime_list)
	print prime_list[-1]

	return prime_list


class Polygon:
	def __init__(self, i, p):
		self.id = i
		self.prime = p
		self.total = p
	def combine(self, p):
		self.total *= p.prime
	def check(self, p):
		if self.id == p.id:
			return False
		if self.total % p.prime == 0:
			return True
		return False

prime_list = gen_primes()
poly_list = []

for i in xrange(len(prime_list)):
	poly_list += [Polygon(i, prime_list[i])]

# Now just combine a bunch of them and see what happens.
combo_list = create_random_list()
#print combo_list

for i1, i2 in combo_list:
	if not poly_list[i1].check(poly_list[i2]):
		poly_list[i1].combine(poly_list[i2])
		poly_list[i2].combine(poly_list[i1])


# Now check to make sure that each polygon has only been combined with something in combos:
for p1 in poly_list:
	for p2 in poly_list:
		if p1.check(p2):
			found = False
			for c1, c2 in combo_list:
				if (c1 == p1.id and c2 == p2.id) or (c1 == p2.id and c2 == p1.id):
					found = True
					break
			if not found:
				print p1.id, p2.id

# That's pretty much it.
print 'fin'


#poly_list[10].combine(poly_list[20])
#poly_list[20].combine(poly_list[10])
#
#poly_list[10].combine(poly_list[15])
#poly_list[15].combine(poly_list[10])
#
#poly_list[25].combine(poly_list[10])
#poly_list[10].combine(poly_list[25])
#
#poly_list[20].combine(poly_list[25])
#poly_list[25].combine(poly_list[20])











